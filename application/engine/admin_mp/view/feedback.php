<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Marketing Partner Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
  <!-- jvectormap -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $feedback = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <?php
	if($_SESSION['app_user']['mp_details_step']!=2)
	{
	?> 
    	<section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row" style="background-color: red;opacity: 0.6;color: #fff;margin-top: -15px;">
                  <div class="col-lg-12">
                      <section>
                        <h4>Profile Incomplete!</h4>
                        <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile" style="color: #000;font-weight: bold;">Click Here</a> to update your profile.</p>			
                      </section>
                  </div>
              </div>
           </section>
        </section>                
              
	<?php }else{?>
  
  
  <section id="main-content">
          <section class="wrapper">
              <!--state overview start-->
              <div class="row state-overview">
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <div class="symbol terques">
                              <i class="fa fa-user"></i>
                          </div>
                          <div class="value">
                              <h1 class="count">
                                  123
                              </h1>
                              <p>Regsitered Users</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6 pull-right">
                          <button type="button" class="btn btn-success btn-sm" class="btn btn-danger" data-target="#sendMessage" href="<?php echo APP_URL."superadmin/all_notifications/view_gallery_profile";?>" data-toggle="modal">Send Message</button>
                  </div>
              </div>
              <!--state overview end-->
			  <section class="panel">
              	<div class="panel-body">
                                <div class="adv-table">
                                    <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Amount Purchased</th>
                  <th>Deals Crack</th>
                </tr>
                </thead>
               <tbody>
					<tr role="row">
                   	 <td>1</td>
                    <td>Deepak Bhardwaj</td>
                    <td>INR 5,000</td>
                    <td>10</td>
                </tbody>
              </table>
                                </div>
                          </div>
              </section>
              
          </section>
      </section>
  
  
   <?php } ?>
  
  
  <!-- /.content-wrapper -->
  
</section>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="sendMessage">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>
</body>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!--<script type="text/javascript" src="<?php //echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php //echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/select/1.1.2/js/dataTables.select.min.js"></script>
<script>
 $(document).ready(function() {
   var table = $('#example').DataTable( {
		dom: 'Bfrtip',
        lengthChange: false,
		select: true,
        buttons: []
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
} );
</script>
<!-- AdminLTE App -->
<script>
function showData1(startDate, endDate)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/analytics_mp/analytics_data_dashboard_mp',
            type: 'POST',
			data: {"startDate" : startDate, "endDate" : endDate},
            success: function (data) {
				$('#loading').hide();
				$('#data1').html(data);
            },
            error: function (data) {
            }
   	});
}
$(function () {
	
	// set current month startdate and enddate
	  var d = new Date();	  
	  var firstDay = new Date(d.getFullYear(), d.getMonth(), 1);	  
	  var month = firstDay.getMonth()+1;
	  var day = firstDay.getDate();
	  var startDate = firstDay.getFullYear() +'/' + ((''+month).length<2 ? '0' : '') + month + '/' + ((''+day).length<2 ? '0' : '') + day;
	  
	  var lastDay = new Date(d.getFullYear(), d.getMonth() + 1, 0);
	  var month1 = lastDay.getMonth()+1;
	  var day1 = d.getDate();
	  var endDate = lastDay.getFullYear() +'/' + ((''+month1).length<2 ? '0' : '') + month1 + '/' + ((''+day1).length<2 ? '0' : '') + day1;
	  showData1(startDate, endDate);
	  
	  // function to execute dashboard data in 10 seconds
	  setInterval(function(){showData1(startDate, endDate);}, 10000);
  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</html>
