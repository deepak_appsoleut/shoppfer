<?php 
	$new_array = array();
	foreach($data[0] as $arr){
		$new_array[] = array_values($arr);
	}
	$dateArray = array();
	$values = array();
	$values1 = array();
	foreach($data[1] as $arr=>$val){
		foreach($val as $key=>$val1)
		{
			if($key=="label")
			{
				array_push($dateArray, $val1);
			}
			else if($key=="cnt")
			{
				array_push($values, $val1);
			}
		}
	}
	foreach($data[2] as $arr=>$val){
		foreach($val as $key=>$val1)
		{
			if($key=="cnt")
			{
				array_push($values1, $val1);
			}
		}
	}
	//echo json_encode($data[3]);
	//echo json_encode($data[2]);
?>

<div class="row">
        <!-- /.col -->
		<div class="col-md-6">
          <!-- Bar chart -->
          <div class="box">
            <div class="box-header with-border" style="background: #242a30;">
              <h3 class="box-title" style="color:#fff; font-size:20px;font-weight: normal; font-size:14px">Pie Chart for Category wise Booking</h3>
            </div>
            <div class="box-body">
              <div id="placeholder" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->

          <!-- Donut chart -->
          
          <!-- /.box -->
        </div>
        <div class="col-md-6">
          <!-- Bar chart -->
          <div class="box">
            <div class="box-header with-border" style="background: #242a30;">
              <h3 class="box-title" style="color:#fff; font-size:20px;font-weight: normal; font-size:14px">Bar Chart for Category wise Booking</h3>
            </div>
            <div class="box-body">
              <div id="bar-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->

          <!-- Donut chart -->
          
          <!-- /.box -->
        </div>
        
        <!-- /.col -->
      </div>
 <div class="row">
 <div class="col-md-12">
          <!-- Line chart -->
          <div class="box">
            <div class="box-header with-border" style="background: #242a30;">
              <h3 class="box-title" style="color:#fff; font-size:20px;font-weight: normal; font-size:14px">Line Chart for Revenue Comparison</h3>
            </div>
            <div class="box-body" style="margin-bottom:40px">
              <div id="line-chart" style="height: 300px;"></div>
              
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->

        </div>
 	
 </div>      
<script type="text/javascript">
$(function () {
    // Create the chart
    $('#placeholder').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return (this.point.name)+" : "+Math.round(this.percentage*100)/100 + ' %';
					}
                },
                    showInLegend: true
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },
        series: [{
            name: 'Category',
            colorByPoint: true,
            data: <?php echo json_encode($data[0], JSON_NUMERIC_CHECK); ?>
        }]
    });
	
	
	// Create the chart
    $('#bar-chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total Bookings Category Wise'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: false
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },

        series: [{
            name: 'category',
            colorByPoint: true,
            data: <?php echo json_encode($data[0], JSON_NUMERIC_CHECK); ?>
        }]
    });
	
	$('#line-chart').highcharts({
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: <?php echo json_encode($dateArray); ?>
        },
        yAxis: {
            title: {
                text: 'Revenue(US Dollar)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{"name":"Selected Range","data":<?php echo json_encode($values, JSON_NUMERIC_CHECK); ?>}, {"name":"Previous Range","data":<?php echo json_encode($values1, JSON_NUMERIC_CHECK); ?>},]
    });
});
		</script>     