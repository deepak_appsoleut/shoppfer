<?php
$category_array1 = array();
foreach($data[2] as $a1){
	foreach($a1 as $a){
		$category_array1[] = $a;
	}
}

for($i=0; $i<count($data[0]); $i++)
{
	$value = 0;
	if($data[1][0]['totalAmount']!=0 && $category_array1[$i]['amount']!=NULL)
	{
		$value = ($category_array1[$i]['amount']/$data[1][0]['totalAmount'])*100;
	}
	if($value!=0)
	{
		$value = number_format($value, 2);
	}
 ?>
<div class="row">
<div class='col-md-5'>
<?php $imageExpolode = explode(',', $data[0][$i]['mp_gallery']); ?>
<img src='<?php echo APP_CRM_UPLOADS_PATH.$imageExpolode[0]; ?>' style="width:100%; padding:10px 0">
</div>
<div class='col-md-7'>
<h4 style='color: #123456;font-weight: bold; cursor:pointer' onclick='myClick("<?php echo $i; ?>");'><?php echo $data[0][$i]['mp_details_name'] ?></h4>
<h5>
<i class="fa fa-map-marker"></i>&nbsp; </span><?php echo $data[0][$i]['mp_details_address'] ?>,<?php echo $data[0][$i]['mp_details_city'] ?>,<?php echo $data[0][$i]['mp_details_state'] ?></h5>
<h5><i class="fa fa-road"></i>&nbsp; <strong>Distance</strong> : <?php echo number_format($data[0][$i]['mp_dist_km'], 2); ?> Km</h5>
<h5><strong>Category</strong> : <?php echo $data[0][$i]['category_name']; ?></h5>
<h5><strong>Speciality</strong> : <p><?php echo $data[0][$i]['sub_cat']; ?></p></h5>
<h5><strong>Rating</strong> : <?php echo $data[0][$i]['mp_details_rating']; ?></h5>
<h5><strong><i class="fa fa-user"></i> Manager : </strong><?php echo $data[0][$i]['mp_details_person'] ?>, <strong>Contact : </strong><?php echo $data[0][$i]['mp_details_contact']; ?></h5>
<p><strong>Description : </strong><?php echo $data[0][$i]['mp_details_description'] ?></p>
<?php 
if((isset($data[0][$i]['hotel_mp_relation_hotel_id'])) && ($data[0][$i]['hotel_mp_relation_hotel_id'] == $_SESSION['app_user']['hotel_details_id'])) { ?>
<button style="margin-top:20px;" type="button" class="btn btn-warning">Already Connected</button>
	<?php 
    if($data[0][$i]['hotel_mp_relation_recommend']==1)
    {
        ?>
        <button style="margin-top:20px;" type="button" disabled="disabled" class="btn btn-danger">Already Recommended</button>
        <?php 
    }
	else
	{
		?>
        <a style="margin-top:20px;" id="<?php echo $data[0][$i]['mp_details_id'] ?>" class="btn btn-danger redeemThis">Recommend This</a>
        <?php
	}
} else 
{ ?>
<a id="<?php echo $data[0][$i]['mp_details_id'] ?>" style="margin-top:20px;" class="btn btn-danger hotel_id" type="button">Connect</a>
<?php } 
	if($value>20)
	{
		?>
		<button style="margin-top:20px;" class="btn btn-primary">Healthy Business Relation</button>
		<?php 
	}
	else if($value>10&&$value<20)
	{
		?>
		<button style="margin-top:20px;" class="btn btn-info">Average Business Relation</button>
		<?php 
	}
	else
	{
		?>
		<button style="margin-top:20px;" class="btn btn-success">Poor Business Relation</button>
		<?php
	}
?>
</div>
</div>
<hr style="border-top: 1px solid #9A9A9A; margin:0; margin-top:10px; margin-bottom:10px" />
<?php } ?>
<!--==========Edit lead view Modal========-->
<script>
$('.hotel_id').click(function()
{
	var id = $(this).attr('id');
	$.ajax({
		url: '<?php echo APP_URL; ?>admin_hp/connect_mp/getConnect',
		type: 'POST',
		data : {"id" : id},
		success: function (data) {
			if(data==1)
			{
				$('#'+id).removeClass();
				$('#'+id).addClass('btn btn-warning');
				$('#'+id).text('Already Connected');
			}
		},
		error: function (data) {
		}
	});	
});
$('.redeemThis').click(function()
{
	var id = $(this).attr('id');
	$.ajax({
		url: '<?php echo APP_URL; ?>admin_mp/connect_mp/getRecommend',
		type: 'POST',
		data : {"id" : id},
		success: function (data) {
			if(data=="true")
			{
				$('#'+id).removeClass();
				$('#'+id).addClass('btn btn-danger');
				$('#'+id).text('Already Recommended');
				$('#'+id).attr('disabled', 'disabled');
			}
		},
		error: function (data) {
		}
	});	
});
</script>

