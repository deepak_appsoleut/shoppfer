<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Profile Edit</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
 <link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/bootstrap-multiselect.css">
  <!--<link rel="stylesheet" type="text/css" href="<?php //echo APP_CRM_BS; ?>css/jquery.timepicker.css" />-->
  <link rel="stylesheet" href="<?php echo APP_CSS; ?>toastr.css">
  <link rel="stylesheet" type="text/css" href="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <!--<link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/timepicker.css" />-->



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
.multiselect-container{max-height: 200px;overflow-y: auto;overflow-x: hidden;}
.btnCross {position: absolute;right: 15px;font-size: 12px;color: #fff;background: #454545;padding: 0px 5px;top: 0px;}
.form-group{ margin-bottom:45px !important;}
.site-min-height{ min-height:680px !important;}
.has-switch span.switch-left{background-color: #13D4A8;color: #fff;}
.has-switch span.switch-right{background-color: #FF6C5E;color: #fff;margin-top: 0px;}
.has-switch > div.switch-on label{background-color: #fff !important;}
.has-switch > div.switch-off label{background-color: #fff !important;}
.has-switch label{border: 7px solid #13D4A8;}
.has-switch{ width: 65px;height: 28px;}
.has-switch > div{ width:163%;}
.has-switch > div.switch-off label{border-color: #FF6C5E;}
.has-switch span{ font-weight:400;}
.input-sm {width: 90%;color: #676566;}
.wizard.vertical > .actions > ul > li {margin: 0em 6em 0 1em;}
.pull-right{font-weight: normal;}
</style>
</head>
<body class="hold-transition skin-white sidebar-mini">

<?php include(APP_VIEW.'includes/header.php');?>
<?php include(APP_VIEW.'includes/nav.php');?>
<section id="main-content">
	<section class="wrapper site-min-height">
      	<!-- page start-->
         	<div class="row">
				<div class="col-lg-12">
					<section class="panel">
                        <!--<header class="panel-heading">
                        	
                            
                        </header>-->
                        <div class="panel-body">
                        	<?php 
							  if($_SESSION['app_user']['mp_details_step_complete']==0)
							  {
							  ?>      
							  <div class="row">
								<div class="col-md-12">
								<div class="callout callout-danger">
										<h4>Profile Incomplete!</h4>
										<p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile">Click Here</a> to update your profile.</p>
									  </div>
								</div>
							  </div>
							  <?php }  ?>
							  <div class="row">
									<?php if($_SESSION['app_user']['mp_details_step_complete']==1)
									{ ?>
									<!--<div class="col-md-2">
										<a href="<?php echo APP_URL; ?>admin_mp/profile/profile_edit" class="btn btn-warning btn-block">Business Details</a>
									</div>
									<div class="col-md-2">
										<a href="<?php echo APP_URL; ?>admin_mp/profile/profile_gallery" class="btn btn-default btn-block">Gallery</a>
									</div>-->
									<?php } ?>
									<!--<div class="col-md-2">
										<a href="<?php echo APP_URL; ?>admin_mp/profile/change_password" class="btn btn-default btn-block">Change Password</a>
									</div>-->
							  </div>
							  <div class="row" style="">
								<div class="col-xs-12">
							   <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Success : </b> <span></span></div>
									<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Warning : </b> <span></span></div>
								<div class="box box-warning">
								<div class="box-header with-border">
								  <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>BUSINESS INFORMATION</strong></h3>
                                  <div style="height: 5px;width: 60px;margin-bottom: 30px;background: #5AD0B6;"></div>
								</div>
								<form id="form">
								<div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">Business Name</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="business" id="business" value="<?php echo $_SESSION['app_user']['mp_details_name']; ?>" placeholder="Business Name" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <!--<div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Country*</label>
                                        </div>
                                        <div class="col-md-9">
                                        <select name="country" id="country" class="form-control input-sm">
                                            <?php for($i=0; $i<count($data[0]); $i++) { 
                                                if($data[0][$i]['country_id']==$_SESSION['app_user']['mp_details_country'])
                                                {
                                                    ?>
                                                    <option selected value="<?php echo $data[0][$i]['country_id']; ?>"><?php echo $data[0][$i]['country_name']; ?></option>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <option value="<?php echo $data[0][$i]['country_id']; ?>"><?php echo $data[0][$i]['country_name']; ?></option>
                                                    <?php	
                                                }
                                            ?>
                                            
                                            <?php } ?>
                                        </select>
                                      </div>
                                    </div>
                                  </div>-->
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">Business Contact</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="business_contact" id="business_contact" value="<?php echo $_SESSION['app_user']['mp_details_contact']; ?>" placeholder="Business Contac" class="form-control input-sm" type="text" maxlength="13">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <!--<div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                      <div class="col-md-6">
                                        <label class="pull-left">State*</label>
                                        </div>
                                        <div class="col-md-6">
                                        <select name="state" id="state" class="form-control input-sm">
                                            <?php for($i=0; $i<count($data[4]); $i++) { 
                                                if($data[4][$i]['state_id']==$_SESSION['app_user']['mp_details_state'])
                                                {
                                                    ?>
                                                    <option data-state="<?php echo $data[4][$i]['state_name']; ?>" selected value="<?php echo $data[4][$i]['state_id']; ?>"><?php echo $data[4][$i]['state_name']; ?></option>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <option data-state="<?php echo $data[4][$i]['state_name']; ?>" value="<?php echo $data[4][$i]['state_id']; ?>"><?php echo $data[4][$i]['state_name']; ?></option>
                                                    <?php	
                                                }
                                            ?>
                                            
                                            <?php } ?>
                                        </select>
                                      </div>
                                    </div>
                                  </div>-->
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">City</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="city" id="city" value="<?php echo $_SESSION['app_user']['mp_details_city']; ?>" placeholder="City" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">Address</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="address" id="address" value="<?php echo $_SESSION['app_user']['mp_details_address']; ?>" placeholder="Address" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                  <!--<div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                      <div class="col-md-6">
                                        <label class="pull-left">Address2</label>
                                        </div>
                                        <div class="col-md-6">
                                        <input name="address1" id="address1" value="<?php echo $_SESSION['app_user']['mp_details_address1']; ?>" placeholder="Address2" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>-->
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">Web URL<span style="font-size: 10px;">(optional)</span></label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="web_url" id="web_url" value="<?php echo $_SESSION['app_user']['mp_details_website']; ?>" placeholder="WEb URL eg : http:www.shoppfer.com" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                  <!--<div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                      <div class="col-md-6">
                                        <label class="pull-left">Address2</label>
                                        </div>
                                        <div class="col-md-6">
                                        <input name="address1" id="address1" value="<?php echo $_SESSION['app_user']['mp_details_address1']; ?>" placeholder="Address2" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>-->
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                <!--<div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                      <div class="col-md-6">
                                        <label class="pull-left">Business Contact Number*</label>
                                        </div>
                                        <div class="col-md-6">
                                        <input name="business_contact" id="business_contact" value="<?php echo $_SESSION['app_user']['mp_details_contact']; ?>" placeholder="Business Contact Number" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">Contact Person's Name</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="person" id="person" value="<?php echo $_SESSION['app_user']['mp_details_person']; ?>" placeholder="person" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">Contact Person Number</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="contact" id="contact" value="<?php echo $_SESSION['app_user']['mp_details_phone']; ?>" placeholder="Contact Number" class="form-control input-sm" type="text" maxlength="13">
                                      </div>
                                    </div>
                                  </div>
								</div>
                                <div class="marTop10"></div>
                                <div class="row">
                                
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right">Designation</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="designation" value="<?php echo $_SESSION['app_user']['mp_details_designation']; ?>" id="designation" placeholder="person" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                  
                                </div>
										<div class="marTop10"></div>
										<div class="box-header with-border" style="MARGIN-TOP: 25PX;">
									 <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>BUSINESS DETAILS</strong></h3>
                                  <div style="height: 5px;width: 60px;margin-bottom: 30px;background: #5AD0B6;"></div>
									</div>
										<div class="marTop10"></div>
										<div class="row">
										  <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
												<label class="pull-right">Category</label>
												</div>
												<div class="col-md-9">
												<select name="category" id="category" class="form-control input-sm" disabled>
												<option value="">--Select Category--</option>
												<?php for($i=0; $i<count($data[1]); $i++) { 
														if($data[1][$i]['category_id']==$_SESSION['app_user']['mp_details_category'])
														{
															?>
															<option selected value="<?php echo $data[1][$i]['category_id']; ?>"><?php echo $data[1][$i]['category_name']; ?></option>
															<?php
														}
														else
														{
															?>
															<option value="<?php echo $data[1][$i]['category_id']; ?>"><?php echo $data[1][$i]['category_name']; ?></option>
															<?php	
														}
													?>
													
													<?php } ?>
												</select>
											  </div>
											</div>
										  </div>
										  <!--<div class="col-sm-6 col-md-6">
											<div class="form-group">
											  <div class="col-md-6">
												<label class="pull-left">Sub Category*</label>
												</div>
												<div class="col-md-6">
												<select name="sub_category[]" id="sub_category" multiple="multiple" class="form-control input-sm">
												 <?php $sub = explode(',', $_SESSION['app_user']['sub_cat']);
												 
													for($i=0; $i<count($data[2]); $i++)
													{
														if(in_array($data[2][$i]['sub_category_id'], $sub))
														{
														?>
															<option selected value="<?php echo $data[2][$i]['sub_category_id']; ?>"><?php echo $data[2][$i]['sub_category_name']; ?></option>
														<?php
														}
														else
														{
															?>
															<option value="<?php echo $data[2][$i]['sub_category_id']; ?>"><?php echo $data[2][$i]['sub_category_name']; ?></option>
															<?php
														}
													}
												 ?>
												</select>
											  </div>
											</div>
										  </div>-->
										</div>
										<div class="marTop10"></div>
										 <div class="row">
                                         <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
												<label class="pull-right">Recommended</label>
												</div>
												<div class="col-md-9">
												 <?php 
												$explode[0] = $explode[1] = $explode[2] = "";
												$explode = explode(',', $_SESSION['app_user']['mp_details_recommend']); 
												if(count($explode)==0)
												{
													$explode[0] = $explode[1] = $explode[2] = "";
												}
												else if(count($explode)==1)
												{
													$explode[1] = $explode[2] = "";
												}
												else if(count($explode)==2)
												{
													$explode[2] = "";
												}
												?>
												<input name="recommend[]" id="recommend1" value="<?php echo $explode[0]; ?>" placeholder="Recommended" class="form-control input-sm" type="text">
												<!--<input name="recommend[]" id="recommend2" value="<?php //echo $explode[1]; ?>" style="margin-top:5px" placeholder="Recommended" class="form-control input-sm" type="text">
												<input name="recommend[]" id="recommend3" value="<?php //echo $explode[2]; ?>" style="margin-top:5px" placeholder="Recommended" class="form-control input-sm" type="text">-->
											  </div>
											</div>
										  </div>
										  
										  
										</div>
										<div class="marTop10"></div>
										 <div class="row">
										  <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
												<label class="pull-right">Business Description</label>
												</div>
												<div class="col-md-9">
												<textarea rows="3" name="description" id="description" placeholder="Description" class="form-control input-sm" type="text"><?php echo $_SESSION['app_user']['mp_details_description']; ?></textarea>
											  </div>
											</div>
										  </div>
										</div>
                                        <input type="hidden" id="mp_details_id" value="<?php echo $_SESSION['app_user']['mp_details_id']; ?>">
										<div class="marTop10"></div>
                                        <div class="marTop10" style="margin-bottom: 20px;"></div>
										 <div class="row">
										  <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
                                             
												<label class="pull-right">Business Images<span style="font-size: 10px;">(optional)</span></label>
												</div>
												<div class="col-md-9">
												<?php 
												$cnt=1;
												
												
												for($i=0; $i<4; $i++){
												$cnt = $i+2; 
												$j = $i+1;
												
												?>
													<div class="col-md-3" style="width:auto;padding-left: 0px;">
														<div class="form-group">
															<button type="button" id="image<?php echo $j; ?>btn" class="btn btn-box-tool btnCross" data-widget="remove" style=" <?php if(isset($data[3][$i]['mp_gallery_image'])){ }else{ echo "display:none"; } ?> " value="image<?php echo $j; ?>">

                                                            <i class="fa fa-times"></i>
															</button>
															<label for="image<?php echo $j; ?>">
																<img id="image<?php echo $j; ?>pic" style="cursor:pointer;" height="100px" width="130px" src="<?php if(isset($data[3][$i]['mp_gallery_image'])){echo APP_CRM_UPLOADS_PATH.$data[3][$i]['mp_gallery_image']; }else{ echo APP_CRM_UPLOADS_PATH.'add_image.jpg'; } ?>"/>
															</label>
															<input name="image<?php echo $j; ?>" id="image<?php echo $j; ?>" style="display:none" class="form-control input-sm" type="file">
															
                                                            
                                                            
                                                            <!--<div style="position: absolute;top: 40px;left: 50px;color: red;display: none;font-weight: bold;" id="image<?php echo $j; ?>Lod"></div>-->
<div style="position: absolute;top: 79px;font-weight: bold;border: 1px solid #5AD0B6;width: 72%;height: 8px;display: none;margin-left: 13px;" id="image<?php echo $j; ?>Lod">
  <div style="width: 0%;background-color: #5AD0B6;height: 8px;" id="image<?php echo $j; ?>bar">
  </div>
  <div style="width: 60px;height: 23px;float: left;color: #fff;font-size: 11px;text-align: left;margin-top: -1px;" id="image<?php echo $j; ?>image_size">10.0 MB</div>
  <div style="width: 40px;height: 23px;float: right;margin-top: 1px;text-align: center;color: #fff;margin-top: -1px;font-size: 11px;" id="image<?php echo $j; ?>perimg"></div>
</div>
                                                            
                                                            
															<input type="hidden" class="imageUrl" name="image<?php echo $j; ?>Url" id="image<?php echo $j; ?>Url" value="<?php if(isset($data[3][$i]['mp_gallery_image'])){echo APP_CRM_UPLOADS_PATH.$data[3][$i]['mp_gallery_image']; }else{  } ?>" />
                                                            <input type="hidden" class="mp_gallery_id" name="mp_gallery_id_image<?php echo $j; ?>" id="mp_gallery_id_image<?php echo $j; ?>" value="<?php if(isset($data[3][$i]['mp_gallery_image'])){echo $data[3][$i]['mp_gallery_id']; }else{  } ?>" />
															<input type="hidden" name="image<?php echo $j; ?>Id" id="image<?php echo $j; ?>Id" value="" />
														</div>
													</div>
												<?php } 
												
												
												?>
                                                
											  </div>
											</div>
										  </div>
										</div>
                                        <div class="marTop10"></div>
                                        <div class="box-header with-border">
									 <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>BUSINESS OPERATIONS</strong></h3>
                                  <div style="height: 5px;width: 60px;margin-bottom: 30px;background: #5AD0B6;"></div>
									</div>
                                     <div class="marTop10"></div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                               <div class="form-group" style="margin-top:10px;">
                                              <div class="col-md-3">
                                                <label class="pull-right">Timings</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="col-md-3" style="margin-left: -13px;">
                                                   <div class="input-group bootstrap-timepicker">
                                                            <!--<input id="opentime" type="text" class="time form-control" name="opentime" value="" style="width: 110px;padding: 5px;" placeholder="Opening Time"/>-->
                                                    <input type="text" class="form-control timepicker-default" id="opentime" name="opentime">
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                                    </span>     
                                                   </div>
                                                    </div>
                                               
                                                     <div style="float: left;margin-right: 45px;margin-top: 9px;font-size: 12px;">
                                                    <!--<img src="<?php echo APP_IMAGES.'Open_close_time.jpg' ?>" style="height: 34px;"/>-->
                                                    To
                                                    </div>
                                                     <div class="col-md-3" style=" padding-left:0px;margin-left: -36px;">
                                                    <div class="input-group bootstrap-timepicker">
                                                           <!-- <input id="closetime" type="text" class="time form-control" name="closetime" value="" style="width: 110px;padding: 5px;" placeholder="Closing Time"/>-->
                                                    <input type="text" class="form-control timepicker-default" id="closetime" name="closetime">
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>       
                                                      </div>
                                                     </div>
                                                  </div>
                                                </div>
                                            </div>
                                            
                                              <div class="col-md-12" style="margin-top:10px;" id="a_c">
                                               <div class="form-group">
                                                  <div class="col-md-3">
                                                    <label class="pull-right">Accept Card</label>
                                                    </div>
                                                    <div class="col-md-9" style="padding-left: 10px;">
                                                        <div class="" style="">
                                                            <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="ac">
                                                            <input type="checkbox" data-toggle="switch" <?php if($_SESSION['app_user']['mp_accept_card']==1){echo 'checked';  }else{ } ?>/>
                                                            <input type="hidden" id="accept_card" name="accept_card" value="<?php echo $_SESSION['app_user']['mp_accept_card']; ?>" />
                                                            </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if($_SESSION['app_user']['mp_details_category']==2) { ?>
                                            <div class="col-md-12" style="margin-top:10px;" id="n_v">
                                               <div class="form-group">
                                                  <div class="col-md-3">
                                                    <label class="pull-right">Serves Non-veg</label>
                                                    </div>
                                                    <div class="col-md-9" style="padding-left: 10px;">
                                                        <div class="" style="">
                                                            <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="nv">
                                                            <input type="checkbox" data-toggle="switch" <?php if($_SESSION['app_user']['mp_nonveg']==1){echo 'checked';  }else{ } ?> />
                                                            <input type="hidden" id="veg_nonveg" name="veg_nonveg" value="<?php echo $_SESSION['app_user']['mp_nonveg'] ?>" />
                                                            </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                       						<div class="col-md-12" style="margin-top:10px;" id="b_a">
                                               <div class="form-group">
                                                    <div class="col-md-3">
                                                    <label class="pull-right">Bar Available</label>
                                                    </div>
                                                    <div class="col-md-9" style="padding-left: 10px;">
                                                        <div class="" style="">
                                                            <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="bar">
                                                            <input type="checkbox" data-toggle="switch" <?php if($_SESSION['app_user']['mp_bar_available']==1){echo 'checked';  }else{ } ?> />
                                                            <input type="hidden" id="bar_available" name="bar_available" value="<?php echo $_SESSION['app_user']['mp_bar_available'] ?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
									
                                    
                                    <input type="hidden" id="mp_details_id" value="<?php echo $_SESSION['app_user']['mp_details_id']; ?>">
									<div class="marTop10"></div>
									<div class="row"> 
										<div class="col-md-2" style="float: right;">
                                        	<div class="">
                                                <button class="btn btn-shadow btn-success pull-right" type="submit" style="margin-top: 25px;padding: 8px 40px; box-shadow:none !important;margin-right: 90px;" id="m_submit">Save</button>
                                            </div>
										</div>   
									</div>
                                    
									</form>
                                    
								</div>
								</div>
							  </div>
                         </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>


<?php include(APP_VIEW.'includes/footer.php');?>
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->

<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-multiselect.js"></script>
<!--<script type="text/javascript" src="<?php //echo APP_CRM_BS; ?>js/jquery.timepicker.js"></script>-->
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-switch.js"></script>
<script src="<?php echo APP_JS; ?>toastr.js"></script> 
<script type="text/javascript" src="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script>
$(function()
{
	$('.timepicker-default').timepicker();
	$("#category").change(function()
	{
		var id	=	$(this).val();
		if(id	==	2){
			$('#a_c').show();
			$('#n_v').show();
			$('#b_a').show();
		}else{
			$('#a_c').show();
			$('#n_v').hide();
			$('#b_a').hide();
		}
	});
	
	
	$('#opentime').timepicker();
	$('#closetime').timepicker();
	$("#opentime").val('<?php echo $_SESSION['app_user']['mp_opentime']; ?>');
	$("#closetime").val('<?php echo $_SESSION['app_user']['mp_closetime']; ?>');
	$("[data-toggle='switch']").wrap('<div class="switch"/>').parent().bootstrapSwitch();
	$("#ac").click(function(){
		var id	=	$("#accept_card").val();
		if(id	==	1)
		{
			$("#accept_card").val(0);
		}else{
			$("#accept_card").val(1);
		}
	});
	$("#nv").click(function(){
		var id	=	$("#veg_nonveg").val();
		if(id	==	1)
		{
			$("#veg_nonveg").val(0);
		}else{
			$("#veg_nonveg").val(1);
		}
	});
	$("#bar").click(function(){
		var id	=	$("#bar_available").val();
		if(id	==	1)
		{
			$("#bar_available").val(0);
		}else{
			$("#bar_available").val(1);
		}
	});
	$('.btnCross').click(function()
	{
		var id = $(this).attr('id');
		id = id.slice(0, -3);
		var mp_gallery_id_image	=	$("#mp_gallery_id_"+id).val();
		
		$(this).hide();
		
		$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/profile/deletePic',
				type: 'POST',
				data: {'mp_gallery_id_image' : mp_gallery_id_image},
				success: function (data) {
					$('#'+id+'pic').attr('src', "<?php echo APP_IMAGES; ?>add_image.jpg");
					$('#'+id).val('');
					$('#'+id+'Id').val('');
					$('#'+id+'Lod').hide();
					$('#'+id+'Url').val('');
					
				},
				error: function (data) {
				}
	   });
	});
	//---------------------------------------------------------
	//Start@To find out which fild is chnaged when profile Edit
	//---------------------------------------------------------1,2,10,11

	var old_data			=	$("#form").serialize();
	console.log(old_data);
	var old_data_values		=	old_data.split("&");
	console.log(old_data_values);
	var form_field_changed	=	[];
	var unique				=	[];
	var mp_details_id		=	$("#mp_details_id").val();
	
	//Fresh function To find out the update Value For businness deatils
	$("#business").change(function(){if(old_data_values[0]!= $(this).val()){form_field_changed.push("mp_details_name");}});
	//$("#country").change(function(){if(old_data_values[1]!= $(this).val()){form_field_changed.push("mp_details_country");}});
	//$("#state").change(function(){if(old_data_values[2]!= $(this).val()){form_field_changed.push("mp_details_state");}});
	$("#city").change(function(){if(old_data_values[3]!= $(this).val()){form_field_changed.push("mp_details_city");}});
	$("#address").change(function(){if(old_data_values[4]!= $(this).val()){form_field_changed.push("mp_details_address");}});
	//$("#address1").change(function(){if(old_data_values[5]!= $(this).val()){form_field_changed.push("mp_details_address1");}});
	//$("#business_contact").change(function(){if(old_data_values[6]!= $(this).val()){form_field_changed.push("mp_details_contact");}});
	$("#person").change(function(){if(old_data_values[7]!= $(this).val()){form_field_changed.push("mp_details_person");}});
	$("#contact").change(function(){if(old_data_values[8]!= $(this).val()){form_field_changed.push("mp_details_phone");}});
	$("#designation").change(function(){if(old_data_values[9]!= $(this).val()){form_field_changed.push("mp_details_designation");}});
	$("#category").change(function(){form_field_changed.push("mp_details_category");form_field_changed.push("mp_details_sub_cat_id");});
	//$("#sub_category").change(function(){form_field_changed.push("mp_details_sub_cat_id");});
	$("#description").change(function(){form_field_changed.push("mp_details_description");});
	$("#recommend1,#recommend2,#recommend3").change(function(){form_field_changed.push("mp_details_recommend");});
	//$("#bank_name").change(function(){form_field_changed.push("mp_details_bank_name");});
	//$("#acc_name").change(function(){form_field_changed.push("mp_details_account_name");});
	//$("#acc_number").change(function(){form_field_changed.push("mp_details_account_number");});
	//$("#ifsc").change(function(){form_field_changed.push("mp_details_bank_ifsc");});
	$("#business_contact").change(function(){form_field_changed.push("mp_details_contact");});

	$("#opentime").change(function(){form_field_changed.push("mp_opentime");});
	$("#web_url").change(function(){form_field_changed.push("mp_details_website");});
	$("#closetime").change(function(){form_field_changed.push("mp_closetime");});
	$("#ac").click(function(){form_field_changed.push("mp_accept_card");});
	$("#nv").click(function(){form_field_changed.push("mp_nonveg");});
	$("#bar").click(function(){form_field_changed.push("mp_bar_available");});
	
	
	//---------------------------------------------------------
	//END@To find out which fild is chnaged when profile Edit
	//---------------------------------------------------------
   $("#country").change(function()
	{
		var id = $(this).val();
		if(id==2)
		{
			$('#labelIfsc').html('Routing Number*');	
		}
		else
		{
			$('#labelIfsc').html('Bank IFSC Code*');	
		}
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/profile/getStateList',
			type: 'POST',
			data: {id : id},
			success: function (data) {
				$("#state").empty();
				$("#state").html(data);
			},
			error: function (data) {
			}
		 });
	});	
	
	$("#business_contact").keyup(function(){
		var v	=	$(this).val();
		if(!$.isNumeric(v))
		{
			alert('Please enter numric value in Businnes contact');
			$("#business_contact").focus();
			$("#business_contact").val('');
			return false;
		}
	});
	$("#contact").keyup(function(){
		var v	=	$(this).val();
		if(!$.isNumeric(v))
		{
			alert('Please enter numric value in Contact Person Number');
			$("#contact").focus();
			$("#contact").val('');
			return false;
		}
	});
					
	$('#sub_category').multiselect();
	$("#category").change(function()
	{
		var id = $(this).val();
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/profile/getSubcategoryList',
			type: 'POST',
			data: {id : id},
			success: function (data) {
				$("#sub_category").empty();
				$("#sub_category").html(data);
				$("#sub_category").attr('multiple', 'multiple'); 
				$("#sub_category").multiselect('rebuild');
			},
			error: function (data) {
			}
		 });
	});
	
	
$('#form').bootstrapValidator({
        fields: {
			business: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Business Name'
                    }
                }
            },
			city: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter City'
                    }
                }
            },
			address: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Address'
                    }
                }
            },
			/*web_url: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter website url eg: http://www.shoppfer.com'
                    }
                }
            },*/
			category: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Category'
                    }
                }
            },
			person: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Person Name'
                    }
                }
            },
			contact: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Contact Number'
                    }
                }
            },
			business_contact: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Business Contact Number'
                    }
                }
            },
			designation: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Designation'
                    }
                }
            },
			description: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Description'
                    }
                }
            },
			opentime: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Opening Time'
                    }
                }
            },
			opentime_ampm: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Opening Time'
                    }
                }
            },
			closetime: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Closeing Time'
                    }
                }
            },
			closetime_ampm: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Closeing Time'
                    }
                }
            },
			accpet_card: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Accept card'
                    }
                }
            },
			veg_nonveg: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Veg/nonVeg'
                    }
                }
            },
			bar_available: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Bar available'
                    }
                }
            }
			
        }
    })
	.on('success.form.bv', function (e) {
		$( "#m_submit" ).removeAttr("disabled");
        e.preventDefault();
		var count 			= ($('.imageUrl[value!=""]').length);
		var web_url		=	$("#web_url").val();
		var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		if(web_url != ""){
			if (!re.test(web_url)) { 
				alert("Please Enter Correct Website Url");
				$('#web_url').addClass("error");
				return false;
			}
		}
		var address		=	$("#address").val();
		var city		=	$("#city").val();
		var add			=	address+','+city;
		var formData = new FormData( this );
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "http://maps.googleapis.com/maps/api/geocode/json",
			data: {'address': add,'sensor':false},
			success: function(data){
				if(data.results.length){
					var lat	=	data.results[0].geometry.location.lat;
					var lng	=	data.results[0].geometry.location.lng;
						//var stateName = $('#state option:selected').data('state');
						
						formData.append('lat', lat);
						formData.append('lng', lng);
							$.ajax({
							url: '<?php echo APP_URL; ?>admin_mp/profile/profileEdit',
							type: 'POST',
							data: formData,
							processData: false,
							contentType: false,
							success: function (data) {
								console.log(data);
								if(data	==	'Loc_error')
								{
									$('.alert-success').hide();
									$('.alert-warning').show();
									$('.alert-warning span').html("There is some error while Updating your Address!");
									$("html, body").animate({ scrollTop: 0 }, 600);
								}
								else
								{
									if(data=="true")
									{
										
										$('.alert-warning').hide();
										//$('.alert-success').show();
										$("html, body").animate({ scrollTop: 0 }, 600);
										//$('.alert-success span').html("Profile updated successfully!");
										var shortCutFunction = 'success'; //success/info/warning/error
										var msg = 'Your Profile Updated Successfully!';
										var title = 'Awesome!';
										toastr.options = {
										  "closeButton": true,
										  "debug": false,
										  "progressBar": false,
										  "positionClass": "toast-bottom-right",
										  "onclick": null,
										  "showDuration": "300",
										  "hideDuration": "1000",
										  "timeOut": "5000",
										  "extendedTimeOut": "1000",
										  "showEasing": "swing",
										  "hideEasing": "linear",
										  "showMethod": "fadeIn",
										  "hideMethod": "fadeOut"
										};
										var $toast = toastr[shortCutFunction](msg, title); 
									}
									else
									{
										$('.alert-success').hide();
										$('.alert-warning').show();
										$('.alert-warning span').html("There is some error while Updating your profile!");
									}
								}
							},
							error: function (data) {
							}
						});
						//------------------------------------------------------------
						//START@ Ajax Called To save data in admin-notfication table
						//------------------------------------------------------------
						//alert(form_field_changed);
						unique_values = form_field_changed.filter(
									 function(a){if (!this[a]) {this[a] = 1; return a;}},
									 {});
						var new_final_values	=	(unique_values.toString());	
						if(new_final_values!="")
						{
							$.ajax({
								url: '<?php echo APP_URL; ?>admin_mp/profile/adminotify_profileEdit',
								type: 'POST',
								data: {change_field:new_final_values,"mp_details_id":mp_details_id,"flag":"profile"},
								success: function (data) {
									console.log(data);
									
								},
								error: function (data) {
								}
						   });
						   form_field_changed	=	Array();
						}
						else
						{
							return false;
						}
						
						//------------------------------------------------------------
						//END@ Ajax Called To save data in admin-notfication table
						//------------------------------------------------------------
					
					
				}else{
					alert('Please Enter vaild City or Address');
					$('#city').addClass("error");
					$('#city').focus();
					$('#address').addClass("error");
					$('#address').focus();
					return false;
			   }
			}
		});
		
		
		
		
		
		

		//------------------------------------------------------------
		//START@ Ajax Called To save data in admin-notfication table
		//------------------------------------------------------------
		//alert(form_field_changed);
		/*unique_values = form_field_changed.filter(
					 function(a){if (!this[a]) {this[a] = 1; return a;}},
					 {});
		var new_final_values	=	(unique_values.toString());	
		if(new_final_values!="")
		{
		$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/profile/adminotify_profileEdit',
				type: 'POST',
				data: {change_field:new_final_values,"mp_details_id":mp_details_id,"flag":"profile"},
				success: function (data) {
					console.log(data);
				},
				error: function (data) {
				}
		   });
		}
		else
		{
			return false;
		}*/
		//------------------------------------------------------------
		//END@ Ajax Called To save data in admin-notfication table
		//-----------------------------------------------------------
		});
});
   //code for image uploading starts here
   
		function readURL(input, id, fileName, mp_gallery_id) {
						if (input.files && input.files[0]) {
							var reader = new FileReader();
							reader.readAsDataURL(input.files[0]);
    						fileSize = Math.round(input.files[0].size);
							var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
							if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png")
							{
								if(fileSize>10485760)
								{
									alert("Please select image less than 10 MB!");
									$('#'+id).val('');
									return false;
								}
								else
								{
									reader.onload = function (e) {
									var image  = new Image();
									image.src = e.target.result;
									image.onload = function () {
										var height = this.height;
										var width = this.width;
										$('#'+id+'Pic').css('opacity', '0.5');
										$('#'+id+'Lod').show();
										//alert(e.target.result);
										//------
										$('#'+id+'pic').attr('src', e.target.result);
										$('#'+id+'pic').css("border","13px solid #D9DADF");
										//------
										var form1 = $('#form')[0];
										var formData = new FormData(form1);
										formData.append("image", id);
										$.ajax({
												xhr: function() {
												var xhr = new window.XMLHttpRequest();
												xhr.upload.addEventListener("progress", function(evt) {
												  if (evt.lengthComputable) {
													var percentComplete = evt.loaded / evt.total;
													percentComplete = parseInt(percentComplete * 100);
													//$('#'+id+'pic').attr('src', '');
													//$('#'+id+'Lod').html(percentComplete+'%');
													//alert(percentComplete);
													$('#'+id+'bar').css("width",percentComplete+'%');
													$('#'+id+'perimg').html(percentComplete+'%');
													var sizeInMB = (fileSize / (1024*1024)).toFixed(2);
													$('#'+id+'image_size').html(sizeInMB+' MB');
													console.log(percentComplete);
													$( "#m_submit" ).prop( "disabled", true );
													if (percentComplete === 100) {
														
													}
											
												  }
												}, false);
												return xhr;
												
											  },
												
												url: '<?php echo APP_URL; ?>admin_mp/createDeal/deal_image_show_profile_update',
												type: 'POST',
												data: formData,
												processData: false,
												contentType: false,
												success: function (data) {
													var obj = JSON.parse(data);
													$('#'+id+'Lod').hide();
													$('#'+id+'pic').css('opacity', '1.0');
													//$('#'+id+'pic').attr('src', '<?php echo APP_CRM_UPLOADS_PATH; ?>'+obj.imageName);
													$('#'+id+'Id').val(obj.id);
													$('#'+id+'btn').show();
													$('#'+id+'Url').val(obj.imageName);
													$('#mp_gallery_id_'+id).val(obj.gallery_id);
													$( "#m_submit" ).removeAttr("disabled");
													$('#'+id+'pic').css("border","none"); 
													
												},
												error: function (data) {
												}
									   });	
									}
								}
							}
						}
						else
						{
							alert("Please upload jpg, png or gif format");
							$('#'+id).val('');
							return false;
						}
					}
				}
				$("input:file").change(function(){
					var id 				= 	$(this).attr('id');
					var fileName 		= 	$('#'+id).val();
					var mp_gallery_id	=	$("#mp_gallery_id_"+id).val();
					readURL(this, id, fileName, mp_gallery_id);
				});	
</script>
</body>
</html>
