<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Marketing Partner Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
  <!-- jvectormap -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>
.form-control {border: none !important;}

  .state-overview .value{ float:left;}
  .p1{font-size: 14px;
    color: #6B6B6D;
    font-weight: 500;
    margin-top: 22px !important;}
  .p2{font-size: 27px;color: #5AD0B6 !important;font-weight: bold;text-align: left;}
  .state-overview .terques {background: #F3F3F3;}
  .state-overview .value {padding-top: 0px; }
  .panel {background-color:#F8F8F8;height: 100px;}

</style>
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $reviews = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <?php
	if($_SESSION['app_user']['mp_details_step']!=2)
	{
	?> 
    	<section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row" style="background-color: red;opacity: 0.6;color: #fff;margin-top: -15px;">
                  <div class="col-lg-12">
                      <section>
                        <h4>Profile Incomplete!</h4>
                        <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile" style="color: #000;font-weight: bold;">Click Here</a> to update your profile.</p>			
                      </section>
                  </div>
              </div>
           </section>
        </section>                
              
	<?php }else{?>
  <section id="main-content">
          <section class="wrapper">
			  <section class="">
                              <?php 		
							 // print_r($data[3]);					  
							  $total =  $data[0][0]['good'] +  $data[1][0]['average'] +  $data[2][0]['bad'];
							  $good = 0;
							  $bad = 0;
							  $average = 0;
							  if($total!=0)
							  {
								  $good = number_format(($data[0][0]['good']/$total)*100);
								  $average =  number_format(($data[1][0]['average']/$total)*100);
								  $bad =  number_format(($data[2][0]['bad']/$total)*100);
							  }
							  ?>
                 <header class="panel-heading">
                 <!--<strong>Reviews</strong>-->
                 <h3 class="box-title" style="    color: #000;margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>CUSTOMER FEEDBACK</strong></h3>
<div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                 </header> 
              <div class="panel-body">
                 <div class="row state-overview">
                      <div class="col-lg-4 col-sm-6">
                          <section class="panel" style="">
                              <div class="value">
                                  <p class="p1" style="text-align: left;padding-left: 30px;">Loved It!</p>
                                  <p class="p2" style="padding-left: 30px !important;"><?php echo $good; ?>%</p>
                              </div>
                              <div class="symbol terques">
                                  <img src="<?php echo APP_IMAGES.'smile.png' ?>">
                              </div>
                          </section>
                      </div>
                      <!--<div class="col-lg-1"></div>-->
                      <div class="col-lg-4 col-sm-6">
                          <section class="panel" style="">
                              <div class="value">
                                  <p class="p1">Something Missing!</p>
                                  <p class="p2" style="padding-left: 42px !important;"><?php echo $average; ?>%</p>
                              </div>
                              <div class="symbol terques">
                                  <img src="<?php echo APP_IMAGES.'smileA.png' ?>">
                              </div>
                          </section>
                      </div>
                     <!-- <div class="col-lg-1"></div>-->
                      <div class="col-lg-4 col-sm-6">
                          <section class="panel" style="">
                              <div class="value">
                                  <p class="p1" style="text-align: left;padding-left: 35px;">Didn’t Like It!</p>
                                  <p class="p2" style="padding-left: 36px !important;"><?PHP echo $bad; ?>%</p>
                              </div>
                              <div class="symbol terques">
                                  <img src="<?php echo APP_IMAGES.'sad.png' ?>">
                              </div>
                          </section>
                      </div>
                      <!--<div class="col-lg-1"></div>-->
                  </div>
              </div>
              </section>
              <section class="">
                 <header class="panel-heading">
                 <!--<strong>Reviews & Reply</strong>-->
                 <div class="pro-sort pull-right" style="margin-top:-12px;padding-right: 0px;">
                                  <select class="form-control" id="sort" style="display:inline; width:auto; height:30px; padding:2px; font-size:14px">
                                      <option value="default">All Reviews</option>
                                      <option value="good">Good Rating</option>
                                      <option value="avg">Average Rating</option>
                                      <option value="poor">Poor Rating</option>
                                  </select>
                              </div>
                 <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>Reviews from Your Users</strong></h3>
<div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                 
                              
                 </header>  
                      <?php 
					  if(count($data[3])	==	0){
					  ?>
                       <style>.site-min-height{ min-height:0px;}</style>
                       <div class="row site-min-height">
                              <div class="col-lg-12">
                                  <section class="panel">
                                <div style="margin: 0 auto;margin-top: 5%;">
                                    <div style="text-align: center;"><img src="<?php echo APP_IMAGES.'no_reviews.png' ?>" style="text-align: center;"></div>
                                    <div style="font-size: 24px;text-align: center;margin-top: 20px;margin-bottom: 20px;color: #4A4A4A;font-weight: 600;">Oops!</div>
                                    <div style="font-size: 16px;text-align: center;margin-top: 20px;margin-bottom: 20px;">Seems like your deal or outlet has not been reviewed as yet.</div>
                                </div>
                                </section>
                                </div>
                                </div>
                      <?php }else{ ?>        
                      <div class="panel-body" id="panelBody">
                                  <!-- /comment -->

                                  <!-- Comment -->
                                  <?php for($i=0; $i<count($data[3]); $i++) { 
								  	if($data[3][$i]['reply_text']=="") {
								  ?>
                                  <div class="msg-time-chat" style="padding-bottom: 5px;padding-top: 0px;">
                                      <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                          <div class="text" style="background: #F3F7FA; float:left; width:100%">
                                              <p class="attribution"><a href="#" style="color:#000 !important; font-weight:bold; font-size:16px;"><?php echo ucfirst($data[3][$i]['customer_name']); ?></a><span style="margin-left:20px">
                                              <?php 
											  $style="";
											  for($j=1; $j<6; $j++) { 
											  if($j<=$data[3][$i]['mp_review_score'])
											  	{
													$style = "color:#C00";
												}
												else
												{
													$style = "";
												}
												?>
                                                <i class="fa fa-star" style=" <?php echo $style; ?>"></i>
											  <?php } ?>
                                              </span>
                                              
                                              
                                              </p>
                                              
                                              <p style="margin-bottom:10px;"><?php echo $data[3][$i]['mp_review_comment'] ?></p>
                                              Posted @<?php echo date('F j Y', strtotime($data[3][$i]['mp_review_modified_on'])); ?>
                                              <span class="pull-right reply" id="rply<?php echo $data[3][$i]['mp_review_id'] ?>" style="cursor:pointer">
                                              <!--<i class="fa fa-reply-all"></i>-->
                                              <img src="<?php echo APP_IMAGES.'trun_small.png' ?>">
                                              </span>
                                              <form class="replyText" id="text<?php echo $data[3][$i]['mp_review_id'] ?>" style="display:none">											  <input type="hidden" value="<?php echo $data[3][$i]['customer_id'] ?>" id="cust<?php echo $data[3][$i]['mp_review_id'] ?>">	
                                              <textarea id="para<?php echo $data[3][$i]['mp_review_id'] ?>" class="form-control" style="border: 1px solid #5AD0B6 !important;"></textarea>
                                              <button type="button" id="bttn<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-primary replyBtn" style="margin-top:5px;font-size: 13px;padding: 7px;">Send Reply</button></form>
                                          </div>
                                      </div>
                                  	<div id="show1<?php echo $data[3][$i]['mp_review_id'] ?>"></div>
                                  </div>
                                  <?php } 
								   else 
								   {
									   ?>
                                       <div class="msg-time-chat">
                                         <div class="message-body msg-in">
                                              <span class="arrow"></span>
                                              <div class="text" style="background: #F3F7FA; float:left; width:100%;">
                                                  <p class="attribution"><a href="#" style="font-size: 16px;color:#000 !important;font-weight: bold;"><?php echo ucfirst($data[3][$i]['customer_name']); ?></a> 
                                                  <span style="">
												  <?php 
												  $style="";
												  for($j=1; $j<6; $j++) { 
                                                  if($j<=$data[3][$i]['mp_review_score'])
                                                    {
                                                        $style = "color:#C00";
                                                    }
                                                    else
                                                    {
                                                        $style = "";
                                                    }
                                                    ?>
                                                    <i class="fa fa-star" style=" <?php echo $style; ?>"></i>
                                                  <?php } ?>
                                                  </span>
                                                  <p style="margin-bottom:10px;"><?php echo $data[3][$i]['mp_review_comment'] ?></p>
                                                  Posted @<?php echo date('F j Y', strtotime($data[3][$i]['mp_review_modified_on'])); ?> 
                                              </div>
                                          </div>
                                          
                                          <div class="message-body msg-out col-md-11 pull-right" style="padding-right: 0px;">
                                              <div class="arrow" style="float: left;"><img src="<?php echo APP_IMAGES.'trun.png' ?>" style="height: 20px;
    margin-top: 12px;margin-right: 4px;margin-left: 12px;"></div>
                                              <div class="text" style="background: #F3F7FA;  float:left; width:96%;    border: none; margin-top:8px;">
                                              <div class="showText" id="show<?php echo $data[3][$i]['reply_mp_review_id']; ?>">
                                                  <p class="attribution"><a href="#" style="font-size: 13px;font-weight:bold; color:#5AD0B6 !important;"><?php echo ucfirst($data[3][$i]['mp_details_name']); ?></a>@ <?php echo date('g:i a, F j Y', strtotime($data[3][$i]['reply_modify_date'])); ?>	<span class="pull-right"><i class="fa fa-pencil-square-o editIcon" data-id="<?php echo $data[3][$i]['reply_mp_review_id']; ?>" style="font-size:20px; cursor:pointer" aria-hidden="true"></i></span>
                                                  </p>
                                                  <p><?php echo $data[3][$i]['reply_text'] ?></p>
                                                  </div>
                                               <div style="display:none" id="form<?php echo $data[3][$i]['reply_mp_review_id']; ?>">
                                               		<form class="replyTextEdit" id="text<?php echo $data[3][$i]['mp_review_id'] ?>">											  <input type="hidden" value="<?php echo $data[3][$i]['customer_id'] ?>" id="cust<?php echo $data[3][$i]['mp_review_id'] ?>">	
                                              <textarea id="par1<?php echo $data[3][$i]['mp_review_id'] ?>" class="form-control" style="border: 1px solid #5AD0B6 !important;"><?php echo $data[3][$i]['reply_text'] ?></textarea>
                                             
                                              <button type="button" data-id="<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-danger cancelEdit" style="margin-top:5px;font-size: 13px;padding: 7px;">Cancel</button>
                                              
                                               <button type="button" id="bttn<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-primary replyEdit" style="margin-top:5px; margin-right:10px;font-size: 13px;padding: 7px;">Edit Reply</button>
                                               </form>
                                                </div>  
                                              </div>
                                          </div>
                                      </div>
                                       <?php
								   }
								  } ?>
                                  <!-- /comment -->
                                  <!-- Comment -->
                                  
                                  <!-- /comment -->
                                  <div class="pull-right">
                                  <?php 
								   $totalrecords = $data[4]['count'];
								   $pg = new pagination(); 
								   $pg->pagenumber = 1;
									$pg->pagesize =10;
									$pg->totalrecords = $totalrecords;
									$pg->showfirst = true;
									$pg->showlast = true;
									$pg->paginationcss = "pagination-normal";
									$pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
									$pg->defaultUrl = "#";
									$pg->paginationUrl = "#";
									echo $pg->process();
									 ?>          
                                     </div>
                          </div>
                      <?php } ?>
              </section>
          </section>
      </section>
       <?php } ?>
  <!-- /.content-wrapper -->
  
</section>
</body>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script>
  $(function () {	  
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });	
  });
  
	
	$(document).on('click','.reply',function(){
		var str = $(this).attr('id');
		var removedStr = str.substr(4);
		if ($("#text"+removedStr).css('display') === 'none') {
			$("#text"+removedStr).show();
		}
		else
		{
			$("#text"+removedStr).hide();
		}
	});
	
	$(document).on('click','.editIcon',function(){
		var str1 = $(this).data('id');
		$('#show'+str1).hide();
		$('#form'+str1).show();
	});
	
	$(document).on('click','.cancelEdit',function(){
		var str1 = $(this).data('id');
		$('#show'+str1).show();
		$('#form'+str1).hide();
	});
	
	$(document).on('click','.replyBtn',function(){
		var str = $(this).attr('id');
		var removedStr = str.substr(4);	
		var txt = $('#para'+removedStr).val();
		var customer_id = $('#cust'+removedStr).val();
		if($.trim(txt)=="")
		{
			return false;
		}
		else
		{
			$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/review_mp/replySubmit',
            data: {'txt' : txt,'customer_id' : customer_id, 'removedStr' : removedStr},
            type: 'POST',
            success: function (data) {
                    //var data1 = $.parseJSON(data);
			$('#rply'+removedStr).hide();
			$('#text'+removedStr).hide();
			$('#show1'+removedStr).html(data);
                  },
                  error: function (data) {
                  }
            });
          return false;
		}
	});


$(document).on('click','.replyEdit',function(){
		var str = $(this).attr('id');
		var removedStr = str.substr(4);	
		var txt = $('#par1'+removedStr).val();
		if($.trim(txt)=="")
		{
			return false;
		}
		else
		{
			$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/review_mp/replyEditSubmit',
            data: {'txt' : txt, 'removedStr' : removedStr},
            type: 'POST',
            success: function (data) {
				console.log(data);
					$('#show'+removedStr).show();
					$('#show'+removedStr).html(data);
					$('#form'+removedStr).hide();
                  },
                  error: function (data) {
                  }
            });
          return false;
		}
	});

	
	$(document).on('click','.pagination-css',function(){
          var page_number = $(this).attr('id');
		  var page_number = page_number.substr(3);
		  var sortValue = $('#sort').val();
          $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/review_mp/pageChange',
            data: {'page_number' : page_number,'sortValue' : sortValue},
            type: 'POST',
            success: function (data) {
                    //var data1 = $.parseJSON(data);
                    $('#panelBody').html('');
                    $('#panelBody').html(data);
                  },
                  error: function (data) {
                  }
            });
          return false;
    });
	
	$(document).on('change','#sort',function(){
		  var sortValue = $('#sort').val();
          $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/review_mp/pageChangeonSort',
            data: {'sortValue' : sortValue},
            type: 'POST',
            success: function (data) {
                    //var data1 = $.parseJSON(data);
                    $('#panelBody').html('');
                    $('#panelBody').html(data);
                  },
                  error: function (data) {
                  }
            });
          return false;
    });
</script>
<!-- AdminLTE App -->

<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</html>
