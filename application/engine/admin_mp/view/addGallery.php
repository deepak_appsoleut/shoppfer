<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">Add Gallery</h4>
    </div>
    <div class="modal-body">
      <div class="error"></div>
      <form id="form" method="post">
      <div class="form-group">
      	  <div class="row">  			
	                  <div class="col-sm-12 col-md-12">
	                      <div class="col-md-4">
	                    <div class="form-group">
	                      <input name="title" value="" class="form-control input-sm" type="text">
                          </div>
                          </div>
                          <div class="col-md-4">
                          <div class="form-group">
	                      <input name="file" class="form-control input-sm" type="file">
	                      </div>
                          </div>
                          <div class="col-md-4">
                           <div class="form-group">
	                      <select name="status" class="form-control input-sm">
                                <option  value="1">Active</option>
                                <option value="0">Inactive</option>
                              </select>
	                      </div>
	                    </div>
	                  </div>
	       </div>	
       </div>
       <div class="row">
       <div class="col-md-5"></div>
       <div class="col-md-6">      
       <button class="btn-warning btn custom_warning_btn" type="submit">Add Gallery</button>
       </div>
       </div>
      </form>
    </div>
    <script>
$('#form').bootstrapValidator({
        fields: {
			title: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Gallery Title'
                    }
                }
            },
			file: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Gallery Image'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/profile/galleryAdd',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				console.log(data);
				if(data==1)
				{
					$('.error').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Gallery Added successfully!</span></div>');
				}
				else
				{
					$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span></span></div>');
				}
            },
            error: function (data) {
            }
   });
});
</script>