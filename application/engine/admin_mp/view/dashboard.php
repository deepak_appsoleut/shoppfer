<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Marketing Partner Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 <link rel="stylesheet" href="https://silviomoreto.github.io/bootstrap-select/dist/css/bootstrap-select.min.css">
 
  <!-- jvectormap -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  .state-overview .value{ float:left;}
  .p1{font-size: 14px;
    color: #6B6B6D;
    font-weight: 500;
    margin-top: 18px !important;}
  .p2{font-size: 40px;color: #5AD0B6 !important;font-weight: bold;text-align: left;}
  .state-overview .terques {background: #F3F3F3;}
  .state-overview .value {padding-top: 0px; }
  .panel {background-color:#F8F8F8;height: 100px;}
  .bootstrap-select>.dropdown-toggle{width: 14%;float: right;}
  .bootstrap-select.btn-group .dropdown-menu{min-width: 11%;float: right;margin-right: -150px;position: relative;}
  /*.dropdown-menu{ position:relative;}*/
  .btn-default {color: #000;background-color: #fff;border-color: #fff;}
  .btn-default:hover{color: #000;background-color: #fff;border-color: #fff;}
  .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {background-color: #5AD0B6;color: #fff;text-decoration: none;}
  .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open .dropdown-toggle.btn-default {background-color: #fff;color: #000;text-decoration: none;}
  </style>
  <!--<script src="http://maps.googleapis.com/maps/api/js"></script>-->
 <!-- <script src="<?php //echo APP_CRM_BS.'js/googlemap.js' ?>"></script>-->
  
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $dashboard = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <?php
	if($_SESSION['app_user']['mp_details_step']!=2)
	{
	?> 
    	<section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row" style="background-color: red;opacity: 0.6;color: #fff;margin-top: -15px;">
                  <div class="col-lg-12">
                      <section>
                        <h4>Profile Incomplete!</h4>
                        <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile" style="color: #000;font-weight: bold;">Click Here</a> to update your profile.</p>			
                      </section>
                  </div>
              </div>
           </section>
        </section>                
              
	<?php }else{?>
 
  <section id="main-content">
          <section class="wrapper" style="width: 98%;">
               	<div style="float: right;position: absolute;z-index: 9;width: 28px;margin-left: 67%;margin-top: 8px;text-align: right;">
                	<i class="fa fa-circle-o" aria-hidden="true" style="color:#5AD0B6;"></i>
                </div>
                <div>
                	<select class="form-control selectpicker" id="sort" dir="rtl">
                    <option value="<?php echo date('Y-m-d', strtotime('-6 days')).'#'.date("Y-m-d") ?>" selected="selected">Last 7 Days</option>
                    <option value="<?php echo date("Y-m-d"); ?>">Today</option>
                    <option value="<?php echo date('Y-m-d', strtotime('-1 days')); ?>">Yesterday</option>
                    <option value="<?php echo date('Y-m-d', strtotime('-29 days')).'#'.date("Y-m-d") ?>">Last 30 Days</option>
                    <option value="<?php echo date("Y-m-".'01').'#'.date('Y-m-d') ?>">This Month</option>
                    <option value="<?php echo date('Y-m-d',strtotime('first day of last month')).'#'.date('Y-m-d',strtotime('last day of last month')) ?>">Previous Month</option>
                </select>
                </div>
  				</div>
              <div id="data1"></div>
          </section>
      </section>
  
  <?php } ?>
  
  <!-- /.content-wrapper -->
  
</section>
</body>
<div class="modal fade" id="getCodeModal_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="top: 110px;width: 370px;float: right;margin-right: 200px;height: 250px;">
        <div class="modal-body" id="getCode_popup">
        	<div>
            	<img src="<?php echo APP_IMAGES.'select.png' ?>" style="position: absolute;margin-top: -75px;width: 110px;left: 133px;"/>
            </div>
            <div style="font-size: 23px;margin-top: 40px;text-align: center;color: #676566;margin-bottom: 15px;;">Hello & Welcome To Shoppfer!</div>
            <div style="font-size: 14px;margin-left: 29px;color: #676566;">Your Profile has been Created Successfully!</div>
            <div>
            <button class="btn btn-success btn-xs" style="padding: 15px 70px;margin-top: 40px;font-size: 21px;    margin-left: 15px;" data-dismiss="modal" id="getnow">Let's Get Started!</button>
            </div>
        </div>
        </div>
    </div>
</div>

<?php include(APP_VIEW.'includes/bottom.php');?>
<script src="https://silviomoreto.github.io/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
function initialize()
{
	var mapProp = {
	center:myCenter,
	zoom:14,
	mapTypeId:google.maps.MapTypeId.ROADMAP,
	};
	var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	var marker	=	[];
	marker=new google.maps.Marker({
 		position:myCenter,
  	});
	
	
	var circle = new google.maps.Circle({
	  map: map,
	  radius: 400,    // metres
	  fillColor: '#B1C3E9',
	  strokeWeight:"1",
	  strokeColor:"#6B94E8"
	});
	
	circle.bindTo('center', marker, 'position');
	marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
var myCenter=new google.maps.LatLng(28.413164,77.042998);


</script>

<script>
function showData1(startDate, endDate, flag)
{
	if(flag	==	'single'){
		edate	=	'lastdate';
	}else{
		edate	=	endDate;
	}
	$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/analytics_mp/analytics_data_dashboard_mp',
            type: 'POST',
			data: {"startDate" : startDate, "endDate" : edate},
            success: function (data) {
				//alert(data);
				$('#data1').hide().html(data).fadeIn(1000);
				initialize();
				
            },
            error: function (data) {
            }
   	});
}

$(function () {
	<?php
	if($_SESSION['app_user']['mp_popup']	==	0){
	?>
	jQuery("#getCodeModal_popup").modal('show');
	<?php } ?>
	$("#getnow").click(function(){
		jQuery("#getCodeModal_popup").modal('hide');
		$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/analytics_mp/popup_window',
				type: 'POST',
				data: {"mp_popup" : 1},
				success: function (data) {
				},
				error: function (data) {
				}
		});
	});
	// set current month startdate and enddate
	  /*var d = new Date();	  
	  var firstDay = new Date(d.getFullYear(), d.getMonth(), 1);	  
	  var month = firstDay.getMonth()+1;
	  var day = firstDay.getDate();
	  var startDate = firstDay.getFullYear() +'/' + ((''+month).length<2 ? '0' : '') + month + '/' + ((''+day).length<2 ? '0' : '') + day;
	  
	  var lastDay = new Date(d.getFullYear(), d.getMonth() + 1, 0);
	  var month1 = lastDay.getMonth()+1;
	  var day1 = d.getDate();
	  var endDate = lastDay.getFullYear() +'/' + ((''+month1).length<2 ? '0' : '') + month1 + '/' + ((''+day1).length<2 ? '0' : '') + day1;*/
	  //showData1(startDate, endDate);
	  // function to execute dashboard data in 10 seconds
	  //setInterval(function(){showData1(startDate, endDate);}, 100000000000000000000000000000000000000000000000000000000000000);
	  // showData1(startDate, endDate);
	  var dat	=	'<?php echo date('Y-m-d', strtotime('-6 days')).'#'.date("Y-m-d") ?>';
	  nn = dat.split("#");
	  showData1(nn[0], nn[1] , 'double');
	  
	  $(document).on('change', '#sort', function(){
	  	var sort_date	=	$(this).val();
		//alert(sort_date);
		n = sort_date.indexOf("#");
		if(n==-1)//not found
		{
			var s_date	=	sort_date;
			var e_date	=	'lastdate';
			showData1(s_date, e_date , 'single');
		}else{//found
			var all_date	=	sort_date.split('#')
			var s_date		=	all_date[0];
			var e_date		=	all_date[1];
			showData1(s_date, e_date , 'double');
		}
		
	  });
  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</html>
