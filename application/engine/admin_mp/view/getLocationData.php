<?php 
//echo $data[0];
$category_array1 = array();
foreach($data[2] as $a1){
	foreach($a1 as $a){
		$category_array1[] = $a;
	}
}
for($i=0; $i<count($data[0]); $i++)
{
	$value = 0;
	if($data[1][0]['totalAmount']!=0 && $category_array1[$i]['amount']!=NULL)
	{
		$value = ($category_array1[$i]['amount']/$data[1][0]['totalAmount'])*100;
	}
	if($value!=0)
	{
		$value = number_format($value, 2);
	}
 ?>
<div class="row">
<div class='col-md-5'>
<img src='<?php echo APP_IMAGES; ?>the-leela1.jpg' style="width:100%; padding:10px 0">
</div>
<div class='col-md-7'>
<h4 style='color: #123456;font-weight: bold; cursor:pointer' onclick='myClick("<?php echo $i; ?>");'><?php echo $data[0][$i]['hotel_details_name'] ?></h4>
<h5>
<i class="fa fa-map-marker"></i>&nbsp; </span><?php echo $data[0][$i]['hotel_details_address'] ?>,<?php echo $data[0][$i]['hotel_details_city'] ?>,<?php echo $data[0][$i]['hotel_details_state'] ?></h5>
<h5><i class="fa fa-road"></i>&nbsp; <strong>Distance</strong> : <?php echo number_format($data[0][$i]['mp_dist_km'], 2); ?> Km</h5>
<h5><strong><i class="fa fa-user"></i> Manager : </strong><?php echo $data[0][$i]['hotel_details_person'] ?>, <strong>Contact : </strong><?php echo $data[0][$i]['hotel_details_contact']; ?></h5>
<p><strong>Description : </strong><?php echo $data[0][$i]['hotel_details_description'] ?></p>
<?php 
if(isset($data[0][$i]['gotData']))
{
$explode = explode(',', $data[0][$i]['gotData']);
if(in_array($_SESSION['app_user']['mp_details_id'], $explode)) { ?>
<button style="margin-top:20px;" type="button" class="btn btn-warning">Already Connected</button>
<?php } 
else
{
	?>
    <a data-toggle="modal" data-target="#editGallery" id="<?php echo $data[0][$i]['hotel_details_id'] ?>" style="margin-top:20px;" class="btn btn-danger hotel_id" type="button">Send Request</a>
    <?php
}
}
else { ?>
<a id="<?php echo $data[0][$i]['hotel_details_id'] ?>" style="margin-top:20px;" class="btn btn-danger hotel_id" type="button">Allready Requested</a>
<?php }
if($value>20)
	{
		?>
		<button style="margin-top:20px;" class="btn btn-primary">Healthy Business Relation</button>
		<?php 
	}
	else if($value>10&&$value<20)
	{
		?>
		<button style="margin-top:20px;" class="btn btn-info">Average Business Relation</button>
		<?php 
	}
	else
	{
		?>
		<button style="margin-top:20px;" class="btn btn-success">Poor Business Relation</button>
		<?php
	}
 ?>
</div>
</div>
<hr style="border-top: 1px solid #9A9A9A; margin:0;" />
<?php } ?>
<!--==========Edit lead view Modal========-->
<div class="modal fade bs-modal-sm" id="editGallery">
 <div class="modal-dialog modal-sm">
      <div class="modal-content admin_custom_modal">
			<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">Send Request</h4>
    </div>
    <div class="modal-body">
      <div id="message"></div>
      
      <form id="form" method="post">
      <div class="form-group">
      	  <div class="row">  			
	                  <div class="col-sm-12 col-md-12">
	                    <div class="form-group">
	                      <div class="col-md-12">
                          <input type="hidden" name="hotel_id" id="hiddenValue" />
	                      <textarea name="text" rows="8" class="form-control input-sm" type="text">Hi, I'd like to add you as a partner.</textarea>
                          </div>
	                    </div>
	                  </div>
	       </div>	
       </div>
       <div class="row">
       <div class="col-md-5"></div>
       <div class="col-md-6">      
       <button class="btn-warning btn custom_warning_btn" type="submit">Send Request</button>
       </div>
       </div>
      </form>
    </div>
     </div>
  </div>
</div>   
<script>

$('.hotel_id').click(function()
{
	var id = $(this).attr('id');
	$('#hiddenValue').val(id);
});

$('.modal').on('hidden.bs.modal', function () {
 $(this).removeData('bs.modal');
 $('#message').html('');
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
});


$('#form').bootstrapValidator({
        fields: {
			text: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Text'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
		    $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/connected_hotels/SendRequest',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data==1)
				{
					$('#message').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Request Sent Successfully</span></div>');
				}
				else
				{
					$('#message').html('<div class="alert alert-warning alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Warning : </b> <span>You have aleardy sent request to this Hotel!</span></div>');
				}
            },
            error: function (data) {
            }
   });
});	
</script>

