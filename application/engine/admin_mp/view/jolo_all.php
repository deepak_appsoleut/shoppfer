<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Shoppfer | jolo all</title>
<?php include(APP_VIEW.'includes/top.php');?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
<link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
	border-top:none;
}
.table>thead>tr>th {
	border-bottom: 0px solid #ddd;
}
table.table thead .sorting {
	background:none !important;
}
table.table thead .sorting_asc {
	background:none !important;
}
</style>
</head>
<body>
<section id="container" >
  <?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $deal = "active";
  include(APP_VIEW.'includes/nav.php');?>
  
  <!-- Content Wrapper. Contains page content -->
  <?php
	if($_SESSION['app_user']['mp_details_step']!=2)
	{
	?>
  <section id="main-content">
    <section class="wrapper site-min-height"> 
      <!-- page start-->
      <div class="row" style="background-color: red;opacity: 0.6;color: #fff;margin-top: -15px;">
        <div class="col-lg-12">
          <section>
            <h4>Profile Incomplete!</h4>
            <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile" style="color: #000;font-weight: bold;">Click Here</a> to update your profile.</p>
          </section>
        </div>
      </div>
    </section>
  </section>
  <?php }else{?>
  <section id="main-content">
    <section class="wrapper site-min-height"> 
      <!-- page start-->
      <div class="row">
        <div class="col-lg-12">
          <?php
					$disabled = "";
                    	if(count($data)	==	0){
                    ?>
          <style>
.site-min-height{ min-height:0px;}
</style>
          <div style="margin: 0 auto;margin-top: 7%;">
            <div style="text-align: center;"><img src="<?php echo APP_IMAGES.'icon.png' ?>" style="text-align: center;"></div>
            <div style="font-size: 30px;text-align: center;margin-top: 20px;margin-bottom: 20px;color: #4A4A4A; font-weight:600;">No Deal!</div>
            <div style="font-size: 14px;text-align: center;margin-top: 20px;margin-bottom: 20px;">You haven't created any deals! Onces will you create a deal it will be shown here!</div>
            
          </div>
          <?php }else{ ?>
          <section class="panel">
            <div id="message"></div>
            <header class="panel-heading"> <span class="pull-right" style="margin-top: -10px;">
               </span>
              <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;width: 100%;"> <strong>MANAGE PLANS</strong> </h3>
              <div style="height: 5px;width: 60px;background: #5AD0B6;margin-bottom: 30px;"></div>
            </header>
            <div class="panel-body">
              <div class="adv-table"> 
                <!--table-bordered-->
                <table id="example" class="table table-striped" style="border: 1px solid #F1F2F7;">
                  <thead style="border-bottom: 1px solid #F1F2F7;">
                    <tr>
                      <th style="text-align: center;font-weight: normal;">ID</th>
                      <th style="text-align: left;font-weight: normal;">Circle</th>
                      <th style="text-align: center;font-weight: normal;">Operator</th>
                      <th style="text-align: center;font-weight: normal;">Operator Type</th>
                      <th style="text-align: center;font-weight: normal;">Category</th>
                      <th style="text-align: center;font-weight: normal;">Price</th>
                      <th style="text-align: center;font-weight: normal;">Validity</th>
                      <th style="text-align: center;font-weight: normal;">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
									
                                    for($i=0; $i<count($data); $i++) {
                                    ?>
                    <tr role="row">
                      <td style="text-align: center;"><?php echo $data[$i]["id"]; ?></td>
                      <td style="text-align: left;"><?php echo $data[$i]["circle"]; ?></td>
                      <td style="text-align: center;"><?php echo $data[$i]["operator"]; ?></td>
                      <td style="text-align: center;"><?php echo $data[$i]["operator_type"]; ?></td>
                      <td style="text-align: center;"><?php echo $data[$i]["category"]; ?></td>
                      <td style="text-align: center;"><?php echo $data[$i]["price"]; ?></td>
                      <td style="text-align: center;"><?php echo $data[$i]["validity"]; ?></td>
                      <td style="text-align: center;"><?php echo $data[$i]["description"]; ?></td>
                    </tr>
                    <?php 
					}
				?>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
          <?php } ?>
        </div>
      </div>
      <!-- page end--> 
    </section>
  </section>
  <?php } ?>
</section>
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables --> 
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script> 
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script> 
<script>
  $(function () {
    $('#example').DataTable(
	 {
  "columnDefs": [
    { "searchable": false, "targets": 0 }
  ]
} 
	);
  });
</script>
</body>
</html>
