<div class="row">
<?php //print_r($data); 
for($i=0; $i<count($data); $i++)
{
 ?>
<div class="col-md-6" style="margin-top:10px;">
<div class="row" style="background:#e8e8e8; margin:0; padding-bottom:10px; padding-top:5px; border: 1px solid #ccc;">
<div class='col-md-5'>
<img src='<?php echo APP_IMAGES; ?>the-leela1.jpg' style="width:100%; padding:10px 0">
</div>
<div class='col-md-7'>
<h4 style='color: #123456;font-weight: bold; cursor:pointer' onclick='myClick("<?php echo $i; ?>");'><?php echo $data[$i]['hotel_details_name'] ?></h4>
<h5>
<i class="fa fa-map-marker"></i>&nbsp; </span><?php echo $data[$i]['hotel_details_address'] ?>,<?php echo $data[$i]['hotel_details_city'] ?>,<?php echo $data[$i]['hotel_details_state'] ?></h5>
<h5><i class="fa fa-road"></i>&nbsp; <strong>Distance</strong> : <?php echo number_format($data[$i]['mp_dist_km'], 2); ?> Km</h5>
<p style="text-align:justify"><?php echo $data[$i]['hotel_details_description'] ?></p>
<?php if($data[$i]['hotel_mp_relation_mp_id'] == $_SESSION['app_user']['mp_details_id']) { ?>
<button style="margin-top:0px;" type="button" class="btn btn-warning">Already Connected</button>
<?php } else { ?>
<button style="margin-top:0px;" class="btn btn-danger" type="button">Send Request</button>
<?php } ?>
</div>
</div>
</div>
<?php } ?>
</div>