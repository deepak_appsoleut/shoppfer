<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Profile Edit</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
 <link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/bootstrap-multiselect.css">
  <link rel="stylesheet" type="text/css" href="<?php echo APP_CRM_BS; ?>css/jquery.timepicker.css" />
  

  <!--<link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/timepicker.css" />-->



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
.multiselect-container{max-height: 200px;overflow-y: auto;overflow-x: hidden;}
.btnCross {position: absolute;right: 15px;font-size: 12px;color: #fff;background: #454545;padding: 0px 5px;top: 0px;}
.form-group{ margin-bottom:45px !important;}
.site-min-height{ min-height:300px !important;}
.has-switch span.switch-left{background-color: #13D4A8;color: #fff;}
.has-switch span.switch-right{background-color: #FF6C5E;color: #fff;margin-top: 0px;}
.has-switch > div.switch-on label{background-color: #fff !important;}
.has-switch > div.switch-off label{background-color: #fff !important;}
.has-switch label{border: 7px solid #13D4A8;}
.has-switch{ width: 65px;height: 28px;}
.has-switch > div{ width:163%;}
.has-switch > div.switch-off label{border-color: #FF6C5E;}
.has-switch span{ font-weight:400;}
</style>
</head>
<body class="hold-transition skin-white sidebar-mini">

<?php include(APP_VIEW.'includes/header.php');?>
<?php include(APP_VIEW.'includes/nav.php');?>
<section id="main-content">
	<section class="wrapper site-min-height">
      	<!-- page start-->
         	<div class="row">
				<div class="col-lg-12">
					<section class="panel">
                        <!--<header class="panel-heading">
                        	
                            
                        </header>-->
                        <div class="panel-body">
                        	<?php 
							  if($_SESSION['app_user']['mp_details_step_complete']==0)
							  {
							  ?>      
							  <div class="row">
								<div class="col-md-12">
								<div class="callout callout-danger">
										<h4>Profile Incomplete!</h4>
										<p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile">Click Here</a> to update your profile.</p>
									  </div>
								</div>
							  </div>
							  <?php }  ?>
							  <div class="row">
									<?php if($_SESSION['app_user']['mp_details_step_complete']==1)
									{ ?>
									<!--<div class="col-md-2">
										<a href="<?php echo APP_URL; ?>admin_mp/profile/profile_edit" class="btn btn-warning btn-block">Business Details</a>
									</div>
									<div class="col-md-2">
										<a href="<?php echo APP_URL; ?>admin_mp/profile/profile_gallery" class="btn btn-default btn-block">Gallery</a>
									</div>-->
									<?php } ?>
									<!--<div class="col-md-2">
										<a href="<?php echo APP_URL; ?>admin_mp/profile/change_password" class="btn btn-default btn-block">Change Password</a>
									</div>-->
							  </div>
							  <div class="row" style="">
								<div class="col-xs-12">
							   <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Success : </b> <span></span></div>
									<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Warning : </b> <span></span></div>
								<div class="box box-warning">
								<div class="box-header with-border">
								  <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>ACCOUNTS SETTINGS</strong></h3>
                                  <div style="height: 5px;width: 60px;margin-bottom: 30px;background: #5AD0B6;"></div>
								</div>
								<form id="form">
								<div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right" style="font-weight:normal;">Business Name</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="business_setting" id="business_setting" value="<?php echo $_SESSION['app_user']['mp_details_name']; ?>" placeholder="Business Name" class="form-control input-sm" type="text" style="width:78%;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right" style="font-weight:normal;">Email Address</label>
                                        </div>
                                        <div class="col-md-7" style="padding-right: 0px;">
                                        <input name="email_setting_parents" id="email_setting_parents" value="<?php echo $_SESSION['app_user']['mp_details_email']; ?>" class="form-control input-sm" style="width: 71%;float: left;" type="text" disabled>
                                         <button class="btn btn-shadow btn-success pull-right" type="button" style="box-shadow: none !important;background-color: #5ACFB5;border-color: #5ACFB5;padding-top: 4px;padding-right: 56px;padding-bottom: 4px;border-radius: 0px;float: left !important;" id="edit_email">CHANGE EMAIL</button>
                                      </div>
                                       <!--<div class="col-md-3" style="width: 16%;padding-left: 0px;">-->
                                       
                                        <!--</div>-->
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-right" style="font-weight:normal;">Password</label>
                                        </div>
                                        <div class="col-md-7" style="padding-right: 0px;">
                                        <input name="password_parents" style="width: 71%;float: left;" id="password_parents" value="" class="form-control input-sm" type="password" placeholder="*******" disabled>
                                        <button class="btn btn-shadow btn-success pull-right" type="button" style="box-shadow: none !important;background-color: #5ACFB5;border-color: #5ACFB5;padding-top: 4px;padding-right: 24px;padding-bottom: 4px;border-radius: 0px;float: left !important;" id="edit_password">CHANGE PASSWORD</button>
                                      </div>
                                      <!--<div class="col-md-3" style="width: 16%;padding-left: 0px;">
                                        
                                        </div>-->
                                    </div>
                                  </div>
                                </div>
                                <!--<div class="marTop10"></div>-->
                                <!--<div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">New Password</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="n_password" placeholder="" class="form-control input-sm" type="password">
                                      </div>
                                    </div>
                                  </div>
                                </div>-->
                                <!--<div class="marTop10"></div>-->
                                <!--<div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Confirm Password</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="c_password" placeholder="" class="form-control input-sm" type="password">
                                      </div>
                                    </div>
                                  </div>
                                </div>-->
									<div class="marTop10"></div>
									<div class="row"> 
										<div class="col-md-2" style="float: right;margin-right: 180px;">
                                        	<div class="">
                                                <button id="s_form" class="btn btn-shadow btn-success pull-right" type="submit" style="margin-top: 25px;padding: 8px 70px;
    font-size: 16px; box-shadow:none !important;">Save</button>
                                            </div>
										</div>   
									</div>
									</form>
                                    <!--MODEL CODE HERE START HERE-->
                                    <div class="modal fade" id="getCodeModal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" style="width:400px;">
                                            <div class="modal-content">
                                                <div class="modal-header" style="font-weight: 600;">
                                                CHANGE EMAIL
                                                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                                                </div>
                                                <div class="modal-body" id="getCode">
                                                    <div style="font-size: 13px;font-weight: bold;margin-bottom: 15px;">
                                                    EMAIL ADDRESS
                                                    </div>
                                                    <div>
                                                    <input name="email_setting" id="email_setting" value="<?php echo $_SESSION['app_user']['mp_details_email']; ?>" placeholder="" class="form-control input-sm" type="text">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" style="width: 70px;padding-top: 3px;
padding-bottom: 3px;background-color: #989697;border-color: #989697;">Cancel</button>
                                                <button type="button" id="ok_email" class="btn btn-default" style="width: 70px;padding-top: 3px;
padding-bottom: 3px;background-color: #E4142A;border-color: #E4142A;">Ok</button>
                                                <!--<input type="hidden" class="image_priority" name="image_priority" value="" />-->
                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="modal fade" id="getCodeModal_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" style="width:400px;">
                                            <div class="modal-content">
                                                <div class="modal-header" style="font-weight: 600;">
                                                CHANGE PASSWORD
                                                </div>
                                                <div class="modal-body" id="getCode">
                                                    <div style="font-size: 13px;font-weight: bold;margin-bottom: 5px;">
                                                    	OLD PASSWORD
                                                    </div>
                                                    <div style="margin-bottom: 15px;">
                                                    	<input name="o_password" id="o_password" value="" placeholder="" class="form-control input-sm" type="text">
                                                    </div>
                                                    <div style="font-size: 13px;font-weight: bold;margin-bottom: 5px;">
                                                    	NEW PASSWORD
                                                    </div>
                                                    <div style="margin-bottom: 15px;">
                                                    	<input name="n_password" id="n_password" value="" placeholder="" class="form-control input-sm" type="text">
                                                    </div>
                                                    <div style="font-size: 13px;font-weight: bold;margin-bottom: 5px;">
                                                    	CONFIRM NEW PASSWORD
                                                    </div>
                                                    <div style="margin-bottom: 15px;">
                                                    	<input name="c_password" id="c_password" value="" placeholder="" class="form-control input-sm" type="text">
                                                    </div>
                                                </div>
                                                <div class="modal-footer" style="border-top:none !important;">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" style="width: 70px;padding-top: 3px;
padding-bottom: 3px;background-color: #989697;border-color: #989697;">Cancel</button>
                                                <button type="button" id="ok_password" class="btn btn-default" style="width: 70px;padding-top: 3px;
padding-bottom: 3px;background-color: #E4142A;border-color: #E4142A;">Ok</button>
                                                <!--<input type="hidden" class="image_priority" name="image_priority" value="" />-->
                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <!--MODEL CODE HERE START HERE-->
								</div>
								</div>
							  </div>
                         </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>


<?php include(APP_VIEW.'includes/footer.php');?>
<?php include(APP_VIEW.'includes/bottom.php');?>




<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-multiselect.js"></script>

<script type="text/javascript" src="<?php echo APP_CRM_BS; ?>js/jquery.timepicker.js"></script>
<script src="http://thevectorlab.net/flatlab/js/bootstrap-switch.js"></script>
<script>
function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}
$(function()
{
	$( "#s_form" ).prop( "disabled", true );
	
	$("#business_setting").click(function(){
		$( "#s_form" ).removeAttr("disabled");
	});
	
	
	$("#ok_email").click(function()
	{
		var emailaddress	=	$("#email_setting").val();
		if( !validateEmail(emailaddress)) { 
			alert("Please enter correct Email address"); 
			$("#email_setting").focus();
			return false;
		}
		$('#email_setting_parents').val(emailaddress);
		jQuery("#getCodeModal_email").modal('hide');
		$("#email_setting").val();
		$( "#s_form" ).removeAttr("disabled");
	});
	
	$(document).on('click', '#edit_password', function(){
		jQuery("#getCodeModal_password").modal('show');
	});
	
	$("#ok_password").click(function(){
		var o_password	=	$("#o_password").val();
		var n_password	=	$("#n_password").val();
		var c_password	=	$("#c_password").val();
		
		if(o_password	==	""){
			alert("Please Enter Old Password"); 
			$("#o_password").focus();
			return false;
		}
		if(n_password	==	""){
			alert("Please Enter New Password"); 
			$("#n_password").focus();
			return false;
		}
		if(c_password	==	""){
			alert("Please Enter Confirm Password"); 
			$("#c_password").focus();
			return false;
		}
		if(n_password	!=	c_password){
			alert("The new password and its confirm password not same"); 
			$("#c_password").focus();
			return false;
		}
		$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/profile/checkPassword',
            type: 'POST',
			data: {'old':o_password},
            success: function (data) {
				if(data	!=	1)
				{
					alert("Your old password is Incorrect"); 
					$("#o_password").focus();
					return false;
				}
            },
		});
		$('#password_parents').val(c_password);
		$( "#s_form" ).removeAttr("disabled");
		jQuery("#getCodeModal_password").modal('hide');
	});
	//$("#email_setting").prop("disabled",true);
	var cnt	=	0;
	//$("#edit_email").click(function(){
	$(document).on('click', '#edit_email', function(){
		//$("#getCode").html('2323');
		jQuery("#getCodeModal_email").modal('show');
		/*
		$("#email_setting").prop("disabled",false);
		$("#email_setting").focus();
		$("#edit_email").html('SAVE');
		var emailaddress	=	$("#email_setting").val();
		
		if( !validateEmail(emailaddress)) { 
			alert("Please enter correct Email address"); 
			$("#email_setting").focus();
			return false;
		}
		
		var formData = new FormData( this );
		console.log(formData);
		formData.append("flag","email_change");
		formData.append("email_setting",emailaddress);
		if(cnt >= 1){
		$.ajax({
				url: '<?php //echo APP_URL; ?>admin_mp/profile/changePassword',
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				success: function (data) {
					
					if(data.match("true"))
					{
						//$('.alert-warning').hide();
						//$('.alert-success').show();
						//$('.alert-success span').html("Email changed successfully!");
						$("#email_setting").prop("disabled",true);
						$("#edit_email").html('EDIT EMAIL');
						location.reload();
					}
					else
					{
						$('.alert-success').hide();
						$('.alert-warning').show();
						$('.alert-warning span').html("There is some error while Updating Email!");
					}
				},
				error: function (data) {
				}
	   });
		}
		
	   cnt++;
	  
	*/});
	
	
	$('#form').bootstrapValidator({
		fields: {
					/*old_password: {
					validators: {
						notEmpty: {
							message: 'Please Enter Old Password'
						}
					}
				},
				n_password: {
					validators: {
						identical: {
							field: 'c_password',
							message: 'The password and its confirm are not the same'
						},
						notEmpty: {
							message: 'Please Enter New Password'
						}
					}
				},
				c_password: {
					validators: {
						identical: {
							field: 'n_password',
							message: 'The password and its confirm are not the same'
						},
						notEmpty: {
							message: 'Please Enter Confirm Password'
						}
					}
				}*/
				}
		}).on('success.form.bv', function (e) {
        e.preventDefault();
        //var formData = new FormData( this );
		var b_name	=	$("#business_setting").val();
		var e_name	=	$("#email_setting_parents").val();
		var p_name	=	$("#password_parents").val();
		
		if(b_name	==	"")
		{
			alert('please Enter Business Name');
			$("#business_setting").focus();
			return false;
		}
		
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/profile/changePassword',
			type: 'POST',
			data: {'b_name':b_name,'e_name':e_name,'p_name':p_name},
			success: function (data) {
				$( "#s_form" ).removeAttr("disabled");
				//alert(data);
				if(data	== "")
				{
					$('.alert-warning').hide();
					$('.alert-success').show();
					$('.alert-success span').html("Setting changed successfully!");
					location.reload();
				}
				else
				{
					$('.alert-success').hide();
					$('.alert-warning').show();
					$('.alert-warning span').html("There is some error while Updating Setting!");
				}
			},
			error: function (data) {
			}
	   });
});

});
</script>
</body>
</html>
