<style> 
.animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
}
.stars
{
    font-size: 24px;
    color: #d17581;
}
</style>
<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">Add Review</h4>
    </div>
    <div class="modal-body">
      <div class="error"></div>
      <form id="form" method="post">
		<div class="row" style="border-bottom:1px solid #ccc">
        <div class="col-md-8">
        	<p><strong>1.</strong> Did you like quality of food?</p>
        </div>
		<div class="col-md-4">
                        <input id="ratings-hidden1" name="rating" type="hidden"> 
                        <div class="text-right">
                            <div class="stars starrr" data-id="ratings-hidden1" data-rating="0"></div>
                        </div>
        </div> 
</div>
		<div class="row" style="margin-top:20px; border-bottom:1px solid #ccc">
        <div class="col-md-8">
        	<p><strong>2.</strong> Did you like services?</p>
        </div>
		<div class="col-md-4">
                        <input id="ratings-hidden2" name="rating" type="hidden"> 
                        <div class="text-right">
                            <div class="stars starrr" data-id="ratings-hidden2" data-rating="0"></div>
                        </div>
        </div> 
</div>
<div class="row" style="margin-top:20px; border-bottom:1px solid #ccc">
        <div class="col-md-8">
        	<p><strong>3.</strong> Did you like ambience?</p>
        </div>
		<div class="col-md-4">
                        <input id="ratings-hidden3" name="rating" type="hidden"> 
                        <div class="text-right">
                            <div class="stars starrr" data-id="ratings-hidden3" data-rating="0"></div>
                        </div>
        </div> 
</div>
<div class="row" style="margin-top:20px; border-bottom:1px solid #ccc">
        <div class="col-md-8">
        	<p><strong>4.</strong> Did you like staff?</p>
        </div>
		<div class="col-md-4">
                        <input id="ratings-hidden4" name="rating" type="hidden"> 
                        <div class="text-right">
                            <div class="stars starrr" data-id="ratings-hidden4" data-rating="0"></div>
                        </div>
        </div> 
</div>
<div class="row" style="margin-top:20px; border-bottom:1px solid #ccc">
        <div class="col-md-8">
        	<p><strong>5.</strong> Did you like cleanliness?</p>
        </div>
		<div class="col-md-4">
                        <input id="ratings-hidden5" name="rating" type="hidden"> 
                        <div class="text-right">
                            <div class="stars starrr" data-id="ratings-hidden5" data-rating="0"></div>
                        </div>
        </div> 
</div>
       <div class="row">
       <div class="col-md-5"></div>
       <div class="col-md-6"> 
       <input type="hidden" name="booking_id" id="booking_id" value="<?php echo $data[0]; ?>" />    
       <button class="btn-warning btn custom_warning_btn" style="margin-top:10px" type="submit" id="Submit_review">Submit Review</button>
       </div>
       </div>
      </form>
    </div>
    <script>
    (function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:10,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<10){for(t=r=e;e<=9?r<=9:r>=9;t=e<=9?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

$(function(){
  $('#new-review').autosize({append: "\n"});
  var reviewBox = $('#post-review-box');
  var newReview = $('#new-review');
  
  $('.starrr').on('starrr:change', function(e, value){
	 var id = $(this).data('id');
	  var ratingsField = $('#'+id);
      ratingsField.val(value);
  });
  var Rateing_array	=	[];
  $('#form').submit(function()
  {
	   var cnt = 0;
	   var score = 0;
	   $('input[name = rating]').each(function() 
	   {
			if($(this).val())
			{
				cnt++;
				score = score + parseInt($(this).val());
				Rateing_array.push($(this).val());
			}
			else
			{
				Rateing_array.push('NULL');
			}
		});
		var scoreAverage = score/cnt;
		scoreAverage = scoreAverage.toFixed(1);
		var booking_id = $('#booking_id').val();
		$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/review_mp/scoreAdd',
				type: 'POST',
				data: {'booking_id' : booking_id, 'score' : scoreAverage, 'Rateing_score' : Rateing_array},
				success: function (data) {
					if(data==1)
					{
						$('.error').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Score Added successfully!</span></div>');
						$("#Submit_review").attr("class","btn-warning btn custom_warning_btn disabled");
						$("#Submit_review").attr("type","button");
					}
					else
					{
						$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span>There is some error!</span></div>');
					}
				},
				error: function (data) {
				}
		});
		Rateing_array = [];
		return false;
  });
  
});
    </script>