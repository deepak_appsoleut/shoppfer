<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Shoppfer Reset Password</title>
<!-- Bootstrap Core CSS -->
    <link href="<?php echo APP_CSS; ?>bootstrap.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo APP_CSS; ?>general.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo APP_CSS; ?>font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
</head>
<body id="page-top" class="index">
<!-- Navigation -->
<nav class="navbar navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="<?php echo APP_URL; ?>">
                	<img height="50" src="<?php echo APP_IMAGES; ?>logo.png" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" style="margin-top:20px">
                    <li>
                        <a class="page-scroll" href="#about">ABOUT</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#forMP">CONTACT</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<!-- Header -->
<div class="container" style="margin-top:150px">
<?php if($data==0) {
	?>
    <div class="row text-center">
      <div class="col-md-12">
        <h2>Reset Password</h2>
      </div>
    </div>
    <div class="row">
    	<div class="col-md-8 col-sm-offset-2">
        <p>Sorry! Your reset code link has been expired or wrong. Please click here to reset your password again</p>
        </div>
        <div class="col-sm-3 col-md-3 col-sm-offset-4">
    	<a class="btn redBtn btn-block" style="background-color:#ef1428;margin-left:50px;" href="<?php echo APP_URL; ?>admin_mp/login/forgot_password" type="submit">Forgot Password</a>
    </div>
    </div>
    <?php
}
else
{
?>
<div id="showForm">
<div class="row text-center">
      <div class="col-md-12">
        <h2>Reset Password</h2>
      </div>
    </div>
<form class="form-signin" id="form" style="margin-top:30px">
  <div class="row text-center">
    <div class="col-sm-4 col-md-4 col-sm-offset-4">
      <div class="form-group">
        <label class="pull-left">New Password</label>
        <input name="password" id="password" placeholder="New Password" class="form-control input-sm" type="password">
        <input name="mp_id" value="<?php echo $data; ?>" type="hidden">
      </div>
    </div>
  </div>
  <div class="row text-center">
    <div class="col-sm-4 col-md-4 col-sm-offset-4">
      <div class="form-group">
        <label class="pull-left">Confirm Password</label>
        <input name="cnf_password" id="cnf_password" placeholder="Confirm Password" class="form-control input-sm" type="password">
      </div>
    </div>
  </div>  
  <div class="row text-center">
    <div class="col-sm-3 col-md-3 col-sm-offset-4">
    	<button class="btn redBtn btn-block" type="submit"  style="margin-left:50px;">OK</button>
    </div>
    <div class="col-sm-5 col-md-5 col-sm-offset-5">
    </div>
  </div> 
</form>
</div>
<div class="row text-center" id="showMessage" style="display:none">
    <div class="col-sm-4 col-md-4 col-sm-offset-4">
      <h3 style="font-size:25px">Password reset successfully!</h3>
      <h5>Please click here to login here!</h5>
    </div>
    <div class="col-sm-3 col-md-3 col-sm-offset-4">
    	<a class="btn redBtn btn-block" style="background-color:#ef1428;margin-left:50px;" href="<?php echo APP_URL; ?>admin_mp/login" type="submit">Login</a>
    </div>
  </div>
<?php } ?> 
</div>
<script src="<?php echo APP_JS; ?>jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo APP_JS; ?>bootstrap.min.js"></script> 
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<!-- jQuery --> 
<!-- Contact Form JavaScript --> 
 <script type="text/javascript">
$('#form').bootstrapValidator({
        fields: {
			password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Password'
                    },
					stringLength: {
                        min: 6,
                        message: 'Password must be atleast 6 characters long!'
                    }
                }
            },
            cnf_password: {
                validators: {
					identical: {
						field: 'password',
						message: 'The password and its confirm password are not the same!'
					},
					notEmpty: {
                        message: 'Please Enter Confirm Password'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/login/reset_password_process',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				console.log(data);
				if(data=="true")
				{
					$('#showForm').hide();
					$('#showMessage').show();
				}
				else
				{
				}
            },
            error: function (data) {
            }
   });
});
</script> 
<!-- Custom Theme JavaScript -->
</body>
</html>
