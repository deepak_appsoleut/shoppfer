<?php 
	$dateArray = array();
	$values = array();
	$values1 = array();
	foreach($data[1] as $arr=>$val){
		foreach($val as $key=>$val1)
		{
			if($key=="label")
			{
				array_push($dateArray, $val1);
			}
			else if($key=="cnt")
			{
				array_push($values, $val1);
			}
		}
	}
	foreach($data[2] as $arr=>$val){
		foreach($val as $key=>$val1)
		{
			if($key=="cnt")
			{
				array_push($values1, $val1);
			}
		}
	}
	//echo json_encode($data[2]);
?>
 <div class="row">
 <div class="col-md-12">
          <!-- Line chart -->
          <div class="box">
            <div class="box-header with-border" style="background: #242a30;">
              <h3 class="box-title" style="color:#fff; font-size:20px;font-weight: normal; font-size:14px">Line Chart for Revenue Comparison</h3>
            </div>
            <div class="box-body" style="margin-bottom:40px">
              <div id="line-chart" style="height: 300px;"></div>
              
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->

        </div>
 	
 </div>      
<script type="text/javascript">
$(function () {
	$('#line-chart').highcharts({
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: <?php echo json_encode($dateArray); ?>
        },
        yAxis: {
            title: {
                text: 'Revenue(US Dollar)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{"name":"Selected Range","data":<?php echo json_encode($values, JSON_NUMERIC_CHECK); ?>}, {"name":"Previous Range","data":<?php echo json_encode($values1, JSON_NUMERIC_CHECK); ?>},]
    });
});
		</script>     