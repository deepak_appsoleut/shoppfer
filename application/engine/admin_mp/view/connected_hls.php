<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
#loading {
    position: absolute;
	display:none;
	top:5%;
    left: 50%;
    z-index: 999999;
}
</style>
</head>
<body class="hold-transition skin-white sidebar-mini">
<div class="wrapper">
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $hotel = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Connected Hotels
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo APP_URL ?>admin_hp/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Connected Hotels</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     <?php if($_SESSION['app_user']['role_type_id']==NULL) { ?>
      
      <?php } ?>
      <?php if($_SESSION['app_user']['role_type_id']==2) { 
	  if($_SESSION['app_user']['mp_details_step_complete']==0)
	  {
	  ?>      
      <div class="row">
      	<div class="col-md-12">
        <div class="callout callout-danger">
                <h4>Profile Incomplete!</h4>
                <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_/profile">Click Here</a> to update your profile.</p>
              </div>
        </div>
      </div>
      <?php } else { ?>
            <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x" style="margin-right:10px;"></i>Loading Data....</div>
              <!-- /.tab-pane -->
              		<div class="row">
                    <div class="col-xs-12">
                            <div class="row">
                                <div class="col-md-12">
                                <div id="map_canvas2" style="height:350px; display:none"></div>
                                </div>
                            </div>
                            <div id="myData2">
                            </div>
                    </div>
                  </div>
      <?php } }?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include(APP_VIEW.'includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<script>
function getData(id, category, sortingValue)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/connected_hotels/getLocationData',
            type: 'POST',
			data: {"tab" : id, "category" : category, "sortingValue" : sortingValue},
            success: function (data) {
				console.log(data);
				$('#loading').hide();
				$('#myData2').html(data);
            },
            error: function (data) {
            }
   	});
}
	$(function()
	{
		getData('tab_3', '', '');
	});
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
