<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Shoppfer Merchant Login</title>
<!-- Bootstrap Core CSS -->
    <link href="<?php echo APP_CSS; ?>bootstrap.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo APP_CSS; ?>general.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo APP_CSS; ?>font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body id="page-top" class="index">
<!-- Navigation -->
<nav class="navbar navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="<?php echo APP_URL; ?>">
                	<img height="50" src="<?php echo APP_IMAGES; ?>logo.png" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" style="margin-top:20px">
                    <li>
                        <a class="page-scroll" href="#about">ABOUT</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#forMP">CONTACT</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<!-- Header -->
<div class="container" style="margin-top:150px">
<div class="row text-center">
  <div class="col-md-12">
    <h2>Sign In</h2>
  </div>
</div>
<form class="form-signin" id="form" style="margin-top:30px">
  <div class="row text-center">
    <div class="col-sm-4 col-md-4 col-sm-offset-2">
      <div class="form-group">
        <label class="pull-left">Email Address</label>
        <input name="email" id="email" placeholder="Enter your email" class="form-control input-sm" type="text">
        <span class="emailError"></span>
      </div>
    </div>
    <div class="col-sm-4 col-md-4">
      <div class="form-group">
        <label class="pull-left">Password</label>
        <input name="password" id="password" placeholder="Enter your password" class="form-control input-sm" type="password">
        <span class="passError"></span>
      </div>
    </div>
  </div>
  
  <div class="row text-center">
    <div class="col-sm-3 col-md-3 col-sm-offset-7">
    	<button class="btn redBtn btn-block"type="submit">Let's Go!</button>
    </div>
    <div class="col-sm-5 col-md-5 col-sm-offset-5">
        <span class="error"></span>
    </div>
  </div>
  
  <div class="row text-center" style="margin-top:20px">
    <div class="col-sm-2 col-md-2 col-sm-offset-4">
      	<a href="<?php echo APP_URL; ?>default/signup" class="link">Register with Shoppfer</a>
    </div>
    <div class="col-sm-2 col-md-3" style="text-align:left">
      <a href="<?php echo APP_URL; ?>admin_mp/login/forgot_password" class="link">Forgot Your Password?</a>
    </div>
  </div>
  </div>
</form>
<script src="<?php echo APP_JS; ?>jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo APP_JS; ?>bootstrap.min.js"></script> 
<!-- jQuery --> 
<!-- Contact Form JavaScript --> 
 <script>
$(function()
{
	$('#form').submit(function()
	{
		var email = $('#email').val();
		var password = $('#password').val();
		if(email=="")
		{
			$('.passError').html('');
			$('.emailError').html('The Email ID entered is in correct. Please check and retry.');
			$('.emailError').fadeIn();
			return false;
		}
		else if(email!='')
		{
			$('.emailError').html('');
			$('.passError').html('');
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var result = regex.test(email);
			if(result==false)
			{
				$('.passError').html('');
				$('.emailError').html('Please enter correct email id!');
				$('.emailError').fadeIn();
				return false;
			}
			else if(password=="")
			{
				$('.emailError').html('');
				$('.passError').html('The Email ID or Password entered is in correct. Please check and retry.');
				$('.passError').fadeIn();
				return false;
			}
			else
			{
				var formData = new FormData( this );
		    $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/login/process',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
					console.log(data);
				if(data==0)
				{
					$('.emailError').html('');
					$('.passError').html('');
					$('.error').html('Please enter correct username or password!');
					$('.error').fadeIn();
					return false;
				}
				else
				{
					//alert("koko");
					var obj = JSON.parse(data);
					if(obj[0]['mp_details_id'])
					{
								if(obj[0]['mp_details_step_complete']==1)
								{
									
									top.location = "<?php echo APP_URL; ?>admin_mp/dashboard";
								}
								else
								{
									top.location = "<?php echo APP_URL; ?>admin_mp/profile";
								}
						}
					}
				},
				error: function (data) {
				}
   				});
				return false;
			}
		}
	})
});
</script> 
<!-- Custom Theme JavaScript -->
</body>
</html>
