<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">Edit Gallery</h4>
    </div>
    <div class="modal-body">
      <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success : </b> <span></span></div>
      <div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning : </b> <span></span></div>
      <form id="form" method="post">
      <div class="form-group">
      	  <div class="row">  			
	                  <div class="col-sm-12 col-md-12">
	                    <div class="form-group">
	                      <div class="col-md-1">
                          
	                      <label class="pull-right"><img src="<?php echo APP_CRM_UPLOADS_PATH.$data[0]["mp_gallery_thumb"]; ?>" width="50" /></label>
	                      <input type="hidden" name="galleryImg" value="<?php echo $data[0]["mp_gallery_image"]; ?>" />
                          <input type="hidden" name="galleryId" value="<?php echo $data[0]["mp_gallery_id"]; ?>" />
                          
                          </div>
	                      <div class="col-md-4">
	                      <input name="title" value="<?php echo $data[0]["mp_gallery_name"]; ?>" class="form-control input-sm" type="text">
                          </div>
                          <div class="col-md-4">
	                      <input name="file" class="form-control input-sm" type="file" id="f_upload">
	                      </div>
                          <div class="col-md-3">
	                      <select name="status" id="status" class="form-control input-sm">
                              <?php
                                if($data[0]['mp_gallery_status']==1)
                                    {	
                                ?>
                                <option selected value="1">Active</option>
                                <option value="0">Inactive</option>
                                <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option value="1">Active</option>
                                        <option selected value="0">Inactive</option>
                                        <?php
                                    }
                                ?>
                              </select>
	                      </div>
	                    </div>
	                  </div>
	       </div>	
       </div>
       <div class="row">
       <div class="col-md-5"></div>
       <div class="col-md-6">      
       <button class="btn-warning btn custom_warning_btn" type="submit">Edit Gallery</button>
       </div>
       </div>
      </form>
    </div>
    <script>
	//------------------------------------------------------------
	//Start@Get upload file and send to the adminotify_profileEdit
	//------------------------------------------------------------
	$("input[type=file]").change(function(){
			var filename 		= 	$('input[type=file]').val().split('\\').pop();
			var flag			=	"gallery";
			var mp_details_id	=	"<?php echo $_SESSION['app_user']['mp_details_id']; ?>";
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/profile/adminotify_profileEdit',
				type: 'POST',
				data: {"flag":flag,"upload_file":filename,"mp_details_id":mp_details_id},
				success: function (data) {
				},
				error: function (data) {
				}
		   });
	});
	//----------------------------------------------------------
	//END@Get upload file and send to the adminotify_profileEdit
	//----------------------------------------------------------
	
	
$('#form').bootstrapValidator({
	fields: {
	}
}).on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/profile/galleryUpdate',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				console.log(data);
				if(data.match("true"))
				{
					$('.alert-warning').hide();
					$('.alert-success').show();
					$('.alert-success span').html("Gallery Updated successfully!");
				}
				else
				{
					$('.alert-success').hide();
					$('.alert-warning').show();
					$('.alert-warning span').html("There is some error while Updating Gallery!");
				}
            },
            error: function (data) {
            }
   });
});
$(function()
{
	var previous;
	 $("#status").on('focus', function () {
        // Store the current value on focus and on change
        previous = this.value;
    }).change(function() {
        // Do something with the previous value after the change
        var cnt = 4;
		var status = $(this).val();
		if((status=='1')&&(cnt<=<?php echo $_SESSION['app_user']['galleryCount']; ?>))
		{
			alert('There is already 4 gallery image is active.');
        	// Make sure the previous value is updated
			$("#status").val(previous);
		}
    });
});
</script>