<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Marketing Partner Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
  <!-- jvectormap -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $reviews = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper">
			  <section class="panel">
                              <?php 							  
							  $total =  $data[0][0]['good'] +  $data[1][0]['average'] +  $data[2][0]['bad'];
							  if($total!=0)
							  {
								  $good = number_format(($data[0][0]['good']/$total)*100, 1);
								  $average =  number_format(($data[1][0]['average']/$total)*100, 1);
								  $bad =  number_format(($data[2][0]['bad']/$total)*100, 1);
							  }
							  ?>
                 <header class="panel-heading"><strong>Reviews</strong></header> 
              <div class="panel-body">
                 <div class="row state-overview">
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel" style="background-color:#DEDEDE">
                          <div class="symbol terques">
                              <i class="fa fa-smile-o"></i>
                          </div>
                          <div class="value">
                              <h1 class="count">
                                  <?php echo $good; ?>%
                              </h1>
                              <p>Best</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-1"></div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel" style="background-color:#DEDEDE">
                          <div class="symbol blue">
                              <i class="fa fa-meh-o"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count2">
                                 <?php echo $average; ?>%
                              </h1>
                              <p>Average</p>
                          </div>
                      </section>
                  </div>
                   <div class="col-lg-1"></div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel" style="background-color:#DEDEDE">
                          <div class="symbol red">
                              <i class="fa fa-frown-o"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count3">
                                  <?php echo $bad; ?>%
                              </h1>
                              <p>Poor</p>
                          </div>
                      </section>
                  </div>
              </div>
              </div>
              </section>
              <section class="panel">
                 <header class="panel-heading"><strong>Reviews & Reply</strong><div class="pro-sort pull-right">
                                  <select class="form-control" style="display:inline; width:auto; height:30px; padding:2px; font-size:14px">
                                      <option>Default Sorting</option>
                                      <option>Good rating</option>
                                      <option>Average Rating</option>
                                      <option>Poor Rating</option>
                                  </select>
                              </div></header>  
                                
             	<div class="panel-body">
                                  <!-- Comment -->
                                  <!--<div class="msg-time-chat">
                                      <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                          <div class="text" style="float:left; width:100%">
                                          	  <div class="row">
                                              <div class="col-md-6" style="background-color:#E6E6E6; margin:0px 0px 10px 15px; padding:10px; border-radius:5px">	
                                              <p class="attribution"><a href="#">Jhon Doe</a> at 1:55pm, 13th April 2013</p>
                                              <p>well good to know that. anyway how much time you need to done your task? well good to know that. anyway how much time you need to done your task?</p>
                                              </div>
                                              </div>
                                              <div class="row">
                                              <div class="col-md-6 pull-right" style="background-color:#DEE1EF; margin-right:15px; padding:10px; border-radius:5px">	
                                              		<p class="attribution"><a href="#">Plaza Hotel</a> at 2:55pm, 13th April 2013</p>
                                              		<p>well good to know that. anyway how much time you need to done your task?</p>
                                                  <!--<i class="fa fa-reply-all"></i>
                                              </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>-->
                                  <!-- /comment -->

                                  <!-- Comment -->
                                  <!--<div class="msg-time-chat">
                                     <div class="message-body msg-out">
                                          <span class="arrow"></span>
                                          <div class="text" style="background: #E6E6E6;">
                                              <p class="attribution"> <a href="#">Jonathan Smith</a> at 2:01pm, 13th April 2013</p>
                                              <p>I'm Fine, Thank you. What about you? How is going on?</p>
                                          </div>
                                          <div style="clear:both"></div>
                                          <div class="text col-md-11 pull-right" style="background: #DEE1EF; margin-top:10px;">
                                              <p class="attribution"><a href="#">Plaza Hotel</a> at 2:55pm, 13th April 2013</p>
                                              		<p>well good to know that. anyway how much time you need to done your task?</p>
                                          </div>
                                      </div>
                                  </div>
                                  <!-- /comment -->

                                  <!-- Comment -->
                                  <div class="msg-time-chat">
                                     <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                          <div class="text" style="background: #E6E6E6; float:left; width:100%">
                                              <p class="attribution"><a href="#">Jhon Doe</a> at 2:03pm, 13th April 2013 <span style="margin-left:20px"><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></p>
                                              <p>Yeah I'm fine too. Everything is going fine here.</p>
                                          </div>
                                      </div>
                                      
                                      <div class="message-body msg-out col-md-11 pull-right">
                                          <span class="arrow"></span>
                                          <div class="text" style="background: #DEE1EF; margin-top:10px; float:left; width:100%">
                                              <p class="attribution"><a href="#">Jhon Doe</a> at 2:03pm, 13th April 2013</p>
                                              <p>Yeah I'm fine too. Everything is going fine here.</p>
                                          </div>
                                      </div>
                                      
                                  </div>
                                  <!-- /comment -->

                                  <!-- Comment -->
                                  <div class="msg-time-chat">
                                      <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                          <div class="text" style="background: #E6E6E6; float:left; width:100%">
                                              <p class="attribution"><a href="#">Jonathan Smith</a> at 2:05pm, 13th April 2013 <span style="margin-left:20px"><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></p>
                                              <p>well good to know that. anyway how much time you need to done your task?</p>
                                              <span class="pull-right reply" id="rply1" style="cursor:pointer"><i class="fa fa-reply-all"></i></span>
                                              <form class="replyText" id="text1" style="display:none">
                                              <textarea id="para1" class="form-control"></textarea>
                                              <button type="button" id="bttn1" class="btn pull-right btn-xs btn-primary" style="margin-top:5px">Send Reply</button></form>
                                          </div>
                                      </div>
                                  <div id="show1"></div>
                                  </div>
                                  
                                  <!-- /comment -->
                                  <!-- Comment -->
                                  <div class="msg-time-chat">
                                     <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                          <div class="text" style="background: #E6E6E6; float:left; width:100%">
                                              <p class="attribution"><a href="#">Jhon Smith</a> at 1:55pm, 13th April 2013 <span style="margin-left:20px"><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star" style="color:#C00"></i><i class="fa fa-star"></i></span></p>
                                              <p>Hello, How are you brother?</p>     
                                              <span class="pull-right reply" id="rply2" style="cursor:pointer"><i class="fa fa-reply-all"></i></span>
                                              <form class="replyText" id="text2" style="display:none">
                                              <textarea id="para2" class="form-control"></textarea>
                                              <button type="button" id="bttn2" class="btn pull-right btn-xs btn-primary" style="margin-top:5px">Send Reply</button></form>                                         
                                          </div>
                                          
                                      </div>
                                   <div id="show2"></div>
                                  </div>
                                  <!-- /comment -->
                                  <div class="pull-right">
                                  <?php $pg = new pagination(); 
				 $pg->pagenumber = 1;
$pg->pagesize = 10;
$pg->totalrecords = 2000;
$pg->showfirst = true;
$pg->showlast = true;
$pg->paginationcss = "pagination-normal";
$pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
$pg->defaultUrl = "#";
$pg->paginationUrl = "/[p]";
echo $pg->process();
				 ?>          </div>
                          </div>
                
              </section>
          </section>
      </section>
  <!-- /.content-wrapper -->
  
</section>
</body>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script>
  $(function () {	  
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
	$('.reply').click(function()
	{
		var str = $(this).attr('id');
		var removedStr = str.substr(4);
		if ($("#text"+removedStr).css('display') === 'none') {
			$("#text"+removedStr).show();
		}
		else
		{
			$("#text"+removedStr).hide();
		}
	});
	$('.btn').click(function()
	{
		var str = $(this).attr('id');
		var removedStr = str.substr(4);	
		var txt = $('#para'+removedStr).val();
		if($.trim(txt)=="")
		{
			return false;
		}
		else
		{
			$('#rply'+removedStr).hide();
			$('#text'+removedStr).hide();
			$('#show'+removedStr).html('<div class="message-body msg-out col-md-11 pull-right"><span class="arrow"></span><div class="text" style="background: #DEE1EF; margin-top:10px; float:left; width:100%"><p class="attribution"><a href="#">Jhon Doe</a> at 2:03pm, 13th April 2013</p><p>'+txt+'</p></div></div>');
		}
	});
  });
</script>
<!-- AdminLTE App -->
<script>
function showData1(startDate, endDate)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/analytics_mp/analytics_data_dashboard_mp',
            type: 'POST',
			data: {"startDate" : startDate, "endDate" : endDate},
            success: function (data) {
				$('#loading').hide();
				$('#data1').html(data);
            },
            error: function (data) {
            }
   	});
}
$(function () {
	
	// set current month startdate and enddate
	  var d = new Date();	  
	  var firstDay = new Date(d.getFullYear(), d.getMonth(), 1);	  
	  var month = firstDay.getMonth()+1;
	  var day = firstDay.getDate();
	  var startDate = firstDay.getFullYear() +'/' + ((''+month).length<2 ? '0' : '') + month + '/' + ((''+day).length<2 ? '0' : '') + day;
	  
	  var lastDay = new Date(d.getFullYear(), d.getMonth() + 1, 0);
	  var month1 = lastDay.getMonth()+1;
	  var day1 = d.getDate();
	  var endDate = lastDay.getFullYear() +'/' + ((''+month1).length<2 ? '0' : '') + month1 + '/' + ((''+day1).length<2 ? '0' : '') + day1;
	  showData1(startDate, endDate);
	  
	  // function to execute dashboard data in 10 seconds
	  setInterval(function(){showData1(startDate, endDate);}, 10000);
  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</html>
