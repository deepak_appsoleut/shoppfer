<?php
if($_SESSION['app_user']['mp_details_step_complete']==1)
{
	header('Location:'.APP_URL.'admin_mp/profile/profile_edit');
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bookmydeal | Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/steps.css">
  <link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/bootstrap-multiselect.css">
  
  
  <!--<link rel="stylesheet" type="text/css" href="<?php echo APP_CRM_BS; ?>css/jquery.timepicker.css" />-->
  <link rel="stylesheet" type="text/css" href="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/compiled/timepicker.css" />
  
  
  
   <!--<link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/timepicker.css" />-->
 
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>
.formElement .btn{padding:2px 6px;}
.multiselect-container{max-height: 200px;overflow-y: auto;overflow-x: hidden;}
#page {display: none;}
#loading {display: block;position: absolute;top: 25%;left: 50%;z-index: 100;}
.site-min-height {min-height: 500px;}
.has-switch span.switch-left{background-color: #13D4A8;color: #fff;}
.has-switch span.switch-right{background-color: #FF6C5E;color: #fff;margin-top: -21px;}
.has-switch > div.switch-on label{background-color: #fff !important;}
.has-switch > div.switch-off label{background-color: #fff !important;}
.has-switch label{border: 7px solid #13D4A8;}
.has-switch{ width: 73px;height: 28px;}
.has-switch > div{ width:163%;}
.has-switch > div.switch-off label{border-color: #FF6C5E;}
.has-switch span{ font-weight:400;}
.btnCross {position: absolute;right: 15px;font-size: 12px;color: #fff;background: #454545;padding: 0px 5px;top: 5px;display: none;}
.wizard > .actions > ul > li.disabled {
  /*display: none;*/
}
.wizard > .actions a, .wizard > .actions a:hover, .wizard > .actions a:active{
background: #E51429;padding: 10px 35px;
}

.wizard > .actions > ul {
    margin-top: 30px !important;
}

.actions > ul > li:first-child{ margin: 0em 0em 0 1em !important;}
.col-md-3{ text-align:right;}
body{    font-family: Arial,sans-serif;}
label { font-weight:100;
    margin-top: 5px;
}
.top-nav {
    margin-top: 20px;
}
.input-sm {
   
    width: 90%;
   
}
.wizard.vertical > .actions > ul > li {
    margin: 0em 6em 0 1em;
}
</style>


<?php
if($_SESSION['app_user']['mp_details_step']!=2)
{
?>
<style>#main-content{margin-left: 340px;}</style>	
<?php }else{ ?>          
<style>#main-content{margin-left: 210px;}</style>
<?php } ?>
</head>
<body class="hold-transition skin-white sidebar-mini">

<?php include(APP_VIEW.'includes/header.php');?>
<?php include(APP_VIEW.'includes/nav.php');?>

	
    
<!-- <div class="wrapper">
  <div class="content-wrapper">
    <section class="content" id="page">
		<div id="wizard">-->
<section id="main-content">

	<section class="wrapper site-min-height">
      	<!-- page start-->
        	<div id="loading"><i class="fa fa-spinner fa-pulse fa-3x" style="margin-right:10px;"></i>Loading Data....</div>
         	<div class="row">
				<div class="col-lg-12">
					<section class="panel content" id="page">
                        <header class="panel-heading" style="color: #474747;font-size: 18px;font-weight: normal;text-align: center;">
                        	<span id="form_title">
                            <?php
							if($_SESSION['app_user']['mp_details_step']!=2)
							{
							?>
                             Please Give the Details of Your Business
                            <?php }else{ ?>
                             A Few More Details Please
                            <?php } ?>
                            </span>
                        </header>
                        <header class="panel-heading" style="color: #474747;font-size: 18px;font-weight: 600;text-align: center;padding: 0px;    margin-bottom: 15px;">
                            <span style="height: 4px;width: 77px;position: absolute;background-color: #E51429;margin-left: -35px;"></span>
                        </header>
                        <div class="panel-body">        
						<div id="wizard">
                        <form id="example-form" action="#">
          		<div>
                    <h2></h2>
                    <section>
                    <!--<div id="triangle-left"></div>-->
                    <div class="marTop10"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">Business Name</label>
                            </div>
                            <div class="col-md-9">
                            <input name="business" id="business" value="<?php echo $_SESSION['app_user']['mp_details_name']; ?>" placeholder="Business Name" class="form-control input-sm" type="text">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                        <!--<div class="form-group">
                          <div class="col-md-6">
                            <label class="pull-left">Country*</label>
                            </div>
                            <div class="col-md-6">
                            <select name="country" id="country" class="form-control input-sm">
                            <option value=""></option>
                                <?php for($i=0; $i<count($data[0]); $i++) { 
                                    if($data[0][$i]['country_id']==$_SESSION['app_user']['mp_details_country'])
                                    {
                                        ?>
                                        <option selected value="<?php echo $data[0][$i]['country_id']; ?>"><?php echo $data[0][$i]['country_name']; ?></option>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option value="<?php echo $data[0][$i]['country_id']; ?>"><?php echo $data[0][$i]['country_name']; ?></option>
                                        <?php	
                                    }
                                ?>
                                
                                <?php } ?>
                            </select>
                          </div>
                        </div>-->
                      </div>
                    </div>
                    <div class="marTop10"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">Business Contact Number</label>
                            </div>
                            <div class="col-md-9">
                            <input name="business_contact" id="business_contact" value="" placeholder="" class="form-control input-sm" type="text" maxlength="13">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                      </div>
                    </div>
                    
                    <div class="marTop10"></div>
                    <div class="row">
                      <div class="col-sm-6 col-md-6">
                        <!--<div class="form-group">
                          <div class="col-md-6">
                            <label class="pull-left">State*</label>
                            </div>
                            <div class="col-md-6">
                            <select name="state" id="state" class="form-control input-sm">
                                <?php for($i=0; $i<count($data[4]); $i++) { 
                                    if($data[4][$i]['state_id']==$_SESSION['app_user']['mp_details_state'])
                                    {
                                        ?>
                                        <option data-state="<?php echo $data[4][$i]['state_name']; ?>" selected value="<?php echo $data[4][$i]['state_id']; ?>"><?php echo $data[4][$i]['state_name']; ?></option>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option data-state="<?php echo $data[4][$i]['state_name']; ?>" value="<?php echo $data[4][$i]['state_id']; ?>"><?php echo $data[4][$i]['state_name']; ?></option>
                                        <?php	
                                    }
                                ?>
                                
                                <?php } ?>
                            </select>
                          </div>
                        </div>-->
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">City</label>
                            </div>
                            <div class="col-md-9">
                            <input name="city" id="city" value="<?php echo $_SESSION['app_user']['mp_details_city']; ?>" placeholder="" class="form-control input-sm" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="marTop10"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">Address</label>
                            </div>
                            <div class="col-md-9">
                            <input name="address" id="address" value="<?php echo $_SESSION['app_user']['mp_details_address']; ?>" placeholder="" class="form-control input-sm" type="text">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                        <!--<div class="form-group">
                          <div class="col-md-6">
                            <label class="pull-left">Address1</label>
                            </div>
                            <div class="col-md-6">
                            <input name="address1" id="address1" value="<?php echo $_SESSION['app_user']['mp_details_address1']; ?>" placeholder="Address 2" class="form-control input-sm" type="text">
                          </div>
                        </div>-->
                      </div>
                    </div>
                    <div class="marTop10"></div>
                    <div class="row">
                      <div class="col-sm-6 col-md-6">
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">Web URL<span style="font-size: 10px;">(optional)</span></label>
                            </div>
                            <div class="col-md-9">
                            <input name="web_url" id="web_url" value="" placeholder="" class="form-control input-sm" type="text">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="marTop10"></div>
                    <div class="row">
                    <!--<div class="col-sm-6 col-md-6">
                        <div class="form-group">
                        <div class="col-md-6">
                        	<label class="pull-left">Business Contact Number*</label>
                        </div>
                        <div class="col-md-6">
                        	<input name="business_contact" id="business_contact" value="<?php echo $_SESSION['app_user']['mp_details_contact']; ?>" placeholder="Contact Number" class="form-control input-sm" type="text">
                      </div>
                        </div>
                      </div>-->
                    <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">Name of Contact Person</label>
                            </div>
                            <div class="col-md-9">
                            <input name="person" id="person" value="<?php echo $_SESSION['app_user']['mp_details_person']; ?>" placeholder="" class="form-control input-sm" type="text">
                          </div>
                        </div>
                      </div>
                                        
                    </div>
                    <div class="marTop10"></div>
                    <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">Number of Contact Person</label>
                            </div>
                            <div class="col-md-9">
                            <input name="contact" id="contact" value="<?php echo $_SESSION['app_user']['mp_details_phone']; ?>" placeholder="" class="form-control input-sm" type="text" maxlength="13">
                          </div>
                        </div>
                      </div>
                      </div>
                    <div class="marTop10"></div>
                    <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                      <div class="col-md-3">
                        <label class="">Designation of Contact Person</label>
                        </div>
                        <div class="col-md-9">
                        <input name="designation" value="<?php echo $_SESSION['app_user']['mp_details_designation']; ?>" id="designation" placeholder="" class="form-control input-sm" type="text">
                      </div>
                    </div>  
                     </div>
                     </div>
                     <div class="marTop10"></div>
                    <div class="row">
                    </div>
                    </section>
                    <h2></h2>
                    <section>
                    <div id="triangle-left" style="top:146px"></div>
                    <div class="row">
                    <h2></h2>
                    <div class="col-md-12">
                      <div class="alert alert-success alert-dismissable" style="display:none;margin-left: 15px;"> 
                      <i class="fa fa-ban"></i>
            			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            			<b>Success : </b> 
                        <span>Data saved successfully!</span>
                     </div>
                     <div class="alert alert-warning alert-dismissable" style="display:none;margin-left: 15px;"> 
                     <i class="fa fa-ban"></i>
            			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            			<b>Warning : </b> <span>There is some error while saving your data!</span>
                     </div>
                     <div class="row">
                    	<div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3" style="padding-left: 0px;">
                            <label class="">Category</label>
                            </div>
                            <div class="col-md-9" style="">
                            <select name="category" id="category" class="form-control input-sm" style="width: 93%;margin-left: -1%;">
                            <option value="">--Select Category--</option>
                            <?php for($i=0; $i<count($data[1]); $i++) { 
                                    if($data[1][$i]['category_id']==$_SESSION['app_user']['mp_details_category'])
                                    {
                                        ?>
                                        <option selected value="<?php echo $data[1][$i]['category_id']; ?>"><?php echo $data[1][$i]['category_name']; ?></option>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option value="<?php echo $data[1][$i]['category_id']; ?>"><?php echo $data[1][$i]['category_name']; ?></option>
                                        <?php	
                                    }
                                ?>
                                
                                <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                     </div>
                    <!--<div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <div class="col-md-6">
                            <label class="pull-left">Sub Category*</label>
                            </div>
                            <div class="col-md-6">
                            <select name="sub_category[]" id="sub_category" multiple="multiple" class="form-control input-sm">
                             <?php $sub = explode(',', $_SESSION['app_user']['sub_cat']);
                                for($i=0; $i<count($data[2]); $i++)
                                {
                                    if(in_array($data[2][$i]['sub_category_id'], $sub))
                                    {
                                    ?>
                                        <option selected value="<?php echo $data[2][$i]['sub_category_id']; ?>"><?php echo $data[2][$i]['sub_category_name']; ?></option>
                                    <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option value="<?php echo $data[2][$i]['sub_category_id']; ?>"><?php echo $data[2][$i]['sub_category_name']; ?></option>
                                        <?php
                                    }
                                }
                             ?>
                            </select>
                          </div>
                        </div>
                      </div>-->
                    </div>
                    <div class="marTop10"></div>
                    <div class="row">
                      <div class="col-md-12">
                      	<div class="form-group">
                          <div class="col-md-3">
                            <label class="">Recommended</label>
                            </div>
                            <div class="col-md-9">
                            <?php 
                            $explode[0] = $explode[1] = $explode[2] = "";
                            $explode = explode(',', $_SESSION['app_user']['mp_details_recommend']); 
                            if(count($explode)==0)
                            {
                                $explode[0] = $explode[1] = $explode[2] = "";
                            }
                            else if(count($explode)==1)
                            {
                                $explode[1] = $explode[2] = "";
                            }
                            else if(count($explode)==2)
                            {
                                $explode[2] = "";
                            }
                            ?>
                            <input name="recommend[]" value="<?php echo $explode[0]; ?>" placeholder="" class="form-control input-sm" type="text">
                            <!--<input name="recommend[]" value="<?php //echo $explode[1]; ?>" style="margin-top:5px" placeholder="Recommended" class="form-control input-sm" type="text">
                            <input name="recommend[]" value="<?php //echo $explode[2]; ?>" style="margin-top:5px" placeholder="Recommended" class="form-control input-sm" type="text">-->
                          </div>
                        </div>
                      </div>
                      <!--<div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <div class="col-md-6">
                            <label class="pull-left">Business Description*</label>
                            </div>
                            <div class="col-md-6">
                            <textarea name="description" rows="5" id="description" placeholder="Description" class="form-control input-sm" type="text"><?php echo $_SESSION['app_user']['mp_details_description']; ?></textarea>
                          </div>
                        </div>
                      </div>-->
                    </div>        
                    
                    <div class="marTop10"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-3">
                            <label class="">About Your Business</label>
                            </div>
                            <div class="col-md-9">
                            <textarea name="description" rows="3" id="description" placeholder="" class="form-control input-sm" type="text"><?php echo $_SESSION['app_user']['mp_details_description']; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="marTop10"></div>
                    <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                      <div class="col-md-3">
                        <label class="">Open & Close Time</label>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-3" style="margin-left: -13px;">
                           <div class="input-group bootstrap-timepicker">
                                   <!-- <input id="opentime" type="text" class="time form-control" name="opentime" value="" style="width: 110px;padding: 5px;font-size: 13px;text-align: center;font-style: italic;" placeholder="Opening Time"/>-->
                                   <!-- <div class="col-md-4">
                                          <div class="input-group bootstrap-timepicker">
                                              <input type="text" class="form-control timepicker-default" id="tp1">
                                                <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                                </span>
                                          </div>
                                      </div>-->
                                    <input type="text" class="form-control timepicker-default" id="opentime" name="opentime">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                    </span>
                              </div>
                            </div>
                       
                            <div style="float: left;margin-right: 25px;margin-top: 9px;font-size: 12px;">
                            To
                            <!--<img src="<?php echo APP_IMAGES.'Open_close_time.jpg' ?>" style="height: 34px;"/>-->
                            </div>
                             <div class="col-md-3" style=" padding-left:0px;margin-left: -7px;">
                            <div class="input-group bootstrap-timepicker">
                                   <!-- <input id="closetime" type="text" class="time form-control" name="closetime" value="" style="font-size: 13px;text-align: center;font-style: italic;width: 110px;padding: 5px;" placeholder="Closing Time"/>-->
                                    <input type="text" class="form-control timepicker-default" id="closetime" name="closetime">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                    </span>

                              </div>
                             </div>
                          </div>
                    	</div>
                    </div>
                       </div>                  
                    <div class="marTop10"></div>
                    
                    <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                        <label class="">Business Images<span style="font-size: 10px;">(optional)</span></label>
                        </div>
                        <div class="col-md-9">
						<?php 
                        $cnt=1;
                        for($i=0; $i<4; $i++){
                        $cnt = $i+2; 
                        $j = $i+1;
                        ?>
                            <div class="col-md-3" style="width:auto;padding-left: 0px;">
                                <div class="form-group">
                                	<button type="button" id="image<?php echo $j; ?>btn" class="btn btn-box-tool btnCross" data-widget="remove">
                                    <i class="fa fa-times"></i>
                                    </button>
                                	<label for="image<?php echo $j; ?>">
                                		<img id="image<?php echo $j; ?>pic" style="cursor:pointer;" height="100px" width="130px" src="<?php echo APP_IMAGES.'add_image.jpg' ?>"/>
                                	</label>
                                	<input name="image<?php echo $j; ?>" id="image<?php echo $j; ?>" style="display:none" class="form-control input-sm" type="file">
                                	<!--<i class="fa fa-spinner fa-pulse" style="display:none;position: absolute;top: 44px; left: 63px;" id="image<?php echo $j; ?>Lod"></i>-->
                                    <!--<div style="position: absolute;top: 40px;left: 50px;color: red;display: none;font-weight: bold;" id="image<?php echo $j; ?>Lod">-->
                                    <div style="position: absolute;top: 79px;font-weight: bold;border: 1px solid #5AD0B6;width: 72%;height: 8px;display: none;margin-left: 13px;" id="image<?php echo $j; ?>Lod">
  <div style="width: 0%;background-color: #5AD0B6;height: 8px;" id="image<?php echo $j; ?>bar">
  </div>
  <div style="width: 60px;height: 23px;float: left;color: #fff;font-size: 11px;text-align: left;margin-top: -1px;" id="image<?php echo $j; ?>image_size">10.0 MB</div>
  <div style="width: 40px;height: 23px;float: right;margin-top: 1px;text-align: center;color: #fff;margin-top: -1px;font-size: 11px;" id="image<?php echo $j; ?>perimg"></div>
</div>
                                    <!--</div>-->
                                	<input type="hidden" class="imageUrl" name="image<?php echo $j; ?>Url" id="image<?php echo $j; ?>Url" value="" />
                                	<input type="hidden" name="image<?php echo $j; ?>Id" id="image<?php echo $j; ?>Id" value="" />
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    </div>
                     <div class="row">
                    
                      
                        </div>
                        
                        
                         <div class="col-md-12" style="margin-top:10px;" id="a_c">
                           <div class="form-group">
                              <div class="col-md-3">
                                <label class="">Cards Accepted</label>
                                </div>
                                <div class="col-md-9" style="padding-left: 10px;">
                                    <div class="" style="">
                                       <!-- <select name="accept_card" id="accept_card" class="form-control input-sm" style="width: 70px;float: left;">
                                            <option selected value=" ">-</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>-->
                                        <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="ac">
                                        <input type="checkbox" data-toggle="switch" checked/>
                                        <input type="hidden" id="accept_card" name="accept_card" value="1" />
                                        </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px;" id="n_v">
                           <div class="form-group">
                              <div class="col-md-3">
                                <label class="">Serve Non Vegetarian Food</label>
                                </div>
                                <div class="col-md-9" style="padding-left: 10px;">
                                    <div class="" style="">
                                        <!--<select name="veg_nonveg" id="veg_nonveg" class="form-control input-sm" style="width: 70px;float: left;">
                                            <option selected value=" ">-</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>-->
                                        <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="nv">
                                        <input type="checkbox" data-toggle="switch" />
                                        <input type="hidden" id="veg_nonveg" name="veg_nonveg" value="0" />
                                        </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px;" id="b_a">
                           <div class="form-group">
                              	<div class="col-md-3">
                                <label class="">Bar Available</label>
                                </div>
                                <div class="col-md-9" style="padding-left: 10px;">
                                    <div class="" style="">
                                        <!--<select name="bar_available" id="bar_available" class="form-control input-sm" style="width: 70px;float: left;">
                                            <option selected value=" ">-</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>-->
                                        <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="bar">
                                        <input type="checkbox" data-toggle="switch" />
                                        <input type="hidden" id="bar_available" name="bar_available" value="0" />
                                        </div>
                                  	</div>
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" id="lat" name="lat" value="" />
                        <input type="hidden" id="lng" name="lng" value="" />
                        <input type="hidden" id="lat_lng" name="lat_lng" value="0" />
                        <input type="hidden" id="imagecountvalues" name="imagecountvalues" value="0" />
                    </div>
                    </section>
                                 
               </div>
			</form>
            <img src="<?php echo APP_IMAGES.'spinner-gif-red.gif' ?>" id="load_img" style="right: 90px;position: absolute;bottom: 47px;height: 20px;">
            			</div>
            			</div>
                      </section>
                  </div>
              </div>
          </section>
      </section>
</section>      
<?php include(APP_VIEW.'includes/footer.php');?>
<?php include(APP_VIEW.'includes/bottom.php');?>

<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-multiselect.js"></script>


<!--<script type="text/javascript" src="<?php echo APP_CRM_BS; ?>js/jquery.timepicker.js"></script>-->
<script type="text/javascript" src="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>



<script src="<?php echo APP_CRM_BS; ?>js/jquery-steps.js"></script>
<script src="http://thevectorlab.net/flatlab/js/bootstrap-switch.js"></script>
<script>
function onReady(callback) 
{
	var intervalID = window.setInterval(checkReady, 1000);
    function checkReady() 
	{
        if (document.getElementsByTagName('body')[0] !== undefined) 
		{
            window.clearInterval(intervalID);
            callback.call(this);
        }
    }
}

function show(id, value) {
    document.getElementById(id).style.display = value ? 'block' : 'none';
}
function doSomethingBasedOnResponse(value){
    if(value){
        alert('yes');
    }else{
        alert('no');
		alert('Please Enter vaild City or Address');
		$('#city').addClass("error");
		$('#address').addClass("error");
		
    }
}
onReady(function () {
    show('page', true);
    show('loading', false);
});     
           
$(function ()
{
	
	
	
	var cnt = <?php echo $cnt; ?>;
	var form = $("#example-form");
	var data = false;
	$("#load_img").css("display","none");
	form.children("div").steps({
		headerTag: "h2",
		bodyTag: "section",
		transitionEffect: "fade",
		stepsOrientation: "vertical",
		transitionEffect : 1,
		enableAllSteps : true, 
		startIndex: <?php echo $_SESSION['app_user']['mp_details_step']; ?>,
		labels: 
		{
			next: "CONTINUE",
			finish:"All Done!"
		},
			/*
				onStepChanged: function (event, currentIndex, priorIndex) {
				var nextStep = '#wizard #wizard-p-' + currentIndex;
				var totalHeight = 0;
				$(nextStep).children().each(function () {
					totalHeight += $(this).outerHeight(true);
				});
				$(nextStep).parent().animate({ height: totalHeight }, 200);
			},*/
		onStepChanging: function (e, currentIndex, newIndex) {
			
			
			if(currentIndex==0)
			{
				
				//$('#example-form .content').css('min-height', '50em');
				var business 		 = $('#business').val();
				var business_contact = $('#business_contact').val();
				var city 			 = $('#city').val();
				var address 		 = $('#address').val();
				var web_url 		 = $('#web_url').val();
				var contact 		 = $('#contact').val();
				var person 			 = $('#person').val();
				var designation 	 = $('#designation').val();
				/*var business_contact = $('#business_contact').val();
				var state = $('#state').val();
				var country = $('#country').val();
				var stateName = $('#state option:selected').data('state');*/
				if(business=="")
				{
					$('#business').addClass('error');
					//$('#country').removeClass('error');
					//$('#state').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					$('#web_url').removeClass('error');
					//$('#business_contact').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					$('#business_contact').removeClass('error');
					data = false;
				}
				if(business_contact=="")
				{
					$('#business').removeClass('error');
					$('#business_contact').addClass('error');
					
					//$('#country').removeClass('error');
					//$('#state').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					$('#web_url').removeClass('error');
					//$('#business_contact').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					data = false;
				}
				/*else if(country=="")
				{
					$('#business').removeClass('error');
					$('#country').addClass('error');
					$('#state').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					$('#business_contact').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					data = false;
				}*/
				/*else if(state=="")
				{
					$('#business').removeClass('error');
					$('#country').removeClass('error');
					$('#state').addClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					$('#business_contact').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					data = false;
				}*/
				else if(city=="")
				{
					$('#business').removeClass('error');
					$('#business_contact').removeClass('error');
					//$('#country').removeClass('error');
					//$('#state').removeClass('error');
					$('#city').addClass('error');
					$('#address').removeClass('error');
					//$('#business_contact').removeClass('error');
					$('#web_url').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					data = false;
				}
				else if(address=="")
				{
					$('#business').removeClass('error');
					$('#business_contact').removeClass('error');
					//$('#country').removeClass('error');
					//$('#state').removeClass('error');
					$('#city').removeClass('error');
					$('#address').addClass('error');
					//$('#business_contact').removeClass('error');
					$('#web_url').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					data = false;
				}
				/*else if((business_contact=="")||(isNaN(business_contact)))
				{
					$('#business').removeClass('error');
					$('#country').removeClass('error');
					$('#state').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					$('#business_contact').addClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					data = false;
				}*/
				/*else if(web_url=="")
				{
					$('#business').removeClass('error');
					
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					$('#web_url').addClass('error');
					data = false;
				}*/
				else if(person=="")
				{
					$('#business').removeClass('error');
					$('#business_contact').removeClass('error');
					//$('#country').removeClass('error');
					//$('#state').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					//$('#business_contact').removeClass('error');
					$('#person').addClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
					$('#web_url').removeClass('error');
					data = false;
				}
				else if((contact=="")||(isNaN(contact)))
				{
					$('#business').removeClass('error');
					$('#business_contact').removeClass('error');
					//$('#country').removeClass('error');
					//$('#state').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					//$('#business_contact').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').addClass('error');
					$('#designation').removeClass('error');
					$('#web_url').removeClass('error');
					data = false;
				}
				else if(designation=="")
				{
					$('#business').removeClass('error');
					//$('#country').removeClass('error');
					//$('#state').removeClass('error');
					$('#business_contact').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					//$('#business_contact').removeClass('error');
					$('#web_url').removeClass('error');
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').addClass('error');
					data = false;
				}
				else
				{
					
					data = true;
					$('#business').removeClass('error');
					$('#business_contact').removeClass('error');
					$('#city').removeClass('error');
					$('#address').removeClass('error');
					$('#web_url').removeClass('error');
					var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
					if(web_url != ""){
						if (!re.test(web_url)) { 
							alert("Please Enter Correct Website Url");
							$('#web_url').addClass("error");
							return false;
						}
					}
					var lat	=	$("#lat").val();
					if(lat	==	'')
					{
						alert("Please Enter Correct Address or city");
						$('#address').focus();
						return false;
					}
					$('#person').removeClass('error');
					$('#contact').removeClass('error');
					$('#designation').removeClass('error');
						
					//$("#business_contact").keyup(function(){
						var v	=	$("#business_contact").val();
						if(!$.isNumeric(v))
						{
							alert('Please enter numric value in Businnes contact');
							$("#business_contact").focus();
							$("#business_contact").val('');
							return false;
						}
						
						
					//});	
						
					var form1 = $('#example-form')[0];
					var formData = new FormData(form1);
					$.ajax({
								url: '<?php echo APP_URL; ?>admin_mp/profile/step1',
								type: 'POST',
								data: formData,
								processData: false,
								contentType: false,
								success: function (data) {
									console.log(data);
									$("#form_title").html('A Few More Details Please');
									$("html, body").animate({ scrollTop: 0 }, 600);
									$('#example-form .content').css('min-height', '50em');
								},
								error: function (data) {
								}
						});							
				}
			}
			else if((currentIndex==1)&&(newIndex==0))
			{
				//alert('2');
				data = true;
				$('#example-form .content').css('min-height', '50em');
				$("#form_title").html(' Please Give the Details of Your Business');
			}
			// to go next from second step
			else if((currentIndex==1)&&(newIndex==2))
			{
				//alert('two step')
				/*
				data = false;
				var country = $('#country').val();
				
				if(country==2)
				{
					$('#labelIfsc').html('Routing Number*');	
				}
				else
				{
					$('#labelIfsc').html('Bank IFSC Code*');	
				}
				
				var description = $('#description').val();
				var category = $('#category').val();
				var sub_cat = $('#sub_category').val();
				var count = ($('.imageUrl[value!=""]').length);
				$('#example-form .content').css('min-height', '35em');
				
				if(description=="")
				{
					$('#description').addClass('error');
					$('#category').removeClass('error');
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					data = false;
				}
				else if(category=="")
				{
					$('#description').removeClass('error');
					$('#category').addClass('error');
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					data = false;
				}
				else if(sub_cat==null)
				{
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					$('.multiselect').addClass('error');
					data = false;
				}
				else if(count<4)
				{
					alert('Please upload all 4 images');
					$('#example-form .content').css('min-height', '50em');
					data = false;
				}
				else
				{
					data = true;
					$('#description').removeClass('error');
					$('#category').removeClass('error');	
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					
					var form1 = $('#example-form')[0];
					var formData = new FormData(form1);
					$.ajax({
							url: '<?php //echo APP_URL; ?>admin_mp/profile/step2',
							type: 'POST',
							data: formData,
							processData: false,
							contentType: false,
							success: function (data) {
								console.log(data);
							},
							error: function (data) {
							}
				   });					
				}
			*/
			$('#example-form .content').css('min-height', '50em');
			}
			if(data)
			{
				return true;
			}
		},
		onFinishing: function (event, currentIndex) {
			$('#example-form .content').css('min-height', '50em');
			data = false;
			//var country = $('#country').val();
			/*if(country==2)
			{
				$('#labelIfsc').html('Routing Number*');	
			}
			else
			{
				$('#labelIfsc').html('Bank IFSC Code*');	
			}*/
			var description 	= $('#description').val();
			var category 		= $('#category').val();
			//var sub_cat 		= $('#sub_category').val();
			var count 			= ($('.imageUrl[value!=""]').length);
			var opentime		=	$("#opentime").val();
			//var opentime_ampm	=	$("#opentime_ampm").val();
			var closetime		=	$("#closetime").val();
			//var closetime_ampm	=	$("#closetime_ampm").val();
			//var accpet_card		=	$("#accpet_card").val();
			//var veg_nonveg		=	$("#veg_nonveg").val();
			//var bar_available	=	$("#bar_available").val();
				
				
				if(category=="")
				{
					$('#description').removeClass('error');
					$('#category').addClass('error');
					//$('#sub_category').removeClass('error');
					//$('.multiselect').removeClass('error');
					$('#opentime').removeClass('error');
					//$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					//$('#closetime_ampm').removeClass('error');
					//$('#accpet_card').removeClass('error');
					//$('#veg_nonveg').removeClass('error');
					//$('#bar_available').removeClass('error');
					data = false;
				}
				else if(description=="")
				{
					$('#description').addClass('error');
					$('#category').removeClass('error');
					//$('#sub_category').removeClass('error');
					//$('.multiselect').removeClass('error');
					$('#opentime').removeClass('error');
					//$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					//$('#closetime_ampm').removeClass('error');
					//$('#accpet_card').removeClass('error');
					//$('#veg_nonveg').removeClass('error');
					//$('#bar_available').removeClass('error');
					data = false;
				}
				else if(opentime=="")
				{
					$('#opentime').addClass('error');
					//$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					//$('#closetime_ampm').removeClass('error');
					//$('#accpet_card').removeClass('error');
					//$('#veg_nonveg').removeClass('error');
					//$('#bar_available').removeClass('error');
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					//$('#sub_category').removeClass('error');
					//$('.multiselect').removeClass('error');
					data = false;
				}
				else if(closetime=="")
				{
					$('#opentime').removeClass('error');
					//$('#opentime_ampm').removeClass('error');
					$('#closetime').addClass('error');
					//$('#closetime_ampm').removeClass('error');
					//$('#accpet_card').removeClass('error');
					//$('#veg_nonveg').removeClass('error');
					//$('#bar_available').removeClass('error');
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					//$('#sub_category').removeClass('error');
					//$('.multiselect').removeClass('error');
					data = false;
				}
				/*else if(count<4)
				{
					alert('Please upload all 4 images');
					$('#example-form .content').css('min-height', '50em');
					data = false;
				}*/
				/*else if(sub_cat==null)
				{
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					$('.multiselect').addClass('error');
					$('#opentime').removeClass('error');
					$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					$('#closetime_ampm').removeClass('error');
					$('#accpet_card').removeClass('error');
					$('#veg_nonveg').removeClass('error');
					$('#bar_available').removeClass('error');
					data = false;
				}*/
				
				
				
				/*else if(opentime_ampm==" ")
				{
					$('#opentime').removeClass('error');
					$('#opentime_ampm').addClass('error');
					$('#closetime').removeClass('error');
					$('#closetime_ampm').removeClass('error');
					$('#accpet_card').removeClass('error');
					$('#veg_nonveg').removeClass('error');
					$('#bar_available').removeClass('error');
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					data = false;
				}*/
				
				/*else if(closetime_ampm==" ")
				{
					$('#opentime').removeClass('error');
					$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					$('#closetime_ampm').addClass('error');
					$('#accpet_card').removeClass('error');
					$('#veg_nonveg').removeClass('error');
					$('#bar_available').removeClass('error');
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					data = false;
				}*/
				/*else if(accpet_card==" ")
				{
					$('#opentime').removeClass('error');
					$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					$('#closetime_ampm').removeClass('error');
					$('#accpet_card').addClass('error');
					$('#veg_nonveg').removeClass('error');
					$('#bar_available').removeClass('error');
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					data = false;
				}
				else if(veg_nonveg==" ")
				{
					$('#opentime').removeClass('error');
					$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					$('#closetime_ampm').removeClass('error');
					$('#accpet_card').removeClass('error');
					$('#veg_nonveg').addClass('error');
					$('#bar_available').removeClass('error');
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					data = false;
				}
				else if(bar_available==" ")
				{
					$('#opentime').removeClass('error');
					$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					$('#closetime_ampm').removeClass('error');
					$('#accpet_card').removeClass('error');
					$('#veg_nonveg').removeClass('error');
					$('#bar_available').addClass('error');
					
					$('#description').removeClass('error');
					$('#category').removeClass('error');
					$('#sub_category').removeClass('error');
					$('.multiselect').removeClass('error');
					data = false;
				}*/
				else
				{
					data = true;
					
					$('#imagecountvalues').val(count);
					
					$('#description').removeClass('error');
					$('#category').removeClass('error');	
					//$('#sub_category').removeClass('error');
					//$('.multiselect').removeClass('error');
					$('#opentime').removeClass('error');
					//$('#opentime_ampm').removeClass('error');
					$('#closetime').removeClass('error');
					//$('#closetime_ampm').removeClass('error');
					//$('#accpet_card').removeClass('error');
					//$('#veg_nonveg').removeClass('error');
					//$('#bar_available').removeClass('error');
					$('.wizard ul li a').css('pointer-events', 'none');
					$('.wizard ul li a').css('background', '#ff7272');
					   
					
					var form1 = $('#example-form')[0];
					var formData = new FormData(form1);
					
					$("#load_img").css("display","block");
					$("#load_img").css("display","block");
					$.ajax({
							url: '<?php echo APP_URL; ?>admin_mp/profile/step2',
							type: 'POST',
							data: formData,
							processData: false,
							contentType: false,
							success: function (data) {
								
								$('#page').css('opacity', '1.0');
								$("#load_img").css("display","none");
								if(data=="true")
								{
									//$('.alert-success').show();
									$('.alert-warning').hide();
									setInterval(function() {ajaxd();}, 500);
									//$("html, body").animate({ scrollTop: 0 }, 600);
								}
								else
								{
									$('.alert-success').hide();
									$('.alert-warning').show();
								}
								console.log(data);
							},
							error: function (data) {
							}
				   });				
				}
				if(data)
				{
					return true;
				}
		}
		
	});
	$('#sub_category').multiselect();					
	$("#category").change(function()
	{
		var id = $(this).val();
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/profile/getSubcategoryList',
			type: 'POST',
			data: {id : id},
			success: function (data) {
				$("#sub_category").empty();
				$("#sub_category").html(data);
				$("#sub_category").attr('multiple', 'multiple'); 
				$("#sub_category").multiselect('rebuild');
			},
			error: function (data) {
			}
		 });
	});		
	$("#country").change(function()
	{
		var id = $(this).val();
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/profile/getStateList',
			type: 'POST',
			data: {id : id},
			success: function (data) {
				$("#state").empty();
				$("#state").html(data);
			},
			error: function (data) {
			}
		 });
	});	
	
	
	//code for image uploading starts here
	function readURL(input, id, fileName) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(input.files[0]);
			fileSize = Math.round(input.files[0].size);
			var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
			if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png")
			{
				if(fileSize>10485760)
				{
					alert("Please select image less than 10 MB!");
					$('#'+id).val('');
					return false;
				}
				else
				{
					reader.onload = function (e) {
					var image  = new Image();
					image.src = e.target.result;
					image.onload = function () {
						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;
							/*if (height < 500 || width < 800) {
								//alert("Height should not less than 500PX and Width should not less than 800PX.");
								//$('#'+id).val('');
								//return false;
							}
							else
							{
								*/
								$('#'+id+'pic').css('opacity', '0.5');
								$('#'+id+'Lod').show();
								$('#'+id+'pic').attr('src', e.target.result);
								$('#'+id+'pic').css("border","18px solid #D9DADF");
								var form1 = $('#example-form')[0];
								var formData = new FormData(form1);
								
								formData.append("image", id);
								$.ajax({
										
										xhr: function() {
											var xhr = new window.XMLHttpRequest();
											xhr.upload.addEventListener("progress", function(evt) {
											  if (evt.lengthComputable) {
												var percentComplete = evt.loaded / evt.total;
												percentComplete = parseInt(percentComplete * 100);
												//$('#'+id+'pic').attr('src', '');
												//$('#'+id+'Lod').html(percentComplete+'%');
												//alert(percentComplete);
												$('#'+id+'bar').css("width",percentComplete+'%');
												$('#'+id+'perimg').html(percentComplete+'%');
												var sizeInMB = (fileSize / (1024*1024)).toFixed(2);
												$('#'+id+'image_size').html(sizeInMB+' MB');
												console.log(percentComplete);
												$( "#m_submit" ).prop( "disabled", true );
												if (percentComplete === 100) {
													
												}
										
											  }
											}, false);
											return xhr;
											
										  },
										
										url: '<?php echo APP_URL; ?>admin_mp/createDeal/deal_image_show_profile',
										type: 'POST',
										data: formData,
										processData: false,
										contentType: false,
										success: function (data) {
											
											console.log(data);
											var obj = JSON.parse(data);
											console.log(obj);
											//alert('profile.php(1034)->'+data);
											//alert(id);
											////alert(obj.imageName);
											//alert('<?php //echo APP_CRM_UPLOADS_PATH; ?>');
											$('#'+id+'Lod').hide();
											$('#'+id+'pic').css('opacity', '1.0');
											//$('#'+id+'pic').attr('src', '<?php echo APP_CRM_UPLOADS_PATH; ?>'+obj.imageName);
											$('#'+id+'Id').val(obj.id);
											$('#'+id+'btn').show();
											$('#'+id+'Url').val(obj.imageName);
											$( "#m_submit" ).removeAttr("disabled");
											$('#'+id+'pic').css("border","none"); 
										},
										error: function (data) {
										}
							   });	
								
							/*}*/
						}
					}
				}
			}
			else
			{
				alert("Please upload jpg, png or gif format");
				$('#'+id).val('');
				return false;
			}
		}
	}
	$("input:file").change(function(){
		var id = $(this).attr('id');
		var fileName = $('#'+id).val();
		//alert('function call==='+fileName);
		readURL(this, id, fileName);
	});	
	
	$('.btnCross').click(function()
	{
		var id = $(this).attr('id');
		id = id.slice(0, -3);
		$(this).hide();
		var deleteImg = $('#'+id+'Id').val();
		var deleteUrl = $('#'+id+'Url').val();
		$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/profile/deletePic',
				type: 'POST',
				data: {'deleteImg' : deleteImg, 'deleteUrl' : deleteUrl},
				success: function (data) {
					
					$('#'+id+'pic').attr('src', "<?php echo APP_IMAGES; ?>add_image.jpg");
					$('#'+id).val('');
					$('#'+id+'Id').val('');
					$('#'+id+'Lod').hide();
				},
				error: function (data) {
				}
	   });	
	});
	//code for image uploading ends here
	
	if(<?php echo $_SESSION['app_user']['mp_details_step']; ?>==1)
	{
		$('#example-form .content').css('min-height', '50em');
	}

});

function ajaxd()
{  
	top.location = "<?php echo APP_URL; ?>admin_mp/dashboard";
}
</script>
<script>
$(function()
{
	$('.timepicker-default').timepicker();
	
	
	$('#opentime').timepicker();
	$('#closetime').timepicker();
	$("[data-toggle='switch']").wrap('<div class="switch"/>').parent().bootstrapSwitch();
	$("#ac").click(function(){
		var id	=	$("#accept_card").val();
		if(id	==	1)
		{
			$("#accept_card").val(0);
		}else{
			$("#accept_card").val(1);
		}
	});
	$("#nv").click(function(){
		var id	=	$("#veg_nonveg").val();
		if(id	==	1)
		{
			$("#veg_nonveg").val(0);
		}else{
			$("#veg_nonveg").val(1);
		}
	});
	$("#bar").click(function(){
		var id	=	$("#bar_available").val();
		if(id	==	1)
		{
			$("#bar_available").val(0);
		}else{
			$("#bar_available").val(1);
		}
	});
	$('#a_c').hide();
	$('#n_v').hide();
	$('#b_a').hide();
	$("#category").change(function()
	{
		$('.switch-left').html("YES");
		$('.switch-right').html("NO");
		var id	=	$(this).val();
		if(id	==	2){
			$('#a_c').show();
			$('#n_v').show();
			$('#b_a').show();
		}else{
			$('#a_c').show();
			$('#n_v').hide();
			$('#b_a').hide();
		}
	});
	/*$("#business_contact").keyup(function(){
		var v	=	$(this).val();
		if(!$.isNumeric(v))
		{
			alert('Please enter numric value in Businnes contact');
			$("#business_contact").focus();
			$("#business_contact").val('');
			return false;
		}
	});*/
	$("#address").blur(function(){
		var address	=	$("#address").val();
		var city	=	$("#city").val();
		var add			=	address+','+city;
		$.ajax({
		type: "GET",
		dataType: "json",
		url: "http://maps.googleapis.com/maps/api/geocode/json",
		data: {'address': add,'sensor':false},
		success: function(data){
				if(data.results.length){
					var lat	=	data.results[0].geometry.location.lat;
					var lng	=	data.results[0].geometry.location.lng
					$("#lat").val(lat);
					$("#lng").val(lng);
					$("#lat_lng").val();
				}else{
			   }
			}
		});
	});
	
	
});
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
