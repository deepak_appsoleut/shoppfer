<div class="row">
      	<div class="col-xs-12">
        <div class="box">
        	<div class="box-body">
            	<table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Booking Date</th>
                  <th>Deal Name</th>
                  <th>Hotel Name</th>
                  <th>Amount</th>
                  <th>My Earning</th>
                  <th>Redeemed Date</th>
                </tr>
                </thead>
               <tbody>
                <?php 
				for($i=0; $i<count($data); $i++) {
					?>
					<tr role="row">
                    <td><?php echo $data[$i]["booking_id"]; ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data[$i]["booking_created_on"])); ?></td>
                    <td><?php echo $data[$i]["deal_name"]; ?></td>
                    <td><?php echo $data[$i]["hotel_details_name"]; ?></td>
                    <td><?php echo $data[$i]["booking_amount"]; ?></td>
                    <td><?php echo $data[$i]["booking_my_earning"]; ?></td>
                    <td><?php if($data[$i]["booking_redeem_date"]!=NULL)
					{
						echo date('d-m-Y', strtotime($data[$i]["booking_redeem_date"]));
					} ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
            </div>
        </div>
        </div>
      </div>
      <script>
       $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
      </script>