<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SHOPPFER | Edit Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>ionslider/ion.rangeSlider.css">
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>ionslider/ion.rangeSlider.skinNice.css">
  <link rel="stylesheet" type="text/css" href="http://thevectorlab.net/flatlab/assets/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo APP_CSS; ?>toastr.css">
  <style>
.marvel-device{display:inline-block;position:relative;box-sizing:content-box !important}.marvel-device .screen{width:100%;position:relative;height:100%;color:#000;z-index:2;text-align:center;display:block;/*-webkit-border-radius:1px;border-radius:1px;-webkit-box-shadow:0 0 0 3px #bcbcbc;box-shadow:0 0 0 3px #bcbcbc*/}.marvel-device .top-bar,.marvel-device .bottom-bar{height:3px;background:black;width:100%;display:block}.marvel-device .middle-bar{width:3px;height:4px;top:0px;left:90px;background:black;position:absolute}.marvel-device.iphone6{width:375px;height:667px;padding:105px 24px;background:#d9dbdc;-webkit-border-radius:56px;border-radius:56px;-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.2);box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.2)}.marvel-device.iphone6:before{width:calc(100% - 12px);height:calc(100% - 12px);position:absolute;top:6px;content:'';left:6px;-webkit-border-radius:50px;border-radius:50px;background:#f8f8f8;z-index:1}.marvel-device.iphone6:after{width:calc(100% - 16px);height:calc(100% - 16px);position:absolute;top:8px;content:'';left:8px;-webkit-border-radius:48px;border-radius:48px;-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #fff;box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #fff;z-index:2}.marvel-device.iphone6 .home{-webkit-border-radius:100%;border-radius:100%;width:68px;height:68px;position:absolute;left:50%;margin-left:-34px;bottom:22px;z-index:3;background:#303233;background:-moz-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:-webkit-gradient(linear, left top, right bottom, color-stop(0%, #303233), color-stop(50%, #b5b7b9), color-stop(69%, #f0f2f2), color-stop(100%, #303233));background:-webkit-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:-o-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:-ms-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:linear-gradient(135deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#303233', endColorstr='#303233',GradientType=1 )}.marvel-device.iphone6 .home:before{background:#f8f8f8;position:absolute;content:'';-webkit-border-radius:100%;border-radius:100%;width:calc(100% - 8px);height:calc(100% - 8px);top:4px;left:4px}.marvel-device.iphone6 .top-bar{height:14px;background:#bfbfc0;position:absolute;top:68px;left:0}.marvel-device.iphone6 .bottom-bar{height:14px;background:#bfbfc0;position:absolute;bottom:68px;left:0}.marvel-device.iphone6 .sleep{position:absolute;top:190px;right:-4px;width:4px;height:66px;-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px;background:#d9dbdc}.marvel-device.iphone6 .volume{position:absolute;left:-4px;top:188px;z-index:0;height:66px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:#d9dbdc}.marvel-device.iphone6 .volume:before{position:absolute;left:2px;top:-78px;height:40px;width:2px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone6 .volume:after{position:absolute;left:0px;top:82px;height:66px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone6 .camera{background:#3c3d3d;width:12px;height:12px;position:absolute;top:24px;left:50%;margin-left:-6px;-webkit-border-radius:100%;border-radius:100%;z-index:3}.marvel-device.iphone6 .sensor{background:#3c3d3d;width:16px;height:16px;position:absolute;top:49px;left:134px;z-index:3;-webkit-border-radius:100%;border-radius:100%}.marvel-device.iphone6 .speaker{background:#292728;width:70px;height:6px;position:absolute;top:54px;left:50%;margin-left:-35px;-webkit-border-radius:6px;border-radius:6px;z-index:3}.marvel-device.iphone6.gold{background:#f9e7d3}.marvel-device.iphone6.gold .top-bar,.marvel-device.iphone6.gold .bottom-bar{background:white}.marvel-device.iphone6.gold .sleep,.marvel-device.iphone6.gold .volume{background:#f9e7d3}.marvel-device.iphone6.gold .home{background:#cebba9;background:-moz-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:-webkit-gradient(linear, left top, right bottom, color-stop(0%, #cebba9), color-stop(50%, #f9e7d3), color-stop(100%, #cebba9));background:-webkit-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:-o-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:-ms-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:linear-gradient(135deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#cebba9', endColorstr='#cebba9',GradientType=1 )}.marvel-device.iphone6.black{background:#464646;-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.7);box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.7)}.marvel-device.iphone6.black:before{background:#080808}.marvel-device.iphone6.black:after{-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #212121;box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #212121}.marvel-device.iphone6.black .top-bar,.marvel-device.iphone6.black .bottom-bar{background:#212121}.marvel-device.iphone6.black .volume,.marvel-device.iphone6.black .sleep{background:#464646}.marvel-device.iphone6.black .camera{background:#080808}.marvel-device.iphone6.black .home{background:#080808;background:-moz-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:-webkit-gradient(linear, left top, right bottom, color-stop(0%, #080808), color-stop(50%, #464646), color-stop(100%, #080808));background:-webkit-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:-o-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:-ms-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:linear-gradient(135deg, #080808 0%, #464646 50%, #080808 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#080808', endColorstr='#080808',GradientType=1 )}.marvel-device.iphone6.black .home:before{background:#080808}.marvel-device.iphone6.landscape{padding:24px 105px;height:375px;width:667px}.marvel-device.iphone6.landscape .sleep{top:100%;-webkit-border-radius:0px 0px 2px 2px;border-radius:0px 0px 2px 2px;right:190px;height:4px;width:66px}.marvel-device.iphone6.landscape .volume{width:66px;height:4px;top:-4px;left:calc(100% - 188px - 66px);-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone6.landscape .volume:before{width:40px;height:2px;top:2px;right:-78px;left:auto;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone6.landscape .volume:after{left:-82px;width:66px;height:4px;top:0;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone6.landscape .top-bar{width:14px;height:100%;left:calc(100% - 68px -  14px);top:0}.marvel-device.iphone6.landscape .bottom-bar{width:14px;height:100%;left:68px;top:0}.marvel-device.iphone6.landscape .home{top:50%;margin-top:-34px;margin-left:0;left:22px}.marvel-device.iphone6.landscape .sensor{top:134px;left:calc(100% - 49px - 16px)}.marvel-device.iphone6.landscape .speaker{height:70px;width:6px;left:calc(100% - 54px - 6px);top:50%;margin-left:0px;margin-top:-35px}.marvel-device.iphone6.landscape .camera{left:calc(100% - 32px);top:50%;margin-left:0px;margin-top:-5px}.marvel-device.iphone6plus{width:414px;height:736px;padding:112px 26px;background:#d9dbdc;-webkit-border-radius:56px;border-radius:56px;-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.2);box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.2)}.marvel-device.iphone6plus:before{width:calc(100% - 12px);height:calc(100% - 12px);position:absolute;top:6px;content:'';left:6px;-webkit-border-radius:50px;border-radius:50px;background:#f8f8f8;z-index:1}.marvel-device.iphone6plus:after{width:calc(100% - 16px);height:calc(100% - 16px);position:absolute;top:8px;content:'';left:8px;-webkit-border-radius:48px;border-radius:48px;-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #fff;box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #fff;z-index:2}.marvel-device.iphone6plus .home{-webkit-border-radius:100%;border-radius:100%;width:68px;height:68px;position:absolute;left:50%;margin-left:-34px;bottom:24px;z-index:3;background:#303233;background:-moz-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:-webkit-gradient(linear, left top, right bottom, color-stop(0%, #303233), color-stop(50%, #b5b7b9), color-stop(69%, #f0f2f2), color-stop(100%, #303233));background:-webkit-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:-o-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:-ms-linear-gradient(-45deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);background:linear-gradient(135deg, #303233 0%, #b5b7b9 50%, #f0f2f2 69%, #303233 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#303233', endColorstr='#303233',GradientType=1 )}.marvel-device.iphone6plus .home:before{background:#f8f8f8;position:absolute;content:'';-webkit-border-radius:100%;border-radius:100%;width:calc(100% - 8px);height:calc(100% - 8px);top:4px;left:4px}.marvel-device.iphone6plus .top-bar{height:14px;background:#bfbfc0;position:absolute;top:68px;left:0}.marvel-device.iphone6plus .bottom-bar{height:14px;background:#bfbfc0;position:absolute;bottom:68px;left:0}.marvel-device.iphone6plus .sleep{position:absolute;top:190px;right:-4px;width:4px;height:66px;-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px;background:#d9dbdc}.marvel-device.iphone6plus .volume{position:absolute;left:-4px;top:188px;z-index:0;height:66px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:#d9dbdc}.marvel-device.iphone6plus .volume:before{position:absolute;left:2px;top:-78px;height:40px;width:2px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone6plus .volume:after{position:absolute;left:0px;top:82px;height:66px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone6plus .camera{background:#3c3d3d;width:12px;height:12px;position:absolute;top:29px;left:50%;margin-left:-6px;-webkit-border-radius:100%;border-radius:100%;z-index:3}.marvel-device.iphone6plus .sensor{background:#3c3d3d;width:16px;height:16px;position:absolute;top:54px;left:154px;z-index:3;-webkit-border-radius:100%;border-radius:100%}.marvel-device.iphone6plus .speaker{background:#292728;width:70px;height:6px;position:absolute;top:59px;left:50%;margin-left:-35px;-webkit-border-radius:6px;border-radius:6px;z-index:3}.marvel-device.iphone6plus.gold{background:#f9e7d3}.marvel-device.iphone6plus.gold .top-bar,.marvel-device.iphone6plus.gold .bottom-bar{background:white}.marvel-device.iphone6plus.gold .sleep,.marvel-device.iphone6plus.gold .volume{background:#f9e7d3}.marvel-device.iphone6plus.gold .home{background:#cebba9;background:-moz-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:-webkit-gradient(linear, left top, right bottom, color-stop(0%, #cebba9), color-stop(50%, #f9e7d3), color-stop(100%, #cebba9));background:-webkit-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:-o-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:-ms-linear-gradient(-45deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);background:linear-gradient(135deg, #cebba9 0%, #f9e7d3 50%, #cebba9 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#cebba9', endColorstr='#cebba9',GradientType=1 )}.marvel-device.iphone6plus.black{background:#464646;-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.7);box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.7)}.marvel-device.iphone6plus.black:before{background:#080808}.marvel-device.iphone6plus.black:after{-webkit-box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #212121;box-shadow:inset 0 0 3px 0 rgba(0,0,0,0.1),inset 0 0 6px 3px #212121}.marvel-device.iphone6plus.black .top-bar,.marvel-device.iphone6plus.black .bottom-bar{background:#212121}.marvel-device.iphone6plus.black .volume,.marvel-device.iphone6plus.black .sleep{background:#464646}.marvel-device.iphone6plus.black .camera{background:#080808}.marvel-device.iphone6plus.black .home{background:#080808;background:-moz-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:-webkit-gradient(linear, left top, right bottom, color-stop(0%, #080808), color-stop(50%, #464646), color-stop(100%, #080808));background:-webkit-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:-o-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:-ms-linear-gradient(-45deg, #080808 0%, #464646 50%, #080808 100%);background:linear-gradient(135deg, #080808 0%, #464646 50%, #080808 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#080808', endColorstr='#080808',GradientType=1 )}.marvel-device.iphone6plus.black .home:before{background:#080808}.marvel-device.iphone6plus.landscape{padding:26px 112px;height:414px;width:736px}.marvel-device.iphone6plus.landscape .sleep{top:100%;-webkit-border-radius:0px 0px 2px 2px;border-radius:0px 0px 2px 2px;right:190px;height:4px;width:66px}.marvel-device.iphone6plus.landscape .volume{width:66px;height:4px;top:-4px;left:calc(100% - 188px - 66px);-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone6plus.landscape .volume:before{width:40px;height:2px;top:2px;right:-78px;left:auto;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone6plus.landscape .volume:after{left:-82px;width:66px;height:4px;top:0;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone6plus.landscape .top-bar{width:14px;height:100%;left:calc(100% - 68px -  14px);top:0}.marvel-device.iphone6plus.landscape .bottom-bar{width:14px;height:100%;left:68px;top:0}.marvel-device.iphone6plus.landscape .home{top:50%;margin-top:-34px;margin-left:0;left:24px}.marvel-device.iphone6plus.landscape .sensor{top:154px;left:calc(100% - 54px - 16px)}.marvel-device.iphone6plus.landscape .speaker{height:70px;width:6px;left:calc(100% - 59px - 6px);top:50%;margin-left:0px;margin-top:-35px}.marvel-device.iphone6plus.landscape .camera{left:calc(100% - 29px);top:50%;margin-left:0px;margin-top:-5px}.marvel-device.iphone5s,.marvel-device.iphone5c{padding:105px 22px;background:#2c2b2c;width:320px;height:568px;-webkit-border-radius:50px;border-radius:50px}.marvel-device.iphone5s:before,.marvel-device.iphone5c:before{width:calc(100% - 8px);height:calc(100% - 8px);position:absolute;top:4px;content:'';left:4px;-webkit-border-radius:46px;border-radius:46px;background:#1e1e1e;z-index:1}.marvel-device.iphone5s .sleep,.marvel-device.iphone5c .sleep{position:absolute;top:-4px;right:60px;width:60px;height:4px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px;background:#282727}.marvel-device.iphone5s .volume,.marvel-device.iphone5c .volume{position:absolute;left:-4px;top:180px;z-index:0;height:27px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:#282727}.marvel-device.iphone5s .volume:before,.marvel-device.iphone5c .volume:before{position:absolute;left:0px;top:-75px;height:35px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone5s .volume:after,.marvel-device.iphone5c .volume:after{position:absolute;left:0px;bottom:-64px;height:27px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone5s .camera,.marvel-device.iphone5c .camera{background:#3c3d3d;width:10px;height:10px;position:absolute;top:32px;left:50%;margin-left:-5px;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;z-index:3}.marvel-device.iphone5s .sensor,.marvel-device.iphone5c .sensor{background:#3c3d3d;width:10px;height:10px;position:absolute;top:60px;left:160px;z-index:3;margin-left:-32px;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px}.marvel-device.iphone5s .speaker,.marvel-device.iphone5c .speaker{background:#292728;width:64px;height:10px;position:absolute;top:60px;left:50%;margin-left:-32px;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;z-index:3}.marvel-device.iphone5s.landscape,.marvel-device.iphone5c.landscape{padding:22px 105px;height:320px;width:568px}.marvel-device.iphone5s.landscape .sleep,.marvel-device.iphone5c.landscape .sleep{right:-4px;top:calc(100% - 120px);height:60px;width:4px;-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px}.marvel-device.iphone5s.landscape .volume,.marvel-device.iphone5c.landscape .volume{width:27px;height:4px;top:-4px;left:calc(100% - 180px);-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone5s.landscape .volume:before,.marvel-device.iphone5c.landscape .volume:before{width:35px;height:4px;top:0px;right:-75px;left:auto;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone5s.landscape .volume:after,.marvel-device.iphone5c.landscape .volume:after{bottom:0px;left:-64px;z-index:999;height:4px;width:27px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone5s.landscape .sensor,.marvel-device.iphone5c.landscape .sensor{top:160px;left:calc(100% - 60px);margin-left:0px;margin-top:-32px}.marvel-device.iphone5s.landscape .speaker,.marvel-device.iphone5c.landscape .speaker{height:64px;width:10px;left:calc(100% - 60px);top:50%;margin-left:0px;margin-top:-32px}.marvel-device.iphone5s.landscape .camera,.marvel-device.iphone5c.landscape .camera{left:calc(100% - 32px);top:50%;margin-left:0px;margin-top:-5px}.marvel-device.iphone5s .home{-moz-border-radius:36px;-webkit-border-radius:36px;border-radius:36px;width:68px;-webkit-box-shadow:inset 0 0 0 4px #2c2b2c;box-shadow:inset 0 0 0 4px #2c2b2c;height:68px;position:absolute;left:50%;margin-left:-34px;bottom:19px;z-index:3}.marvel-device.iphone5s .top-bar{top:70px;position:absolute;left:0}.marvel-device.iphone5s .bottom-bar{bottom:70px;position:absolute;left:0}.marvel-device.iphone5s.landscape .home{left:19px;bottom:50%;margin-bottom:-34px;margin-left:0px}.marvel-device.iphone5s.landscape .top-bar{left:70px;top:0px;width:3px;height:100%}.marvel-device.iphone5s.landscape .bottom-bar{right:70px;left:auto;bottom:0px;width:3px;height:100%}.marvel-device.iphone5s.silver{background:#bcbcbc}.marvel-device.iphone5s.silver:before{background:#fcfcfc}.marvel-device.iphone5s.silver .volume,.marvel-device.iphone5s.silver .sleep{background:#d6d6d6}.marvel-device.iphone5s.silver .top-bar,.marvel-device.iphone5s.silver .bottom-bar{background:#eaebec}.marvel-device.iphone5s.silver .home{-webkit-box-shadow:inset 0 0 0 4px #bcbcbc;box-shadow:inset 0 0 0 4px #bcbcbc}.marvel-device.iphone5s.gold{background:#f9e7d3}.marvel-device.iphone5s.gold:before{background:#fcfcfc}.marvel-device.iphone5s.gold .volume,.marvel-device.iphone5s.gold .sleep{background:#f9e7d3}.marvel-device.iphone5s.gold .top-bar,.marvel-device.iphone5s.gold .bottom-bar{background:white}.marvel-device.iphone5s.gold .home{-webkit-box-shadow:inset 0 0 0 4px #f9e7d3;box-shadow:inset 0 0 0 4px #f9e7d3}.marvel-device.iphone5c{background:white;-webkit-box-shadow:0 1px 2px 0 rgba(0,0,0,0.2);box-shadow:0 1px 2px 0 rgba(0,0,0,0.2)}.marvel-device.iphone5c .top-bar,.marvel-device.iphone5c .bottom-bar{display:none}.marvel-device.iphone5c .home{background:#242324;-moz-border-radius:36px;-webkit-border-radius:36px;border-radius:36px;width:68px;height:68px;z-index:3;position:absolute;left:50%;margin-left:-34px;bottom:19px}.marvel-device.iphone5c .home:after{width:20px;height:20px;border:1px solid rgba(255,255,255,0.1);-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;position:absolute;display:block;content:'';top:50%;left:50%;margin-top:-11px;margin-left:-11px}.marvel-device.iphone5c.landscape .home{left:19px;bottom:50%;margin-bottom:-34px;margin-left:0px}.marvel-device.iphone5c .volume,.marvel-device.iphone5c .sleep{background:#dddddd}.marvel-device.iphone5c.red{background:#f96b6c}.marvel-device.iphone5c.red .volume,.marvel-device.iphone5c.red .sleep{background:#ed5758}.marvel-device.iphone5c.yellow{background:#f2dc60}.marvel-device.iphone5c.yellow .volume,.marvel-device.iphone5c.yellow .sleep{background:#e5ce4c}.marvel-device.iphone5c.green{background:#97e563}.marvel-device.iphone5c.green .volume,.marvel-device.iphone5c.green .sleep{background:#85d94d}.marvel-device.iphone5c.blue{background:#33a2db}.marvel-device.iphone5c.blue .volume,.marvel-device.iphone5c.blue .sleep{background:#2694cd}.marvel-device.iphone4s{padding:129px 27px;width:320px;height:480px;background:#686868;-webkit-border-radius:54px;border-radius:54px}.marvel-device.iphone4s:before{content:'';width:calc(100% - 8px);height:calc(100% - 8px);position:absolute;top:4px;left:4px;z-index:1;-webkit-border-radius:50px;border-radius:50px;background:#1e1e1e}.marvel-device.iphone4s .top-bar{top:60px;position:absolute;left:0}.marvel-device.iphone4s .bottom-bar{bottom:90px;position:absolute;left:0}.marvel-device.iphone4s .camera{background:#3c3d3d;width:10px;height:10px;position:absolute;top:72px;left:134px;z-index:3;margin-left:-5px;-webkit-border-radius:100%;border-radius:100%}.marvel-device.iphone4s .speaker{background:#292728;width:64px;height:10px;position:absolute;top:72px;left:50%;z-index:3;margin-left:-32px;-webkit-border-radius:5px;border-radius:5px}.marvel-device.iphone4s .sensor{background:#292728;width:40px;height:10px;position:absolute;top:36px;left:50%;z-index:3;margin-left:-20px;-webkit-border-radius:5px;border-radius:5px}.marvel-device.iphone4s .home{background:#242324;-webkit-border-radius:100%;border-radius:100%;width:72px;height:72px;z-index:3;position:absolute;left:50%;margin-left:-36px;bottom:30px}.marvel-device.iphone4s .home:after{width:20px;height:20px;border:1px solid rgba(255,255,255,0.1);-webkit-border-radius:4px;border-radius:4px;position:absolute;display:block;content:'';top:50%;left:50%;margin-top:-11px;margin-left:-11px}.marvel-device.iphone4s .sleep{position:absolute;top:-4px;right:60px;width:60px;height:4px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px;background:#4D4D4D}.marvel-device.iphone4s .volume{position:absolute;left:-4px;top:160px;height:27px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:#4D4D4D}.marvel-device.iphone4s .volume:before{position:absolute;left:0px;top:-70px;height:35px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone4s .volume:after{position:absolute;left:0px;bottom:-64px;height:27px;width:4px;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px;background:inherit;content:'';display:block}.marvel-device.iphone4s.landscape{padding:27px 129px;height:320px;width:480px}.marvel-device.iphone4s.landscape .bottom-bar{left:90px;bottom:0px;height:100%;width:3px}.marvel-device.iphone4s.landscape .top-bar{left:calc(100% - 60px);top:0px;height:100%;width:3px}.marvel-device.iphone4s.landscape .camera{top:134px;left:calc(100% - 72px);margin-left:0}.marvel-device.iphone4s.landscape .speaker{top:50%;margin-left:0;margin-top:-32px;left:calc(100% - 72px);width:10px;height:64px}.marvel-device.iphone4s.landscape .sensor{height:40px;width:10px;left:calc(100% - 36px);top:50%;margin-left:0;margin-top:-20px}.marvel-device.iphone4s.landscape .home{left:30px;bottom:50%;margin-left:0;margin-bottom:-36px}.marvel-device.iphone4s.landscape .sleep{height:60px;width:4px;right:-4px;top:calc(100% - 120px);-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px}.marvel-device.iphone4s.landscape .volume{top:-4px;left:calc(100% - 187px);height:4px;width:27px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone4s.landscape .volume:before{right:-70px;left:auto;top:0px;width:35px;height:4px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone4s.landscape .volume:after{width:27px;height:4px;bottom:0px;left:-64px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.iphone4s.silver{background:#bcbcbc}.marvel-device.iphone4s.silver:before{background:#fcfcfc}.marvel-device.iphone4s.silver .home{background:#fcfcfc;-webkit-box-shadow:inset 0 0 0 1px #bcbcbc;box-shadow:inset 0 0 0 1px #bcbcbc}.marvel-device.iphone4s.silver .home:after{border:1px solid rgba(0,0,0,0.2)}.marvel-device.iphone4s.silver .volume,.marvel-device.iphone4s.silver .sleep{background:#d6d6d6}.marvel-device.nexus5{padding:50px 15px 50px 15px;width:320px;height:568px;background:#1e1e1e;-webkit-border-radius:20px;border-radius:20px}.marvel-device.nexus5:before{-webkit-border-radius:600px / 50px;border-radius:600px / 50px;background:inherit;content:'';top:0;position:absolute;height:103.1%;width:calc(100% - 26px);top:50%;left:50%;-moz-transform:translateX(-50%) translateY(-50%);-webkit-transform:translateX(-50%) translateY(-50%);-o-transform:translateX(-50%) translateY(-50%);-ms-transform:translateX(-50%) translateY(-50%);transform:translateX(-50%) translateY(-50%)}.marvel-device.nexus5 .top-bar{width:calc(100% - 8px);height:calc(100% - 6px);position:absolute;top:3px;left:4px;-webkit-border-radius:20px;border-radius:20px;background:#181818}.marvel-device.nexus5 .top-bar:before{-webkit-border-radius:600px / 50px;border-radius:600px / 50px;background:inherit;content:'';top:0;position:absolute;height:103.0%;width:calc(100% - 26px);top:50%;left:50%;-moz-transform:translateX(-50%) translateY(-50%);-webkit-transform:translateX(-50%) translateY(-50%);-o-transform:translateX(-50%) translateY(-50%);-ms-transform:translateX(-50%) translateY(-50%);transform:translateX(-50%) translateY(-50%)}.marvel-device.nexus5 .bottom-bar{display:none}.marvel-device.nexus5 .sleep{width:3px;position:absolute;left:-3px;top:110px;height:100px;background:inherit;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px}.marvel-device.nexus5 .volume{width:3px;position:absolute;right:-3px;top:70px;height:45px;background:inherit;-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px}.marvel-device.nexus5 .camera{background:#3c3d3d;width:10px;height:10px;position:absolute;top:18px;left:50%;z-index:3;margin-left:-5px;-webkit-border-radius:100%;border-radius:100%}.marvel-device.nexus5 .camera:before{background:#3c3d3d;width:6px;height:6px;content:'';display:block;position:absolute;top:2px;left:-100px;z-index:3;-webkit-border-radius:100%;border-radius:100%}.marvel-device.nexus5.landscape{padding:15px 50px 15px 50px;height:320px;width:568px}.marvel-device.nexus5.landscape:before{width:103.1%;height:calc(100% - 26px);-webkit-border-radius:50px / 600px;border-radius:50px / 600px}.marvel-device.nexus5.landscape .top-bar{left:3px;top:4px;height:calc(100% - 8px);width:calc(100% - 6px)}.marvel-device.nexus5.landscape .top-bar:before{width:103%;height:calc(100% - 26px);-webkit-border-radius:50px / 600px;border-radius:50px / 600px}.marvel-device.nexus5.landscape .sleep{height:3px;width:100px;left:calc(100% - 210px);top:-3px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.nexus5.landscape .volume{height:3px;width:45px;right:70px;top:100%;-webkit-border-radius:0px 0px 2px 2px;border-radius:0px 0px 2px 2px}.marvel-device.nexus5.landscape .camera{top:50%;left:calc(100% - 18px);margin-left:0;margin-top:-5px}.marvel-device.nexus5.landscape .camera:before{top:-100px;left:2px}.marvel-device.s5{padding:60px 18px;-webkit-border-radius:42px;border-radius:42px;width:270px;/*height:495px;*/background:#bcbcbc}.marvel-device.s5:before,.marvel-device.s5:after{width:calc(100% - 52px);content:'';display:block;height:26px;background:inherit;position:absolute;-webkit-border-radius:500px / 40px;border-radius:500px / 40px;left:50%;-moz-transform:translateX(-50%);-webkit-transform:translateX(-50%);-o-transform:translateX(-50%);-ms-transform:translateX(-50%);transform:translateX(-50%)}.marvel-device.s5:before{top:-5px}.marvel-device.s5:after{bottom:-7px}.marvel-device.s5 .bottom-bar{display:none}.marvel-device.s5 .top-bar{-webkit-border-radius:37px;border-radius:37px;width:calc(100% - 10px);height:calc(100% - 10px);top:5px;left:5px;background:radial-gradient(rgba(0,0,0,0.02) 20%, transparent 60%) 0 0,radial-gradient(rgba(0,0,0,0.02) 20%, transparent 60%) 3px 3px;background-color:white;background-size:4px 4px;background-position:center;z-index:2;position:absolute}.marvel-device.s5 .top-bar:before,.marvel-device.s5 .top-bar:after{width:calc(100% - 48px);content:'';display:block;height:26px;background:inherit;position:absolute;-webkit-border-radius:500px / 40px;border-radius:500px / 40px;left:50%;-moz-transform:translateX(-50%);-webkit-transform:translateX(-50%);-o-transform:translateX(-50%);-ms-transform:translateX(-50%);transform:translateX(-50%)}.marvel-device.s5 .top-bar:before{top:-7px}.marvel-device.s5 .top-bar:after{bottom:-5px}.marvel-device.s5 .sleep{width:3px;position:absolute;left:-3px;top:100px;height:100px;background:#cecece;-webkit-border-radius:2px 0px 0px 2px;border-radius:2px 0px 0px 2px}.marvel-device.s5 .speaker{width:68px;height:8px;position:absolute;top:20px;display:block;z-index:3;left:50%;margin-left:-34px;background-color:#bcbcbc;background-position:top left;-webkit-border-radius:4px;border-radius:4px}.marvel-device.s5 .sensor{display:block;position:absolute;top:20px;right:110px;background:#3c3d3d;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;width:8px;height:8px;z-index:3}.marvel-device.s5 .sensor:after{display:block;content:'';position:absolute;top:0px;right:12px;background:#3c3d3d;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;width:8px;height:8px;z-index:3}.marvel-device.s5 .camera{display:block;position:absolute;top:24px;right:42px;background:black;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;width:10px;height:10px;z-index:3}.marvel-device.s5 .camera:before{width:4px;height:4px;background:#3c3d3d;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;position:absolute;content:'';top:50%;left:50%;margin-top:-2px;margin-left:-2px}.marvel-device.s5 .home{position:absolute;z-index:3;bottom:17px;left:50%;width:70px;height:20px;background:white;-webkit-border-radius:18px;border-radius:18px;display:block;margin-left:-35px;border:2px solid black}.marvel-device.s5.landscape{padding:18px 60px;height:320px;width:568px}.marvel-device.s5.landscape:before,.marvel-device.s5.landscape:after{height:calc(100% - 52px);width:26px;-webkit-border-radius:40px / 500px;border-radius:40px / 500px;-moz-transform:translateY(-50%);-webkit-transform:translateY(-50%);-o-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}.marvel-device.s5.landscape:before{top:50%;left:-7px}.marvel-device.s5.landscape:after{top:50%;left:auto;right:-7px}.marvel-device.s5.landscape .top-bar:before,.marvel-device.s5.landscape .top-bar:after{width:26px;height:calc(100% - 48px);-webkit-border-radius:40px / 500px;border-radius:40px / 500px;-moz-transform:translateY(-50%);-webkit-transform:translateY(-50%);-o-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}.marvel-device.s5.landscape .top-bar:before{right:-7px;top:50%;left:auto}.marvel-device.s5.landscape .top-bar:after{left:-7px;top:50%;right:auto}.marvel-device.s5.landscape .sleep{height:3px;width:100px;left:calc(100% - 200px);top:-3px;-webkit-border-radius:2px 2px 0px 0px;border-radius:2px 2px 0px 0px}.marvel-device.s5.landscape .speaker{height:68px;width:8px;left:calc(100% - 20px);top:50%;margin-left:0;margin-top:-34px}.marvel-device.s5.landscape .sensor{right:20px;top:calc(100% - 110px)}.marvel-device.s5.landscape .sensor:after{left:-12px;right:0px}.marvel-device.s5.landscape .camera{top:calc(100% - 42px);right:24px}.marvel-device.s5.landscape .home{width:20px;height:70px;bottom:50%;margin-bottom:-35px;margin-left:0;left:17px}.marvel-device.s5.black{background:#1e1e1e}.marvel-device.s5.black .speaker{background:black}.marvel-device.s5.black .sleep{background:#1e1e1e}.marvel-device.s5.black .top-bar{background:radial-gradient(rgba(0,0,0,0.05) 20%, transparent 60%) 0 0,radial-gradient(rgba(0,0,0,0.05) 20%, transparent 60%) 3px 3px;background-color:#2c2b2c;background-size:4px 4px}.marvel-device.s5.black .home{background:#2c2b2c}.marvel-device.lumia920{padding:80px 35px 125px 35px;background:#ffdd00;width:320px;height:533px;-moz-border-radius:40px / 3px;-webkit-border-radius:40px / 3px;border-radius:40px / 3px}.marvel-device.lumia920 .bottom-bar{display:none}.marvel-device.lumia920 .top-bar{width:calc(100% - 24px);height:calc(100% - 32px);position:absolute;top:16px;left:12px;-moz-border-radius:24px;-webkit-border-radius:24px;border-radius:24px;background:black;z-index:1}.marvel-device.lumia920 .top-bar:before{background:#1e1e1e;display:block;content:'';width:calc(100% - 4px);height:calc(100% - 4px);top:2px;left:2px;position:absolute;-moz-border-radius:22px;-webkit-border-radius:22px;border-radius:22px}.marvel-device.lumia920 .volume{width:3px;position:absolute;top:130px;height:100px;background:#1e1e1e;right:-3px;-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px}.marvel-device.lumia920 .volume:before{width:3px;position:absolute;top:190px;content:'';display:block;height:50px;background:inherit;right:0px;-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px}.marvel-device.lumia920 .volume:after{width:3px;position:absolute;top:460px;content:'';display:block;height:50px;background:inherit;right:0px;-webkit-border-radius:0px 2px 2px 0px;border-radius:0px 2px 2px 0px}.marvel-device.lumia920 .camera{background:#3c3d3d;width:10px;height:10px;position:absolute;top:34px;right:130px;z-index:5;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px}.marvel-device.lumia920 .speaker{background:#292728;width:64px;height:10px;position:absolute;top:38px;left:50%;margin-left:-32px;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;z-index:3}.marvel-device.lumia920.landscape{padding:35px 80px 35px 125px;height:320px;width:568px;-moz-border-radius:2px / 100px;-webkit-border-radius:2px / 100px;border-radius:2px / 100px}.marvel-device.lumia920.landscape .top-bar{height:calc(100% - 24px);width:calc(100% - 32px);left:16px;top:12px}.marvel-device.lumia920.landscape .volume{height:3px;right:130px;width:100px;top:100%;-webkit-border-radius:0px 0px 2px 2px;border-radius:0px 0px 2px 2px}.marvel-device.lumia920.landscape .volume:before{height:3px;right:190px;top:0px;width:50px;-webkit-border-radius:0px 0px 2px 2px;border-radius:0px 0px 2px 2px}.marvel-device.lumia920.landscape .volume:after{height:3px;right:430px;top:0px;width:50px;-webkit-border-radius:0px 0px 2px 2px;border-radius:0px 0px 2px 2px}.marvel-device.lumia920.landscape .camera{right:30px;top:calc(100% - 140px)}.marvel-device.lumia920.landscape .speaker{width:10px;height:64px;top:50%;margin-left:0;margin-top:-32px;left:calc(100% - 48px)}.marvel-device.lumia920.black{background:black}.marvel-device.lumia920.white{background:white;-webkit-box-shadow:0 1px 2px 0 rgba(0,0,0,0.2);box-shadow:0 1px 2px 0 rgba(0,0,0,0.2)}.marvel-device.lumia920.blue{background:#00acdd}.marvel-device.lumia920.red{background:#CC3E32}.marvel-device.htc-one{padding:72px 25px 100px 25px;width:320px;height:568px;background:#bebebe;-webkit-border-radius:34px;border-radius:34px}.marvel-device.htc-one:before{content:'';display:block;width:calc(100% - 4px);height:calc(100% - 4px);position:absolute;top:2px;left:2px;background:#adadad;-webkit-border-radius:32px;border-radius:32px}.marvel-device.htc-one:after{content:'';display:block;width:calc(100% - 8px);height:calc(100% - 8px);position:absolute;top:4px;left:4px;background:#eeeeee;-webkit-border-radius:30px;border-radius:30px}.marvel-device.htc-one .top-bar{width:calc(100% - 4px);height:635px;position:absolute;background:#424242;top:50px;z-index:1;left:2px}.marvel-device.htc-one .top-bar:before{content:'';position:absolute;width:calc(100% - 4px);height:100%;position:absolute;background:black;top:0px;z-index:1;left:2px}.marvel-device.htc-one .bottom-bar{display:none}.marvel-device.htc-one .speaker{height:16px;width:216px;display:block;position:absolute;top:22px;z-index:2;left:50%;margin-left:-108px;background:radial-gradient(#343434 25%, transparent 50%) 0 0,radial-gradient(#343434 25%, transparent 50%) 4px 4px;background-size:4px 4px;background-position:top left}.marvel-device.htc-one .speaker:after{content:'';height:16px;width:216px;display:block;position:absolute;top:676px;z-index:2;left:50%;margin-left:-108px;background:inherit}.marvel-device.htc-one .camera{display:block;position:absolute;top:18px;right:38px;background:#3c3d3d;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;width:24px;height:24px;z-index:3}.marvel-device.htc-one .camera:before{width:8px;height:8px;background:black;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;position:absolute;content:'';top:50%;left:50%;margin-top:-4px;margin-left:-4px}.marvel-device.htc-one .sensor{display:block;position:absolute;top:29px;left:60px;background:#3c3d3d;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;width:8px;height:8px;z-index:3}.marvel-device.htc-one .sensor:after{display:block;content:'';position:absolute;top:0px;right:12px;background:#3c3d3d;-moz-border-radius:100%;-webkit-border-radius:100%;border-radius:100%;width:8px;height:8px;z-index:3}.marvel-device.htc-one.landscape{padding:25px 72px 25px 100px;height:320px;width:568px}.marvel-device.htc-one.landscape .top-bar{height:calc(100% - 4px);width:635px;left:calc(100% - 685px);top:2px}.marvel-device.htc-one.landscape .speaker{width:16px;height:216px;left:calc(100% - 38px);top:50%;margin-left:0px;margin-top:-108px}.marvel-device.htc-one.landscape .speaker:after{width:16px;height:216px;left:calc(100% - 692px);top:50%;margin-left:0;margin-top:-108px}.marvel-device.htc-one.landscape .camera{right:18px;top:calc(100% - 38px)}.marvel-device.htc-one.landscape .sensor{left:calc(100% - 29px);top:60px}.marvel-device.htc-one.landscape .sensor :after{right:0;top:-12px}.marvel-device.ipad{width:576px;height:768px;padding:90px 25px;background:#242324;-webkit-border-radius:44px;border-radius:44px}.marvel-device.ipad:before{width:calc(100% - 8px);height:calc(100% - 8px);position:absolute;content:'';display:block;top:4px;left:4px;-webkit-border-radius:40px;border-radius:40px;background:#1e1e1e}.marvel-device.ipad .camera{background:#3c3d3d;width:10px;height:10px;position:absolute;top:44px;left:50%;margin-left:-5px;-webkit-border-radius:100%;border-radius:100%}.marvel-device.ipad .top-bar,.marvel-device.ipad .bottom-bar{display:none}.marvel-device.ipad .home{background:#242324;-webkit-border-radius:36px;border-radius:36px;width:50px;height:50px;position:absolute;left:50%;margin-left:-25px;bottom:22px}.marvel-device.ipad .home:after{width:15px;height:15px;margin-top:-8px;margin-left:-8px;border:1px solid rgba(255,255,255,0.1);-webkit-border-radius:4px;border-radius:4px;position:absolute;display:block;content:'';top:50%;left:50%}.marvel-device.ipad.landscape{height:576px;width:768px;padding:25px 90px}.marvel-device.ipad.landscape .camera{left:calc(100% - 44px);top:50%;margin-left:0;margin-top:-5px}.marvel-device.ipad.landscape .home{top:50%;left:22px;margin-left:0;margin-top:-25px}.marvel-device.ipad.silver{background:#bcbcbc}.marvel-device.ipad.silver:before{background:#fcfcfc}.marvel-device.ipad.silver .home{background:#fcfcfc;-webkit-box-shadow:inset 0 0 0 1px #bcbcbc;box-shadow:inset 0 0 0 1px #bcbcbc}.marvel-device.ipad.silver .home:after{border:1px solid rgba(0,0,0,0.2)}.marvel-device.macbook{width:960px;height:600px;padding:44px 44px 76px;margin:0 auto;background:#bebebe;-webkit-border-radius:34px;border-radius:34px}.marvel-device.macbook:before{width:calc(100% - 8px);height:calc(100% - 8px);position:absolute;content:'';display:block;top:4px;left:4px;-webkit-border-radius:30px;border-radius:30px;background:#1e1e1e}.marvel-device.macbook .top-bar{width:calc(100% + 2 * 70px);height:40px;position:absolute;content:'';display:block;top:680px;left:-70px;border-bottom-left-radius:90px 18px;border-bottom-right-radius:90px 18px;background:#bebebe;-webkit-box-shadow:inset 0px -4px 13px 3px rgba(34,34,34,0.6);-moz-box-shadow:inset 0px -4px 13px 3px rgba(34,34,34,0.6);box-shadow:inset 0px -4px 13px 3px rgba(34,34,34,0.6)}.marvel-device.macbook .top-bar:before{width:100%;height:24px;content:'';display:block;top:0;left:0;background:#f0f0f0;border-bottom:2px solid #aaa;-webkit-border-radius:5px;border-radius:5px;position:relative}.marvel-device.macbook .top-bar:after{width:16%;height:14px;content:'';display:block;top:0;background:#ddd;position:absolute;margin-left:auto;margin-right:auto;left:0;right:0;-webkit-border-radius:0 0 20px 20px;border-radius:0 0 20px 20px;-webkit-box-shadow:inset 0px -3px 10px #999;-moz-box-shadow:inset 0px -3px 10px #999;box-shadow:inset 0px -3px 10px #999}.marvel-device.macbook .bottom-bar{background:transparent;width:calc(100% + 2 * 70px);height:26px;position:absolute;content:'';display:block;top:680px;left:-70px}.marvel-device.macbook .bottom-bar:before,.marvel-device.macbook .bottom-bar:after{height:calc(100% - 2px);width:80px;content:'';display:block;top:0;position:absolute}.marvel-device.macbook .bottom-bar:before{left:0;background:#f0f0f0;background:-moz-linear-gradient(left, #747474 0%, #c3c3c3 5%, #ebebeb 14%, #979797 41%, #f0f0f0 80%, #f0f0f0 100%, #f0f0f0 100%);background:-webkit-gradient(linear, left top, right top, color-stop(0%, #747474), color-stop(5%, #c3c3c3), color-stop(14%, #ebebeb), color-stop(41%, #979797), color-stop(80%, #f0f0f0), color-stop(100%, #f0f0f0), color-stop(100%, #f0f0f0));background:-webkit-linear-gradient(left, #747474 0%, #c3c3c3 5%, #ebebeb 14%, #979797 41%, #f0f0f0 80%, #f0f0f0 100%, #f0f0f0 100%);background:-o-linear-gradient(left, #747474 0%, #c3c3c3 5%, #ebebeb 14%, #979797 41%, #f0f0f0 80%, #f0f0f0 100%, #f0f0f0 100%);background:-ms-linear-gradient(left, #747474 0%, #c3c3c3 5%, #ebebeb 14%, #979797 41%, #f0f0f0 80%, #f0f0f0 100%, #f0f0f0 100%);background:linear-gradient(to right, #747474 0%, #c3c3c3 5%, #ebebeb 14%, #979797 41%, #f0f0f0 80%, #f0f0f0 100%, #f0f0f0 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#747474', endColorstr='#f0f0f0', GradientType=1)}.marvel-device.macbook .bottom-bar:after{right:0;background:#f0f0f0;background:-moz-linear-gradient(left, #f0f0f0 0%, #f0f0f0 0%, #f0f0f0 20%, #979797 59%, #ebebeb 86%, #c3c3c3 95%, #747474 100%);background:-webkit-gradient(linear, left top, right top, color-stop(0%, #f0f0f0), color-stop(0%, #f0f0f0), color-stop(20%, #f0f0f0), color-stop(59%, #979797), color-stop(86%, #ebebeb), color-stop(95%, #c3c3c3), color-stop(100%, #747474));background:-webkit-linear-gradient(left, #f0f0f0 0%, #f0f0f0 0%, #f0f0f0 20%, #979797 59%, #ebebeb 86%, #c3c3c3 95%, #747474 100%);background:-o-linear-gradient(left, #f0f0f0 0%, #f0f0f0 0%, #f0f0f0 20%, #979797 59%, #ebebeb 86%, #c3c3c3 95%, #747474 100%);background:-ms-linear-gradient(left, #f0f0f0 0%, #f0f0f0 0%, #f0f0f0 20%, #979797 59%, #ebebeb 86%, #c3c3c3 95%, #747474 100%);background:linear-gradient(to right, #f0f0f0 0%, #f0f0f0 0%, #f0f0f0 20%, #979797 59%, #ebebeb 86%, #c3c3c3 95%, #747474 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f0f0f0', endColorstr='#747474', GradientType=1)}.marvel-device.macbook .camera{background:#3c3d3d;width:10px;height:10px;position:absolute;top:20px;left:50%;margin-left:-5px;-webkit-border-radius:100%;border-radius:100%}.marvel-device.macbook .home{display:none}
.mob_name{text-align: center;color: #DF182B;font-weight: bold;font-size: 16px;padding-top: 10px;}
.mob_image img{width: 100%;height: 160px;}
.mob_title{text-align: center;font-size: 13px;color: #9B9B9B; padding-bottom:5px;padding-left: 10px;padding-right: 10px;}
.mob_deal{width: 100%;height: auto;}
.mob_radio_button{width: 10%;float: left;}
.mob_price{width: 20%;float: left;}
.mob_offer_desc{width: 70%;float: left;margin-bottom:28px;}
.scrollbar{float: left;height: 300px;width: 65px;background: #F5F5F5;margin-bottom:25px;}
.force-overflow{min-height: 414px;}
#style-3::-webkit-scrollbar-track{-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);background-color: #bcbcbc;}
#style-3::-webkit-scrollbar{width: 6px;background-color: #bcbcbc;}
#style-3::-webkit-scrollbar-thumb{background-color: #bcbcbc;}
.mob_name_desc{text-align: left;padding-left: 10px;padding-top: 5px;font-weight: bold;font-size: 18px;}
.mob_long_desc{text-align: left;padding-left: 10px;padding-top: 5px;font-weight: 600;font-size: 13px;float: left;}
.btnCross{position:absolute;right:5px;color:#fff;background:#454545;padding: 0px 3px;top:0px;display:none;}
.col_class{border:1px solid #e8e8e8; padding:3px; font-weight:600;background-color: #d8d8d8;}
.col_class_value{border:1px solid #e8e8e8; padding:3px;text-align: center;}
#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
#sortable li span { position: absolute; margin-left: -1.3em; }
label {font-weight: 500;font-size: 14px;}
.ui-slider-horizontal .ui-slider-range-min{background: #22bacf !important;}
.ui-slider-horizontal .ui-slider-range{top:-2px !important;}
.irs{height: 55px;}
.irs-with-grid {height: 75px;}
.irs-line {height: 10px; top: 33px;background: #EEE;background: linear-gradient(to bottom, #DDD -50%, #FFF 150%);border-radius: 16px;-moz-border-radius: 16px;}
.irs-line-left {height: 8px;}
.irs-line-mid {height: 8px;}
.irs-line-right {height: 8px;}
.irs-bar {height: 10px; top: 33px;border-top: 1px solid #57C8F2;border-bottom: 1px solid #57C8F2;background: #57C8F2;background: linear-gradient(to top, #57C8F2 0%,#57C8F2 100%);}
.irs-bar-edge {height: 10px; top: 33px;width: 14px;border: 1px solid #57C8F2;border-right: 0;background: #57C8F2;background: linear-gradient(to top, #57C8F2 0%,#57C8F2 100%);border-radius: 16px 0 0 16px;-moz-border-radius: 16px 0 0 16px;}
.irs-shadow {height: 2px; top: 38px;background: #000; opacity: 0.3;border-radius: 5px;-moz-border-radius: 5px;}
.lt-ie9 .irs-shadow {filter: alpha(opacity=30);}
.irs-slider {top: 29px; width: 18px; height: 18px; border: 1px solid #AAA;
    background: #DDD;
    background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(220,220,220,1) 20%,rgba(255,255,255,1) 100%); /* W3C */
    border-radius: 27px;
    -moz-border-radius: 27px;
    box-shadow: 1px 1px 3px rgba(0,0,0,0.3);
    cursor: pointer;
}

.irs-slider.state_hover, .irs-slider:hover {
    background: #FFF;
}

.irs-min, .irs-max {
    color: #333;
    font-size: 12px; line-height: 1.333;
    text-shadow: none;
    top: 0;
    padding: 1px 5px;
    background: rgba(0,0,0,0.1);
    border-radius: 3px;
    -moz-border-radius: 3px;
}

.lt-ie9 .irs-min, .lt-ie9 .irs-max {
    background: #ccc;
}
.irs-from, .irs-to, .irs-single {
    position: absolute;
    display: block;
    top: 9px;
    left: 0;
    cursor: default;
    white-space: nowrap;
}
.irs-from, .irs-to, .irs-single {
    color: #fff;
       margin-left: 5px;
    font-size: 10px;
	line-height: 1.333;
    text-shadow: none;
    padding: 1px 5px;
    background: #428bca;
    border-radius: 3px;
    -moz-border-radius: 3px;
}
.lt-ie9 .irs-from, .lt-ie9 .irs-to, .lt-ie9 .irs-single {
    background: #999;
}

.irs-grid {
    height: 27px;
}
.irs-grid-pol {
    opacity: 0.5;
    background: #428bca;
}
.irs-grid-pol.small {
    background: #999;
}

.irs-grid-text {
    bottom: 5px;
    color: #99a4ac;
}

.irs-line-mid, .irs-line-left, .irs-line-right, .irs-diapason, .irs-slider {
    background: #E5EAEE;
    border: none;
}
.irs-min{ display:none;}
.irs-max{ display:none;}
.carousel-inner>.item>img, .carousel-inner>.item>a>img{ height:160px;}
.flat_offer_div{ display:none;}
.day_time_offer_div{ display:none;}
.combo_offer_div{ display:none;}
.box-title{margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #F6F6F6;height: 22px; font-size:16PX;}
.ui-sortable > li {padding: 0px !important;position: relative;background: #fff;margin-bottom: 0px;border-bottom: none !important;}
.model_offersdays{width: 89%;margin-left: 14px;margin-top: 15px;height: 25px;float: left;text-align:center;cursor: pointer;border-radius: 3px;}
#upload-file-container {background: url(<?php echo APP_IMAGES.'button.png' ?>) no-repeat;background-position: 3px 3px;height: 40px;}
#upload-file-container input {filter: alpha(opacity=0);opacity: 0;}
.bg{ background-color:#DADADA !important;}
.site-min-height {min-height: 723px;}
.rad_menu_bar{height: 50px;    background-color: #DF182B;}
.rad_one{width: 25%;border-right: 1px solid #E24E58;height: 50px;float: left;}
.r_img1{height: 19px;margin-top: 10px;float: left;margin-left: 16px;}
.r_img2{height: 19px;margin-top: 10px;float: left;margin-left: 20px;}
.r_img3{height: 19px;margin-top: 10px;float: left;margin-left: 24px;}
.r_img4{height: 19px;margin-top: 10px;float: left;margin-left: 25px;}
.r_text1{float: left;color: #fff;font-size: 10px;margin-left: 11px;   margin-top: 2px;}
.r_text2{float: left;color: #fff;font-size: 10px;margin-left: 7px;   margin-top: 2px;}
.r_text3{float: left;color: #fff;font-size: 10px;margin-left: 18px;   margin-top: 2px;}
.r_text4{float: left;color: #fff;font-size: 10px;margin-left: 16px;   margin-top: 2px;}
#main_submit_deal{float: right;margin-top: 15px;width: 125px; box-shadow:none;margin-right: 300px;}
#ok_day{background-color: #E51429;border-color: #E51429;padding: 2px 15px;margin-top: 29px;}
.cle1{background-color: #BCBCBC;border-color: #BCBCBC;padding: 2px 15px;margin-top: 29px;margin-right: 20px;}
.cl1{background-color: #BCBCBC;border-color: #BCBCBC;padding: 2px 15px;margin-top: 65px;}
 #ok_custom_day{background-color: #E51429;border-color: #E51429;padding: 2px 30px;margin-top: 65px;}
 .offers_class{float: left;border: 1px solid;width: 150px;text-align: center;margin-left: 15px;border-top-left-radius: 5px;border-top-right-radius: 5px;margin-right: 5px;cursor: pointer;background-color: #E51429;color: #fff;font-weight: 500;padding: 4px;font-size: 15px;}
.shoutout_class{float: left;border: 1px solid;width: 150px;text-align: center;border-top-left-radius: 5px;border-top-right-radius: 5px;cursor: pointer;background-color: #DEDEDE;color: #fff;font-weight: 500;padding: 4px;}
</style>
</head>
<body>
<section id="container">
<?php 
	include(APP_VIEW.'includes/header.php');
	include(APP_VIEW.'includes/nav.php');
?>
<section id="main-content">
	<section class="wrapper site-min-height">
    	<div class="row">
        	<div class="col-lg-12">
            	<section class="panel">
                	<div id="message"></div>
                    <header class="panel-heading col-lg-8">
                        <!--<h3 class="box-title" style=""><strong>DEAL DETAILS</strong></h3>-->
                        <div style="height: 31px;border-bottom: 1px solid #dedede;">
                    		<div id="offers_div" class="offers_class">OFFERS</div>
                        	<div id="shout_out_div" class="shoutout_class">SHOUT OUT</div>
                        </div>
                        <!--<div style="height: 5px;width: 60px;background: #5AD0B6;"></div>-->
                    </header>
                    <header class="panel-heading col-lg-4">
                        <h3 class="box-title" style=""><strong>PREVIEW</strong></h3>
                        <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                    </header>
                    <div class="panel-body">
                    <div class="row">
                    <form id="form" action="#">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="box1">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="pull-left">Name your Deal</label>
                                                </div>
                                                <div class="col-md-12">
                                                <input name="deal_name" id="deal_name" class="form-control input-sm" placeholder="Deal Name" style="margin-bottom: 15px;" maxlength="30" value="<?php echo $data[1][0]["deal_name"] ?>"/>
												</div>
                                            </div>
                                            <div class="form-group">     
                                                <div class="col-md-12" style="margin-top:10px;">
                                                    <label class="pull-left">Images related to your Deal</label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:10px;margin-left: 0%;padding-right: 0px;">
                                                    <div id="all_images" style="width: 650px;height: 88px;">
                                                    <div id="dragable" style="float:left;"></div>
                                                    <div id="nondragable">
														<div class="col-md-3" style="width:auto;padding-right: 5px;padding-left: 5px;">
                                                            <div class="form-group">
                                                                <button type="button" id="" class="btn btn-box-tool btnCross" data-widget="remove">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                                <label>
                                                                    <img id="add_image" style="cursor:pointer;" height="80px" width="110px" src="<?php echo APP_IMAGES."add_image.jpg"; ?>">
                                                                </label>
															</div>
                                                        </div>
                                                    </div>
                                                	</div>
                                                </div>
                                            </div>
                                            <div class="deal_desc_so">
                                               <div class="form-group">       
                                                    <div class="col-md-4" style="margin-top:10px;">
                                                        <label class="pull-left">Deal Description</label>
                                                    </div>
                                                    <div class="col-md-8" style="margin-top:10px;">
                                                        <textarea name="so_desc" id="so_desc" class="form-control input-sm" maxlength="300" style="height:100px"/><?php if(isset($data[1][0]["deal_shoutout_description"])){ echo $data[1][0]["deal_shoutout_description"]; } ?></textarea>
                                                    </div>  
                                                </div>
                                               <div id="">
                                                <div class="form-group"> 
                                                        <div class="col-md-4" style="margin-top:10px;">
                                                            <label class="pull-left">Deal Days</label>
                                                        </div>
                                                        <div class="col-md-8" style="margin-top:10px;">
                                                            <select name="shoutout_deal_days_select" id="shoutout_deal_days_select" class="form-control input-sm offer_timeing" onClick="onSelect()">
                                                                <option selected value="0">--Select--</option>
                                                                <option value="1" class="dayclass">Everyday</option>
                                                                <option value="2" class="dayclass">Weekdays</option>
                                                                <option value="3" class="dayclass">Weekends</option>
                                                                <option value="4" class="dayclass">Custom Days</option>
                                                                <option value="5" class="dayclass">Custom Date</option>
                                                            </select>
                                                            <input type="hidden" id="shoutout_deal_days" name="shoutout_deal_days" value="" />
                                                        </div>	
                                                    </div>
                                               </div>
                                               <div id="">
                                               	<div class="form-group"> 
                                                        <div class="col-md-4" style="margin-top:10px;">
                                                            <label class="pull-left">Deal Duration</label>
                                                        </div>
                                                        <div class="col-md-8" style="margin-top:10px;">
                                                            <select name="offers_timeings_so" id="offers_timeings_so" class="form-control input-sm offer_timeing">
                                                                <option selected value="0">--Select--</option>
                                                                <option value="1">Only Mornings (09:00 AM to 12:00 PM)</option>
                                                                <option value="2">Early Afternoons (12:00 PM to 03:00 PM)</option>
                                                                <option value="3">Late Afternoons (03:00 PM to 07:00 PM)</option>
                                                                <option value="4">Only Afternoons (07:00 PM to 11:00 PM)</option>
                                                                <option value="5">Custom Time</option>
                                                                <option value="6">All Day</option>
                                                            </select>
                                                            <input type="hidden" id="offers_timeings_so_hidden" name="offers_timeings_so_hidden" value="" />
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <header class="panel-heading col-lg-12" style="text-align: center;">
                                            <span style="position: absolute;margin-top: 13px;background-color: #fff;width: 121px;margin-left: -54px;font-size: 13px;">EDIT OFFER</span>
                                                <h3 class="box-title" style=""></h3>
                                               
                                            </header>
                                   			<div class="col-md-12" style="margin-bottom:10px; padding:0;">
                                               <!-- <span id="offer_table">
                                                    <div class="col-md-1 col_class" style="text-align:center;">S.no</div>
                                                    <div class="col-md-10 col_class" style="text-align:center;">Offer name</div>
                                                    <div class="col-md-1 col_class" style="text-align:center;height: 26px;"></div>
                                                    <div class="offer_type_values"></div>
                                                </span>-->
                                                <span id="offer_str"></span>
                                            </div>
                                            <header class="panel-heading col-lg-12" style="text-align: center;" id="text_head">
                                            <span style="position: absolute;margin-top: 13px;background-color: #fff;width: 121px;margin-left: -54px;font-size: 13px;">EDIT OFFER</span>
                                                <h3 class="box-title" style=""></h3>
                                               
                                            </header>
                                            <div class="shout_offer_div_hide">
                                            	<div class="offer_type_div">
                                                	<div class="form-group"> 
                                                <div class="col-md-4" style="margin-top:10px;">
                                                    <label class="pull-left">Offer Type</label>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                    <select name="offer_type" id="offer_type" class="form-control input-sm">
                                                        <option selected value=" ">--Select--</option>
                                                        <option value="1" title="Flat Deal" class="Flat Deal">Flat Offer</option>
                                                        <option value="2" title="Combo Deal">Combo Offer</option>
                                                        <!--<option value="3" title="Shout-Out">Shout-Out</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                            	</div>
                                            	<div class="flat_offer_div">
                                                	<div class="form-group">       
                                                    <div class="col-md-4" style="margin-top:35px;">
                                                        <label class="pull-left">Flat Discount(%)</label>
                                                    </div>
                                                    <div class="col-md-8" style="margin-top:10px;">
                                                        <input id="range_flat" type="text" name="range_flat" value="">
                                                        <input type="hidden" id="flat_discount" class="flat_discount" name="flat_discount" value="10">
                                                    </div>
                                                </div>
                                            	</div>
                                            
                                            </div>
                                           
                                            
                                   		</div>   <!--box-body div close here-->
                                        <div class="shout_offer_div_hide">
                                        <div class="combo_offer_div">
                                            <div class="form-group">       
                                                <div class="col-md-4" style="margin-top:10px;">
                                                    <label class="pull-left">Offer Description</label>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                    <textarea name="short_desc" id="short_desc" class="form-control input-sm" placeholder="" maxlength="" style="height:100px" /></textarea>
                                                </div>  
                                            </div>  
                                            <div class="form-group">     
                                                <div class="col-md-4" style="margin-top:10px;">
                                                	<label class="pull-left">Inculde Price</label> 
                                                    <small style="font-size: 10px;margin-left: 5px;">(Optinal)</small>
                                                    
                                                </div>
                                            	<div class="col-md-8" style="margin-top:10px;margin-bottom: 10px;">
                                            		<!--<input type="checkbox" id="include_price_input" class="include_price_input">
                                            		<input type="hidden" id="input_chk_value" name="input_chk_value" class="input_chk_value" value="0">-->
                                                    <input type="text" id="inc_price" name="inc_price" class="form-control input-sm" value="">
                                            	</div>
                                            </div>
                                            <!--<div class="include_price_div">  
                                                <div class="form-group">     
                                                    <div class="col-md-4" style="margin-top:10px;">
                                                    	<label class="pull-left">Original Price*</label>
                                                    </div>
                                                    <div class="col-md-2" style="margin-top:10px;">
                                                        <input name="price" id="price" class="form-control input-sm" value="0" placeholder="Price" />
                                                    </div>
                                                </div>
                                            <div class="form-group">      
                                                <div class="col-md-3" style="margin-top:10px;">
                                                    <label class="pull-left">Offer Price*</label>
                                                </div>
                                                <div class="col-md-3" style="margin-top:10px;">
                                                    <h3 style="padding:0; margin:0">Rs <span id="priceLeft">0</span></h3>
                                                    <input id="original_price" type="hidden" name="original_price"  value="" />
                                                    <input id="after_discount_price" type="hidden" name="after_discount_price" value="" />
                                                    <input id="save_price" type="hidden" name="save_price" value="" />
                                                    <input id="combo_discount_bar" type="hidden" name="combo_discount_bar" value="" />
                                                </div>
                                            </div> 
                                            <div class="col-md-12 margin">
                                            <input id="range" type="text" name="range" value="">
                                            </div>
                                            </div>-->
                                        </div>
                                        <div id="min_bill_div">
                                            <div class="form-group">       
                                                <div class="col-md-4" style="margin-top:10px;">
                                                    <label class="pull-left">Minimum Billing Amount</label>
                                                    <small style="font-size: 10px;">(Optinal)</small>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                    <input name="min_billing" id="min_billing" class="form-control input-sm" style="" maxlength="5" value="" placeholder="Rs. 100"/> 
                                                </div>
                                            </div>
                                        </div>
                                      	<div id="offers_day_div">
                                        <div class="form-group"> 
                                                <div class="col-md-4" style="margin-top:10px;">
                                                    <label class="pull-left">Offer Days</label>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                    <select name="offers_days" id="offers_days" class="form-control input-sm offer_timeing" onClick="onSelect()">
                                                        <option selected value="0">--Select--</option>
                                                        <option value="1" class="dayclass">Everyday</option>
                                                        <option value="2" class="dayclass">Weekdays</option>
                                                        <option value="3" class="dayclass">Weekends</option>
                                                        <option value="4" class="dayclass">Custom Days</option>
                                                        <option value="5" class="dayclass">Custom Date</option>
                                                    </select>
                                                    <input type="hidden" id="offer_day_hidden_type" name="offer_day_hidden_type" value="" />
                                                    <small class="help-block" style="display: none;color:#a94442;" id="ofd">Please Select Offer Days</small>
                                            	</div>	
                                            </div>
                                       </div>
                                        <div id="offers_timeing_div">
                                            <div class="form-group"> 
                                                <div class="col-md-4" style="margin-top:10px;">
                                                    <label class="pull-left">Offer Duration</label>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                    <select name="offers_timeings" id="offers_timeings" class="form-control input-sm offer_timeing">
                                                        <option selected value="0">--Select--</option>
                                                        <option value="1">Only Mornings (09:00 AM to 12:00 PM)</option>
                                                        <option value="2">Early Afternoons (12:00 PM to 03:00 PM)</option>
                                                        <option value="3">Late Afternoons (03:00 PM to 07:00 PM)</option>
                                                        <option value="4">Only Afternoons (07:00 PM to 11:00 PM)</option>
                                                        <option value="5">Custom Time</option>
                                                        <option value="6">All Day</option>
                                                    </select>
                                                    <input type="hidden" id="offer_time_hidden_type" name="offer_time_hidden_type" value="" />
                                                    <small class="help-block" style="display: none;color:#a94442;" id="oft">Please Select Offer Timeings</small>
                                            	</div>
                                        	</div>
                                       </div>
                                        <div id="coupon_active_inactive_div">
                                            <div class="form-group"> 
                                                <div class="col-md-4" style="margin-top:10px;">
                                                    <label class="pull-left">Deal Type</label>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                    <select name="coupon_active_inactive" id="coupon_active_inactive" class="form-control input-sm">
                                                        <option selected value="00">--Select--</option>
                                                        <option value="1">Coupon Required (customer will need to avil the coupon)</option>
                                                        <option value="0">Coupon Not Required (No need to avil the coupon)</option>
                                                    </select>
                                            	</div>
                                       		</div>
                                        </div>
                                        <div id="maxcap"> 
                                            <div class="form-group">       
                                                <div class="col-md-4" style="margin-top:10px;">
                                                    <label class="pull-left">Max No Of Coupons Offered</label>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                    <input name="max_capping" id="max_capping" class="form-control input-sm" style="" maxlength="4" value="" placeholder="100"/> 
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div id="">
                                            <div class="form-group">       
                                                <div class="col-md-12" style="margin-top:10px;">
                                                	<button type="button" class="btn btn-primary" id="add_offer_cancel" style="border-color: rgb(194, 199, 192);color: rgb(255, 255, 255);padding-left: 15px;padding-right: 15px;font-size: 15px;float: right;display: block;background-color: rgb(194, 199, 192);
margin-left: 10px;">Cancel</button>
                                                    <button type="button" class="btn btn-primary" id="add_offer" style="background-color: #79CD51;
border-color: #79CD51;color: #FFFFFF;padding-left: 15px;padding-right: 15px;font-size: 15px;float: right;">Save</button>
                                                </div>
                                                <div class="col-md-8" style="margin-top:10px;">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="shout_offer_input_value">
                                        	<div class="col-md-12" style="margin-bottom:10px; padding:0;">
                                                <span id="offer_table_so">
                                                    <!--<div class="col-md-1 col_class" style="text-align:center;">S.no</div>
                                                    <div class="col-md-10 col_class" style="text-align:center;">Offer name</div>
                                                    <div class="col-md-1 col_class" style="text-align:center;height: 26px;"></div>-->
                                                    <div class="offer_type_values_so"></div>
                                                </span>
                                                <span id="offer_str_so"></span>
                                            </div>
                                            <div class="so_offer_type_div">
                                                 <div class="form-group"> 
                                                    <div class="col-md-12" style="margin-top:10px;">
                                                        <label class="pull-left">Offer Type</label>
                                                    </div>
                                                    <div class="col-md-12" style="margin-top:10px;">
                                                        <select name="offer_type_so" id="offer_type_so" class="form-control input-sm">
                                                            <option selected value=" ">--Select--</option>
                                                            <option value="1" title="Flat Deal" class="Flat Deal">Flat Offer</option>
                                                            <option value="2" title="Combo Deal">Combo Offer</option>
                                                            <!--<option value="3" title="Shout-Out">Shout-Out</option>-->
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="so_flate_offer_div">
                                                 <div class="form-group">       
                                                    <div class="col-md-4" style="margin-top:35px;">
                                                        <label class="pull-left">Flat Discount(%)</label>
                                                    </div>
                                                    <div class="col-md-8" style="margin-top:10px;">
                                                        <input id="range_flat_so" type="text" name="range_flat_so" value="">
                                                        <input type="hidden" id="flat_discount_so" class="flat_discount_so" name="flat_discount_so" value="10">
                                                    </div> 
                                                </div>
                                            </div>
                                             <div class="so_offer_desc_div">
                                                 <div class="form-group">       
                                                    <div class="col-md-4" style="margin-top:10px;">
                                                        <label class="pull-left">Offer Description</label>
                                                    </div>
                                                    <div class="col-md-8" style="margin-top:10px;">
                                                        <textarea name="so_offer_desc" id="so_offer_desc" class="form-control input-sm" maxlength="120" style="height:100px" /></textarea>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="so_add_more_button">
                                            	<div class="form-group">       
                                                    <div class="col-md-12" style="margin-top:10px;">
                                                    <div id="add_offer_so_cancel" style="font-size: 12px;float: right;margin-left: 7px;margin-top: 6px; cursor:pointer;">Cancel</div>
                                                        <button type="button" class="btn btn-primary" id="ads" style="background-color: #79CD51;
border-color: #79CD51;color: #FFFFFF;padding-left: 15px;padding-right: 15px;font-size: 15px;float: right;">ADD OFFER</button>
                                                        <button type="button" class="btn btn-primary" id="add_offer_so" style="background-color: #79CD51;
border-color: #79CD51;color: #FFFFFF;padding-left: 15px;padding-right: 15px;font-size: 15px;float: right;">Save</button>
                                                        
                                                    </div>
                                                    <div class="col-md-8" style="margin-top:10px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      
                        			</div> <!--box1 div close here-->
                    			</div><!--col-md-8 div close here-->
<!--Mobile view HTML Start-->
<div class="dealContent">	
<div class="col-md-4" >
    <div class="box" style="border:0; padding:0; margin:0 auto; float:none;margin-top: 30px;">
        <div class="box-body" style="padding:0; padding-bottom:10px">
            <div class="row">
                <div class="col-md-6">
                    <div class="marvel-device s5 black">
                        <div class="top-bar"></div>
                        <div class="sleep"></div>
                        <div class="camera"></div>
                        <div class="sensor"></div>
                        <div class="speaker"></div>
                            <div class="screen scrollbar" id="style-3">
                                <div class="force-overflow">
                                    <div class="mob_image">
                                        <div style="position: absolute;color: #fff;width: 50%;height: 34px;float: left;"></div>
                                        <div style="position: absolute;color: #fff;width: 50%;height: 34px;float: left;left: 50%;top:5px;">
                                        <img src="<?php echo APP_IMAGES.'ic_menu_favourite.png' ?>" style="height: 20px;width: 35px;float: right;">
                                        <img src="<?php echo APP_IMAGES.'ic_menu_sharenew.png';?>" style="height: 20px;width:35px;float:right;margin-right: 5px;">
                                        </div>
                                        <div style="position: absolute;color: #fff;background-color: #000;width: 100%;text-align: left;height: 34px;top: 127px;opacity: 0.6;">
                                            <span style="float: left;font-size: 11px;width: 100%;">
                                                <?php echo $_SESSION['app_user']['mp_details_name']; ?>
                                            </span>
                                            <span style="float: left;font-size: 11px;width: 100%;">
                                            Open from <?php echo $_SESSION['app_user']['mp_opentime']; ?> - <?php echo $_SESSION['app_user']['mp_closetime']; ?>
                                            </span>
                                       </div>
                                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="height:160px;z-index: -1;">
                                        <ol class="carousel-indicators">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img data-src="<?php echo APP_IMAGES.'bg_sample.png' ?>" alt="" src="<?php echo APP_IMAGES.'bg_sample.png' ?>" data-holder-rendered="true">
                                            </div>
                                            <div class="item">
                                                <img data-src="<?php echo APP_IMAGES.'bg_sample.png' ?>" alt="" src="<?php echo APP_IMAGES.'bg_sample.png' ?>" data-holder-rendered="true">
                                            </div>
                                            <div class="item">
                                                <img data-src="<?php echo APP_IMAGES.'bg_sample.png' ?>" alt="" src="<?php echo APP_IMAGES.'bg_sample.png' ?>" data-holder-rendered="true">
                                            </div>
                                            <div class="item">
                                                <img data-src="<?php echo APP_IMAGES.'bg_sample.png' ?>" alt="" src="<?php echo APP_IMAGES.'bg_sample.png' ?>" data-holder-rendered="true">
                                            </div>
                                            <div class="item">
                                                <img data-src="<?php echo APP_IMAGES.'bg_sample.png' ?>" alt="" src="<?php echo APP_IMAGES.'bg_sample.png' ?>" data-holder-rendered="true">
                                            </div>
                                        </div>
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                          <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                          <span class="sr-only">Next</span>
                                        </a>
                                        </div>
                                    </div>
                                    <div class="rad_menu_bar" style="">
                                         <div class="rad_one" style="background-color: #5AD0B6;">
                                         	<img src="<?php echo APP_IMAGES.'ic_tab_deal.png' ?>" class="r_img1"><span class="r_text1">The Deal</span>
                                         </div>   
                                         <div class="rad_one">
                                         	<img src="<?php echo APP_IMAGES.'ic_tab_detail.png' ?>" class="r_img2"> <span class="r_text2">Must Know</span>
                                         </div>
                                         <div class="rad_one">
                                         	<img src="<?php echo APP_IMAGES.'ic_tab_favorite.png' ?>" class="r_img3"> <span class="r_text3">Review</span>
                                         </div>
                                         <div class="rad_one">
                                         	<img src="<?php echo APP_IMAGES.'ic_tab_contact.png' ?>" class="r_img4"> <span class="r_text4">Contact</span>
                                         </div>
                                    </div>
                                    <div class="mob_name">
                                       <?php
									   echo $data[1][0]["deal_name"];
									   ?>
                                    </div>
                                    <div id="shoutout_dealdesc" style="border-bottom: 1px solid #BCBBB7;padding-left: 3px;padding-right: 3px;
    padding-bottom: 3px;"></div>
                                    <div class="offer_mobile_block" style="padding: 5px;color: #6B6A68;">
                                        <div class="offer_mobile_desc">
                                        <div class="" style="text-align: left;font-size: 11px;">
                                           Flat offer with 10% Discount.
                                        </div>
                                        <div class="" style="width: 100%;height: 30px;border-bottom: 1px solid #BCBBB7;">
                                        <div class="offer_mobile_avile1" style="font-size: 10px;border-radius: 2px;background-color: #E11723;color: #fff;padding: 3px;margin-top: 5px;float: right;">AVAIL NOW</div>
                                        </div>
                                        </div>
                                    </div>
        </div>
    </div>
    <div class="home"></div>
</div>
</div>
</div>
</div>
</div>
<div class="box_small" style="border:0; padding:0; margin:0 auto; float:left; border:1px solid;">
</div>
</div>
</div>
<!--Mobile view HTML End--->                                    
                        	</div><!--Row div close here-->
                    	</div><!--col-xs-12 div close here-->
                        <div class="col-md-8">
                            <input type="hidden" id="offer_type_array" name="offer_type_array" value=""/>
                            <input type="hidden" id="offer_desc_array" name="offer_desc_array" value=""/>
                            <input type="hidden" id="offer_original_array" name="offer_original_array" value=""/>
                            <input type="hidden" id="offer_deal_array" name="offer_deal_array" value=""/>
                            <input type="hidden" id="offer_discount_array" name="offer_discount_array" value=""/>
                            <input type="hidden" id="offer_pepole_count_array" name="offer_pepole_count_array" value=""/>
                            <input type="hidden" id="offer_save_amount_array" name="offer_save_amount_array" value="" />
                            <input type="hidden" class="image_priority" name="image_priority" value="" />
                            <input type="hidden" id="image_priority_url" name="image_priority_url" value="" />
                            <input type="hidden" id="img_selected_url" name="img_selected_url" value=""/>
                            <input type="hidden" id="upload_image_url" name="upload_image_url" class="upload_image_url" value="" />
                            <input type="hidden" id="Global_offer_array_hidden" name="Global_offer_array_hidden" value="" />
                            <input type="hidden" id="Global_offer_array_hidden_so" name="Global_offer_array_hidden_so" value="" />
                            <input type="hidden" id="add_more_offer_so_flag" name="add_more_offer_so_flag" value="0" />
                            <input type="hidden" id="main_deal_type" name="main_deal_type" value="" />
                            <input type="hidden" id="offer_type_status" name="offer_type_status" value="" />
                            <input type="hidden" id="offer_shout_open" name="offer_shout_open" value="" />
                            <input type="hidden" id="array_hidden" name="array_hidden" value="" />
                            <input type="hidden" id="global_add_array_input" name="global_add_array_input" value="" />
                            <input type="hidden" id="global_cross_array_input" name="global_cross_array_input" value="" />
                            <input type="hidden" id="global_array_img_status" name="global_array_img_status" value="0" />
                            <input type="hidden" id="name_status" name="name_status" value="0" />
                            <input type="hidden" id="image_status" name="image_status" value="0" />
                            
                            
                           <button class="btn btn-success" type="button" id="add_more_normal_offer" style="
    float: right;margin-top: 15px;width: 125px;box-shadow: none;margin-right: 300px;">Add a new offer</button>
                            <div style="float: right;margin-top: 15px;width: 125px;box-shadow: none;margin-right: 300px;text-align: center;" id="add_more_normal_offer_or">OR</div>
                            <button class="btn btn-success" type="button" id="main_submit_deal">Submit Deal</button>
                        </div> 
                    </form>  
                    </div>
<!--MODEL CODE HERE START HERE-->
<div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow:hidden;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="top: 110px;width: 500px;float: right;margin-right: 200px;">
            <div class="modal-header" style="text-align: center;font-size: 16px;padding-top: 10px;padding-bottom: 10px;font-weight: 600;">
            NOTE
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="getCode">
                
            </div>
            <input type="hidden" class="image_priority" name="image_priority" value="" />
        </div>
    </div>
</div>

<div class="modal fade" id="getCodeModal_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 460px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5ACFB5;text-align: center;font-size: 20px;padding-top: 10px;padding-bottom: 10px;">
            	ADD IMAGE
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="getCode_image" style="padding: 0px;">
                
            </div>
            <div class="modal-footer">
            <button type="button" id="m_save" class="btn btn-default" style="background-color: #E51429;border-color: #E51429;padding: 6px 30px;">
            DONE
            </button>
           <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            <input type="hidden" class="image_priority" name="image_priority" value="" />
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getCodeModal_offersdays" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 460px;">
        <div class="modal-content" style="top: 120px;width: 250px;margin-left: 100px;">
            <div class="modal-header" style="background-color: #5ACFB5;text-align: left;font-size: 16px;padding-top: 5px;padding-bottom: 5px;">Select Days			  				<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
            </div>
            <div class="modal-body" id="getCode_offersdays" style="padding: 0px;">
                <div class="model_offersdays" title="0" lang="Sun" id="1">Sunday</div>
                <div class="model_offersdays" title="0" lang="Mon" id="2">Monday</div>
                <div class="model_offersdays" title="0" lang="Tue" id="3">Tuesday</div>
                <div class="model_offersdays" title="0" lang="Wed" id="4">Wednesday</div>
                <div class="model_offersdays" title="0" lang="Thu" id="5">Thursday</div>
                <div class="model_offersdays" title="0" lang="Fri" id="6">Friday</div>
                <div class="model_offersdays" title="0" lang="Sat" id="7">Saturday</div>
            </div>
            <div class="modal-footer" style="border-top:none !important;">
            <button type="button" id="ok_day" class="btn btn-default" style="">APPLY</button>
            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-default cle1" style="">CANCEL</button>
            </div>
        </div>
    </div>
</div>
                    
<div class="modal fade" id="getCodeModal_offersdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 350px;">
        <div class="modal-content" style="top: 120px;">
            <div class="modal-header" style="background-color: #5ACFB5;text-align: center;font-size: 16px;padding-top: 8px;padding-bottom: 8px;">
            NOTE<button type="button" class="close cl1" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" id="getCode_offersdate" style="padding: 0px;">
                <div class="form-group">
                  <label class="control-label col-md-12" style="margin-bottom: 15px;">Select start date and end date range</label>
                  <div class="col-md-4">
                      <div class="input-group input-large" data-date="13/07/2013" data-date-format="dd/mm/yyyy">
                          <input type="text" class="form-control dpd1" name="from" style="width: 135px;" placeholder="Start Date">
                          <span class="input-group-addon" style="background-color: #DDDDDD;border: 1px solid #DDDDDD;">
                            <img src="<?php echo APP_IMAGES.'calander.png' ?>"/>
                          </span>
                          <input type="text" class="form-control dpd2" name="to" style="width: 135px;" placeholder="End Date">
                      </div>
                      <!--<span class="help-block">Select date range</span>-->
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top:none !important;">
            <button type="button" data-dismiss="modal" aria-label="Close" id="" class="btn btn-default cl1" style="">CANCEL</button>
            <button type="button" id="ok_custom_day" class="btn btn-default" style="">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="getCodeModal_customtime" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 460px;">
        <div class="modal-content" style="top: 120px;">
            <div class="modal-header" style="background-color: #5ACFB5;text-align: left;font-size: 16px;padding-top: 5px;padding-bottom: 5px;">
            Custome Time<button type="button" class="close cl2" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" id="getCode_customtime" style="padding: 0px;">
                <div class="form-group">
                  <label class="control-label col-md-3">Set Time</label>
                  <div>
                  <div class="col-md-4">
                      <div class="input-group bootstrap-timepicker">
                          <input type="text" class="form-control timepicker-default" id="tp1">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                            </span>
                      </div>
                  </div>
                  <div class="col-md-1" style="padding: 0px;width: 14px;padding-top: 6px;">
                      To
                  </div>
                  <div class="col-md-4">
                      <div class="input-group bootstrap-timepicker">
                          <input type="text" class="form-control timepicker-default" id="tp2">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                            </span>
                      </div>
                  </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer" style="border-top:none !important;">
            <button type="button" id="ok_custom_time" class="btn btn-default" style="background-color: #E51429;border-color: #E51429;padding: 2px 15px;margin-top: 29px;">OK</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="getCodeModal_onpage_offeredit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 460px;">
        <div class="modal-content" style="top: 120px;">
            <div class="modal-header" style="background-color: #5ACFB5;text-align: left;font-size: 16px;padding-top: 5px;padding-bottom: 5px;">
            IPL DEAL<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" id="getCode_onpage_offeredit" style="padding: 0px;">
                
            </div>
            <div class="modal-footer" style="border-top:none !important;">
            <button type="button" id="" class="btn btn-default" style="background-color: #E51429;border-color: #E51429;padding: 2px 15px;margin-top: 29px;">DELETE</button>
            <button type="button" id="" class="btn btn-default" style="background-color: #E51429;border-color: #E51429;padding: 2px 15px;margin-top: 29px;">CANCEL</button>
            <button type="button" id="" class="btn btn-default" style="background-color: #E51429;border-color: #E51429;padding: 2px 15px;margin-top: 29px;">SAVE</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content" style="top: 100px;width: 440px;float: right;margin-right: 10%;">
          <div class="modal-header" style="padding-top: 10px;padding-bottom: 10px;">
            <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
            <h4 class="modal-title">DELETED</h4>
          </div>
          <div class="modal-body">
            <div style="width: 100px;float: left;">
            <img src="<?php echo APP_IMAGES.'del.png' ?>"/>
            </div>
            <p style="color: #000;font-size: 15px;margin-top: 10px;font-weight: 500;">Are you sure want to delete this offer?</p>
          </div>
          <div class="modal-footer" style="border-top:none !important;">
           <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>-->
            <!--<button type="button" class="btn btn-danger confirm_delete" id="">Delete</button>-->
            <button class="btn btn-success btn-xs" style="padding: 2px 20px;background-color: #CFCFCF;border-color: #CFCFCF;" data-dismiss="modal">CANCEL</button>
            <button class="btn btn-success btn-xs confirm_delete" style="padding: 2px 30px;">OK</button>
          </div>
        </div>

      </div>
</div>
<!--MODEL CODE HERE END HERE-->
                  	</div> <!--panel-body div close-->
              	</section><!--section close-->
          	</div><!--col-lg-12 close-->
      	</div><!--row close-->
  	</section><!--wrapper div close-->
</section><!--main-content div close-->
</section><!--container div close-->
<?php
/*$param 				= array("mp_gallery_mp_id"=>$_SESSION['app_user']['mp_details_id']);
$data_value[0] 		= $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_mp_id =:mp_gallery_mp_id", $param);
$with_image			=	0;
$without_image		=	0;
$with_image_array				=	array();
$with_image_gallery_id_array	=	array();
$without_image_array	=	array('plus.jpg');
for($i=0;$i<count($data_value[0]);$i++)
{
	
	if($data_value[0][$i]['mp_gallery_image']	!=	"plus.jpg")
	{
		
		$with_image		=	$with_image+1;	
		array_push($with_image_array,$data_value[0][$i]['mp_gallery_image']);
		array_push($with_image_gallery_id_array,$data_value[0][$i]['mp_gallery_id']);
	}
	else
	{
		
		$without_image	=	$without_image+1;	
		array_push($without_image_array,$data_value[0][$i]['mp_gallery_image']);
	}
}

$with_image_string		=	implode("#",$with_image_array);
$with_gallery_id_string	=	implode("#",$with_image_gallery_id_array);*/
$image_array			=	array();
for($i=0;$i<count($data[3]);$i++)
{
	array_push($image_array,$data[3][$i]['deal_image_media']);
}
$with_image_string		=	implode("#",$image_array);
if($data[1][0]['deal_type']	==	1)
{
	
	$offer_type_id			=	array();
	$main_offer_array		=	array();
	
	//Offers loop
	for($i=0;$i<count($data[2]);$i++)
	{	
		if($data[2][$i]['min_billing']	==	'')
		{
			$data[2][$i]['min_billing']	=	'NULL';
		}
		if($data[2][$i]['max_capping']	==	'')
		{
			$data[2][$i]['max_capping']	=	'NULL';
		}
		if($data[2][$i]['offer_price']	==	'')
		{
			$data[2][$i]['offer_price']	=	'NULL';
		}
		
		if($data[2][$i]['offer_type_id']	==	1)
		{
	
			$fo		=	'update#flat#'.$data[2][$i]['offer_id'].'#'.$data[2][$i]['offer_discount'].'#'.$data[2][$i]['min_billing'].'#'.$data[2][$i]['offers_day_type'].'#'.$data[2][$i]['offers_day_value'].'#'.$data[2][$i]['offers_timeings'].'#'.$data[2][$i]['offer_coupon_status'].'#'.$data[2][$i]['max_capping'];
			
			array_push($main_offer_array,$fo);
		}
		if($data[2][$i]['offer_type_id']	==	2)
		{
			$co		=	'update#combo#'.$data[2][$i]['offer_id'].'#'.$data[2][$i]['offer_description'].'#'.$data[2][$i]['offer_price'].'#'.$data[2][$i]['min_billing'].'#'.$data[2][$i]['offers_day_type'].'#'.$data[2][$i]['offers_day_value'].'#'.$data[2][$i]['offers_timeings'].'#'.$data[2][$i]['offer_coupon_status'].'#'.$data[2][$i]['max_capping'];
			array_push($main_offer_array,$co);
		}
		
		
	}
	$main_offer_array_string	=	implode("$@$",$main_offer_array);
}else
{
	$offer_string	=	array();
	for($i=0;$i<count($data[2]);$i++)
	{
		if($data[2][$i]['offer_type_id']	==	1)
		{
			$fo		=	'update#flat#'.$data[2][$i]['offer_id'].'#'.$data[2][$i]['offer_discount'];
			array_push($offer_string,$fo);
		}
		if($data[2][$i]['offer_type_id']	==	2)
		{
			$co		=	'update#combo#'.$data[2][$i]['offer_id'].'#'.$data[2][$i]['offer_description'];
			array_push($offer_string,$co);
		}
	}
	$main_offer_array_string	=	implode("$@$",$offer_string);
}
?>

<input type="hidden" id="image_path" name="image_path" value="<?php if($with_image_string	==	''){ }else{ echo "#".$with_image_string; } ?>">
<input type="hidden" id="main_offer_array" name="main_offer_array" value="<?php echo $main_offer_array_string; ?>" >
<?php include(APP_VIEW.'includes/bottom.php');?>


<script src="<?php echo APP_CRM_PLUGIN; ?>ionslider/ion.rangeSlider.min.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="http://thevectorlab.net/flatlab/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo APP_JS; ?>toastr.js"></script>
<script>
var valuePercent = 10;
$(function () 
{
	
	//======================================================================
	//---------------------- Global GET/SET Variable Block ---------------//
	var Total_offer_create	=	0;
	var offer_type			=	[];
	var short_desc			=	[];
	var original_price		=	[];
	var deal_price			=	[];
	var discount			=	[];
	var deal_count			=	[];
	var array_offersdays	=	[];
	var array_sortdays		=	[];
	$(".flat_offer_div").hide();
	$(".combo_offer_div").hide();
	$(".create_button_or_no_pepole_div").hide();
	$(".day_time_offer_div").hide();
	$(".all_day_div").hide();
	$(".pariticular_date_div").hide();
	$(".pariticular_date_time_div").hide();
	$(".custom_date_time_div").hide();
	$("#offer_type").val(1);
	$(".flat_offer_div").show();
	$(".combo_offer_div").hide();
	$(".day_time_offer_div").show();
	$("#maxcap").hide();
	$(".include_price_div").hide();
	$('#add_more_normal_offer').hide();
	
	$('#add_more_normal_offer_or').hide();
	var	input_check_s	=	0;
	var img_img	=	$('#img_img').attr("src");
	$("#img_selected_url").val(img_img);
	var image_path		=	$("#image_path").val();
	$("#image_priority_url").val(image_path);
	var nu	=	$(".image_priority").val();
	$( "#m_save" ).prop( "disabled", true );
	
	$("#deal_name").keyup(function(){
		var value	=	$(this).val();
		$(".mob_name").html(value);
		$("#name_status").val(1);
	});
	
	
	
	/*$("#range_flat").change(function(){
		$("#shoutout_dealdesc").hide();
		var Global_offer_array_hidden	=	$("#Global_offer_array_hidden").val();
		if(Global_offer_array_hidden	== "")
		{
			var value	=	$(this).val();
			$(".offer_mobile_avile1").hide();
			$(".offer_mobile_desc").html('<div class="offer_mobile_desc" style="text-align: left;font-size: 11px;">Flat Offer with '+value+'% discount</div><div class="offer_mobile_avile" style="width: 100%;height: 30px;border-bottom: 1px solid #BCBBB7;"><div class="offer_mobile_avile" style="font-size: 10px;border-radius: 2px;background-color: #E11723;color: #fff;padding: 3px;margin-top: 5px;float: right;">AVAIL NOW</div></div>');
		}
	});*/
	
	/*$("#short_desc").keyup(function(){
		$("#shoutout_dealdesc").hide();
		var Global_offer_array_hidden	=	$("#Global_offer_array_hidden").val();
		if(Global_offer_array_hidden	== "")
		{
		var value		=	$(this).val();
		$(".offer_mobile_avile1").hide();
		$(".offer_mobile_desc").html('<div class="offer_mobile_desc" style="text-align: left;font-size: 11px;">'+value+'</div><div class="offer_mobile_avile" style="width: 100%;height: 30px;border-bottom: 1px solid #BCBBB7;"><div class="offer_mobile_avile" style="font-size: 10px;border-radius: 2px;background-color: #E11723;color: #fff;padding: 3px;margin-top: 5px;float: right;">AVAIL NOW</div></div>');
		}
	});*/
	
	$("#so_desc").keyup(function(){
		var main_deal_type	=	$("#main_deal_type").val();
		if(main_deal_type	==	2)
		{
			var value		=	$(this).val();
			$("#shoutout_dealdesc").html(value);
		}
	});
	
	//---------------------- Global GET/SET Variable Block ---------------//
	//===================================================================
	
	
	//===================================================================
	//---------------------- Global Function Call Block ---------------//
	$('.timepicker-default').timepicker();
	$(".dpd1").datepicker({minDate: 0,dateFormat: 'dd/mm/yy'});
	$(".dpd2").datepicker({minDate: 0,dateFormat: 'dd/mm/yy'});	
	$("#slider-range-min").slider({
        range: "min",
        value: 10,
        min: 1,
        max: 100,
        slide: function (event, ui) {
            $("#slider-range-min-amount").text(ui.value+"%");
        }
    });
	$("#slider-range-min-amount").text($("#slider-range-min").slider("value")+"%");
	with_image(image_path);
	save_new_order();
	drag_width(nu);
	$( "#images" ).sortable({'delay':'100',
		update:function(){
			save_new_order();
			$('#global_array_img_status').val('1');
			$('#image_status').val('1');
		},
    	cursor: 'move',
		placeholder: "marker",
		connectToSortable: "#images"
	});
	$('#images').draggable({ 
		cursor: "move",
		handle: "#images",
		cancel: "#images"
	 });
	$('#price').keyup(function(){
		var price = $(this).val();
			var valuePercent = $('#range').val();
			calculateValue(valuePercent, price, status = 1);
	});
	
	//---------------------- Global Function Call Block ---------------//
	//===================================================================
	
	//===================================================================
	//--------------------------- Edit Form make--------- --------------//
	$("#offer_table").hide();
	$(".offer_type_div").hide();
	$(".flat_offer_div").hide();
	$("#min_bill_div").hide();
	$("#offers_day_div").hide();
	$("#offers_timeing_div").hide();
	$("#coupon_active_inactive_div").hide();
	$(".combo_offer_div").hide();
	$("#add_offer").show();
	$("#add_offer_cancel").show();
	$("#text_head").hide();
	$("#add_more_normal_offer").hide();
	$("#add_more_normal_offer_or").hide();
	$("#main_submit_deal").hide();
	$("#offers_day_div").hide();
	$("#offers_timeing_div").hide();
	var Global_offer_array	=	[];
	var deal_type			=	'<?php echo $data[1][0]['deal_type'] ?>';
	var Total_number_offer	=	'<?php echo count($data[2]) ?>';
	
	//Normal deal
	if(deal_type	==	1)
	{
		//alert('1');
		$("#shout_out_div").removeClass('offers_class').addClass('shoutout_class');
		$("#offers_div").removeClass('shoutout_class').addClass('offers_class');
		$(".deal_desc_so").hide();
		$(".offer_deatils_text_div").html('OFFERS DETAILS');
		$(".shout_offer_div_hide").show();
		$(".shout_offer_input_value").hide();
		$("#main_deal_type").val(1);
		var o_str	=	'';
		var o_str_mobile	=	'';
		var main_offer_array	=	$("#main_offer_array").val();
		if(main_offer_array	!=	''){
			var main_offer_array_value	=	main_offer_array.split("$@$");
			var offers_id	=	[];
			var offers_type	=	[];
			var offers_desc	=	[];
			for(var i=0;i<main_offer_array_value.length;i++)
			{
			var j	=	main_offer_array_value[i].split("#");
			
			offers_id.push(j[2]);
			offers_type.push(j[1]);
			if(offers_type[i]	==	'flat')
			{
				var od	=	"Flat offer with "+j[3]+"% Discount";
				offers_desc.push(od);
			}
			if(offers_type[i]	==	'combo')
			{
				offers_desc.push(j[3]);
			}
			//alert(offers_desc);
			o_str	+=	'<div style="height: 50px;background-color: #F6F6F6;margin-bottom: 8px;" class="main_div_'+offers_id[i]+'"><div class="col-md-10" style="text-align:center;"><div class="col-md-12" style="text-align: left;font-weight: 600;padding-left: 0px;padding-top: 3px;">'+offers_type[i]+'</div><div class="col-md-12" style="text-align: left;padding-left: 0px;padding-top: 3px;">'+offers_desc[i]+'</div></div><div class="col-md-1" style="text-align: right;    padding-top: 15px;"><i class="fa fa-pencil-square-o offer_edit" lang="m-'+i+'" aria-hidden="true" style="font-size: 20px;cursor: pointer;" id="'+main_offer_array_value[i]+'"></i></div><div class="col-md-1" style="text-align: right;padding-top: 15px;"><img src="<?php echo APP_IMAGES.'cross.png' ?>"  style="cursor: pointer;" class="offer_delete" id="'+offers_id[i]+'"/></div></div><div style="height:250px;border:1px solid;display: none;" id="pannel_form_'+offers_id[i]+'" class="pannel_form"></div>';
			
			o_str_mobile	+=	'<div class="mm-'+i+'" style="text-align: left;font-size: 11px;">'+offers_desc[i]+'</div><div class="" style="width: 100%;height: 30px;border-bottom: 1px solid #BCBBB7;"><div class="offer_mobile_avile1" style="font-size: 10px;border-radius: 2px;background-color: #E11723;color: #fff;padding: 3px;margin-top: 5px;float: right;">AVAIL NOW</div></div>';
			
			
			}
		}
		$("#offer_str").html(o_str);
		$(".offer_mobile_desc").html(o_str_mobile);
	}
	//Shoutout deal
	if(deal_type	==	2)
	{
		$("#shout_out_div").removeClass('shoutout_class').addClass('offers_class');
		$("#offers_div").removeClass('offers_class').addClass('shoutout_class');
		$(".deal_desc_so").show();
		$(".offer_deatils_text_div").html('OFFERS DETAILS');
		$(".shout_offer_div_hide").hide();
		$(".shout_offer_input_value").show();
		$("#main_deal_type").val(2);
		var offers_day_value	=	'<?php  if(isset($data[1][0]["deal_shoutout_day"])){ echo $data[1][0]["deal_shoutout_day"]; } ?>';
		if(offers_day_value	=="Everyday")
		{
			$("#shoutout_deal_days_select").val(1);	
		}
		if(offers_day_value	=="Mon - Fri")
		{
			$("#shoutout_deal_days_select").val(2);	
		}
		if(offers_day_value	=="Sat - Sun")
		{
			$("#shoutout_deal_days_select").val(3);	
		}
		
		if ( offers_day_value.indexOf("/") > -1 )  
		{ 
			$('#shoutout_deal_days_select option:contains("Custom Date")').text('Custom Date ('+ offers_day_value +')');
			$("#shoutout_deal_days_select").val(5);	
		}else{
			$('#shoutout_deal_days_select option:contains("Custom Days")').text('Custom Days ('+ offers_day_value +')');
			$("#shoutout_deal_days_select").val(4);	
		}
		$("#shoutout_deal_days").val(offers_day_value);
		var offers_time_value	=	'<?php  if(isset($data[1][0]["deal_shoutout_time"])){ echo $data[1][0]["deal_shoutout_time"]; } ?>';
		$('#offers_timeings_so option:contains("Custom Time")').text('Custom Time ('+ offers_time_value +')');
		$("#offers_timeings_so").val(5);	
		$("#offers_timeings_so_hidden").val(offers_time_value);
		$("#ads").hide();
		$("#add_offer_so").show();
		var o_str	=	'';
		var o_str_mobile	=	'';
		var main_offer_array	=	$("#main_offer_array").val();
		//alert(main_offer_array)
		if(main_offer_array	!=	''){
			var main_offer_array_value	=	main_offer_array.split("$@$");
			var offers_id	=	[];
			var offers_type	=	[];
			var offers_desc	=	[];
			for(var i=0;i<main_offer_array_value.length;i++)
			{
			var j	=	main_offer_array_value[i].split("#");
			offers_id.push(j[2]);
			offers_type.push(j[1]);
			if(offers_type[i]	==	'flat')
			{
				var od	=	"Flat offer with "+j[3]+"% Discount";
				offers_desc.push(od);
			}
			if(offers_type[i]	==	'combo')
			{
				offers_desc.push(j[3]);
			}
			//alert(offers_desc);
			o_str	+=	'<div style="height: 50px;background-color: #F6F6F6;margin-bottom: 8px;" class="main_div_'+offers_id[i]+'"><div class="col-md-10" style="text-align:center;"><div class="col-md-12" style="text-align: left;font-weight: 600;padding-left: 0px;padding-top: 3px;">'+offers_type[i]+'</div><div class="col-md-12" style="text-align: left;padding-left: 0px;padding-top: 3px;">'+offers_desc[i]+'</div></div><div class="col-md-1" style="text-align: right;    padding-top: 15px;"><i class="fa fa-pencil-square-o offer_edit_so" lang="m-'+i+'" aria-hidden="true" style="font-size: 20px;cursor: pointer;" id="'+main_offer_array_value[i]+'"></i></div><div class="col-md-1" style="text-align: right;padding-top: 15px;"><img src="<?php echo APP_IMAGES.'cross.png' ?>"  style="cursor: pointer;" class="offer_delete" id="'+offers_id[i]+'"/></div></div><div style="height:250px;border:1px solid;display: none;" id="pannel_form_'+offers_id[i]+'" class="pannel_form"></div>';
			
			o_str_mobile	+=	'<div class="mm-'+i+'" style="text-align: left;font-size: 11px;">'+offers_desc[i]+'</div><div class="" style="width: 100%;height: 30px;border-bottom: 1px solid #BCBBB7;"><div class="offer_mobile_avile1" style="font-size: 10px;border-radius: 2px;background-color: #E11723;color: #fff;padding: 3px;margin-top: 5px;float: right;">AVAIL NOW</div></div>';
			
			
			}
		}
		$("#offer_str").html(o_str);
		$(".offer_mobile_desc").html(o_str_mobile);
		$("#offer_shout_open").val('hello');
	}
	
	$(document).on('click', '.offer_edit', function(){
		var m_value		=	$(this).attr("lang");
		var all_val		=	$(this).attr("id");
		var all_values	=	all_val.split("#");
		var offer_id	=	all_values[2];
		var offer_type	=	all_values[1];
		
		$('.shout_offer_div_hide').show();
		$("#text_head").show();
		$("#min_bill_div").show();
		$("#offers_day_div").show();
		$("#offers_timeing_div").show();
		$("#coupon_active_inactive_div").show();
		$("#add_offer").show();
		$("#add_offer_cancel").show();
		$("#add_more_normal_offer").hide();
		$("#add_more_normal_offer_or").hide();
		$("#main_submit_deal").hide();
		
		//flat offer
		//alert(m_value)
		if(offer_type	==	'flat')
		{
			$("#range_flat").change(function(){
				var value	=	$(this).val();
				$(".m"+m_value).html('Flat offer with '+value+'% discount');
			});
			
			$(".flat_offer_div").show();
			$(".combo_offer_div").hide();
			$("#offer_type_status").val(all_values[0]+'#'+all_values[1]+'#'+all_values[2]);
			var discount			=	all_values[3];
			var min_billing			=	all_values[4];
			if(min_billing	==	'NULL')
			{ 
				min_billing	=	''; 
			}else{
				min_billing	=	all_values[4];
			}
			var offers_day_type		=	all_values[5];
			var offers_day_value	=	all_values[6];
			var offers_timeings		=	all_values[7];
			var offer_coupon_status	=	all_values[8];
			if(offer_coupon_status	==	1)
			{
				var max_capping		=	all_values[9];
			}else{
				var max_capping		=	"";
			}
			valuePercent	=	discount;
			
			$("#range_flat").ionRangeSlider({
				min: 0,
				max: 100,
				type: 'single',
				step: 1,
				from: valuePercent,
				postfix: " %",
				keyboard: true,
				prettify: false,
				hasGrid: true,
				onChange: function (data) {
					var valuePercent = data.from;
					$("#flat_discount").val(valuePercent);
			}});
			$("#flat_discount").val(discount);
			$("#min_billing").val(min_billing);
			//alert(offers_day_type);
			if(offers_day_type	==	4)
			{
				
				$('#offers_days option:contains("Custom Days")').text('Custom Days ('+ offers_day_value +')');
				$("#offers_days").val(offers_day_type);
			}
			if(offers_day_type	==	5)
			{
				$('#offers_days option:contains("Custom Date")').text('Custom Date ('+offers_day_value+')');
				$("#offers_days").val(offers_day_type);
			}
			if((offers_day_type	==	1) || (offers_day_type	==	2) || (offers_day_type	==	3))
			{
				$("#offers_days").val(offers_day_type);
			}
			$("#offer_day_hidden_type").val(offers_day_value);
			var id_value	=	offers_timeings;
			//alert(id_value);
			$('#offers_timeings option:contains("Custom Time")').text('Custom Time ('+id_value+')');
			$("#offers_timeings").val(5);
			$("#offer_time_hidden_type").val(offers_timeings);
			$("#coupon_active_inactive").val(offer_coupon_status);
			if(offer_coupon_status	==	1)
			{
				$("#max_capping").val(max_capping);
				$("#maxcap").show();
			}
		}
		if(offer_type	==	'combo')
		{
			$("#short_desc").keyup(function(){
				var value		=	$(this).val();
				$(".m"+m_value).html(value);
			});
			
			$("#offer_type_status").val(all_values[0]+'#'+all_values[1]+'#'+all_values[2]);
			$(".flat_offer_div").hide();
			$(".combo_offer_div").show();
			var description				=	all_values[3];
			$("#short_desc").val(description);
			var include_price			=	all_values[4];
			if(include_price	==	'NULL')
			{ 
				include_price	=	''; 
			}else{
				include_price	=	all_values[4];
			}
			$("#inc_price").val(include_price);
			var min_billing			=	all_values[5];
			if(min_billing	==	'NULL')
			{ 
				min_billing	=	''; 
			}else{
				min_billing	=	all_values[5];
			}
			$("#min_billing").val(min_billing);
			var offers_day_type		=	all_values[6];
			var offers_day_value	=	all_values[7];
			var offers_timeings		=	all_values[8];
			//alert(offers_timeings);
			var offer_coupon_status	=	all_values[9];
			if(offer_coupon_status	==	1)
			{
				var max_capping		=	all_values[10];
			}else{
				var max_capping		=	"";
			}
			
			if(offers_day_type	==	4)
			{
				$('#offers_days option:contains("Custom Days")').text('Custom Days ('+ offers_day_value +')');
				$("#offers_days").val(offers_day_type);
			}
			if(offers_day_type	==	5)
			{
				$('#offers_days option:contains("Custom Date")').text('Custom Date ('+offers_day_value+')');
				$("#offers_days").val(offers_day_type);
			}
			if((offers_day_type	==	1) || (offers_day_type	==	2) || (offers_day_type	==	3))
			{
				$("#offers_days").val(offers_day_type);
			}
			$("#offer_day_hidden_type").val(offers_day_value);
			var id_value	=	offers_timeings;
			//alert(id_value)
			$('#offers_timeings option:contains("Custom Time")').text('Custom Time ('+id_value+')');
			$("#offers_timeings").val(5);
			$("#offer_time_hidden_type").val(offers_timeings);
			$("#coupon_active_inactive").val(offer_coupon_status);
			if(offer_coupon_status	==	1)
			{
				$("#max_capping").val(max_capping);
				$("#maxcap").show();
			}
		
		}
	});
	$(document).on('click', '.offer_edit_so', function(){
		var m_value	=	$(this).attr("lang");
		
		var all_val		=	$(this).attr("id");
		var all_values	=	all_val.split("#");
		var offer_id	=	all_values[2];
		var offer_type	=	all_values[1];
		$("#offer_shout_open").val(all_val);
		$('.shout_offer_div_hide').show();
		$("#text_head").show();
		$("#min_bill_div").hide();
		$("#offers_day_div").hide();
		$("#offers_timeing_div").hide();
		$("#coupon_active_inactive_div").hide();
		$("#add_offer").hide();
		$("#add_offer_cancel").hide();
		$("#add_more_normal_offer").hide();
		$("#add_more_normal_offer_or").hide();
		$("#main_submit_deal").hide();
		
		//flat offer
		//alert(m_value)
		if(offer_type	==	'flat')
		{
			$(".so_flate_offer_div").show();
			$(".so_offer_desc_div").hide();
			var discount			=	all_values[3];
			//alert(discount);
			valuePercent	=	discount;
			$("#range_flat_so").ionRangeSlider({
				min: 0,
				max: 100,
				type: 'single',
				step: 1,
				from: valuePercent,
				postfix: " %",
				keyboard: true,
				prettify: false,
				hasGrid: true,
				onChange: function (data) {
					var valuePercent = data.from;
					$("#flat_discount_so").val(valuePercent);
			}});
			$("#flat_discount_so").val(discount);
			$("#range_flat_so").change(function(){
				var value	=	$(this).val();
				$(".m"+m_value).html('Flat offer with '+value+'% discount');
			});
		}
		if(offer_type	==	'combo')
		{
			$(".so_offer_desc_div").show();
			$(".so_flate_offer_div").hide();
			var desc	=	all_values[3];
			$("#so_offer_desc").val(desc);
			$("#so_offer_desc").keyup(function(){
				var value		=	$(this).val();
				$(".m"+m_value).html(value);
			});
		}
	});
	var form_field_changed	=	[];
	var global_cross_array	=	[];
	var global_add_array	=	[];
	$("#deal_name").change(function(){form_field_changed.push("deal_name");console.log(form_field_changed)});
	$("#range_flat").change(function(){form_field_changed.push("offer_discount");console.log(form_field_changed)});
	$("#min_billing").change(function(){form_field_changed.push("min_billing");console.log(form_field_changed)});
	$("#offers_days").change(function(){form_field_changed.push("offers_day_type");console.log(form_field_changed)});
	$("#offers_timeings").change(function(){form_field_changed.push("offers_timeings");console.log(form_field_changed)});
	$("#coupon_active_inactive").change(function(){form_field_changed.push("offer_coupon_status");console.log(form_field_changed)});
	$("#max_capping").change(function(){form_field_changed.push("max_capping");console.log(form_field_changed)});
	$("#short_desc").change(function(){form_field_changed.push("offer_description");console.log(form_field_changed)});
	$("#inc_price").change(function(){form_field_changed.push("offer_price");console.log(form_field_changed)});
	$("#so_desc").change(function(){form_field_changed.push("deal_shoutout_description");console.log(form_field_changed)});
	$("#shoutout_deal_days_select").change(function(){form_field_changed.push("deal_shoutout_day");console.log(form_field_changed)});
	$("#offers_timeings_so").change(function(){form_field_changed.push("deal_shoutout_time");console.log(form_field_changed)});
	$("#range_flat_so").change(function(){form_field_changed.push("offer_discount");console.log(form_field_changed)});
	$("#so_offer_desc").change(function(){form_field_changed.push("offer_description");console.log(form_field_changed)});
	
	$(document).on('click', '#add_offer', function(){
		var add_offer_string	=	$("#offer_type_status").val();
		//alert(add_offer_string);
		var add_offer_values	=	add_offer_string.split("#");
		var status				=	add_offer_values[0];
		var type				=	add_offer_values[1];
		var o_id				=	add_offer_values[2];
		var deal_name			=	$("#deal_name").val();
		var count 				= 	($('.imageUrl[value!=""]').length);
		//alert(o_id)
		var name_status			=	$('#name_status').val();
		var image_status		=	$('#image_status').val();
		
		//alert('2');
		if(name_status	==	1)
		{
			//alert('name');
			var flat_string	=	"update#name#NULL";
			var deal_id		=	'<?php echo $data[1][0]['deal_id']; ?>';
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/createDeal/edit_deal',
				type: 'POST',
				data: {'id':flat_string,'deal_id':deal_id,'deal_name':deal_name},
				success: function (data) {
						if(data	==	1)
						{
							var shortCutFunction = 'success'; //success/info/warning/error
					 		var msg = 'Deal Name Update Successfully!';
					  		var title = 'Awesome!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 6000);
						}
						else
						{
							var shortCutFunction = 'error'; //success/info/warning/error
					 		var msg = 'Deal Name Update Successfully!';
					  		var title = 'Opps!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 5000);
						}
				},
				error: function (data) {
				}
			});
			//alert(deal_id);
			o_id	=	deal_id;
			var an_process	=	"deal_update";
		}
		if(image_status	==	1)
		{
			//alert('image');
			var flat_string	=	"update#image#NULL";
			var deal_id		=	'<?php echo $data[1][0]['deal_id']; ?>';
			var image_priority_url	=	$("#image_priority_url").val();
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/createDeal/edit_deal',
				type: 'POST',
				data: {'id':flat_string,'deal_id':deal_id,'deal_name':deal_name,'image_priority_url':image_priority_url},
				success: function (data) {
						if(data	==	1)
						{
							var shortCutFunction = 'success'; //success/info/warning/error
					 		var msg = 'Deal Images Update Successfully!';
					  		var title = 'Awesome!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 6000);
						}
						else
						{
							var shortCutFunction = 'error'; //success/info/warning/error
					 		var msg = 'Deal Images Update Successfully!';
					  		var title = 'Opps!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 5000);
						}
				},
				error: function (data) {
				}
			});
			o_id	=	deal_id;
			var an_process	=	"deal_update";
			
		}
		if(type	==	'flat')
		{
			//alert('flat');
			if(deal_name	==	"")
			{
				alert("Please enter the deal name");
				$("#deal_name").css("border-color","#E51429");
				$("#deal_name").focus();
				return false;
			}
			else{
			$("#deal_name").css("border-color","#5AD0B6");
			}
			if(count<3)
			{
				alert('Please upload atleast 3 images');
				$( "#main_submit_deal" ).removeAttr("disabled");
				return false
			}
			var coupon_active_inactive	=	$("#coupon_active_inactive").val();
			var min_billing				=	$("#min_billing").val();
			//alert(min_billing);
			var od						=	$('#offers_days').val();
			var ot						=	$('#offers_timeings').val();
			id_value					=	$('#offer_time_hidden_type').val();
			
			var id						=	$("#max_capping").val();
			if(min_billing	!=	"")
			{
				var a	=	$.isNumeric(min_billing);
				if(a){
					
				}else{
					alert('Please Enter number in Minimum Billing');
					$("#min_billing").val('');
					$("#min_billing").focus();
					return false;
				}
			}
			if(od	==	0)
			{
						alert('Please Select Offer Days');
						$("#offers_days").focus();
						$('#main_submit_deal').removeAttr("disabled");
						return false;
					}
			if(ot	==	0)
			{
						alert('Please Select Offer Duration');
						$("#offers_timeings").focus();
						$('#main_submit_deal').removeAttr("disabled");
						return false;
				
					}
			if ($("#coupon_active_inactive").is(":visible") == true)
			{
						var copoun_reuqierd	=	$("#coupon_active_inactive").val();
						if(copoun_reuqierd	==	'00')
						{
							alert('Please select Deal Type');
							$("#coupon_active_inactive").focus();
							return false;
						}
					}
			if ($("#max_capping").is(":visible") == true) { 
						if(id	!=	""){
								var a	=	$.isNumeric(id);
								if(a){
									if(id >= 0 && id <= 9)
									{
										alert('Please enter number greater than 9');
										$("#max_capping").val('');
										$("#max_capping").focus();
										return false;
									}
								}else{
									alert('Please Enter Numaric Value in Maximum Capping');
									$("#max_capping").val('');
									$("#max_capping").focus();
									return false;
								}
						}else{
							alert('Please Enter Maximum Capping');
							$("#max_capping").val('');
							$("#max_capping").focus();
							return false;
						}
					}
			var flat_discount	=	$("#flat_discount").val();
			if(min_billing	==	'')
			{
				min_billing	=	'NULL';
			}
			var od_values	=	$("#offer_day_hidden_type").val();
			var max_cap		=	$("#max_capping").val();
			if(max_cap	==	'')
			{
				max_cap	=	'NULL';
			}
			ott	=	$("#offers_timeings_so_hidden").val();
			var flat_string	=	"update#flat#"+o_id+'#'+flat_discount+'#'+min_billing+'#'+od+'#'+od_values+'#'+id_value+'#'+coupon_active_inactive+'#'+max_cap;
			
			var o_str	=	'';
			var t	=	'Falt offer';
			var tt	=	'Falt offer with '+flat_discount+'% discount';
			o_str	+=	'<div style="height: 50px;background-color: #F6F6F6;margin-bottom: 8px;" class="main_div_'+o_id+'"><div class="col-md-10" style="text-align:center;"><div class="col-md-12" style="text-align: left;font-weight: 600;padding-left: 0px;padding-top: 3px;">'+t+'</div><div class="col-md-12" style="text-align: left;padding-left: 0px;padding-top: 3px;">'+tt+'</div></div><div class="col-md-1" style="text-align: right;    padding-top: 15px;"><i class="fa fa-pencil-square-o offer_edit" aria-hidden="true" style="font-size: 20px;cursor: pointer;" id="'+flat_string+'"></i></div><div class="col-md-1" style="text-align: right;padding-top: 15px;"><img src="<?php echo APP_IMAGES.'cross.png' ?>"  style="cursor: pointer;" class="offer_delete" id="'+o_id+'"/></div></div><div style="height:250px;border:1px solid;display: none;" id="pannel_form_'+o_id+'" class="pannel_form"></div>';
			$(".main_div_"+o_id).html(o_str);
			var deal_id		=	'<?php echo $data[1][0]['deal_id']; ?>';
			var image_priority_url	=	$("#image_priority_url").val();
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/createDeal/edit_deal',
				type: 'POST',
				data: {'id':flat_string,'deal_id':deal_id,'deal_name':deal_name,'image_priority_url':image_priority_url},
				success: function (data) {
						if(data	==	1)
						{
							var shortCutFunction = 'success'; //success/info/warning/error
					 		var msg = 'Offer Update Successfully!';
					  		var title = 'Awesome!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 4000);
						}
						else
						{
							var shortCutFunction = 'error'; //success/info/warning/error
					 		var msg = 'Offer Not Update Successfully!';
					  		var title = 'Opps!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 4000);
						}
				},
				error: function (data) {
				}
			});
			
		
		var an_process	=	"offer_update";
		}
		if(type	==	'combo')
		{
			//alert('combo');
			
			//alert('100');
			if(deal_name	==	"")
			{
				alert("Please enter the deal name");
				$("#deal_name").css("border-color","#E51429");
				$("#deal_name").focus();
				return false;
			}
			else{
			$("#deal_name").css("border-color","#5AD0B6");
			}
			if(count<3)
			{
				alert('Please upload atleast 3 images');
				$( "#main_submit_deal" ).removeAttr("disabled");
				return false
			}
			var coupon_active_inactive	=	$("#coupon_active_inactive").val();
			var min_billing				=	$("#min_billing").val();
			//alert(min_billing);
			var od						=	$('#offers_days').val();
			var ot						=	$('#offers_timeings').val();
			id_value					=	$('#offer_time_hidden_type').val();
			//alert(id_value);
			var id						=	$("#max_capping").val();
			if(min_billing	!=	"")
			{
				var a	=	$.isNumeric(min_billing);
				if(a){
					
				}else{
					alert('Please Enter number in Minimum Billing');
					$("#min_billing").val('');
					$("#min_billing").focus();
					return false;
				}
			}
			if(od	==	0)
			{
						alert('Please Select Offer Days');
						$("#offers_days").focus();
						$('#main_submit_deal').removeAttr("disabled");
						return false;
					}
			if(ot	==	0)
			{
						alert('Please Select Offer Duration');
						$("#offers_timeings").focus();
						$('#main_submit_deal').removeAttr("disabled");
						return false;
				
					}
			if ($("#coupon_active_inactive").is(":visible") == true)
			{
						var copoun_reuqierd	=	$("#coupon_active_inactive").val();
						if(copoun_reuqierd	==	'00')
						{
							alert('Please select Deal Type');
							$("#coupon_active_inactive").focus();
							return false;
						}
					}
			if ($("#max_capping").is(":visible") == true) { 
						if(id	!=	""){
								var a	=	$.isNumeric(id);
								if(a){
									if(id >= 0 && id <= 9)
									{
										alert('Please enter number greater than 9');
										$("#max_capping").val('');
										$("#max_capping").focus();
										return false;
									}
								}else{
									alert('Please Enter Numaric Value in Maximum Capping');
									$("#max_capping").val('');
									$("#max_capping").focus();
									return false;
								}
						}else{
							alert('Please Enter Maximum Capping');
							$("#max_capping").val('');
							$("#max_capping").focus();
							return false;
						}
					}
			var short_desc	=	$("#short_desc").val();
			if(short_desc	==	"")
			{
				alert("Please enter the Offer description");
				$("#short_desc").focus();
				return false;
			}
			var inc_price	=	$("#inc_price").val();
			if(inc_price	!=	"")
			{
				var a	=	$.isNumeric(inc_price);
				if(a){
					
				}else{
					alert('Please Enter number in Include price');
					$("#inc_price").val('');
					$("#inc_price").focus();
					return false;
				}
			}
			if(inc_price	==	"")
			{
				inc_price	=	"NULL";
			}
			if(min_billing	==	'')
			{
				min_billing	=	'NULL';
			}
			var od_values	=	$("#offer_day_hidden_type").val();
			var max_cap		=	$("#max_capping").val();
			if(max_cap	==	'')
			{
				max_cap	=	'NULL';
			}
			ott	=	$("#offers_timeings_so_hidden").val();
			var flat_string	=	"update#combo#"+o_id+'#'+short_desc+'#'+inc_price+'#'+min_billing+'#'+od+'#'+od_values+'#'+id_value+'#'+coupon_active_inactive+'#'+max_cap;
			
			var o_str	=	'';
			var t	=	'Combo offer';
			var tt	=	short_desc;
			o_str	+=	'<div style="height: 50px;background-color: #F6F6F6;margin-bottom: 8px;" class="main_div_'+o_id+'"><div class="col-md-10" style="text-align:center;"><div class="col-md-12" style="text-align: left;font-weight: 600;padding-left: 0px;padding-top: 3px;">'+t+'</div><div class="col-md-12" style="text-align: left;padding-left: 0px;padding-top: 3px;">'+tt+'</div></div><div class="col-md-1" style="text-align: right;    padding-top: 15px;"><i class="fa fa-pencil-square-o offer_edit" aria-hidden="true" style="font-size: 20px;cursor: pointer;" id="'+flat_string+'"></i></div><div class="col-md-1" style="text-align: right;padding-top: 15px;"><img src="<?php echo APP_IMAGES.'cross.png' ?>"  style="cursor: pointer;" class="offer_delete" id="'+o_id+'"/></div></div><div style="height:250px;border:1px solid;display: none;" id="pannel_form_'+o_id+'" class="pannel_form"></div>';
			$(".main_div_"+o_id).html(o_str);
			var deal_id				=	'<?php echo $data[1][0]['deal_id']; ?>';
			var image_priority_url	=	$("#image_priority_url").val();
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/createDeal/edit_deal',
				type: 'POST',
				data: {'id':flat_string,'deal_id':deal_id,'deal_name':deal_name,'image_priority_url':image_priority_url},
				success: function (data) {
					if(data	==	1)
					{
							var shortCutFunction = 'success'; //success/info/warning/error
					 		var msg = 'Offer Update Successfully!';
					  		var title = 'Awesome!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 4000);
						}
					else
					{
							var shortCutFunction = 'error'; //success/info/warning/error
					 		var msg = 'Offer Not Update Successfully!';
					  		var title = 'Opps!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 4000);
						}
				},
				error: function (data) {
				}
			});
			
		
		
		var an_process	=	"offer_update";
		}
		$('.shout_offer_div_hide').show();
		$("#text_head").hide();
		$("#min_bill_div").hide();
		$("#offers_day_div").hide();
		$("#offers_timeing_div").hide();
		$("#coupon_active_inactive_div").hide();
		$("#flat_offer_div").hide();
		$("#add_offer").show();
		$("#add_offer_cancel").show();
		/************************************************************/
		//----------------------Admin notification-------------------//
		/************************************************************/
		unique_values = form_field_changed.filter(
						 function(a){if (!this[a]) {this[a] = 1; return a;}},
						 {});
		var new_final_values	=	(unique_values.toString());	
		console.log(new_final_values);
		var oldvalueimg = $('#global_cross_array_input').val();
		var newvalueimg	= $('#global_add_array_input').val();
		var globalvalue = $('#global_array_img_status').val();
		
		//alert(o_id);
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/profile/adminotify_dealEdit',
				type: 'POST',
				data: {"change_field":new_final_values, "offer_id":o_id , "oldvalueimg":oldvalueimg , "newvalueimg":newvalueimg , "globalvalue" : globalvalue , "an_deal_type" : '<?php echo $data[1][0]['deal_type']; ?>',"an_process":an_process},
				success: function (data) {
				console.log(data);
			},
			error: function (data) {
			}
		});
		form_field_changed	=	Array();
		
		/************************************************************/
		//----------------------Admin notification-------------------//
		/************************************************************/
	});
	$(document).on('click', '.offer_delete', function(e){
		var id	=	$(this).attr("id");
		$(".confirm_delete").attr('id',id);
		jQuery("#confirmDelete").modal('show');
	});
	$(document).on('click', '.confirm_delete', function(e){
		var id = $(this).attr('id');
		$(".main_div_"+id).hide();
		$.ajax({
				 url: '<?php echo APP_URL; ?>admin_mp/createDeal/offer_delete',
				  type: 'POST',
				  data: {'id':id},
				  success: function (data) {
					  if(data	==	1)
					  {
						  	jQuery("#confirmDelete").modal('hide');
							
							var shortCutFunction = 'success'; //success/info/warning/error
					 		var msg = 'Your Offer delete Successfully!';
					  		var title = 'Awesome!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 6000);
						
							
							
					  }else{
					  		jQuery("#confirmDelete").modal('hide');
							
							var shortCutFunction = 'error'; //success/info/warning/error
					 		var msg = 'Something Wrong !';
					  		var title = 'Opps!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 5000);
						
							
							
					  }
				  }
		 });
	});
	
	//--------------------------- Edit Form make--------- --------------//
	//===================================================================
	
	//===================================================================
	//---------------------- Global Click Function Block --------------//
	
	$(document).on('click', '#uploading', function(){
		$("#ex").hide();
		$("#up").show();
		$("#ex_line").hide();
		$("#up_line").show();
		$("#uploading").addClass("bg");
		$("#existing").removeClass("bg");
	});
	
	$(document).on('click', '#existing', function()	{
		$("#ex").show();
		$("#up").hide();
		$("#ex_line").show();
		$("#up_line").hide();
		$("#existing").addClass("bg");
		$("#uploading").removeClass("bg");
	});
	
	Oci	=	parseInt($(".image_priority").val());
	if(Oci	>=	5)
	{
		$("#nondragable").hide();
	}
	else
	{
		$("#nondragable").show();
	}
	$(document).on('click', '#m_save', function(){
		$('#global_array_img_status').val('1');
		$('#image_status').val(1);
		var upload_image_url	=	$("#upload_image_url").val();
		//alert(upload_image_url);
		if(upload_image_url	==	"")
		{
			alert("Please upload Images");
			return false;
		}
		
		
		old_count_images	=	parseInt($(".image_priority").val()) + parseInt(1);
		//alert(old_count_images);
		if(old_count_images	>=	5)
		{
			//alert('hi');
			$("#nondragable").hide();
			//alert("image_priority hide");
			//$("#dragable").css("min-width","500px");
			
		}
		var nu	=	$(".image_priority").val();
		drag_width(nu);
		
		//alert('m save');
		
		//alert(upload_image_url);
		if(IsJsonString(upload_image_url)){
			//alert('json strng');
			var obj = JSON.parse(upload_image_url);
			//alert(obj.imageName);
			//alert(obj.id);
			var imageName	=	obj.imageName;
			var id			=	obj.id;
		}else{
			//alert('json not strng');
			var imageName	=	upload_image_url;
			var id			=	1;
		}
		
		var image_path		=	$("#image_path").val();
		global_add_array.push(imageName);
		$('#global_add_array_input').val(global_add_array);
		//var image_numbering	=	$("#image_numbering").val();
		//alert(image_path);
		
		//var str1 = "ABCDEFGHIJKLMNOP";
		//var str2 = "DEFG";
		if(image_path.indexOf(upload_image_url) != -1){
			alert("You are already selected this Image");
			$("#nondragable").show();
			return false;
		}else{
			console.log("image_path="+image_path);
			//alert(image_numbering);
			if(image_path	==	'')
			{
				var new_image_path			=	'#'+imageName;
			}else{
				var new_image_path			=	image_path +'#'+imageName;
			}
			console.log("new_image_path="+new_image_path);
			$("#image_path").val(new_image_path);
			with_image_upload(new_image_path);
			save_new_order();
			jQuery("#getCodeModal_image").modal('hide');
		}
		
	});
	
	$(document).on('click', '.gallery_img', function(){
		var id	= $(this).attr("id");
		var alt	= $(this).attr("alt");
		
		console.log(id)
		$(".gallery_img").css("outline","1px solid #fff");
		$(this).css("outline","3px solid #00A8B3");
		$("#upload_image_url").val(id);
		$(".gmi").hide();
		$("#gallery_img_"+alt).show();
		$( "#m_save" ).removeAttr("disabled");
	});
	
	$("#add_image").click(function(){
		 $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/createDeal/Images_add_from_gallery',
            type: 'POST',
			data: '100',
			processData: false,
			contentType: false,
            success: function (data) {
				$("#getCode_image").html(data);
				jQuery("#getCodeModal_image").modal('show');
				$("#m_save").show();
				
			},
            error: function (data) {
            }
			});
	});
	
	
	$(document).on('click', '.btnCross', function()	{
		$('#image_status').val('1');
		var id = $(this).attr('id');
		$(this).closest("li").remove();
		var old_count_images	=	parseInt($(".image_priority").val()) + parseInt(1);
		//alert(old_count_images);	
		
		global_cross_array.push(id);
		$('#global_cross_array_input').val(global_cross_array);
		
		if(old_count_images	<=	6)
		{
			$("#nondragable").show();
			//$("#dragable").css("min-width","400px");
		}
		var image_path	=	$("#image_path").val();
		//alert(image_path);
		
		var new_image_path 		= image_path.replace("#"+id,'');
		//alert(new_image_path);
		
		$("#image_path").val(new_image_path);
		
		save_new_order();
		var nu	=	$(".image_priority").val();
		drag_width(nu);
		
		
	});
	
	$(document).on('change', 'input:file', function(){
		var id = $(this).attr('id');
		var fileName = $('#'+id).val();
		readURL(this, id, fileName);
	});
	
	$("#offer_type").change(function(){
		var index	=	$(this).val();
		var offer_type_name		=	["NODEAL","Flat Deal","Combo Deal","Shout-Out","Time Deal","Day Time Deal"];
		if(offer_type_name[index]	==	"Flat Deal")
		{
			$(".flat_offer_div").show();
			$(".combo_offer_div").hide();
			$(".day_time_offer_div").show();
			$("#coupon_active_inactive_div").show();
			$("#min_bill_div").show();
			$("#short_desc").attr("maxlength","120");
			$('.marvel-device').css('height','495px');
			$('#short_desc').val('');
			$('.mob_title').html('');
			$(".mobile_bill_descc_div").show();
		}
		if(offer_type_name[index]	==	"Combo Deal")
		{
			$(".flat_offer_div").hide();
			$(".combo_offer_div").show();
			$(".day_time_offer_div").show();
			$("#coupon_active_inactive_div").show();
			$("#min_bill_div").show();
			$("#short_desc").attr("maxlength","120");
			$('.marvel-device').css('height','495px');
			$('#short_desc').val('');
			$('.mob_title').html('');
			$(".mobile_bill_descc_div").show();
		}
		$(".create_button_or_no_pepole_div").show();
	});
	
	$(document).on('click', '#disable_deal', function(){
				var deal_id	=	$(this).val();
				$("#m_save").hide();
				$.ajax({
					url: '<?php echo APP_URL; ?>admin_mp/createDeal/disable_deal',
					type: 'POST',
					data: {'deal_id':deal_id},
					success: function (data) 
					{
						//alert(data);
						/*if(data	==	1)
						{

							$("#getCode").html('<h3 style="text-align:center;margin-top: 0px;color: #000;font-style: normal;">Your Deal Is Succefully Disabled.</h3><p style="text-align: center;color: #000;">Please create deal Now!</p>');
							jQuery("#getCodeModal").modal('hide');
						}
						else
						{
							$("#getCode").html('<h3 style="text-align:center;color:red;">Something Going Wrong.</h3><p style="text-align: center;">Please Contact To Adminstrator</p>');
							jQuery("#getCodeModal").modal('show');
						}*/
					},
					error: function (data) {
					}
				});
				
		});
		
	$("#coupon_active_inactive").change(function(){
		var i	=	$(this).val();
		//coupon reuqierd
		if(i	==	1)
		{
			$("#maxcap").show();
			$(".mob_coupon_left").css("visibility","visible");
		}
		//not required
		if(i	==	0)
		{
			$("#maxcap").hide();
			$(".mob_coupon_left").css("visibility","hidden");
			$("#max_capping").val('');
		}
	});
	
	$("#offers_days,#shoutout_deal_days_select").change(function(){
		id_value	=	$(this).val();
		if(id_value	==	 1)
		{
			$("#offer_day_hidden_type").val('Everyday');
			$("#shoutout_deal_days").val('Everyday');
			$("#day_text").html('Everyday');
		}
		if(id_value	==	 2)
		{
			$("#offer_day_hidden_type").val('Mon - Fri');
			$("#shoutout_deal_days").val('Mon - Fri');
			$("#day_text").html('Weakdays');
		}
		if(id_value	==	 3)
		{
			$("#offer_day_hidden_type").val('Sat - Sun');
			$("#shoutout_deal_days").val('Sat - Sun');
			$("#day_text").html('Weakends');
		}
		if(id_value	==	 4)
		{
			jQuery("#getCodeModal_offersdays").modal('show');
		}
		if(id_value	==	 5)
		{
			jQuery("#getCodeModal_offersdate").modal('show');
		}
	});
	
	$("#offers_timeings,#offers_timeings_so").change(function(){
		id_value	=	$(this).val();
		if(id_value	==	 1)
		{
			$("#offer_time_hidden_type").val('09:00 AM - 12:00 PM');
			$("#offers_timeings_so_hidden").val('09:00 AM - 12:00 PM');
			$("#time_text").text('09:00 AM - 12:00 PM');
		}
		if(id_value	==	 2)
		{
			$("#offer_time_hidden_type").val('12:00 PM - 03:00 PM');
			$("#offers_timeings_so_hidden").val('12:00 PM - 03:00 PM');
			$("#time_text").text('12:00 PM - 03:00 PM');
		}
		if(id_value	==	 3)
		{
			$("#offer_time_hidden_type").val('03:00 PM - 07:00 PM');
			$("#offers_timeings_so_hidden").val('03:00 PM - 07:00 PM');
			$("#time_text").text('03:00 PM - 07:00 PM');
		}
		if(id_value	==	 4)
		{
			$("#offer_time_hidden_type").val('07:00 PM - 11:00 PM');
			$("#offers_timeings_so_hidden").val('07:00 PM - 11:00 PM');
			$("#time_text").text('07:00 PM - 11:00 PM');
		}
		if(id_value	==	 5)
		{
			jQuery("#getCodeModal_customtime").modal('show');
		}
		if(id_value	==	 6)
		{
			$("#offer_time_hidden_type").val('All Day');
			$("#offers_timeings_so_hidden").val('All Day');
			$("#time_text").text('All Day');
		}
	});
	
	$(document).on('click', '.model_offersdays', function(){
		array_offersdays		=	'';
		var title_value			=	$(this).attr("title");
		var lang				=	$(this).attr("lang");
		var id					=	$(this).attr("id");
		if(title_value	==	0)
		{
			$(this).css("border","2px solid #43C4A8");
			$(this).css("background","#5AD0B6");
			$(this).css("color","#fff");
			$(this).attr("title",1);
			array_sortdays.push(id);
		}
		else
		{
			$(this).css("border","none");
			$(this).css("background","#fff");
			$(this).css("color","#000");
			$(this).attr("title",0);
			var i = array_sortdays.indexOf(id);
			
			if(i != -1) {
				array_sortdays.splice(i, 1);
			}
		}
		var array_sortdays_array	=	array_sortdays.sort(function(a, b){return a-b});
		var list = ["","Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
		for(var i=0;i<array_sortdays_array.length;i++)
		{
			//array_offersdays.push(list[array_sortdays_array[i]]);
			array_offersdays	+=	list[array_sortdays_array[i]]+'|';
		}
		//alert(array_offersdays);
		
	});
	
	$(document).on('click', '#ok_day', function(){
		if(array_offersdays.length	!= 0){
			$('#offers_days option:contains("Custom Days")').text('Custom Days ('+ array_offersdays +')');
			$("#offer_day_hidden_type").val(array_offersdays);
			$('#shoutout_deal_days_select option:contains("Custom Days")').text('Custom Days ('+ array_offersdays +')');
			$("#shoutout_deal_days").val(array_offersdays);
		}
		else
		{
			$('#offers_days').val(0);
			$("#offer_day_hidden_type").val(0);
		}
		jQuery("#getCodeModal_offersdays").modal('hide');
		//alert(array_offersdays);
		$("#day_text").text(array_offersdays);
		//$("#offers_days").change(function(){
			form_field_changed.push("offers_day_type");
			console.log(form_field_changed);
			//});
	});
	
	$(document).on('click', '#ok_custom_day', function(){
		var d1	=	$('.dpd1').val();
		var d2	=	$('.dpd2').val();
		if(d1	==	""){
			alert("Please select start date");
			$('.dpd1').focus();
			return false;
		}
		if(d2	==	""){
			alert("Please select end date");
			$('.dpd2').focus();
			return false;
		}
		$('#offers_days option:contains("Custom Date")').text('Custom Date ('+d1+' - '+d2+')');
		$("#offer_day_hidden_type").val(d1+' - '+d2);
		$('#shoutout_deal_days_select option:contains("Custom Date")').text('Custom Date ('+d1+' - '+d2+')');
		$("#shoutout_deal_days").val(d1+' - '+d2);
		jQuery("#getCodeModal_offersdate").modal('hide');
		$("#day_text").text(d1+' - '+d2);
		
		form_field_changed.push("offers_day_value");
		console.log(form_field_changed);
	});
	
	$(document).on('click', '#ok_custom_time', function(){
		var t1	=	$('#tp1').val();
		var t2	=	$('#tp2').val();
		if(t1	==	""){
			alert("Please select start Time");
			$('.tp1').focus();
			return false;
		}
		if(t2	==	""){
			alert("Please select end Time");
			$('.tp2').focus();
			return false;
		}
		$('#offers_timeings option:contains("Custom Time")').text('Custom Time ('+t1+' - '+t2+')');
		$("#offer_time_hidden_type").val(t1+' - '+t2);
		$('#offers_timeings_so option:contains("Custom Time")').text('Custom Time ('+t1+' - '+t2+')');
		$("#offers_timeings_so_hidden").val(t1+' - '+t2);
		jQuery("#getCodeModal_customtime").modal('hide');
		$("#time_text").text(t1+' - '+t2);
		
		form_field_changed.push("offers_timeings");
		console.log(form_field_changed);
		
	});
	
	$(document).on('click', '.cl1', function(){
		$('#offers_days').val(0);
		$('#shoutout_deal_days_select').val(0);
	});
	
	$(document).on('click', '.cl2', function(){
		$('#offers_timeings').val(0);
		$('#offers_timeings_so').val(0);
	});
	
	$(document).on('click', '.cle1', function(){
		$('#offers_days').val(0);
		$('#shoutout_deal_days_select').val(0);
	});
	
	$('#include_price_input').click(function(){
		input_check_status	=	$('.include_price_input').is(':checked');
		if(input_check_status	==	true)
		{
			$(".include_price_div").show();
			input_check_s	=	1;
			$("#input_chk_value").val(1);
		}
		else
		{
			$(".include_price_div").hide();
			input_check_s	=	0;
			$("#input_chk_value").val(0);
		}
	});
	
	$('.imgPIC').click(function(){
		var src	=	$(this).attr("src");
		$("#img_img").attr("src",src);
		var img_img	=	$('#img_img').attr("src");
		$("#img_selected_url").val(img_img);
	});
	
	$("#add_more_normal_offer").click(function(){
		
		var main_deal_type	=	deal_type;
		alert(deal_type)
		$('.offer_type_div').show();
		if(main_deal_type	==	1)
		{
			$('.shout_offer_div_hide').show();
			$('#add_more_normal_offer').hide();
			$('#add_more_normal_offer_or').hide();
		}
		else
		{	
			$(".so_offer_type_div").show();
			$(".so_flate_offer_div").hide();
			$(".so_offer_desc_div").hide();
			$(".so_add_more_button").show();
			$('#add_more_normal_offer').hide();
			$('#add_more_normal_offer_or').hide();
		}
	});
	
	$("#add_offer_cancel").click(function(){
		$(".offer_type_div").hide();
		$(".flat_offer_div").hide();
		$("#min_bill_div").hide();
		$("#offers_day_div").hide();
		$("#offers_timeing_div").hide();
		$("#coupon_active_inactive_div").hide();
		$(".combo_offer_div").hide();
		$("#add_offer").hide();
		$("#add_offer_cancel").hide();
		$("#text_head").hide();
	});
	
	
	//$(".deal_desc_so").hide();
	//$(".shout_offer_input_value").hide();
	//$("#main_deal_type").val(1);
	
	/*$(document).on('click', '#offers_div', function(){
		$("#shout_out_div").removeClass('offers_class').addClass('shoutout_class');
		$("#offers_div").removeClass('shoutout_class').addClass('offers_class');
		$(".deal_desc_so").hide();
		$(".offer_deatils_text_div").html('OFFERS DETAILS');
		$("#offers_day_div").show();
		$("#offers_timeing_div").show();
		$(".shout_offer_div_hide").show();
		$(".shout_offer_input_value").hide();
		$("#main_deal_type").val(1);
		var deal_name	=	$("#deal_name").val();
		if(deal_name	!= "")
		{
			alert("Your All progress is lost!");
			location.reload();
		}
	});
	$(document).on('click', '#shout_out_div', function(){
		$("#shout_out_div").removeClass('shoutout_class').addClass('offers_class');
		$("#offers_div").removeClass('offers_class').addClass('shoutout_class');
		$(".deal_desc_so").show();
		$(".offer_deatils_text_div").html('ADD MORE OFFERS');
		$("#offers_day_div").hide();
		$("#offers_timeing_div").hide();
		$(".shout_offer_div_hide").hide();
		$(".shout_offer_input_value").show();
		$("#main_deal_type").val(2);
		var deal_name	=	$("#deal_name").val();
		if(deal_name	!= "")
		{
			alert("Your All progress is lost!");
			location.reload();
		}
	});*/
	
	$(".so_offer_type_div").hide();
	$(".so_offer_desc_div").hide();
	$(".so_flate_offer_div").hide();
	$("#offer_table_so").hide();
	var Global_offer_array_hidden_so	=	[];
	
	
	//$("#add_offer_so").hide();
	$("#add_offer_so_cancel").hide();
	$(document).on('click', '#ads', function(){
		$(".so_offer_type_div").show();
		$("#add_offer_so").show();
		$("#ads").hide();
		$("#add_more_offer_so_flag").val(1);
		$("#add_offer_so_cancel").show();
	});
	$(document).on('click', '#add_offer_so_cancel', function(){
		$(".so_offer_type_div").hide();
		$("#add_offer_so").hide();
		$("#ads").show();
		$("#add_more_offer_so_flag").val(0);
		$("#add_offer_so_cancel").hide();
		$(".so_flate_offer_div").hide();
		$(".so_offer_desc_div").hide();
		$("#offer_type_so").val(" ");
		
	});
	$("#offer_type_so").change(function(){
		var type	=	$(this).val();
		if(type	==	" ")
		{
			$(".so_offer_desc_div").hide();
			$(".so_flate_offer_div").hide();
		}
		if(type	==	1)
		{
			$(".so_offer_desc_div").hide();
			$(".so_flate_offer_div").show();
		}
		if(type	==	2)
		{
			$(".so_offer_desc_div").show();
			$(".so_flate_offer_div").hide();
		}
	});
	$(document).on('click', '#add_offer_so', function(){
		var deal_id						=	'<?php echo $data[1][0]['deal_id']; ?>';
		var deal_name					=	$("#deal_name").val();
		var image_priority_url			=	$("#image_priority_url").val();
		var so_desc						=	$("#so_desc").val();
		var shoutout_deal_days			=	$("#shoutout_deal_days").val();
		var offers_timeings_so_hidden	=	$("#offers_timeings_so_hidden").val();
		var deal_values					=	deal_name+'#'+so_desc+'#'+shoutout_deal_days+'#'+offers_timeings_so_hidden;
		var count 						= 	($('.imageUrl[value!=""]').length);
		if(deal_name	==	"")
		{
			alert("Please enter the deal name");
			$("#deal_name").css("border-color","#E51429");
			$("#deal_name").focus();
			return false;
		}
		else
		{
			$("#deal_name").css("border-color","#5AD0B6");
		}
		if(count<3)
		{
			alert('Please upload atleast 3 images');
			$( "#main_submit_deal" ).removeAttr("disabled");
			return false
		}
		if(so_desc	==	"")
		{
			alert("Please enter Deal Description");
			$("#so_desc").css("border-color","#E51429");
			$("#so_desc").focus();
			return false;
		}
		else
		{
			$("#so_desc").css("border-color","#5AD0B6");
		}
		var shoutout_deal_days_select	=	$("#shoutout_deal_days_select").val();
		if(shoutout_deal_days_select	==	0)
		{
			alert("Please enter Deal days");
			$("#shoutout_deal_days_select").focus();
			return false;
		}
		var offers_timeings_so	=	$("#offers_timeings_so").val();
		if(offers_timeings_so	==	0)
		{
			alert("Please enter Deal Timeing");
			$("#offers_timeings_so").focus();
			return false;
		}
		
		var offer_shout_open	=	$("#offer_shout_open").val();
		//alert(offer_shout_open);
		if(offer_shout_open	==	'hello')
		{
			//alert('22');
			var oid	=	deal_id;
			var an_process	=	"deal_shoutout_update";
			var offer_shout_open_new	=	"";
		}else{
		var ko	=	offer_shout_open.split("#");
		var oid	=	ko[2];
		//alert(oid);
		var ott	=	ko[1];
		if(ott	=='flat')
		{
			var flat_discount_so		=	$("#flat_discount_so").val();
			off_type					=	'Falt Offer'
			off_desc					=	'Falte Offer with '+flat_discount_so+ '% discount';
			var offer_shout_open_new	=	'update#flat#'+oid+'#'+flat_discount_so;
		}
		else
		{
			off_type	=	'Combo Offer';
			off_desc	=	$("#so_offer_desc").val();
			var offer_shout_open_new	=	'update#combo#'+oid+'#'+off_desc;
		}
		
		var o_str	=	'';
		o_str	+=	'<div class="col-md-10" style="text-align:center;"><div class="col-md-12" style="text-align: left;font-weight: 600;padding-left: 0px;padding-top: 3px;">'+off_type+'</div><div class="col-md-12" style="text-align: left;padding-left: 0px;padding-top: 3px;">'+off_desc+'</div></div><div class="col-md-1" style="text-align: right;    padding-top: 15px;"><i class="fa fa-pencil-square-o offer_edit_so" lang="m-'+i+'" aria-hidden="true" style="font-size: 20px;cursor: pointer;" id="'+offer_shout_open_new+'"></i></div><div class="col-md-1" style="text-align: right;padding-top: 15px;"><img src="<?php echo APP_IMAGES.'cross.png' ?>"  style="cursor: pointer;" class="offer_delete" id="'+oid+'"/></div>';
		
		$('.main_div_'+oid).html(o_str);
			var an_process	=	"deal_update";
		}
		var image_status	=	$("#image_status").val();
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/createDeal/edit_deal_shoutout',
			type: 'POST',
			data: {'deal_values':deal_values,'image_priority_url':image_priority_url,'deal_id':deal_id,'offer_shout_open':offer_shout_open_new,'image_status':image_status},
			success: function (data) {
					//alert(data);
					if(data	==	1)
					{
							var shortCutFunction = 'success'; //success/info/warning/error
					 		var msg = 'Your Deal Update Successfully!';
					  		var title = 'Awesome!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 6000);
						}
					else
					{
							var shortCutFunction = 'error'; //success/info/warning/error
					 		var msg = 'Your Deal Not Update Successfully!';
					  		var title = 'Opps!';
								toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
					  		var $toast = toastr[shortCutFunction](msg, title); 
							window.setTimeout(function() {
								window.location.href = '<?php echo APP_URL; ?>admin_mp/deals';
							}, 5000);
						}
			},
			error: function (data) {
			}
		});
		
		unique_values = form_field_changed.filter(
						 function(a){if (!this[a]) {this[a] = 1; return a;}},
						 {});
		var new_final_values	=	(unique_values.toString());	
		var oldvalueimg = $('#global_cross_array_input').val();
		var newvalueimg	= $('#global_add_array_input').val();
		var globalvalue = $('#global_array_img_status').val();
		//alert(an_process)
		//alert('dd>'+oid);
		$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/profile/adminotify_dealEdit',
				type: 'POST',
				data: {"change_field":new_final_values, "offer_id":oid , "oldvalueimg":oldvalueimg , "newvalueimg":newvalueimg , "globalvalue" : globalvalue , "an_deal_type" : '<?php echo $data[1][0]['deal_type']; ?>',"an_process":an_process},
				success: function (data) {
				console.log(data);
			},
			error: function (data) {
			}
		});
		form_field_changed	=	Array();
		return false;
		
	});	
	$(document).on('click', '.offer_delete_so', function(){
		var id	=	$(this).attr("id");
		Global_offer_array_hidden_so.splice(id, 1);
		console.log(Global_offer_array_hidden_so);
		var o_str	=	'';
		var o_str_so_mobile	=	'';
		for(var i=0;i<Global_offer_array_hidden_so.length;i++)
		{
			$("#offer_table_so").show();
			var offer_values_so	=	Global_offer_array_hidden_so[i].split('#');
			if(offer_values_so[0]	==	1){
				offer_name	=	'Flat offer '+offer_values_so[1];
			}else{
				offer_name	=	'Combo offer '+offer_values_so[2];
			}
			o_str_so	+=	'<div style="height: 50px;background-color: #F6F6F6;"><div class="col-md-11" style="text-align:center;"><div class="col-md-12" style="text-align: left;font-weight: 600;padding-left: 0px;padding-top: 3px;">'+o_type+'</div><div class="col-md-12" style="text-align: left;padding-left: 0px;padding-top: 3px;">'+offer_name+'</div></div><div class="col-md-1" style="text-align: right;"><img src="<?php echo APP_IMAGES.'cross.png' ?>"  style="cursor: pointer;" class="offer_delete_so" id="'+i+'"/></div><div class="offer_type_values"></div></div>';
			
			o_str_so_mobile += '<div class="offer_mobile_desc" style="text-align: left;font-size: 11px;">'+offer_name+'</div><div class="offer_mobile_avile" style="width: 100%;height: 30px;border-bottom: 1px solid #BCBBB7;"><div class="offer_mobile_avile" style="font-size: 10px;border-radius: 2px;background-color: #E11723;color: #fff;padding: 3px;margin-top: 5px;float: right;">AVAIL NOW</div></div>';
		}
		$("#offer_str_so").html(o_str);
		$(".offer_mobile_desc").html(o_str_so_mobile);
		$(".offer_mobile_desc").html('');
		$("#Global_offer_array_hidden_so").val(Global_offer_array_hidden_so);
	});
	
	
	//---------------------- Global Click Function Block --------------//
	//===================================================================
	
	
	
	
	//===================================================================
	//---------------------- Form Submit Function Block --------------//
	
	
	/*$("#main_submit_deal").click(function()
	{
		var main_deal_type	=	$("#main_deal_type").val();
		var deal_name		=	$("#deal_name").val();
		var count 			= 	($('.imageUrl[value!=""]').length);
		var offer_type	=	$("#offer_type").val();
		
		if(deal_name	==	"")
		{
			alert("Please enter the deal name");
			$("#deal_name").css("border-color","#E51429");
			$("#deal_name").focus();
			return false;
		}
		else{
		$("#deal_name").css("border-color","#5AD0B6");
		}
		if(count<3)
		{
			alert('Please upload atleast 3 images');
			$( "#main_submit_deal" ).removeAttr("disabled");
			return false
		}
		if(main_deal_type	==	1) 
		{
			
			var Global_offer_array_hidden	=	$("#Global_offer_array_hidden").val();
			if(Global_offer_array_hidden	==	"")
			{
				if(offer_type	==	1){
					var coupon_active_inactive	=	$("#coupon_active_inactive").val();
					var min_billing				=	$("#min_billing").val();
					var od						=	$('#offers_days').val();
					var ot						=	$('#offers_timeings').val();
					var id						=	$("#max_capping").val();
					if(min_billing	!=	"")
					{
						var a	=	$.isNumeric(min_billing);
						if(a){
							
						}else{
							alert('Please Enter number in Minimum Billing');
							$("#min_billing").val('');
							$("#min_billing").focus();
							return false;
						}
					}
					if(od	==	0)
					{
						alert('Please Select Offer Days');
						$("#offers_days").focus();
						$('#main_submit_deal').removeAttr("disabled");
						return false;
					}
					if(ot	==	0)
					{
						alert('Please Select Offer Duration');
						$("#offers_timeings").focus();
						$('#main_submit_deal').removeAttr("disabled");
						return false;
				
					}
					if ($("#coupon_active_inactive").is(":visible") == true)
					{
						var copoun_reuqierd	=	$("#coupon_active_inactive").val();
						if(copoun_reuqierd	==	'00')
						{
							alert('Please select Deal Type');
							$("#coupon_active_inactive").focus();
							return false;
						}
					}
					if ($("#max_capping").is(":visible") == true) { 
						if(id	!=	""){
								var a	=	$.isNumeric(id);
								if(a){
									if(id >= 0 && id <= 9)
									{
										alert('Please enter number greater than 9');
										$("#max_capping").val('');
										$("#max_capping").focus();
										return false;
									}
								}else{
									alert('Please Enter Numaric Value in Maximum Capping');
									$("#max_capping").val('');
									$("#max_capping").focus();
									return false;
								}
						}else{
							alert('Please Enter Maximum Capping');
							$("#max_capping").val('');
							$("#max_capping").focus();
							return false;
						}
					}
				}
				if(offer_type	==	2){
					var coupon_active_inactive	=	$("#coupon_active_inactive").val();
					var min_billing				=	$("#min_billing").val();
					var od						=	$('#offers_days').val();
					var ot						=	$('#offers_timeings').val();
					var id						=	$("#max_capping").val();
					var short_desc				=	$("#short_desc").val();
					var	inc_price				=	$("#inc_price").val();
					if(inc_price	!=	"")
					{
						var a	=	$.isNumeric(inc_price);
						if(a){
							
						}else{
							alert('Please Enter number in Include price');
							$("#inc_price").val('');
							$("#inc_price").focus();
							return false;
						}
					}
					if(min_billing	!=	"")
					{
					var a	=	$.isNumeric(min_billing);
					if(a){
						
					}else{
						alert('Please Enter number in Minimum Billing');
						$("#min_billing").val('');
						$("#min_billing").focus();
						return false;
					}
				}
					if(short_desc	==	"")
					{
					alert('Please Enter Offer Description');
					$("#short_desc").focus();
					return false;
				}
					if(od	==	0)
					{
					alert('Please Select Offer Days');
					$("#offers_days").focus();
					$('#main_submit_deal').removeAttr("disabled");
					return false;
				}
					if(ot	==	0)
					{
					alert('Please Select Offer Duration');
					$("#offers_timeings").focus();
					$('#main_submit_deal').removeAttr("disabled");
					return false;
			
				}
					if ($("#coupon_active_inactive").is(":visible") == true)
					{
					var copoun_reuqierd	=	$("#coupon_active_inactive").val();
					if(copoun_reuqierd	==	'00')
					{
						alert('Please select Deal Type');
						$("#coupon_active_inactive").focus();
						return false;
					}
				}
					if ($("#max_capping").is(":visible") == true) { 
					if(id	!=	""){
							var a	=	$.isNumeric(id);
							if(a){
								if(id >= 0 && id <= 9)
								{
									alert('Please enter number greater than 9');
									$("#max_capping").val('');
									$("#max_capping").focus();
									return false;
								}
							}else{
								alert('Please Enter Numaric Value in Maximum Capping');
								$("#max_capping").val('');
								$("#max_capping").focus();
								return false;
							}
					}else{
						alert('Please Enter Maximum Capping');
						$("#max_capping").val('');
						$("#max_capping").focus();
						return false;
					}
				}
				}
			}
		}
		else 		
		{
			var so_desc							=	$("#so_desc").val();
			var so_offer_desc					=	$("#so_offer_desc").val();
			var od								=	$('#shoutout_deal_days_select').val();
			var ot								=	$('#offers_timeings_so').val();
			var Global_offer_array_hidden_so	=	$("#Global_offer_array_hidden_so").val();
			var add_more_offer_so_flag			=	$("#add_more_offer_so_flag").val();
			var offer_type_so					=	$("#offer_type_so").val();
			if(so_desc	==	"")
			{
				alert('Please Enter Offer Description');
				$("#so_desc").focus();
				return false;
			}
			if(od	==	0)
			{
				alert('Please Select Offer Days');
				$("#shoutout_deal_days_select").focus();
				$('#main_submit_deal').removeAttr("disabled");
				return false;
			}
			if(ot	==	0)
			{
				alert('Please Select Offer Duration');
				$("#offers_timeings_so").focus();
				$('#main_submit_deal').removeAttr("disabled");
				return false;
			}
			if(add_more_offer_so_flag	==	1)
			{
				if(Global_offer_array_hidden_so	==	"")
				{
					if(offer_type_so	==	" ")
					{
						alert("Please select shutout offer type");
						$("#offer_type_so").focus();
						return false;
					}
					if(offer_type_so	==	2){
						if(so_offer_desc	==	"")
						{
							alert('Please Enter shoutout Offer Description');
							$("#so_offer_desc").focus();
							return false;
						}	
					}
				}
			}
		}
		
		var form = $('form')[0];
		var formData = new FormData(form);
		console.log(formData);
		$.ajax({
		url: '<?php echo APP_URL; ?>admin_mp/createDeal/Create_deal',
		type: 'POST',
		data: formData,
		processData: false,
		contentType: false,
		success: function (data) {
			
			if($.isNumeric(data))
			{
				$('#message').html('');
				var success = '<div class="alert alert-success alert-dismissable" style="margin-left: 15px;"> <i class="fa fa-ban"></i> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Deal Added successfully!</span></div>';
				$('#message').html(success);
				$("html, body").animate({ scrollTop: 0 }, 600);
				window.location.replace("<?php echo APP_URL; ?>admin_mp/deals");
			}
			else
			{
				document.getElementById('main_submit_deal').disabled = false;
				$("#getCode").html(data);
				jQuery("#getCodeModal").modal('show');
			}
			},
			error: function (data) {
			}
		});
	});*/
	/*});*/
	//---------------------- Form Submit Function Block --------------//
	//===================================================================
});
/*valuePercent	= 56;
$("#range_flat").ionRangeSlider({
	min: 0,
	max: 100,
	type: 'single',
	step: 1,
	from: valuePercent,
	postfix: " %",
	keyboard: true,
	prettify: false,
	hasGrid: true,
	onChange: function (data) {
		var valuePercent = data.from;
		$("#flat_discount").val(valuePercent);
	}});*/
/*$("#range_flat_so").ionRangeSlider({
	min: 0,
	max: 100,
	type: 'single',
	step: 1,
	from: valuePercent,
	postfix: " %",
	keyboard: true,
	prettify: false,
	hasGrid: true,
	onChange: function (data) {
		var valuePercent = data.from;
		$("#flat_discount_so").val(valuePercent);
	}});*/
$("#range").ionRangeSlider({
	min: 0,
	max: 60,
	type: 'single',
	step: 1,
	from: valuePercent,
	postfix: " %",
	keyboard: true,
	prettify: false,
	hasGrid: true,
	onChange: function (data) {
		var valuePercent = data.from;
		price = $('#price').val();
		calculateValue(valuePercent, price, status = 0);
	}});
var slider = $("#range").data("ionRangeSlider");
slider.update({from:valuePercent});
function calculateValue(valuePercent, price, status){
	if((price<1)&&(status==1)){
		alert('Please enter value more than 1');
		$('#price').focus();
	}else{
		if(price>1)
		{
			$('#totValue').html('$'+price);
			var youSave = ((valuePercent*price)/100).toFixed(2);
			var priceLeft = (price-youSave).toFixed(2);
			$('#youSave').html('$'+youSave);
			$('#priceLeft').html(priceLeft);
			
			$('#original_price').val(price);
			$('#after_discount_price').val(priceLeft);
			$('#save_price').val(youSave);
			$('#combo_discount_bar').val(valuePercent);
		}
	}}
function drag_width(img_number){
	var width	=	img_number * 115;	
	$("#dragable").css("min-width",width+"px");}
function readURL(input, id, fileName) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(input.files[0]);
			fileSize = Math.round(input.files[0].size);
			var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
				if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png")
				{
					if(fileSize>10485760)
					{
						alert("Please select image less than 10 MB!");
						$('#'+id).val('');
						return false;
					}
					else
					{
						reader.onload = function (e) {
						var image  = new Image();
						image.src = e.target.result;
						
						image.onload = function () {
							var height = this.height;
							var width = this.width;
									$('#'+id+'pic').css('opacity', '0.5');
									$('#'+id+'Lod').show();
									$("#uploading_image").attr("src",e.target.result);
									
									var form1 = $('#gallery_image_form')[0];
									var formData = new FormData(gallery_image_form);
									var idd =	$("#"+id).val();
									formData.append("image", idd);
									console.log(formData);
									$.ajax({
											xhr: function() {
												var xhr = new window.XMLHttpRequest();
												xhr.upload.addEventListener("progress", function(evt) {
												  if (evt.lengthComputable) {
													var percentComplete = evt.loaded / evt.total;
													percentComplete = parseInt(percentComplete * 100);
													$("#uploading_image").attr("src",'');
													$("#uploading_image").attr("height","0px");
													$("#uploading_image").attr("width","0px");
													$('#'+id+'Lod').html(percentComplete+'%');
													console.log(percentComplete);
													$('#img_per').show();
													$('#img_per_bar').show();
													$('#img_per').html(percentComplete+'%');
													$('#img_per_bar_v').css("width",percentComplete+'%');
													
													if (percentComplete === 100) {
													}
											
												  }
												}, false);
												return xhr;
											  },
											url: '<?php echo APP_URL; ?>admin_mp/createDeal/deal_image_show',
											type: 'POST',
											data: formData,
											processData: false,
											contentType: false,
											success: function (data) {
												//alert(data);
												
												$("#upload_image_url").val(data);
												console.log(data);
												console.log(data);
												var obj = JSON.parse(data);
												console.log(obj);
												//alert('profile.php(1034)->'+data);
												//alert(id);
												////alert(obj.imageName);
												//alert('<?php //echo APP_CRM_UPLOADS_PATH; ?>');
												//$('#'+id+'Lod').hide();
	//											$('#'+id+'pic').css('opacity', '1.0');
	//											$('#'+id+'pic').attr('src', '<?php echo APP_CRM_UPLOADS_PATH; ?>'+obj.imageName);
	//											$('#'+id+'Id').val(obj.id);
	//											$('#'+id+'btn').show();
	//											$('#'+id+'Url').val(obj.imageName);
												$("#uploading_image").attr("src",'<?php echo APP_CRM_UPLOADS_PATH; ?>'+obj.imageName);
												$("#uploading_image").attr("height","185px");
												$("#uploading_image").attr("width","250px");
												$('#img_per').hide();
												$('#img_per_bar').hide();
												$( "#m_save" ).removeAttr("disabled");
											},
											error: function (data) {
											}
								   });	
								
							}
						}
					}
				}
				else
				{
					alert("Please upload jpg, png or gif format");
					$('#'+id).val('');
					return false;
				}
		}}
function unique_array(){
	var a=[1,5,1,6,4,5,2,5,4,3,1,2,6,6,3,3,2,4];
	var unique=a.filter(function(itm,i,a){
		return i==a.indexOf(itm);
	});
	if(a.length == unique.length)
	{
		alert("no need any chnage");
	}else{
		alert("need chnage");
		alert(unique);
	}}
function save_new_order(){	
	var a = [];
	$('#images').children().each(function (i) {
	a.push($(this).attr('id'));
	});
	var order = a.join('#');
	$(".image_priority").val(a.length);
	$("#image_priority_url").val(order);
	var ipu			=	$('#image_priority_url').val();
	var ipu_array	=	ipu.split('#');
	var ipu_string	=	'';
	if(ipu	!=	''){
			for(var i=0;i<ipu_array.length;i++)
			{
				if(i==0){
				var act	=	'active';
				}else{
				var act	=	'';
				}
				ipu_string	=	ipu_string + '<div class="item '+act+'"><img data-src="<?php echo APP_CRM_UPLOADS_PATH ?>'+ipu_array[i]+'" alt="" src="<?php echo APP_CRM_UPLOADS_PATH ?>'+ipu_array[i]+'" data-holder-rendered="true"></div></div>';
			}
	}
	$('.carousel-inner').html(ipu_string);}
	
function with_image(img_string){
	i_count		=	img_string;
	var array_with_img		=	i_count.split("#");
	var drag_ul_open	=	'<ul id="images" class="ui-sortable ui-draggable">';
	var 	dragable	=	'';
	for(i=1;i<array_with_img.length;i++)
	{
		var j	=	parseInt(i)+1;
		dragable	=	dragable + '<li id="'+array_with_img[i]+'" style="margin-right: 10px;width:auto;padding-right: 5px;padding-left: 5px; width:120px; float:left;"><button type="button" id="'+array_with_img[i]+'" class="btn btn-box-tool btnCross"  data-widget="remove" style="display:block;right: 0px;"><i class="fa fa-times"></i></button><label for="image'+j+'"><img id="image'+j+'pic" style="cursor:pointer;" height="80px" width="120px"  src="<?php echo APP_CRM_UPLOADS_PATH; ?>'+array_with_img[i]+'" style="border-radius: 5px;"/><i class="fa fa-spinner fa-pulse" style="display:none;position: absolute;top: 35px; left: 65px;" id="image'+j+'Lod"></i><input type="hidden" class="imageUrl" name="image'+j+'Url" id="image'+j+'Url" value="1" /><input type="hidden" name="image'+j+'Id" id="image'+j+'Id" value="" /></li>';
	}
	var drag_ul_close	=	 '</ul>';
	var html_ele	=	drag_ul_open + dragable + drag_ul_close;
	$("#dragable").html(html_ele);}
	
function with_image_upload(img_string){
	i_count		=	img_string;
	var array_with_img		=	i_count.split("#");
	var drag_ul_open	=	'<ul id="images" class="ui-sortable ui-draggable">';
	var 	dragable	=	'';
	for(i=1;i<array_with_img.length;i++)
	{
		var j	=	parseInt(i)+1;
		dragable	=	dragable + '<li id="'+array_with_img[i]+'" style="margin-right: 10px;width:auto;padding-right: 5px;padding-left: 5px; width:120px; float:left;"><button type="button" id="'+array_with_img[i]+'" class="btn btn-box-tool btnCross"  data-widget="remove" style="display:block;right: 0px;"><i class="fa fa-times"></i></button><label for="image'+j+'"><img id="image'+j+'pic" style="cursor:pointer;" height="80px" width="120px"  src="<?php echo APP_CRM_UPLOADS_PATH; ?>'+array_with_img[i]+'" style="border-radius: 5px;"/><i class="fa fa-spinner fa-pulse" style="display:none;position: absolute;top: 35px; left: 65px;" id="image'+j+'Lod"></i><input type="hidden" class="imageUrl" name="image'+j+'Url" id="image'+j+'Url" value="1" /><input type="hidden" name="image'+j+'Id" id="image'+j+'Id" value="" /></li>';
	}
	var drag_ul_close	=	 '</ul>';
	var html_ele	=	dragable;
	$("#images").html(html_ele);}
	
var prevIndex = "";
function onSelect(){
    var currIndex = document.getElementById("offers_days").selectedIndex;
    if( currIndex > 0 )
    {
        if( prevIndex != currIndex )
        {
            //alert("Selected Index = " + currIndex);
			if(currIndex	==	4)
			{
				jQuery("#getCodeModal_offersdays").modal('show');
			}
			if(currIndex	==	5)
			{
				jQuery("#getCodeModal_offersdate").modal('show');
			}
            prevIndex = currIndex;
        }
        else
        {
            prevIndex = "";
        }
    }}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;}
</script>
</body>
</html>




