<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Shoppfer | Deals</title>
    <?php include(APP_VIEW.'includes/top.php');?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{ border-top:none;}
.table>thead>tr>th{border-bottom: 0px solid #ddd;}
#example_length,#example_filter{ display:none !important;}
table.table thead .sorting{ background:none !important;}
table.table thead .sorting_asc{ background:none !important;}
.has-switch span.switch-left{background-color: #13D4A8;color: #fff;}
.has-switch span.switch-right{background-color: #FF6C5E;color: #fff;}
.has-switch > div.switch-on label{background-color: #fff !important;}
.has-switch > div.switch-off label{background-color: #fff !important;}
.has-switch label{border: 7px solid #13D4A8;}
.has-switch{ width: 65px;height: 28px;}
.has-switch > div{ width:163%;}
.has-switch > div.switch-off label{border-color: #FF6C5E;}
.has-switch span{ font-weight:400;}
.site-min-height {min-height: 0px;}

</style>

</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $deal = "active";

  include(APP_VIEW.'includes/nav.php');?>
 
  <!-- Content Wrapper. Contains page content -->
    <?php
	if($_SESSION['app_user']['mp_details_step']!=2)
	{
	?> 
    	<section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row" style="background-color: red;opacity: 0.6;color: #fff;margin-top: -15px;">
                  <div class="col-lg-12">
                      <section>
                        <h4>Profile Incomplete!</h4>
                        <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile" style="color: #000;font-weight: bold;">Click Here</a> to update your profile.</p>			
                      </section>
                  </div>
              </div>
           </section>
        </section>                
              
	<?php }else{?>
   
	<section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
					<?php
					$disabled = "";
					/*
					 for($i=0; $i<count($data); $i++)
						 {
							 			if($data[$i]['offer_status_time']!=NULL&&$data[$i]['off_id']!=NULL&&$data[$i]['offer_status']==0)
							 			{
											$disabled = 'disabled ="disabled"';
										}
										else if($data[$i]['offer_status_time']!=NULL&&$data[$i]['off_id']!=NULL&&$data[$i]['offer_status']==1)
							 			{
											$disabled = 'disabled ="disabled"';
										}
										else if($data[$i]['offer_status_time']==NULL&&$data[$i]['off_id']==NULL&&$data[$i]['offer_status']==1)
							 			{
											$disabled = 'disabled ="disabled"';
										}
						 }*/
                    	if(count($data)	==	0){
                    ?>
                    <style>.site-min-height{ min-height:0px;}</style>
                    <div style="margin: 0 auto;margin-top: 7%;">
                    	<div style="text-align: center;"><img src="<?php echo APP_IMAGES.'icon.png' ?>" style="text-align: center;"></div>
                        <div style="font-size: 30px;text-align: center;margin-top: 20px;margin-bottom: 20px;color: #4A4A4A; font-weight:600;">No Deal!</div>
                        <div style="font-size: 14px;text-align: center;margin-top: 20px;margin-bottom: 20px;">Welcome to Your Personal Deal Manager! When you create a Deal it will be displayed here.</div>
                        <div style="text-align: center;">
                            <a href="<?php echo APP_URL; ?>admin_mp/createDeal" type="button" class="btn btn-shadow btn-success" style="text-align: center;width: 370px;font-size: 20px;box-shadow: NONE;">
                           Let's Create Your First Deal!
                            </a>
                        </div>
                    </div>
                    <?php }else{ ?>
                    
                      <section class="panel">
                      		<div id="message"></div>
                          	<header class="panel-heading">
                            <span class="pull-right" style="margin-top: -10px;">
                              <?php if($disabled=="") { ?>
                              <span class="pull-right" style="margin-top: -8px;">
                          			<a href="<?php echo APP_URL; ?>admin_mp/createDeal" class=" btn btn-success btn-xs" style="padding: 3px 14px;font-size: 12px;"> Create A New Deal</a>
                                    <?php } else { ?>
                                    <button disabled class=" btn btn-success btn-xs" style="padding: 3px 14px;font-size: 12px;"> Create New Deals</button>
                                    <?php } ?>
                     			</span>
                                </span>
                              <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;width: 100%;">
    <strong>MANAGE DEALS</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;margin-bottom: 30px;"></div>
                              	
                          </header>
                          
                          	<div class="panel-body">
                          		<div class="adv-table">
                                	<!--table-bordered-->
                                    <table id="example" class="table table-striped" style="border: 1px solid #F1F2F7;">
                                    <thead style="border-bottom: 1px solid #F1F2F7;">
                                    <tr>
                                        <th style="text-align: center;font-weight: normal;">S.No.</th>
                                        <th style="text-align: left;font-weight: normal;">Deal Name</th>
                                        <th style="text-align: center;font-weight: normal;">&nbsp;</th>
                                        <th style="text-align: center;font-weight: normal;">Deal Status</th>
                                        <th style="text-align: center;font-weight: normal;">Delete Deal</th>
                                        <th style="text-align: center;font-weight: normal;">View Deals</th>
                                        <th style="text-align: center;font-weight: normal;">Edit Deal</th>
                                    </tr>
                                    </thead>
                                   <tbody>
									<?php 
									
                                    for($i=0; $i<count($data); $i++) {
                                    ?>
                                    <tr role="row">
                                        <td style="text-align: center;"><?php echo $i+1; ?></td>
                                        <td style="text-align: left;font-weight: bold;"><?php echo $data[$i]["deal_name"]; ?>
                                        <br>
                                         <?php 
                                        /*if($data[$i]['offer_status_time']!=NULL&&$data[$i]['off_id']!=NULL&&$data[$i]['offer_status']==0)
							 			{
											$text =  '<span style="color:#f00">A request to deactivate this deal has been sent. Your deal will be deactivated asap.</span>';
											
										}
										else if($data[$i]['offer_status_time']!=NULL&&$data[$i]['off_id']!=NULL&&$data[$i]['offer_status']==1)
							 			{
											$text = '<span style="color:#f00">A request to activate this deal has been sent. Your deal will be activated asap.</span>';
										}
										else if($data[$i]['offer_status_time']==NULL&&$data[$i]['off_id']==NULL&&$data[$i]['offer_status']==1)
							 			{
											$text = '<span style="color:#f00">Your deal has been created and sent for approval! It will be live soon!</span>';
										}
										else
										{
											$text = '';
										}	
										*/	
														
										if($data[$i]["deal_status"]==1)
										{
											// $active	=	'<div style="width: 83px;margin: 0 auto;" class="inactive" title="green" id="'.$data[$i]["offer_id"].'"><input type="checkbox" '.$disabled.' checked data-toggle="switch" /></div>';	 
											 $active	=	'<div style="width: 83px;margin: 0 auto;" class="inactive" title="green" id="'.$data[$i]["deal_id"].'"><input type="checkbox" '.$disabled.' checked data-toggle="switch" /></div>';	 
										}
										else
										{
											//$active	=	'<div style="width: 83px;margin: 0 auto;" class="active1" title="rad" id="'.$data[$i]["offer_id"].'"><input type="checkbox" '.$disabled.' data-toggle="switch"/></div>';
											$active	=	'<div style="width: 83px;margin: 0 auto;" class="active1" title="rad" id="'.$data[$i]["deal_id"].'"><input type="checkbox" '.$disabled.' data-toggle="switch"/></div>';
										}
										
										/*echo $text;*/
										?>
                                        </td>
                                        <td style="text-align: left;">
                                       
                                        </td>
                                        <td style="text-align: center;">
										<?php echo $active; ?>
                                        </td>
                                        <td style="text-align: center;width: 120px;">
                                         <?php if($disabled=="") { ?>
                                        <a class="delete" id="<?php echo $data[$i]["deal_id"]; ?>" style="cursor:pointer;">
                                        	<i class="fa fa-trash-o" aria-hidden="true" style="font-size: 21px;cursor:pointer;"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a style="cursor:pointer;">
                                        	<i class="fa fa-trash-o" aria-hidden="true" style="font-size: 21px;cursor:pointer;"></i>
                                        </a>
                                        <?php } ?>
                                        </td>
                                        <td style="text-align: center;">
                                        <a class="view_deals_offers" id="<?php echo $data[$i]["deal_id"]; ?>" style="cursor:pointer;">
                                      	<!--<i class="fa fa-file-o" aria-hidden="true" style="font-size: 21px;cursor:pointer;"></i>-->
                                       <img src=" <?php echo APP_IMAGES.'VIEW.png' ?>" />
                                        </a>
                                        </td>
                                        <td style="text-align: center;">
                                        <?php if($disabled=="") { ?>
                                        <a style="padding: 2px 17px;" class="btn btn-round btn-info" href="<?php echo APP_URL; ?>admin_mp/deals/edit_deal/<?php echo $data[$i]['deal_id'];?>">
                                        	EDIT
                                            </a>
                                        <?php } else { ?>
                                         <a href="#" class="btn btn-round btn-info" style="padding: 2px 17px;">
                                         EDIT
                                        <a>
                                        <?php } ?>
                                        </td>
                                    </tr>
                    <?php 
					}
				?>
                </tbody>
              </table>
                                </div>
                          </div>
                      </section>
                    
					<?php } ?>  
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!-- Modal Dialog -->
        <div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content" style="top: 100px;width: 440px;float: right;margin-right: 5%;">
              <div class="modal-header" style="padding-top: 10px;padding-bottom: 10px;">
                <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <h4 class="modal-title">Delete Deal</h4>
              </div>
              <div class="modal-body">
              	<div style="width: 100px;float: left;">
                <img src="<?php echo APP_IMAGES.'del.png' ?>"/>
                </div>
                <p style="color: #000;font-size: 15px;margin-top: 10px;font-weight: 500;">Are you sure you want to delete this deal?</p>
              </div>
              <div class="modal-footer" style="border-top:none !important;">
               <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>-->
                <!--<button type="button" class="btn btn-danger confirm_delete" id="">Delete</button>-->
                <button class="btn btn-success btn-xs" style="padding: 2px 20px;background-color: #CFCFCF;border-color: #CFCFCF;" data-dismiss="modal">No</button>
                <button class="btn btn-success btn-xs confirm_delete" style="padding: 2px 30px;">Yes</button>
              </div>
            </div>

          </div>
        </div>
        <div class="modal fade" id="disableoffer" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
          <div class="modal-dialog">
            <!--<div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Disable Offer</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure about this ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger disable_offer" id="">Disable</button>
              </div>
            </div>-->
            <div class="modal-content" style="top: 100px;width: 440px;float: right;margin-right: 5%;">
              <div class="modal-header" style="padding-top: 10px;padding-bottom: 10px;">
                <h4 class="modal-title">DISABLE OFFER</h4>
              </div>
              <div class="modal-body">
              	<div style="width: 100px;float: left;">
                <img src="<?php echo APP_IMAGES.'del.png' ?>"/>
                </div>
                <p style="color: #000;font-size: 15px;margin-top: 10px;font-weight: 500;">Are you sure want to disable this deal?</p>
              </div>
              <div class="modal-footer" style="border-top:none !important;">
                <button class="btn btn-success btn-xs can" style="padding: 2px 20px;background-color: #CFCFCF;border-color: #CFCFCF;" data-dismiss="modal" type="button">CANCEL</button>
                <button class="btn btn-success btn-xs disable_offer" style="padding: 2px 30px;">OK</button>
              </div>
            </div>
          </div>
        </div>
         <!-- Modal Dialog -->
    <?php } ?>
</section>
<!--MODEL CODE HERE START HERE-->

<div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content" style="top: 110px;width: 500px;float: right;margin-right: 200px;">
                                        <div class="modal-header" style="text-align: center;font-size: 16px;padding-top: 10px;padding-bottom: 10px;
font-weight: 600;">
                                        NOTE
                                            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                                        </div>
                                        <div class="modal-body" id="getCode">
                                            
                                        </div>
                                        <input type="hidden" class="image_priority" name="image_priority" value="" />
                                       <!-- <div class="modal-footer" style="border-top:none;">-->
                                        <!--<button type="button" id="m_save" class="btn btn-default" style="display:none;">save</button>-->
                                        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                        
                                       
                                       <!-- </div>-->
                                    </div>
                                </div>
                            </div>
<div class="modal fade" id="getCodeModal_offerview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="top: 110px;width: 700px;float: right;margin-right: 100px;">
            <div class="modal-header" style="text-align: center;font-size: 16px;padding-top: 10px;padding-bottom: 10px;
font-weight: 600;background: #fff !important;color: #000;">
			<div class="modal-content1">
            </div>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="<?php echo APP_IMAGES.'cross.png' ?>" style="margin-top: -50px;"/>
            </button>
            </div>
            
            <div class="modal-body" id="getCode_offerview" style="padding-top: 10px;">
            </div>
        </div>
    </div>
</div>
<!--MODEL CODE HERE START HERE-->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script src="<?php echo APP_CRM_BS; ?>/js/bootstrap-switch.js"></script> 
<!--<script src="http://thevectorlab.net/flatlab/js/bootstrap-switch.js"></script>-->
<script>
  $(function () {
	  
	$("[data-toggle='switch']").wrap('<div class="switch"/>').parent().bootstrapSwitch();
	
	$(document).on('click', '.delete', function(e){
		var id	=	$(this).attr("id");
		$(".confirm_delete").attr('id',id);
		jQuery("#confirmDelete").modal('show');
	});
	
	$(document).on('click', '.confirm_delete', function(e){
		var deal_id = $(this).attr('id');
		$.ajax({
				  url: '<?php echo APP_URL; ?>admin_mp/deals/delete_deals',
				  type: 'POST',
				  data: {deal_id : deal_id},
				  success: function (data) {
					  if(data	==	1)
					  {
						  	jQuery("#confirmDelete").modal('hide');
							$('#message').html('');
							var success = '<div class="alert alert-success alert-dismissable" style="margin-left: 15px;"> <i class="fa fa-ban"></i> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Offer Delete successfully!</span></div>';
							$('#message').html(success);
							window.setTimeout(function(){location.reload()},1000);
							$("html, body").animate({ scrollTop: 0 }, 600);
					  }else{
					  		jQuery("#confirmDelete").modal('hide');
							$('#message').html('');
							var success = '<div class="alert alert-success alert-dismissable" style="margin-left: 15px;"> <i class="fa fa-ban"></i> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>There is some Problme please contat to Admin!</span></div>';
							$('#message').html(success);
							window.setTimeout(function(){location.reload()},3000);
							$("html, body").animate({ scrollTop: 0 }, 600);
					  }
				  }
		 });
	});
	
	$(document).on('click', '.disable_offer', function(e){
		var deal_id	=	$(this).attr("id");
		$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/createDeal/disable_deal',
				 type: 'POST',
				 data: {'deal_id':deal_id},
				 success: function (data) {
					// alert(data);
					 location.reload();
				 }
		});
	});
	
	$(document).on('click', '.inactive', function(e){
		var id	=	$(this).attr("id");
		var dis = $('#'+id+' input').attr("disabled");
		if(dis=="disabled")
		{
			return false;
		}
		else
		{
			$(".disable_offer").attr('id',id);
			jQuery("#disableoffer").modal('show');
		}
	});
	
	$(document).on('click', '.can', function(e){
		$(".inactive").html('<div class="switch has-switch"><div class="switch-animate switch-on"><input type="checkbox" checked="" data-toggle="switch"><span class="switch-left">ON</span><label>&nbsp;</label><span class="switch-right">OFF</span></div></div>');
	});
	
	
	
	
	var global_offer_id	=	'';
	$(document).on('click', '.active1', function(e){
		var deal_id	=	$(this).attr("id");
		var dis = $('#'+deal_id+' input').attr("disabled");
		
		if(dis=="disabled")
		{
			return false;
		}
		else
		{
			global_offer_id	=	deal_id;
			$.ajax({
					 url: '<?php echo APP_URL; ?>admin_mp/deals/active_inactive',
					 type: 'POST',
					 data: {'deal_id':deal_id},
					 success: function (data) {
						if(data	==	1){
							 location.reload();
						}else{
						 $("#getCode").html(data);
						 jQuery("#getCodeModal").modal('show');
						}
					 }
			});
		}
	});
	
	$(document).on('click', '.popupcancel', function(e){
		var idd	=	global_offer_id;
		
		
		
		$("#"+idd).html('<div class="switch has-switch"><div class="switch-animate switch-off"><input type="checkbox" data-toggle="switch"><span class="switch-left">ON</span><label>&nbsp;</label><span class="switch-right">OFF</span></div></div>');
	});
	
	
	
	  
	//-------------Disable Deal-------------------------------//
	$(document).on('click', '#active_inavtive_deal', function(){
			var id_value	=	$(this).val();
			var id_val		=	id_value.split("-");
			var deal_id		=	id_val[0];
			var status		=	id_val[1];
			
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/createDeal/disable_deal',
				type: 'POST',
				data: {'deal_id':deal_id},
					success: function (data) {
					//alert(data);
					if(data	==	1)
					{
						if(data	==	1)
						{
							$.ajax({
								url: '<?php echo APP_URL; ?>admin_mp/createDeal/enable_deal',
								type: 'POST',
								data: {'deal_id':global_offer_id},
									success: function (data) {
									if(data	==	1)
									{
										//$("#getCode").html('<h3 style="text-align:center;">This Offer Is Succssfully Enabled</h3>');
										//jQuery("#getCodeModal").modal('show');
										location.reload();
									}
									else
									{
									}
								},
								error: function (data) {
								}
							});
						}else{
							alert('response Inactive');
						}
					}
					else
					{
						$("#getCode").html('<h3 style="text-align:center;">Something Going Wrong.</h3><p style="text-align: center;">Please Contact To Adminstrator</p>');
						jQuery("#getCodeModal").modal('show');
					}
				},
					error: function (data) {
				}
			});
			
	});
	//-------------Disable Deal-------------------------------//  
	
	$(document).on('click', '.view_deals_offers', function(){
		var id 	=	$(this).attr("id");
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/createDeal/view_deal_offers',
			type: 'POST',
			data: {'deal_id':id},
			success: function (data) {
				//alert(data);
				var d	=	data.split("#$");
				$(".modal-content1").html(d[0]);
				$("#getCode_offerview").html(d[1]);
				jQuery("#getCodeModal_offerview").modal('show');		
			},
			error: function (data) {
			}
		});
	});
	
	  
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
