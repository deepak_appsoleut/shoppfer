<?php 
	$dateArray = array();
	$values = array();
	foreach($data[0] as $arr=>$val){
		foreach($val as $key=>$val1)
		{
			if($key=="label")
			{
				array_push($dateArray, $val1);
			}
			else if($key=="cnt")
			{
				array_push($values, $val1);
			}
		}
	}
?>
<div class="row">
      	<div class="col-md-12">
        <div class="box">
              <div class="box-header with-border" style="background: #242a30;">
              <h3 class="box-title" style="color:#fff; font-size:20px;font-weight: normal; font-size:14px">Interactive Area Chart for Showing Bookings</h3>
            </div>
              <div class="box-body">
              <div id="interactive" style="height: 300px;"></div>
            </div>
        </div>
        </div>
</div>
<script type="text/javascript">
$(function () {
	var xCategories = <?php echo json_encode($dateArray)?>;
	 $('#interactive').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        legend: {
			align: 'center',
            verticalAlign: 'top',
            y: 0,
            layout: 'vertical',
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
			 labels: {
                formatter: function() {
                    return xCategories[this.value];
                }
            },
			startOnTick: false,
            endOnTick: false,
            minPadding: 0,
            maxPadding: 0
        },
        yAxis: {
            title: {
                text: 'Number of Bookings'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.key}</span>: <b>{point.y}</b><br/>'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            },
			line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Bookings',
            data: <?php echo json_encode($values, JSON_NUMERIC_CHECK); ?>
        }]
    });
});
</script>     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>