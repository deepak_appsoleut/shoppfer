<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Payment Details</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>datatables/dataTables.bootstrap.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-white sidebar-mini">
<div class="wrapper">
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $payment = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment Details
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo APP_URL ?>admin_mp/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Payment Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
	  if($_SESSION['app_user']['mp_details_step_complete']==0)
	  {
	  ?>      
      <div class="row">
      	<div class="col-md-12">
        <div class="callout callout-danger">
                <h4>Profile Incomplete!</h4>
                <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile">Click Here</a> to update your profile.</p>
              </div>
        </div>
      </div>
      <?php } else { 
	  ?>
      <div class="row">
      	<div class="col-xs-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Invoice</a></li>
              <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">Payment History</a></li>
            </ul>
            <div class="tab-content">
              
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_2">
                <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Invoice ID</th>
                  <th>Invoice Date</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
               <tbody>
                <?php 
				for($i=0; $i<count($data); $i++) {
					if($data[$i]["invoice_status"]==1)
                    {
                        $active = "<span class='badge bg-green'>Paid</span>";
                    }
                    else
                    {
                        $active = "<span class='badge bg-red'>Pending</span>";
                    }
					?>
					<tr role="row">
                    <td><?php echo $data[$i]["invoice_id"]; ?></td>
                    <td><?php echo date('M j, Y', strtotime($data[$i]["invoice_created_on"])); ?></td>
                    <td>$ <?php echo number_format($data[$i]["invoice_amount"], 2);?></td>
                    <td><?php echo $active; ?></td>
                    <td></td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_1">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Invoice ID</th>	
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Earnings</th>
                  <th>Payment Date</th>
                </tr>
                </thead>
               <tbody>
                <?php 
				for($i=0; $i<count($data); $i++) {
					?>
					<tr role="row">
                    <td><?php echo $data[$i]["invoice_id"]; ?></td>
                    <td><?php echo date('M j, Y', strtotime($data[$i]["invoice_start_date"])); ?></td>
                    <td><?php echo date('M j, Y', strtotime($data[$i]["invoice_end_date"])); ?></td>
                    <td>$ <?php echo number_format($data[$i]["invoice_amount"], 2); ?></td>
                    <td><?php 
					if(($data[$i]["invoice_pay_date"]!=NULL))
					{
						echo date('M j, Y', strtotime($data[$i]["invoice_pay_date"])); 
					} 
					else
					{
						echo "<span class='badge bg-red'>Pending</span>";
					}
					?>
                    </td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
      </div>
      <?php }?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include(APP_VIEW.'includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<!-- DataTables -->
<script src="<?php echo APP_CRM_PLUGIN; ?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
	 $(document).on('click', '.addDeal', function(e)
	{
		var deal_id = $(this).attr('id');
		var name = $(this).data('name');
		$.ajax({
				  url: '<?php echo APP_URL; ?>admin_mp/deals/getDeal',
				  type: 'POST',
				  data: {name : name, deal_id : deal_id},
				  success: function (data) {
					  if(data==1)
					  {
						if(name=='remove')
						{
							$('#'+deal_id).removeClass('btn btn-danger addDeal');
							$('#'+deal_id).html('Get Deal');
							$('#'+deal_id).addClass('btn btn-warning addDeal');
						}
						else
						{
							$('#'+deal_id).removeClass('btn btn-warning addDeal');
							$('#'+deal_id).html('Remove Deal');
							$('#'+deal_id).addClass('btn btn-danger addDeal');
						}
					  }
				  },
				  error: function (data) {
				  }
		 });	
	});
	  
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
	$('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
