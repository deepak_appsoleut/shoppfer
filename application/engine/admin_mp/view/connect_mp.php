<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
  <link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/bootstrap-multiselect.css">
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>imagepicker/image-picker.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
#loading {
    position: absolute;
	display:none;
	top:5%;
    left: 50%;
    z-index: 999999;
}
.image_picker_image
{
	cursor:pointer;
}
</style>
</head>
<body class="hold-transition skin-white sidebar-mini">
<div class="wrapper">
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $connect = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="page-header">
    <div class="row">
    <div class="col-md-6">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <strong>Home</strong> - All MP</h4>
						</div>
				</div>
                </div>
    <div class="col-md-6 pull-right">            
    <select id="imageSelect" multiple class="image-picker show-html">
    <?php for($i=0; $i<count($data); $i++) { ?>
      <option data-img-src="<?php echo APP_IMAGES.$data[$i]['category_name']; ?>.png" value="<?php echo $data[$i]['category_id']; ?>"><?php echo $data[$i]['category_name']; ?></option>
	<?php } ?>
</select>  
</div>
</div>
</div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     <?php if($_SESSION['app_user']['role_type_id']==NULL) { ?>
      
      <?php } ?>
      <?php if($_SESSION['app_user']['role_type_id']==1) { 
	  if($_SESSION['app_user']['hotel_details_step_complete']==0)
	  {
	  ?>      
      <div class="row">
      	<div class="col-md-12">
        <div class="callout callout-danger">
                <h4>Profile Incomplete!</h4>
                <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile">Click Here</a> to update your profile.</p>
              </div>
        </div>
      </div>
      <?php } else { ?>
            <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x" style="margin-right:10px;"></i>Loading Data....</div>
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_1">
                  <div class="row">
                    <div class="col-xs-12">
                    		<div class="row" style="margin-bottom:10px">
                            	<div class="col-md-2">
                                	<h5><strong>Refine Your Search</strong></h5>
                                </div>
                                <div class="col-md-2">
                                	<select id="category" multiple class="form-control input-sm" name="category[]">
                                    	<?php for($i=0; $i<count($data); $i++) { ?>
                                        	<option value="<?php echo $data[$i]['category_id']; ?>"><?php echo $data[$i]['category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                	<select id="sort" class="form-control input-sm" name="sort">
                                    	<option value="dist_asc" selected>Sort By Distance(Smallest)</option>
                                        <option value="dist_desc">Sort By Distance(Highest)</option>
                                    	<option value="rat_asc">Sort By Rating(Smallest)</option>
                                        <option value="rat_desc">Sort By Rating(Highest)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <div id="map_canvas1" style="height:350px;"></div>
                                </div>
                            </div>
                            <div id="myData1">
                            </div>
                    </div>
                  </div>
              </div>
      <?php } }?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include(APP_VIEW.'includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-multiselect.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>imagepicker/image-picker.js"></script>
<script>
var myId = "";
var myCategory = "";
function getData(id, category, sortingValue)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/connect_mp/getLocationDataHotels',
            type: 'POST',
			data: {"tab" : id, "category" : category, "sortingValue" : sortingValue},
            success: function (data) {
				$('#loading').hide();
					$('#myData1').html(data);
					myId = "1";
					myCategory = category;
					initialize();
            },
            error: function (data) {
            }
   	});
	
	/*$.ajax({
            url: '<?php //echo APP_URL; ?>admin/connect_mp/connect_mp_all',
            type: 'POST',
			data: {},
            success: function (data) {
					$('#tab_1').html('');
					$('#tab_2').html('');
					$('#tab_3').html('');
					$('#loading').hide();
					$('#'+id).html(data);
            },
            error: function (data) {
            }
   	});*/
}

var markers = [];
        // Map options
     function initialize() {
		    var options = {
            zoom: 11,
            center: new google.maps.LatLng(28.6139391,77.2090212), // Centered
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            mapTypeControl: false
        };

        // Init map
        var map = new google.maps.Map(document.getElementById('map_canvas'+myId), options);
		var url = '<?php echo APP_URL; ?>admin_mp/connect_mp/getLocation'+myId;		
        $.ajax({
            url: url,
			type : "POST",
			data: {"category" : myCategory},
            success:function(data){
				console.log(data);
                var obj = JSON.parse(data);
                var totalLocations = obj.length;
				var image = {
					url: "<?php echo APP_IMAGES; ?>hotel.png", // url
					scaledSize: new google.maps.Size(50, 50)
				};
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<?php echo $_SESSION['app_user']['hotel_details_latitude']; ?>,<?php echo $_SESSION['app_user']['hotel_details_longitude']; ?>),
                        map: map,
                        title: '<?php echo $_SESSION['app_user']['hotel_details_name']; ?>',
						icon: image
                    });
				// Process multiple info windows
                    (function(marker, i) {
                        // add click event
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow = new google.maps.InfoWindow({
                                content: '<p><?php echo $_SESSION['app_user']['hotel_details_name']; ?></p><p><?php echo $_SESSION['app_user']['hotel_details_phone']; ?></p>'
                            });
                            infowindow.open(map, marker);
                        });
                    })(marker, i);
                for (var i = 0; i < totalLocations; i++) {
					/*
					if(obj[i].hotel_mp_relation_hotel_id == <?php //echo $_SESSION['app_user']['hotel_details_id']; ?>)
					{
						var imageDisplay = '<?php //echo APP_IMAGES; ?>location-pointer-green.png';
					}
					else
					{
						var imageDisplay = '<?php //echo APP_IMAGES; ?>location-pointer.png';
					}*/
					
                    // Init markers
					var image = {
					url: '<?php echo APP_IMAGES; ?>category-'+obj[i].mp_details_category+'.png', // url
					scaledSize: new google.maps.Size(50, 50)
				};
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(obj[i].mp_details_latitude, obj[i].mp_details_longitude),
                        map: map,
                        title: obj[i].mp_details_name,
						icon: image
                    });
                    // Process multiple info windows
                    (function(marker, i) {
                        // add click event
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow = new google.maps.InfoWindow({
                                content: '<p><strong>'+obj[i].mp_details_name+'</strong></p><p>'+obj[i].mp_details_city+'</p>'
                            });
                            infowindow.open(map, marker);
                        });
                    })(marker, i);
					markers.push(marker);
                }
            }
        });
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	$(function()
	{
		getData('tab_1', '', '');
		$('#category').multiselect();
		$('#imageSelect').change(function()
		{
			var category = $(this).val();
			var sortValue = $('#sort').val();
			getData('tab_1', category, sortValue);
		});
		$('#sort').change(function()
		{
			var sortValue = $(this).val();
			var category = $('#category').val();
			getData('tab_1', category, sortValue);
		});
		$("#imageSelect").imagepicker()
	});
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
