<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>daterangepicker/daterangepicker-bs3.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
#loading {
    display: none;
    position: absolute;
    top: 25%;
    left: 50%;
    z-index: 100;
}
</style>
</head>
<body class="hold-transition skin-white sidebar-mini">
<div class="wrapper">
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $revenue = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="row">	
    	<div class="col-md-3">
          <h3 style="margin:0; padding:0">Revenue Management</h3>
        </div>
        <!--<div class="col-md-2">
              <div class="form-group">
              <select name="dateSelect" id="dateSelect" class="form-control input-sm">
                  <option value="t" selected>Today</option>
                  <option value="y">Yesterday</option>
                  <option value="w">Weekly</option>
                  <option value="m">Monthly</option>
                  <option value="q">Quaterly</option>
                  <option value="c">Custom Date</option>
              </select>
              </div>
       </div>-->
        <div class="col-md-5">
        	<div class="row" id="customDate">
            <div class="col-md-6">
        	 <div class="form-group">
             	<input name="datefrom" id="config-demo" placeholder="Please pick date here" class="form-control input-sm" type="text">
             </div>
             </div>
             <div class="col-md-3">
        	 <div class="form-group">
             	<button type="submit" class="btn btn-primary btn-block" id="filter">Filter</button>
             </div>
             </div>
             </div>
        </div>
        </div>
      <ol class="breadcrumb">
        <li><a href="<?php echo APP_URL ?>admin_mp/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Deal Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     
      <?php
	  if($_SESSION['app_user']['mp_details_step_complete']==0)
	  {
	  ?>      
      <div class="row">
      	<div class="col-md-12">
        <div class="callout callout-danger">
                <h4>Profile Incomplete!</h4>
                <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile">Click Here</a> to update your profile.</p>
              </div>
        </div>
      </div>
      <?php } else { ?>
      <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x" style="margin-right:10px;"></i>Loading Data....</div>
      <div id="data">
      </div>
      <?php  }?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include(APP_VIEW.'includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<!-- DataTables -->
<script src="<?php echo APP_CRM_PLUGIN; ?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>daterangepicker/moment.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>daterangepicker/daterangepicker.js"></script>
<script>

function showData(startDate, endDate)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/revenue_mp/revenue_data',
            type: 'POST',
			data: {"startDate" : startDate, "endDate" : endDate},
            success: function (data) {
				$('#loading').hide();
				console.log(data);
				$('#data').html(data);
            },
            error: function (data) {
            }
   	});
}
	   $('#config-demo').daterangepicker(
	   {
			 ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			'Last 7 Days': [moment().subtract('days', 6), moment()],
			'Last 30 Days': [moment().subtract('days', 29), moment()],
			'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
			'Last Quarter': [moment().subtract('month', 3).startOf('month'), moment().subtract('month', 1).endOf('month')]
				},
				 format: 'YYYY/MM/DD',
	   }
	   );

  $(function () {
	  var d = new Date();
	  
	  var month1 = d.getMonth();
	  var day1 = d.getDate();
	  var startDate = d.getFullYear() +'/' + ((''+month1).length<2 ? '0' : '') + month1 + '/' + ((''+day1).length<2 ? '0' : '') + day1;
	   
	  var month = d.getMonth()+1;
	  var day = d.getDate();
      var endDate = d.getFullYear() +'/' + ((''+month).length<2 ? '0' : '') + month + '/' + ((''+day1).length<2 ? '0' : '') + day;
	  showData(startDate, endDate);
	  $('#filter').click(function()
	  {
		  var dateSelect = $('#config-demo').val();
		  var res = dateSelect.split("-");
		  showData(res[0], res[1]);
	  });
	 /* 
	 $(document).on('click', '.addDeal', function(e)
	{
		var deal_id = $(this).attr('id');
		var name = $(this).data('name');
		$.ajax({
				  url: '<?php //echo APP_URL; ?>admin/deals/getDeal',
				  type: 'POST',
				  data: {name : name, deal_id : deal_id},
				  success: function (data) {
					  if(data==1)
					  {
						if(name=='remove')
						{
							$('#'+deal_id).removeClass('btn btn-danger addDeal');
							$('#'+deal_id).html('Get Deal');
							$('#'+deal_id).addClass('btn btn-warning addDeal');
						}
						else
						{
							$('#'+deal_id).removeClass('btn btn-warning addDeal');
							$('#'+deal_id).html('Remove Deal');
							$('#'+deal_id).addClass('btn btn-danger addDeal');
						}
					  }
				  },
				  error: function (data) {
				  }
		 });	
	});*/
	  
   
  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
