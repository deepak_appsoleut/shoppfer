<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Redeem Voucher</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $redeem = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  	<?php
	if($_SESSION['app_user']['mp_details_step']!=2)
	{
	?> 
    	<section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row" style="background-color: red;opacity: 0.6;color: #fff;margin-top: -15px;">
                  <div class="col-lg-12">
                      <section>
                        <h4>Profile Incomplete!</h4>
                        <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile" style="color: #000;font-weight: bold;">Click Here</a> to update your profile.</p>			
                      </section>
                  </div>
              </div>
           </section>
        </section>                
              
	<?php }else{?>
  
  <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
			<?php
            	//if(count($data[2])	==	0){
            ?>
             <!-- <style>.site-min-height{ min-height:0px;}</style>
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>REDEEM VOUCHER</strong></h3>
<div style="height: 5px;width: 60px;background: #5AD0B6;"></div>                           
                          </header>
                          
                    <div style="margin: 0 auto;margin-top: 7%;">
                    	<div style="text-align: center;"><img src="<?php //echo APP_IMAGES.'redeem.png' ?>" style="text-align: center;"></div>
                        <div style="font-size: 21px;text-align: center;margin-top: 20px;margin-bottom: 20px;">Welcome to Deal Manager</div>
                        <div style="text-align: center;">
                            <a href="<?php //echo APP_URL; ?>admin_mp/createDeal" type="button" class="btn btn-shadow btn-success" style="text-align: center;
width: 370px;font-size: 20px;box-shadow: none;background-color: #fff;color: #2d2d2d;border-color: #000;">
                            CREATE YOUR FIRST DEAL
                            </a>
                        </div>
                    </div>
                    </section>
                    </div>
                    </div>-->
            <?php
				//}else{
			?>  
            	<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>Redeem Voucher</strong></h3>
<div style="height: 5px;width: 60px;background: #5AD0B6;"></div>                           
                          </header>
                          <div class="panel-body" style="padding-top: 0px;">
                               <h6 style=" font-weight:600;">Search for Customer via Voucher Number.</h6>
                               <div class="input-group m-bot15">
                                              <input type="text" placeholder="Add a Voucher Number to search for customer" id="voucher" class="form-control"	>
                                              <span class="input-group-btn">
                                                <button class="btn btn-primary" type="button" id="search" style="width: 170px;">Search</button>
                                              </span>
                                          </div>
                          </div>
                          </section>
                          <section class="panel">
                          	<div class="pro-sort pull-right">
                                  <select class="form-control" id="sort" style="display:inline; width:auto; height:30px; padding:2px; font-size:14px;    border: 1px solid #fff;">
                                      <option value="default">All</option>
                                      <option value="not-redeemed">No Redeemed</option>
                                      <option value="redeemed" selected>Redeemed</option>
                                      <option value="cancelled">Cancelled</option>
                                  </select>
                              </div>
                           <header class="panel-heading" style="height: 60px;">
                            <!--<strong> Voucher History</strong>-->
                            <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>Voucher History</strong></h3>
<div style="height: 5px;width: 60px;background: #5AD0B6;"></div> 
                                                          
                          </header>
                          <?php
							if(count($data[2])	==	0){
							?>
                           <style>.site-min-height{ min-height:0px;}</style>
                          <div class="row site-min-height">
                              <div class="col-lg-12">
                                  <section class="panel">
                                <div style="margin: 0 auto;margin-top: 3%;">
                                    <div style="text-align: center;"><img src="<?php echo APP_IMAGES.'no_history.png' ?>" style="text-align: center;"></div>
                                    <div style="font-size: 24px;text-align: center;margin-top: 20px;margin-bottom: 20px;color: #4A4A4A;font-weight: 600;">No History!</div>
                                    <div style="font-size: 16px;text-align: center;margin-top: 20px;margin-bottom: 20px;">Currently, there is no Voucher history available! Once you will create a deal, it will be shown here!</div>
                                </div>
                                </section>
                                </div>
                                </div>
                          <?php
							}else{
						  ?>
                          <div class="panel-body" id="panelBody">
                          <div class="content-customer">
                          	<div class="row">
                            	<div class="col-md-2 text-center">
                                	<strong style="font-size:16px;">Name</strong>
                                </div>
                                <div class="col-md-2 text-center">
                                	<strong style="font-size:16px;">Offer</strong>
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-2">
                                	<strong style="font-size: 16px;margin-left: 15px;">Status</strong>
                                </div>
                            </div>
                            </div>
                          <?php for($i=0; $i<count($data[0]); $i++) { 
						  if($data[0][$i]['booking_status']==0) {
							 $status = '<span class="label label-primary" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'pending_icon.png'.'"/> <span style="margin-left: 10px;">Redeem This</span></span>';
	 
						  }
						  else if($data[0][$i]['booking_status']==1)
						  {
							  $status = '<span class="label label-primary" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'right_icon.png'.'"/> <span style="margin-left: 10px;">Redeemed</span></span>';
						  }
						  else
						  {
							  $status = '<span class="label label-danger" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'cancel_icon.png'.'"/> <span style="margin-left: 10px;">Cancelled</span></span>';
						  }
						  ?>
                          <div class="row">
                            	<div class="content-customer" style="padding: 20px 0px 50px; margin:5px 20px; border-radius:5px;background-color: #F3F7FA;">
                            	<div class="col-md-2">
                                	<strong style="font-size: 16px;"><?php echo ucfirst($data[0][$i]['customer_name']); ?></strong>
                                </div>
                                <div class="col-md-8" style="border-left: 2px solid #ccc;font-size: 14px;margin-top: -4px;">
                                	<?php echo ucfirst($data[0][$i]['deal_name']); ?>
                                    <br>
                                    <i class="fa fa-calendar"></i> <?php echo date('jS F Y', strtotime($data[0][$i]['booking_created_on'])); ?>
                                </div>
                                <div class="col-md-2  text-right" style="margin-top:5px;">
                                	<?php echo $status; ?>
                                </div>
                                </div>
                            </div>
                          <?php } ?>
                            <div class="pull-right">
                                  <?php 
								   $totalrecords = $data[1]['count'];
								   $pg = new pagination(); 
								   $pg->pagenumber = 1;
									$pg->pagesize =10;
									$pg->totalrecords = $totalrecords;
									$pg->showfirst = true;
									$pg->showlast = true;
									$pg->paginationcss = "pagination-normal";
									$pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
									$pg->defaultUrl = "#";
									$pg->paginationUrl = "#";
									echo $pg->process();
									 ?>          
                                     </div>                            
                          </div>
                          <?php } ?>
                      </section>
                  </div>
              </div>
            <?php
				}
			?>
              <!-- page end-->
          </section>
      </section>
      
      <?php //} ?>



</section>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="viewVoucher">
  <div class="modal-dialog modal-sm mc">
    <!--<div class="modal-content" style="box-shadow: none;border: none;background-position: 0px -2px;background-size: 100% 100%;background-repeat: no-repeat;
    background-color: rgb(239, 239, 239);width: 400px;top: 78px;">
    </div>-->
  </div>
</div>
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->

<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
<script>
$(function () {
	
$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal', '.modal', function () {
$(this).removeData('bs.modal');
});
});

$(document).on('click','.pagination-css',function(){
          var page_number = $(this).attr('id');
		  var page_number = page_number.substr(3);
		  var sortValue = $('#sort').val();
          $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/redeem_voucher/pageChange',
            data: {'page_number' : page_number,'sortValue' : sortValue},
            type: 'POST',
            success: function (data) {
                    //var data1 = $.parseJSON(data);
					console.log(data);
                    $('#panelBody').html('');
                    $('#panelBody').html(data);
                  },
                  error: function (data) {
                  }
            });
          return false;
    });
	
	$(document).on('change','#sort',function(){
		  var sortValue = $('#sort').val();
          $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/redeem_voucher/pageChangeonSort',
            data: {'sortValue' : sortValue},
            type: 'POST',
            success: function (data) {
				console.log(data);
                    //var data1 = $.parseJSON(data);
                    $('#panelBody').html('');
                    $('#panelBody').html(data);
                  },
                  error: function (data) {
                  }
            });
          return false;
    });
	
	$(document).on('click','#search',function(){
		  var searchValue = $('#voucher').val();
		  if(($.trim(searchValue))!="")
		  {
          $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/redeem_voucher/searchVoucher',
            data: {'searchValue' : searchValue},
            type: 'POST',
            success: function (data) {
				console.log(data);
				 $("#viewVoucher").modal('show');
    			 $("#viewVoucher .mc").html(data);
                  },
                  error: function (data) {
                  }
            });
          return false;
		  }
		  else
		  {
			  return false;
		  }
    });	
	</script>
</body>
</html>
