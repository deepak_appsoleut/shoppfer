<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Profile Gallery</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
<link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-white sidebar-mini">
<?php include(APP_VIEW.'includes/header.php');?>
<?php include(APP_VIEW.'includes/nav.php'); ?>
<section id="main-content">
	<section class="wrapper site-min-height">
      	<!-- page start-->
         	<div class="row">
				<div class="col-lg-12">
					<section class="panel">
                        <header class="panel-heading">
                        	Profile Management - Gallery
                        </header>
                        <div class="panel-body">
						 <?php 
                         if($_SESSION['app_user']['mp_details_step_complete']==0)
                         {
                         ?>      
                          <div class="row">
                            <div class="col-md-12">
                            <div class="callout callout-danger">
                                    <h4>Profile Incomplete!</h4>
                                    <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile">Click Here</a> to update your profile.</p>
                                  </div>
                            </div>
                          </div>
                         <?php 
                         }
                         ?>
                          <div class="row">
                                <?php if($_SESSION['app_user']['mp_details_step_complete']==1)
                                { ?>
                                <div class="col-md-2">
                                    <a href="<?php echo APP_URL; ?>admin_mp/profile/profile_edit" class="btn btn-default btn-block">Business Details</a>
                                </div>
                                <div class="col-md-2">
                                    <a href="<?php echo APP_URL; ?>admin_mp/profile/profile_gallery" class="btn btn-warning btn-block">Gallery</a>
                                </div>
                                <?php } ?>
                                <div class="col-md-2">
                                    <a href="<?php echo APP_URL; ?>admin_mp/profile/change_password" class="btn btn-default btn-block">Change Password</a>
                                </div>
                                <?php if($_SESSION['app_user']['galleryCount']<4) { ?>
                                <!--<div class="col-md-2 pull-right">
                                    <a data-toggle="modal" data-target="#editGallery" href="<?php echo APP_URL; ?>admin_mp/profile/addGallery" class="btn btn-warning btn-block">Add Gallery</a>
                                </div>-->
                                <?php } ?>
                          </div>
                          <div class="row" style="margin-top:15px;">
                            <div class="col-xs-12">
                            <div class="box">
                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>Gallery Title</th>
                                      <th>Image</th>
                                      <th>Status</th>
                                      <th>Operation</th>
                                    </tr>
                                    </thead>
                                   <tbody>
                                    <?php 
                    
                                    for($i=0; $i<count($data); $i++) {
                                        if($data[$i]["mp_gallery_status"]==1)
                                        {
                                            $active = "<span class='badge bg-success'>Active</span>";
                                        }
                                        else
                                        {
                                            $active = "<span class='badge bg-important'>Inactive</span>";
                                        }
                                        ?>
                                        <tr role="row">
                                        <td><?php echo $data[$i]["mp_gallery_id"]; ?></td>
                                        <td><?php echo $data[$i]["mp_gallery_name"]; ?></td>
                                        <td><a href="<?php echo APP_CRM_UPLOADS_PATH.$data[$i]["mp_gallery_thumb"]; ?>" target="_blank"><img width="50" src="<?php echo APP_CRM_UPLOADS_PATH.$data[$i]["mp_gallery_thumb"]; ?>" /></a></td>
                                        <td><?php echo $active; ?></td>
                                        <td><a data-toggle="modal" data-target="#editGallery" href="<?php echo APP_URL; ?>admin_mp/profile/editGallery/<?php echo $data[$i]['mp_gallery_id'];?>"><i class="fa fa-pencil-square-o"></i>Edit</a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                  </table>
                                </div>
                            </div>
                            </div>
                          </div>
						</div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>      
  
  
  
<?php include(APP_VIEW.'includes/footer.php');?>
<?php include(APP_VIEW.'includes/bottom.php');?>
<!--==========Edit lead view Modal========-->
<div class="modal fade bs-modal-lg" id="editGallery">
 <div class="modal-dialog modal-lg">
      <div class="modal-content admin_custom_modal">
			
     </div>
  </div>
</div>   
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script>
$(function()
{
<!----Modal Close Reload-------->   
$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
});

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
		});
});

</script>

<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
