<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Change Password</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
<link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>
.form-group{ margin-bottom:45px !important;}
.site-min-height {min-height: 402px !important;}
</style>  
</head>
<body class="hold-transition skin-white sidebar-mini">

<?php include(APP_VIEW.'includes/header.php');?>
<?php include(APP_VIEW.'includes/nav.php'); ?>
<section id="main-content">
	<section class="wrapper site-min-height">
      	<!-- page start-->
         	<div class="row">
				<div class="col-lg-12">
					<section class="panel">
                        <header class="panel-heading">
                        	Profile Management - Change Password
                        </header>
                        <div class="panel-body">
						  <?php 
                          if($_SESSION['app_user']['mp_details_step_complete']==0)
                          {
                          ?>      
                          <div class="row">
                            <div class="col-md-12">
                            <div class="callout callout-danger">
                                    <h4>Profile Incomplete!</h4>
                                    <p>Your profile has not been completed yet. Please <a href="<?php echo APP_URL ?>admin_mp/profile">Click Here</a> to update your profile.</p>
                                  </div>
                            </div>
                          </div>
                          <?php }  ?>
                          <div class="row">
                                <?php if($_SESSION['app_user']['mp_details_step_complete']==1)
                                { ?>
                                <div class="col-md-2">
                                    <a href="<?php echo APP_URL; ?>admin_mp/profile/profile_edit" class="btn btn-default btn-block">Business Details</a>
                                </div>
                                <div class="col-md-2">
                                    <a href="<?php echo APP_URL; ?>admin_mp/profile/profile_gallery" class="btn btn-default btn-block">Gallery</a>
                                </div>
                                <?php } ?>
                                <div class="col-md-2">
                                    <a href="<?php echo APP_URL; ?>admin_mp/profile/change_password" class="btn btn-warning btn-block">Change Password</a>
                                </div>
                          </div>
                          <div class="row" style="margin-top:15px;">
                            <div class="col-xs-12">
                            <div class="box">
                                <div class="box-body">
                                <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b>Success : </b> <span></span></div>
                                <div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b>Warning : </b> <span></span></div>
                                    <form id="form1" method="post" enctype="multipart/form-data">
                          <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                  <div class="col-md-4">
                                  <label>Old Password</label>
                                  </div>
                                  <div class="col-md-8">
                                  <input name="old_password" id="old" placeholder="Old Password" class="form-control input-sm" type="password">
                                  </div>
                                </div>
                            </div>
                            </div>
                            <div class="row marTop10">
                            <div class="col-md-6">
                            <div class="form-group">
                                  <div class="col-md-4">
                                  <label>New Password</label>
                                  </div>
                                  <div class="col-md-8">
                                  <input name="n_password" placeholder="New Password" class="form-control input-sm" type="password">
                                  </div>
                                </div>
                            </div>
                            </div>
                            <div class="row marTop10">
                            <div class="col-md-6">
                            <div class="form-group">
                                  <div class="col-md-4">
                                  <label>Confirm Password</label>
                                  </div>
                                  <div class="col-md-8">
                                 <input name="c_password" placeholder="Confirm Password" class="form-control input-sm" type="password">
                                  </div>
                                </div>
                            </div>
                          </div>
                          <div class="clear"></div>      
                          <div class="row marTop20">   
                            <div class="col-md-6">
                            <div class="form-group">
                                    <div class="col-md-4">
                                    </div>
                                  <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary">Change Password</button>
                                  </div>
                                </div>
                            </div>
                          </div>
                          </form>
                                </div>
                            </div>
                            </div>
                          </div>
							</div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>      


<?php include(APP_VIEW.'includes/footer.php');?>
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script>
$('#form1').bootstrapValidator({
        fields: {
			old_password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Old Password'
                    }
                }
            },
            n_password: {
                validators: {
					identical: {
						field: 'c_password',
						message: 'The password and its confirm are not the same'
					},
					notEmpty: {
                        message: 'Please Enter New Password'
                    }
                }
            },
            c_password: {
                validators: {
					identical: {
						field: 'n_password',
						message: 'The password and its confirm are not the same'
					},
					notEmpty: {
                        message: 'Please Enter Confirm Password'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/profile/changePassword',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data.match("true"))
				{
					$('.alert-warning').hide();
					$('.alert-success').show();
					$('.alert-success span').html("Password changed successfully!");
				}
				else
				{
					$('.alert-success').hide();
					$('.alert-warning').show();
					$('.alert-warning span').html("There is some error while Updating Password!");
				}
            },
            error: function (data) {
            }
   });
});
$(function()
{
	$('#old').change(function()
	{
		var old = $(this).val();
		    $.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/profile/checkPassword',
            type: 'POST',
			data: {"old" : old},
            success: function (data) {
				if(data==0)
				{
					alert('Password does not match!');
					$('#old').val('');
				}
            },
            error: function (data) {
            }
   		});
	});
});
</script>

<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
