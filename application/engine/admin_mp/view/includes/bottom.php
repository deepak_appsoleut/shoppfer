<!-- jQuery 2.1.4 -->
<script src="<?php echo APP_CRM_BS; ?>js/jquery.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap.min.js"></script>
  <script src="<?php echo APP_CRM_BS; ?>js/jquery.scrollTo.min.js"></script>
    <!--common script for all pages-->
    <script src="<?php echo APP_CRM_BS; ?>js/common-scripts.js"></script>
    <script class="include" type="text/javascript" src="<?php echo APP_CRM_BS; ?>js/jquery.dcjqaccordion.2.7.js"></script>
<script>
function runNotification()
{
	$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/notification',
			type: 'POST',
			success: function (data) {
				if(data.match('!doctype'))
				{
					top.location = "<?php echo APP_URL; ?>admin_mp/login";
				}
				var data_value	=	data.split('###');
				$('.notification').html(data_value[0]);
				$('.notification_li').html(data_value[1]);
			},
			error: function (data) {
			}
	});	
	
}
function redeem_notfiy()
{
	$.ajax({
		url: '<?php echo APP_URL; ?>admin_mp/notification/redeemnotification',
		type: 'POST',
		success: function (data) {
			console.log(data);
			var data_value	=	data.split('###');
			$('.notification_redeemd').html(data_value[0]);
			$('.notification_li_redeemd').html(data_value[1]);
		},
		error: function (data) {
		}
	});	
}
runNotification();
//redeem_notfiy();
setInterval(function(){runNotification();}, 5000);
//setInterval(function(){redeem_notfiy();}, 5000);
</script>