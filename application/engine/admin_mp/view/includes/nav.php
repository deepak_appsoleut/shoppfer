<aside>
<?php
if($_SESSION['app_user']['mp_details_step']!=2)
{
?>
	<div id="sidebar"  class="nav-collapse" style="width:340px;">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              </ul>
              <!-- sidebar menu end-->
          </div>
<?php }else{ ?>          
	<div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                     <span style="color: #fff;text-decoration: none;display: block;padding: 20px 0 20px 10px;font-size: 12px;outline: none;-webkit-transition: all 0.3s ease;-moz-transition: all 0.3s ease;-o-transition: all 0.3s ease;-ms-transition: all 0.3s ease;transition: all 0.3s ease;">
                     </span>
                  </li>
                  <li>
                      <a class="<?php echo $dashboard ?>" href="<?php echo APP_URL; ?>admin_mp/dashboard">
                          <!--<i class="fa fa-home" aria-hidden="true" style="color:#fff;"></i>-->
                          <span style="font-size: 14px;font-weight: bold;margin-left: 10px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;">Dashboard</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $deal ?>" href="<?php echo APP_URL; ?>admin_mp/deals">
                          <!--<i class="fa fa-tag" aria-hidden="true" style="color:#fff;"></i>-->
                          <span style="font-size: 14px;font-weight: bold;margin-left: 10px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;">Manage offers</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $redeem ?>" href="<?php echo APP_URL; ?>admin_mp/redeem_voucher">
                         <!-- <i class="fa fa-check" aria-hidden="true" style="color:#fff;"></i>-->
                          <span style="font-size: 14px;font-weight: bold;margin-left: 10px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;">Redeem vouchers</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $reviews ?>" href="<?php echo APP_URL; ?>admin_mp/review_mp" >
                         <!-- <i class="fa fa-users" aria-hidden="true" style="color:#fff;"></i>-->
                          <span style="font-size: 14px;font-weight: bold;margin-left: 10px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;">Customer Feedback</span>
                      </a>
                      <!--<ul class="sub">
                          <li class="<?php echo $feedback ?>" ><a href="<?php echo APP_URL; ?>admin_mp/feedback" style="font-size: 11px;font-weight: bold;margin-left: 10px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;">Customer Feedback</a></li>
                          <li class="<?php echo $reviews ?>" ><a href="<?php echo APP_URL; ?>admin_mp/review_mp" style="font-size: 11px;font-weight: bold;margin-left: 10px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;">Customer Reviews</a></li>
                      </ul>-->
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
<?php   }  ?>
</aside>