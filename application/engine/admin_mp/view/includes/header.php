<?php
            if($_SESSION['app_user']['mp_details_step']!=2)
            {
            ?>
            <header class="header white-bg">
			<div style="float: left;padding-left: 14px;height: 61px;background:#E51429;">
              <div style="color: #fff;font-size: 67px;margin-top: 65px;width: 326px;">
              <!--Shoppfer-->
              <img src="<?php echo APP_IMAGES.'s_logo.png' ?>" style="width: 87%;"/>
              </div>
              <div style="color: #fff;font-size: 13px;font-weight: 600;width: 100%;">
              A wonderful solution for your business sales!
              </div>
              <div style="color: #fff;font-size: 11px;font-weight: 600;margin-top: 15px;width: 100%;">
              <img src="<?php echo APP_IMAGES.'pimage.jpg' ?>" style="width: 260px;margin-top: 200px;margin-left: 12px;">
              </div>
            </div>
            <div style="width:100%;">
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <li id="header_notification_bar" class="dropdown">
                       <span style="color: #5ad0b6;font-size: 22px;font-weight: 600;margin-left: 40px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;">Welcome, <?php if($_SESSION['app_user']['mp_details_name']!=NULL){echo ucwords($_SESSION['app_user']['mp_details_name']);}else{echo "Guest";}?></span>
                    </li>
                </ul>
            </div>
            <?php }else{ ?> 
            <header class="header white-bg">
            <div style="width: 210px;float: left;padding-left: 14px;height: 59px;background-image: url('<?php echo APP_IMAGES.'header.jpg' ?>');">
              <div class="sidebar-toggle-box">
                  <div class="tooltips" data-placement="right" data-original-title="Toggle Navigation" style="width: 28px;"></div>
              </div>
              <a href="<?php echo APP_URL; ?>admin_mp/dashboard" class="logo">
                <img src="<?php echo APP_IMAGES.'logo.png' ?>" style="height: 40px;margin-left: -27px;
    margin-top: -2px;"/>
                </a>
            </div>
            <div style="width:100%;">
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <li id="header_notification_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="">

                            <!--<i class="fa fa-bell-o" style="color: #fff;"></i>-->
                            <img src="<?php echo APP_IMAGES.'notification.jpg' ?>" style="height: 25px;"/>
                            <span class="badge bg-warning"><span class="notification"></span></span>
                        </a>
                        
                        <ul class="dropdown-menu extended inbox" style="height: 228px;overflow-x: hidden;border: 1px solid #D8E2E4 !important;">
                            <!--<div class="notify-arrow notify-arrow-red"></div>-->
                            <li>
                                <p class="green" style="background-color: #5AD0B6;">You have <!--<span class="notification"></span>-->new notifications</p>
                            </li>
                            <li class="notification_li"></li>
                            <!--<li>
                                <a href="#">See all messages</a>
                            </li>-->
                        </ul>
                    </li>
                    <!-- notification dropdown end -->
                     <!-- inbox dropdown start-->
                    <!--<li id="header_inbox_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="">
                            <img src="<?php //echo APP_IMAGES.'redeem.jpg' ?>" style="height: 25px;"/>
                            <span class="badge bg-important"><span class="notification_redeemd"></span></span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-red"></div>
                            <li>
                                <p class="red">You have <span class="notification_redeemd"></span> new messages</p>
                            </li>
                            <li class="notification_li_redeemd"></li>
                        </ul>
                    </li>-->
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <?php   }  ?>
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu" style="margin-right:15px;">
                   <!-- <li>
                        <input type="text" class="form-control search" placeholder="Search">
                    </li>-->
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="<?php echo APP_IMAGES.'user.png' ?>">
                            <span class="username"> 
							<?php if($_SESSION['app_user']['mp_details_name']!=NULL)
                              { 
                                echo ucwords($_SESSION['app_user']['mp_details_name']); 
                              }
                              else
                              {
								  echo "Guest";
                              }
							  ?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                             <?php
							if($_SESSION['app_user']['mp_details_step']!=2)
							{
							?> 
                            <li style=" width:100%;"><a href="<?php echo APP_URL; ?>admin_mp/profile/"><i class=" fa fa-suitcase"></i>Edit Profile</a></li>
                            
                            <?php }else{?>
                            <li><a href="<?php echo APP_URL; ?>admin_mp/profile/profile_edit"><i class=" fa fa-suitcase"></i>PROFILE</a></li>
                            <li><a href="<?php echo APP_URL; ?>admin_mp/profile/settings"><i class="fa fa-cog" aria-hidden="true"></i>SETTINGS</a></li>
                             <?php } ?>
                            <!--<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
                            <!--<li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>-->
                            <li><a href="<?php echo APP_URL; ?>admin_mp/login/deactivate"><i class="fa fa-key"></i>Logout</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
            </div>
        </header>