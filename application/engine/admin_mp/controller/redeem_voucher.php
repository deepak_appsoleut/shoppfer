<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class redeem_voucher extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function redeem_voucher()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function redeemThis()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function booking_past()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function pageChange()
	{
				$data = $this->model->process(__CLASS__, __FUNCTION__);
				?>
                  <div class="content-customer">
                          	<div class="row">
                            	<div class="col-md-2 text-center">
                                	<strong style="font-size:16px;">NAME</strong>
                                </div>
                                <div class="col-md-2 text-center">
                                	<strong style="font-size:16px;">OFFER</strong>
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-2 text-center">
                                	<strong style="font-size:16px;">STATUS</strong>
                                </div>
                            </div>
                            </div>
                           <?php for($i=0; $i<count($data[0]); $i++) { 
						  if($data[0][$i]['booking_status']==0) {
							 $status = '<span class="label label-primary" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'pending_icon.png'.'"/> <span style="margin-left: 10px;">Redeem This</span></span>';
	 
						  }
						  else if($data[0][$i]['booking_status']==1)
						  {
							  $status = '<span class="label label-primary" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'right_icon.png'.'"/> <span style="margin-left: 10px;">Redeemed</span></span>';
						  }
						  else
						  {
							  $status = '<span class="label label-danger" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'cancel_icon.png'.'"/> <span style="margin-left: 10px;">Cancelled</span></span>';
						  }
						  ?>
                          <div class="row">
                            	<div class="content-customer" style="padding: 20px 0px 50px; margin:5px 20px; border-radius:5px;background-color: #F3F7FA;">
                            	<div class="col-md-2">
                                	<strong style="font-size: 16px;"><?php echo ucfirst($data[0][$i]['customer_name']); ?></strong>
                                </div>
                                <div class="col-md-8" style="border-left: 2px solid #ccc;font-size: 14px;margin-top: -4px;">
                                	<?php echo ucfirst($data[0][$i]['deal_name']); ?>
                                    <br>
                                    <i class="fa fa-calendar"></i> <?php echo date('jS F Y', strtotime($data[0][$i]['booking_created_on'])); ?>
                                </div>
                                <div class="col-md-2  text-right" style="margin-top:5px;">
                                	<?php echo $status; ?>
                                </div>
                                </div>
                            </div>
                          <?php } ?>
                            <div class="pull-right">
                                  <?php 
								    $pageNumber = 1;
									if(isset($_POST['page_number']))
									{
										$pageNumber = $_POST['page_number'];
									} 
								   $totalrecords = $data[1]['count'];
								   $pg = new pagination(); 
								   $pg->pagenumber = $pageNumber;
									$pg->pagesize =10;
									$pg->totalrecords = $totalrecords;
									$pg->showfirst = true;
									$pg->showlast = true;
									$pg->paginationcss = "pagination-normal";
									$pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
									$pg->defaultUrl = "#";
									$pg->paginationUrl = "#";
									echo $pg->process();
									 ?>          
                                     </div>
                                     <?php              
	}
	function pageChangeonSort()
	{
			$data = $this->model->process(__CLASS__, __FUNCTION__);
				?>
                  <div class="content-customer">
                          	<div class="row">
                            	<div class="col-md-2 text-center">
                                	<strong>NAME</strong>
                                </div>
                                <div class="col-md-2 text-center">
                                	<strong>OFFER</strong>
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-2 text-center">
                                	<strong>STATUS</strong>
                                </div>
                            </div>
                            </div>
                           <?php for($i=0; $i<count($data[0]); $i++) { 
						  if($data[0][$i]['booking_status']==0) {
							 $status = '<span class="label label-primary" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'pending_icon.png'.'"/> <span style="margin-left: 10px;">Redeem This</span></span>';
	 
						  }
						  else if($data[0][$i]['booking_status']==1)
						  {
							  $status = '<span class="label label-primary" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'right_icon.png'.'"/> <span style="margin-left: 10px;">Redeemed</span></span>';
						  }
						  else
						  {
							  $status = '<span class="label label-danger" style="background-color: #5ACFB5;padding-right: 15px;padding-top: 15px;    padding-bottom: 15px;font-size: 15px;"><img src="'.APP_IMAGES.'cancel_icon.png'.'"/> <span style="margin-left: 10px;">Cancelled</span></span>';
						  }
						  ?>
                          <div class="row">
                            	<div class="content-customer" style="padding: 20px 0px 50px; margin:5px 20px; border-radius:5px;background-color: #F3F7FA;">
                            	<div class="col-md-2">
                                	<strong style="font-size: 16px;"><?php echo ucfirst($data[0][$i]['customer_name']); ?></strong>
                                </div>
                                <div class="col-md-8" style="border-left: 2px solid #ccc;font-size: 14px;margin-top: -4px;">
                                	<?php echo ucfirst($data[0][$i]['deal_name']); ?>
                                    <br>
                                    <i class="fa fa-calendar"></i> <?php echo date('jS F Y', strtotime($data[0][$i]['booking_created_on'])); ?>
                                </div>
                                <div class="col-md-2  text-right" style="margin-top:5px;">
                                	<?php echo $status; ?>
                                </div>
                                </div>
                            </div>
                          <?php } ?>
                            <div class="pull-right">
                                  <?php 
								   $totalrecords = $data[1]['count'];
								   $pg = new pagination(); 
								   $pg->pagenumber = 1;
									$pg->pagesize =10;
									$pg->totalrecords = $totalrecords;
									$pg->showfirst = true;
									$pg->showlast = true;
									$pg->paginationcss = "pagination-normal";
									$pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
									$pg->defaultUrl = "#";
									$pg->paginationUrl = "#";
									echo $pg->process();
									 ?>          
                                     </div>
                                     <?php  
	}
	function searchVoucher()
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__);
		?>
        
      <?php if(count($data)>0) { ?>
      <!--<div class="modal-header" style="padding-top: 7px;padding-bottom: 7px;">
        <h4 class="modal-title" id="myModalLabel" style="font-weight: 500;font-size: 17px;">ERROR1</h4>
    	</div>-->
        <div class="modal-content" style="border-radius: 13px;box-shadow: none;border: none;background-position: 0px -2px;background-size: 100% 100%;background-repeat: no-repeat;background-color: rgb(239, 239, 239);width: 300px;top: 78px;">
        
    	<div class="modal-body" style="height: 450px;width: 300px;padding: 0px;">
        <button type="button" class="btn btn-round btn-info" data-dismiss="modal" style="width: 25px;height: 25px;margin-left: 20px;background-color: #5AD0B6;border-color: #5AD0B6;float: right;border-radius: 6px; cursor:pointer;">
       <i class="fa fa-times" aria-hidden="true" style="margin-top: -1px;float: right;margin-right: -6px;"></i>
       </button>
      <div class="error"></div>
      <form id="form" method="post">
      <div class="row">
      		<div class="col-md-12" style="    text-align: center;">
            	<!--<label style="width:100px"><strong>Name : </strong></label>-->
                
                
                <div style="height: 200px;">
                <img src="<?php echo APP_IMAGES.'coupon_ticket.png' ?>" style="margin-top: 35px;" id="cpn_img"/>
                <div>
                <div style="float: left;margin-left: 95px;margin-top: 30px;color: #000;"><i class="fa fa-star" aria-hidden="true"></i></div>
                <div style="margin-left:10px;margin-right:10px;float: left;color: #000;font-size: 16px;margin-top: 30px;font-weight: bold;"> 
                <?php echo $data[0]['booking_coupon_code']; ?>
                </div>
               <div style="float: left;margin-top: 30px;color: #000;"><i class="fa fa-star" aria-hidden="true"></i></div>
                </div>
                </div>
                <div style="height: 260px;background-image: url('<?php echo APP_IMAGES.'coupon_bg.png' ?>');background-repeat: no-repeat;background-size: 302px 361px;">
                	<div style="margin-bottom: 10px;">
                    <strong>
                    <div style="font-size: 16px;padding-top: 24px; color:#fff;"><?php echo strtoupper ($data[0]['customer_name']); ?></div>
                    </strong>
                    </div>
                    <div class="clearfix"></div>
                    <div style="margin-bottom: 10px;">
                    <div style="color:#fff;"><?php echo $data[0]['deal_name']; ?></div>
                    <div style="color:#fff; margin-bottom:10px;padding-left: 5px;padding-right: 5px;">
                    <?php 
					if($data[0]['offer_description']	==	''){
						echo 'Flat Offer with '. $data[0]['offer_discount'] .'%';
					}else{
						echo $data[0]['offer_description'];
					}
					?>
                    </div>
                   
                    <div style="color:#fff;">
                    <?php 
					$day	=	'';
					if($data[0]['offers_day_type']	==	1){
						$day	=	'Everyday';
					}
					if($data[0]['offers_day_type']	==	2){
						$day	=	'Weakdays';
					}
					if($data[0]['offers_day_type']	==	3){
						$day	=	'Weakend';
					}
					if($data[0]['offers_day_type']	==	4){
						$day	=	$data[0]['offers_day_type'];
					}
					if($data[0]['offers_day_type']	==	5){
						$day	=	$data[0]['offers_day_type'];
					}
					echo 'Offers vaild on '. $day .' only';
					?>
                    </div>
                    <div style="color:#fff;">
                    <?php 
					echo 'Offers Timeings are '. $data[0]['offers_timeings'];
					?>
                    </div>
                    <div style="color:#fff;">
                    <?php 
					if($data[0]['min_billing']	==	0 || $data[0]['min_billing'] == ''){
						echo 'No Minimum Billing Amount';
					}else{
						echo 'Minimum bill are Rs-'.$data[0]['min_billing'];
					}
					?>
                    </div>
                    <input type="hidden" id="id" value="<?php echo $data[0]['booking_id']; ?>" />
                    </div>
                    <div class="clearfix"></div>
                     <div style="margin-bottom: 10px;">
                     <span style="color:#fff;">
					 <?php //echo date('jS F Y', strtotime($data[0]['booking_created_on'])); ?></span></div>
                      <?php if($data[0]['booking_status']=='0') { ?>     
       <button class=" btn-round btn-danger btn" style="background-color: #fff;border-color: #fff;color: #000;opacity: 1;font-weight: 500; cursor:pointer;border-radius: 8px;text-align: center;font-size: 13px;" type="submit" id="cpn_btn">
       
<!--<i class="fa fa-check" aria-hidden="true" style="color:#fff;margin-top: 3px;margin-left: 6px;"></i>-->
REDEEM COUPON</button><div id="succe_msg" style="color: #000;font-weight: 600;font-style: italic;margin-top: 16px;"></div>
       <!--<button type="button" class="btn btn-round btn-info" data-dismiss="modal" style="width: 40px;height: 33px;margin-left:20px; margin-top:20px;background-color: #5AD0B6;border-color: #5AD0B6;float: right;">
       <i class="fa fa-times" aria-hidden="true"></i>
       <span class="sr-only"></span></button>-->
       
       
       <?php } else if($data[0]['booking_status']=='1') { ?>
       <button class="btn-primary btn" disabled="disabled" type="submit" style="background-color: #fff;border-color: #fff;color: #000;opacity: 1;
font-weight: 500; cursor:pointer;margin-top: 15px;">Already Redeemed</button>
       <?php } else { ?>
        <button class="btn-success btn" disabled="disabled" style="margin-left:20px; margin-top:20px" type="submit">Cancelled</button>
       <?php } ?>
                </div>
            </div>
      </div>
       
      
       
      </form>
      </div>
      </div>
      <?php } else { ?>
      <div class="modal-content" style="box-shadow: none;border: none;background-position: 0px -2px;background-size: 100% 100%;background-repeat: no-repeat;
    background-color: rgb(239, 239, 239);width: 400px;top: 78px;">
      	<div class="modal-header" style="padding-top: 7px;padding-bottom: 7px;">
        <h4 class="modal-title" id="myModalLabel" style="font-weight: 500;font-size: 17px;">ERROR</h4>
    	</div>
    	<div class="modal-body">
      <div class="error"></div>
      <div style="width: 80px;float: left;">
      <img src="<?php echo APP_IMAGES.'error.png' ?>" />
      </div>
      <div style="text-align:center;height: 73px;margin-top: 14px;color: #000;font-weight: 500;">Sorry! Coupon Code is not available.
      <div style="float: right;margin-top: 36px;"><button class="btn btn-success btn-xs" style="padding: 2px 30px;" data-dismiss="modal">OK</button></div>
      </div>
      <?php } ?>
    </div>
    </div>
    <script>
	$("#form button:submit").click(function()
	{
		var value = $('#id').val();
		$.ajax({
            url: '<?php echo APP_URL; ?>admin_mp/redeem_voucher/redeemCoupon',
            type: 'POST',
			data: {'id':value},
            success: function (data) {
				console.log(data);
				//alert(data);
				if(data=="true")
				{
					//$('.error').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span>Coupon Redeemed successfully!</span></div>');
					//$('#form button:submit').attr('disabled', 'disabled');
					$("#cpn_img").attr("src","<?php echo APP_IMAGES.'select.png' ?>");
					$("#cpn_img").css("margin-top","0px");
					$("#cpn_btn").hide();
					$("#succe_msg").html('Coupon Redeemed successfully!');
				}
				else
				{
					$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span>There is some error while redemeption!</span></div>');
				}
            },
            error: function (data) {
            }
   			});
			return false;
	});
</script>
        <?php
	}
	
	function redeemCoupon()
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__);
		echo $data;
	}
}
?>