<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class revenue_mp extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function revenue_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function revenue_data()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
}
?>