<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class analytics_mp extends controller {

	function __construct() {
		parent::__construct();
	}
	function analytics_mp()
	{
		$this->view->render(__CLASS__, NULL);
	}
	function analytics_data_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function analytics_data_area_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	
	// function to shaw data on MP dashboard 
	function analytics_data_dashboard_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function popup_window()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	
}
?>