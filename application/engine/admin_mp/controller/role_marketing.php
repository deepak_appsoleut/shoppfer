<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class role_marketing extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function role_marketing()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}

}
?>