<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class login extends controller {

	function __construct() {
		parent::__construct();
		session::init();
	}
	
	function login()
	{
		$this->view->render(__CLASS__);
	}

	/* function for login process */
	function process()
	{
		$data = $this->model->process(__CLASS__, "checkLogin");
		if (!empty($data)) {
			$_SESSION['app_user'] = $data[0];
			$return = json_encode($data);
		}
		else
		{
			$return = 0;
		}
		echo $return;
	}


	/* function for logout*/
	function deactivate()
	{
		unset($_SESSION['app_user']);
		header("Location:".APP_URL."admin_mp/login");
	}
	function forgot_password()
	{
		$this->view->render(__FUNCTION__);
	}
	function reset_password($id=null)
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $data);
	}
	function forgot_process()
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__);
		echo $data;
	}
	function reset_password_process()
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__);
		echo $data;
	}
}
?>