<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class deals extends controller {

	function __construct() {
		parent::__construct();
	}
	function deals()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function edit_deal($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function updateDeal()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function updateOffer()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	} 
	function adminotify_dealEdit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	} 
	function delete_deals()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	} 
	function active_inactive()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function inactive()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	} 
}
?>