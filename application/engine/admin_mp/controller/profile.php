<?php

class profile extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function profile()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function step1()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function uploadPics()
	{
		
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function uploadLogo()
	{
		
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function uploadPersonPic()
	{
		
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function step2()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function step3()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function getSubcategoryList()
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__);
		$mydata = '';
		for($i=0; $i<count($data);$i++)
		{
			$mydata .="<option value='".$data[$i]['sub_category_id']."'>".$data[$i]['sub_category_name']."</option>";
		}
		echo $mydata;
	}
	function getStateList()
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__);
		$mydata = '';
		for($i=0; $i<count($data);$i++)
		{
			$mydata .="<option value='".$data[$i]['state_id']."'>".$data[$i]['state_name']."</option>";
		}
		echo $mydata;
	}
	function profile_edit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function settings()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function profile_gallery()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function change_password()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function checkPassword()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function changePassword()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function profileEdit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function galleryUpdate()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function editGallery($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function addGallery()
	{
		$this->view->render(__FUNCTION__, null);
	}
	function galleryAdd()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function deletePic()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function deleteLogo()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function adminotify_profileEdit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function uploadpics_edit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function deletePic_edit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function adminotify_dealEdit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function chkpassword()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
}
?>