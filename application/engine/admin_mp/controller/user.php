<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class user extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function user()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}

}
?>