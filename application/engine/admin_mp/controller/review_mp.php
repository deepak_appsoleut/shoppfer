<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class review_mp extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function review_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function addReview($param)
	{
		$this->view->render(__FUNCTION__, $param);
	}
	function scoreAdd()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function review_cir()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function pageChange()
	{
				$data = $this->model->process(__CLASS__, __FUNCTION__);
                                 for($i=0; $i<count($data[3]); $i++) { 
								  	if($data[3][$i]['reply_text']=="") {
								  ?>
                                  <div class="msg-time-chat" style="padding-bottom: 5px;padding-top: 0px;">
                                      <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                         <div class="text" style="background: #F3F7FA; float:left; width:100%">
                                              <p class="attribution"><a href="#" style="color:#000 !important; font-weight:bold;font-size:16px;"><?php echo ucfirst($data[3][$i]['customer_name']); ?></a><span style="margin-left:20px">
                                              <?php 
											  $style="";
											  for($j=1; $j<6; $j++) { 
											  if($j<=$data[3][$i]['mp_review_score'])
											  	{
													$style = "color:#C00";
												}
												else
												{
													$style = "";
												}
												?>
                                                <i class="fa fa-star" style=" <?php echo $style; ?>"></i>
											  <?php } ?>
                                              </span>
                                              
                                              
                                              </p>
                                              
                                              <p style="margin-bottom:10px;"><?php echo $data[3][$i]['mp_review_comment'] ?></p>
                                              Posted @ <?php echo date('F j Y', strtotime($data[3][$i]['mp_review_created_on'])); ?>
                                              <span class="pull-right reply" id="rply<?php echo $data[3][$i]['mp_review_id'] ?>" style="cursor:pointer">
                                              <!--<i class="fa fa-reply-all"></i>-->
                                              <img src="<?php echo APP_IMAGES.'trun_small.png' ?>">
                                              </span>
                                              <form class="replyText" id="text<?php echo $data[3][$i]['mp_review_id'] ?>" style="display:none">											  <input type="hidden" value="<?php echo $data[3][$i]['customer_id'] ?>" id="cust<?php echo $data[3][$i]['mp_review_id'] ?>">	
                                              <textarea id="para<?php echo $data[3][$i]['mp_review_id'] ?>" class="form-control" style="border: 1px solid #5AD0B6 !important;"></textarea>
                                              <button type="button" id="bttn<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-primary" style="margin-top:5px;font-size: 13px;padding: 7px;">Send Reply</button></form>
                                          </div>
                                      </div>
                                  <div id="show<?php echo $data[3][$i]['mp_review_id'] ?>"></div>
                                  </div>
                                  <?php } 
								   else 
								   {
									   ?>
                                       <div class="msg-time-chat">
                                         <div class="message-body msg-in">
                                              <span class="arrow"></span>
                                              <div class="text" style="background: #F3F7FA; float:left; width:100%;">
                                                  <p class="attribution"><a href="#" style="font-size: 16px;color:#000 !important;font-weight: bold;"><?php echo ucfirst($data[3][$i]['customer_name']); ?></a>  							
                                                  <span style="">
												  <?php 
												  $style="";
												  for($j=1; $j<6; $j++) { 
                                                  if($j<=$data[3][$i]['mp_review_score'])
                                                    {
                                                        $style = "color:#C00";
                                                    }
                                                    else
                                                    {
                                                        $style = "";
                                                    }
                                                    ?>
                                                    <i class="fa fa-star" style=" <?php echo $style; ?>"></i>
                                                  <?php } ?>
                                                  </span>
                                                  <p style="margin-bottom:10px;"><?php echo $data[3][$i]['mp_review_comment'] ?></p>
                                                  Posted @<?php echo date('F j Y', strtotime($data[3][$i]['mp_review_created_on'])); ?> 
                                              </div>
                                          </div>
                                          
                                          <div class="message-body msg-out col-md-11 pull-right" style="padding-right: 0px;">
                                              <div class="arrow" style="float: left;"><img src="<?php echo APP_IMAGES.'trun.png' ?>" style="height: 20px;
    margin-top: 12px;margin-right: 4px;margin-left: 12px;"></div>
                                              <div class="text" style="background: #F3F7FA;  float:left; width:96%;border: none; margin-top:8px;">
                                                  <p class="attribution"><a href="#" style="font-size: 13px;font-weight:bold; color:#5AD0B6 !important;"><?php echo ucfirst($data[3][$i]['mp_details_name']); ?></a> @ <?php echo date('g:i a, F j Y', strtotime($data[3][$i]['reply_date'])); ?></p>
                                                  <p><?php echo $data[3][$i]['reply_text'] ?></p>
                                              </div>
                                              <div style="display:none" id="form<?php echo $data[3][$i]['reply_mp_review_id']; ?>">
                                               		<form class="replyTextEdit" id="text<?php echo $data[3][$i]['mp_review_id'] ?>">											  <input type="hidden" value="<?php echo $data[3][$i]['customer_id'] ?>" id="cust<?php echo $data[3][$i]['mp_review_id'] ?>">	
                                              <textarea id="par1<?php echo $data[3][$i]['mp_review_id'] ?>" class="form-control" style="border: 1px solid #5AD0B6 !important;"><?php echo $data[3][$i]['reply_text'] ?></textarea>
                                             
                                              <button type="button" data-id="<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-danger cancelEdit" style="margin-top:5px;font-size: 13px;padding: 7px;">Cancel</button>
                                              
                                               <button type="button" id="bttn<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-primary replyEdit" style="margin-top:5px; margin-right:10px;font-size: 13px;padding: 7px;">Edit Reply</button>
                                               </form>
                                                </div>
                                          </div>
                                      </div>
                                       <?php
								   }
								  } ?>
                                  <div class="pull-right">
                                  <?php
								  $pageNumber = 1;
									if(isset($_POST['page_number']))
									{
										$pageNumber = $_POST['page_number'];
									} 
								   $totalrecords = $data[4]['count'];
								   $pg = new pagination(); 
								   $pg->pagenumber = $pageNumber;
									$pg->pagesize =10;
									$pg->totalrecords = $totalrecords;
									$pg->showfirst = true;
									$pg->showlast = true;
									$pg->paginationcss = "pagination-normal";
									$pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
									$pg->defaultUrl = "#";
									$pg->paginationUrl = "#";
									echo $pg->process();
									?>
                                    <div>        
        							<?php
	}
	function pageChangeonSort()
	{
				$data = $this->model->process(__CLASS__, __FUNCTION__);
                                 for($i=0; $i<count($data[3]); $i++) { 
								  	if($data[3][$i]['reply_text']=="") {
								  ?>
                                  <div class="msg-time-chat" style="padding-bottom: 5px;padding-top: 0px;">
                                      <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                         <div class="text" style="background: #F3F7FA; float:left; width:100%">
                                              <p class="attribution"><a href="#" style="color:#000 !important; font-weight:bold;font-size:16px;"><?php echo ucfirst($data[3][$i]['customer_name']); ?></a><span style="margin-left:20px">
                                              <?php 
											  $style="";
											  for($j=1; $j<6; $j++) { 
											  if($j<=$data[3][$i]['mp_review_score'])
											  	{
													$style = "color:#C00";
												}
												else
												{
													$style = "";
												}
												?>
                                                <i class="fa fa-star" style=" <?php echo $style; ?>"></i>
											  <?php } ?>
                                              </span>
                                              
                                              
                                              </p>
                                              
                                              <p style="margin-bottom:10px;"><?php echo $data[3][$i]['mp_review_comment'] ?></p>
                                              Posted @ <?php echo date('F j Y', strtotime($data[3][$i]['mp_review_created_on'])); ?>
                                              <span class="pull-right reply" id="rply<?php echo $data[3][$i]['mp_review_id'] ?>" style="cursor:pointer">
                                              <!--<i class="fa fa-reply-all"></i>-->
                                              <img src="<?php echo APP_IMAGES.'trun_small.png' ?>">
                                              </span>
                                              <form class="replyText" id="text<?php echo $data[3][$i]['mp_review_id'] ?>" style="display:none">											  <input type="hidden" value="<?php echo $data[3][$i]['customer_id'] ?>" id="cust<?php echo $data[3][$i]['mp_review_id'] ?>">	
                                              <textarea id="para<?php echo $data[3][$i]['mp_review_id'] ?>" class="form-control" style="border: 1px solid #5AD0B6 !important;"></textarea>
                                              <button type="button" id="bttn<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-primary" style="margin-top:5px;font-size: 13px;padding: 7px;">Send Reply</button></form>
                                          </div>
                                      </div>
                                  <div id="show<?php echo $data[3][$i]['mp_review_id'] ?>"></div>
                                  </div>
                                  <?php } 
								   else 
								   {
									   ?>
                                       <div class="msg-time-chat">
                                         <div class="message-body msg-in">
                                              <span class="arrow"></span>
                                              <div class="text" style="background: #F3F7FA; float:left; width:100%;">
                                                  <p class="attribution"><a href="#" style="font-size: 16px;color:#000 !important;font-weight: bold;"><?php echo ucfirst($data[3][$i]['customer_name']); ?></a>  							
                                                  <span style="">
												  <?php 
												  $style="";
												  for($j=1; $j<6; $j++) { 
                                                  if($j<=$data[3][$i]['mp_review_score'])
                                                    {
                                                        $style = "color:#C00";
                                                    }
                                                    else
                                                    {
                                                        $style = "";
                                                    }
                                                    ?>
                                                    <i class="fa fa-star" style=" <?php echo $style; ?>"></i>
                                                  <?php } ?>
                                                  </span>
                                                  <p style="margin-bottom:10px;"><?php echo $data[3][$i]['mp_review_comment'] ?></p>
                                                  Posted @<?php echo date('F j Y', strtotime($data[3][$i]['mp_review_created_on'])); ?> 
                                              </div>
                                          </div>
                                          
                                          <div class="message-body msg-out col-md-11 pull-right" style="padding-right: 0px;">
                                              <div class="arrow" style="float: left;"><img src="<?php echo APP_IMAGES.'trun.png' ?>" style="height: 20px;
    margin-top: 12px;margin-right: 4px;margin-left: 12px;"></div>
                                              <div class="text" style="background: #F3F7FA;  float:left; width:96%;border: none; margin-top:8px;">
                                                  <p class="attribution"><a href="#" style="font-size: 13px;font-weight:bold; color:#5AD0B6 !important;"><?php echo ucfirst($data[3][$i]['mp_details_name']); ?></a> @ <?php echo date('g:i a, F j Y', strtotime($data[3][$i]['reply_date'])); ?></p>
                                                  <p><?php echo $data[3][$i]['reply_text'] ?></p>
                                              </div>
                                              <div style="display:none" id="form<?php echo $data[3][$i]['reply_mp_review_id']; ?>">
                                               		<form class="replyTextEdit" id="text<?php echo $data[3][$i]['mp_review_id'] ?>">											  <input type="hidden" value="<?php echo $data[3][$i]['customer_id'] ?>" id="cust<?php echo $data[3][$i]['mp_review_id'] ?>">	
                                              <textarea id="par1<?php echo $data[3][$i]['mp_review_id'] ?>" class="form-control" style="border: 1px solid #5AD0B6 !important;"><?php echo $data[3][$i]['reply_text'] ?></textarea>
                                             
                                              <button type="button" data-id="<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-danger cancelEdit" style="margin-top:5px;font-size: 13px;padding: 7px;">Cancel</button>
                                              
                                               <button type="button" id="bttn<?php echo $data[3][$i]['mp_review_id'] ?>" class="btn pull-right btn-xs btn-primary replyEdit" style="margin-top:5px; margin-right:10px;font-size: 13px;padding: 7px;">Edit Reply</button>
                                               </form>
                                                </div>
                                          </div>
                                      </div>
                                       <?php
								   }
								  } ?>
                                  <div class="pull-right">
                                  <?php
								   $totalrecords = $data[4]['count'];
								   $pg = new pagination(); 
								   $pg->pagenumber = 1;
									$pg->pagesize =10;
									$pg->totalrecords = $totalrecords;
									$pg->showfirst = true;
									$pg->showlast = true;
									$pg->paginationcss = "pagination-normal";
									$pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
									$pg->defaultUrl = "#";
									$pg->paginationUrl = "#";
									echo $pg->process();
									?>
                                    <div>        
        							<?php
	}
	function replySubmit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$data = false;
		if($result)
		{
			$date = date('g:i a, jS F Y');
		 	$data = '<div class="message-body msg-out col-md-11 pull-right">
                                              <div class="arrow" style="float: left;"><img src="'.APP_IMAGES.'trun.png"></div>
                                              <div class="text" style="background: #fff;  float:left; width:95%;    border: none;">
                                              <div class="showText" id="show'.$_POST['removedStr'].'">
                                                  <p class="attribution">
												  <a href="#" style="font-weight:bold; color:#5AD0B6 !important;">'.$_SESSION['app_user']['mp_details_name'].'</a> @ '.$date.'<span class="pull-right"><i class="fa fa-pencil-square-o editIcon" data-id="'.$_POST['removedStr'].'" style="font-size:20px; cursor:pointer" aria-hidden="true"></i></span>
                                                  </p>
                                                  <p>'.$_POST['txt'].'</p>
                                                  </div>
                                               <div style="display:none" id="form'.$_POST['removedStr'].'">
                                               		<form class="replyTextEdit" id="text'.$_POST['removedStr'].'">											  <input type="hidden" value="'.$_POST['customer_id'].'" id="cust'.$_POST['removedStr'].'">	
                                              <textarea id="par1'.$_POST['removedStr'].'" class="form-control" style="border: 1px solid #5AD0B6 !important;">'.$_POST['txt'].'</textarea>
                                             
                                              <button type="button" data-id="'.$_POST['removedStr'].'" class="btn pull-right btn-xs btn-danger cancelEdit" style="margin-top:5px">Cancel</button>
                                              
                                               <button type="button" id="bttn'.$_POST['removedStr'].'" class="btn pull-right btn-xs btn-primary replyEdit" style="margin-top:5px; margin-right:10px">Edit Reply</button>
                                               </form>
                                                </div>  
                                              </div>
                                          </div>';
		}
		echo $data;
	}
	function replyEditSubmit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$data = false;
		if($result)
		{
			$date = date('g:i a, jS F Y');
		 	$data = '<p class="attribution"><a href="#" style="font-weight:bold; color:#5AD0B6 !important;">'.$_SESSION['app_user']['mp_details_name'].'</a> @ '.$date.' <span class="pull-right"><i class="fa fa-pencil-square-o editIcon" data-id="'.$_POST['removedStr'].'" style="font-size:20px; cursor:pointer" aria-hidden="true"></i></span></p><p>'.$_POST['txt'].'</p>';
		}
		echo $data;
	}
}
?>