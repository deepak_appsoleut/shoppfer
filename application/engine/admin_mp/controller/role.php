<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class role extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function role()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}

}
?>