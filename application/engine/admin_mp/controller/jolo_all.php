<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class jolo_all extends controller {

	function __construct() {
		parent::__construct();
	}
	function jolo_all()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
}
?>