<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class login_model {

	function checkLogin()
	{
		$math = new math();
		$password = $math->hash("sha1", $_POST['password'], APP_SEC_KEY);
		$param = array("user_email"=>$_POST['email'], "pass"=>$password, "status"=>1);
		$data = $GLOBALS["db"]->select("SELECT m.* FROM mp_details_temp m
										WHERE m.mp_details_email =:user_email 
										AND m.mp_details_password =:pass 
										AND m.mp_status =:status", $param);									
		if(count($data)>0)
		{
			/* get mp details */
			$condition = array("mp_details_id"=>$data[0]["mp_details_id"],"status"=>1);
			$mydata = $GLOBALS["db"]->select("SELECT m.*, r.mp_role_name
											  FROM mp_role r, mp_details_temp m 
											  WHERE m.mp_details_id=:mp_details_id AND r.mp_role_id = m.mp_details_role_id 
											  AND r.mp_role_status =:status", $condition);
		}
		else
		{
			$mydata = array();
		}
		return $mydata;
	}
	function reset_password($id=null)
	{
		$param = array("code"=>$id);
		$selectData = $GLOBALS["db"]->select("SELECT mp_details_id FROM mp_details_temp WHERE mp_reset_code =:code LIMIT 1", $param);
		if(count($selectData)>0)
		{
			return $selectData[0]['mp_details_id'];
		}
		else
		{
			return 0;
		}
	}
	function reset_password_process()
	{
		$math = new math();
		$password = $math->hash("sha1", $_POST['password'], APP_SEC_KEY);
		$param = array("mp_reset_code"=>NULL, "mp_details_password"=>$password);
		$condition 	= array("mp_details_id"=>$_POST['mp_id']);
		$data 		= $GLOBALS["db"]->update("mp_details_temp", $param, $condition);
		return $data;
	}
	function forgot_process()
	{
		$param = array("email"=>$_POST['email']);
		$selectData = $GLOBALS["db"]->select("SELECT mp_details_id, mp_verify, mp_details_name FROM mp_details_temp WHERE mp_details_email =:email LIMIT 1", $param);
		if(count($selectData)>0)
		{
			if($selectData[0]['mp_verify']==0)
			{
				return 1;
			}
			else
			{
				$math = new math();
				$link 	  = $math->hash("sha1", rand('00000', '99999').time(), APP_SEC_KEY);
				$paramUpdate 	= array("mp_reset_code"=>$link);
				$condition 		= array("mp_details_id"=>$selectData[0]['mp_details_id']);
				$data 		= $GLOBALS["db"]->update("mp_details_temp", $paramUpdate, $condition);
				
				//code for making email body 
				$body = $math->resetTemplate($business_name = $selectData[0]['mp_details_name'], $link);
				$GLOBALS["db"]->mailSend($_POST['email'], $subject = 'SHOPPFER - Reset your password!', $body, $from = "");
				return $data;
			}
		}
		else
		{
			return 0;
		}
	}
}
?>