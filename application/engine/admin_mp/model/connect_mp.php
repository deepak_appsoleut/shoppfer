<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class connect_mp_model {

	function connect_mp()
	{
		$param = array("status"=>1);
		/*$data = $GLOBALS["db"]->select("SELECT d.deal_id, d.deal_name, d.deal_description, d.deal_image, s.sub_category_name,c.category_name, dm.deal_mp_details_id FROM deals d LEFT JOIN deals_mp dm ON d.deal_id = dm.deal_mp_deal_id LEFT JOIN mp_details mp ON mp.mp_details_id = dm.deal_mp_details_id, sub_category s, category c WHERE FIND_IN_SET(d.deal_sub_cat_id,'".$_SESSION['app_user']['sub_cat']."') AND s.sub_category_id = d.deal_sub_cat_id AND s.sub_category_cat_id = c.category_id", $param);
		return $data;	*/	
		$data = $GLOBALS["db"]->select("SELECT category_id, category_name FROM category WHERE category_status =:status", $param);
		return $data;
	}
	function getLocation1()
	{
		$param = array('hotel_id'=>$_SESSION['app_user']['hotel_details_id']);	
		if($_POST['category']=="")
		{
			$andQuery = "";
		}
		else
		{
			$_POST['category'] = implode(',', $_POST['category']);
			$andQuery = " AND m.mp_details_category IN (".$_POST['category'].")";
		}
		/*$data = $GLOBALS["db"]->select("SELECT m.mp_details_id, m.mp_details_name,m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude, hr.hotel_mp_relation_id, hr.hotel_mp_relation_hotel_id FROM mp_details m LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_mp_id = m.mp_details_id".$andQuery." GROUP BY m.mp_details_id", $param);*/
		
		$data = $GLOBALS["db"]->select("SELECT m.mp_details_id, m.mp_details_name, m.mp_details_description, m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude,hr.hotel_mp_relation_id, hr.hotel_mp_relation_hotel_id, m.mp_details_category FROM mp_details m LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_mp_id = m.mp_details_id WHERE hr.hotel_mp_relation_hotel_id =:hotel_id".$andQuery." GROUP BY m.mp_details_id", $param);
		return json_encode($data);
	}
	function getLocation2()
	{
		$param = array('hotel_id'=>$_SESSION['app_user']['hotel_details_id']);	
		$data = $GLOBALS["db"]->select("SELECT m.mp_details_id, m.mp_details_name,m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude FROM mp_details m, hotel_request hr, hotel_details h WHERE h.hotel_details_id =:hotel_id AND h.hotel_details_id = hr.hotel_request_to AND m.mp_details_id = hr.hotel_request_from", $param);
		return json_encode($data);
	}
	function getLocation3()
	{
		$param = array('hotel_id'=>$_SESSION['app_user']['hotel_details_id']);	
		$data = $GLOBALS["db"]->select("SELECT m.mp_details_id, m.mp_details_name,m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude, hr.hotel_mp_relation_id, hr.hotel_mp_relation_hotel_id FROM mp_details m LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_mp_id = m.mp_details_id WHERE hr.hotel_mp_relation_hotel_id IS NULL GROUP BY m.mp_details_id", $param);
		return json_encode($data);
	}
	function getLocationDataHotels()
	{
		if($_POST['tab']=="tab_1")
		{
			if($_POST['category']=="" && $_POST['sortingValue']=="")
			{
				$query = " ORDER BY mp_dist_km ASC";
				$andQuery = "";
			}
			else
			{
				if(isset($_POST['category']) && $_POST['category']!="")
				{
					$_POST['category'] = implode(',', $_POST['category']);
					$andQuery = " AND m.mp_details_category IN (".$_POST['category'].")";
				}
				else
				{
					$andQuery = "";
				}
					if($_POST['sortingValue']=="dist_asc")
					{
						$query = " ORDER BY mp_dist_km ASC";
					}
					else if($_POST['sortingValue']=="dist_desc")
					{
						$query = " ORDER BY mp_dist_km DESC";
					}
					else if($_POST['sortingValue']=="rat_asc")
					{
						$query = " ORDER BY m.mp_details_rating ASC";
					}
					else
					{
						$query = " ORDER BY m.mp_details_rating DESC";
					}
			}
			$param = array();		
			$data[0] = $GLOBALS["db"]->select("SELECT m.mp_details_id, m.mp_details_name, m.mp_details_person, m.mp_details_contact, m.mp_details_description, m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude, hr.hotel_mp_relation_id, hr.hotel_mp_relation_recommend, hr.hotel_mp_relation_hotel_id, m.mp_details_rating, c.category_name, GROUP_CONCAT(DISTINCT s.sub_category_name) as sub_cat, COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(m.mp_details_latitude)) * COS(RADIANS(".$_SESSION['app_user']["hotel_details_latitude"].")) * COS(RADIANS(m.mp_details_longitude - ".$_SESSION['app_user']["hotel_details_longitude"]."))+ SIN(RADIANS(m.mp_details_latitude))* SIN(RADIANS(".$_SESSION['app_user']["hotel_details_latitude"].")))), 0) AS mp_dist_km, GROUP_CONCAT(g.gallery_actual_image) as mp_gallery FROM gallery g, mp_details_sub mds, category c, sub_category s, mp_details m LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_mp_id = m.mp_details_id WHERE c.category_id = m.mp_details_category AND mds.mp_details_sub_cat_id = s.sub_category_id AND mds.mp_details_mp_id = m.mp_details_id AND g.gallery_mp_details_id = m.mp_details_id".$andQuery." GROUP BY m.mp_details_id".$query, $param);
			$paramEmpty = array();
			$data[1] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as totalAmount FROM booking WHERE (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
			$data[2][] = array();
				if(!empty($data[0]))
				{
				for($i=0; $i<count($data[0]); $i++)
				{
					$data[2][] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as amount FROM booking WHERE booking_mp_id = ".$data[0][$i]['mp_details_id']." AND (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
				}
			}
		}
		else if($_POST['tab']=="tab_2")
		{
			$param = array('hotel_id'=>$_SESSION['app_user']['hotel_details_id']);	
			$data[0] = $GLOBALS["db"]->select("SELECT m.mp_details_id, m.mp_details_name, m.mp_details_description, m.mp_details_person, m.mp_details_contact, m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude, m.mp_details_rating, c.category_name, GROUP_CONCAT(DISTINCT s.sub_category_name) as sub_cat, COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(m.mp_details_latitude)) * COS(RADIANS(".$_SESSION['app_user']["hotel_details_latitude"].")) * COS(RADIANS(m.mp_details_longitude - ".$_SESSION['app_user']["hotel_details_longitude"]."))+ SIN(RADIANS(m.mp_details_latitude))* SIN(RADIANS(".$_SESSION['app_user']["hotel_details_latitude"].")))), 0) AS mp_dist_km, GROUP_CONCAT(g.gallery_actual_image) as mp_gallery FROM gallery g, category c, sub_category s, mp_details_sub mds, mp_details m, hotel_request hr, hotel_details h WHERE h.hotel_details_id =:hotel_id AND h.hotel_details_id = hr.hotel_request_to AND m.mp_details_id = hr.hotel_request_from AND c.category_id = m.mp_details_category AND mds.mp_details_sub_cat_id = s.sub_category_id AND mds.mp_details_mp_id = m.mp_details_id AND g.gallery_mp_details_id = m.mp_details_id GROUP BY m.mp_details_id ORDER BY mp_dist_km ASC", $param);
			$paramEmpty = array();
			$data[1] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as totalAmount FROM booking WHERE (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
			$data[2][] = array();
			if(!empty($data[0]))
			{
				for($i=0; $i<count($data[0]); $i++)
				{
					$data[2][] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as amount FROM booking WHERE booking_mp_id = ".$data[0][$i]['mp_details_id']." AND (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
				}
			}
		}
		else
		{
			$paramEmpty = array();
			$param = array('hotel_id'=>$_SESSION['app_user']['hotel_details_id']);					
			$data[0] = $GLOBALS["db"]->select("SELECT m.mp_details_id, m.mp_details_name, m.mp_details_description, m.mp_details_person, m.mp_details_contact,m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude, m.mp_details_rating, hr.hotel_mp_relation_hotel_id, hr.hotel_mp_relation_recommend, c.category_name, GROUP_CONCAT(DISTINCT s.sub_category_name) as sub_cat, COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(m.mp_details_latitude)) * COS(RADIANS(".$_SESSION['app_user']["hotel_details_latitude"].")) * COS(RADIANS(m.mp_details_longitude - ".$_SESSION['app_user']["hotel_details_longitude"]."))+ SIN(RADIANS(m.mp_details_latitude))* SIN(RADIANS(".$_SESSION['app_user']["hotel_details_latitude"].")))), 0) AS mp_dist_km, GROUP_CONCAT(g.gallery_actual_image) as mp_gallery FROM gallery g, mp_details_sub mds, category c, sub_category s, mp_details m LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_mp_id = m.mp_details_id WHERE hr.hotel_mp_relation_hotel_id =:hotel_id  AND c.category_id = m.mp_details_category AND mds.mp_details_sub_cat_id = s.sub_category_id AND mds.mp_details_mp_id = m.mp_details_id AND g.gallery_mp_details_id = m.mp_details_id GROUP BY m.mp_details_id ORDER BY mp_dist_km ASC", $param);
			$data[1] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as totalAmount FROM booking WHERE (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
			$data[2][] = array();
			if(!empty($data[0]))
			{
				for($i=0; $i<count($data[0]); $i++)
				{
					$data[2][] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as amount FROM booking WHERE booking_mp_id = ".$data[0][$i]['mp_details_id']." AND (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
				}
			}
		}
		return $data;
	}
	function getConnect()
	{
		$param = array("hotel_mp_relation_mp_id"=>$_POST['id'], "hotel_mp_relation_hotel_id"=>$_SESSION['app_user']['hotel_details_id']);
		$data = $GLOBALS["db"]->insert("hotel_mp_relation", $param);
		return $data;
	}
	function getRecommend()
	{
		$condition = array("hotel_mp_relation_mp_id"=>$_POST['id'], "hotel_mp_relation_hotel_id"=>$_SESSION['app_user']['hotel_details_id']);
		$param = array("hotel_mp_relation_recommend"=>1);
		$data = $GLOBALS["db"]->update("hotel_mp_relation", $param, $condition);
		return $data;
	}
}
?>