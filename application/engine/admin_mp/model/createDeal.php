<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class createDeal_model 
{
	function createDeal()
	{
		$param = array();
		$data[1]= $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_status = 1 AND mp_gallery_mp_id = ".$_SESSION['app_user']['mp_details_id']."", $param);
		$data[2]= $GLOBALS["db"]->select("SELECT * FROM mp_details_temp WHERE mp_details_id = ".$_SESSION['app_user']['mp_details_id']."", $param);
		return $data;		
	}
	function deal_image_show()
	{
		$image 			= $_POST['image'];
		$newNamePrefix 	= time() . '_';
		$manipulator 	= new imageManipulator($_FILES['imageMy']['tmp_name']);
		$imageName 		= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES['imageMy']['name']);
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . str_replace(' ', '_', $_FILES['imageMy']['name']));
		$tm 			= $manipulator->resample(190, 140, true);
		$media 			= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES['imageMy']['name']);
		$response['imageName'] = $media;
		$response['id'] = 1;
		return json_encode($response);	
	}
	function deal_image_show_profile()
	{
		$image 			= $_POST['image'];
		
		$newNamePrefix 	= time() . '_';
		$manipulator 	= new imageManipulator($_FILES[$image]['tmp_name']);
		$imageName 		= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix .  str_replace(' ', '_', $_FILES[$image]['name']));
		$tm 			= $manipulator->resample(190, 140, true);
		$media 			= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$response['imageName'] = $media;
		$response['id'] = 1;
		return json_encode($response);	
	}
	function deal_image_show_profile_update()
	{
		
		$image 			= $_POST['image'];
		$newNamePrefix 	= time() . '_';
		$manipulator 	= new imageManipulator($_FILES[$image]['tmp_name']);
		$imageName 		= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix .  str_replace(' ', '_', $_FILES[$image]['name']));
		$tm 			= $manipulator->resample(190, 140, true);
		$media 			= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$thumb 			= 'thumb/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$response['imageName'] = $media;
		$response['id'] = 1;
		
		
		if($_POST['mp_gallery_id_'.$image]	==	'')
		{
			
			$param = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_modified_by'=>$_SESSION['app_user']['mp_details_id']);
			
			$insert_temp 	= $GLOBALS["db"]->lastInsertNow('mp_gallery_temp', $param, 'mp_gallery_created_on');
				
			//$param1 = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_id'=>$insert_temp, 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_modified_by'=>$_SESSION['app_user']['mp_details_id']);
				
			//$insert		=	$GLOBALS["db"]->lastInsertNow('mp_gallery', $param1, 'mp_gallery_created_on');
			$response['gallery_id']	= $insert_temp;
			
		}
		else
		{
			$param = array('mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_modified_by'=>$_SESSION['app_user']['mp_details_id'],'mp_gallery_status'=>1);
			$condition = array("mp_gallery_id"=>$_POST['mp_gallery_id_'.$image]);
			$update = $GLOBALS["db"]->update('mp_gallery_temp', $param, $condition);
		}
		//-----Admin-Notification-----//
		$explode = explode('/', $imageName);
		$json_str	= $explode[1];	
		$notifiy_array	= array("an_mp_user_id"=>$_SESSION['app_user']['mp_details_id'],"an_process"=>"gallery_update","an_text"=>$json_str,"an_notify_from"=>"MP");
		$insert_notifiy = $GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
		//-----Admin-Notification-----//
		return json_encode($response);
	}
	function Create_deal()
	{
		$param 			= array("id"=>$_SESSION['app_user']['mp_details_id']);
		$data 			= $GLOBALS["db"]->select("SELECT * FROM deal_temp WHERE deal_mp_id  =:id AND deal_status  = 1", $param);
		
		if($_POST["main_deal_type"]	==	1)
		{
			if(count($data) == 0)
			{
					//------------------Getting Global variable for page--------------------
					$deal_name					=	$_POST['deal_name'];
					$deal_type					=	$_POST["main_deal_type"];
					$img_selected_url			=	$_POST['image_priority_url'];
					$Global_offer_array_hidden	=	$_POST['Global_offer_array_hidden'];
					$mp_id						=	$_SESSION['app_user']['mp_details_id'];
					//------------------Getting Global variable for page--------------------
					
					//------------------Deal + Deal gallery insert--------------------
					$param_temp				=	array("deal_name"=>$deal_name,"deal_type"=>$deal_type,"deal_mp_id"=>$mp_id);
					$deal_insert_id_temp 	=	$GLOBALS["db"]->lastInsertNow('deal_temp', $param_temp, 'deal_created_on');
					
					$offers_image	=	explode("#",$img_selected_url);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
							$gallery_param_temp = array("deal_image_media"=>$offers_image[$i],"deal_id"=>$deal_insert_id_temp,"deal_gallery_status"=>1);	
							$deal_gallery_id 	= $GLOBALS["db"]->lastInsertNow('deal_gallery_temp', $gallery_param_temp, 'deal_gallery_created_on');
						}
					}
					//------------------Deal + Deal gallery insert--------------------
					
					//-----START#Admin-Notification Table instertion-----//
					$notifiy_array	= array("an_mp_user_id"=>$deal_insert_id_temp,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP","an_deal_type"=>1);
					$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
					//-----END#Admin-Notification Table instertion-----//
					
					//------------------Offer insert--------------------
					if($Global_offer_array_hidden	!=	''){
						
						
						
						$Global_offer_array	=	explode("%%",$Global_offer_array_hidden);
						//flat#47#2#Mon - Fri#12:00 PM - 03:00 PM#0#NULL%%,combo#NULL#xxxx#Mon - Fri#09:00 AM - 12:00 PM#NULL#2#0%%
						for($i=0;$i<count($Global_offer_array);$i++)
						{
							//Flate offer
							$main_array_value		=	explode("#",$Global_offer_array[$i]);
							
							if(($main_array_value[0]	==	'flat') || ($main_array_value[0]	==	',flat'))
							{
								if(count($main_array_value)==8) // if max caping have value
								{
									$offer_mp_id		=	$mp_id;
									$offer_deal_id		=	$deal_insert_id_temp;
									$offer_type_id		=	1;
									if($main_array_value[6]	==	'NULL')
									{
										$min_billing		=	NULL;
									}
									else
									{
										$min_billing		=	$main_array_value[6];
									}
									$max_capping		=	$main_array_value[7];
									$offer_discount		=	$main_array_value[1];
									$offers_day_type	=	$main_array_value[2];
									$offers_day_value	=	$main_array_value[3];
									$offers_timeings	=	$main_array_value[4];
									$offer_coupon_status=	$main_array_value[5];
									$flat_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_discount"=>$offer_discount,"offer_deal_id"=>$offer_deal_id,"offer_type_id"=>$offer_type_id,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offers_day_value,"offers_timeings"=>$offers_timeings,"offer_coupon_status"=>$offer_coupon_status,"offer_status"=>1);
									
								
								}
								else// if max caping have not value
								{
									$offer_mp_id		=	$mp_id;
									$offer_deal_id		=	$deal_insert_id_temp;
									$offer_type_id		=	1;
									if($main_array_value[6]	==	'NULL')
									{
										$min_billing		=	NULL;
									}
									else
									{
										$min_billing		=	$main_array_value[6];
									}
									$offers_day_type	=	$main_array_value[2];
									$offer_discount		=	$main_array_value[1];
									$offers_day_value	=	$main_array_value[3];
									$offers_timeings	=	$main_array_value[4];
									$offer_coupon_status=	$main_array_value[5];
									$flat_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_discount"=>$offer_discount,"offer_deal_id"=>$offer_deal_id,"offer_type_id"=>$offer_type_id,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offers_day_value,"offers_timeings"=>$offers_timeings,"offer_coupon_status"=>$offer_coupon_status,"offer_status"=>1);
								
								}
								echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $flat_param, 'offer_created_on');
							}
							if(($main_array_value[0]	==	'combo') || ($main_array_value[0]	==	',combo'))
							{//combo offer
								if($main_array_value[7]	==	1) 
								{
									//deal type include
									$offer_mp_id		=	$mp_id;
									$offer_deal_id		=	$deal_insert_id_temp;
									if($main_array_value[1]	==	'NULL')
									{
										$offer_price		=	NULL;
									}
									else
									{
										$offer_price		=	$main_array_value[1];	
									}
									$offer_description	=	$main_array_value[2];
									$offer_type_id		=	2;
									if($main_array_value[5]	==	'NULL')
									{
										$min_billing		=	NULL;
									}
									else
									{
										$min_billing		=	$main_array_value[5];
									}
									
									$offers_day_type	=	$main_array_value[6];
									$offers_day_value	=	$main_array_value[3];
									$offers_timeings	=	$main_array_value[4];
									$offer_coupon_status=	$main_array_value[7];
									$max_capping		=	$main_array_value[8];
									$combo_param		=	array("offer_mp_id"=>$offer_mp_id,
																"offer_deal_id"=>$offer_deal_id,
																"offer_price"=>$offer_price,
																"offer_description"=>$offer_description,
																"offer_type_id"=>$offer_type_id,
																"max_capping"=>$max_capping,
																"min_billing"=>$min_billing,
																"offers_day_type"=>$offers_day_type,
																"offers_day_value"=>$offers_day_value,
																"offers_timeings"=>$offers_timeings,
																"offer_coupon_status"=>$offer_coupon_status,
																"offer_status"=>1);
								
								}
								else
								{
									//deal type not include
									$offer_mp_id		=	$mp_id;
									$offer_deal_id		=	$deal_insert_id_temp;
									if($main_array_value[1]	==	'NULL')
									{
										$offer_price		=	NULL;
									}
									else
									{
										$offer_price		=	$main_array_value[1];	
									}
										
									$offer_description	=	$main_array_value[2];
									$offer_type_id		=	2;
									if($main_array_value[5]	==	'NULL')
									{
										$min_billing		=	NULL;
									}
									else
									{
										$min_billing		=	$main_array_value[5];
									}
									$offers_day_type	=	$main_array_value[6];
									$offers_day_value	=	$main_array_value[3];
									$offers_timeings	=	$main_array_value[4];
									$offer_coupon_status=	$main_array_value[7];
									$combo_param	=	array(	"offer_mp_id"=>$offer_mp_id,
																"offer_deal_id"=>$offer_deal_id,
																"offer_price"=>$offer_price,
																"offer_description"=>$offer_description,
																"offer_type_id"=>$offer_type_id,
																"min_billing"=>$min_billing,
																"offers_day_type"=>$offers_day_type,
																"offers_day_value"=>$offers_day_value,
																"offers_timeings"=>$offers_timeings,
																"offer_coupon_status"=>$offer_coupon_status,"offer_status"=>1);
								}
								echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $combo_param, 'offer_created_on');
							}
							
							
						}
					}
					else
					{
						$offer_type		=	$_POST['offer_type'];
						if(isset($_POST['max_capping']))
						{
							$max_capping	=	$_POST['max_capping'];
						}
						else
						{
							$max_capping	=	NULL;
						}
						if(isset($_POST['min_billing']))
						{
							$min_billing	=	$_POST['min_billing'];
						}
						else
						{
							$min_billing	=	NULL;
						}
						$coupon_active_inactive	=	$_POST['coupon_active_inactive'];
						//Flat Offer
						if($offer_type	==	1)
						{
							$flat_discount			=	$_POST['flat_discount'];
							$offers_day_type		=	$_POST['offers_days'];
							$offer_day_hidden_type	=	$_POST['offer_day_hidden_type'];
							$offer_time_hidden_type	=	$_POST['offer_time_hidden_type'];
							//deal_insert_id_temp
							$param = array("offer_mp_id"=>$mp_id,"offer_deal_id"=>$deal_insert_id_temp, "offer_discount"=>$flat_discount, "offer_type_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
							
							$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
							echo $offer_id;
						}
						//Combo Offer
						if($offer_type	==	2)
						{
							$offers_day_type		=	$_POST['offers_days'];
							$offer_day_hidden_type	=	$_POST['offer_day_hidden_type'];
							$offer_time_hidden_type	=	$_POST['offer_time_hidden_type'];
							$offer_description		=	$_POST['short_desc'];
							$offer_price			=	$_POST['inc_price'];
							$param = array("offer_mp_id"=>$mp_id,"offer_deal_id"=>$deal_insert_id_temp,"offer_description"=>$offer_description,"offer_type_id"=>2,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive,"offer_price"=>$offer_price);
							$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
							echo $offer_id;
						}
					}
					//------------------Offer insert--------------------
				
				
					
			}
			else
			{
				$deal_type	=	$data[0]['deal_type'];
				$deal_name	=	$data[0]['deal_name'];
				if($deal_type	==	1)
				{
					$deal_type_name	=	'Normal Deal';
				}else{
					$deal_type_name	=	'Shoutout Deal';
				}
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$deal_name.' Deal is currently running!</h2><p style="text-align: center;"></p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new deal,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running deal!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['deal_id'].'">DISABLE</button></div></div>';
				
			}
		}
		else //Shout Deal code start here ------------------------------------------//
		{
			
			if(count($data) == 0)
			{
				
			//------------------Getting Global variable for page--------------------
			$deal_name						=	$_POST['deal_name'];
			$deal_type						=	$_POST["main_deal_type"];
			$img_selected_url				=	$_POST['image_priority_url'];
			$mp_id							=	$_SESSION['app_user']['mp_details_id'];
			$deal_shoutout_description		=	$_POST['so_desc'];
			$deal_days						=	$_POST["shoutout_deal_days"];
			$deal_duration					=	$_POST["offers_timeings_so_hidden"];
			$Global_offer_array_hidden_so	=	$_POST["Global_offer_array_hidden_so"];
			$add_more_offer_so_flag			=	$_POST["add_more_offer_so_flag"];
			//------------------Getting Global variable for page--------------------
			
			//------------------Deal + Deal gallery insert--------------------
			$param_temp				=	array("deal_name"=>$deal_name,"deal_shoutout_description"=>$deal_shoutout_description,"deal_shoutout_day"=>$deal_days,"deal_shoutout_time"=>$deal_duration,"deal_type"=>$deal_type,"deal_mp_id"=>$mp_id);
			$deal_insert_id_temp 	=	$GLOBALS["db"]->lastInsertNow('deal_temp', $param_temp, 'deal_created_on');
			
			$offers_image	=	explode("#",$img_selected_url);
			for($i=0;$i<count($offers_image);$i++)
			{
				if($offers_image[$i]!="")
				{
					$gallery_param_temp = array("deal_image_media"=>$offers_image[$i],"deal_id"=>$deal_insert_id_temp,"deal_gallery_status"=>1);	
					$deal_gallery_id 	= $GLOBALS["db"]->lastInsertNow('deal_gallery_temp', $gallery_param_temp, 'deal_gallery_created_on');
				}
			}
			//------------------Deal + Deal gallery insert--------------------
			
			//-----START#Admin-Notification Table instertion-----//
			$notifiy_array	= array("an_mp_user_id"=>$deal_insert_id_temp,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP","an_deal_type"=>2);
			$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
			//-----END#Admin-Notification Table instertion-----//
			
			//------------------Multiple offer insert-------------------------
			if($Global_offer_array_hidden_so	!=	"")
			{
				$Global_offer_array_hidden_so_array	=	explode("%%",$Global_offer_array_hidden_so);
				for($i=0;$i<count($Global_offer_array_hidden_so_array);$i++)
				{
					//Flate offer
					//1#19%%,2#tyhn%%
					$main_array_value		=	explode("#",$Global_offer_array_hidden_so_array[$i]);
					
					if(($main_array_value[0]	==	'1') || ($main_array_value[0]	==	',1'))
					{
						
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	1;
						$flat_discount_so	=	$main_array_value[1];
						$flat_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_deal_id"=>$offer_deal_id,"offer_discount"=>$flat_discount_so,"offer_type_id"=>$offer_type_id,"offer_status"=>1);
						echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $flat_param, 'offer_created_on');
					
					
					}
					if(($main_array_value[0]	==	'2') || ($main_array_value[0]	==	',2'))
					{//combo offer
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	2;
						$offer_desc			=	$main_array_value[1];
						$combo_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_deal_id"=>$offer_deal_id,"offer_type_id"=>$offer_type_id,"offer_status"=>1,"offer_description"=>$offer_desc);
						echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $combo_param, 'offer_created_on');
					}
				}
			}
			else
			{
				if($add_more_offer_so_flag	==	1)
				{
					$offer_type_so	=	$_POST["offer_type_so"];
					if($offer_type_so	==	1)//flate offer
					{
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	1;
						$flat_discount_so	=	$_POST["flat_discount_so"];
						$flat_param			=	array("offer_mp_id"=>$offer_mp_id,
											  	"offer_deal_id"=>$offer_deal_id,
											  	"offer_type_id"=>$offer_type_id,
											  	"offer_status"=>1,
											  	"offer_discount"=>$flat_discount_so);
						
					}else{	//combo offer
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	2;
						$offer_desc			=	$_POST["so_offer_desc"];
						$flat_param			=	array("offer_mp_id"=>$offer_mp_id,
											  	"offer_deal_id"=>$offer_deal_id,
											  	"offer_type_id"=>$offer_type_id,
											  	"offer_status"=>1,
											  	"offer_description"=>$offer_desc);
												
						
					}
					echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $flat_param, 'offer_created_on');
				}else{ echo '1'; }
			}
			//------------------Multiple offer insert-------------------------
		
			}
			else
			{
				$deal_type	=	$data[0]['deal_type'];
				$deal_name	=	$data[0]['deal_name'];
				if($deal_type	==	1)
				{
					$deal_type_name	=	'Normal Deal';
				}else{
					$deal_type_name	=	'Shoutout Deal';
				}
				
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$deal_name.' Deal is currently running!</h2><p style="text-align: center;"></p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new deal,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running deal!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['deal_id'].'">DISABLE</button></div></div>';
			
			}
		}
		die;
		//------------------Old Code from here start--------------------
		//------------------Getting Global variable for page--------------------
		$offer_type				=	$_POST['offer_type'];
		$deal_name				=	$_POST['deal_name'];
		$deal_desc				=	'NULL';
		$img_selected_url		=	$_POST['image_priority_url'];
		
		if(isset($_POST['max_capping']))
		{
			$max_capping	=	$_POST['max_capping'];
		}
		else
		{
			$max_capping	=	NULL;
		}
		$min_billing			=	$_POST['min_billing'];
		$image_priority_url		=	$_POST['image_priority_url'];
		$coupon_active_inactive	=	$_POST['coupon_active_inactive'];
		//------------------Getting Global variable for page--------------------
	
		//For Flat Offer
		if($offer_type	==	1)
		{
			//Find out how many Flat deal create it
			$param 	= array("id"=>$_SESSION['app_user']['mp_details_id']);
			$offer_mp_id	=	$_SESSION['app_user']['mp_details_id'];
			$data 	= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//one flat deal is create it
			if(count($data) == 0)							
			{
				//echo 'if';
				//$input_value	=	explode("$#$",$_POST['input_value']);
				$flat_discount	=	$_POST['flat_discount'];
				//$offer_timeing	=	$_POST['offer_timeing'];
				
				$offers_day_type	=	$_POST['offers_days'];
				$offer_day_hidden_type=	$_POST['offer_day_hidden_type'];
				$offer_time_hidden_type=	$_POST['offer_time_hidden_type'];
				
				
				
				$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_discount"=>$flat_discount, "offer_type_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
				
				$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
				$lastID;
				if(is_numeric($offer_id))
				{
					$offers_image	=	explode("#",$_POST['image_priority_url']);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
						$gallery_param_temp = array("offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						$offer_gallery_id 	= $GLOBALS["db"]->lastInsertNow('offers_gallery_temp', $gallery_param_temp, 'offer_gallery_created_on');
						
						//$gallery_param 		= array("offer_gallery_id"=>$offer_gallery_id,"offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						//$offer_gallery_idd 	= $GLOBALS["db"]->lastInsertNow('offers_gallery', $gallery_param, 'offer_gallery_created_on');
						$lastID	=	$offer_gallery_id;
						}
						
					}
				}
				if(is_numeric($lastID)){
					//-----START#Admin-Notification Table instertion-----//
					$notifiy_array	= array("an_mp_user_id"=>$offer_id,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP");
					$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
					//-----END#Admin-Notification Table instertion-----//
				}
				echo $lastID;
				
			}
			else//Show old data and tell him to disbale old flat deal first.
			{
				$offer_type_name	=	$data[0]['offer_type_id'];
				$offer_name			=	$data[0]['deal_name'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' Offer is currently running!</h2><p style="text-align: center;">'.$desc.'</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new offer,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running offer!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['offer_id'].'">DISABLE</button></div></div>';
			}
		}
		//For Combo Offer
		if($offer_type	==	2)
		{
			//Check how many offer which is LIVE
			$param 			= array("id"=>$_SESSION['app_user']['mp_details_id']);
			$offer_mp_id	=	$_SESSION['app_user']['mp_details_id'];
			$data 			= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//One Combo Offer is create it
			
			
			$offers_day_type	=	$_POST['offers_days'];
			$offer_day_hidden_type=	$_POST['offer_day_hidden_type'];
			$offer_time_hidden_type=	$_POST['offer_time_hidden_type'];
			
			
			if(count($data) == 0)							
			{
				//$input_value	=	explode("$#$",$_POST['input_value']);
				//$offer_timeing	=	$_POST['offer_timeing'];
				/*if($offer_timeing	==	1)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion, "offer_type_id"=>2, "offer_timeing_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);	
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
				}
				if($offer_timeing	==	2)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					$particular_date_1	=	$_POST['pariticular_date_input'];
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1" => $particular_date_1, "offer_type_id"=>2, "offer_timeing_id"=>2,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1" => $particular_date_1, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>2, "offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
				}
				if($offer_timeing	==	3)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					$particular_date_1	=	$_POST['pariticular_date_time_input'];
					//$time				=	$_POST['pariticular_date_time_input_hh'].":".$_POST['pariticular_date_time_input_mm'];
					$time_1					=	$_POST['pariticular_date_time_input_hh'];
					$ampm_1					=	$_POST['pariticular_date_time_input_hh_hh'];
					$time_2					=	$_POST['pariticular_date_time_input_mm'];
					$ampm_2					=	$_POST['pariticular_date_time_input_mm_mm'];
					$time1					=	$time_1." ".$ampm_1;
					$time2					=	$time_2." ".$ampm_2;
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_type_id"=>2, "offer_timeing_id"=>3,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>3, "offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
				}
				if($offer_timeing	==	4)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					$particular_date_1	=	$_POST['custom_date_time_input_1'];
					$particular_date_2	=	$_POST['custom_date_time_input_2'];
					//$time				=	$_POST['custom_date_time_input_hh'].":".$_POST['custom_date_time_input_mm'];
					$time_1						=	$_POST['custom_date_time_input_hh'];
					$ampm_1						=	$_POST['custom_date_time_input_hh_hh'];
					$time_2						=	$_POST['custom_date_time_input_mm'];
					$ampm_2						=	$_POST['custom_date_time_input_mm_mm'];
					$time1						=	$time_1." ".$ampm_1;
					$time2						=	$time_2." ".$ampm_2;
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_date_2"=>$particular_date_2,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_type_id"=>2, "offer_timeing_id"=>4,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_date_2"=>$particular_date_2,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>4, "offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
			}*/
			
				$offer_description	=	$_POST['short_desc'];
			
				$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_description,"offer_type_id"=>2,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
			
				$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
				
				$lastID;
				if(is_numeric($offer_id))
				{
					$offers_image	=	explode("#",$_POST['image_priority_url']);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
						$gallery_param_temp = array("offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						$offer_gallery_id 	= $GLOBALS["db"]->lastInsertNow('offers_gallery_temp', $gallery_param_temp, 'offer_gallery_created_on');
						
						//$gallery_param 		= array("offer_gallery_id"=>$offer_gallery_id,"offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						//$offer_gallery_idd 	= $GLOBALS["db"]->lastInsertNow('offers_gallery', $gallery_param, 'offer_gallery_created_on');
						$lastID	=	$offer_gallery_id;
						}
						
					}
					if(is_numeric($lastID)){
						//-----START#Admin-Notification Table instertion-----//
						$notifiy_array	= array("an_mp_user_id"=>$offer_id,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP");
						$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
						//-----END#Admin-Notification Table instertion-----//
					}
					echo $lastID;
				}
				
			}
			else//Show old data and tell him to disbale old deal first.
			{
				$offer_type_name	=	$data[0]['offer_type_id'];
				$offer_name			=	$data[0]['deal_name'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' Offer is currently running!</h2><p style="text-align: center;">'.$desc.'</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new offer,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running offer!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['offer_id'].'">DISABLE</button></div></div>';
			}
		}
		//For shout out
		/*if($offer_type	==	3)
		{
			//Check how many offer which is LIVE
			$param 			= array("id"=>$_SESSION['app_user']['mp_details_id']);
			$offer_mp_id	=	$_SESSION['app_user']['mp_details_id'];
			$data 			= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//One Combo Offer is create it
			
			
			$offers_day_type	=	$_POST['offers_days'];
			$offer_day_hidden_type=	$_POST['offer_day_hidden_type'];
			$offer_time_hidden_type=	$_POST['offer_time_hidden_type'];
			
			
			if(count($data) == 0)							
			{
				$offer_description	=	$_POST['short_desc'];
			
				$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_description,"offer_type_id"=>3,"offer_status"=>1,"image_priority"=>$img_selected_url,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
			
				$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
				
				$lastID;
				if(is_numeric($offer_id))
				{
					$offers_image	=	explode("#",$_POST['image_priority_url']);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
						$gallery_param_temp = array("offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						$offer_gallery_id 	= $GLOBALS["db"]->lastInsertNow('offers_gallery_temp', $gallery_param_temp, 'offer_gallery_created_on');
						
						//$gallery_param 		= array("offer_gallery_id"=>$offer_gallery_id,"offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						//$offer_gallery_idd 	= $GLOBALS["db"]->lastInsertNow('offers_gallery', $gallery_param, 'offer_gallery_created_on');
						$lastID	=	$offer_gallery_id;
						}
						
					}
					if(is_numeric($lastID)){
						//-----START#Admin-Notification Table instertion-----//
						$notifiy_array	= array("an_mp_user_id"=>$offer_id,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP");
						$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
						//-----END#Admin-Notification Table instertion-----//
					}
					echo $lastID;
				}
				
			}
			else//Show old data and tell him to disbale old deal first.
			{
				$offer_type_name	=	$data[0]['offer_type_id'];
				$offer_name			=	$data[0]['deal_name'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' Offer is currently running!</h2><p style="text-align: center;">'.$desc.'</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new offer,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running offer!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['offer_id'].'">DISABLE</button></div></div>';
			}
		}*/
	}
	function edit_deal_shoutout()
	{
		//--------------------------------------------------Deal Update------------------------------------------//
		
		$deal_values_array			=	explode("#",$_POST["deal_values"]);
		$deal_id					=	$_POST["deal_id"];
		$deal_name					=	$deal_values_array[0];
		$deal_shoutout_description	=	$deal_values_array[1];
		$deal_shoutout_day			=	$deal_values_array[2];
		$deal_shoutout_time			=	$deal_values_array[3];		
		
		$condition 			=	array('deal_id'=>$deal_id);
		$deal_name_update 	=	array("deal_name"=>$deal_name,"deal_shoutout_description"=>$deal_shoutout_description,"deal_shoutout_day"=>$deal_shoutout_day,"deal_shoutout_time"=>$deal_shoutout_time);
		$res	=	$insert_deal_name 	=	$GLOBALS["db"]->update('deal_temp', $deal_name_update, $condition);
		if($res	==	"true")
		{
			echo 1;
		}else{
			echo 0;
		}
		//--------------------------------------------------Deal Update------------------------------------------//
		
		//--------------------------------------------------Gallery Update------------------------------------------//
		$image_status			=	$_POST['image_status'];
		if($image_status	==	1)
		{
			$image_priority_url		=	$_POST["image_priority_url"];
			$image_url				=	explode("#",$image_priority_url);
			$param_delete			=	array("deal_id"=>$deal_id);
			$delete 				= 	$GLOBALS["db"]->deleteBulkQuery('deal_gallery_temp', $param_delete);
			$image_count			=	count($image_url);
			if($delete	==	"true")
			{	
				for($i=0;$i<$image_count;$i++)
				{
					$param_gallery_update 	=	array("deal_id"=>$deal_id,"deal_image_media"=>$image_url[$i]);
					$insert_up				= 	$GLOBALS["db"]->insertNow('deal_gallery_temp', $param_gallery_update, 'deal_gallery_created_on');
				}
			}
		}
		//--------------------------------------------------Gallery Update------------------------------------------//
		
		//--------------------------------------------------offer Update------------------------------------------//
		$offer_shout_open	=	$_POST["offer_shout_open"];
		
		if($offer_shout_open	!=	'')
		{
			$offer_values_array	=	explode("#",$offer_shout_open);
			$offertype			=	$offer_values_array[1];
			$offerid			=	$offer_values_array[2];
			$condition 			=	array('offer_id'=>$offerid);
			
			if($offertype	==	'flat')
			{
				$offerdiscount	=	$offer_values_array[3];
				$shout_param 	=	array("offer_discount"=>$offerdiscount);
			}
			if($offertype	==	'combo')
			{
				$offerdesc		=	$offer_values_array[3];
				$shout_param 	=	array("offer_description"=>$offerdesc);
			}
			
			$insert_deal_name1 	=	$GLOBALS["db"]->update('offers_temp', $shout_param, $condition);
			
		}
		//--------------------------------------------------offer Update------------------------------------------//
	}
	function edit_deal()
	{
		
		//--------------------------------------------------Normal deal------------------------------------------//
		$form_value_array		=	explode("#",$_POST["id"]);
		$status					=	$form_value_array[0];
		$offer_type				=	$form_value_array[1];
		$offer_id				=	$form_value_array[2];
		if($offer_type	==	'name')
		{
			$condition 			=	array('deal_id'=>$_POST["deal_id"]);
			$deal_name_update 	=	array("deal_name"=>$_POST["deal_name"]);
			$insert_deal_name 	=	$GLOBALS["db"]->update('deal_temp', $deal_name_update, $condition);
			if($insert_deal_name	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		if($offer_type	==	'image')
		{
			$image_priority_url		=	$_POST["image_priority_url"];
			$deal_id				=	$_POST["deal_id"];
			$image_url				=	explode("#",$image_priority_url);
			$param_delete			=	array("deal_id"=>$deal_id);
			$delete 				= 	$GLOBALS["db"]->deleteBulkQuery('deal_gallery_temp', $param_delete);
			$image_count			=	count($image_url);
			if($delete	==	"true")
			{	
				for($i=0;$i<$image_count;$i++)
				{
					$param_gallery_update 	=	array("deal_id"=>$deal_id,"deal_image_media"=>$image_url[$i]);
					$insert_up				= 	$GLOBALS["db"]->insertNow('deal_gallery_temp', $param_gallery_update, 'deal_gallery_created_on');
				}
			
			}
			if($delete	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		if($offer_type	==	'flat')
		{
			$discount		=	$form_value_array[3];
			if($form_value_array[4]	==	'NULL')
			{
				$minbilling		=	NULL;
			}
			else
			{
				$minbilling		=	$form_value_array[4];
			}
			$offer_day_type	=	$form_value_array[5];
			$offer_day_value=	$form_value_array[6];
			$offer_timeings	=	$form_value_array[7];
			$coupon_status	=	$form_value_array[8];
			$max_capping	=	$form_value_array[9];
			
			$condition 		=	array('offer_id'=>$offer_id);
			$param 			=	array(	"offer_discount"=>$discount,
										"min_billing"=>$minbilling,
										"offers_day_type"=>$offer_day_type,
										"offers_day_value"=>$offer_day_value,
										"offers_timeings"=>$offer_timeings,
										"offer_coupon_status"=>$coupon_status,
										"max_capping"=>$max_capping);
										
			$data 		=	$GLOBALS["db"]->update('offers_temp', $param, $condition);
			if($data	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		if($offer_type	==	'combo')
		{
			$offer_desc		=	$form_value_array[3];
			if($form_value_array[4]	==	'NULL')
			{
				$include_price	=	NULL;
			}
			else
			{
				$include_price	=	$form_value_array[4];
			}
			if($form_value_array[5]	==	'NULL')
			{
				$minbilling		=	NULL;
			}
			else
			{
				$minbilling		=	$form_value_array[5];
			}
			$offer_day_type	=	$form_value_array[6];
			$offer_day_value=	$form_value_array[7];
			$offer_timeings	=	$form_value_array[8];
			$coupon_status	=	$form_value_array[9];
			$max_capping	=	$form_value_array[10];
			
			$condition 		=	array('offer_id'=>$offer_id);
			$param 			=	array(	"offer_description"=>$offer_desc,
										"offer_price"=>$include_price,
										"min_billing"=>$minbilling,
										"offers_day_type"=>$offer_day_type,
										"offers_day_value"=>$offer_day_value,
										"offers_timeings"=>$offer_timeings,
										"offer_coupon_status"=>$coupon_status,
										"max_capping"=>$max_capping);
										
			$data 		=	$GLOBALS["db"]->update('offers_temp', $param, $condition);
			if($data	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		//--------------------------------------------------Normal deal------------------------------------------//
		die;
		//array_hidden
		//array_img_status
		$offers_day_type		=	$_POST['offers_days'];
		$offer_day_hidden_type	=	$_POST['offer_day_hidden_type'];
		$offer_time_hidden_type	=	$_POST['offer_time_hidden_type'];
		$offer_image_status		=	$_POST['array_img_status'];
		$coupon_active_inactive	=	$_POST['coupon_active_inactive'];
		$ah						=	'';
		if(empty($_POST['array_hidden']))
		{
			$ah		=	'1';	//blank
		}else{
			$ah		=	'0';	//not blank
		}
		//CASE - 1 = when offer_image_status = 1 or array_chnages = NULL
		
		if(($offer_image_status == "") && ($ah == 1))
		{
			echo 1;
		}
		else if(($offer_image_status=="")&& ($ah == 0))
		{
			if(isset($_POST['deal_name'])){
				$deal_name	=	$_POST['deal_name'];
			}else{
				$deal_name	=	'';
			}
			if(isset($_POST['dealText'])){
					$deal_desc	=	$_POST['dealText'];
				}else{
					$deal_desc='';
			}
			if(isset($_POST['short_desc'])){
					$offer_description	=	$_POST['short_desc'];
				}else{
					$offer_description	=	'';
			}
			if(isset($_POST['price'])){
					$price	=	$_POST['price'];
				}else{
					$price	=	'';
			}
			if(isset($_POST['range_flat'])){
					$offer_discount	=	$_POST['range_flat'];
				}else{
					$offer_discount	=	'';
			}
			if(isset($_POST['saveAmount'])){
					$offer_save_price	=	$_POST['saveAmount'];
				}else{
					$offer_save_price	=	'';
			}
			if(isset($_POST['saveAmount'])){
					$offer_save_price	=	$_POST['saveAmount'];
				}else{
					$offer_save_price	=	'';
			}
			if(isset($_POST['offer_selling_price'])){
					$offer_selling_price	=	$_POST['offer_selling_price'];
				}else{
					$offer_selling_price	=	'';
			}
			if(isset($_POST['max_capping'])){
					$max_capping	=	$_POST['max_capping'];
				}else{
					$max_capping	=	NULL;
			}
			if(isset($_POST['min_billing'])){
					$min_billing	=	$_POST['min_billing'];
				}else{
					$min_billing	=	'';
			}
			
			$hidden_time			=	$_POST["hidden_time"];
			$hidden_time_value		=	explode("$$",$hidden_time);
			$selected_offer_timeing =	$hidden_time_value[0];
			
			/*if($selected_offer_timeing	==	1){//all_date
				$offer_pariticular_date_1	=	'';
				$offer_pariticular_date_2	=	'';
				$offer_pariticular_time_1	=	'';
				$offer_pariticular_time_2	=	'';
			}
			if($selected_offer_timeing	==	2){//pariticular_date
				$offer_pariticular_date_1	=	$hidden_time_value[1];
				$offer_pariticular_date_2	=	'';
				$offer_pariticular_time_1	=	'';
				$offer_pariticular_time_2	=	'';
			}
			if($selected_offer_timeing	==	3){//pariticular_date_time
				$offer_pariticular_date_1	=	$hidden_time_value[1];
				$offer_pariticular_date_2	=	'';
				$offer_pariticular_time_1	=	$hidden_time_value[2].' '.$hidden_time_value[3];
				$offer_pariticular_time_2	=	$hidden_time_value[4].' '.$hidden_time_value[5];
			}
			if($selected_offer_timeing	==	4){//custom_date_time
				$offer_pariticular_date_1	=	$hidden_time_value[1];
				$offer_pariticular_date_2	=	$hidden_time_value[2];
				$offer_pariticular_time_1	=	$hidden_time_value[3].' '.$hidden_time_value[4];
				$offer_pariticular_time_2	=	$hidden_time_value[5].' '.$hidden_time_value[6];
			}*/
			
			$img_selected_url				=	$_POST['image_priority_url'];
			
			$condition 	=	array('offer_id'=>$_POST['offer_id']);
			$param 		=	array(	"deal_name"=>$deal_name,
									'image_priority'=>$img_selected_url,
									'offer_description'=>$offer_description,
									//'offer_price'=>$price,
									'offer_discount'=>$offer_discount,
									//'offer_save_price'=>$offer_save_price,
									//'offer_selling_price'=>$offer_selling_price,
									//'offer_timeing_id'=>$selected_offer_timeing,
									//'offer_pariticular_date_1'=>$offer_pariticular_date_1,
									//'offer_pariticular_date_2'=>$offer_pariticular_date_2,
									//'offer_pariticular_time_1'=>$offer_pariticular_time_1,
									//'offer_pariticular_time_2'=>$offer_pariticular_time_2,
									"offers_day_type"=>$offers_day_type,
									"offers_day_value"=>$offer_day_hidden_type,
									"offers_timeings"=>$offer_time_hidden_type,
									"max_capping"=>$max_capping,
									"min_billing"=>$min_billing,
									"offer_coupon_status"=>$coupon_active_inactive);
			
			
			//-----------------------------------Gallery Update Here End------------------------------------------//
			
			$data 		=	$GLOBALS["db"]->update('offers_temp', $param, $condition);
			if($data	==	"true"){
			echo 1;
			}else{
			echo 0;

			}
		}
		else if(($offer_image_status==0)&& ($ah == 1))
		{
			//-----------------------------------Gallery Update Here Start------------------------------------------//	
			$offer_id			=	$_POST["offer_id"];
			$image_priority_url	=	$_POST["image_priority_url"];
			$image_url			=	explode("#",$image_priority_url);
			$offer_image_details = array();
			if(trim($_POST["offer_image_details"])!="")
			{
				$offer_image_details		=	explode(",",$_POST["offer_image_details"]);
			}
			$image_count	=	count($image_url);
			$param 			= 	array();
			$param_delete	=	array("offer_id"=>$offer_id);
			$delete 		= 	$GLOBALS["db"]->deleteBulkQuery('offers_gallery_temp', $param_delete);
			
			if($delete	==	"true")
			{
				for($i=0;$i<$image_count;$i++)
				{
					if(!isset($offer_image_details[$i]))
					{
						$offer_image_details[$i] = NULL;
					}
					$param_gallery_update 		=	array("offer_id"=>$offer_id,"offer_image_media"=>$image_url[$i],"offer_image_details"=>$offer_image_details[$i]);
					$insert_up				= 	$GLOBALS["db"]->insertNow('offers_gallery_temp', $param_gallery_update, 'offer_gallery_created_on');
				}
			}
			else
			{
				echo "else delete";
			}
			$condition = array("offer_id"=>$offer_id);
			$paramOffer = array("offer_image_status"=>0);
			$update = $GLOBALS["db"]->update('offers_temp', $paramOffer, $condition);
			if($update	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		else if(($offer_image_status==0)&& ($ah == 0))
		{
			//-----------------------------------Gallery Update Here Start------------------------------------------//	
			$offer_id			=	$_POST["offer_id"];
			$image_priority_url	=	$_POST["image_priority_url"];
			$image_url		=	explode("#",$image_priority_url);
			$offer_image_details = array();
			if(trim($_POST["offer_image_details"])!="")
			{
				$offer_image_details		=	explode(",",$_POST["offer_image_details"]);
			}
			$image_count	=	count($image_url);
			$param 			= 	array();
			$param_delete	=	array("offer_id"=>$offer_id);
			$delete 		= 	$GLOBALS["db"]->deleteBulkQuery('offers_gallery_temp', $param_delete);
			//$data[0]		= $GLOBALS["db"]->select("SELECT * FROM offers_gallery_temp WHERE offer_id = '".$offer_id."' AND offer_gallery_status = 1", $param);
			if($delete	==	"true")
			{
				for($i=0;$i<$image_count;$i++)
				{
					if(!isset($offer_image_details[$i]))
					{
						$offer_image_details[$i] = NULL;
					}
					$param_gallery_update 		=	array("offer_id"=>$offer_id,"offer_image_media"=>$image_url[$i],"offer_image_details"=>$offer_image_details[$i]);
					$insert_up				= 	$GLOBALS["db"]->insertNow('offers_gallery_temp', $param_gallery_update, 'offer_gallery_created_on');
				}
			}
			else
			{
				echo "else delete";
			}
			
			$condition = array("offer_id"=>$offer_id);
				$paramOffer = array("offer_image_status"=>0);
				$GLOBALS["db"]->update('offers_temp', $paramOffer, $condition);
			//-----------------------------------Gallery Update Here End------------------------------------------//
			//str_replace("media/","details/",$image_url[$i]);
			
			
			//-----------------------------------Gallery Update Here Start------------------------------------------//
			
				if(isset($_POST['deal_name'])){
					$deal_name	=	$_POST['deal_name'];
				}else{
					$deal_name	=	'';
			}
			if(isset($_POST['dealText'])){
					$deal_desc	=	$_POST['dealText'];
				}else{
					$deal_desc='';
			}
			if(isset($_POST['short_desc'])){
					$offer_description	=	$_POST['short_desc'];
				}else{
					$offer_description	=	'';
			}
			if(isset($_POST['price'])){
					$price	=	$_POST['price'];
				}else{
					$price	=	'';
			}
			if(isset($_POST['range_flat'])){
					$offer_discount	=	$_POST['range_flat'];
				}else{
					$offer_discount	=	'';
			}
			if(isset($_POST['saveAmount'])){
					$offer_save_price	=	$_POST['saveAmount'];
				}else{
					$offer_save_price	=	'';
			}
			if(isset($_POST['saveAmount'])){
					$offer_save_price	=	$_POST['saveAmount'];
				}else{
					$offer_save_price	=	'';
			}
			if(isset($_POST['offer_selling_price'])){
					$offer_selling_price	=	$_POST['offer_selling_price'];
				}else{
					$offer_selling_price	=	'';
			}
			if(isset($_POST['max_capping'])){
					$max_capping	=	$_POST['max_capping'];
				}else{
					$max_capping	=	'';
			}
			if(isset($_POST['min_billing'])){
					$min_billing	=	$_POST['min_billing'];
				}else{
					$min_billing	=	'';
			}
			
			$hidden_time			=	$_POST["hidden_time"];
			$hidden_time_value		=	explode("$$",$hidden_time);
			$selected_offer_timeing =	$hidden_time_value[0];
			
			/*if($selected_offer_timeing	==	1){//all_date
				$offer_pariticular_date_1	=	'';
				$offer_pariticular_date_2	=	'';
				$offer_pariticular_time_1	=	'';
				$offer_pariticular_time_2	=	'';
			}
			if($selected_offer_timeing	==	2){//pariticular_date
				$offer_pariticular_date_1	=	$hidden_time_value[1];
				$offer_pariticular_date_2	=	'';
				$offer_pariticular_time_1	=	'';
				$offer_pariticular_time_2	=	'';
			}
			if($selected_offer_timeing	==	3){//pariticular_date_time
				$offer_pariticular_date_1	=	$hidden_time_value[1];
				$offer_pariticular_date_2	=	'';
				$offer_pariticular_time_1	=	$hidden_time_value[2].' '.$hidden_time_value[3];
				$offer_pariticular_time_2	=	$hidden_time_value[4].' '.$hidden_time_value[5];
			}
			if($selected_offer_timeing	==	4){//custom_date_time
				$offer_pariticular_date_1	=	$hidden_time_value[1];
				$offer_pariticular_date_2	=	$hidden_time_value[2];
				$offer_pariticular_time_1	=	$hidden_time_value[3].' '.$hidden_time_value[4];
				$offer_pariticular_time_2	=	$hidden_time_value[5].' '.$hidden_time_value[6];
			}*/
			
			$img_selected_url				=	$_POST['image_priority_url'];
			
			$condition 	=	array('offer_id'=>$_POST['offer_id']);
			$param 		=	array(	"deal_name"=>$deal_name,
									'image_priority'=>$img_selected_url,
									'offer_description'=>$offer_description,
									//'offer_price'=>$price,
									'offer_discount'=>$offer_discount,
									//'offer_save_price'=>$offer_save_price,
									//'offer_selling_price'=>$offer_selling_price,
									//'offer_timeing_id'=>$selected_offer_timeing,
									//'offer_pariticular_date_1'=>$offer_pariticular_date_1,
									//'offer_pariticular_date_2'=>$offer_pariticular_date_2,
									//'offer_pariticular_time_1'=>$offer_pariticular_time_1,
									//'offer_pariticular_time_2'=>$offer_pariticular_time_2,
									"offers_day_type"=>$offers_day_type,
									"offers_day_value"=>$offer_day_hidden_type,
									"offers_timeings"=>$offer_time_hidden_type,
									"max_capping"=>$max_capping,
									"min_billing"=>$min_billing,
									"offer_coupon_status"=>$coupon_active_inactive);
			
			
			//-----------------------------------Gallery Update Here End------------------------------------------//
			
			$data 		=	$GLOBALS["db"]->update('offers_temp', $param, $condition);
			if($data	==	"true"){
			echo 1;
			}else{
			echo 0;
			}
		}
		
	}
	function Create_a_offer()
	{
		$offer_mp_id	=	$_SESSION['app_user']['mp_details_id'];
		if($_POST['flag_check']	==	'flat')
		{
			//Find out how many Flat deal create it
			$param 	= array("id"=>$_SESSION['app_user']['mp_details_id']);
			$data 	= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//one flat deal is create it
			if(count($data) == 0)							
			{
				//echo 'if';
				$input_value	=	explode("$#$",$_POST['input_value']);
				$flat_discount	=	$input_value[0];
				$offer_timeing	=	$input_value[1];
				if($offer_timeing	==	1)
				{
				$deal_count		=		$input_value[2];
				//offer_timeing_id
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_discount"=>$flat_discount, 'offer_type_id'=>1,'offer_size'=> $deal_count,'offer_timeing_id'=>1,"offer_status"=>1);
				
				}
				if($offer_timeing	==	2)
				{
				$particular_date_2	=	$input_value[2];
				$deal_count			=	$input_value[3];
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_discount"=>$flat_discount, "offer_type_id"=>1, "offer_size"=> $deal_count, "offer_timeing_id"=>2 , "offer_pariticular_date_1" => $particular_date_2,"offer_status"=>1);
				}
				if($offer_timeing	==	3)
				{
				$particular_date_3		=	$input_value[2];
				$particular_time_HH_3	=	$input_value[3];
				$particular_time_MM_3	=	$input_value[4];
				$time					=	$particular_time_HH_3 .":".$particular_time_MM_3;
				$deal_count				=	$input_value[5];
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_discount"=>$flat_discount, "offer_type_id"=>1, "offer_size"=> $deal_count, "offer_timeing_id"=>3 , "offer_pariticular_date_1" => $particular_date_3, "offer_pariticular_time" => $time,"offer_status"=>1);
				}
				if($offer_timeing	==	4)
				{
				$particular_date_start_4	=	$input_value[2];
				$particular_date_end_4		=	$input_value[3];
				$particular_time_HH_3		=	$input_value[4];
				$particular_time_MM_3		=	$input_value[5];
				$time						=	$particular_time_HH_3 .":".$particular_time_MM_3;
				$deal_count					=	$input_value[6];
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_discount"=>$flat_discount, "offer_type_id"=>1, "offer_size"=> $deal_count, "offer_timeing_id"=>4 , "offer_pariticular_date_1" => $particular_date_start_4, "offer_pariticular_date_2" => $particular_date_end_4, "offer_pariticular_time" => $time,"offer_status"=>1);
				}
				
				//Insert in Offer Table
				$insert = $GLOBALS["db"]->insertNow('offers_temp', $param, 'offer_created_on');
				return $insert;
			}
			else//Show old data and tell him to disbale old flat deal first.
			{
				$offer_type_name	=	$data[0]['offer_type_id'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				echo '<style>.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {border: 1px solid !important;}</style><div><h2 style="text-align: center;">You have already running Offer</h2><p style="    text-align: center;">If you want to create this Offer, first disbale old Offer</p><div class="table-responsive" style="margin-top:20px;"> <table class="table" style="margin-bottom: 0px;"> <thead> <tr> <th>#</th> <th>Deal Type</th> <th>Deal Descripition</th><th>Status</th></tr></thead> <tbody> <tr> <td>1</td><td>'.$offer_type_name.'</td><td>'.$desc.'</td><td><button id="disable_deal" type="button" class="btn btn-warning" value="'.$data[0]['offer_id'].'">Disable</button></td></tr></tbody> </table> </div></div>';
			}
		}
		else
		{
			//Find out how many Flat deal create it
			$param 	= array("id"=>$_SESSION['app_user']['mp_details_id']);
			$data 	= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//one flat deal is create it
			if(count($data) == 0)							
			{
				$input_value	=	explode("$#$",$_POST['input_value']);
				$offer_timeing	=	$input_value[0];
				if($offer_timeing	==	1)
				{
				$offer_descriptiion	=	$input_value[1];
				$orignal_price		=	$input_value[2];
				$offer_price		=	$input_value[3];
				$discount			=	$input_value[4];
				$saved_money		=	$input_value[5];
				$deal_count			=	$input_value[6];
				
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_description"=>$offer_descriptiion, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>1, "offer_size"=>$deal_count,"offer_status"=>1);
			}
				if($offer_timeing	==	2)
				{
				$offer_pariticular_date_1	=	$input_value[1];
				$offer_descriptiion			=	$input_value[2];
				$orignal_price				=	$input_value[3];
				$offer_price				=	$input_value[4];
				$discount					=	$input_value[5];
				$saved_money				=	$input_value[6];
				$deal_count					=	$input_value[7];
				
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_description"=>$offer_descriptiion, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>1,"offer_pariticular_date_1"=>$offer_pariticular_date_1, "offer_size"=>$deal_count,"offer_status"=>1);
				
			}
				if($offer_timeing	==	3)
				{
				$offer_pariticular_date_1	=	$input_value[1];
				$time						=	$input_value[2].":".$input_value[3];
				$offer_descriptiion			=	$input_value[4];
				$orignal_price				=	$input_value[5];
				$offer_price				=	$input_value[6];
				$discount					=	$input_value[7];
				$saved_money				=	$input_value[8];
				$deal_count					=	$input_value[9];
				
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_description"=>$offer_descriptiion, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>1,"offer_pariticular_date_1"=>$offer_pariticular_date_1,"offer_pariticular_time"=>$time, "offer_size"=>$deal_count,"offer_status"=>1);
				
			}
				if($offer_timeing	==	4)
				{
				$offer_pariticular_date_1	=	$input_value[1];
				$offer_pariticular_date_2	=	$input_value[2];
				$time						=	$input_value[3].":".$input_value[4];
				$offer_descriptiion			=	$input_value[5];
				$orignal_price				=	$input_value[6];
				$offer_price				=	$input_value[7];
				$discount					=	$input_value[8];
				$saved_money				=	$input_value[9];
				$deal_count					=	$input_value[10];
				
				$param = array("offer_mp_id"=>$offer_mp_id, "offer_description"=>$offer_descriptiion, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>1,"offer_pariticular_date_1"=>$offer_pariticular_date_1,"offer_pariticular_date_2"=>$offer_pariticular_date_2,"offer_pariticular_time"=>$time, "offer_size"=>$deal_count,"offer_status"=>1);
				
			}
				$insert = $GLOBALS["db"]->insertNow('offers_temp', $param, 'offer_created_on');
				return $insert;
			}
			else//Show old data and tell him to disbale old deal first.
			{
				//echo 'else';
				$offer_type_name	=	$data[0]['offer_type_id'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				echo '<style>.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {border: 1px solid !important;}</style><div><h2 style="text-align: center;">You have already running Offer</h2><p style="    text-align: center;">If you want to create this Offer, first disbale old Offer</p><div class="table-responsive" style="margin-top:20px;"> <table class="table" style="margin-bottom: 0px;"> <thead> <tr> <th>#</th> <th>Deal Type</th> <th>Deal Descripition</th><th>Status</th></tr></thead> <tbody> <tr> <td>1</td><td>'.$offer_type_name.'</td><td>'.$desc.'</td><td><button id="disable_deal" type="button" class="btn btn-warning" value="'.$data[0]['offer_id'].'">Disable</button></td></tr></tbody> </table> </div></div>';
			}
		
		}
	}
	function disable_deal()
	{
		$deal_id	=	$_POST['deal_id'];
		$condition	=	array("deal_id"=>$deal_id);
		$param		=	array("deal_status"=>0);
		$update 	= 	$GLOBALS["db"]->updateNow('deal_temp', $param, $condition, 'deal_status_time');
		if($update)
		{
			echo "1";
			//-----START#Admin-Notification Table instertion-----//
			$notifiy_array	= array("an_mp_user_id"=>$deal_id,"an_process"=>"deal_status","an_text"=>'deal_status',"an_notify_from"=>"MP");
			$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
			//-----END#Admin-Notification Table instertion-----//
		}
		else
		{
			echo "0";
		}
	}
	function enable_deal()
	{
		
		$deal_id	=	$_POST['deal_id'];
		$condition	=	array("deal_id"=>$deal_id);
		$param		=	array("deal_status"=>1);
		$update 	= 	$GLOBALS["db"]->updateNow('deal_temp', $param, $condition, 'deal_status_time');
		if($update)
		{
			echo "1";
			//-----START#Admin-Notification Table instertion-----//
			$notifiy_array	= array("an_mp_user_id"=>$deal_id,"an_process"=>"deal_status","an_text"=>'deal_status',"an_notify_from"=>"MP");
			$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
			//-----END#Admin-Notification Table instertion-----//
		}
		else
		{
			echo "0";
		}
	}
	function img_order()
	{
		$order			=	$_POST['order'];
		$condition		=	array("mp_gallery_mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$param			=	array("image_priority"=>$order);
		$update 		= 	$GLOBALS["db"]->update('mp_gallery', $param, $condition);
		$update_temp 	= 	$GLOBALS["db"]->update('mp_gallery_temp', $param, $condition);
		if($update_temp	==	"true"){
			echo 'true';
		}else{
			echo 'false';
		}
	}
	function Images_add_from_gallery()
	{
		$param = array();
		$data[1]= $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_status = 1 AND mp_gallery_mp_id = ".$_SESSION['app_user']['mp_details_id']."", $param);
		
		$data[2]= $GLOBALS["db"]->select("SELECT o.offer_image_media FROM offers_gallery_temp o, mp_details_temp m, offers_temp ot WHERE o.offer_id = ot.offer_id AND m.mp_details_id = ot.offer_mp_id AND m.mp_details_id = ".$_SESSION['app_user']['mp_details_id']."", $param);
		
		$div_open	=	'<div id="dragable" style="overflow: scroll;padding: 10px;width: 100%;float: left;overflow-x: hidden;height: 250px;"><ul id="images" class="ui-sortable ui-draggable" style="margin-left: 16px;position: relative;min-height: 200px;    ">';
		
		$div_mid	=	'';
		
		$image_array	=	array();
		
		for($i=0;$i<count($data[1]);$i++)
		{
			array_push($image_array,$data[1][$i]['mp_gallery_image']);
		}
		for($i=0;$i<count($data[2]);$i++)
		{
			array_push($image_array,$data[2][$i]['offer_image_media']);
		}
		
		for($i=0;$i<count(array_unique($image_array));$i++)
		{
			$j	=	$i + 1;
			$div_mid	=	$div_mid. '<li style="width:auto;padding-right: 5px;padding-left: 5px; width:125px; float:left;margin-bottom: 3px;" class="ui-sortable-handle"><img alt="'.$j.'" class="gallery_img" id="'.$image_array[$i].'" style="cursor:pointer;" height="90px" width="120px" src="'. APP_CRM_UPLOADS_PATH.$image_array[$i].'"><div style="position: absolute;top: 70px;right: 6px; display:none;" id="gallery_img_'.$j.'" class="gmi"><img src="'.APP_IMAGES.'right.png">
</div></li>';
		}
		
		$div_close		=	'</ul></div>';
		//$upload_button	=	'<div style="margin-top: 90px;margin-left: 30px;"><form id="gallery_image_form" name="gallery_image_form"><div><img src="http://localhost/st/public/assets/web/images/right.png"><input type="file" id="image1" name="imageMy"></div></form></div>';
		
		$upload_button	=	'<div style="margin-top: 90px;margin-left: 30px;"><form id="gallery_image_form" name="gallery_image_form"><div class="image-upload" style="bottom: -10px;position: absolute;"><div id="upload-file-container"><input type="file" id="image1" name="imageMy"></div><div style="font-weight: 500;font-size: 11px;">You can upload multiple files at once. Use only JPG, GIF or PNG format.</div></div></form></div>';
		
		
		
		//echo $div_open . $div_mid . $div_close . $upload_button;
		
		
		echo '<div style="width: 100%;height: 280px;"><div id="uploading" class="bg" style="text-align: center;float: left;width: 50%;height: 30px;"><span style="top: 7px;position: absolute;cursor: pointer;font-size:15px;margin-left: -32px;">UPLOAD</span><div style="border-bottom: 2px solid #FF2F3C;width: 80px;height: 30px;margin-left: 70px;top: 20px;" id="up_line"></div></div><div id="existing" class="" style="cursor: pointer;float: left;background: #F1F1F1;text-align: center;height: 250px;height: 30px;width: 50%;"><span style="top: 7px;position: absolute;margin-left: -25px;font-size:15px;">EXISTING</span><div style="border-bottom: 2px solid #FF2F3C;width: 80px;height: 30px;margin-left: 80px;top: 20px;display:none;" id="ex_line"></div></div><div id="up" style="width: 100%;float: left;height: 250px;padding: 10px;"><div id="img_show" style="height: 185px;width: 250px;margin: 0 auto;"><img src="" id="uploading_image" style="    position: absolute;"><div id="img_per_bar" style="text-align: center;margin-top: 58px;font-weight: bold;color: red;display:none;border: 1px solid #5ACFB5;width: 50%;position: fixed;"><div id="img_per_bar_v" style="background-color: #5ACFB5;height: 6px;"></div></div><div id="img_per" style="text-align: center;margin-top: 80px;font-weight: bold;color: red;display:none;position: fixed;margin-left: 100px;">100%</div></div>'.$upload_button.'</div><div id="ex" style="padding: 10px;width: 100%;float: left;height: 250px;display:none;">'.$div_open . $div_mid . $div_close.'</div></div>';
		
	}
	/*function image_status()
	{
		$param 		= array('offer_image_status'=>'0');
		$condition 	= array("offer_id"=>$_POST['offer_id']);
		$update 	= $GLOBALS["db"]->update('offers_temp', $param, $condition);
		//-----Admin-Notification-----//
		$notifiy_array	= array("an_mp_user_id"=>$_SESSION['app_user']['mp_details_id'],"an_process"=>"deal_update","an_text"=>'',"an_notify_from"=>"MP");
		$insert_notifiy = $GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
		//-----Admin-Notification-----//
	}*/
	function view_deal_offers()
	{
		$deal_id=	$_POST['deal_id'];
		$data1	=	$GLOBALS["db"]->select("SELECT * FROM `offers_temp` WHERE `offer_deal_id` = '".$deal_id."'");
		$data2	=	$GLOBALS["db"]->select("SELECT deal_name FROM `deal_temp` WHERE `deal_id` = '".$deal_id."'");
		$start	=	'<div style="height: 31px;margin-bottom: 10px;background-color: #ffffff;border-bottom: 1px solid #f6f6f6;border-top: 1px solid #f6f6f6;"><div class="col-md-1" style="text-align: right;font-size: 16px;font-weight: 600;">S.No.</div><div class="col-md-11" style="text-align:center;"><div class="col-md-12" style="text-align: center;font-weight: 600;padding-left: 0px;padding-top: 3px;font-size: 16px;">Offer Name</div></div></div>';
		$end	=	'';
		$middle	=	'';
		
		
		
		for($i=0;$i<count($data1);$i++)
		{
			if ($i % 2 == 0) {
			  $color	=	"height: 50px;background-color: #fff;"; 	
			}else{
			  $color	=	"height: 50px;background-color: #f6f6f6;"; 	
			}
			if($data1[$i]["offer_type_id"]	==	1)
			{
				$text	=	"Flate Offer with ".$data1[$i]["offer_discount"]. "% Discount";
				$o_type	=	"Flat Offer";
			}
			if($data1[$i]["offer_type_id"]	==	2)
			{
				$text	=	$data1[$i]["offer_description"];
				$o_type	=	"Special Offer";
			}
			
			//$middle	.=	'<div style="height: 38px;"><div style="width: 70px;float: left;border: 1px solid;height: 25px;">'.($i + 1).'</div><div style="width: 89%;float: left;border: 1px solid;height: 25px;">'.$text.'</div></div>';
			
			$middle	.=	'<div style="'.$color.'"><div class="col-md-1" style="text-align: right;padding-top: 6px;font-size: 15px;padding-right: 21px;">'.($i + 1).'</div><div class="col-md-11" style="text-align:center;"><div class="col-md-12" style="text-align: left;font-weight: 600;padding-left: 0px;padding-top: 3px;">'.$o_type.'</div><div class="col-md-12" style="text-align: left;padding-left: 0px;padding-top: 3px;">'.$text.'</div></div></div>';
		}
		if(count($data1)	==	0)
		{
			echo $data2[0]["deal_name"].'#$'.'<div style="height: 50px;background-color: #fff;"><div class="col-md-12" style="text-align:center;"><div class="col-md-12" style="padding-left: 0px;padding-top: 3px;font-size: 16px;text-align: center;">Opps no Offer !</div></div></div>';
		}
		else
		{
			echo $data2[0]["deal_name"].'#$'.$start.$middle. $end;
		}
	}
    
	function offer_delete()
	{
		$param 	= 	array("offer_id"=>$_POST['id']);
		$data	 = $GLOBALS["db"]->deleteQuery("offers_temp", $param, $limit = 1);
		return $data;
	}
	
}

?>



 

