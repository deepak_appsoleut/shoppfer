<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class review_mp_model {

	function review_mp()
	{
		$param = array("mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$data[0] = $GLOBALS["db"]->select("SELECT COUNT(mp_review_score) AS good FROM mp_reviews WHERE mp_review_score>3 AND mp_review_mp_details_id =:mp_id", $param);
		$data[1] = $GLOBALS["db"]->select("SELECT COUNT(mp_review_score) AS average FROM mp_reviews WHERE mp_review_score>1 AND mp_review_score < 4 AND mp_review_mp_details_id =:mp_id", $param);
		$data[2] = $GLOBALS["db"]->select("SELECT COUNT(mp_review_score) AS bad FROM mp_reviews WHERE mp_review_score<2 AND mp_review_mp_details_id =:mp_id", $param);
		$data[3] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS m.mp_review_id, c.customer_name, c.customer_id, m.mp_review_modified_on, m.mp_review_score, m.mp_review_comment, r.reply_text, r.reply_mp_review_id, mp.mp_details_name, r.reply_modify_date FROM customer c, mp_reviews m LEFT JOIN reply r ON r.reply_mp_review_id = m.mp_review_id LEFT JOIN mp_details_temp mp ON mp.mp_details_id = r.mp_id WHERE m.mp_review_customer_id = c.customer_id AND mp_review_mp_details_id =:mp_id ORDER BY m.mp_review_created_on DESC LIMIT 0, 10", $param);
		$count = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		$data[4]= array('count'=>$count[0]['count']);
		return $data;
	}
	function pageChange()
	{
		//return json_encode($_POST);
		$param = array("mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$where = "";
		if(isset($_POST['page_number']))
		{
			$limit = $_POST['page_number'];
		}
		else
		{
			$limit = 1;
		}
		if($_POST['sortValue']=='good')
		{
			$where = ' AND m.mp_review_score>3';
		}
		else if($_POST['sortValue']=='poor')
		{
			$where = ' AND m.mp_review_score<2';
		}
		else if($_POST['sortValue']=='avg')
		{
			$where = ' AND m.mp_review_score>1 AND m.mp_review_score < 4';
		}
		$ulimit = $limit * 10;
		$llimit = $ulimit -  10;		
		
		$data[3] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS m.mp_review_id, c.customer_name, c.customer_id, m.mp_review_created_on, m.mp_review_score, m.mp_review_comment, r.reply_text, mp.mp_details_name, r.reply_date FROM customer c, mp_reviews m LEFT JOIN reply r ON r.reply_mp_review_id = m.mp_review_id LEFT JOIN mp_details_temp mp ON mp.mp_details_id = r.mp_id WHERE m.mp_review_customer_id = c.customer_id AND mp_review_mp_details_id =:mp_id ".$where." ORDER BY m.mp_review_created_on DESC LIMIT ".$llimit.", 10", $param);
		$count = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		$data[4]= array('count'=>$count[0]['count']);
		return $data;
	}
	function pageChangeonSort()
	{
		//return json_encode($_POST);
		$param = array("mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$where = "";
		if($_POST['sortValue']=='good')
		{
			$where = ' AND m.mp_review_score>3';
		}
		else if($_POST['sortValue']=='poor')
		{
			$where = ' AND m.mp_review_score<2';
		}
		else if($_POST['sortValue']=='avg')
		{
			$where = ' AND m.mp_review_score>1 AND m.mp_review_score < 4';
		}		
		$data[3] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS m.mp_review_id, c.customer_name, c.customer_id, m.mp_review_created_on, m.mp_review_score, m.mp_review_comment, r.reply_text, mp.mp_details_name, r.reply_date FROM customer c, mp_reviews m LEFT JOIN reply r ON r.reply_mp_review_id = m.mp_review_id LEFT JOIN mp_details_temp mp ON mp.mp_details_id = r.mp_id WHERE m.mp_review_customer_id = c.customer_id AND mp_review_mp_details_id =:mp_id".$where." ORDER BY m.mp_review_created_on DESC LIMIT 0, 10", $param);
		$count = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		$data[4]= array('count'=>$count[0]['count']);
		return $data;
	}
	function replySubmit()
	{
		$param = array('reply_mp_review_id'=>$_POST['removedStr'], 'reply_text'=>$_POST['txt'], 'mp_id'=>$_SESSION['app_user']['mp_details_id'], 'customer_id'=>$_POST['customer_id']);
		$insert =$GLOBALS["db"]->lastInsertNow('reply', $param, 'reply_date');
		return $insert;
	}
	function replyEditSubmit()
	{
		$param = array('reply_text'=>$_POST['txt']);
		$condition = array('reply_mp_review_id'=>$_POST['removedStr']);
		$insert =$GLOBALS["db"]->update('reply', $param,$condition);
		return $insert;
	}
}
?>