<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class redeem_voucher_model {

	function redeem_voucher()
	{
		/*
			//0 means order booked
			//1 means redeemed booking
			//2 menas cancelled booking
		*/
		$param = array("booking_mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$data[0] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS c.customer_name, b.booking_created_on, o.deal_name, b.booking_status  FROM booking b, offers_temp o, mp_details_temp mp, customer c WHERE c.customer_id = b.booking_customer_id AND b.booking_mp_id = mp.mp_details_id AND o.offer_id = b.booking_offer_id AND b.booking_mp_id =:booking_mp_id AND b.booking_status =1 ORDER BY b.booking_created_on DESC LIMIT 0,10", $param);
		$count = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		
		$paramAll = array();
		$data[2] = $GLOBALS["db"]->select("SELECT offer_id as cc FROM `offers_temp` WHERE `offer_mp_id` = ".$_SESSION['app_user']['mp_details_id']."", $paramAll);
		
		$data[1]= array('count'=>$count[0]['count']);
		return $data;		
	}
	function pageChange()
	{
		//return json_encode($_POST);
		$param = array("booking_mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$where = "";
		if(isset($_POST['page_number']))
		{
			$limit = $_POST['page_number'];
		}
		else
		{
			$limit = 1;
		}
		if($_POST['sortValue']=='default')
		{
			$where = '';
		}
		else if($_POST['sortValue']=='not-redeemed')
		{
			$where = ' AND b.booking_status =0';
		}
		else if($_POST['sortValue']=='redeemed')
		{
			$where = ' AND b.booking_status =1';
		}
		else
		{
			$where = ' AND b.booking_status =2';
		}
			
		$ulimit = $limit * 10;
		$llimit = $ulimit -  10;		
		
		$data[0] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS c.customer_name, b.booking_created_on, o.deal_name, b.booking_status  FROM booking b, offers_temp o, mp_details_temp mp, customer c WHERE c.customer_id = b.booking_customer_id AND b.booking_mp_id = mp.mp_details_id AND o.offer_id = b.booking_offer_id AND b.booking_mp_id =:booking_mp_id ".$where." ORDER BY b.booking_created_on DESC LIMIT ".$llimit.", 10", $param);
		$count = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		$data[1]= array('count'=>$count[0]['count']);
		return $data;
	}
	function pageChangeonSort()
	{
		//return json_encode($_POST);
		$param = array("booking_mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$where = "";
		if($_POST['sortValue']=='default')
		{
			$where = '';
		}
		else if($_POST['sortValue']=='not-redeemed')
		{
			$where = ' AND b.booking_status =0';
		}
		else if($_POST['sortValue']=='redeemed')
		{
			$where = ' AND b.booking_status =1';
		}
		else
		{
			$where = ' AND b.booking_status =2';
		}	
		
		$data[0] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS c.customer_name, b.booking_created_on, o.deal_name, b.booking_status  FROM booking b, offers_temp o, mp_details_temp mp, customer c WHERE c.customer_id = b.booking_customer_id AND b.booking_mp_id = mp.mp_details_id AND o.offer_id = b.booking_offer_id AND b.booking_mp_id =:booking_mp_id ".$where." ORDER BY b.booking_created_on DESC LIMIT 0,10", $param);
		$count = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		$data[1]= array('count'=>$count[0]['count']);
		return $data;
	}
	function searchVoucher()
	{
		$param = array("booking_mp_id"=>$_SESSION['app_user']['mp_details_id'], "coupon" =>$_POST['searchValue']);
		$data = $GLOBALS["db"]->select("SELECT c.customer_name, b.booking_id, b.booking_created_on, o.deal_name,o.offer_description,o.offer_discount,o.offers_day_type,o.offers_timeings,o.min_billing, b.booking_status, b.booking_coupon_code FROM booking b, offers_temp o, mp_details_temp mp, customer c WHERE c.customer_id = b.booking_customer_id AND b.booking_mp_id = mp.mp_details_id AND o.offer_id = b.booking_offer_id AND b.booking_mp_id =:booking_mp_id AND b.booking_coupon_code =:coupon", $param);
		return $data;
	}
	function redeemCoupon()
	{
		$condition = array("booking_id"=>$_POST['id']);
		$param = array("booking_status"=>1);
		$data = $GLOBALS["db"]->update("booking", $param, $condition);
		if($data=="true")
		{
			$selectMy = $GLOBALS["db"]->select("SELECT d.device_token FROM device d, booking b, customer c, cust_device_relation cd WHERE b.booking_customer_id = c.customer_id AND d.device_id =cd.device_id AND c.customer_id = cd.cust_id AND b.booking_id =:booking_id LIMIT 1", $condition);
			
			if(count($selectMy)>0)
			{
				$registrationIds = $selectMy[0]['device_token'];			
				// prep the bundle
				$msg = array
				(
					'title'		=> 'Coupon Redeemed',
					'message' 	=> 'Your Coupon Redeemed',
					'code'=>'101'
				);
				$fields = array
				(
					'registration_ids' 	=> $registrationIds,
					'data'				=> $msg
				);
				 
				$headers = array
				(
					'Authorization: key=' . APP_GOOGLE_KEY,
					'Content-Type: application/json'
				);
				
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				curl_close( $ch );
			}
				//echo $result;
		}
		return $data;
	}
}
?>