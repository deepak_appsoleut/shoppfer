<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class feedback_model {

	function feedback()
	{
		/*
			//0 means order booked
			//1 means payment done
			//2 means redeemed booking
			//3 menas cancelled booking
		*/
		$param = array("booking_mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$data = $GLOBALS["db"]->select("SELECT b.*, h.hotel_details_name, mp.mp_details_name, d.deal_name FROM booking b, hotel_details h, mp_details mp, deals d WHERE h.hotel_details_id = b.booking_hotel_id AND mp.mp_details_id = b.booking_mp_id AND d.deal_id= b.booking_deal_id AND b.booking_mp_id =:booking_mp_id", $param);
		return $data;		
	}
}
?>