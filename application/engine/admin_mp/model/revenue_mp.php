<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class revenue_mp_model {

	function revenue_mp()
	{
		$param = array();
		/*$data = $GLOBALS["db"]->select("SELECT b.*, h.hotel_details_name, mp.mp_details_name, t.template_name FROM booking b, hotel_details h, mp_details mp, deals d, template t WHERE h.hotel_details_id = b.booking_hotel_id AND mp.mp_details_id = b.booking_mp_id AND d.deal_id= b.booking_deal_id AND t.template_id = d.deal_template_id AND (b.booking_status = 2 OR (b.booking_date<CURDATE() AND b.booking_status!=3 AND b.booking_status!=0))", $param);
		return $data;	*/	
	}
	function revenue_data()
	{
		$param = array("mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$startDate = date('Y-m-d', strtotime($_POST['startDate']));
		$endDate = date('Y-m-d', strtotime($_POST['endDate']));
		$data = $GLOBALS["db"]->select("SELECT b.*, b.booking_created_on, h.hotel_details_name, mp.mp_details_name, d.deal_name FROM booking b, hotel_details h, mp_details mp, deals d WHERE h.hotel_details_id = b.booking_hotel_id AND mp.mp_details_id = b.booking_mp_id AND d.deal_id= b.booking_deal_id AND b.booking_mp_id =:mp_id AND b.booking_created_on BETWEEN '".$startDate."' AND '".$endDate."' AND (b.booking_status = 2 OR b.booking_status = 1 AND (CURDATE() > DATE_ADD(b.booking_created_on,INTERVAL 7 DAY))) GROUP BY b.booking_id", $param);
		return $data;
	}
}
?>