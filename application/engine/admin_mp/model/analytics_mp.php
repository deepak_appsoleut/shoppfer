<?php
class analytics_mp_model 
{
	function analytics_mp()
	{		
	}
	function analytics_data_mp()
	{
		$param = array("mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$startDate = date('Y-m-d', strtotime($_POST['startDate']));
		$endDate = date('Y-m-d', strtotime($_POST['endDate']));	
		$data[1] = $GLOBALS["db"]->select("SELECT DATE_FORMAT(selected_date, '%b %e') as label, cnt 
FROM(
        SELECT 
        DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) AS selected_date , 
        SUM(b.booking_amount) AS cnt 
        FROM booking b, mp_details m
        WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' AND b.booking_mp_id =:mp_id AND b.booking_mp_id = m.mp_details_id AND (b.booking_status = 1 OR b.booking_status = 2)
        GROUP BY DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')
        UNION 
select selected_date,0 AS cnt from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
where selected_date BETWEEN '".$startDate."' AND '".$endDate."' AND selected_date NOT IN (SELECT DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) FROM booking b WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."')
) AS T1 
ORDER BY selected_date", $param);

		$startDate1 = date_create($startDate);
		$endDate1 = date_create($endDate);
		$intervalStart = date_diff($startDate1, $endDate1);
		$diffStart = $intervalStart->format('%a');
		
		$newStartDate = date_format(date_sub($startDate1, date_interval_create_from_date_string($diffStart.' days')), 'Y-m-d');
		$newEndDate = date_format(date_sub($endDate1, date_interval_create_from_date_string($diffStart.' days')), 'Y-m-d');
				
		$data[2] = $GLOBALS["db"]->select("SELECT DATE_FORMAT(selected_date, '%b %e') as label, cnt 
FROM(
        SELECT 
        DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) AS selected_date , 
        SUM(b.booking_amount) AS cnt 
        FROM booking b, mp_details m
        WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$newStartDate."' AND '".$newEndDate."' AND b.booking_mp_id =:mp_id AND b.booking_mp_id = m.mp_details_id AND (b.booking_status = 1 OR b.booking_status = 2)
        GROUP BY DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')
        UNION 
select selected_date,0 AS cnt from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
where selected_date BETWEEN '".$newStartDate."' AND '".$newEndDate."' AND selected_date NOT IN (SELECT DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) FROM booking b WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$newStartDate."' AND '".$newEndDate."')
) AS T1 
ORDER BY selected_date", $param);
		return $data;
	}
	function analytics_data_area_mp()
	{
		$param = array("mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$startDate = date('Y-m-d', strtotime($_POST['startDate']));
		$endDate = date('Y-m-d', strtotime($_POST['endDate']));	
		
		//$data[0] = $GLOBALS["db"]->select("SELECT DATE_FORMAT(b.booking_created_on, '%b %e') AS label, COUNT(b.booking_id) AS data FROM booking b, mp_details m WHERE b.booking_mp_id = m.mp_details_id AND b.booking_hotel_id =:hotel_id AND b.booking_status =:status AND b.booking_created_on BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY b.booking_created_on", $param);
		$data[0] =  $GLOBALS["db"]->select("SELECT DATE_FORMAT(selected_date, '%b %e') as label, cnt 
FROM(
        SELECT 
        DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) AS selected_date , 
        COUNT(b.booking_id) AS cnt 
        FROM booking b, mp_details m
        WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' AND b.booking_mp_id =:mp_id AND b.booking_mp_id = m.mp_details_id AND (b.booking_status = 1 OR b.booking_status = 2)
        GROUP BY DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')
        UNION 
select selected_date,0 AS cnt from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
where selected_date BETWEEN '".$startDate."' AND '".$endDate."' AND selected_date NOT IN (SELECT DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) FROM booking b WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."')
) AS T1 
ORDER BY selected_date", $param);
		/*if($diffStart=="6"||$diffStart=="7")
		{
			$data[0] = $GLOBALS["db"]->select("SELECT DATE_FORMAT(b.booking_created_on, '%a') AS label, COUNT(b.booking_id) AS data FROM booking b, mp_details m WHERE b.booking_mp_id = m.mp_details_id AND b.booking_hotel_id =:hotel_id AND b.booking_status =1 AND b.booking_created_on BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY label ORDER BY b.booking_created_on ASC", $param);
		}
		if($diffStart=="90"||$diffStart=="91")
		{
			$data[0] = $GLOBALS["db"]->select("SELECT DATE_FORMAT(b.booking_created_on, '%b') AS label, COUNT(b.booking_id) AS data FROM booking b, mp_details m WHERE b.booking_mp_id = m.mp_details_id AND b.booking_hotel_id =:hotel_id AND b.booking_status =1 AND b.booking_created_on BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY label ORDER BY b.booking_created_on ASC", $param);
		}
		if(($diffStart>300)&&($diffStart<365))
		{
			$data[0] = $GLOBALS["db"]->select("SELECT DATE_FORMAT(b.booking_created_on, '%b') AS label, COUNT(b.booking_id) AS data FROM booking b, mp_details m WHERE b.booking_mp_id = m.mp_details_id AND b.booking_hotel_id =1 AND b.booking_status =1 AND b.booking_created_on BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY label ORDER BY b.booking_created_on ASC", $param);
		}	*/	
		//$data[1] = $startDate;
		
		return $data;
	}
	function analytics_data_dashboard_mp()
	{
		$param 		= 	array("mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$arr		=	array();
		$date_array	=	array();
		if($_POST["endDate"]  ==	"lastdate")	//Single days
		{
			$startDate 			= 	date('Y-m-d', strtotime($_POST['startDate']));
			$last_weak_query_1	=	"WHERE DATE_FORMAT(booking_created_on, '%Y-%m-%d') = '".$startDate."' AND booking_mp_id='".$_SESSION['app_user']['mp_details_id']."'";
			$last_weak_query_2	= 	"WHERE mp_review_mp_details_id='".$_SESSION['app_user']['mp_details_id']."'";
			$last_weak_query_3	= 	"WHERE c.customer_id=b.booking_customer_id AND b.booking_mp_id = m.mp_details_id AND b.booking_mp_id = ".$_SESSION['app_user']['mp_details_id'];
			$last_weak_query_4	= "where booking_mp_id = '".$_SESSION['app_user']['mp_details_id']."' AND (booking_status =0 OR booking_status =1) GROUP BY booking_partial_address,booking_location ORDER by address_cnt DESC";
			
			$datediff	=	7;
			//------------------------------------------------------
			//Start Sales report chart For Today/Yesterday/One weak
			//------------------------------------------------------
			for ($j=0; $j<7; $j++)
			{
				$startdate		=	date("Y-m-d", strtotime($j." days ago"));
				$startdate1		=	date("M d", strtotime($j." days ago"));
				$data_wise_data	=	$GLOBALS["db"]->select("SELECT booking_status,DATE_FORMAT(booking_created_on, '%Y-%m-%d') FROM `booking` where DATE_FORMAT(booking_created_on, '%Y-%m-%d') = '".$startdate."' AND booking_mp_id = '".$_SESSION['app_user']['mp_details_id']."'");
				array_push($date_array,$startdate1);
				if(count($data_wise_data)>0)
				{
					for($x=0;$x<count($data_wise_data);$x++)
					{
						//$d	=	$j.'*'.$data_wise_data[$x]['booking_status'].'#'.$startdate1;
						$d	=	$data_wise_data[$x]['booking_status'].'#'.$startdate1;
						array_push($arr,$d);
					}
				}
				else
				{
					//$d	=	$j.'*'.'Null'.'#'.$startdate1;
					$d	=	'Null#'.$startdate1;
					array_push($arr,$d);
				}
			}	
			//------------------------------------------------------
			//Finish Sales reprot Chart For Today/Yesterday/One weak
			//------------------------------------------------------
			
			
		}
		else									//Double Days
		{
			$startDate 	= 	date('Y-m-d', strtotime($_POST['startDate']));
			$endDate 	= 	date('Y-m-d', strtotime($_POST['endDate']));	
			//Last WEak days
			$last_weak_query_1	=	"WHERE DATE_FORMAT(booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' AND booking_mp_id='".$_SESSION['app_user']['mp_details_id']."'";
			$last_weak_query_2	= "WHERE mp_review_mp_details_id='".$_SESSION['app_user']['mp_details_id']."'";
			$last_weak_query_3	= "WHERE c.customer_id=b.booking_customer_id AND b.booking_mp_id = m.mp_details_id AND b.booking_mp_id = '".$_SESSION['app_user']['mp_details_id']."'";
			$last_weak_query_4	= "WHERE booking_mp_id = '".$_SESSION['app_user']['mp_details_id']."' AND (booking_status =0 OR booking_status =1)  GROUP BY booking_partial_address,booking_location ORDER by address_cnt DESC";
			
			
			
			//------------------------------------------------------
			//Start Sales report chart For Today/Yesterday/One weak
			//------------------------------------------------------
			$date1		=	date_create($startDate);
			$date2		=	date_create($endDate);
			$diff		=	date_diff($date1,$date2);
			$datediff	=	($diff->format("%a")+1);
			
			
			
			for ($j=1; $j<=$datediff; $j++)
			{
				$startdate		=	date("Y-m-d", strtotime($j." days ago"));
				//array_push($arr,$startdate);
				$startdate1		=	date("M d", strtotime($j." days ago"));
				$data_wise_data	=	$GLOBALS["db"]->select("SELECT booking_status,DATE_FORMAT(booking_created_on, '%Y-%m-%d') FROM `booking` where DATE_FORMAT(booking_created_on, '%Y-%m-%d') = '".$startdate."' AND booking_mp_id = '".$_SESSION['app_user']['mp_details_id']."'");
				array_push($date_array,$startdate1);
				if(count($data_wise_data)>0)
				{
					for($x=0;$x<count($data_wise_data);$x++)
					{
						$d	=	$data_wise_data[$x]['booking_status'].'#'.$startdate1;
						array_push($arr,$d);
					}
				}
				else
				{
					$d	=	'Null#'.$startdate1;
					array_push($arr,$d);
				}
			}	
			//------------------------------------------------------
			//Finish Sales reprot Chart For Today/Yesterday/One weak
			//------------------------------------------------------
			
			
			
			
		}
		//echo "SELECT * FROM booking ".$last_weak_query_1;
		//echo "</br>";
		//echo "SELECT ROUND(AVG(mp_review_score), 1) as avg_cnt FROM mp_reviews ".$last_weak_query_2;
		//echo "</br>";
	//echo "SELECT ROUND(DATEDIFF(NOW(), customer_dob) / 365.25) as age , customer_gender FROM customer c, mp_details_temp m, booking b ".$last_weak_query_3;
		//echo "</br>";
		//echo "SELECT `booking_partial_address`, booking_location, count(*) as address_cnt FROM booking ".$last_weak_query_4;
		$data[0]	=	$GLOBALS["db"]->select("SELECT * FROM booking ".$last_weak_query_1);
		$data[1]	=	$GLOBALS["db"]->select("SELECT ROUND(AVG(mp_review_score), 1) as avg_cnt FROM mp_reviews ".$last_weak_query_2);
		$data[2]	=	$GLOBALS["db"]->select("SELECT ROUND(DATEDIFF(NOW(), customer_dob) / 365.25) as age , customer_gender FROM customer c, mp_details_temp m, booking b ".$last_weak_query_3);							
		$data[3]	=	$GLOBALS["db"]->select("SELECT `booking_partial_address`, booking_location, count(*) as address_cnt FROM booking ".$last_weak_query_4);
		
		$data[4]	=	$arr;
		$data[5]	=	$datediff;
		return $data;
		
		
									
		
		
		
		//return $data;
		//SELECT * FROM `booking` WHERE DATE_FORMAT(booking_created_on, '%Y-%m-%d') BETWEEN '2016-05-19' AND '2016-05-24'
		/*$data[0] =  $GLOBALS["db"]->select("SELECT DATE_FORMAT(selected_date, '%b %e') as label, cnt 
		FROM(
        SELECT 
        DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) AS selected_date , 
        COUNT(b.booking_id) AS cnt 
        FROM booking b, mp_details m
        WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' AND b.booking_mp_id = m.mp_details_id AND b.booking_mp_id =:mp_id AND (b.booking_status = 1 OR b.booking_status = 2)
        GROUP BY DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')
        UNION 
select selected_date,0 AS cnt from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
where selected_date BETWEEN '".$startDate."' AND '".$endDate."' AND selected_date NOT IN (SELECT DISTINCT(DATE_FORMAT(b.booking_created_on, '%Y-%m-%d')) FROM booking b WHERE DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."')
) AS T1 
ORDER BY selected_date", $param);*/

		

	
		//$newStartDate = date('Y-m-01',strtotime('last month'));
		//$newEndDate = date('Y-m-t',strtotime('last month'));
		//count of Deal within a date range
 		//$data[1] = $GLOBALS["db"]->select("SELECT COUNT(booking_id) as prevCont, SUM(booking_amount) as amount FROM booking WHERE booking_mp_id =:mp_id AND (booking_status = 1 OR booking_status = 2) AND DATE_FORMAT(booking_created_on, '%Y-%m-%d') BETWEEN '".$newStartDate."' AND '".$newEndDate."'", $param);
		
		//total booking amount withing a date range		
		//$data[2] = $GLOBALS["db"]->select("SELECT SUM(booking_amount) as amount FROM booking WHERE booking_mp_id =:mp_id AND (booking_status = 1 OR booking_status = 2) AND DATE_FORMAT(booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'", $param);
		
		//total booking amount
		// booking status = 1 means payment received and 2 means booking also redeemed
		//$data[3] = $GLOBALS["db"]->select("SELECT SUM(booking_amount) as amount FROM booking WHERE booking_mp_id =:mp_id AND (booking_status = 1 OR booking_status = 2)", $param);
		
		//count of network operators
		//$data[4] = $GLOBALS["db"]->select("SELECT COUNT(hr.hotel_mp_relation_id) as cnt FROM hotel_mp_relation hr WHERE hr.hotel_mp_relation_mp_id =:mp_id", $param);	
		
		//count of endorsements
		//$data[5] = $GLOBALS["db"]->select("SELECT COUNT(hr.hotel_mp_relation_id) as cnt FROM hotel_mp_relation hr WHERE hr.hotel_mp_relation_recommend = 1 AND hr.hotel_mp_relation_mp_id =:mp_id", $param);
		
		
		//trending partners
		//$data[6] = $GLOBALS["db"]->select("SELECT h.hotel_details_name, h.hotel_details_city, SUM(b.booking_amount) as amount FROM booking b, mp_details mp, hotel_details h WHERE b.booking_mp_id =:mp_id AND (b.booking_status = 1 OR b.booking_status = 2) AND b.booking_mp_id = mp.mp_details_id AND h.hotel_details_id = b.booking_hotel_id AND DATE_FORMAT(booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY h.hotel_details_id ORDER BY amount DESC", $param);
		
		//count of happy customers
		//$data[7] = $GLOBALS["db"]->select("SELECT COUNT(review_id) as happy FROM reviews WHERE review_mp_details_id =:mp_id HAVING AVG(review_score)>=8", $param);
		
		//average score 
		//$data[8] = $GLOBALS["db"]->select("SELECT AVG(review_score) as review FROM reviews WHERE review_mp_details_id =:mp_id", $param);
		
	}
	function popup_window()
	{
		$order			=	array("mp_popup"=>1);
		$condition		=	array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		$update_temp 	= 	$GLOBALS["db"]->update('mp_details_temp', $order, $condition);
		if($update_temp	==	"true"){
			echo 'true';
			foreach($order as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
		}else{
			echo 'false';
		}
	}
	
}
?>