<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class deals_model 
{
	function deals()
	{
		$param = array("id"=>$_SESSION['app_user']['mp_details_id']);
		//$data = $GLOBALS["db"]->select("SELECT * FROM offers_temp where offer_mp_id =:id and offer_status =1 UNION SELECT * FROM offers_temp where offer_mp_id =:id and offer_status =0", $param);
		//$data = $GLOBALS["db"]->select("SELECT ot.*, o.offer_id as off_id FROM offers_temp ot LEFT JOIN offers o ON ot.offer_id = o.offer_id where ot.offer_mp_id =:id and (ot.offer_status =1 OR ot.offer_status =0) ORDER BY ot.offer_status DESC", $param);
		$data	=	$GLOBALS["db"]->select("SELECT * FROM `deal_temp` WHERE `deal_mp_id` = :id and deal_status=1 UNION SELECT * FROM deal_temp where deal_mp_id=:id and deal_status=0",$param);
		
		return $data;		
	}
	function edit_deal($id)
	{
		$param = array();
		$paramAll 	= array("id"=>$id);
		$data[1]	= $GLOBALS["db"]->select("SELECT * FROM deal_temp WHERE deal_id =:id", $paramAll);
		$data[2]	= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_deal_id =:id", $paramAll);
		$data[3]	= $GLOBALS["db"]->select("SELECT * FROM deal_gallery_temp WHERE deal_id =:id", $paramAll);
		
		return $data;
	}
	function updateDeal()
	{	
		$img_array	=	array();
		if($_POST['image_value_1']	!= ''){
			array_push($img_array,1);
		}
		if($_POST['image_value_2']	!= ''){
			array_push($img_array,2);
		}
		if($_POST['image_value_3']	!= ''){
			array_push($img_array,3);
		}
		//print_r($img_array);
		$media_path_image			=	array();
		$thumb_path_image			=	array();
		$condition 					=	array('deal_id'=>$_POST['dealID']);
		for($i=0;$i<count($img_array);$i++)
		{
			$image			=	"image".$img_array[$i];
			$newNamePrefix 	= time() . '_';
			$manipulator 	= new imageManipulator($_FILES[$image]['tmp_name']);
			$imageName 		=  'media/'.$newNamePrefix . $_FILES[$image]['name'];
			$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES[$image]['name']);
			$tm 			= $manipulator->resample(190, 140, true);
			$save			= $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES[$image]['name']);
			$thumb 			= 'thumb/'.$newNamePrefix . $_FILES[$image]['name'];
			$response['imageName1'] = $imageName;
			$response['imageName2'] = $thumb;
			//array_push($media_path_image,$response['imageName1']);	
			//array_push($thumb_path_image,$response['imageName2']);
			$param	=	array("deal_gallery_image_1"=>$response['imageName1'],"deal_gallery_thumb_1"=>$response['imageName2']);
			$data = $GLOBALS["db"]->update('deals_temp', $param, $condition);

			//deal_gallery_image_1
			//deal_gallery_thumb_1
		}
		//print_r($media_path_image);
		//print_r($thumb_path_image);
		
		$param 		= array('deal_name'=>$_POST['deal_name'],'deal_description'=>$_POST['dealText']);
		$data = $GLOBALS["db"]->update('deals_temp', $param, $condition);
		return $data;
	} 
	function updateOffer()
	{
		$param 		= array('offer_description'=>$_POST['offer_description'],'offer_price'=>$_POST['offer_price'],'offer_discount'=>$_POST['offer_discount'],'offer_save_price'=>$_POST['offer_save_price'],'offer_selling_price'=>$_POST['offer_selling_price'],'offer_type_id'=>$_POST['offer_type_id'],'offer_size'=>$_POST['offer_size']);
		$condition 	= array('offer_id'=>$_POST['offer_id'],'offer_deal_id'=>$_POST['dealID']);
		$data = $GLOBALS["db"]->update('offers_temp', $param, $condition);
		return $data;
	} 
	function adminotify_dealEdit()
	{
		$flag	=	$_POST["flag"];
		
		if($flag	==	"deal_edit")
		{
			$dealID				=	$_POST["dealID"];
			$str				=	$_POST["change_field"];
			$an_text			=	$str;
			$notifiy_array		= 	array("an_mp_user_id"=>$dealID,"an_process"=>"deal_update","an_text"=>$an_text,"an_notify_from"=>"MP");
			$insert_notifiy 	= 	$GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
			return $insert_notifiy;
		}
		else
		{
			$offerID				=	$_POST["offerID"];
			$str					=	$_POST["change_field"];
			$an_text				=	$str;
			$notifiy_array			= 	array("an_mp_user_id"=>$offerID,"an_process"=>"offer_update","an_text"=>$an_text,"an_notify_from"=>"MP");
			$insert_notifiy 		= 	$GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
			return $insert_notifiy;
		}
	
	} 
	function delete_deals()
	{
		$param = array("deal_id"=>$_POST['deal_id']);
		$data_1 = $GLOBALS["db"]->deleteQuery("deal_temp", $param, $limit = 1);
		
		
		
		//-----START#Admin-Notification Table instertion-----//
		$notifiy_array	= array("an_mp_user_id"=>$_POST['deal_id'],"an_process"=>"delete_deal","an_text"=>'',"an_notify_from"=>"MP");
		$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
		//-----END#Admin-Notification Table instertion-----//
		return $data_1;
	}
	function active_inactive()
	{
		$status			=	'active';
		
		$param 			= 	array("id"=>$_SESSION['app_user']['mp_details_id']);
		
		$data 			= 	$GLOBALS["db"]->select("SELECT * FROM deal_temp WHERE deal_mp_id =:id AND deal_status = 1", $param);
		
		
		if(count($data) == 0)
		{
			$deal_id	=	$_POST['deal_id'];
			$condition	=	array("deal_id"=>$deal_id);
			$param		=	array("deal_status"=>1);
			$update = $GLOBALS["db"]->updateNow('deal_temp', $param, $condition, "deal_status_time");
			if($update)
			{
				echo "1";
				//-----START#Admin-Notification Table instertion-----//
				$notifiy_array	= array("an_mp_user_id"=>$deal_id,"an_process"=>"deal_status","an_text"=>'deal_status',"an_notify_from"=>"MP");
				$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
				//-----END#Admin-Notification Table instertion-----//
			}
			else
			{
				echo "0";
			}
		}
		else
		{
			$offer_name			=	$data[0]['deal_name'];
			if($data[0]['deal_type']	==	1)
			{
				//$desc	=	"Normal deal";
			}else{
				//$desc	=	"Shoutout deal";
			}
			//echo '<style>.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {}</style><div><h2 style="text-align: center;">You have already running Offer</h2><p style="    text-align: center;">If you want to Active this Offer, first disable old Offer</p><div class="table-responsive" style="margin-top:20px;"> <table class="table" style="margin-bottom: 0px;"> <thead> <tr> <th>#</th> <th>Deal Type</th> <th>Deal Descripition</th><th>Status</th></tr></thead> <tbody> <tr> <td>1</td><td>'.$offer_type_name.'</td><td>'.$desc.'</td><td><button id="active_inavtive_deal" type="button" class="btn btn-warning" value="'.$data[0]['offer_id'].'-'.$status.'">Inactive</button></td></tr></tbody> </table> </div></div>';
			echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' deal is currently running!</h2><p style="text-align: center;"></p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new deal,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running deal!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs popupcancel" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="active_inavtive_deal" value="'.$data[0]['deal_id'].'-'.$status.'">DISABLE</button></div></div>';
			
		}
	}
	
}
?>