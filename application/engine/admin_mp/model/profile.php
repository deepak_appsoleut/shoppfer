<?php

class profile_model {
	
	function profile()
	{
		$param = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		$paramStatus = array("status"=>1);
		$data[0] = $GLOBALS["db"]->select("SELECT country_id, country_name FROM country WHERE country_status =:status", $paramStatus);
		$data[1] = $GLOBALS["db"]->select("SELECT category_id, category_name FROM category WHERE category_status =:status", $paramStatus);
		$data[2] = $GLOBALS["db"]->select("SELECT sub_category_id, sub_category_name FROM sub_category WHERE sub_category_status =:status AND sub_category_cat_id = ".$_SESSION['app_user']['mp_details_category'], $paramStatus);
		$param = array("id"=>$_SESSION['app_user']['mp_details_id'], "status"=>1);
		$data[3] = $GLOBALS["db"]->select("SELECT mp_gallery_id, mp_gallery_name, mp_gallery_image, mp_gallery_thumb, mp_gallery_actual_image FROM mp_gallery_temp WHERE mp_gallery_mp_id =:id AND mp_gallery_status =:status", $param);
		
		$data[4] = $GLOBALS["db"]->select("SELECT state_id, state_name FROM state WHERE state_status =:status AND state_country_id =".$_SESSION['app_user']['mp_details_country'], $paramStatus);
		return $data;
	}
	function step1()
	{
		// We define our address
		/*if(trim($_POST['address1'])!="")
		{
			$address = $_POST['address'].','.$_POST['address1'].','.$_POST['city'].','.$_POST['stateName'];
		}
		else
		{
			$address = $_POST['address'].','.$_POST['city'].','.$_POST['stateName'];
		}*/
		
		/*$address = $_POST['address'].','.$_POST['city'];
		$address = str_replace(' ', '%20', $address);
		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&sensor=false');
		$geo = json_decode($geo, true);
		if ($geo['status'] = 'OK') {
		  if(isset($geo['results'][0]['geometry']['location']['lat'])){
		  	$latitude = $geo['results'][0]['geometry']['location']['lat'];
		  }else{
			  $latitude = NULL;
		  }
		  if(isset($geo['results'][0]['geometry']['location']['lng'])){
		  	$longitude = $geo['results'][0]['geometry']['location']['lng'];
		  }else{
		  	$longitude = NULL;
		  }		  
		}
		else
		{
			if($_POST['city']=="New Delhi")
			{
				$latitude = '28.6314155';
				$longitude = '77.224758';
			}
			else
			{
				$latitude = '28.4662243';
				$longitude = '77.0307778';
			}

		}*/
		$latitude 	= $_POST["lat"];
		$longitude 	= $_POST["lng"];
		
		$param = array("mp_details_name"=>$_POST['business'],"mp_details_contact"=>$_POST['business_contact'],"mp_details_city"=>$_POST['city'], "mp_details_address"=>$_POST['address'],"mp_details_website"=>$_POST['web_url'],"mp_details_person"=>$_POST['person'], "mp_details_phone"=>$_POST['contact'], "mp_details_designation"=>$_POST['designation'], "mp_details_step"=>1, "mp_details_latitude"=>$latitude, "mp_details_longitude"=>$longitude, "mp_details_modified_by"=>$_SESSION['app_user']['mp_details_id']);
		
		
		$condition = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		//print_r($param);
		$result 	 = $GLOBALS["db"]->update('mp_details', $param, $condition);
		
		$result_1 	 = $GLOBALS["db"]->update('mp_details_temp', $param, $condition);
		//-----Admin-Notification-----//
		/*$result_temp = $GLOBALS["db"]->update('mp_details_temp', $param, $condition);*/
		//-----Admin-Notification-----//

		if($result_1=="true")
		{
			foreach($param as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
		}
		return $result;
	}
	function uploadPics()
	{
		
		$image	=	$_POST["image"];
		
		
		if($_POST[$image.'Id']=="")
		{
			//insertion will be there		
			$newNamePrefix = time() . '_';
			$manipulator = new imageManipulator($_FILES[$image]['tmp_name']);
			$imageName =  'media/'.$newNamePrefix . $_FILES[$image]['name'];
			$save = $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES[$image]['name']);
			$tm = $manipulator->resample(190, 140, true);
			$save = $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES[$image]['name']);
			$thumb = 'thumb/'.$newNamePrefix . $_FILES[$image]['name'];
			//Uploads image is done  here
			/*$centreX = round($width / 2);
			$centreY = round($height / 2);
			// our dimensions will be 200x130
			$x1 = $centreX - 400; // 200 / 2
			$y1 = $centreY - 250; // 130 / 2
	 
			$x2 = $centreX + 400; // 200 / 2
			$y2 = $centreY + 250; // 130 / 2
	 
			// center cropping to 200x130
			$newImage = $manipulator->crop($x1, $y1, $x2, $y2);
			// saving file to uploads folder
			*/
			//$param = array('gallery_mp_details_id'=>$_SESSION['app_user']['mp_details_id'], 'gallery_name'=>$_POST[$image.'Title'], 'gallery_image'=>$imageName, 'gallery_thumb'=>$thumb, 'gallery_created_by'=>$_SESSION['app_user']['user_id'], 'gallery_modified_by'=>$_SESSION['app_user']['user_id']);
			$param = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_modified_by'=>$_SESSION['app_user']['mp_details_id']);
			//print_r($param);
			//$insert = $GLOBALS["db"]->lastInsertNow('mp_gallery', $param, 'mp_gallery_created_on');
	
			$insert_temp 	= $GLOBALS["db"]->lastInsertNow('mp_gallery_temp', $param, 'mp_gallery_created_on');
	
			$param1 = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_id'=>$insert_temp, 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_modified_by'=>$_SESSION['app_user']['mp_details_id']);
			
			$insert 		= $GLOBALS["db"]->lastInsertNow('mp_gallery', $param1, 'mp_gallery_created_on');
			if($insert_temp>0)
			{
				$response['imageName'] = $thumb;
				$response['id'] = $insert_temp;
			}
			return json_encode($response);	
		}
		else
		{
			//updation will be there
			$newNamePrefix = time() . '_';
			$manipulator = new imageManipulator($_FILES[$image]['tmp_name']);
			$imageName =  'media/'.$newNamePrefix . $_FILES[$image]['name'];
			$save = $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES[$image]['name']);
			$tm = $manipulator->resample(190, 140, true);
			$save = $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES[$image]['name']);
			$thumb = 'thumb/'.$newNamePrefix . $_FILES[$image]['name'];
			// saving file to uploads folder
			$param = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_modified_on'=>$_SESSION['app_user']['mp_details_id']);
						
			/*
			$condition = array('mp_gallery_id'=>$_POST[$image."Id"]);
			
			$data = $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_id =:mp_gallery_id", $condition);
			
			if(isset($data[0]['mp_gallery_actual_image'])!=NULL)
			{
				if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_gallery_image']))
				{
					unlink(APP_CRM_UPLOADS.$data[0]['mp_gallery_image']);
				}
				
				if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_gallery_thumb']))
				{
					unlink(APP_CRM_UPLOADS.$data[0]['mp_gallery_thumb']);
				}
			}*/
			
			
			
			$update = $GLOBALS["db"]->update('mp_gallery_temp', $param, $condition);			
			$update_2 = $GLOBALS["db"]->update('mp_gallery', $param, $condition);
			
			
			if($update=="true")
			{
				if(isset($_POST[$image.'Url'])){
				$_POST[$image.'Url'] = substr($_POST[$image.'Url'], 5);
				unlink(APP_CRM_UPLOADS.'media/'.$_POST[$image.'Url']);
				unlink(APP_CRM_UPLOADS.'thumb/'.$_POST[$image.'Url']);
				}
				$response['imageName'] = $imageName;
				$response['id'] = $_POST[$image.'Id'];
			}
			
			return json_encode($response);			
		
		}
	}
	function uploadpics_edit()
	{
		$image = $_POST['image'];
		$plus	=	explode("_",$image);
		if(isset($plus[1])	==	'plus')
		{
			$image				=	$plus[0];
			$param 				= 	array("mp_gallery_mp_id"=>$_SESSION['app_user']['mp_details_id']);
			$data_value[0] 		= 	$GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_mp_id =:mp_gallery_mp_id AND mp_gallery_image='plus.jpg'", $param);
			if(count($data_value[0])	==	0)
			{
				//This for image5
				return "this for image5";
			}
			else
			{
		    	//return "this for delte 4 images";
				//not image5 Here
				$g_id	=	$data_value[0][0]['mp_gallery_id'];
				return 'xx'.$g_id;
			}
			
			
			
			
				
				//updation will be there
				/*$newNamePrefix 	= time() . '_';
				$manipulator 	= new imageManipulator($_FILES[$image]['tmp_name']);
				$imageName 		= 'media/'.$newNamePrefix . $_FILES[$image]['name'];
				$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES[$image]['name']);
				$tm 			= $manipulator->resample(190, 140, true);
				$save 			= $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES[$image]['name']);
				$thumb 			= 'thumb/'.$newNamePrefix . $_FILES[$image]['name'];
				// saving file to uploads folder
				$param = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_actual_image'=>NULL, 'mp_gallery_modified_on'=>$_SESSION['app_user']['mp_details_id']);
				
				$condition = array('mp_gallery_id'=>$_POST[$image."Id"]);*/
				
				
				
				//$data = $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_id =:mp_gallery_id", $condition);
				
				/*if(isset($data[0]['mp_gallery_actual_image'])!=NULL)
				{
					if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']))
					{
						unlink(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']);
					}
				}*/
				
				
				//print_r($condition);
				//die;	
				
				//$update = $GLOBALS["db"]->update('mp_gallery_temp', $param, $condition);
				
				
				
				
				
				/*if($update=="true")
				{
					
					$response['imageName'] = $imageName;
					$response['id'] = $_POST[$image.'Id'];
				}
				
				return json_encode($response);	*/
		}
		else
		{
			
			//updation will be there
		$newNamePrefix 	= time() . '_';
		$manipulator 	= new imageManipulator($_FILES[$image]['tmp_name']);
		$imageName 		= 'media/'.$newNamePrefix . $_FILES[$image]['name'];
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES[$image]['name']);
		$tm 			= $manipulator->resample(190, 140, true);
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES[$image]['name']);
		$thumb 			= 'thumb/'.$newNamePrefix . $_FILES[$image]['name'];
		// saving file to uploads folder
		$param = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_actual_image'=>NULL, 'mp_gallery_modified_on'=>$_SESSION['app_user']['mp_details_id']);
		
		$condition = array('mp_gallery_id'=>$_POST[$image."Id"]);
		
		
		
		//$data = $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_id =:mp_gallery_id", $condition);
		
		/*if(isset($data[0]['mp_gallery_actual_image'])!=NULL)
		{
			if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']))
			{
				unlink(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']);
			}
		}*/
		
		
		//print_r($condition);
		//die;	
		
		$update = $GLOBALS["db"]->update('mp_gallery_temp', $param, $condition);
		
		
		
		
		
		if($update=="true")
		{
			/*if(isset($_POST[$image.'Url'])){
			$_POST[$image.'Url'] = substr($_POST[$image.'Url'], 5);
			unlink(APP_CRM_UPLOADS.'media/'.$_POST[$image.'Url']);
			unlink(APP_CRM_UPLOADS.'thumb/'.$_POST[$image.'Url']);
			}*/
			$response['imageName'] = $imageName;
			$response['id'] = $_POST[$image.'Id'];
		}
		
		return json_encode($response);	
		}
	
				
		
		
	}
	/*
	function uploadLogo()
	{
		//insertion will be there
		$selectArray = array('mp_details_id'=>$_SESSION['app_user']['mp_details_id']);
		$data = $GLOBALS["db"]->select("SELECT mp_details_logo, mp_details_logo_thumb FROM mp_details WHERE mp_details_id =:mp_details_id", $selectArray);
		if($data[0]['mp_details_logo']!=NULL || $data[0]['mp_details_logo_thumb']!=NULL)
		{
			if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_logo']))
			{
				unlink(APP_CRM_UPLOADS.$data[0]['mp_details_logo']);
			}
			if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_logo_thumb']))
			{
				unlink(APP_CRM_UPLOADS.$data[0]['mp_details_logo_thumb']);
			}
		}
		$newNamePrefix = time() . '_';
		$manipulator = new imageManipulator($_FILES['file']['tmp_name']);
		$imageName =  'media/'.$newNamePrefix . $_FILES['file']['name'];
		$save = $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES['file']['name']);
		$tm = $manipulator->resample(190, 140, true);
		$save = $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES['file']['name']);
		$thumb = 'thumb/'.$newNamePrefix . $_FILES['file']['name'];
		$param = array('mp_details_logo'=>$imageName, 'mp_details_logo_thumb'=>$thumb, 'mp_details_modified_by'=>$_SESSION['app_user']['user_id']);
		$condition = array('mp_details_id'=>$_SESSION['app_user']['mp_details_id']);
		$update = $GLOBALS["db"]->update('mp_details', $param, $condition);
		
		foreach($param as $k=>$v)
		{
			$_SESSION['app_user'][$k]= $v;
		}
		if($update == "true")
		{
			 return $thumb;	
		}
	}
	function uploadPersonPic()
	{
		//insertion will be there
		$selectArray = array('mp_details_id'=>$_SESSION['app_user']['mp_details_id']);
		$data = $GLOBALS["db"]->select("SELECT mp_details_person_pic, mp_details_person_thumb FROM mp_details WHERE mp_details_id =:mp_details_id", $selectArray);
		if($data[0]['mp_details_person_pic']!=NULL || $data[0]['mp_details_person_thumb']!=NULL)
		{
			if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_person_pic']))
			{
				unlink(APP_CRM_UPLOADS.$data[0]['mp_details_person_pic']);
			}
			if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_person_thumb']))
			{
				unlink(APP_CRM_UPLOADS.$data[0]['mp_details_person_thumb']);
			}
		}
		$newNamePrefix = time() . '_';
		$manipulator = new imageManipulator($_FILES['personImg']['tmp_name']);
		$imageName =  'media/'.$newNamePrefix . $_FILES['personImg']['name'];
		$save = $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES['personImg']['name']);
		$tm = $manipulator->resample(190, 140, true);
		$save = $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES['personImg']['name']);
		$thumb = 'thumb/'.$newNamePrefix . $_FILES['personImg']['name'];
		$param = array('mp_details_person_pic'=>$imageName, 'mp_details_person_thumb'=>$thumb, 'mp_details_modified_by'=>$_SESSION['app_user']['user_id']);
		$condition = array('mp_details_id'=>$_SESSION['app_user']['mp_details_id']);
		$update = $GLOBALS["db"]->update('mp_details', $param, $condition);
		
		foreach($param as $k=>$v)
		{
			$_SESSION['app_user'][$k]= $v;
		}
		if($update == "true")
		{
			 return $thumb;	
		}
	}*/
	function step2()
	{
		$imagecountvalues	=	$_POST['imagecountvalues'];
		
		if(!empty($_POST['recommend']))
		{
			$_POST['recommend']= implode(',', $_POST['recommend']);
		}
		else
		{
			$_POST['recommend'] = NULL;
		}
		if($imagecountvalues > 0){
			for($i=0; $i<$imagecountvalues;$i++)
			{
				$j=$i+1;
				$media	=	'image'.$j.'Url';
				$thumb	=	str_replace("media","thumb",$_POST[$media]);
				
				$param = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$_POST[$media], 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_modified_by'=>$_SESSION['app_user']['mp_details_id']);
				$insert_temp 	= $GLOBALS["db"]->lastInsertNow('mp_gallery_temp', $param, 'mp_gallery_created_on');
				
				$param1 = array('mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_id'=>$insert_temp, 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$_POST[$media], 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_modified_by'=>$_SESSION['app_user']['mp_details_id']);
				
				$insert		=	$GLOBALS["db"]->lastInsertNow('mp_gallery', $param1, 'mp_gallery_created_on');
			}
		}
		
		$param = array("mp_details_category"=>$_POST['category'],"mp_details_description"=>$_POST['description'], "mp_details_recommend"=>$_POST['recommend'], "mp_opentime"=>$_POST['opentime'], "mp_closetime"=>$_POST['closetime'], "mp_accept_card"=>$_POST['accept_card'], "mp_nonveg"=>$_POST['veg_nonveg'], "mp_bar_available"=>$_POST['bar_available'], "mp_details_step"=>2, "mp_status"=>1, "mp_details_step_complete"=>1, "mp_details_modified_by"=>$_SESSION['app_user']['mp_details_id']);
		
		$condition = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		
		$result = $GLOBALS["db"]->update('mp_details', $param, $condition);
		
		$result_temp = $GLOBALS["db"]->update('mp_details_temp', $param, $condition);
		
		//-----Admin-Notification-----//
		$type		=	"profile_complete";
		$fields		=	"";
		$json_str	= 	$fields;	
		$notifiy_array	= array("an_mp_user_id"=>$_SESSION['app_user']['mp_details_id'],"an_process"=>"profile_complete","an_text"=>$json_str,"an_notify_from"=>"MP");
		$insert_notifiy = $GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
		//-----Admin-Notification-----//
		
	if($result_temp=="true")
	{
			foreach($param as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
		}
		return $result_temp;
	}
	
	/*function step3()
	{
		//$param = array("mp_details_bank_name"=>$_POST['bank_name'], "mp_details_account_name"=>$_POST['acc_name'],"mp_details_account_number"=>$_POST['acc_number'], "mp_details_bank_ifsc"=>$_POST['ifsc'], "mp_details_modified_by"=>$_SESSION['app_user']['mp_details_id']);
		
		$param_temp = array("mp_details_bank_name"=>$_POST['bank_name'], "mp_details_account_name"=>$_POST['acc_name'],"mp_details_account_number"=>$_POST['acc_number'], "mp_details_bank_ifsc"=>$_POST['ifsc'], "mp_details_step_complete"=>1, "mp_details_modified_by"=>$_SESSION['app_user']['mp_details_id']);
		
		$condition = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		
		//$result = $GLOBALS["db"]->update('mp_details', $param, $condition);
		
		$result_temp = $GLOBALS["db"]->update('mp_details_temp', $param_temp, $condition);
		
		//-----Admin-Notification-----//
		
		$type		=	"profile_complete";
		$fields		=	"";
		//$json_str	= '{"data": {"type":"'.$type.'","fields":"'.$fields.'"}}';	
		$json_str	= $fields;	
		//an_process
		$notifiy_array	= array("an_mp_user_id"=>$_SESSION['app_user']['mp_details_id'],"an_process"=>"profile_complete","an_text"=>$json_str,"an_notify_from"=>"MP");
		$insert_notifiy = $GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
		
		//-----Admin-Notification-----//
		
		if($result_temp=="true")
		{
			foreach($param_temp as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
		}
		return $result_temp;
	}*/
	
	function getSubcategoryList()
	{
		$param = array("id"=>$_POST['id']);
		$data = $GLOBALS["db"]->select("SELECT sub_category_id, sub_category_name FROM sub_category WHERE sub_category_cat_id=:id ORDER BY sub_category_name ASC", $param);
		return $data;
	}
	function getStateList()
	{
		$param = array("id"=>$_POST['id']);
		$data = $GLOBALS["db"]->select("SELECT state_id, state_name FROM state WHERE state_country_id=:id ORDER BY state_name ASC", $param);
		return $data;
	}
	function profile_edit()
	{
		$param = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		$paramStatus = array("status"=>1);
		$data[0] = $GLOBALS["db"]->select("SELECT country_id, country_name FROM country WHERE country_status =:status", $paramStatus);
		$data[1] = $GLOBALS["db"]->select("SELECT category_id, category_name FROM category WHERE category_status =:status", $paramStatus);
		$data[2] = $GLOBALS["db"]->select("SELECT sub_category_id, sub_category_name FROM sub_category WHERE sub_category_status =:status AND sub_category_cat_id = ".$_SESSION['app_user']['mp_details_category'], $paramStatus);
		$param = array("id"=>$_SESSION['app_user']['mp_details_id'], "status"=>1);
		$data[3] = $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_mp_id  =:id AND mp_gallery_status  =:status", $param);
		$data[4] = $GLOBALS["db"]->select("SELECT state_id, state_name FROM state WHERE state_status =:status AND state_country_id =".$_SESSION['app_user']['mp_details_country'], $paramStatus);
		return $data;
	}
	function settings()
	{
		
	}
	function change_password()
	{
		
	}
	function checkPassword()
	{
		$math = new math();
		$password = $math->hash("sha1", $_POST['old'], APP_SEC_KEY);
		if($password==$_SESSION['app_user']['mp_details_password'])
		{
			echo 1;
		}
		else
		{
			echo 0;
		}
	}
	function profileEdit()
	{
		if(!empty($_POST['recommend']))
		{
			$_POST['recommend']= implode(',', $_POST['recommend']);
		}
		else
		{
			$_POST['recommend'] = NULL;
		}
		
		/*$address = $_POST['address'].','.$_POST['city'];
		$address = str_replace(' ', '%20', $address);
		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&sensor=false');
		$geo = json_decode($geo, true);
		if ($geo['status'] = 'OK') 
		{
		  if(isset($geo['results'][0]['geometry']['location']['lat'])){
		  	$latitude = $geo['results'][0]['geometry']['location']['lat'];
		  }else{
			  $latitude = NULL;
		  }
		  if(isset($geo['results'][0]['geometry']['location']['lng'])){
		  	$longitude = $geo['results'][0]['geometry']['location']['lng'];
		  }else{
		  	$longitude = NULL;
		  }		  
		}
		else
		{
			if($_POST['city']=="New Delhi")
			{
				$latitude = '28.6314155';
				$longitude = '77.224758';
			}
			else
			{
				$latitude = '28.4662243';
				$longitude = '77.0307778';
			}
		}*/
		
		$latitude 	= $_POST["lat"];
		$longitude 	= $_POST["lng"];
		
		
		if(!isset($_POST['veg_nonveg']))
		{
			$_POST['veg_nonveg'] = NULL;
		}
		if(!isset($_POST['bar_available']))
		{
			$_POST['bar_available'] = NULL;
		}
		
		
		$param = array(	"mp_details_name"=>$_POST['business'], 
						"mp_details_contact"=>$_POST['business_contact'], 
						"mp_details_city"=>$_POST['city'], 
						"mp_details_address"=>$_POST['address'], 
						"mp_details_person"=>$_POST['person'], 
						"mp_details_phone"=>$_POST['contact'],
						/*"mp_details_category"=>$_POST['category'], */
						"mp_details_designation"=>$_POST['designation'], 
						"mp_details_website"=>$_POST['web_url'], 
						"mp_details_description"=>$_POST['description'], 
						"mp_details_recommend"=>$_POST['recommend'], 
						"mp_opentime"=>$_POST['opentime'], 
						"mp_closetime"=>$_POST['closetime'], 
						"mp_accept_card"=>$_POST['accept_card'], 
						"mp_nonveg"=>$_POST['veg_nonveg'], 
						"mp_bar_available"=>$_POST['bar_available'],
						"mp_details_latitude"=>$latitude, 
						"mp_details_longitude"=>$longitude, 
						"mp_details_modified_by"=>$_SESSION['app_user']['mp_details_id']);
			
			
			$condition = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
			$resultUpdate = $GLOBALS["db"]->update('mp_details_temp', $param, $condition);
			if($resultUpdate=="true")
			{
				/*if(isset($_POST['sub_category']))
				{
					$paramDelete = array("mp_details_mp_id"=>$_SESSION['app_user']['mp_details_id']);
					$result = $GLOBALS["db"]->deleteBulkQuery("mp_details_sub_temp", $paramDelete);
					for($j=0; $j<count($_POST['sub_category']); $j++)
					{
						$paramater = array("mp_details_mp_id"=>$_SESSION['app_user']['mp_details_id'], "mp_details_sub_cat_id"=>$_POST['sub_category'][$j]);
						$result = $GLOBALS["db"]->insert("mp_details_sub_temp", $paramater);
					}
					$_SESSION['app_user']['sub_cat']= implode(',',$_POST['sub_category']);
				}*/			
				foreach($param as $k=>$v)
				{
					$_SESSION['app_user'][$k]= $v;
				}
			}
		
		return $resultUpdate;
		
	}
	function profile_gallery()
	{
		
		$param = array("id"=>$_SESSION['app_user']['mp_details_id']);
		$data = $GLOBALS["db"]->select("SELECT mp_gallery_id, mp_gallery_name, mp_gallery_image, mp_gallery_status, mp_gallery_thumb FROM mp_gallery_temp WHERE mp_gallery_mp_id =:id", $param);
		
		$param = array("id"=>$_SESSION['app_user']['mp_details_id'], "status"=>1);
		
		$dataCount = $GLOBALS["db"]->select("SELECT mp_gallery_id, mp_gallery_name, mp_gallery_image, mp_gallery_status FROM mp_gallery_temp WHERE mp_gallery_mp_id =:id AND gallery_status = :status", $param);
		
		$_SESSION['app_user']['galleryCount'] = count($dataCount);
		return $data;
	}
	function galleryUpdate()
	{
		if($_FILES['file']['name']!="")
		{
			if($_POST['galleryImg']!="")
			{
				$_POST['galleryImg'] = substr($_POST['galleryImg'], 5);
				unlink(APP_CRM_UPLOADS.'media/'.$_POST['galleryImg']);
				unlink(APP_CRM_UPLOADS.'thumb/'.$_POST['galleryImg']);
				$condition = array("gallery_id"=>$_POST['galleryId']);
				/*$data = $GLOBALS["db"]->select("SELECT mp_gallery_actual_image FROM mp_gallery_temp WHERE mp_gallery_id =:gallery_id", $condition);
				if($data[0]['mp_gallery_actual_image']!=NULL)
				{
					if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']))
					{
						unlink(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']);
					}
				}*/
			}
			$newNamePrefix = time() . '_';
			$manipulator = new imageManipulator($_FILES['file']['tmp_name']);
			$imageName =  'media/'.$newNamePrefix . $_FILES['file']['name'];
			$save = $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES['file']['name']);
			$tm = $manipulator->resample(190, 140, true);
			$save = $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES['file']['name']);
			$thumb = 'thumb/'.$newNamePrefix . $_FILES['file']['name'];
			// saving file to uploads folder
			$param = array('mp_gallery_name'=>$_POST['title'], 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_modified_by'=>$_SESSION['app_user']['user_id'],'mp_gallery_status'=>$_POST['status']);
		}
		else
		{
			$imageName = $_POST['galleryImg'];
			$param = array('mp_gallery_name'=>$_POST['title'], 'mp_gallery_modified_by'=>$_SESSION['app_user']['user_id'],'mp_gallery_status'=>$_POST['status']);
		}
		$condition = array("mp_gallery_id"=>$_POST['galleryId']);
		$update = $GLOBALS["db"]->update('mp_gallery_temp', $param, $condition);
		//$update_notfy = $GLOBALS["db"]->update('mp_gallery', $param, $condition);
		
		//-----Admin-Notification-----//
		
		//$json_str	= '{"data": {"type":"'.$type.'","fields":"'.$fields.'"}}';	
		$explode = explode('/', $imageName);
		$json_str	= $explode[1];	
		//an_process
		$notifiy_array	= array("an_mp_user_id"=>$_SESSION['app_user']['mp_details_id'],"an_process"=>"gallery_update","an_text"=>$json_str,"an_notify_from"=>"MP");
		$insert_notifiy = $GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
		
		//-----Admin-Notification-----//
		
		return $update;
	}
	function editGallery($id)
	{
		$param = array('id'=>$id);
		$data = $GLOBALS["db"]->select("SELECT mp_gallery_id, mp_gallery_name, mp_gallery_image, mp_gallery_status, mp_gallery_thumb FROM mp_gallery_temp WHERE mp_gallery_id =:id", $param);
		return $data;	
	}
	function galleryAdd()
	{
		$newNamePrefix = time() . '_';
		$manipulator = new imageManipulator($_FILES['file']['tmp_name']);
		$imageName =  'media/'.$newNamePrefix . $_FILES['file']['name'];
		$save = $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . $_FILES['file']['name']);
		$tm = $manipulator->resample(190, 140, true);
		$save = $manipulator->save(APP_CRM_UPLOADS.'thumb/' . $newNamePrefix . $_FILES['file']['name']);
		$thumb = 'thumb/'.$newNamePrefix . $_FILES['file']['name'];
		
		$param = array('mp_gallery_name'=>$_POST['title'], 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_modified_by'=>$_SESSION['app_user']['user_id'], 'mp_gallery_created_by'=>$_SESSION['app_user']['mp_details_id'], 'mp_gallery_status'=>$_POST['status'], 'mp_gallery_mp_id'=>$_SESSION['app_user']['mp_details_id']);
		$insert = $GLOBALS["db"]->insertNow('mp_gallery_temp', $param, 'mp_gallery_created_on');
		return $insert;
	}
	function deletePic_edit()
	{
		//$deal_id	=	$_POST["deal_id"];
		//$del		=	"image".$_POST['deleteImg'];
		//$param = array("mp_gallery_id"=>$_POST['deleteImg']);
		//$data = $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_id =:mp_gallery_id", $param);
		//$_POST['deleteUrl'] = substr($_POST['deleteUrl'], 5);
		
		/*if($data[0]['mp_gallery_actual_image']!=NULL)
		{
			if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']))
			{
				unlink(APP_CRM_UPLOADS.$data[0]['mp_gallery_actual_image']);
			}
		}*/
		$mp_gallery_id			=	$_POST['mp_gallery_id'];
		$path					=	"plus.jpg";
		$param_update 			= 	array("mp_gallery_image"=>$path,"mp_gallery_thumb"=>$path);
		$condition 				= 	array("mp_gallery_id"=>$mp_gallery_id,"mp_gallery_mp_id"=>$_SESSION['app_user']['mp_details_id']);
		$data_temp 				= 	$GLOBALS["db"]->update("mp_gallery_temp", $param_update, $condition);
		$data	 				= 	$GLOBALS["db"]->update("mp_gallery", $param_update, $condition);
		
		if($data_temp	==	"true")
		{
			$param 				= array("mp_gallery_mp_id"=>$_SESSION['app_user']['mp_details_id']);
			$data_value[0] 		= $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_mp_id =:mp_gallery_mp_id", $param);
			$with_image			=	0;
			$without_image		=	0;
			
			$with_image_array				=	array();
			$with_image_gallery_id_array	=	array();
			
			$without_image_array	=	array('plus.jpg');
			
			for($i=0;$i<count($data_value[0]);$i++)
			{
				if($data_value[0][$i]['mp_gallery_image']	!=	"plus.jpg")
				{
					$with_image		=	$with_image+1;	
					array_push($with_image_array,$data_value[0][$i]['mp_gallery_image']);
					array_push($with_image_gallery_id_array,$data_value[0][$i]['mp_gallery_id']);
				}
				else
				{
					$without_image	=	$without_image+1;	
					array_push($without_image_array,$data_value[0][$i]['mp_gallery_image']);
				}
			}
			$with_image_string		=	implode("#",$with_image_array);
			$with_gallery_id_string	=	implode("#",$with_image_gallery_id_array);
			echo $with_image_string.'$#$'.$with_gallery_id_string.'$#$'.count($without_image_array);
		}
		
			/*$drag_ul_open	=	'<ul id="images" class="ui-sortable">';
			$dragable		=	'';
			for($i=0;$i<$with_image;$i++)
			{
				$j	=	$i+1;
				$dragable	=	$dragable + '<li id="'+four_images[i]+'" style="width:auto;padding-right: 5px;padding-left: 5px; width:115px; float:left;"><button type="button" id="'+mp_gallery_id[i]+'" class="btn btn-box-tool btnCross"  data-widget="remove" style="display:block"><i class="fa fa-times"></i></button><label for="image'+j+'"><img id="image'+j+'pic" style="cursor:pointer;border: 1px solid;" height="80px" width="110px"  src="'+four_images[i]+'"/><input name="image'+j+'" id="image'+j+'" style="display:none" class="form-control input-sm" type="file"><i class="fa fa-spinner fa-pulse" style="display:none;position: absolute;top: 35px; left: 65px;" id="image'+j+'Lod"></i><input type="hidden" class="imageUrl" name="image'+j+'Url" id="image'+j+'Url" value="'+four_images[i]+'" /><input type="hidden" name="image'+j+'Id" id="image'+j+'Id" value="'+four_images[i]+'" /></li>';
			}
		}*/
		/*
		var drag_ul_open	=	'<ul id="images" class="ui-sortable">';
	var 	dragable	=	'';
	for(i=0;i<img_number;i++)
	{
		var j	=	parseInt(i)+1;
		dragable	=	dragable + '<li id="'+four_images[i]+'" style="width:auto;padding-right: 5px;padding-left: 5px; width:115px; float:left;"><button type="button" id="'+mp_gallery_id[i]+'" class="btn btn-box-tool btnCross"  data-widget="remove" style="display:block"><i class="fa fa-times"></i></button><label for="image'+j+'"><img id="image'+j+'pic" style="cursor:pointer;border: 1px solid;" height="80px" width="110px"  src="'+four_images[i]+'"/><input name="image'+j+'" id="image'+j+'" style="display:none" class="form-control input-sm" type="file"><i class="fa fa-spinner fa-pulse" style="display:none;position: absolute;top: 35px; left: 65px;" id="image'+j+'Lod"></i><input type="hidden" class="imageUrl" name="image'+j+'Url" id="image'+j+'Url" value="'+four_images[i]+'" /><input type="hidden" name="image'+j+'Id" id="image'+j+'Id" value="'+four_images[i]+'" /></li>';
	}
	var drag_ul_close	=	 '</ul>';
	var html_ele	=	drag_ul_open + dragable + drag_ul_close;
	//<<<<<----------------------Drageable Code---------------------->>>>>
	$("#dragable").html(html_ele);
		*/
		
		
		/*$delete = $GLOBALS["db"]->deleteQuery('mp_gallery_temp', $param);
		$delete_1 = $GLOBALS["db"]->deleteQuery('mp_gallery', $param);
		if($delete)
		{
			unlink(APP_CRM_UPLOADS.'media/'.$_POST['deleteUrl']);
			unlink(APP_CRM_UPLOADS.'thumb/'.$_POST['deleteUrl']);
		}
		echo $delete;*/
	}
	function deletePic()
	{
		$mp_gallery_id_image	=	$_POST['mp_gallery_id_image'];
		$condition = array("mp_gallery_id"=>$mp_gallery_id_image);
		$delete_temp 	= $GLOBALS["db"]->deleteQuery('mp_gallery_temp', $condition);
	}
	function deleteLogo()
	{
		$param = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		$data = $GLOBALS["db"]->select("SELECT mp_details_logo, mp_details_logo_thumb, mp_details_person_pic, mp_details_person_thumb FROM mp_details WHERE mp_details_id =:mp_details_id", $param);
		if($_POST['id']=="file")
		{
			if($data[0]['mp_details_logo']!=NULL || $data[0]['mp_details_logo_thumb']!=NULL)
			{
				if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_logo']))
				{
					unlink(APP_CRM_UPLOADS.$data[0]['mp_details_logo']);
				}
				if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_logo_thumb']))
				{
					unlink(APP_CRM_UPLOADS.$data[0]['mp_details_logo_thumb']);
				}
			}	
			$parameter = array("mp_details_logo"=>NULL, "mp_details_logo_thumb"=>NULL);
			$update = $GLOBALS["db"]->update("mp_details", $parameter, $param);
			foreach($parameter as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
		}
		else
		{
			if($data[0]['mp_details_person_pic']!=NULL || $data[0]['mp_details_person_thumb']!=NULL)
			{
				if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_person_pic']))
				{
					unlink(APP_CRM_UPLOADS.$data[0]['mp_details_person_pic']);
				}
				if(file_exists(APP_CRM_UPLOADS.$data[0]['mp_details_person_thumb']))
				{
					unlink(APP_CRM_UPLOADS.$data[0]['mp_details_person_thumb']);
				}
			}
			$parameter = array("mp_details_person_pic"=>NULL, "mp_details_person_thumb"=>NULL);
			$update = $GLOBALS["db"]->update("mp_details", $parameter, $param);
			foreach($parameter as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
		}
		return $update;
	}
	function changePassword()
	{
		
		$b_name			=	$_POST['b_name'];
		$e_name			=	$_POST['e_name'];
		$p_name			=	$_POST['p_name'];
		
		$param 			=	array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		if($b_name	!=	''){
			$parameter 		= 	array("mp_details_name"=>$b_name);
			$update_temp 	= 	$GLOBALS["db"]->update("mp_details_temp", $parameter, $param);
			$update 		= 	$GLOBALS["db"]->update("mp_details", $parameter, $param);
			foreach($parameter as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
			//return $update;
		}
		if($e_name	!=	''){
			$parameter 		= 	array("mp_details_email"=>$e_name);
			$update_temp 	= 	$GLOBALS["db"]->update("mp_details_temp", $parameter, $param);
			$update 		= 	$GLOBALS["db"]->update("mp_details", $parameter, $param);
			foreach($parameter as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
			//return $update;
		}
		if($p_name	!=	''){
			$math = new math();
			$password = $math->hash("sha1", $p_name, APP_SEC_KEY);
			$parameter 		= 	array("mp_details_password"=>$password);
			$update_temp 	= 	$GLOBALS["db"]->update("mp_details_temp", $parameter, $param);
			$update 		= 	$GLOBALS["db"]->update("mp_details", $parameter, $param);
			foreach($parameter as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
			//return $update;
		}
		
		
		
		
		/*if($p_name	!=	''){
			$math = new math();
			$password = $math->hash("sha1", $p_name, APP_SEC_KEY);
			//$param 			=	array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
			$parameter 		= 	array("mp_details_email"=>$e_name,"mp_details_name"=>$b_name,"mp_details_password"=>$password);
			$update_temp 	= 	$GLOBALS["db"]->update("mp_details_temp", $parameter, $param);
			$update 		= 	$GLOBALS["db"]->update("mp_details", $parameter, $param);
			foreach($parameter as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
			return $update;
		}else{
			
		}*/
		//die;
		
		/*if($_POST["flag"]	==	"email_change")
		{
			$email			=	$_POST["email_setting"];
			$param 			=	array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
			$parameter 		= 	array("mp_details_email"=>$email);
			$update_temp 	= 	$GLOBALS["db"]->update("mp_details_temp", $parameter, $param);
			$update 		= 	$GLOBALS["db"]->update("mp_details", $parameter, $param);
			foreach($parameter as $k=>$v)
			{
				$_SESSION['app_user'][$k]= $v;
			}
			return $update;
		}else{
			
			$business_setting	=	$_POST["business_setting"];
			$param 				=	array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
			$parameter 			= 	array("mp_details_name"=>$business_setting);
			$update_temp 		= 	$GLOBALS["db"]->update("mp_details_temp", $parameter, $param);
			$update 			= 	$GLOBALS["db"]->update("mp_details", $parameter, $param);
			
			if($update){
				foreach($parameter as $k=>$v)
				{
					$_SESSION['app_user'][$k]= $v;
				}
				$math = new math();
				$password = $math->hash("sha1", $_POST['n_password'], APP_SEC_KEY);
				$param = array("mp_details_password"=>$password,"mp_details_modified_by"=>$_SESSION['app_user']['mp_details_id']);
				$condition = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
				$data = $GLOBALS["db"]->update("mp_details_temp", $param, $condition);
				return $data;
			}
		}*/	
		
	}
	
	/*function chkpassword()
	{
		//$math = new math();
		//echo $password = $math->hash("sha1", $_POST['o_password'], APP_SEC_KEY);
		//$param = array("mp_details_password"=>$password,"mp_details_modified_by"=>$_SESSION['app_user']['mp_details_id']);
		//$condition = array("mp_details_id"=>$_SESSION['app_user']['mp_details_id']);
		//$data = $GLOBALS["db"]->update("mp_details_temp", $param, $condition);
		//return $data;
	}*/
	
	function adminotify_profileEdit()
	{
		$flag	=	$_POST["flag"];
		if($flag	==	"profile")
		{
			$str				=	$_POST["change_field"];
			$an_mp_details_id	=	$_POST["mp_details_id"];
			$an_text			=	$str;
			$notifiy_array		= 	array("an_mp_user_id"=>$an_mp_details_id,"an_process"=>"profile_update","an_text"=>$an_text,"an_notify_from"=>"MP");
			$insert_notifiy 	= 	$GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
			//return $insert_notifiy;
		}
		else
		{
			$new_gallery_file_name	=	$_POST["upload_file"];
			$an_mp_details_id		=	$_POST["mp_details_id"];
			//$an_text				=	'{"data":{"type":"gallery","field":"'.$new_gallery_file_name.'"}}';
			$an_text				=	$new_gallery_file_name;
			$notifiy_array			= 	array("an_mp_user_id"=>$an_mp_details_id,"an_process"=>"gallery_update","an_text"=>$an_text,"an_notify_from"=>"MP");
			$insert_notifiy 		= 	$GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
			//return $insert_notifiy;
		}
	}
	function adminotify_dealEdit()
	{
		$oldvalueimg	=	$_POST['oldvalueimg'];
		$newvalueimg	=	$_POST['newvalueimg'];
		$globalvalue	=	$_POST['globalvalue'];
		
		$str				=	$_POST["change_field"];
		$an_mp_details_id	=	$_POST["offer_id"];
		$an_deal_type		=	$_POST["an_deal_type"];
		$an_text			=	$str;
		$an_process			=	$_POST["an_process"];
		
		$notifiy_array		= 	array("an_mp_user_id"=>$an_mp_details_id,"an_process"=>$an_process,"an_text"=>$an_text,"an_notify_from"=>"MP","old_image"=>$oldvalueimg,"new_image"=>$newvalueimg,"is_drag"=>$globalvalue,"an_deal_type"=>$an_deal_type);
		
			
		$insert_notifiy 	= 	$GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
		return $insert_notifiy;
	}
	
}

?>