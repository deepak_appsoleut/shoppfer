<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Shoppfer</title>
<?php include('includes/top.php'); ?>
<link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
</head>
<body id="page-top" class="index">
<!-- Navigation -->
<?php include('includes/nav.php'); ?>
<!-- Header -->
<header>
  <div class="container">
    <div class="intro-text">
      <div class="intro-lead-in">Welcome To Shoppfer</div>
      <div class="intro-heading">It's Nice To Meet You</div>
      <a href="#" class="page-scroll btn btn-xl">Tell Me More</a> </div>
  </div>
</header>
<div class="portfolio-modal modal fade" id="forHotels" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"> </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body"> 
            <!-- Project Details Go Here -->
            <h4 style="border-bottom:1px solid #ccc; padding-bottom:10px;">Already a Partner? <a href="<?php echo APP_URL; ?>admin">Sign In</a></h4>
            <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success : </b> <span></span></div>
      <div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning : </b> <span></span></div>
            <form id="form" method="post">
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Hotel Name*</label>
                        <input name="hotel" placeholder="Hotel Name" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">First Name*</label>
                        <input name="fname" placeholder="First Name" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Last Name</label>
                        <input name="lname" placeholder="Last Name" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Email Address*</label>
                        <input name="email" placeholder="Email Address" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Website</label>
                        <input name="website" placeholder="Website" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Password*</label>
                        <input name="password" placeholder="Password" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Confirm Password*</label>
                        <input name="cnf_password" placeholder="Confirm Password" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Phone or Mobile*</label>
                        <input name="phone" placeholder="Phone or Mobile" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Hotel Representative*</label>
                        <select name="representative" class="form-control input-sm">
                        <?php 
						for($i=0; $i<count($data[0]); $i++) { if($i==0) { $selected = "selected";}else {$selected = "";} ?>
                        <option value="<?php echo $data[0][$i]['role_id']; ?>" <?php echo $selected; ?>><?php echo $data[0][$i]['role_name']; ?></option>
                        <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Country*</label>
                        <select name="country" class="form-control input-sm">
                        <option value="AF">Afghanistan</option>
<option value="AL">Albania</option>
<option value="DZ">Algeria</option>
<option value="AS">American Samoa</option>
<option value="AD">Andorra</option>
<option value="AO">Angola</option>
<option value="AI">Anguilla</option>
<option value="AQ">Antarctica</option>
<option value="AG">Antigua and Barbuda</option>
<option value="AR">Argentina</option>
<option value="AM">Armenia</option>
<option value="AW">Aruba</option>
<option value="AU">Australia</option>
<option value="AT">Austria</option>
<option value="AZ">Azerbaijan</option>
<option value="BS">Bahamas</option>
<option value="BH">Bahrain</option>
<option value="BD">Bangladesh</option>
<option value="BB">Barbados</option>
<option value="BY">Belarus</option>
<option value="BE">Belgium</option>
<option value="BZ">Belize</option>
<option value="BJ">Benin</option>
<option value="BM">Bermuda</option>
<option value="BT">Bhutan</option>
<option value="BO">Bolivia, Plurinational State of</option>
<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
<option value="BA">Bosnia and Herzegovina</option>
<option value="BW">Botswana</option>
<option value="BV">Bouvet Island</option>
<option value="BR">Brazil</option>
<option value="IO">British Indian Ocean Territory</option>
<option value="BN">Brunei Darussalam</option>
<option value="BG">Bulgaria</option>
<option value="BF">Burkina Faso</option>
<option value="BI">Burundi</option>
<option value="KH">Cambodia</option>
<option value="CM">Cameroon</option>
<option value="CA">Canada</option>
<option value="CV">Cape Verde</option>
<option value="KY">Cayman Islands</option>
<option value="CF">Central African Republic</option>
<option value="TD">Chad</option>
<option value="CL">Chile</option>
<option value="CN">China</option>
<option value="CX">Christmas Island</option>
<option value="CC">Cocos (Keeling) Islands</option>
<option value="CO">Colombia</option>
<option value="KM">Comoros</option>
<option value="CG">Congo</option>
<option value="CD">Congo, The Democratic Republic of the</option>
<option value="CK">Cook Islands</option>
<option value="CR">Costa Rica</option>
<option value="HR">Croatia</option>
<option value="CU">Cuba</option>
<option value="CW">Curaçao</option>
<option value="CY">Cyprus</option>
<option value="CZ">Czech Republic</option>
<option value="CI">Côte d'Ivoire</option>
<option value="DK">Denmark</option>
<option value="DJ">Djibouti</option>
<option value="DM">Dominica</option>
<option value="DO">Dominican Republic</option>
<option value="EC">Ecuador</option>
<option value="EG">Egypt</option>
<option value="SV">El Salvador</option>
<option value="GQ">Equatorial Guinea</option>
<option value="ER">Eritrea</option>
<option value="EE">Estonia</option>
<option value="ET">Ethiopia</option>
<option value="FK">Falkland Islands (Malvinas)</option>
<option value="FO">Faroe Islands</option>
<option value="FJ">Fiji</option>
<option value="FI">Finland</option>
<option value="FR">France</option>
<option value="GF">French Guiana</option>
<option value="PF">French Polynesia</option>
<option value="TF">French Southern Territories</option>
<option value="GA">Gabon</option>
<option value="GM">Gambia</option>
<option value="GE">Georgia</option>
<option value="DE">Germany</option>
<option value="GH">Ghana</option>
<option value="GI">Gibraltar</option>
<option value="GR">Greece</option>
<option value="GL">Greenland</option>
<option value="GD">Grenada</option>
<option value="GP">Guadeloupe</option>
<option value="GU">Guam</option>
<option value="GT">Guatemala</option>
<option value="GG">Guernsey</option>
<option value="GN">Guinea</option>
<option value="GW">Guinea-Bissau</option>
<option value="GY">Guyana</option>
<option value="HT">Haiti</option>
<option value="HM">Heard Island and McDonald Islands</option>
<option value="VA">Holy See (Vatican City State)</option>
<option value="HN">Honduras</option>
<option value="HK">Hong Kong</option>
<option value="HU">Hungary</option>
<option value="IS">Iceland</option>
<option value="IN">India</option>
<option value="ID">Indonesia</option>
<option value="IR">Iran, Islamic Republic of</option>
<option value="IQ">Iraq</option>
<option value="IE">Ireland</option>
<option value="IM">Isle of Man</option>
<option value="IL">Israel</option>
<option value="IT">Italy</option>
<option value="JM">Jamaica</option>
<option value="JP">Japan</option>
<option value="JE">Jersey</option>
<option value="JO">Jordan</option>
<option value="KZ">Kazakhstan</option>
<option value="KE">Kenya</option>
<option value="KI">Kiribati</option>
<option value="KP">Korea, Democratic People's Republic of</option>
<option value="KR">Korea, Republic of</option>
<option value="KW">Kuwait</option>
<option value="KG">Kyrgyzstan</option>
<option value="LA">Lao People's Democratic Republic</option>
<option value="LV">Latvia</option>
<option value="LB">Lebanon</option>
<option value="LS">Lesotho</option>
<option value="LR">Liberia</option>
<option value="LY">Libya</option>
<option value="LI">Liechtenstein</option>
<option value="LT">Lithuania</option>
<option value="LU">Luxembourg</option>
<option value="MO">Macao</option>
<option value="MK">Macedonia, Republic of</option>
<option value="MG">Madagascar</option>
<option value="MW">Malawi</option>
<option value="MY">Malaysia</option>
<option value="MV">Maldives</option>
<option value="ML">Mali</option>
<option value="MT">Malta</option>
<option value="MH">Marshall Islands</option>
<option value="MQ">Martinique</option>
<option value="MR">Mauritania</option>
<option value="MU">Mauritius</option>
<option value="YT">Mayotte</option>
<option value="MX">Mexico</option>
<option value="FM">Micronesia, Federated States of</option>
<option value="MD">Moldova, Republic of</option>
<option value="MC">Monaco</option>
<option value="MN">Mongolia</option>
<option value="ME">Montenegro</option>
<option value="MS">Montserrat</option>
<option value="MA">Morocco</option>
<option value="MZ">Mozambique</option>
<option value="MM">Myanmar</option>
<option value="NA">Namibia</option>
<option value="NR">Nauru</option>
<option value="NP">Nepal</option>
<option value="NL">Netherlands</option>
<option value="AN">Netherlands Antilles</option>
<option value="NC">New Caledonia</option>
<option value="NZ">New Zealand</option>
<option value="NI">Nicaragua</option>
<option value="NE">Niger</option>
<option value="NG">Nigeria</option>
<option value="NU">Niue</option>
<option value="NF">Norfolk Island</option>
<option value="MP">Northern Mariana Islands</option>
<option value="NO">Norway</option>
<option value="OM">Oman</option>
<option value="PK">Pakistan</option>
<option value="PW">Palau</option>
<option value="PS">Palestine, State of</option>
<option value="PA">Panama</option>
<option value="PG">Papua New Guinea</option>
<option value="PY">Paraguay</option>
<option value="PE">Peru</option>
<option value="PH">Philippines</option>
<option value="PN">Pitcairn</option>
<option value="PL">Poland</option>
<option value="PT">Portugal</option>
<option value="PR">Puerto Rico</option>
<option value="QA">Qatar</option>
<option value="RO">Romania</option>
<option value="RU">Russian Federation</option>
<option value="RW">Rwanda</option>
<option value="RE">Réunion</option>
<option value="BL">Saint Barthélemy</option>
<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
<option value="KN">Saint Kitts and Nevis</option>
<option value="LC">Saint Lucia</option>
<option value="MF">Saint Martin (French part)</option>
<option value="PM">Saint Pierre and Miquelon</option>
<option value="VC">Saint Vincent and the Grenadines</option>
<option value="WS">Samoa</option>
<option value="SM">San Marino</option>
<option value="ST">Sao Tome and Principe</option>
<option value="SA">Saudi Arabia</option>
<option value="SN">Senegal</option>
<option value="RS">Serbia</option>
<option value="SC">Seychelles</option>
<option value="SL">Sierra Leone</option>
<option value="SG">Singapore</option>
<option value="SX">Sint Maarten (Dutch part)</option>
<option value="SK">Slovakia</option>
<option value="SI">Slovenia</option>
<option value="SB">Solomon Islands</option>
<option value="SO">Somalia</option>
<option value="ZA">South Africa</option>
<option value="GS">South Georgia and the South Sandwich Islands</option>
<option value="SS">South Sudan</option>
<option value="ES">Spain</option>
<option value="LK">Sri Lanka</option>
<option value="SD">Sudan</option>
<option value="SR">Suriname</option>
<option value="SJ">Svalbard and Jan Mayen</option>
<option value="SZ">Swaziland</option>
<option value="SE">Sweden</option>
<option value="CH">Switzerland</option>
<option value="SY">Syrian Arab Republic</option>
<option value="TW">Taiwan</option>
<option value="TJ">Tajikistan</option>
<option value="TZ">Tanzania, United Republic of</option>
<option value="TH">Thailand</option>
<option value="TL">Timor-Leste</option>
<option value="TG">Togo</option>
<option value="TK">Tokelau</option>
<option value="TO">Tonga</option>
<option value="TT">Trinidad and Tobago</option>
<option value="TN">Tunisia</option>
<option value="TR">Turkey</option>
<option value="TM">Turkmenistan</option>
<option value="TC">Turks and Caicos Islands</option>
<option value="TV">Tuvalu</option>
<option value="UG">Uganda</option>
<option value="UA">Ukraine</option>
<option value="AE">United Arab Emirates</option>
<option value="GB">United Kingdom</option>
<option selected="selected" value="US">United States</option>
<option value="UM">United States Minor Outlying Islands</option>
<option value="UY">Uruguay</option>
<option value="UZ">Uzbekistan</option>
<option value="VU">Vanuatu</option>
<option value="VE">Venezuela, Bolivarian Republic of</option>
<option value="VN">Viet Nam</option>
<option value="VG">Virgin Islands, British</option>
<option value="VI">Virgin Islands, U.S.</option>
<option value="WF">Wallis and Futuna</option>
<option value="EH">Western Sahara</option>
<option value="YE">Yemen</option>
<option value="ZM">Zambia</option>
<option value="ZW">Zimbabwe</option>
<option value="AX">Åland Islands</option>
                        </select>
                      </div>
                    </div>
                </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">City*</label>
                        <input name="city" placeholder="City" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                </div>
              <div class="marTop10"></div>
              <div class="row">
                <div class="col-md-3"></div>
                <div class="form-group">
                <div class="col-md-6">
                  <button class="btn-warning btn" style="width:100%" type="submit">SIGN UP</button>
                </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portfolio-modal modal fade" id="forMP" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"> </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body"> 
            <!-- Project Details Go Here -->
            <h4 style="border-bottom:1px solid #ccc; padding-bottom:10px;">Already a Marketing Partner? <a href="<?php echo APP_URL; ?>admin">Sign In</a></h4>
             <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success : </b> <span></span></div>
      		<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning : </b> <span></span></div>
            <form id="form1" method="post">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Business Name*</label>
                        <input name="business" placeholder="Business Name" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Email Address</label>
                        <input name="email" placeholder="Email Address" class="form-control input-sm" type="email">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Password*</label>
                        <input name="password" placeholder="Password" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Confirm Password*</label>
                        <input name="cnf_password" placeholder="Confirm Password" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
              <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                  <button class="btn-warning btn" style="width:100%" type="submit">SIGN UP</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
<!-- jQuery --> 
<!-- Contact Form JavaScript --> 
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script type="text/javascript">
$('#form').bootstrapValidator({
        fields: {
			hotel: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Hotel Name!'
                    }
                }
            },
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter First Name!'
                    }
                }
            },
			email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Email Address!'
                    }
                }
            },
			password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Password'
                    }
                }
            },
            cnf_password: {
                validators: {
					identical: {
						field: 'password',
						message: 'The password and its confirm password are not the same!'
					},
					notEmpty: {
                        message: 'Please Enter Confirm Password'
                    }
                }
            },
			phone: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Phone or Mobile Number!'
                    }
                }
            },
			city: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter City!'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>default/home/add_hotel',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data==1)
				{
					$('.alert-warning').hide();
					$('.alert-success').show();
					$('.alert-success span').html("Thanks for the registration!");
				}
				else
				{
					$('.alert-success').hide();
					$('.alert-warning').show();
					$('.alert-warning span').html("This email is already exist with same Hotel!");
				}
            },
            error: function (data) {
            }
   });
});
$('#form1').bootstrapValidator({
        fields: {
			business: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Business Name!'
                    }
                }
            },
			email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Email Address!'
                    }
                }
            },
			password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Password'
                    }
                }
            },
            cnf_password: {
                validators: {
					identical: {
						field: 'password',
						message: 'The password and its confirm password are not the same!'
					},
					notEmpty: {
                        message: 'Please Enter Confirm Password'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>default/home/add_mp',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				alert(data);
				if(data==1)
				{
					$('.alert-warning').hide();
					$('.alert-success').show();
					$('.alert-success span').html("Thanks for the registration!");
				}
				else
				{
					$('.alert-success').hide();
					$('.alert-warning').show();
					$('.alert-warning span').html("This email is already exist with same Marketing partner!");
				}
            },
            error: function (data) {
            }
   });
});
</script>
<script type="text/javascript">
$(function() {
<!----Modal Close Reload-------->   
$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal','.modal', function () {
$(this).removeData('bs.modal');
});
});
</script>
<!-- Custom Theme JavaScript --> 
<script src="<?php echo APP_JS; ?>agency.js"></script>
</body>
</html>
