<?php
/*
 * File: Agent.php
 * Created By: Deepak Bhardwaj
 */

class web_view extends controller {

	function __construct() {
		parent::__construct();
	}	
	/**
	 * API for sending all categories list 
	 * Using GET Method 
	 */
	function web_view()
	{
		$this->view->render(__FUNCTION__, null);
	}
}
?>