<?php
/*
 * File: Agent.php
 * Created By: Deepak Bhardwaj
 */

class kiosk extends controller {

	function __construct() {
		parent::__construct();
	}
	/**
	 * API for getting all API from mobile APP
	 * Using GET Method 
	 * @param using for receiving parameters
	 */
	 /*
	function getAPI($param = null)
	{	
	   $headers = apache_request_headers();
	   $publicKey = $headers['oauth-public'];
	   $timeStamp = $headers['oauth-time'];
	   $key = $publicKey.$timeStamp;
	   $signature = $headers['oauth-hash'];
	   $math = new math();
	   $match = $math->hash_function($key,$signature, APP_SEC_KEY);
	   if($match)
	   {
			if($_SERVER["REQUEST_METHOD"] == "GET")
			{
				$result['success'] =  true;
				$result['code'] = 601;
				$apiArray = array('signUp','signIn','profileUpdate','statusUpdate');
				$apiURL = array();
				for($i=0; $i<count($apiArray); $i++)
				{
					$apiURL[$apiArray[$i]] = APP_URL.'api/agent/'.$apiArray[$i];
				}
				echo json_encode($apiURL);
			}
			else
			{
				$result['success'] =  true;
				$result['code'] = 701;
				$result['message'] = "You are not sending the valid headers and request method correctly!";
				echo json_encode($result);
			}
	   }
	   else
	   {
		   $result['success'] =  true;
		   $result['code'] = 700;
		   $result['message'] = "Invalid Request!";
		   echo json_encode($result);
	   }
	}*/
	
	
/*=============================================================== isUserExist API Starts =========================================*/	
	/**
	 * API to check user Exist or not from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function isUserExist($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== isUserExist API Ends =========================================*/	

/*=============================================================== Registration API Starts =========================================*/	
	/**
	 * API to check user Exist or not from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	 function registration($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== Registration API Ends =========================================*/		
	
/*=============================================================== deviceInfo API Starts =========================================*/	
	/**
	 * API for deviceInfo from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function deviceInfo($param = null)
	{	
	   $headers = apache_request_headers();
	   //$result = $this->model->process(__CLASS__, __FUNCTION__, array($headers));
	   //echo json_encode($result);
	   //die;
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== deviceInfo API Ends =========================================*/

/*=============================================================== deviceNewInfo API Starts =========================================*/
	
	/**
	 * API for deviceNewInfo from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function deviceNewInfo($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else if($_SERVER["REQUEST_METHOD"] == "GET")
		{
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($headers));
	   		echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== deviceNewInfo API Ends =========================================*/

/*=============================================================== getOfferDetail API Starts =========================================*/
	
	/**
	 * API for get Offer details for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getOfferDetail($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			//$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($param));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			header("location:https://play.google.com/store/apps/details?id=com.shoppferdeals.appsoleut&hl=en");
			echo json_encode($result);
		}
	}

/*=============================================================== getOfferDetail API Ends =========================================*/	

/*=============================================================== availOffer API Starts =========================================*/
	
	/**
	 * API for get availOffer for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function availOffer($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== availOffer API Ends =========================================*/	

/*=============================================================== cancelAvailOffer API Starts =========================================*/
	
	/**
	 * API for get availOffer for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function cancelAvailOffer($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== cancelAvailOffer API Ends =========================================*/	

/*=============================================================== check writeReview API Starts =========================================*/
	
	/**
	 * API for writeReview for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function checkWriteReview($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== check writeReview API Ends =========================================*/	

/*=============================================================== writeReview API Starts =========================================*/
	
	/**
	 * API for writeReview for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function writeReview($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== writeReview API Ends =========================================*/	

/*=============================================================== Bookmark API Starts =========================================*/
	
	/**
	 * API for writeReview for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function bookmark($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== Bookmark API Ends =========================================*/	


/*=============================================================== getFavouriteList API Starts =========================================*/
	
	/**
	 * API for writeReview for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getFavouriteList($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== getFavouriteList API Ends =========================================*/	

/*=============================================================== filterApiCity API Starts =========================================*/
	
	/**
	 * API for writeReview for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function filterApiCity($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== filterApiCity API Ends =========================================*/	

/*=============================================================== filterApiSearch API Starts =========================================*/
	
	/**
	 * API for writeReview for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function filterApiSearch($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== filterApiSearch API Ends =========================================*/	

/*=============================================================== filterApiDistance


 API Starts =========================================*/
	
	/**
	 * API for writeReview for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function filterApiDistance($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== filterApiDistance API Ends =========================================*/	

/*=============================================================== login API Starts =========================================*/
	
	/**
	 * API for Sign IN from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	 /*
	function login($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}

/*=============================================================== login API Ends =========================================*/
/*		
	function login1($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}


/*=============================================================== otpVerify API Starts =========================================*/	
	/**
	 * API for OTP verifation from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function otpVerify($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== otpVerify API Ends =========================================*/	

/*=============================================================== statusUpdate API Starts =========================================*/		
	/**
	 * API for deleting Any value from database
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function statusUpdate($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== otpVerify API Ends =========================================*/	

/*=============================================================== resendOTP API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function resendOTP($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== resendOTP API Ends =========================================*/		

/*=============================================================== profileUpdate API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function profileUpdate($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== profileUpdate API Ends =========================================*/	
/*=============================================================== profileUpdateVerify API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function profileUpdateVerify($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== profileUpdateVerify API Ends =========================================*/		

/*=============================================================== getCoupon API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getCoupon($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== getCoupon API Ends =========================================*/

/*=============================================================== getMerchantName API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getMerchantName($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "GET")
		{
			$result = $this->model->process(__CLASS__, __FUNCTION__);
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== getMerchantName API Ends =========================================*/

/*=============================================================== getMerchantName API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getMerchantAddress($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== getMerchantName API Ends =========================================*/
/*=============================================================== sendGCMNearBy API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function sendGCMNearBy($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== sendGCMNearBy API Ends =========================================*/
/*=============================================================== sendGCMNearBy API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP

	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function sendGCMNotification($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== sendGCMNearBy API Ends =========================================*/

/*=============================================================== getStoreData API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP

	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getStoreData($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== getStoreData API Ends =========================================*/
/*=============================================================== getStoreData API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP

	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getStoreValue($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			//$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($param));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== getStoreData API Ends =========================================*/
/*=============================================================== getStoreData API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP

	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getDataByCategory($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			//$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($param));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== getStoreData API Ends =========================================*/
/*=============================================================== getStoreData API Starts =========================================*/		
	
	/**
	 * API for resend code for mobile APP

	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function getRefreshData($param = null)
	{	
	   $headers = apache_request_headers();
	   if($_SERVER["REQUEST_METHOD"] == "POST")
		{
			$input = json_decode(file_get_contents('php://input'), true);
			$result = $this->model->process(__CLASS__, __FUNCTION__, array($input));
			echo json_encode($result);
		}
		else
		{
			$result['success'] =  true;
			$result['code'] = 701;
			$result['message'] = "You are not sending the valid headers and request method correctly!";
			echo json_encode($result);
		}
	}
/*=============================================================== getStoreData API Ends =========================================*/
		
}
?>