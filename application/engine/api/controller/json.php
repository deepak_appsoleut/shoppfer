<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class json extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function json()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
}
?>