<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class json_model {
	function json()
	{
		
		/*
		for stores
		$url = "https://www.coupomated.com/apiv3/7d52-4930-2db5-28ce/getStores/N/json";*/
		
		/* for categories 
		$url = "https://www.coupomated.com/apiv3/7d52-4930-2db5-28ce/getCategories/json";*/
		
		/* for getall coupons */
		$url = "https://www.coupomated.com/apiv3/7d52-4930-2db5-28ce/getAllCoupons/json";
		
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch);
		$data = json_decode($result, true);
		
		/*
		for data insertion of stores
		for($i=0; $i<count($data); $i++)
		{
			$insertParam = array("store_id"=>$data[$i]['STORE_ID'],"store_name"=>$data[$i]['STORE_NAME'],"store_link"=>$data[$i]['STORE_LINK'],"store_cat"=>$data[$i]['STORE_CAT'],"store_logo"=>$data[$i]['STORE_LOGO']);
			$query = $GLOBALS["db"]->insert("store", $insertParam);
		}*/
		
		
		/*for data insertion of categories
		for($i=0; $i<count($data); $i++)
		{
			$insertParam = array("cat_id"=>$data[$i]['CAT_ID'],"cat_name"=>$data[$i]['CAT_NAME'],"cat_slug"=>$data[$i]['CAT_SLUG'],"cat_parent"=>$data[$i]['CAT_PARENT']);
			$query = $GLOBALS["db"]->insert("product_category", $insertParam);
		}*/
		
		/*for data insertion of coupons*/
		
		for($i=0; $i<count($data); $i++)
		{
			$insertParam = array("coupon_id"=>$data[$i]['CM_CID'],"seq"=>$data[$i]['SEQ'],"coupon_title"=>$data[$i]['TITLE'],"coupon_description"=>$data[$i]['DESCRIPTION'],"coupon_code"=>$data[$i]['COUPON'],"coupon_type"=>$data[$i]['TYPE'],"link"=>$data[$i]['LINK'],"aff_link"=>$data[$i]['AFF_LINK'], "validity_unix"=>$data[$i]['VALIDITY_UNIX'],"validity_date"=>$data[$i]['VALIDITY_DATE'],"discount"=>$data[$i]['DISCOUNT'],"final_cat_list"=>$data[$i]['FINAL_CAT_LIST'],"cats_text"=>str_replace('|,', ' | ', $data[$i]['CATS_TEXT']),"store_id"=>$data[$i]['STORE_ID'],"store_name"=>$data[$i]['STORE_NAME'],"created_date"=>$data[$i]['CREATED_DATE']);
			$query = $GLOBALS["db"]->insert("coupons", $insertParam);
		}
		echo $query;
	}
}
?>