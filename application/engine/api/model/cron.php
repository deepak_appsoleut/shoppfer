<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class cron_model {

	function cron()
	{
		$param = array("status"=>1);
		$data = $GLOBALS["db"]->select("SELECT TIMESTAMPDIFF(MINUTE, offer_status_time, NOW()) as date1, offer_status, offer_id FROM offers_temp WHERE offer_status_time IS NOT NULL HAVING date1 >-690", $param);
		$up = "false";
		if(count($data)>0)
		{
			// update offers table
			for($i=0; $i<count($data); $i++)
			{
				$upadateParam = array("offer_status"=>$data[$i]['offer_status']);
				$updateCondition = array("offer_id"=>$data[$i]['offer_id']);
				$up = $GLOBALS["db"]->update("offers", $upadateParam, $updateCondition);
				
				$upadateParam = array("offer_status_time"=>NULL);
				$updateCondition = array("offer_id"=>$data[$i]['offer_id']);
				$up = $GLOBALS["db"]->update("offers_temp", $upadateParam, $updateCondition);
			}
		}
		return $up;
	}
}
?>