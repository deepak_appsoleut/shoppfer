<?php
/*
 * File: Agent_Screen.php
 * Created By: Deepak Bhardwaj
 */

class kiosk_model 
{	
	
	//=================================================  isUserExist API Starts ==============================================//
	function isUserExist($param = null)
	{
		$request = json_encode($param);
		$paramData = array("login_api_via"=>$param['c_via'], "login_api_user_id"=>$param['c_api_id']);
		$selectData = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_verify AS c_verify, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, c.customer_dob AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic FROM customer c, login_api l WHERE l.login_api_customer_id = c.customer_id AND l.login_api_user_id =:login_api_user_id AND l.login_api_via =:login_api_via LIMIT 1", $paramData);
		if(count($selectData)>0)
		{
			if($selectData[0]['c_verify']==1)
			{
				$response = array("success"=>true);
				$response['code'] = 601;
				$response['c_id'] = $selectData[0]['c_id'];
				unset($selectData[0]['c_verify']);
				$response['data'] = $selectData;
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 602;
				$response['c_id'] = $selectData[0]['c_id'];
				$response['message'] = "User is not verified!";
			}
		}
		else
		{
			//check email adress exist or not
			if(trim($param['c_email'])!="")
			{
				$checkArray = array("customer_email"=>$param['c_email']);
				$selectDataCheck = $GLOBALS["db"]->select("SELECT customer_id FROM customer WHERE customer_email=:customer_email", $checkArray);
				if(count($selectDataCheck)>0)
				{
					$response = array("success"=>true);
					$response['code'] = 604;
					$response['message'] = "User with this email is already exist in our database!";	
				}
				else
				{
					//insert into customer table
					$paramData = array("customer_name"=>$param['c_name'], "customer_email"=>$param['c_email']);
					$insertCust = $GLOBALS["db"]->lastInsertNow("customer", $paramData, "customer_created_on");
										
					//insert into login api table			
					$paramDataApi = array("login_api_customer_id"=>$insertCust, "login_api_via"=>$param['c_via'], "login_api_user_id"=>$param['c_api_id'], "login_api_user_pic"=>$param['c_pic']);
					$insertApi = $GLOBALS["db"]->lastInsertNow("login_api", $paramDataApi, "login_api_created_on");	
					
					//insert into customer device relation table			
					$paramDataRel = array("device_id"=>$param['device_id'], "cust_id"=>$insertCust);
					$insertRel = $GLOBALS["db"]->insert("cust_device_relation", $paramDataRel);	
						
					$response = array("success"=>true);
					$response['code'] = 603;
					$response['c_id'] = $insertCust;
					$response['message'] = "User is not exist in our database!";
				}
			}
			/*
			else
			{
				//insert into customer table
				$paramData = array("customer_name"=>$param['c_name'], "customer_email"=>$param['c_email']);
				$insertCust = $GLOBALS["db"]->lastInsertNow("customer", $paramData, "customer_created_on");
				
				//insert into login api table			
				$paramDataApi = array("login_api_customer_id"=>$insertCust, "login_api_via"=>$param['c_via'], "login_api_user_id"=>$param['c_api_id'], "login_api_user_pic"=>$param['c_pic']);
				$insertApi = $GLOBALS["db"]->lastInsertNow("login_api", $paramDataApi, "login_api_created_on");	
				
				//insert into customer device relation table			
				$paramDataRel = array("device_id"=>$param['device_id'], "cust_id"=>$insertCust);
				$insertRel = $GLOBALS["db"]->insert("cust_device_relation", $paramDataRel);	
					
				$response = array("success"=>true);
				$response['code'] = 603;
				$response['c_id'] = $insertCust;
				$response['message'] = "User is not exist in our database!";
			}*/
		}
		
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
		
	}
	//=================================================  isUserExist API Ends ================================================//
	
	//=================================================  Regsitration API starts ==============================================//
	function registration($param = null)
	{
		$request = json_encode($param);
	
		$paramData = array("customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email'], "customer_id"=>$param['c_id']);
		$selectData = $GLOBALS["db"]->select("SELECT customer_id, customer_verify, customer_mobile, customer_email FROM customer WHERE (customer_mobile=:customer_mobile OR customer_email=:customer_email) AND customer_id !=:customer_id", $paramData);
		if(count($selectData)>0)
		{
			$response = array("success"=>true);
			$response['code'] = 605;
			if(($selectData[0]['customer_mobile']==$param['c_mob'])&&($selectData[0]['customer_mobile']==$param['c_email']))
			{
				$response['message'] = "Hey! This Email ID and Mobile Number is already registered with us! Want to Sign In?";
			}
			else if(($selectData[0]['customer_mobile']==$param['c_mob'])&&($selectData[0]['customer_email']!=$param['c_email']))
			{
				$response['message'] = "Hey! This Mobile Number is already registered with us! Please check the number you added!
";
			}
			else if(($selectData[0]['customer_mobile']!=$param['c_mob'])&&($selectData[0]['customer_email']==$param['c_email']))
			{
				$response['message'] = "Hey! This Email ID is already registered with us! Want to Sign In?";
			}
		}
		else
		{
			$paramDataUpdate = array("customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email'], "customer_gender"=>$param['c_gender'], "customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])));
			$condition = array("customer_id"=>$param['c_id']);
			$update = $GLOBALS["db"]->update("customer", $paramDataUpdate, $condition);
				if($update=="true")
				{
					$otp = rand('1111', '9999');
					$mobile = '91'.$param['c_mob'];
					$validate = self::sendSMS($mobile, $otp);
					$response = array("success"=>true);
					$response['code'] = 606;
					$response['c_id'] = $param['c_id'];
					$response['otp'] = $otp;
				}		
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	
	//=================================================  Regsitration API Ends ==============================================//
	
	//=================================================  Device INFO API starts ==============================================//
	function deviceInfo($param1 = null)
	{		
		$request = json_encode($param1);
		$param = array();
		foreach($param1 as $k=>$v)
		{
			$param[strtolower(str_replace('-', '_', $k))] = $v;
		}
		/*unset($param['host']);
		unset($param['connection']);
		unset($param['content_length']);
		unset($param['origin']);
		unset($param['user_agent']);
		unset($param['cache_control']);
		unset($param['postman_token']);
		unset($param['accept']);
		unset($param['accept_encoding']);
		unset($param['accept_language']);
		unset($param['cookie']);
		unset($param['content_type']);*/
		
		$selectParam = array("imei"=>$param['device_imei']);
		$selectData = $GLOBALS["db"]->select("SELECT device_id, device_version FROM device WHERE device_imei =:imei", $selectParam);
		if(count($selectData)>0)
		{
			if($selectData[0]['device_version']!=$param['device_version'])
			{
				$response = array("success"=>true);
				$response['code'] = 702;
				$response['message'] = "Please update application!";
			}
			else
			{
				$response = self::mpDetails($selectData[0]['device_id'], $param['device_location'], $param['c_id'], $param['city']);
				unset($param['device_location']);
				unset($param['c_id']);
				unset($param['city']);
				$condition = array("device_id"=>$selectData[0]['device_id']);
				$updateParam = array("device_imei"=>$param['device_imei'], "device_name"=>$param['device_name'], "device_version"=>$param['device_version'], "device_resolution"=>$param['device_resolution'], "device_os"=>$param['device_os'], "device_token"=>$param['device_token'], "device_phone_id"=>$param['device_phone_id']);
				$update = $GLOBALS["db"]->update("device", $updateParam, $condition);
			}
		}
		else
		{
			$location =  $param['device_location'];
			$cid = $param['c_id'];
			$city = $param['city'];
			unset($param['device_location']);
			unset($param['c_id']);
			unset($param['city']);
			$query = $GLOBALS["db"]->lastInsertNow("device", $param, "device_created_on");
			$response = self::mpDetails($query, $location, $cid, $city);
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	//=================================================  Device INFO API Ends ==============================================//
	
	//=================================================  Device NEW INFO API starts ==============================================//
	
	function deviceNewInfo($param1 = null)
	{
		$request = json_encode($param1);
		$param = array();
		foreach($param1 as $k=>$v)
		{
			$param[strtolower(str_replace('-', '_', $k))] = $v;
		}	
		
		if(isset($param['device_version']))
		{
			$selectParam = array("device_id"=>$param['device_id']);
			$selectData = $GLOBALS["db"]->select("SELECT device_version FROM device WHERE device_id =:device_id", $selectParam);
			if(count($selectData)>0)
			{
				if($selectData[0]['device_version']!=$param['device_version'])
				{
					$response = array("success"=>true);
					$response['code'] = 702;
					$response['message'] = "Please update application!";
					return $response;
				}
			}
		}
		$response = self::mpDetails($param['device_id'], $param['device_location'], $param['c_id'], $param['city']);
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	
	//=================================================  Device NEW INFO API Ends ==============================================//
	
	//=================================================  get Offer Details API starts ==============================================//
	
	function getOfferDetail($param = null)
	{
		$request = json_encode($param);
		$param['offer_id'] = $param[0];
		$param['c_id'] = $param[1];
		$paramData = array("offer_id"=>$param['offer_id'], "c_id"=>$param['c_id']);
		
		$selectData = $GLOBALS["db"]->select("SELECT m.*, o.*, GROUP_CONCAT(og.offer_image_details) as image_details, b.bookmark_id FROM mp_details m, offers_gallery og,  offers o LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE o.offer_mp_id = m.mp_details_id AND og.offer_id = o.offer_id AND o.offer_id =:offer_id GROUP BY o.offer_id", $paramData);
		
		$paramData = array("offer_id"=>$param['offer_id']);
		$selectReviewData = $GLOBALS["db"]->select("SELECT r.mp_review_id, r.mp_review_score, r.mp_review_comment, c.customer_name, c.customer_id, l.login_api_user_pic, rt.reply_text, m.mp_details_name FROM login_api l, offers o, mp_details m LEFT JOIN mp_reviews r ON r.mp_review_mp_details_id = m.mp_details_id LEFT JOIN customer c ON c.customer_id = r.mp_review_customer_id LEFT JOIN reply rt ON rt.reply_mp_review_id = r.mp_review_id WHERE l.login_api_customer_id = c.customer_id AND o.offer_mp_id = m.mp_details_id AND o.offer_id =:offer_id", $paramData);
		
		if(count($selectData)>0)
		{
			//count of coupons left
			$selectCountOffer = $GLOBALS["db"]->select("SELECT SUM(booking_qty) as totBooking FROM booking WHERE booking_offer_id=:offer_id AND booking_status!=2", $paramData);
			if(($selectData[0]['max_capping']!=NULL)&&($selectData[0]['max_capping']!=0))
			{
				$cap = $selectData[0]['max_capping'] - $selectCountOffer[0]['totBooking'];
				if($cap>0)
				{
					$capTxt = $cap.' Coupons Left';
				}
				else
				{
					$capTxt ='Sold Out';
				}
			}
			else
			{
				$capTxt = NULL;
			}
			
			$bannerArray = array();
			$reviewArray = array();
			$myReview = array();
			$dealArray = array();
			foreach($selectData as $a)
			{
				// array creation for banner part	
				  $explode = explode(',', $a['image_details']);
				  for($i=0; $i<count($explode); $i++)
				  {
					  array_push($bannerArray, APP_CRM_UPLOADS_PATH.$explode[$i]);
				  }
				  
				// array creation for deal part   
				  $dealArray[0]['o_title'] = $a['deal_name'];
				  if($a['offer_type_id'] ==1)
				  {
					  $a['offer_description'] = "Enjoy ".$a['offer_discount']."% on Full Menu";
				  }
				  $dealArray[0]['o_desc'] = $a['offer_description'];
				  $dealArray[0]['o_coupon'] = $capTxt;
				  $needs = array();
				 // $acceptCard = NULL;
				  //$veg = NULL;
				  //$bar = NULL;
				 
				  if($a['min_billing']!=NULL)
				  {
					  $Txt = 'Minimum bill should be '.$a['min_billing'];
					  array_push($needs, $Txt);
				  }
				  if($a['mp_accept_card']==1)
				  {
					  $Txt = 'Accept card';
					  array_push($needs, $Txt);
				  }
				  
				  
				  if($a['mp_details_category']==2)
				  {
					  if($a['mp_nonveg']==1)
					  {
						  $vegTxt = 'Veg/Non-Veg';
						  array_push($needs, $vegTxt);
					  }
					  else
					  {
						  $vegTxt = 'Veg';
						  array_push($needs, $vegTxt);
					  }
					  if($a['mp_bar_available']==1)
					  {
						  $bar = 'Bar available';
						  array_push($needs, $bar);
					  }
					  else
					  {
						  $bar = 'Bar not available';
						  array_push($needs, $bar);
					  }
				  }
				  
				  
				  $dealArray[0]['needs'] = $needs;
			}
			
			 
				//array creation for reviews
				if(count($selectReviewData)>0)
				{
					$i=0;
					foreach($selectReviewData as $b)
					{
						if($b['mp_review_id']!=NULL)
						{
							if($param['c_id']!=NULL)
							{
								if($b['customer_id']!=$param['c_id'])
								{
									$reviewArray[$i]['cust_name'] = $b['customer_name'];
									$reviewArray[$i]['c_id'] = $b['customer_id'];
									$reviewArray[$i]['score'] = $b['mp_review_score']; 
									$reviewArray[$i]['comment'] = $b['mp_review_comment']; 
									$reviewArray[$i]['c_pic'] = $b['login_api_user_pic'];
									$reviewArray[$i]['reply'] = $b['reply_text']; 
									$reviewArray[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
								else
								{
									$myReview[$i]['cust_name'] = $b['customer_name'];
									$myReview[$i]['c_id'] = $b['customer_id'];
									$myReview[$i]['score'] = $b['mp_review_score']; 
									$myReview[$i]['comment'] = $b['mp_review_comment']; 
									$myReview[$i]['c_pic'] = $b['login_api_user_pic'];
									$myReview[$i]['reply'] = $b['reply_text']; 
									$myReview[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
							}
							else
							{
								$reviewArray[$i]['cust_name'] = $b['customer_name'];
								$reviewArray[$i]['c_id'] = $b['customer_id'];
								$reviewArray[$i]['score'] = $b['mp_review_score']; 
								$reviewArray[$i]['comment'] = $b['mp_review_comment']; 
								$reviewArray[$i]['c_pic'] = $b['login_api_user_pic']; 
								$reviewArray[$i]['reply'] = $b['reply_text']; 
								$reviewArray[$i]['mp_name'] = $b['mp_details_name']; 
								$i++;
							}
						}
					}
				}
				
				$response = array("success"=>true);
				$response['code'] = 608;
				$response['mp_id'] = $selectData[0]['mp_details_id'];
				$response['o_id'] = $selectData[0]['offer_id'];
				$response['bookmark_id'] = $selectData[0]['bookmark_id'];
				$response['mp_name'] = $selectData[0]['mp_details_name'];
				$response['mp_opentime'] = $selectData[0]['mp_opentime'];
				$response['mp_closetime'] = $selectData[0]['mp_closetime'];
				
				// array creation for contact part 
				$response['mp_address1'] = $selectData[0]['mp_details_address'];
				$response['mp_city'] = $selectData[0]['mp_details_city'];
				$response['mp_longitude'] = $selectData[0]['mp_details_longitude'];
				$response['mp_latitude'] = $selectData[0]['mp_details_latitude'];
				
				if($selectData[0]['mp_details_contact']==NULL)
				{
					$response['mp_contact'] = '+91 '.$selectData[0]['mp_details_phone'];
				}
				else
				{
					$response['mp_contact'] = '+91 '.$selectData[0]['mp_details_contact'];
				}
				  
				// array creation for details part 
				$response['mp_recommend'] = $selectData[0]['mp_details_recommend'];
				$response['mp_description'] = $selectData[0]['mp_details_description'];
				
				$response['gallery'] = array_values($bannerArray);
				$response['review'] = array_values($reviewArray);
				$response['myReview'] = array_values($myReview);
				$response['deal'] = array_values($dealArray);
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 609;
			$response['message'] = "Sorry! Offer is not available!";
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	
	//================================================= get Offer Details API Ends ==============================================//
		
	function mpDetails($device_id,$location, $c_id, $city)
	{
		//return $device_id.','.$location.','.$c_id.','.$city;
		
		$category_array1 = array();
		$category_array = array();
		$banner = array();
		$device_lat = 0;
		$device_long = 0;
		$query = "";
		$param = array("status"=>1, "c_id"=>$c_id);
		
		if($location=="null")
		{
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status AND UPPER(mp.mp_details_city) ='".$city."' GROUP BY mp_details_id", $param);
		}
		else
		{
			$explode = explode(',', $location);
			$device_lat = $explode[0];
			$device_long = $explode[1];
			if(($city=="null")||($city =="Nearby"))
			{
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
			else
			{
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status AND UPPER(mp.mp_details_city) ='".$city."' GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
		}
		
		if(count($data)>0)
		{
		$response = array("success"=>true);
		$response['code'] = 607;
		$response['device_id'] = $device_id;
	
		$trending[] = $data;
		$category_array = array();
					foreach($data as $a){
						$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
						$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
						$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
						if($a['offer_type_id'] == "1")
						{
							$a['o_desc'] = "Enjoy ".$a['o_disc']."% on Full Menu";
						}
						unset($a['offer_type_id']);
						unset($a['o_disc']);
						$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
						$category_array[$a['mp_cat']]['data'][]= $a;
					}
					
		//storing array into all		
		$tot = 0;
					foreach($trending as $a1){
						foreach($a1 as $a){
								$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
								$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
								$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
								if($a['offer_type_id'] == "1")
								{
									$a['o_desc'] = "Enjoy ".$a['o_disc']."% on Full Menu";
								}
								unset($a['o_disc']);
								unset($a['offer_type_id']);
								$category_array1[]= $a;
								if($tot<4)
								{
									if($a['is_trending'] != 0)
									{
										$tot++;
										$banner[]= $a;
									}
								}
						}
					}
					
		$response['all'] = array_values($category_array1);
		$response['banner'] = array_values($banner);
		$response['category'] = array_values($category_array);
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 608;
			$response['all'] = array_values($category_array1);
			$response['banner'] = array_values($banner);
			$response['category'] = array_values($category_array);
		}
		return $response;
	}
	
	//=================================================  availOffer API starts ==============================================//
	
	function availOffer($param = null)
	{
		$request = json_encode($param);
		//to check mp or offer exist or not
		$myParam = array("o_id"=>$param['o_id'],"mp_id"=>$param['mp_id'],"status"=>1);
		$myData = $GLOBALS["db"]->select("SELECT o.offer_id, o.max_capping FROM offers o, mp_details m WHERE o.offer_id =:o_id AND m.mp_details_id =:mp_id AND o.offer_mp_id = m.mp_details_id AND o.offer_status=:status AND m.mp_status =:status", $myParam);
		
		if(count($myData)>0)
		{
			$paramData = array("offer_id"=>$param['o_id']);
			$selectCountOffer = $GLOBALS["db"]->select("SELECT SUM(booking_qty) as totBooking FROM booking WHERE booking_offer_id=:offer_id AND booking_status!=2", $paramData);
			if($selectCountOffer[0]['totBooking']<$myData['0']['max_capping'])
			{			
				$checkParam = array("booking_customer_id"=>$param['c_id'], "status"=>"0");
				$select = $GLOBALS["db"]->select("SELECT booking_id, booking_coupon_code, booking_offer_id, booking_mp_id FROM booking WHERE booking_customer_id =:booking_customer_id AND booking_status =:status LIMIT 1", $checkParam);
				if(count($select)>0)
				{
					if(($select[0]['booking_offer_id']==$param['o_id'])&&($select[0]['booking_mp_id']==$param['mp_id']))
					{
						$response = array("success"=>true);
						$response['code'] = 613;
						$response['message'] = "We know you love this offer, because you already have an active coupon for this offer! Check your Coupons section!";
					}
					else
					{
						$paramData = array("o_id"=>$select[0]['booking_offer_id'], "mp_id"=>$select[0]['booking_mp_id']);
						$selectMy = $GLOBALS["db"]->select("SELECT m.mp_details_name, o.deal_name, o.offer_discount, o.offer_type_id, o.offer_description, m.mp_details_address, m.mp_details_city, mp_details_latitude, mp_details_longitude FROM mp_details m, offers o WHERE m.mp_details_id =o.offer_mp_id AND o.offer_id =:o_id AND m.mp_details_id =:mp_id LIMIT 1", $paramData);
						if($selectMy[0]['offer_type_id']==1)
						{
							$selectMy[0]['offer_description'] = "Enjoy ".$selectMy[0]['offer_discount']."% on Full Menu";
						}				
						$response = array("success"=>true);
						$response['code'] = 612;
						$response['booking_id'] = $select[0]['booking_id'];
						$response['voucher'] = $select[0]['booking_coupon_code'];
						$response['mp_name'] = $selectMy[0]['mp_details_name'];
						$response['o_title'] = $selectMy[0]['deal_name'];
						$response['o_desc'] = $selectMy[0]['offer_description'];
						$response['mp_address'] = $selectMy[0]['mp_details_address'].','.$selectMy[0]['mp_details_city'];
						$response['mp_latitude'] = $selectMy[0]['mp_details_latitude'];
						$response['mp_longitude'] = $selectMy[0]['mp_details_longitude'];
					}
				}
				else
				{
					$randomString = self::checkUniqe();
					$address = self::getMapDetails($param["location"]);
					$partial = explode(',', $address);
					if(!isset($partial[1]))
					{
						$partial[1] = NULL;
					}
					if(!isset($partial[2]))
					{
						$partial[2] = NULL;
					}
					$full = $partial[0].','.$partial[1].','.$partial[2];
					$paramData = array("booking_offer_id"=>$param['o_id'],"booking_customer_id"=>$param['c_id'],"booking_mp_id"=>$param['mp_id'], "booking_coupon_code"=>$randomString, "booking_location"=>$param["location"],"booking_full_address"=>$address,"booking_partial_address"=>$full);
					
					$insert = $GLOBALS["db"]->lastInsertNow("booking", $paramData, "booking_created_on");
					if($insert)
					{
						$paramData1 = array("o_id"=>$param['o_id'], "mp_id"=>$param['mp_id']);
						$selectMy = $GLOBALS["db"]->select("SELECT m.mp_details_name, o.deal_name, o.offer_discount, o.offer_description, o.offer_type_id, m.mp_details_address, m.mp_details_city, mp_details_latitude, mp_details_longitude FROM mp_details m, offers o WHERE m.mp_details_id =o.offer_mp_id AND o.offer_id =:o_id AND m.mp_details_id =:mp_id LIMIT 1", $paramData1);
						if($selectMy[0]['offer_type_id']==1)
						{
							$selectMy[0]['offer_description'] = "Enjoy ".$selectMy[0]['offer_discount']."% on Full Menu";
						}
						$otp = $randomString;
						$mobile = '91'.$param['c_mob'];
						//$validate = self::sendSMS($mobile, $otp);
						$response = array("success"=>true);
						$response['code'] = 610;
						$response['voucher'] = $otp;
						$response['booking_id'] = $insert;
						$response['mp_name'] = $selectMy[0]['mp_details_name'];
						$response['o_title'] = $selectMy[0]['deal_name'];
						$response['o_desc'] = $selectMy[0]['offer_description'];
						$response['mp_address'] = $selectMy[0]['mp_details_address'].','.$selectMy[0]['mp_details_city'];
						$response['mp_latitude'] = $selectMy[0]['mp_details_latitude'];
						$response['mp_longitude'] = $selectMy[0]['mp_details_longitude'];
					}
					else
					{
						$response = array("success"=>true);
						$response['code'] = 611;
						$response['message'] = "Oh No! Our server is acting funny! Can you please try again in a few minutes?";
					}
				}
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 631;
				$response['message'] = "Coupons are out of stock!";
			}
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 630;
			$response['message'] = "Sorry! Offer is not available.";
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	function checkUniqe()
	{
		$string = new math();
		$selectParam = array();
		$randomString = $string->randomString();
		
		$selectData = $GLOBALS["db"]->select("SELECT booking_id FROM booking WHERE booking_status = 0 AND booking_coupon_code =".$randomString, $selectParam);
		if(count($selectData)>0)
		{
			self::checkUniqe();
		}
		else
		{
			return strtoupper($randomString);
		}
	}
	function getMapDetails($location)
	{
		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$location.'&sensor=false');
		$geo = json_decode($geo, true);
		if ($geo['status'] = 'OK') 
		{
		  if(isset($geo['results'][0]['formatted_address']))
		  {
		  		$address = $geo['results'][0]['formatted_address'];
		  }
		  else
		  {
			  $address = NULL;
		  }
		}
		else
		{
			$address = NULL;
		}
		return $address;
	}
	//=================================================  checkUniqe API Ends ==============================================//
	
	//=================================================  cancelAvailOffer starts ==============================================//
	
	function cancelAvailOffer($param = null)
	{
		$request = json_encode($param);
		
		$condition = array("booking_id"=>$param['booking_id']);
		$paramUpdate = array("booking_status"=>2);
		$update = $GLOBALS["db"]->update("booking", $paramUpdate, $condition);
		if($update=="true")
		{
			$response = array("success"=>true);
			$response['code'] = 624;
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 625;
			$response['message'] = "This offer loves you! But if you don't like it, then we must delete it! Can you please try again?";
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	
	//=================================================  CheckwriteReview API starts ==============================================//
	function checkWriteReview($param = null)
	{
		$request = json_encode($param);
		$selectParam = array("customer_id"=>$param['c_id'],"mp_id"=>$param['mp_id']);
		$selectData = $GLOBALS["db"]->select("SELECT mp_review_score, mp_review_comment, mp_review_id FROM mp_reviews WHERE mp_review_customer_id =:customer_id AND mp_review_mp_details_id =:mp_id LIMIT 1", $selectParam);
		if(count($selectData)>0)
		{
			$response = array("success"=>true);
			$response['code'] = 614;
			$response['score'] = $selectData[0]['mp_review_score'];
			$response['comment'] = $selectData[0]['mp_review_comment'];
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 615;			
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	//=================================================  Write Review API starts ==============================================//
	
	//=================================================  writeReview API starts ==============================================//
	function writeReview($param = null)
	{
		$request = json_encode($param);
		
		$selectParam = array("customer_id"=>$param['c_id'],"mp_id"=>$param['mp_id']);
		$selectData = $GLOBALS["db"]->select("SELECT mp_review_id FROM mp_reviews WHERE mp_review_customer_id =:customer_id AND 	mp_review_mp_details_id =:mp_id", $selectParam);
		if(count($selectData)>0)
		{
			// update review
			$condition = array("mp_review_customer_id"=>$param['c_id'],"mp_review_mp_details_id"=>$param['mp_id']);
			$updateParam = array("mp_review_comment"=>$param['comment'],"mp_review_score"=>$param['score']);
			$update = $GLOBALS["db"]->update("mp_reviews", $updateParam, $condition);
			if($update=="true")
			{
				$response = self::getReviews($param['mp_id'], $param['c_id']);
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 613;
				$response['message'] = "Oops! Your review fell through our hands, Sorry! Can you please review it again?";
			}
		}
		else
		{
			//insert review
			$insertParam = array("mp_review_comment"=>$param['comment'],"mp_review_score"=>$param['score'], "mp_review_customer_id"=>$param['c_id'],"mp_review_mp_details_id"=>$param['mp_id']);
			$insert = $GLOBALS["db"]->lastInsertNow('mp_reviews', $insertParam, 'mp_review_created_on');
			if($insert)
			{
				$response = self::getReviews($param['mp_id'], $param['c_id']);
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 614;
				$response['message'] = "Oops! Your review fell through our hands, Sorry! Can you please review it again?";
			}
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	//=================================================  Write Review API ends ==============================================//
	
	//=================================================   get Review list API starts ==============================================//
	function getReviews($mp_id, $cid)
	{
		$paramData = array("mp_id"=>$mp_id);
		$reviewArray = array();
		$myReview = array();
		$selectReviewData = $GLOBALS["db"]->select("SELECT r.mp_review_id, r.mp_review_score, r.mp_review_comment, c.customer_name, c.customer_id, l.login_api_user_pic, rt.reply_text, m.mp_details_name FROM login_api l, mp_details m LEFT JOIN mp_reviews r ON r.mp_review_mp_details_id = m.mp_details_id LEFT JOIN customer c ON c.customer_id = r.mp_review_customer_id LEFT JOIN reply rt ON rt.reply_mp_review_id = r.mp_review_id WHERE l.login_api_customer_id = c.customer_id AND m.mp_details_id =:mp_id", $paramData);
		
		//array creation for reviews
				if(count($selectReviewData)>0)
				{
					$i=0;
					foreach($selectReviewData as $b)
					{
						if($b['mp_review_id']!=NULL)
						{
							if($b['customer_id']!=$cid)
								{
									$reviewArray[$i]['cust_name'] = $b['customer_name'];
									$reviewArray[$i]['c_id'] = $b['customer_id'];
									$reviewArray[$i]['score'] = $b['mp_review_score']; 
									$reviewArray[$i]['comment'] = $b['mp_review_comment']; 
									$reviewArray[$i]['c_pic'] = $b['login_api_user_pic']; 
									$reviewArray[$i]['reply'] = $b['reply_text']; 
									$reviewArray[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
								else
								{
									$myReview[$i]['cust_name'] = $b['customer_name'];
									$myReview[$i]['c_id'] = $b['customer_id'];
									$myReview[$i]['score'] = $b['mp_review_score']; 
									$myReview[$i]['comment'] = $b['mp_review_comment']; 
									$myReview[$i]['c_pic'] = $b['login_api_user_pic']; 
									$myReview[$i]['reply'] = $b['reply_text']; 
									$myReview[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
						}
					}
				}
		$response = array("success"=>true);
		$response['code'] = 612;		
		$response['review'] = array_values($reviewArray);
		$response['myReview'] = array_values($myReview);
		return $response;
	}
	//=================================================  get Review list API Ends ==============================================//
	
	//=================================================  bookmark API starts ==============================================//
	function bookmark($param = null)
	{
		$request = json_encode($param);
		
		$selectParam = array("bookmark_customer_id"=>$param['c_id'],"bookmark_offer_id"=>$param['o_id']);
		$selectData = $GLOBALS["db"]->select("SELECT bookmark_id FROM bookmark WHERE bookmark_customer_id =:bookmark_customer_id AND bookmark_offer_id =:bookmark_offer_id LIMIT 1", $selectParam);
		if(count($selectData)>0)
		{
			$seleteArray = array("bookmark_id"=>$selectData[0]['bookmark_id']);
			$result = $GLOBALS["db"]->deleteQuery('bookmark',$seleteArray);	
			if($result)
			{
				$value = NULL;
			}
		}
		else
		{
			$result = $GLOBALS["db"]->lastInsertNow('bookmark', $selectParam, 'bookmark_created_on');
			if($result>0)
			{
				$value = $result;
			}
		}
		
		if($result>0)
		{
			$response = array("success"=>true);
			$response['bookmark_id'] = $value;
			$response['code'] = 616;
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 617;
			$response['message'] = "There is some error while adding bookmark!  Please try again";
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	//=================================================  bookmark API starts ==============================================//
	
	//=================================================  getFavouriteList API starts ==============================================//
	function getFavouriteList($param = null)
	{
		$request = json_encode($param);
		$validate = self::filterData($customer = $param['c_id'], $device_lat = $param['latitude'], $device_long = $param['longitude'], $apiName = "getFavourite");
		$jsonInsert = self::capture_json($request, json_encode($validate));
		return $validate;
	}
	//=================================================  getFavouriteList API starts ==============================================//
	
	//=================================================  filterApiCIty API starts ==============================================//
	function filterApiCity($param = null)
	{
		$request = json_encode($param);
		$validate = self::getfilterData($customer = $param['c_id'], $city = $param['city'], $distance = null,  $device_lat = $param['latitude'], $device_long = $param['longitude'], $searchText = null, $apiName = "city");
		$jsonInsert = self::capture_json($request, json_encode($validate));
		return $validate;
	}
	//=================================================  filterApiCIty API starts ==============================================//
	
	//=================================================  filterApiDistance API starts ==============================================//
	function filterApiDistance($param = null)
	{
		$request = json_encode($param);
		$validate = self::getfilterData($customer = $param['c_id'], $city = null, $distance = $param['distance'],  $device_lat = $param['latitude'], $device_long = $param['longitude'], $searchText = null, $apiName = "distance");
		$jsonInsert = self::capture_json($request, json_encode($validate));
		return $validate;
	}
	//=================================================  filterApiDistance API ends ==============================================//
	
	//=================================================  filterApiSearch API starts ==============================================//
	function filterApiSearch($param = null)
	{
		$request = json_encode($param);
		$validate = self::getfilterData($customer = $param['c_id'], $city = null, $distance = null,  $device_lat = $param['latitude'], $device_long = $param['longitude'], $searchText = $param['searchTxt'], $apiName = "search");
		$jsonInsert = self::capture_json($request, json_encode($validate));
		return $validate;
	}
	//=================================================  filterApiSearch API Ends ==============================================//
	
		
	function filterData($customer, $device_lat, $device_long, $apiName)
	{
		// to get bookmark list
		if($apiName=='getFavourite')
		{
			$param = array("c_id"=>$customer, "status"=>1);	
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id INNER JOIN bookmark b ON o.offer_id = b.bookmark_offer_id INNER JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND b.bookmark_customer_id =:c_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			if(count($data)>0)
			{	
				$category_array = array();
							foreach($data as $a){
								$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
								$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
								$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
								if($a['offer_type_id'] == 1)
								{
									$a['o_desc'] = "Enjoy ".$a['o_disc']."% on Full Menu";
								}
								unset($a['o_disc']);
								unset($a['offer_type_id']);
								$category_array[]= $a;
							}			
				$response = array("success"=>true);
				$response['code'] = 618;
				$response['data'] = array_values($category_array);
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 619;
				$response['message'] = "No Bookmark Exist";
			}
			return $response;
		}	
	}
	
	function getfilterData($customer, $city, $distance,  $device_lat, $device_long, $searchText, $apiName)
	{
		$category_array1 = array();
		$category_array = array();
		$banner = array();
		//filter by city
		if($apiName=='city')
		{
			$param = array("city"=>strtoupper($city), "status"=>1, "c_id"=>$customer);				
			//$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND mp_details_city =:city AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			
			if($city =="Nearby")
			{
				$param = array("status"=>1, "c_id"=>$customer);	
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
			else
			{
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status AND UPPER(mp.mp_details_city) =:city GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
		}
		//filter by distance
		if($apiName=='distance')
		{
			if($distance==0)
			{
				$query1 = "";
			}
			else
			{
				$query1 = " HAVING mp_dist <=".$distance;
			}
			$param = array("status"=>1, "c_id"=>$customer);
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status GROUP BY mp_details_id ".$query1." ORDER BY mp_dist ASC", $param);
		}
		
		//filter by search Txt
		if($apiName=='search')
		{
			$query1 = " AND (mp.mp_details_name LIKE '%".$searchText."%' OR mp.mp_details_city LIKE '".$searchText."' OR mp.mp_details_address LIKE '%".$searchText."%')";
			$param = array("status"=>1, "c_id"=>$customer);
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status ".$query1." GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
		}
		
		if(count($data)>0)
		{
		$response = array("success"=>true);
		$response['code'] = 607;
			
		$trending[] = $data;
		$category_array = array();
					foreach($data as $a){
						$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
						$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
						$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
						if($a['offer_type_id'] == 1)
						{
							$a['o_desc'] = "Enjoy ".$a['o_disc']."% on Full Menu";
						}
						unset($a['offer_type_id']);
						unset($a['o_disc']);
						$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
						$category_array[$a['mp_cat']]['data'][]= $a;
					}
					
		//storing array into all			
		$category_array1 = array();
		$tot = 0;
		$banner = array();
					foreach($trending as $a1){
						foreach($a1 as $a){
								$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
								$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
								$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
								if($a['offer_type_id'] == 1)
								{
									$a['o_desc'] = "Enjoy ".$a['o_disc']."% on Full Menu";
								}
								unset($a['offer_type_id']);
								unset($a['o_disc']);
								$category_array1[]= $a;
								if($tot<6)
								{
									if($a['is_trending'] != 0)
									{
										$tot++;
										$banner[]= $a;
									}
								}
						}
					}
					
		$response['all'] = array_values($category_array1);
		$response['banner'] = array_values($banner);
		$response['category'] = array_values($category_array);	
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 608;
			$response['all'] = array_values($category_array1);
			$response['banner'] = array_values($banner);
			$response['category'] = array_values($category_array);
		}
		
		return $response;
	}
	
	/*
	
	function login($param = null)
	{
		$paramData = array("customer_name"=>$param['c_name'], "customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email'], "customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])));
		$query1 = $GLOBALS["db"]->lastInsertNow("customer", $paramData, "customer_created_on");
		
		//insert into device customer relation
		$paramRel = array("cust_id"=>$query1, "device_id"=>$param['device_id']);
		$query2 = $GLOBALS["db"]->insert("cust_device_relation", $paramRel);
		
		//insert into login api relation
		$paramApi = array("login_api_customer_id"=>$query1, "login_api_user_id"=>$param['api_id'],"login_api_via"=>$param['via'], "login_api_user_pic"=>$param['pic']);
		$query3 = $GLOBALS["db"]->lastInsertNow("login_api", $paramApi, 'login_api_created_on');
		
		if($query3>0)
		{
			$otp = rand('1111', '9999');
			$mobile = '91'.$param['c_mob'];
			$validate = self::sendSMS($mobile, $otp);
			$response = array("success"=>true);
			$response['code'] = 601;
			$response['c_id'] = $query1;
			$response['otp'] = $otp;
		}	
		return $response;
	}
	
	function login1($param = null)
	{
		// to check mobile number or email address is exist or not
		$selectParam = array("customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email']);
		$selectData = $GLOBALS["db"]->select("SELECT customer_id, customer_verify FROM customer WHERE (customer_mobile=:customer_mobile OR customer_email=:customer_email)", $selectParam);
		if(count($selectData)>0)
		{
			if($selectData[0]['customer_verify']==0)
			{
				// means customer is not verified
			}
			else
			{
				
			}
			return 1;
		}
		else
		{
			return 0;
		}
		$data = $GLOBALS["db"]->select("SELECT");
		$paramData = array("customer_name"=>$param['c_name'], "customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email'], "customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])));
		$query1 = $GLOBALS["db"]->lastInsertNow("customer", $paramData, "customer_created_on");
		
		//insert into device customer relation
		$paramRel = array("cust_id"=>$query1, "device_id"=>$param['device_id']);
		$query2 = $GLOBALS["db"]->insert("cust_device_relation", $paramRel);
		
		//insert into login api relation
		$paramApi = array("login_api_customer_id"=>$query1, "login_api_user_id"=>$param['api_id'],"login_api_via"=>$param['via'], "login_api_user_pic"=>$param['pic']);
		$query3 = $GLOBALS["db"]->lastInsertNow("login_api", $paramApi, 'login_api_created_on');
		
		if($query3>0)
		{
			$otp = rand('1111', '9999');
			$mobile = '91'.$param['c_mob'];
			$validate = self::sendSMS($mobile, $otp);
			$response = array("success"=>true);
			$response['code'] = 601;
			$response['c_id'] = $query1;
			$response['otp'] = $otp;
		}	
		return $response;
	}
	*/
	
	function SendSMS($mobile, $otp)
	{
		$url = 'http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=355074&username=9999999574&password=ddgdt&senderid=ODDEVN&To='.$mobile.'&Text=Hi!%20Welcome%20to%20Odd%20Even%20Ride!%20Your%20OTP%20to%20register%20is%20'.$otp;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_exec($ch);
		curl_close($ch);	
		$sendParam = array('sms_number'=>$mobile);			
		$insert = $GLOBALS["db"]->insert("sms_sent", $sendParam);
		return $insert;
	}
	
	/**
	 * API for Sign IN from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function otpVerify($param = null)
	{	
		$request = json_encode($param);
		
		$condition = array("customer_id"=>$param['c_id']);
		$updateData = array("customer_status"=>1, "customer_verify"=>1);
		$update = $GLOBALS["db"]->update("customer", $updateData, $condition);
		if($update=="true")
		{
			$response = array("success"=>true);
			$response['code'] = 601;
			$data = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, c.customer_dob AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic FROM customer c, login_api l WHERE l.login_api_customer_id = c.customer_id AND c.customer_id =:customer_id", $condition);
			$response['data'] = $data;
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	function statusUpdate($param = null)
	{
		$response = array("success"=>true);
		$response['code'] = 601;
		$condition = array("customer_mobile"=>$param['c_mob']);
		$data = $GLOBALS["db"]->deleteQuery("customer", $condition);
		if($data)
		{
			$response = array("success"=>true);
			$response['code'] = 601;
		}
		else
		{
			$response['success'] = true;
			$response['code'] = 707;
			$response['message'] = "There is some error while updating your status";
		}
		return $response;
	}
	function resendOTP($param = null)
	{
		$request = json_encode($param);
		
		$response = array("success"=>true);
		// means give data list
		$response['code'] = 607;
		// send sms
		$otp = rand('1111', '9999');
		$mobile = '91'.$param['c_mob'];
		$validate = self::sendSMS($mobile, $otp);
		$response['otp'] = $otp;
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	function profileUpdate($param = null)
	{
		$request = json_encode($param);
		$paramData = array("id"=>$param['c_id']);
		$selectData = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, c.customer_dob AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic FROM customer c, login_api l WHERE l.login_api_customer_id = c.customer_id AND c.customer_id =:id LIMIT 1", $paramData);
			if($selectData[0]['c_mob']!= $param['c_mob'])
			{	
				$paramDataMob = array("id"=>$param['c_id'], "mob"=>$param['c_mob']);
				$selectMob = $GLOBALS["db"]->select("SELECT customer_id FROM customer WHERE customer_id!=:id AND customer_mobile =:mob", $paramDataMob);
				if(count($selectMob)>0)
				{
					$response = array("success"=>true);
					$response['code'] = 625;
					$response['message'] = "User already exist with same Mobile!";
				}
				else
				{
					$response = array("success"=>true);
					$response['code'] = 622;
					// send sms
					$otp = rand('1111', '9999');
					$mobile = '91'.$param['c_mob'];
					$response['c_id'] = $param['c_id'];
					$validate = self::sendSMS($mobile, $otp);
					$response['otp'] = $otp;
				}
			}
			else
			{
				$paramData = array("customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])), "customer_name"=>$param['c_name']);
				$condition = array("customer_id"=>$param['c_id']);
				$update = $GLOBALS["db"]->update("customer", $paramData, $condition);
				if($update=="true")
				{
					$response = array("success"=>true);
					$response['code'] = 623;
					$response['c_id'] = $selectData[0]['c_id'];
					$selectData[0]['c_dob'] = $param['c_dob'];
					$selectData[0]['c_name'] = $param['c_name'];
					$response['data'] = $selectData;
				}
				else
				{
					$response = array("success"=>true);
					$response['code'] = 624;
					$response['message'] = "Oops! Our server needs a wake up call! Data could not be updated, can you try again?";
				}
			}
			$jsonInsert = self::capture_json($request, json_encode($response));
			return $response;
	}
	function profileUpdateVerify($param = null)
	{
		$request = json_encode($param);
		
		$paramData = array("customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])), "customer_name"=>$param['c_name'], "customer_mobile"=>$param['c_mob']);
		$condition = array("customer_id"=>$param['c_id']);
		$update = $GLOBALS["db"]->update("customer", $paramData, $condition);
		if($update=="true")
		{
			$response = array("success"=>true);
			$response['code'] = 626;
			
			$paramData = array("id"=>$param['c_id']);
			$selectData = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, c.customer_dob AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic FROM customer c, login_api l WHERE l.login_api_customer_id = c.customer_id AND c.customer_id =:id LIMIT 1", $paramData);
			
			$response['c_id'] = $selectData[0]['c_id'];
			$response['data'] = $selectData;	
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 627;
			$response['message'] = "Data not updated!";
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	
	function getCoupon($param = null)
	{
		$request = json_encode($param);
		
		$checkParam = array("booking_customer_id"=>$param['c_id'], "status"=>"0");
		$select = $GLOBALS["db"]->select("SELECT booking_id, booking_coupon_code, booking_offer_id, booking_mp_id  FROM booking WHERE booking_customer_id =:booking_customer_id AND booking_status =:status LIMIT 1", $checkParam);
		if(count($select)>0)
		{
			$paramData = array("o_id"=>$select[0]['booking_offer_id'], "mp_id"=>$select[0]['booking_mp_id']);
			$selectMy = $GLOBALS["db"]->select("SELECT m.mp_details_name, o.deal_name, o.offer_discount, o.offer_type_id, o.offer_description, m.mp_details_address, m.mp_details_city, mp_details_latitude, mp_details_longitude FROM mp_details m, offers o WHERE m.mp_details_id =o.offer_mp_id AND o.offer_id =:o_id AND m.mp_details_id =:mp_id LIMIT 1", $paramData);
			if($selectMy[0]['offer_type_id']==1)
			{
				$selectMy[0]['offer_description'] = "Enjoy ".$selectMy[0]['offer_discount']."% on Full Menu";
			}				
			$response = array("success"=>true);
			$response['code'] = 614;
			$response['booking_id'] = $select[0]['booking_id'];
			$response['voucher'] = $select[0]['booking_coupon_code'];
			$response['mp_name'] = $selectMy[0]['mp_details_name'];
			$response['o_title'] = $selectMy[0]['deal_name'];
			$response['o_desc'] = $selectMy[0]['offer_description'];
			$response['mp_address'] = $selectMy[0]['mp_details_address'].','.$selectMy[0]['mp_details_city'];
			$response['mp_latitude'] = $selectMy[0]['mp_details_latitude'];
			$response['mp_longitude'] = $selectMy[0]['mp_details_longitude'];
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 615;
			$response['message'] = "No coupon exist!";
		}
		$jsonInsert = self::capture_json($request, json_encode($response));
		return $response;
	}
	function capture_json($json_request, $json_response)
	{
		$array = array("json_request"=>$json_request, "json_response"=>$json_response);
		$insertCust = $GLOBALS["db"]->lastInsertNow("api_request", $array, "date");
	}
}
?>