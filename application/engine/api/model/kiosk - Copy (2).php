<?php
/*
 * File: Agent_Screen.php
 * Created By: Deepak Bhardwaj
 */

class kiosk_model 
{	
	
	//=================================================  isUserExist API Starts ==============================================//
	function isUserExist($param = null)
	{
		$request = json_encode($param);
		$paramData = array("login_api_via"=>$param['c_via'], "login_api_user_id"=>$param['c_api_id']);
		$selectData = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_verify AS c_verify, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, DATE_FORMAT( c.customer_dob,  '%M %e %Y' ) AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic, COUNT(DISTINCT m.mp_review_id) as reviews, COUNT(DISTINCT b.booking_id) AS redeemed FROM login_api l, customer c LEFT JOIN mp_reviews m ON c.customer_id = m.mp_review_customer_id LEFT JOIN booking b ON c.customer_id = b.booking_customer_id AND b.booking_status =1 WHERE l.login_api_customer_id = c.customer_id AND l.login_api_user_id =:login_api_user_id AND l.login_api_via =:login_api_via GROUP BY c.customer_id LIMIT 1", $paramData);
		if(count($selectData)>0)
		{
			if($selectData[0]['c_verify']==1)
			{
				$response = array("success"=>true);
				$response['code'] = 601;
				$response['c_id'] = $selectData[0]['c_id'];
				unset($selectData[0]['c_verify']);
				$response['data'] = $selectData;
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 602;
				$response['c_id'] = $selectData[0]['c_id'];
				$response['message'] = "User is not verified!";
			}
		}
		else
		{
			//check email adress exist or not
			if(trim($param['c_email'])!="")
			{
				$checkArray = array("customer_email"=>$param['c_email']);
				$selectDataCheck = $GLOBALS["db"]->select("SELECT customer_id FROM customer WHERE customer_email=:customer_email", $checkArray);
				if(count($selectDataCheck)>0)
				{
					$response = array("success"=>true);
					$response['code'] = 604;
					$response['message'] = "User with this email is already exist in our database!";	
				}
				else
				{
					//insert into customer table
					$paramData = array("customer_name"=>$param['c_name'], "customer_email"=>$param['c_email']);
					$insertCust = $GLOBALS["db"]->lastInsertNow("customer", $paramData, "customer_created_on");
										
					//insert into login api table			
					$paramDataApi = array("login_api_customer_id"=>$insertCust, "login_api_via"=>$param['c_via'], "login_api_user_id"=>$param['c_api_id'], "login_api_user_pic"=>$param['c_pic']);
					$insertApi = $GLOBALS["db"]->lastInsertNow("login_api", $paramDataApi, "login_api_created_on");	
					
					//insert into customer device relation table			
					$paramDataRel = array("device_id"=>$param['device_id'], "cust_id"=>$insertCust);
					$insertRel = $GLOBALS["db"]->insert("cust_device_relation", $paramDataRel);	
						
					$response = array("success"=>true);
					$response['code'] = 603;
					$response['c_id'] = $insertCust;
					$response['message'] = "User is not exist in our database!";
				}
			}
		}
		
		$jsonInsert = self::capture_json($request, json_encode($response), 'isUserExist');
		return $response;
	}
	//=================================================  isUserExist API Ends ================================================//
	
	//=================================================  Regsitration API starts ==============================================//
	function registration($param = null)
	{
		$request = json_encode($param);
		$paramData = array("customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email'], "customer_id"=>$param['c_id']);
		$selectData = $GLOBALS["db"]->select("SELECT customer_id, customer_verify, customer_mobile, customer_email FROM customer WHERE (customer_mobile=:customer_mobile OR customer_email=:customer_email) AND customer_id !=:customer_id", $paramData);
		if(count($selectData)>0)
		{
			$response = array("success"=>true);
			$response['code'] = 605;

			if(($selectData[0]['customer_mobile']==$param['c_mob'])&&($selectData[0]['customer_mobile']==$param['c_email']))
			{
				$response['message'] = "Hey! This Email ID and Mobile Number is already registered with us! Want to Sign In?";
			}
			else if(($selectData[0]['customer_mobile']==$param['c_mob'])&&($selectData[0]['customer_email']!=$param['c_email']))
			{
				$response['message'] = "Hey! This Mobile Number is already registered with us! Please check the number you added!
";
			}
			else if(($selectData[0]['customer_mobile']!=$param['c_mob'])&&($selectData[0]['customer_email']==$param['c_email']))
			{
				$response['message'] = "Hey! This Email ID is already registered with us! Want to Sign In?";
			}
		}
		else
		{
			if($param['c_dob']!=NULL)
			{
				$paramDataUpdate = array("customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email'], "customer_gender"=>$param['c_gender'], "customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])));
			}
			else
			{
				$paramDataUpdate = array("customer_mobile"=>$param['c_mob'], "customer_email"=>$param['c_email'], "customer_gender"=>$param['c_gender']);
			}
			$condition = array("customer_id"=>$param['c_id']);
			$update = $GLOBALS["db"]->update("customer", $paramDataUpdate, $condition);
				if($update=="true")
				{
					$otp = rand('1111', '9999');
					$mobile = '91'.$param['c_mob'];
					$validate = self::sendSMS($mobile, $otp);
					$response = array("success"=>true);
					$response['code'] = 606;
					$response['c_id'] = $param['c_id'];
					$response['otp'] = $otp;
				}		
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'registration');
		return $response;
	}
	
	//=================================================  Regsitration API Ends ==============================================//
	
	//=================================================  Device INFO API starts ==============================================//
	function deviceInfo($param1 = null)
	{		
		$request = json_encode($param1);
		$param = array();
		foreach($param1 as $k=>$v)
		{
			$param[strtolower(str_replace('-', '_', $k))] = $v;
		}
		
		unset($param['host']);
		unset($param['connection']);
		unset($param['content_length']);
		unset($param['origin']);
		unset($param['user_agent']);
		unset($param['cache_control']);
		unset($param['postman_token']);
		unset($param['accept']);
		unset($param['accept_encoding']);
		unset($param['accept_language']);
		unset($param['cookie']);
		unset($param['content_type']);
		
		$selectParam = array("imei"=>$param['device_imei']);
		$selectData = $GLOBALS["db"]->select("SELECT device_id, device_version FROM device WHERE device_imei =:imei", $selectParam);
		if(count($selectData)>0)
		{
			if($selectData[0]['device_version']!=$param['device_version'])
			{
				$response = array("success"=>true);
				$response['code'] = 702;
				$response['message'] = "Please update application!";
			}
			else
			{
				$response = self::mpDetails($selectData[0]['device_id'], $param['device_location'], $param['c_id'], $param['city']);
				
				if(($param['device_location']=="null")||($param['device_location']==NULL))
				{
					$param['device_address'] = $param['city'];
				}
				else
				{
					$param['device_address'] = self::getMapDetails($param["device_location"]);	
				}
				
				unset($param['c_id']);
				unset($param['city']);
				$condition = array("device_id"=>$selectData[0]['device_id']);
				
				$device_lat = NULL;
				$device_long = NULL;
				if(($param['device_location']!="")&&($param['device_location']!="null"))
				{
					$explodeLocation = explode(',', $param['device_location']);
					if(isset($explodeLocation[0]))
					{
						$device_lat =$explodeLocation[0]; 
					}
					if(isset($explodeLocation[1]))
					{
						$device_long =$explodeLocation[1]; 
					}
				}
				
				$updateParam = array("device_imei"=>$param['device_imei'], "device_name"=>$param['device_name'], "device_version"=>$param['device_version'], "device_resolution"=>$param['device_resolution'], "device_os"=>$param['device_os'], "device_token"=>$param['device_token'], "device_phone_id"=>$param['device_phone_id'], "device_address"=>$param['device_address'], "device_location"=>$param['device_location'], "device_lat"=>$device_lat, "device_long"=>$device_long);
				$update = $GLOBALS["db"]->update("device", $updateParam, $condition);
			}
		}
		else
		{
			$location =  $param['device_location'];
			$cid = $param['c_id'];
			$city = $param['city'];
			
			if($param['device_location']=="null")
			{
				$param['device_address'] = $city;
			}
			else
			{
				$param['device_address'] = self::getMapDetails($param["device_location"]);	
			}
			unset($param['c_id']);
			unset($param['city']);
			
			$device_lat = NULL;
			$device_long = NULL;
			if(($param['device_location']!="")&&($param['device_location']!="null"))
			{
				$explodeLocation = explode(',', $param['device_location']);
				if(isset($explodeLocation[0]))
				{
					$device_lat =$explodeLocation[0]; 
				}
				if(isset($explodeLocation[1]))
				{
					$device_long =$explodeLocation[1]; 
				}
			}
				
			$updateParam = array("device_imei"=>$param['device_imei'], "device_name"=>$param['device_name'], "device_version"=>$param['device_version'], "device_resolution"=>$param['device_resolution'], "device_os"=>$param['device_os'], "device_token"=>$param['device_token'], "device_phone_id"=>$param['device_phone_id'], "device_address"=>$param['device_address'], "device_location"=>$param['device_location'], "device_lat"=>$device_lat, "device_long"=>$device_long);
			
			$query = $GLOBALS["db"]->lastInsertNow("device", $updateParam, "device_created_on");
			$response = self::mpDetails($query, $location, $cid, $city);
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'deviceInfo');
		return $response;
	}
	//=================================================  Device INFO API Ends ==============================================//
	
	//=================================================  Device NEW INFO API starts ==============================================//	
	function deviceNewInfo($param1 = null)
	{
		$request = json_encode($param1);
		$param = array();
		foreach($param1 as $k=>$v)
		{
			$param[strtolower(str_replace('-', '_', $k))] = $v;
		}	
		
		if(isset($param['device_version']))
		{
			$selectParam = array("device_id"=>$param['device_id']);
			$selectData = $GLOBALS["db"]->select("SELECT device_version FROM device WHERE device_id =:device_id", $selectParam);
			if(count($selectData)>0)
			{
				if($selectData[0]['device_version']!=$param['device_version'])
				{
					$response = array("success"=>true);
					$response['code'] = 702;
					$response['message'] = "Please update application!";
					return $response;
				}
			}
		}
		$response = self::mpDetails($param['device_id'], $param['device_location'], $param['c_id'], $param['city']);
		$jsonInsert = self::capture_json($request, json_encode($response), 'deviceNewInfo');
		return $response;
	}
	
	//=================================================  Device NEW INFO API Ends ==============================================//
	
	//=================================================  get Offer Details API starts ==============================================//
	
	function getOfferDetail($param = null)
	{
		$request = json_encode($param);
		$param['deal_id'] = $param[0];
		$param['c_id'] = $param[1];
		$paramData = array("deal_id"=>$param['deal_id'], "c_id"=>$param['c_id']);
		
		$selectData = $GLOBALS["db"]->select("SELECT m.*, d.*, GROUP_CONCAT(DISTINCT(dg.deal_image_details)) as image_details, b.bookmark_id FROM mp_details m, deal_gallery dg,  deal d LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE d.deal_mp_id = m.mp_details_id AND dg.deal_id = d.deal_id AND d.deal_id =:deal_id GROUP BY d.deal_id", $paramData);
		
		$paramData = array("deal_id"=>$param['deal_id']);
		$selectReviewData = $GLOBALS["db"]->select("SELECT r.mp_review_id, r.mp_review_score, r.mp_review_comment, c.customer_name, c.customer_id, l.login_api_user_pic, rt.reply_text, m.mp_details_name FROM login_api l, deal d, mp_details m LEFT JOIN mp_reviews r ON r.mp_review_mp_details_id = m.mp_details_id LEFT JOIN customer c ON c.customer_id = r.mp_review_customer_id LEFT JOIN reply rt ON rt.reply_mp_review_id = r.mp_review_id WHERE l.login_api_customer_id = c.customer_id AND d.deal_mp_id = m.mp_details_id AND d.deal_id =:deal_id", $paramData);
		
		if(count($selectData)>0)
		{
			$bannerArray = array();
			$dealArray = array();
			foreach($selectData as $a)
			{
				// array creation for banner part	
				  $explode = explode(',', $a['image_details']);
				  for($i=0; $i<count($explode); $i++)
				  {
					  if($i<5)
					  {
					   	array_push($bannerArray, APP_CRM_UPLOADS_PATH.$explode[$i]);
					  }
				  }
				  $paramSelect = array("deal_id"=>$selectData[0]['deal_id'], "status"=>1);
				  $selectDataOffer = $GLOBALS["db"]->select("SELECT o.* FROM offers o WHERE offer_deal_id=:deal_id AND o.offer_status=:status", $paramSelect);
				  if(count($selectDataOffer)>0)
				  {
					  for($j=0; $j<count($selectDataOffer); $j++)
					  {
						  if($selectDataOffer[$j]['offer_coupon_status']==1)
							{
								//count of coupons left
								$paramData1 = array("offer_id"=>$selectDataOffer[$j]['offer_id']); 
								$selectCountOffer = $GLOBALS["db"]->select("SELECT SUM(booking_qty) as totBooking FROM booking WHERE booking_offer_id=:offer_id AND booking_status!=2", $paramData1);
								if(($selectDataOffer[$j]['max_capping']!=NULL)&&($selectDataOffer[$j]['max_capping']!=0))
								{
									$cap = $selectDataOffer[$j]['max_capping'] - $selectCountOffer[0]['totBooking'];
									if($cap>0)
									{
										$capTxt = $cap.' Coupons Left';
									}
									else
									{
										$capTxt ='Sold Out';
									}
								}
								else
								{
									$capTxt = NULL;
								}
							}
							else
							{
								$capTxt = NULL;
							}
						  // array creation for deal part   
						  $dealArray[$j]['o_id'] = $selectDataOffer[$j]['offer_id'];
						  if($selectDataOffer[$j]['offer_type_id'] ==1)
						  {
							  $selectDataOffer[$j]['offer_description'] = "Get ".$selectDataOffer[$j]['offer_discount']."% Off";
						  }
						  $dealArray[$j]['o_desc'] = $selectDataOffer[$j]['offer_description'];
						  $dealArray[$j]['o_coupon_status'] = $selectDataOffer[$j]['offer_coupon_status'];
						  $dealArray[$j]['o_coupon'] = $capTxt; 
						  $dealArray[$j]['deal_name'] = $selectData[0]['deal_name']; 
						  
						    $needs = array();
							 // $acceptCard = NULL;
							  //$veg = NULL;
							  //$bar = NULL;
							 
							  if($selectDataOffer[$j]['min_billing']!=NULL)
							  {
								  $Txt = 'Minimum bill should be '.$selectDataOffer[$j]['min_billing'];
								  array_push($needs, $Txt);
							  }				  
							  if($selectDataOffer[$j]['offers_day_type']!=NULL)
							  {
								  if($selectDataOffer[$j]['offers_day_type']==2)
								  {
									  $offerValid = "Weekdays only";
									  $offerTime = $selectDataOffer[$j]['offers_timeings'];
								  }
								  else if($selectDataOffer[$j]['offers_day_type']==3)
								  {
									   $offerValid = "Weekends only";
									   $offerTime = $selectDataOffer[$j]['offers_timeings'];
								  }
								  else
								  {
									   $offerValid = str_replace(',', ', ',$selectDataOffer[$j]['offers_day_value']);
									   $offerTime = $selectDataOffer[$j]['offers_timeings'];
								  }
							  }
							  else
							  {
								  $offerValid = $selectData[0]['deal_shoutout_day'];
								  $offerTime = $selectData[0]['deal_shoutout_time'];
							  }
							  $offerValidOn = "Offer valid on ".$offerValid;
							  $offerTimeOn = "Offer Timings are ".$offerTime;
							  array_push($needs, $offerValidOn);
							  array_push($needs, $offerTimeOn);
							  
							  if($a['mp_accept_card']==1)
							  {
								  $Txt = 'Accept card';
								  array_push($needs, $Txt);
							  }
							  
							  
							  if($a['mp_details_category']==2)
							  {
								  if($a['mp_nonveg']==1)
								  {
									  $vegTxt = 'Veg/Non-Veg';
									  array_push($needs, $vegTxt);
								  }
								  else
								  {
									  $vegTxt = 'Veg';
									  array_push($needs, $vegTxt);
								  }
								  if($a['mp_bar_available']==1)
								  {
									  $bar = 'Bar available';
									  array_push($needs, $bar);
								  }
								  else
								  {
									  $bar = 'Bar not available';
									  array_push($needs, $bar);
								  }
							  }
							  $dealArray[$j]['needs'] = $needs;
					  }
				  }
			}
			
			 
				//array creation for reviews
				$reviewArray = array();
				$myReview = array();
				if(count($selectReviewData)>0)
				{
					$i=0;
					foreach($selectReviewData as $b)
					{
						if($b['mp_review_id']!=NULL)
						{
							if($param['c_id']!=NULL)
							{
								if($b['customer_id']!=$param['c_id'])
								{
									$reviewArray[$i]['cust_name'] = $b['customer_name'];
									$reviewArray[$i]['c_id'] = $b['customer_id'];
									$reviewArray[$i]['score'] = $b['mp_review_score']; 
									$reviewArray[$i]['comment'] = $b['mp_review_comment']; 
									$reviewArray[$i]['c_pic'] = $b['login_api_user_pic'];
									$reviewArray[$i]['reply'] = $b['reply_text']; 
									$reviewArray[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
								else
								{
									$myReview[$i]['cust_name'] = $b['customer_name'];
									$myReview[$i]['c_id'] = $b['customer_id'];
									$myReview[$i]['score'] = $b['mp_review_score']; 
									$myReview[$i]['comment'] = $b['mp_review_comment']; 
									$myReview[$i]['c_pic'] = $b['login_api_user_pic'];
									$myReview[$i]['reply'] = $b['reply_text']; 
									$myReview[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
							}
							else
							{
								$reviewArray[$i]['cust_name'] = $b['customer_name'];
								$reviewArray[$i]['c_id'] = $b['customer_id'];
								$reviewArray[$i]['score'] = $b['mp_review_score']; 
								$reviewArray[$i]['comment'] = $b['mp_review_comment']; 
								$reviewArray[$i]['c_pic'] = $b['login_api_user_pic']; 
								$reviewArray[$i]['reply'] = $b['reply_text']; 
								$reviewArray[$i]['mp_name'] = $b['mp_details_name']; 
								$i++;
							}
						}
					}
				}
				
				$response = array("success"=>true);
				$response['code'] = 675;
				$response['mp_id'] = $selectData[0]['mp_details_id'];
				$response['deal_id'] = $selectData[0]['deal_id'];
				$response['deal_name'] = $selectData[0]['deal_name'];
				if($selectData[0]['deal_type']==1)
				{
					$response['deal_type'] = "normal";
				}
				else
				{
					$response['deal_type'] = "shoutout";
				}
				$response['deal_description'] = $selectData[0]['deal_shoutout_description'];
				$response['deal_shoutout_day'] = $selectData[0]['deal_shoutout_day'];
				$response['deal_shoutout_time'] = $selectData[0]['deal_shoutout_time'];
				$response['bookmark_id'] = $selectData[0]['bookmark_id'];
				$response['mp_name'] = $selectData[0]['mp_details_name'];
				$response['mp_email'] = $selectData[0]['mp_details_email'];
				$response['mp_website'] = $selectData[0]['mp_details_website'];
				$response['mp_opentime'] = date("h:i A", strtotime($selectData[0]['mp_opentime']));
				$response['mp_closetime'] = date("h:i A", strtotime($selectData[0]['mp_closetime']));
				
				// array creation for contact part 
				$response['mp_address1'] = $selectData[0]['mp_details_address'];
				$response['mp_city'] = $selectData[0]['mp_details_city'];
				$response['mp_longitude'] = $selectData[0]['mp_details_longitude'];
				$response['mp_latitude'] = $selectData[0]['mp_details_latitude'];
				
				if($selectData[0]['mp_details_contact']==NULL)
				{
					$response['mp_contact'] = '+91 '.$selectData[0]['mp_details_phone'];
				}
				else
				{
					$response['mp_contact'] = '+91 '.$selectData[0]['mp_details_contact'];
				}
				  
				// array creation for details part 
				$response['mp_recommend'] = $selectData[0]['mp_details_recommend'];
				$response['mp_description'] = $selectData[0]['mp_details_description'];
				
				$response['gallery'] = array_values($bannerArray);
				$response['review'] = array_values($reviewArray);
				$response['myReview'] = array_values($myReview);
				$response['deal'] = array_values($dealArray);
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 609;
			$response['message'] = "Sorry! Deal is not available!";
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'getOfferDetail');
		return $response;
	}
	
	//================================================= get Offer Details API Ends ==============================================//
		
	function mpDetails($device_id,$location, $c_id, $city)
	{
		//return $device_id.','.$location.','.$c_id.','.$city;
		$category_array1 = array();
		$category_array = array();
		$banner = array();
		$device_lat = 0;
		$device_long = 0;
		$query = "";
		$param = array("status"=>1, "c_id"=>$c_id);
		if(($location=="null")||($location==NULL))
		{
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, o.offers_day_type, o.offers_day_value, o.offers_timeings, d.deal_image_thumb AS img_all_thumb, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time, b.bookmark_id FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status AND d.deal_status =:status AND UPPER(mp.mp_details_city) ='".$city."' GROUP BY mp_details_id", $param);
		}
		else
		{
			$explode = explode(',', $location);
			$device_lat = $explode[0];
			$device_long = $explode[1];
			if(($city==NULL)||($city =="Nearby")||($city=="null"))
			{								
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name,mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, o.offers_day_type, o.offers_day_value, o.offers_timeings, d.deal_image_thumb AS img_all_thumb, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status AND d.deal_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
			else
			{
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name,mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, o.offers_day_type, o.offers_day_value, o.offers_timeings, d.deal_image_thumb AS img_all_thumb, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status AND d.deal_status =:status AND UPPER(mp.mp_details_city) ='".$city."' GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
		}
		
		if(count($data)>0)
		{
		$response = array("success"=>true);
		$response['code'] = 607;
		$response['device_id'] = $device_id;
	
		$trending[] = $data;
		$category_array = array();
					foreach($data as $a){
						//for normal deal
						if($a['deal_type']==1)
						{
							if($a['offers_day_type']==5)
							{
								$day_type = explode('-', $a['offers_day_value']);
								if($a['offers_timeings']=="All Day")
								{
									$a['offers_timeings'] = $a['mp_opentime'].'-'.$a['mp_closetime'];
								}
								$timings = explode('-', $a['offers_timeings']);
								$var = new DateTime('now');
								$starTime =  $var->format('Y-m-d H:i:s');		
								$day_type[1] = str_replace('/', '-', $day_type[1]);			
								//$format =  date("Y-m-d", strtotime($day_type[1]));
								$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
								if($starTime<$format)
								{
									$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
									$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
									$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
									if($a['offer_type_id'] == "1")
									{
										$a['o_desc'] = "Get ".$a['o_disc']."% Off";
									}
									unset($a['offer_type_id']);
									unset($a['o_disc']);
									unset($a['offers_day_type']);
									unset($a['offers_day_value']);
									unset($a['offers_timeings']);
									unset($a['deal_shoutout_day']);
									unset($a['deal_shoutout_time']);
									unset($a['deal_shoutout_description']);
									unset($a['mp_opentime']);
									unset($a['mp_closetime']);
									$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
									$category_array[$a['mp_cat']]['data'][]= $a;
								}
							}
							else
							{
								$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
								$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
								$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
								if($a['offer_type_id'] == "1")
								{
									$a['o_desc'] = "Get ".$a['o_disc']."% Off";
								}
								unset($a['offer_type_id']);
								unset($a['o_disc']);
								unset($a['offers_day_type']);
								unset($a['offers_day_value']);
								unset($a['offers_timeings']);
								unset($a['deal_shoutout_day']);
								unset($a['deal_shoutout_time']);
								unset($a['deal_shoutout_description']);
								unset($a['mp_opentime']);
								unset($a['mp_closetime']);
								$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
								$category_array[$a['mp_cat']]['data'][]= $a;
							}
						}
						//for shout out deal
						else
						{
							if(strpos($a['deal_shoutout_day'], '/'))
							{
								$day_type = explode('-', $a['deal_shoutout_day']);
								$timings = explode('-', $a['deal_shoutout_time']);
								$var = new DateTime('now');
								$starTime =  $var->format('Y-m-d H:i:s');		
								$day_type[1] = str_replace('/', '-', $day_type[1]);			
								//$format =  date("Y-m-d", strtotime($day_type[1]));
								$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
								if($starTime<$format)
								{
									$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
									$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
									$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
									$a['o_desc'] = $a['deal_shoutout_description'];
									unset($a['offer_type_id']);
									unset($a['o_disc']);
									unset($a['offers_day_type']);
									unset($a['offers_day_value']);
									unset($a['offers_timeings']);
									unset($a['deal_shoutout_day']);
									unset($a['deal_shoutout_time']);
									unset($a['deal_shoutout_description']);
									unset($a['mp_opentime']);
									unset($a['mp_closetime']);
									$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
									$category_array[$a['mp_cat']]['data'][]= $a;
								}
							}
							else
							{
								$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
								$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
								$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
								$a['o_desc'] = $a['deal_shoutout_description'];
								unset($a['offer_type_id']);
								unset($a['o_disc']);
								unset($a['offers_day_type']);
								unset($a['offers_day_value']);
								unset($a['offers_timeings']);
								unset($a['deal_shoutout_day']);
								unset($a['deal_shoutout_time']);
								unset($a['deal_shoutout_description']);
								unset($a['mp_opentime']);
								unset($a['mp_closetime']);
								$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
								$category_array[$a['mp_cat']]['data'][]= $a;
							}
						}
					}
					
					//storing array into all		
								$tot = 0;
								foreach($trending as $a1){
								foreach($a1 as $a){
									//for normal deal
								if($a['deal_type']==1)
								{	
									if($a['offers_day_type']==5)
									{
										$day_type = explode('-', $a['offers_day_value']);
										if($a['offers_timeings']=="All Day")
										{
											$a['offers_timeings'] = $a['mp_opentime'].'-'.$a['mp_closetime'];
										}
										$timings = explode('-', $a['offers_timeings']);
										$var = new DateTime('now');
										$starTime =  $var->format('Y-m-d H:i:s');		
										$day_type[1] = str_replace('/', '-', $day_type[1]);			
										//$format =  date("Y-m-d", strtotime($day_type[1]));
										$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
										if($starTime<$format)
										{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
											if($a['offer_type_id'] == "1")
											{
												$a['o_desc'] = "Get ".$a['o_disc']."% Off";
											}
											unset($a['o_disc']);
											unset($a['offer_type_id']);
											unset($a['offers_day_type']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array1[]= $a;
											if($tot<4)
											{
												if($a['is_trending'] != 0)
												{
													$tot++;
													$banner[]= $a;
												}
											}
										}
									}
									else
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
										if($a['offer_type_id'] == "1")
										{
											$a['o_desc'] = "Get ".$a['o_disc']."% Off";
										}
										unset($a['o_disc']);
										unset($a['offer_type_id']);
										unset($a['offers_day_type']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array1[]= $a;
										if($tot<4)
										{
											if($a['is_trending'] != 0)
											{
												$tot++;
												$banner[]= $a;
											}
										}
									}
								}
								//for shout out deal
								else
								{
									if(strpos($a['deal_shoutout_day'], '/'))
									{
										$day_type = explode('-', $a['deal_shoutout_day']);
										$timings = explode('-', $a['deal_shoutout_time']);
										$var = new DateTime('now');
										$starTime =  $var->format('Y-m-d H:i:s');		
										$day_type[1] = str_replace('/', '-', $day_type[1]);			
										//$format =  date("Y-m-d", strtotime($day_type[1]));
										$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
										if($starTime<$format)
										{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
											$a['o_desc'] = $a['deal_shoutout_description'];
											unset($a['o_disc']);
											unset($a['offer_type_id']);
											unset($a['offers_day_type']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array1[]= $a;
											if($tot<4)
											{
												if($a['is_trending'] != 0)
												{
													$tot++;
													$banner[]= $a;
												}
											}
										}
									}
									else
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
										$a['o_desc'] = $a['deal_shoutout_description'];
										unset($a['o_disc']);
										unset($a['offer_type_id']);
										unset($a['offers_day_type']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array1[]= $a;
										if($tot<4)
										{
											if($a['is_trending'] != 0)
											{
												$tot++;
												$banner[]= $a;
											}
										}
									}
								}
						}
					}
					
		$response['all'] = array_values($category_array1);
		$response['banner'] = array_values($banner);
		$response['category'] = array_values($category_array);
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 608;
			$response['all'] = array_values($category_array1);
			$response['banner'] = array_values($banner);
			$response['category'] = array_values($category_array);
		}
		return $response;
	}
	
	//=================================================  availOffer API starts ==============================================//
	
	function availOffer($param = null)
	{
		$request = json_encode($param);
		//to check mp or offer exist or not
		$myParam = array("o_id"=>$param['o_id'],"mp_id"=>$param['mp_id'],"status"=>1);
		$myData = $GLOBALS["db"]->select("SELECT o.offer_id, o.max_capping, o.min_billing, o.offers_timeings, o.offers_day_type, o.offers_day_value, m.mp_accept_card, m.mp_details_category, m.mp_nonveg, m.mp_bar_available FROM offers o, mp_details m WHERE o.offer_id =:o_id AND m.mp_details_id =:mp_id AND o.offer_mp_id = m.mp_details_id AND o.offer_status=:status AND m.mp_status =:status", $myParam);
		
		if(count($myData)>0)
		{
			$paramData = array("offer_id"=>$param['o_id']);
			$selectCountOffer = $GLOBALS["db"]->select("SELECT SUM(booking_qty) as totBooking FROM booking WHERE booking_offer_id=:offer_id AND booking_status!=2", $paramData);
						
			if($selectCountOffer[0]['totBooking']<$myData[0]['max_capping'])
			{			
				$checkParam = array("booking_customer_id"=>$param['c_id'], "status"=>"0");
				$select = $GLOBALS["db"]->select("SELECT booking_id, booking_coupon_code, booking_offer_id, booking_mp_id FROM booking WHERE booking_customer_id =:booking_customer_id AND booking_status =:status LIMIT 1", $checkParam);
				if(count($select)>0)
				{
					if(($select[0]['booking_offer_id']==$param['o_id'])&&($select[0]['booking_mp_id']==$param['mp_id']))
					{
						$response = array("success"=>true);
						$response['code'] = 613;
						$response['message'] = "We know you love this offer, because you already have an active coupon for this offer! Check your Coupons section!";
					}
					else
					{
						$paramData = array("o_id"=>$select[0]['booking_offer_id'], "mp_id"=>$select[0]['booking_mp_id']);
						$selectMy = $GLOBALS["db"]->select("SELECT m.mp_details_name, o.offer_discount, o.offer_type_id, o.offer_description, m.mp_details_address, m.mp_details_city, mp_details_latitude, mp_details_longitude FROM mp_details m, offers o WHERE m.mp_details_id =o.offer_mp_id AND o.offer_id =:o_id AND m.mp_details_id =:mp_id LIMIT 1", $paramData);
						if($selectMy[0]['offer_type_id']==1)
						{
							$selectMy[0]['offer_description'] = "Get ".$selectMy[0]['offer_discount']."% Off";
						}				
						$response = array("success"=>true);
						$response['code'] = 612;
						$response['booking_id'] = $select[0]['booking_id'];
						$response['voucher'] = $select[0]['booking_coupon_code'];
						$response['mp_name'] = $selectMy[0]['mp_details_name'];
						//$response['o_title'] = $selectMy[0]['deal_name'];
						$response['o_desc'] = $selectMy[0]['offer_description'];
						$response['mp_address'] = $selectMy[0]['mp_details_address'].','.$selectMy[0]['mp_details_city'];
						$response['mp_latitude'] = $selectMy[0]['mp_details_latitude'];
						$response['mp_longitude'] = $selectMy[0]['mp_details_longitude'];
						
						//for storing data into needs array
						$needs = array();
										 
						/*if($myData[0]['min_billing']!=NULL)
						{
							$Txt = 'Minimum bill should be '.$myData[0]['min_billing'];
							array_push($needs, $Txt);
						}	*/			  
						if($myData[0]['offers_day_type']==2)
						{
							$offerValid = "Weekdays only";
							$offerTime = $myData[0]['offers_timeings'];
						}
						else if($myData[0]['offers_day_type']==3)
						{
							 $offerValid = "Weekends only";
							 $offerTime = $myData[0]['offers_timeings'];
						}
						else
						{
							 $offerValid = $myData[0]['offers_day_value'];
							 $offerTime = $myData[0]['offers_timeings'];
						}
						
						$offerValidOn = "Offer valid on ".$offerValid;
						$offerTimeOn = "Offer Timings are ".$offerTime;
						array_push($needs, $offerValidOn);
						array_push($needs, $offerTimeOn);
						$response['deal'] = $needs;
						// ends needs section
					}
				}
				else
				{
					$randomString = self::checkUniqe();
					$address = self::getMapDetails($param["location"]);
					$partial = explode(',', $address);
					if(!isset($partial[1]))
					{
						$partial[1] = NULL;
					}
					if(!isset($partial[2]))
					{
						$partial[2] = NULL;
					}
					$full = $partial[0].','.$partial[1].','.$partial[2];					
					$paramData = array("booking_offer_id"=>$param['o_id'],"booking_customer_id"=>$param['c_id'],"booking_mp_id"=>$param['mp_id'], "booking_coupon_code"=>$randomString, "booking_location"=>$param["location"],"booking_full_address"=>$address,"booking_partial_address"=>$full);
					$insert = $GLOBALS["db"]->lastInsertNow("booking", $paramData, "booking_created_on");
					if($insert)
					{
						$paramData1 = array("o_id"=>$param['o_id'], "mp_id"=>$param['mp_id']);
						$selectMy = $GLOBALS["db"]->select("SELECT m.mp_details_name, d.deal_name, o.offer_discount, o.offer_description, o.offer_type_id, m.mp_details_address, m.mp_details_city, mp_details_latitude, mp_details_longitude FROM mp_details m, offers o, deal d WHERE m.mp_details_id =d.deal_mp_id AND o.offer_deal_id = d.deal_id AND o.offer_id =:o_id AND m.mp_details_id =:mp_id LIMIT 1", $paramData1);
						if($selectMy[0]['offer_type_id']==1)
						{
							$selectMy[0]['offer_description'] = "Get ".$selectMy[0]['offer_discount']."% Off";
						}
						$otp = $randomString;
						$mobile = '91'.$param['c_mob'];
						//$validate = self::sendSMS($mobile, $otp);
						$response = array("success"=>true);
						$response['code'] = 610;
						$response['voucher'] = $otp;
						$response['booking_id'] = $insert;
						$response['mp_name'] = $selectMy[0]['mp_details_name'];
						$response['o_title'] = $selectMy[0]['deal_name'];
						$response['o_desc'] = $selectMy[0]['offer_description'];
						$response['mp_address'] = $selectMy[0]['mp_details_address'].','.$selectMy[0]['mp_details_city'];
						$response['mp_latitude'] = $selectMy[0]['mp_details_latitude'];
						$response['mp_longitude'] = $selectMy[0]['mp_details_longitude'];
						
						//for storing data into needs array
						$dealArray = array();
						$needs = array();
										 
						if($myData[0]['min_billing']!=NULL)
						{
							$Txt = 'Minimum bill should be '.$myData[0]['min_billing'];
							array_push($needs, $Txt);
						}				  
						if($myData[0]['offers_day_type']==2)
						{
							$offerValid = "Weekdays only";
							$offerTime = $myData[0]['offers_timeings'];
						}
						else if($myData[0]['offers_day_type']==3)
						{
							 $offerValid = "Weekends only";
							 $offerTime = $myData[0]['offers_timeings'];
						}
						else
						{
							 $offerValid = $myData[0]['offers_day_value'];
							 $offerTime = $myData[0]['offers_timeings'];
						}
						
						$offerValidOn = "Offer valid on ".$offerValid;
						$offerTimeOn = "Offer Timings are ".$offerTime;
						array_push($needs, $offerValidOn);
						array_push($needs, $offerTimeOn);
						
						if($myData[0]['mp_accept_card']==1)
						{
							$Txt = 'Accept card';
							array_push($needs, $Txt);
						}						
						if($myData[0]['mp_details_category']==2)
						{
							if($myData[0]['mp_nonveg']==1)
							{
								$vegTxt = 'Veg/Non-Veg';
								array_push($needs, $vegTxt);
							}
							else
							{
								$vegTxt = 'Veg';
								array_push($needs, $vegTxt);
							}
							if($myData[0]['mp_bar_available']==1)
							{
								$bar = 'Bar available';
								array_push($needs, $bar);
							}
							else
							{
								$bar = 'Bar not available';
								array_push($needs, $bar);
							}
						}
						$response['deal'] = $needs;
					}
					else
					{
						$response = array("success"=>true);
						$response['code'] = 611;
						$response['message'] = "Oh No! Our server is acting funny! Can you please try again in a few minutes?";
					}
				}
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 631;
				$response['message'] = "Coupons are out of stock!";
			}
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 630;
			$response['message'] = "Sorry! Offer is not available.";
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'availOffer');

		return $response;
	}
	function checkUniqe()
	{
		$string = new math();
		$selectParam = array();
		$randomString = $string->randomString();
		$selectData = $GLOBALS["db"]->select("SELECT booking_id FROM booking WHERE booking_status = 0 AND booking_coupon_code =".$randomString, $selectParam);
		if(count($selectData)>0)
		{
			self::checkUniqe();
		}
		else
		{
			return strtoupper($randomString);
		}
	}
	function getMapDetails($location)
	{
		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$location.'&sensor=false');
		$geo = json_decode($geo, true);
		if ($geo['status'] = 'OK') 
		{
		  if(isset($geo['results'][0]['formatted_address']))
		  {
		  		$address = $geo['results'][0]['formatted_address'];
		  }
		  else
		  {
			  $address = NULL;
		  }
		}
		else
		{
			$address = NULL;
		}
		return $address;
	}
	//=================================================  checkUniqe API Ends ==============================================//
	
	//=================================================  cancelAvailOffer starts ==============================================//
	
	function cancelAvailOffer($param = null)
	{
		$request = json_encode($param);
		$condition = array("booking_id"=>$param['booking_id']);
		$paramUpdate = array("booking_status"=>2);
		$update = $GLOBALS["db"]->update("booking", $paramUpdate, $condition);
		if($update=="true")
		{
			$response = array("success"=>true);
			$response['code'] = 624;
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 625;
			$response['message'] = "This offer loves you! But if you don't like it, then we must delete it! Can you please try again?";
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'cancelAvailOffer');
		return $response;
	}
	
	//=================================================  CheckwriteReview API starts ==============================================//
	function checkWriteReview($param = null)
	{
		$request = json_encode($param);
		$selectParam = array("customer_id"=>$param['c_id'],"mp_id"=>$param['mp_id']);
		$selectData = $GLOBALS["db"]->select("SELECT mp_review_score, mp_review_comment, mp_review_id FROM mp_reviews WHERE mp_review_customer_id =:customer_id AND mp_review_mp_details_id =:mp_id LIMIT 1", $selectParam);
		if(count($selectData)>0)
		{
			$response = array("success"=>true);
			$response['code'] = 614;
			$response['score'] = $selectData[0]['mp_review_score'];
			$response['comment'] = $selectData[0]['mp_review_comment'];
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 615;			
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'checkWriteReview');
		return $response;
	}
	//=================================================  Write Review API starts ==============================================//
	
	//=================================================  writeReview API starts ==============================================//
	function writeReview($param = null)
	{
		$request = json_encode($param);
		$selectParam = array("customer_id"=>$param['c_id'],"mp_id"=>$param['mp_id']);
		$selectData = $GLOBALS["db"]->select("SELECT mp_review_id FROM mp_reviews WHERE mp_review_customer_id =:customer_id AND 	mp_review_mp_details_id =:mp_id", $selectParam);
		if(count($selectData)>0)
		{
			// update review
			$condition = array("mp_review_customer_id"=>$param['c_id'],"mp_review_mp_details_id"=>$param['mp_id']);
			$updateParam = array("mp_review_comment"=>$param['comment'],"mp_review_score"=>$param['score']);
			$update = $GLOBALS["db"]->update("mp_reviews", $updateParam, $condition);
			if($update=="true")
			{
				$response = self::getReviews($param['mp_id'], $param['c_id']);
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 677;
				$response['message'] = "Oops! Your review fell through our hands, Sorry! Can you please review it again?";
			}
		}
		else
		{
			//insert review
			$insertParam = array("mp_review_comment"=>$param['comment'],"mp_review_score"=>$param['score'], "mp_review_customer_id"=>$param['c_id'],"mp_review_mp_details_id"=>$param['mp_id']);
			$insert = $GLOBALS["db"]->lastInsertNow('mp_reviews', $insertParam, 'mp_review_created_on');
			if($insert)
			{
				$response = self::getReviews($param['mp_id'], $param['c_id']);
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 677;
				$response['message'] = "Oops! Your review fell through our hands, Sorry! Can you please review it again?";
			}
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'writeReview');
		return $response;
	}
	//=================================================  Write Review API ends ==============================================//
	
	//=================================================   get Review list API starts ==============================================//
	function getReviews($mp_id, $cid)
	{
		$paramData = array("mp_id"=>$mp_id);
		$reviewArray = array();
		$myReview = array();
		$selectReviewData = $GLOBALS["db"]->select("SELECT r.mp_review_id, r.mp_review_score, r.mp_review_comment, c.customer_name, c.customer_id, l.login_api_user_pic, rt.reply_text, m.mp_details_name FROM login_api l, mp_details m LEFT JOIN mp_reviews r ON r.mp_review_mp_details_id = m.mp_details_id LEFT JOIN customer c ON c.customer_id = r.mp_review_customer_id LEFT JOIN reply rt ON rt.reply_mp_review_id = r.mp_review_id WHERE l.login_api_customer_id = c.customer_id AND m.mp_details_id =:mp_id", $paramData);
		//array creation for reviews
				if(count($selectReviewData)>0)
				{
					$i=0;
					foreach($selectReviewData as $b)
					{
						if($b['mp_review_id']!=NULL)
						{
							if($b['customer_id']!=$cid)
								{
									$reviewArray[$i]['cust_name'] = $b['customer_name'];
									$reviewArray[$i]['c_id'] = $b['customer_id'];
									$reviewArray[$i]['score'] = $b['mp_review_score']; 
									$reviewArray[$i]['comment'] = $b['mp_review_comment']; 
									$reviewArray[$i]['c_pic'] = $b['login_api_user_pic']; 
									$reviewArray[$i]['reply'] = $b['reply_text']; 
									$reviewArray[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
								else
								{
									$myReview[$i]['cust_name'] = $b['customer_name'];
									$myReview[$i]['c_id'] = $b['customer_id'];
									$myReview[$i]['score'] = $b['mp_review_score']; 
									$myReview[$i]['comment'] = $b['mp_review_comment']; 
									$myReview[$i]['c_pic'] = $b['login_api_user_pic']; 
									$myReview[$i]['reply'] = $b['reply_text']; 
									$myReview[$i]['mp_name'] = $b['mp_details_name']; 
									$i++;
								}
						}
					}
				}
		$response = array("success"=>true);
		$response['code'] = 676;		
		$response['review'] = array_values($reviewArray);
		$response['myReview'] = array_values($myReview);
		return $response;
	}
	//=================================================  get Review list API Ends ==============================================//
	
	//=================================================  bookmark API starts ==============================================//
	function bookmark($param = null)
	{
		$request = json_encode($param);
		$selectParam = array("bookmark_customer_id"=>$param['c_id'],"bookmark_deal_id"=>$param['deal_id']);
		$selectData = $GLOBALS["db"]->select("SELECT bookmark_id FROM bookmark WHERE bookmark_customer_id =:bookmark_customer_id AND bookmark_deal_id =:bookmark_deal_id LIMIT 1", $selectParam);
		if(count($selectData)>0)
		{
			$seleteArray = array("bookmark_id"=>$selectData[0]['bookmark_id']);
			$result = $GLOBALS["db"]->deleteQuery('bookmark',$seleteArray);	
			if($result)
			{
				$value = NULL;
			}
		}
		else
		{
			$result = $GLOBALS["db"]->lastInsertNow('bookmark', $selectParam, 'bookmark_created_on');
			if($result>0)
			{
				$value = $result;
			}
		}
		if($result>0)
		{
			$response = array("success"=>true);
			$response['bookmark_id'] = $value;
			$response['code'] = 616;
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 617;
			$response['message'] = "There is some error while adding bookmark!  Please try again";
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'bookmark');
		return $response;
	}
	//=================================================  bookmark API starts ==============================================//
	
	//=================================================  getFavouriteList API starts ==============================================//
	function getFavouriteList($param = null)
	{
		$request = json_encode($param);
		$validate = self::filterData($customer = $param['c_id'], $device_lat = $param['latitude'], $device_long = $param['longitude'], $apiName = "getFavourite");
		$jsonInsert = self::capture_json($request, json_encode($validate), 'getFavouriteList');
		return $validate;
	}
	//=================================================  getFavouriteList API starts ==============================================//
	
	//=================================================  filterApiCIty API starts ==============================================//
	function filterApiCity($param = null)
	{
		$request = json_encode($param);
		$validate = self::getfilterData($customer = $param['c_id'], $city = $param['city'], $distance = null,  $device_lat = $param['latitude'], $device_long = $param['longitude'], $searchText = null, $apiName = "city");
		$jsonInsert = self::capture_json($request, json_encode($validate), 'filterApiCity');
		return $validate;
	}
	//=================================================  filterApiCIty API starts ==============================================//
	
	//=================================================  filterApiDistance API starts ==============================================//
	function filterApiDistance($param = null)
	{
		$request = json_encode($param);
		$validate = self::getfilterData($customer = $param['c_id'], $city = null, $distance = $param['distance'],  $device_lat = $param['latitude'], $device_long = $param['longitude'], $searchText = null, $apiName = "distance");
		$jsonInsert = self::capture_json($request, json_encode($validate), 'filterApiDistance');
		return $validate;
	}
	//=================================================  filterApiDistance API ends ==============================================//
	
	//=================================================  filterApiSearch API starts ==============================================//
	function filterApiSearch($param = null)
	{
		$request = json_encode($param);
		$validate = self::getfilterData($customer = $param['c_id'], $city = null, $distance = null,  $device_lat = $param['latitude'], $device_long = $param['longitude'], $searchText = $param['searchTxt'], $apiName = "search");
		$jsonInsert = self::capture_json($request, json_encode($validate), 'filterApiSearch');
		return $validate;
	}
	//=================================================  filterApiSearch API Ends ==============================================//
	
		
	function filterData($customer, $device_lat, $device_long, $apiName)
	{
		// to get bookmark list
		if($apiName=='getFavourite')
		{
			$param = array("c_id"=>$customer, "status"=>1);		
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, o.offers_day_type, o.offers_day_value, o.offers_timeings, d.deal_image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status INNER JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id INNER JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status AND d.deal_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			if(count($data)>0)
			{	
				$category_array = array();
							foreach($data as $a){
								if($a['deal_type']==1)
								{
									if($a['offers_day_type']==5)
									{
										$day_type = explode('-', $a['offers_day_value']);
										if($a['offers_timeings']=="All Day")
										{
											$a['offers_timeings'] = $a['mp_opentime'].'-'.$a['mp_closetime'];
										}
										$timings = explode('-', $a['offers_timeings']);
										$var = new DateTime('now');
										$starTime =  $var->format('Y-m-d H:i:s');		
										$day_type[1] = str_replace('/', '-', $day_type[1]);			
										//$format =  date("Y-m-d", strtotime($day_type[1]));
										$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
										if($starTime<$format)
										{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
											if($a['offer_type_id'] == 1)
											{
												$a['o_desc'] = "Get ".$a['o_disc']."% Off";
											}
											unset($a['o_disc']);
											unset($a['offer_type_id']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['offers_day_type']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array[]= $a;
										}
									}
									else
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										if($a['offer_type_id'] == 1)
										{
											$a['o_desc'] = "Get ".$a['o_disc']."% Off";
										}
										unset($a['o_disc']);
										unset($a['offer_type_id']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[]= $a;
									}
								}
								else
								{
									if(strpos($a['deal_shoutout_day'], '/'))
									{
										$day_type = explode('-', $a['deal_shoutout_day']);
										$timings = explode('-', $a['deal_shoutout_time']);
										$var = new DateTime('now');
										$starTime =  $var->format('Y-m-d H:i:s');		
										$day_type[1] = str_replace('/', '-', $day_type[1]);			
										//$format =  date("Y-m-d", strtotime($day_type[1]));
										$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
										if($starTime<$format)
										{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
											$a['o_desc'] = $a['deal_shoutout_description'];
											unset($a['o_disc']);
											unset($a['offer_type_id']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['offers_day_type']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array[]= $a;
										}
									}
									else
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										$a['o_desc'] = $a['deal_shoutout_description'];
										unset($a['o_disc']);
										unset($a['offer_type_id']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[]= $a;
									}
								}
							}			
				$response = array("success"=>true);
				$response['code'] = 618;
				$response['data'] = array_values($category_array);
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 619;
				$response['message'] = "No Bookmark Exist";
			}
			return $response;
		}	
	}
	
	function getfilterData($customer, $city, $distance,  $device_lat, $device_long, $searchText, $apiName)
	{
		$category_array1 = array();
		$category_array = array();
		$data1 = array();
		$banner = array();
		//filter by city
		if($apiName=='city')
		{
			$param = array("city"=>strtoupper($city), "status"=>1, "c_id"=>$customer);							
			if($city =="Nearby")
			{
				$param = array("status"=>1, "c_id"=>$customer);	
				//$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, o.offer_description AS o_desc, o.offer_id AS o_id, o.deal_name as o_gen_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, o.offer_trending_image as o_trending_banner, o.is_trending, o.image_category AS img_category, o.image_thumb AS img_all_thumb, o.offers_day_type, o.offers_day_value, o.offers_timeings, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id FROM category c, offers_gallery og, offers o INNER JOIN mp_details mp ON o.offer_mp_id = mp.mp_details_id LEFT JOIN bookmark b ON o.offer_id = b.bookmark_offer_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.offer_id = o.offer_id AND c.category_status =:status AND mp_status =:status AND o.offer_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);				
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name,mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, d.deal_image_thumb AS img_all_thumb, o.offers_day_type, o.offers_day_value, o.offers_timeings, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
			else
			{
				$param = array("status"=>1, "c_id"=>$customer);	
				$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, o.offers_day_type, o.offers_day_value, o.offers_timeings, d.deal_image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status AND mp.mp_details_city LIKE '%".$city."' GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			}
		}
		//filter by distance
		if($apiName=='distance')
		{
			if($distance==0)
			{
				$query1 = "";
			}
			else
			{
				$query1 = " HAVING mp_dist <=".$distance;
			}
			$param = array("status"=>1, "c_id"=>$customer);
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name,mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, d.deal_image_thumb AS img_all_thumb, o.offers_day_type, o.offers_day_value, o.offers_timeings, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status GROUP BY mp_details_id ".$query1." ORDER BY mp_dist ASC", $param);
		}
		
		//filter by search Txt
		if($apiName=='search')
		{
			$searchText = trim($searchText);
			$query1 = " AND (mp.mp_details_name LIKE '%".$searchText."%' OR mp.mp_details_city LIKE '%".$searchText."%' OR mp.mp_details_address LIKE '%".$searchText."%')";
			$param = array("status"=>1, "c_id"=>$customer);
			$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name,mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, o.offers_day_type, o.offers_day_value, o.offers_timeings, d.deal_image_thumb AS img_all_thumb, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status".$query1." GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);			
		$data1 = $GLOBALS["db"]->select("SELECT c.*, s.store_logo FROM coupons c, store s WHERE c.store_name LIKE '%".$searchText."%' OR c.cats_text LIKE '%".$searchText."%' GROUP BY c.coupon_id", $param);
		}
		
		if((count($data)>0)||(count($data1)>0))
		{
			if($apiName=='search')
			{
				$response = array("success"=>true);
				$response['code'] = 607;	
				$category_array = array();
				foreach($data as $a)
				{
					if($a['deal_type']==1)
					{
						if($a['offers_day_type']==5)
								{
									$day_type = explode('-', $a['offers_day_value']);
									if($a['offers_timeings']=="All Day")
									{
										$a['offers_timeings'] = $a['mp_opentime'].'-'.$a['mp_closetime'];
									}
									$timings = explode('-', $a['offers_timeings']);
									$var = new DateTime('now');
									$starTime =  $var->format('Y-m-d H:i:s');		
									$day_type[1] = str_replace('/', '-', $day_type[1]);			
									//$format =  date("Y-m-d", strtotime($day_type[1]));
									$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
									if($starTime<$format)
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										if($a['offer_type_id'] == 1)
										{
											$a['o_desc'] = "Get ".$a['o_disc']."% Off";
										}
										unset($a['offer_type_id']);
										unset($a['o_disc']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[]= $a;
									}
								}
								else
								{
									$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
									$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
									$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
									if($a['offer_type_id'] == 1)
									{
										$a['o_desc'] = "Get ".$a['o_disc']."% Off";
									}
									unset($a['offer_type_id']);
									unset($a['o_disc']);
									unset($a['offers_day_value']);
									unset($a['offers_timeings']);
									unset($a['offers_day_type']);
									unset($a['deal_shoutout_day']);
									unset($a['deal_shoutout_time']);
									unset($a['deal_shoutout_description']);
									unset($a['mp_opentime']);
									unset($a['mp_closetime']);
									$category_array[]= $a;
								}
					}
					else
					{
						if(strpos($a['deal_shoutout_day'], '/'))
								{
									$day_type = explode('-', $a['deal_shoutout_day']);
									$timings = explode('-', $a['deal_shoutout_time']);
									$var = new DateTime('now');
									$starTime =  $var->format('Y-m-d H:i:s');		
									$day_type[1] = str_replace('/', '-', $day_type[1]);			
									//$format =  date("Y-m-d", strtotime($day_type[1]));
									$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
									if($starTime<$format)
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										$a['o_desc'] = $a['deal_shoutout_description'];
										unset($a['offer_type_id']);
										unset($a['o_disc']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[]= $a;
									}
								}
								else
								{
									$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
									$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
									$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
									$a['o_desc'] = $a['deal_shoutout_description'];
									unset($a['offer_type_id']);
									unset($a['o_disc']);
									unset($a['offers_day_value']);
									unset($a['offers_timeings']);
									unset($a['offers_day_type']);
									unset($a['deal_shoutout_day']);
									unset($a['deal_shoutout_time']);
									unset($a['deal_shoutout_description']);
									unset($a['mp_opentime']);
									unset($a['mp_closetime']);
									$category_array[]= $a;
								}
					}
				}	
				$response['data'] = array_values($category_array);	
				$response['data1'] = array_values($data1);	
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 607;
				$trending[] = $data;
				$category_array = array();
							foreach($data as $a)
							{
								if($a['deal_type']==1)
								{
									if($a['offers_day_type']==5)
									{
									$day_type = explode('-', $a['offers_day_value']);
									if($a['offers_timeings']=="All Day")
									{
										$a['offers_timeings'] = $a['mp_opentime'].'-'.$a['mp_closetime'];
									}
									$timings = explode('-', $a['offers_timeings']);
									$var = new DateTime('now');
									$starTime =  $var->format('Y-m-d H:i:s');		
									$day_type[1] = str_replace('/', '-', $day_type[1]);			
									//$format =  date("Y-m-d", strtotime($day_type[1]));
									$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
									if($starTime<$format)
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										if($a['offer_type_id'] == 1)
										{
											$a['o_desc'] = "Get ".$a['o_disc']."% Off";
										}
										unset($a['offer_type_id']);
										unset($a['o_disc']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
										$category_array[$a['mp_cat']]['data'][]= $a;
									}
								}
								else
								{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										if($a['offer_type_id'] == 1)
										{
											$a['o_desc'] = "Get ".$a['o_disc']."% Off";
										}
										unset($a['offer_type_id']);
										unset($a['o_disc']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
										$category_array[$a['mp_cat']]['data'][]= $a;
								}
								}
								else
								{
									if(strpos($a['deal_shoutout_day'], '/'))
									{
										$day_type = explode('-', $a['deal_shoutout_day']);
										$timings = explode('-', $a['deal_shoutout_time']);
										$var = new DateTime('now');
										$starTime =  $var->format('Y-m-d H:i:s');		
										$day_type[1] = str_replace('/', '-', $day_type[1]);			
										//$format =  date("Y-m-d", strtotime($day_type[1]));
										$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
										if($starTime<$format)
										{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
											$a['o_desc'] = $a['deal_shoutout_description'];
											unset($a['offer_type_id']);
											unset($a['o_disc']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['offers_day_type']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
											$category_array[$a['mp_cat']]['data'][]= $a;
										}
									}
									else
									{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
											$a['o_desc'] = $a['deal_shoutout_description'];
											unset($a['offer_type_id']);
											unset($a['o_disc']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['offers_day_type']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array[$a['mp_cat']]['name']= $a['mp_cat'];
											$category_array[$a['mp_cat']]['data'][]= $a;
									}
								}
							}
							
				//storing array into all			
				$category_array1 = array();
				$tot = 0;
				$banner = array();
							foreach($trending as $a1){
								foreach($a1 as $a)
								{
									if($a['deal_type']==1)
									{
										if($a['offers_day_type']==5)
										{
											$day_type = explode('-', $a['offers_day_value']);
											if($a['offers_timeings']=="All Day")
											{
												$a['offers_timeings'] = $a['mp_opentime'].'-'.$a['mp_closetime'];
											}
											$timings = explode('-', $a['offers_timeings']);
											$var = new DateTime('now');
											$starTime =  $var->format('Y-m-d H:i:s');		
											$day_type[1] = str_replace('/', '-', $day_type[1]);			
											//$format =  date("Y-m-d", strtotime($day_type[1]));
											$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
											if($starTime<$format)
											{
												$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
												$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
												$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
												if($a['offer_type_id'] == 1)
												{
													$a['o_desc'] = "Get ".$a['o_disc']."% Off";
												}
												unset($a['offer_type_id']);
												unset($a['o_disc']);
												unset($a['offers_day_value']);
												unset($a['offers_timeings']);
												unset($a['offers_day_type']);
												unset($a['deal_shoutout_day']);
												unset($a['deal_shoutout_time']);
												unset($a['deal_shoutout_description']);
												unset($a['mp_opentime']);
												unset($a['mp_closetime']);
												$category_array1[]= $a;
												/*if($tot<6)
												{
													if($a['is_trending'] != 0)
													{
														$tot++;
														$banner[]= $a;
													}
												}*/
											}
										}
										else
										{
												$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
												$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
												$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
												if($a['offer_type_id'] == 1)
												{
													$a['o_desc'] = "Get ".$a['o_disc']."% Off";
												}
												unset($a['offer_type_id']);
												unset($a['o_disc']);
												unset($a['offers_day_value']);
												unset($a['offers_timeings']);
												unset($a['offers_day_type']);
												unset($a['deal_shoutout_day']);
												unset($a['deal_shoutout_time']);
												unset($a['deal_shoutout_description']);
												unset($a['mp_opentime']);
												unset($a['mp_closetime']);
												$category_array1[]= $a;
												/*if($tot<6)
												{
													if($a['is_trending'] != 0)
													{
														$tot++;
														$banner[]= $a;
													}
												}*/
										}
									}
									else
									{
										if(strpos($a['deal_shoutout_day'], '/'))
										{
											$day_type = explode('-', $a['deal_shoutout_day']);
											$timings = explode('-', $a['deal_shoutout_time']);
											$var = new DateTime('now');
											$starTime =  $var->format('Y-m-d H:i:s');		
											$day_type[1] = str_replace('/', '-', $day_type[1]);			
											//$format =  date("Y-m-d", strtotime($day_type[1]));
											$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
											if($starTime<$format)
											{
												$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
												$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
												$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
												$a['o_desc'] = $a['deal_shoutout_description'];
												unset($a['offer_type_id']);
												unset($a['o_disc']);
												unset($a['offers_day_value']);
												unset($a['offers_timeings']);
												unset($a['offers_day_type']);
												unset($a['deal_shoutout_day']);
												unset($a['deal_shoutout_time']);
												unset($a['deal_shoutout_description']);
												unset($a['mp_opentime']);
												unset($a['mp_closetime']);
												$category_array1[]= $a;
												/*if($tot<6)
												{
													if($a['is_trending'] != 0)
													{
														$tot++;
														$banner[]= $a;
													}
												}*/
											}
										}
										else
										{
												$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
												$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
												$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];
												$a['o_desc'] = $a['deal_shoutout_description'];
												unset($a['offer_type_id']);
												unset($a['o_disc']);
												unset($a['offers_day_value']);
												unset($a['offers_timeings']);
												unset($a['offers_day_type']);
												unset($a['deal_shoutout_day']);
												unset($a['deal_shoutout_time']);
												unset($a['deal_shoutout_description']);
												unset($a['mp_opentime']);
												unset($a['mp_closetime']);
												$category_array1[]= $a;
												/*if($tot<6)
												{
													if($a['is_trending'] != 0)
													{
														$tot++;
														$banner[]= $a;
													}
												}*/
										}
									}
								}
							}
				$response['all'] = array_values($category_array1);
				
			$param = array("status"=>1, "c_id"=>$customer);
			$dataTrend = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name,mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name as deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, d.deal_image_thumb AS img_all_thumb, o.offers_day_type, o.offers_day_value, o.offers_timeings, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id, d.deal_type, d.deal_shoutout_description, d.deal_shoutout_day, d.deal_shoutout_time FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON d.deal_id = o.offer_deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id AND b.bookmark_customer_id =:c_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status AND is_trending=1 GROUP BY mp_details_id ORDER BY mp_dist ASC", $param);
			
			
				
				$response['banner'] = $dataTrend;
				$response['category'] = array_values($category_array);	
			}
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 608;
			$response['all'] = array_values($category_array1);
			$response['banner'] = array_values($banner);
			$response['category'] = array_values($category_array);
		}
		
		return $response;
	}
	
	function SendSMS($mobile, $otp)
	{
		//$url = 'http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=355074&username=9999999574&password=ddgdt&senderid=ODDEVN&To='.$mobile.'&Text=Hi!%20Welcome%20to%20Odd%20Even%20Ride!%20Your%20OTP%20to%20register%20is%20'.$otp;
		
		$url = 'http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=355074&username=9999999574&password=ddgdt&senderid=SHOPFR&To='.$mobile.'&Text=Hi!%20Welcome%20to%20Shoppfer!%20Your%20OTP%20to%20register%20is%20'.$otp;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_exec($ch);
		curl_close($ch);	
		$sendParam = array('sms_number'=>$mobile);			
		$insert = $GLOBALS["db"]->insert("sms_sent", $sendParam);
		return $insert;
	}
	
	/**
	 * API for Sign IN from mobile APP
	 * Using POST Method 
	 * @param using for receiving parameters
	 */
	function otpVerify($param = null)
	{	
		$request = json_encode($param);
		$condition = array("customer_id"=>$param['c_id']);
		$updateData = array("customer_status"=>1, "customer_verify"=>1);
		$update = $GLOBALS["db"]->update("customer", $updateData, $condition);
		if($update=="true")
		{
			$response = array("success"=>true);
			$response['code'] = 601;
			$data = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, DATE_FORMAT( c.customer_dob,  '%M %e %Y' ) AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic, COUNT(DISTINCT m.mp_review_id) as reviews, COUNT(DISTINCT b.booking_id) AS redeemed FROM login_api l, customer c LEFT JOIN mp_reviews m ON c.customer_id = m.mp_review_customer_id LEFT JOIN booking b ON c.customer_id = b.booking_customer_id AND b.booking_status =1 WHERE l.login_api_customer_id = c.customer_id AND c.customer_id =:customer_id GROUP BY c.customer_id", $condition);			
			$response['data'] = $data;
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'otpVerify');
		return $response;
	}
	/*function statusUpdate($param = null)
	{
		$response = array("success"=>true);
		$response['code'] = 601;
		$condition = array("customer_mobile"=>$param['c_mob']);
		$data = $GLOBALS["db"]->deleteQuery("customer", $condition);
		if($data)
		{
			$response = array("success"=>true);
			$response['code'] = 601;
		}
		else
		{
			$response['success'] = true;
			$response['code'] = 707;
			$response['message'] = "There is some error while updating your status";
		}
		return $response;
	}*/
	function resendOTP($param = null)
	{
		$request = json_encode($param);
		
		$response = array("success"=>true);
		// means give data list
		$response['code'] = 682;
		// send sms
		$otp = rand('1111', '9999');
		$mobile = '91'.$param['c_mob'];
		$validate = self::sendSMS($mobile, $otp);
		$response['otp'] = $otp;
		$jsonInsert = self::capture_json($request, json_encode($response), 'resendOTP');
		return $response;
	}
	function profileUpdate($param = null)
	{
		$request = json_encode($param);
		$paramData = array("id"=>$param['c_id']);
		$selectData = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, DATE_FORMAT( c.customer_dob,  '%M %e %Y' ) AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic, COUNT(DISTINCT m.mp_review_id) as reviews, COUNT(DISTINCT b.booking_id) AS redeemed FROM login_api l,customer c LEFT JOIN mp_reviews m ON c.customer_id = m.mp_review_customer_id LEFT JOIN booking b ON c.customer_id = b.booking_customer_id AND b.booking_status =1 WHERE l.login_api_customer_id = c.customer_id AND c.customer_id =:id GROUP BY c.customer_id LIMIT 1", $paramData);
				
			if($selectData[0]['c_mob']!= $param['c_mob'])
			{	
				$paramDataMob = array("id"=>$param['c_id'], "mob"=>$param['c_mob']);
				$selectMob = $GLOBALS["db"]->select("SELECT customer_id FROM customer WHERE customer_id!=:id AND customer_mobile =:mob", $paramDataMob);
				if(count($selectMob)>0)
				{
					$response = array("success"=>true);
					$response['code'] = 679;
					$response['message'] = "User already exist with same Mobile!";
				}
				else
				{
					$response = array("success"=>true);
					$response['code'] = 622;
					// send sms
					$otp = rand('1111', '9999');
					$mobile = '91'.$param['c_mob'];
					$response['c_id'] = $param['c_id'];
					$validate = self::sendSMS($mobile, $otp);
					$response['otp'] = $otp;
				}
			}
			else
			{
				if($param['c_dob']=="" || $param['c_dob']==NULL)
				{
					$paramData = array("customer_name"=>$param['c_name'],"customer_gender"=>$param['c_gender']);
				}
				else
				{
					$paramData = array("customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])), "customer_name"=>$param['c_name'],"customer_gender"=>$param['c_gender']);
				}
				$condition = array("customer_id"=>$param['c_id']);
				$update = $GLOBALS["db"]->update("customer", $paramData, $condition);
				if($update=="true")
				{
					$response = array("success"=>true);
					$response['code'] = 623;
					$response['c_id'] = $selectData[0]['c_id'];
					$selectData[0]['c_dob'] = $param['c_dob'];
					$selectData[0]['c_name'] = $param['c_name'];
					$selectData[0]['c_gen'] = $param['c_gender'];
					$response['data'] = $selectData;
				}
				else
				{
					$response = array("success"=>true);
					$response['code'] = 678;
					$response['message'] = "Oops! Our server needs a wake up call! Data could not be updated, can you try again?";
				}
			}
			$jsonInsert = self::capture_json($request, json_encode($response), 'profileUpdate');
			return $response;
	}
	function profileUpdateVerify($param = null)
	{
		$request = json_encode($param);
		$paramData = array("customer_dob"=>date('Y-m-d', strtotime($param['c_dob'])), "customer_name"=>$param['c_name'], "customer_mobile"=>$param['c_mob'], "customer_gender"=>$param['c_gender']);
		$condition = array("customer_id"=>$param['c_id']);
		$update = $GLOBALS["db"]->update("customer", $paramData, $condition);
		if($update=="true")
		{
			$response = array("success"=>true);
			$response['code'] = 626;
			
			$paramData = array("id"=>$param['c_id']);
			$selectData = $GLOBALS["db"]->select("SELECT c.customer_id AS c_id, c.customer_name as c_name, c.customer_mobile AS c_mob, c.customer_email AS c_email, c.customer_gender as c_gen, DATE_FORMAT( c.customer_dob,  '%M %e %Y' ) AS c_dob, l.login_api_user_id AS c_api_id, l.login_api_user_pic AS c_pic, COUNT(DISTINCT m.mp_review_id) as reviews, COUNT(DISTINCT b.booking_id) AS redeemed FROM login_api l, customer c LEFT JOIN mp_reviews m ON c.customer_id = m.mp_review_customer_id LEFT JOIN booking b ON c.customer_id = b.booking_customer_id AND b.booking_status =1 WHERE l.login_api_customer_id = c.customer_id AND c.customer_id =:id GROUP BY c.customer_id LIMIT 1", $paramData);
			$response['c_id'] = $selectData[0]['c_id'];
			$response['data'] = $selectData;	
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 627;
			$response['message'] = "Data not updated!";
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'profileUpdateVerify');
		return $response;
	}
	
	function getCoupon($param = null)
	{
		$request = json_encode($param);
		$checkParam = array("booking_customer_id"=>$param['c_id'], "status"=>"0");
		$select = $GLOBALS["db"]->select("SELECT booking_id, booking_coupon_code, booking_offer_id, booking_mp_id  FROM booking WHERE booking_customer_id =:booking_customer_id AND booking_status =:status LIMIT 1", $checkParam);
		if(count($select)>0)
		{
			$paramData = array("o_id"=>$select[0]['booking_offer_id'], "mp_id"=>$select[0]['booking_mp_id']);
			//$selectMy = $GLOBALS["db"]->select("SELECT m.mp_details_name, o.deal_name, o.offer_discount, o.offer_type_id, o.offer_description, m.mp_details_address, m.mp_details_city, mp_details_latitude, mp_details_longitude, o.min_billing, o.offers_timeings, o.offers_day_type, o.offers_day_value, m.mp_accept_card, m.mp_details_category, m.mp_nonveg, m.mp_bar_available FROM mp_details m, offers o WHERE m.mp_details_id =o.offer_mp_id AND o.offer_id =:o_id AND m.mp_details_id =:mp_id LIMIT 1", $paramData);
			$selectMy = $GLOBALS["db"]->select("SELECT m.mp_details_name, o.offer_discount, o.offer_type_id, o.offer_description, m.mp_details_address, m.mp_details_city, m.mp_details_latitude, m.mp_details_longitude, o.min_billing, o.offers_timeings, o.offers_day_type, o.offers_day_value, m.mp_accept_card, m.mp_details_category, m.mp_nonveg, m.mp_bar_available FROM mp_details m, offers o WHERE m.mp_details_id =o.offer_mp_id AND o.offer_id =:o_id AND m.mp_details_id =:mp_id LIMIT 1", $paramData);
			
			if($selectMy[0]['offer_type_id']==1)
			{
				$selectMy[0]['offer_description'] = "Get ".$selectMy[0]['offer_discount']."% Off";
			}				
			$response = array("success"=>true);
			$response['code'] = 680;
			$response['booking_id'] = $select[0]['booking_id'];
			$response['voucher'] = $select[0]['booking_coupon_code'];
			$response['mp_name'] = $selectMy[0]['mp_details_name'];
			//$response['o_title'] = $selectMy[0]['deal_name'];
			$response['o_desc'] = $selectMy[0]['offer_description'];
			$response['mp_address'] = $selectMy[0]['mp_details_address'].','.$selectMy[0]['mp_details_city'];
			$response['mp_latitude'] = $selectMy[0]['mp_details_latitude'];
			$response['mp_longitude'] = $selectMy[0]['mp_details_longitude'];
			
			//for storing data into needs array
						$needs = array();
										 
						/*if($selectMy[0]['min_billing']!=NULL)
						{
							$Txt = 'Minimum bill should be '.$selectMy[0]['min_billing'];
							array_push($needs, $Txt);
						}	*/			  
						if($selectMy[0]['offers_day_type']==2)
						{
							$offerValid = "Weekdays only";
							$offerTime = $selectMy[0]['offers_timeings'];
						}
						else if($selectMy[0]['offers_day_type']==3)
						{
							 $offerValid = "Weekends only";
							 $offerTime = $selectMy[0]['offers_timeings'];
						}
						else
						{
							 $offerValid = $selectMy[0]['offers_day_value'];
							 $offerTime = $selectMy[0]['offers_timeings'];
						}
						
						$offerValidOn = "Offer valid on ".$offerValid;
						$offerTimeOn = "Offer Timings are ".$offerTime;
						array_push($needs, $offerValidOn);
						array_push($needs, $offerTimeOn);
						$response['deal'] = $needs;
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 681;
			$response['message'] = "No coupon exist!";
		}
		$jsonInsert = self::capture_json($request, json_encode($response), 'getCoupon');
		return $response;
	}
	function capture_json($json_request, $json_response, $api_name = null)
	{
		$array = array("json_request"=>$json_request, "json_response"=>$json_response, "json_api_name"=>$api_name);
		$insertCust = $GLOBALS["db"]->lastInsertNow("api_request", $array, "date");
	}
	function getMerchantName()
	{
		$param = array("status"=>1);
		$select = $GLOBALS["db"]->select("SELECT DISTINCT m.mp_details_name as mp_name, o.offer_status FROM mp_details m, category c, offers o WHERE m.mp_details_id = o.offer_mp_id AND m.mp_details_category =c.category_id AND c.category_status =:status AND m.mp_status=:status HAVING offer_status>0", $param);
		if(count($select)>0)
		{
			$response = array("success"=>true);
			$response['code'] = 650;
			$response['data'] = $select;
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 651;

		}
		return $response;
	}
	function getMerchantAddress($param=null)
	{
		$merchant_address = explode(',', $param["merchant_location"]);
		$param = array("merchant_name"=>$param["merchant_name"], "merchant_latitude"=>$merchant_address[0],"merchant_longitude"=>$merchant_address[1]);
		$insertCust = $GLOBALS["db"]->lastInsertNow("get_merchant_address", $param, 'date');
		if($insertCust)
		{
			$response = array("success"=>true);
			$response['code'] = 652;
		}
		else
		{
			$response = array("success"=>true);
			$response['code'] = 653;
		}
		return $response;
	}
	function sendGCMNearBy($param = null)
	{
		$paramData = array("status"=>1);	
		$location = explode(',', $param['location']);
		$device_lat = $location[0];
		$device_long = $location[1];
		$param['radius'] = round(($param['radius']/1000), 2);
		$data = $GLOBALS["db"]->select("SELECT mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name, ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist FROM category c, deal_gallery dg, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id WHERE c.category_id = mp.mp_details_category AND dg.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status AND d.deal_status =:status GROUP BY mp.mp_details_id HAVING mp_dist<".$param['radius']." ORDER BY mp_dist ASC", $paramData);
		if(count($data)>0)
			{	
			
				//get image from geofence table
				$noData = array();
				$selectGeo =  $GLOBALS["db"]->select("SELECT geofence_name, geofence_image FROM geofence WHERE geofence_location='".$param['location']."' LIMIT 1", $noData);
				
				//send gcm notifcation here
				$selectToken =  $GLOBALS["db"]->select("SELECT device_token FROM device WHERE device_id=".$param['device_id'], $noData);
				$registrationIds = array($selectToken[0]['device_token']);
				
				if(count($selectGeo)>0)
				{
					$smallIcon = $selectGeo[0]['geofence_image'];
					$title = count($data).' Offers at '.$selectGeo[0]['geofence_name'];
					$message = "Enjoy offers at ".$selectGeo[0]['geofence_name'];
				}
				else
				{
					$smallIcon = 'http://www.travelfromindiatovietnam.com/wp-content/uploads/2014/11/tandoor-restaurant-hanoi2.jpg';
					$title = count($data).' Offers near you';
					$message = 'Enjoy and have fun!';
				}
					// prep the bundle
					$msg = array
					(
						'title'		=> $title,
						'message' 	=> $message,
						'smallIcon'	=> $smallIcon,
						'location'=>$param['location'],
						'radius'=>$param['radius'],
						'code'=>'102'
					);
					$fields = array
					(
						'registration_ids' 	=> $registrationIds,
						'data'			=> $msg
					);
					 
					$headers = array
					(
						'Authorization: key=' . APP_GOOGLE_KEY,
						'Content-Type: application/json'
					);
					 
					$ch = curl_init();
					curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
			}
			else
			{
				$result = array("success"=>true);
				$result['code'] = 660;
				$result['message'] = "No Data Exist";
			}
			return $result;
	}
	function sendGCMNotification($param = null)
	{
		$paramData = array("c_id"=>$param['c_id'], "status"=>1);	
		$location = explode(',', $param['location']);
		$device_lat = $location[0];
		$device_long = $location[1];
		$param['radius'] = round(($param['radius']/1000), 2);
		$data = $GLOBALS["db"]->select("SELECT c.category_name AS mp_cat, mp.mp_details_id AS mp_id, mp.mp_details_name AS mp_name,mp.mp_opentime, mp.mp_closetime, o.offer_description AS o_desc, d.deal_id AS deal_id, d.deal_name, mp.mp_details_description as o_gen_desc, o.offer_type_id, o.offer_discount as o_disc, d.deal_trending_image as o_trending_banner, d.is_trending, d.deal_image_category AS img_category, o.offers_day_type, o.offers_day_value, o.offers_timeings, d.deal_image_thumb AS img_all_thumb,ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(mp.mp_details_latitude)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(mp.mp_details_longitude - ".$device_long."))+ SIN(RADIANS(mp.mp_details_latitude))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist, b.bookmark_id, d.deal_type, d.deal_shoutout_day, d.deal_shoutout_time, d.deal_shoutout_description FROM category c, deal_gallery og, deal d INNER JOIN mp_details mp ON d.deal_mp_id = mp.mp_details_id LEFT JOIN offers o ON o.offer_deal_id = d.deal_id AND o.offer_status =:status LEFT JOIN bookmark b ON d.deal_id = b.bookmark_deal_id LEFT JOIN customer ct ON b.bookmark_customer_id = ct.customer_id AND b.bookmark_customer_id =:c_id WHERE c.category_id = mp.mp_details_category AND og.deal_id = d.deal_id AND c.category_status =:status AND mp_status =:status GROUP BY mp_details_id HAVING mp_dist<".$param['radius']." ORDER BY mp_dist ASC", $paramData);
		if(count($data)>0)
			{	
				$category_array = array();
							foreach($data as $a)
							{
								if($a['deal_type']==1)
								{
									if($a['offers_day_type']==5)
									{
										$day_type = explode('-', $a['offers_day_value']);
										if($a['offers_timeings']=="All Day")
										{
											$a['offers_timeings'] = $a['mp_opentime'].'-'.$a['mp_closetime'];
										}
										$timings = explode('-', $a['offers_timeings']);
										$var = new DateTime('now');
										$starTime =  $var->format('Y-m-d H:i:s');		
										$day_type[1] = str_replace('/', '-', $day_type[1]);			
										//$format =  date("Y-m-d", strtotime($day_type[1]));
										$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
										if($starTime<$format)
										{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
											if($a['offer_type_id'] == 1)
											{
												$a['o_desc'] = "Get ".$a['o_disc']."% Off";
											}
											unset($a['o_disc']);
											unset($a['offer_type_id']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['offers_day_type']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array[]= $a;
										}
									}
									else
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										if($a['offer_type_id'] == 1)
										{
											$a['o_desc'] = "Get ".$a['o_disc']."% Off";
										}
										unset($a['o_disc']);
										unset($a['offer_type_id']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[]= $a;
									}
								}
								else
								{
									if(strpos($a['deal_shoutout_day'],'/'))
									{
										$day_type = explode('-', $a['deal_shoutout_day']);
										$timings = explode('-', $a['deal_shoutout_time']);
										$var = new DateTime('now');
										$starTime =  $var->format('Y-m-d H:i:s');		
										$day_type[1] = str_replace('/', '-', $day_type[1]);			
										//$format =  date("Y-m-d", strtotime($day_type[1]));
										$format = date("Y-m-d H:i:s", strtotime($day_type[1].' '.$timings[1]));
										if($starTime<$format)
										{
											$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
											$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
											$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
											$a['o_desc'] = $a['deal_shoutout_description'];
											unset($a['o_disc']);
											unset($a['offer_type_id']);
											unset($a['offers_day_value']);
											unset($a['offers_timeings']);
											unset($a['offers_day_type']);
											unset($a['deal_shoutout_day']);
											unset($a['deal_shoutout_time']);
											unset($a['deal_shoutout_description']);
											unset($a['mp_opentime']);
											unset($a['mp_closetime']);
											$category_array[]= $a;
										}
									}
									else
									{
										$a['img_category'] = APP_CRM_UPLOADS_PATH.$a['img_category'];
										$a['img_all_thumb'] = APP_CRM_UPLOADS_PATH.$a['img_all_thumb'];
										$a['o_trending_banner'] = APP_CRM_UPLOADS_PATH.$a['o_trending_banner'];	
										$a['o_desc'] = $a['deal_shoutout_description'];
										unset($a['o_disc']);
										unset($a['offer_type_id']);
										unset($a['offers_day_value']);
										unset($a['offers_timeings']);
										unset($a['offers_day_type']);
										unset($a['deal_shoutout_day']);
										unset($a['deal_shoutout_time']);
										unset($a['deal_shoutout_description']);
										unset($a['mp_opentime']);
										unset($a['mp_closetime']);
										$category_array[]= $a;
									}
								}
							}			
				$response = array("success"=>true);
				$response['code'] = 659;
				$response['data'] = array_values($category_array);
			}
			else
			{
				$response = array("success"=>true);
				$response['code'] = 660;
				$response['message'] = "No Data Exist";
			}
			return $response;
	}
	function getStoreData($param = null)
	{
		$response = array("success"=>true);
		$response['code'] = 670;
		$param1 = array();
		$data1 = $GLOBALS["db"]->select("SELECT s . * , COUNT( coupon_id ) AS cnt FROM store s, coupons c WHERE c.store_id = s.store_id GROUP BY s.store_id LIMIT 15", $param1);
		for($i=0; $i<count($data1); $i++)
		{
			$data2[] = $GLOBALS["db"]->select("SELECT c.*, s.store_logo, s.store_link FROM coupons c, store s WHERE s.store_id = c.store_id AND c.store_id = '".$data1[$i]['store_id']."' AND c.validity_date >= CURDATE() LIMIT 2", $param1);
		}
		$array = array();
		foreach($data2 as $a1)
		{
			foreach($a1 as $a)
			{
				$array[] = $a;
			}
		}
		$data3 = array_values($array);
		$response['top_offers'] = $data3;
		$response['top_stores'] = $data1;
		$param = array("status"=>0);
		$data = $GLOBALS["db"]->select("SELECT cat_name FROM product_category WHERE cat_parent=:status", $param);
		/*$category_array = array();
		for($i=0; $i<count($data); $i++)
		{
			$par = array();
			$data1 = $GLOBALS["db"]->select("SELECT c.*, s.store_logo, s.store_link FROM coupons c, store s WHERE s.store_id = c.store_id AND cats_text LIKE '%".$data[$i]['cat_name']."%' AND c.validity_date >= CURDATE()", $par);			
			foreach($data1 as $a)
			{
				$category_array[$data[$i]['cat_name']]['name']= $data[$i]['cat_name'];
				$category_array[$data[$i]['cat_name']]['data'][]= $a;
			}
		}*/
		
		$data4[0]['cat_name']= 'Top Offers';
		$data5[0]['cat_name']= 'Top Stores';
		$myData = array_merge($data4, $data5, $data); 
		$response['category'] = $myData;		
		//$response['count'] = $getCount;
		return $response;
	}
	function getDataByCategory($param = null)
	{
		$par = array();
		$param['cat_name'] = $param[0];
		$response = array("success"=>true);
		$response['code'] = 699;
		$data = $GLOBALS["db"]->select("SELECT c.*, s.store_logo, s.store_link FROM coupons c, store s WHERE s.store_id = c.store_id AND cats_text LIKE '%".$param['cat_name']."%' AND c.validity_date >= CURDATE()", $par);		
		/*
		if($param['cat_name']!="Top Offers")
		{
			$data = $GLOBALS["db"]->select("SELECT c.*, s.store_logo, s.store_link FROM coupons c, store s WHERE s.store_id = c.store_id AND cats_text LIKE '%".$param['cat_name']."%' AND c.validity_date >= CURDATE()", $par);	
		}
		else
		{
			$param1 = array();
			$data1 = $GLOBALS["db"]->select("SELECT s.store_id FROM store s, coupons c WHERE c.store_id = s.store_id GROUP BY s.store_id LIMIT 15", $param1);
			for($i=0; $i<count($data1); $i++)
			{
				$data2[] = $GLOBALS["db"]->select("SELECT c.*, s.store_logo, s.store_link FROM coupons c, store s WHERE s.store_id = c.store_id AND c.store_id = '".$data1[$i]['store_id']."' AND c.validity_date >= CURDATE() LIMIT 2", $par);
			}
			$array = array();
			foreach($data2 as $a1)
			{
				foreach($a1 as $a)
				{
					$array[] = $a;
				}
			}
			$data = array_values($array);
		}*/
		$response['data'] = $data;
		return $response;
	}
	function getStoreValue($param = null)
	{
		$response = array("success"=>true);
		$param['store_id'] = $param[0];
		$response['code'] = 671;
		$param = array("store_id"=>$param['store_id']);
		$data1 = $GLOBALS["db"]->select("SELECT c.*, s.store_logo, s.store_link FROM coupons c, store s WHERE s.store_id = c.store_id AND s.store_id =:store_id AND c.validity_date >= CURDATE()", $param);
		$response['data'] = $data1;
		return $response;
	}
}
?>