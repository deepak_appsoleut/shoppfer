<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class login_model {

	function checkLogin()
	{
		$math = new math();
		$password = $math->hash("sha1", $_POST['password'], APP_SEC_KEY);
		$param = array("user_email"=>$_POST['email'], "pass"=>$password, "status"=>1);
		$data = $GLOBALS["db"]->select("SELECT u.*, r.crm_role_name FROM user u, crm_role r WHERE  u.user_email_id =:user_email AND u.user_password =:pass AND u.user_status =:status AND r.crm_role_status =:status AND u.user_role_id = r.crm_role_id", $param);
		return $data;
	}
}
?>