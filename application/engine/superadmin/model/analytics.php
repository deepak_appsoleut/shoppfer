<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class analytics_model {

	function analytics()
	{
		$param = array();
		// fetch all the notifications from notification table
		$data = $GLOBALS["db"]->select("SELECT d.device_address, d.device_name, d.device_location, d.device_created_on, d.device_version, c.customer_name, c.customer_mobile, c.customer_gender FROM device d LEFT JOIN cust_device_relation cd ON d.device_id = cd.device_id LEFT JOIN customer c ON c.customer_id = cd.cust_id", $param);
		return $data;
	}
}
?>