<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class image_upload_model {

	function image_upload()
	{
		$param = array("status"=>1);
		$data = $GLOBALS["db"]->select("SELECT a.an_process, a.an_id, m.mp_details_name, d.deal_name, a.an_created_time, d.deal_id, d.deal_image_status FROM deal_temp d, mp_details_temp m, admin_notification a WHERE d.deal_mp_id = m.mp_details_id AND d.deal_status =:status AND m.mp_status =:status AND a.an_mp_user_id = d.deal_id AND (a.an_process ='create_deal' || ((a.an_process ='deal_update') && (a.is_drag = 1))) AND an_status = 0", $param);
		return $data;
	}
	function view_images($id)
	{
		$param = array("id"=>$id);
		$data = $GLOBALS["db"]->select("SELECT d.*, dt.deal_image_category, dt.deal_image_thumb FROM deal_gallery_temp d, deal_temp dt WHERE d.deal_id =:id AND dt.deal_id =:id AND d.deal_id = dt.deal_id", $param);
		return $data;
	}
	function view_updated_images($id)
	{
		$param = array("id"=>$id);
		$data = $GLOBALS["db"]->select("SELECT o.*, ot.deal_image_category, ot.deal_image_thumb FROM deal_gallery_temp o, deal_temp ot WHERE o.deal_id =:id AND ot.deal_id =:id AND o.deal_id = ot.deal_id", $param);
		return $data;
	}
	function upload_process()
	{
		if($_POST['fileType']=="category")
		{
			//save into category
			$manipulatorCat = new imageManipulator($_FILES['category']['tmp_name']);
			$newNamePrefix = time() . '_';
			$save = $manipulatorCat->save(APP_CRM_UPLOADS.'category/'.$newNamePrefix.$_FILES['category']['name']);
			// update data
			$updateParams = array('deal_image_category'=>'category/'. $newNamePrefix.$_FILES['category']['name']); 
			$condition = array('deal_id'=>$_POST['deal_id']);
			$update = $GLOBALS["db"]->update('deal_temp', $updateParams, $condition);		
			return $update;
		}
		else if($_POST['fileType']=="thumb")
		{
			//save into category
			$manipulatorCat = new imageManipulator($_FILES['thumb']['tmp_name']);
			$newNamePrefix = time() . '_';
			$save = $manipulatorCat->save(APP_CRM_UPLOADS.'thumb/'.$newNamePrefix.$_FILES['thumb']['name']);
			// update data
			$updateParams = array('deal_image_thumb'=>'thumb/'. $newNamePrefix.$_FILES['thumb']['name']); 
			$condition = array('deal_id'=>$_POST['deal_id']);
			$update = $GLOBALS["db"]->update('deal_temp', $updateParams, $condition);		
			return $update;
		}
		else if($_POST['fileType']=="details")
		{
			$image	=	$_POST["deal_gallery_id"];
			$det_temp = $_FILES['det'.$image]['tmp_name'];
			$det_name = $_FILES['det'.$image]['name'];
			//save into category
			$manipulatorCat = new imageManipulator($det_temp);
			$newNamePrefix = time() . '_';
			$save = $manipulatorCat->save(APP_CRM_UPLOADS.'details/'.$newNamePrefix.$det_name);
			// update data
			$updateParams = array('deal_image_details'=>'details/'.$newNamePrefix.$det_name); 
			$condition = array('deal_gallery_id'=>$_POST['deal_gallery_id']);
			$update = $GLOBALS["db"]->update('deal_gallery_temp', $updateParams, $condition);		
			return $update;
		}
	}
	function upload_status()
	{
		// update data
			$updateParams = array('deal_image_status'=>1); 
			$condition = array('deal_id'=>$_POST['deal_id']);
			$update = $GLOBALS["db"]->update('deal_temp', $updateParams, $condition);	
			return $update;
	}
	function upload_new_process()
	{
		if($_POST['fileType']=="cateoryImage")
		{
			//save into category
			$manipulatorCat = new imageManipulator($_FILES['category']['tmp_name']);
			$newNamePrefix = time() . '_';
			$save = $manipulatorCat->save(APP_CRM_UPLOADS.'category/'. $newNamePrefix.$_FILES['category']['name']);
			// update data
			$updateParams = array('deal_image_category'=>'category/'. $newNamePrefix.$_FILES['category']['name']); 
			$condition = array('deal_id'=>$_POST['deal_id']);
			$update = $GLOBALS["db"]->update('deal_temp', $updateParams, $condition);		
			$update = $GLOBALS["db"]->update('deal', $updateParams, $condition);		
			return $update;
		}
		else if($_POST['fileType']=="thumbImage")
		{
			//save into category
			$manipulatorCat = new imageManipulator($_FILES['thumb']['tmp_name']);
			$newNamePrefix = time() . '_';
			$save = $manipulatorCat->save(APP_CRM_UPLOADS.'thumb/'. $newNamePrefix.$_FILES['thumb']['name']);
			// update data
			$updateParams = array('deal_image_thumb'=>'thumb/'. $newNamePrefix.$_FILES['thumb']['name']); 
			$condition = array('deal_id'=>$_POST['deal_id']);
			$update = $GLOBALS["db"]->update('deal_temp', $updateParams, $condition);	
			$update = $GLOBALS["db"]->update('deal', $updateParams, $condition);			
			return $update;
		}
		else if($_POST['fileType']=="detailsImage")
		{
			$image	=	$_POST["deal_gallery_id"];
			$det_temp = $_FILES['det'.$image]['tmp_name'];
			$det_name = $_FILES['det'.$image]['name'];
			//save into category
			$manipulatorCat = new imageManipulator($det_temp);
			$newNamePrefix = time() . '_';
			$save = $manipulatorCat->save(APP_CRM_UPLOADS.'details/'.$newNamePrefix.$det_name);
			// update data
			$updateParams = array('deal_image_details'=>'details/'.$newNamePrefix.$det_name); 
			$condition = array('deal_gallery_id'=>$_POST['deal_gallery_id']);
			$update = $GLOBALS["db"]->update('deal_gallery_temp', $updateParams, $condition);	
			$update = $GLOBALS["db"]->update('deal_gallery', $updateParams, $condition);	
			return $update;
		}
	}
}
?>