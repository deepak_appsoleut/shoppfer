<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class send_notification_model {

	function send_notification()
	{
		$param = array("status"=>1);
		$data =  $GLOBALS["db"]->select("SELECT DISTINCT(m.mp_details_name), m.mp_details_id FROM mp_details m, offers o WHERE o.offer_mp_id = m.mp_details_id AND o.offer_status =:status AND m.mp_status =:status", $param);
		return $data;
	}
	function filterData()
	{
		$select = "";
		$having = "";
		$param = array();
		if($_POST['user_type']=="all")
		{
			$query = " WHERE 1";
		}
		else if($_POST['user_type']=="registered")
		{
			$query = ", cust_device_relation cd WHERE d.device_id = cd.device_id";	
		}
		else if($_POST['user_type']=="not-registered")
		{
			$query = " WHERE NOT EXISTS(SELECT * FROM cust_device_relation cd WHERE d.device_id = cd.device_id)";
		}
		else if($_POST['user_type']=="merchant")
		{
			$selectLocation = $GLOBALS["db"]->select("SELECT mp_details_latitude, mp_details_longitude FROM mp_details WHERE mp_details_id =".$_POST['merchant_id'], $param);
			$device_lat = $selectLocation[0]['mp_details_latitude'];
			$device_long = $selectLocation[0]['mp_details_longitude'];
			$select = ", ROUND(COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(d.device_lat)) * COS(RADIANS(".$device_lat.")) * COS(RADIANS(d.device_long - ".$device_long."))+ SIN(RADIANS(d.device_lat))* SIN(RADIANS(".$device_lat.")))), 0), 2) AS mp_dist";
			$having = "HAVING mp_dist<4";
			$query = " WHERE 1";
		}
		if(trim($_POST["location"])!="")
		{
			$select = "";
			$having = "";
			$query .=" AND d.device_address LIKE '%".$_POST["location"]."%'";  
		}		
		//$data[0] = "SELECT d.device_id ".$select." FROM device d".$query." ".$having." ORDER BY d.device_id ASC";
		$data[0] = $GLOBALS["db"]->select("SELECT d.device_id ".$select." FROM device d".$query." ".$having." ORDER BY d.device_id ASC", $param);
		$data[1] = "SELECT d.device_token, d.device_id ".$select." FROM device d".$query." ".$having." ORDER BY d.device_id ASC";
		return $data;
	}
	function upload_process()
	{
		//return json_encode($_FILES['file']['name']);
		
		$newNamePrefix 	= time() . '_';
		$manipulator 	= new imageManipulator($_FILES['file']['tmp_name']);
		$imageName 		= 'notify/'.$newNamePrefix . str_replace(' ', '_', $_FILES['file']['name']);
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'notify/' . $newNamePrefix .  str_replace(' ', '_', $_FILES['file']['name']));
		return $imageName;
	}
	function send_process()
	{
		//get offer id of merchant
		$offer_id = NULL;
		$mp_details_name = NULL;
		$code = 101;
		if($_POST['name']!="")
		{
			$mpParam = array("merchant_id"=>$_POST['name'], "status"=>1);
			$data1 = $GLOBALS["db"]->select("SELECT o.offer_id, m.mp_details_name FROM offers o, mp_details m WHERE m.mp_details_id =:merchant_id AND o.offer_mp_id = m.mp_details_id AND o.offer_status =:status LIMIT 1", $mpParam);
			if(count($data1)>0)
			{
				$offer_id = $data1[0]['offer_id'];
				$mp_details_name = $data1[0]['mp_details_name'];
			}
			$code = 103;
		}
		
		$param = array("send_gcm_mp"=>$mp_details_name,"send_gcm_title"=>$_POST['title'], "send_gcm_desc"=>$_POST['description'],"send_gcm_image"=>$_POST['imageVal'],"send_gcm_type"=>$_POST['user_type'],"send_gcm_query"=>$_POST['query'],"send_gcm_count"=>$_POST['resultFound']);
		$insert = $GLOBALS["db"]->insert("send_gcm", $param);
		$paramData = array();
		$select = $GLOBALS["db"]->select($_POST['query'], $paramData);
		if(count($select)>0)
		{
				for($i=0; $i<count($select); $i++)
				{
					$registrationIds = array($select[$i]['device_token']);
					// prep the bundle
					$msg = array
					(
						'title'		=> $_POST['title'],
						'message' 	=> $_POST['description'],
						'smallIcon'	=> APP_CRM_UPLOADS_PATH.$_POST['imageVal'],
						'offer_id' 	=> $offer_id,
						'code'=>$code
					);
					$fields = array
					(
						'registration_ids' 	=> $registrationIds,
						'data'			=> $msg
					);
					 
					$headers = array
					(
						'Authorization: key=' . APP_GOOGLE_KEY,
						'Content-Type: application/json'
					);
					 
					$ch = curl_init();
					curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					echo $result;					
				}
		}
	}
}
?>