<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class dashboard_model {

	function dashboard()
	{
	}
	function dashboard_data()
	{
		$array = array();
		if(($_POST['startDate']==NULL)&&($_POST['endDate']==NULL))
		{
			$data[0] = $GLOBALS["db"]->select("SELECT count(d.device_id) TotalCount, SUM(CASE WHEN d.device_location = '0.0,0.0' THEN 1 ELSE 0 END) as noLocation, SUM(CASE WHEN (d.device_address LIKE '%Delhi%' || d.device_address LIKE '%Gurgaon%' || d.device_address LIKE '%Faridabad%' || d.device_address LIKE '%Noida%') THEN 1 ELSE 0 END) as delhi_ncr FROM device d", $array);
			$data[4] = $GLOBALS["db"]->select("SELECT count(c.customer_id) as customerCnt, SUM(CASE WHEN c.customer_gender = 'Male' THEN 1 ELSE 0 END) as MaleCount, SUM(CASE WHEN c.customer_gender = 'Female' THEN 1 ELSE 0 END) as FemaleCount , SUM(CASE WHEN l.login_api_via = 'google' THEN 1 ELSE 0 END) as googleCount, SUM(CASE WHEN l.login_api_via = 'facebook' THEN 1 ELSE 0 END) as facebookCount FROM customer c, login_api l WHERE c.customer_id = l.login_api_customer_id", $array);
			$data[1] = $GLOBALS["db"]->select("SELECT DATE(device_created_on) as cdate, COUNT(device_created_on) as totalCount FROM device GROUP BY DATE(device_created_on) ORDER BY device_created_on DESC LIMIT 10", $array);
			$data[2] = $GLOBALS["db"]->select("SELECT COUNT(device_name) as cnt, device_name FROM device GROUP BY device_name Order BY cnt DESC LIMIT 10", $array);
			$data[3] = $GLOBALS["db"]->select("SELECT SUM(CASE WHEN d.device_address LIKE '%Delhi%' THEN 1 ELSE 0 END) AS delhi, SUM(CASE WHEN d.device_address LIKE '%Gurgaon%' THEN 1 ELSE 0 END) AS gurgaon, SUM(CASE WHEN d.device_address LIKE '%Faridabad%' THEN 1 ELSE 0 END) AS faridabad , SUM(CASE WHEN d.device_address LIKE '%Noida%' THEN 1 ELSE 0 END) AS noida FROM device d", $array);
		}
		else
		{
			$startDate = date('Y-m-d', strtotime($_POST['startDate']));
			$endDate = date('Y-m-d', strtotime($_POST['endDate']));	
			$data[0] = $GLOBALS["db"]->select("SELECT count(d.device_id) TotalCount, SUM(CASE WHEN d.device_location = '0.0,0.0' THEN 1 ELSE 0 END) as noLocation, SUM(CASE WHEN (d.device_address LIKE '%Delhi%' || d.device_address LIKE '%Gurgaon%' || d.device_address LIKE '%Faridabad%' || d.device_address LIKE '%Noida%') THEN 1 ELSE 0 END) as delhi_ncr FROM device d WHERE DATE_FORMAT(d.device_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'", $array);
		$data[4] = $GLOBALS["db"]->select("SELECT count(c.customer_id) as customerCnt, SUM(CASE WHEN c.customer_gender = 'Male' THEN 1 ELSE 0 END) as MaleCount, SUM(CASE WHEN c.customer_gender = 'Female' THEN 1 ELSE 0 END) as FemaleCount , SUM(CASE WHEN l.login_api_via = 'google' THEN 1 ELSE 0 END) as googleCount, SUM(CASE WHEN l.login_api_via = 'facebook' THEN 1 ELSE 0 END) as facebookCount FROM customer c, login_api l WHERE c.customer_id = l.login_api_customer_id AND DATE_FORMAT(c.customer_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' ", $array);
		$data[1] = $GLOBALS["db"]->select("SELECT DATE(device_created_on) as cdate, COUNT(device_created_on) as totalCount FROM device WHERE DATE_FORMAT(device_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY DATE(device_created_on) ORDER BY device_created_on DESC LIMIT 10", $array);
		$data[2] = $GLOBALS["db"]->select("SELECT COUNT(device_name) as cnt, device_name FROM device WHERE DATE_FORMAT(device_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY device_name ORDER BY cnt DESC LIMIT 10", $array);
		$data[3] = $GLOBALS["db"]->select("SELECT SUM(CASE WHEN d.device_address LIKE '%Delhi%' THEN 1 ELSE 0 END) AS delhi, SUM(CASE WHEN d.device_address LIKE '%Gurgaon%' THEN 1 ELSE 0 END) AS gurgaon, SUM(CASE WHEN d.device_address LIKE '%Faridabad%' THEN 1 ELSE 0 END) AS faridabad , SUM(CASE WHEN d.device_address LIKE '%Noida%' THEN 1 ELSE 0 END) AS noida FROM device d WHERE DATE_FORMAT(d.device_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'", $array);
		}
		return $data;
	}
}
?>