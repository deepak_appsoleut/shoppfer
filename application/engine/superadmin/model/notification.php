<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class notification_model {

	function notification()
	{
		$param = array("view"=>0);
		
		// fetch all the notifications from notification table
		$data = $GLOBALS["db"]->select("SELECT an_id, an_process FROM admin_notification WHERE an_view =:view ORDER BY an_id DESC", $param);
		$mydata = array();
		for($i=0; $i<count($data);$i++)
		{
			if(($data[$i]['an_process']=="profile_complete")||($data[$i]['an_process']=="profile_update")||($data[$i]['an_process']=="signup")||($data[$i]['an_process']=="gallery_update"))
			{
				if($data[$i]['an_process']=="signup")
				{
					$mydata[] = $GLOBALS["db"]->select("SELECT m.mp_details_name as name, a.an_text, a.an_process, a.an_id, a.an_created_time FROM admin_notification a, mp_details_temp m WHERE m.mp_details_id = a.an_mp_user_id AND a.an_id = ".$data[$i]['an_id']." ORDER BY an_id DESC", $param);
				}
				else
				{
				  	$mydata[] = $GLOBALS["db"]->select("SELECT m.mp_details_name as name, c.category_name, a.an_text, a.an_process, a.an_id, a.an_created_time FROM admin_notification a, mp_details_temp m, category c WHERE m.mp_details_category = c.category_id AND m.mp_details_id = a.an_mp_user_id AND a.an_id = ".$data[$i]['an_id']." ORDER BY an_id DESC", $param);
				}
			}
			else
			{
				// insert offer id in an_mp_user_id while adding or updating deal
				$mydata[] = $GLOBALS["db"]->select("SELECT m.mp_details_name as name, c.category_name, a.an_text, a.an_process, a.an_id, a.an_created_time FROM admin_notification a, mp_details_temp m, offers_temp o, category c WHERE o.offer_id = a.an_mp_user_id AND m.mp_details_category = c.category_id AND o.offer_mp_id = m.mp_details_id AND a.an_id = ".$data[$i]['an_id']." ORDER BY an_id DESC", $param);
			}
		}
		if(!empty($mydata))
		{
			$category_array1 = array();
			foreach($mydata as $a1){
				foreach($a1 as $a){
					$category_array1[]= $a;
				}
			}
			return array_values($category_array1);	
		}
		else
		{
			return $mydata;
		}
	}
	function viewNotification()
	{
		$param = array("an_view"=>1);
		$condition = array("an_notify_from"=>'MP');
		$update = $GLOBALS["db"]->update("admin_notification", $param, $condition);
		return $update;
	}
}
?>