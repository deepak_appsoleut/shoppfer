<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class profile_model {
	
	function profile()
	{
		
	}
	function checkPassword()
	{
		$math = new math();
		$password = $math->hash("sha1", $_POST['old'], APP_SEC_KEY);
		if($password==$_SESSION['app_admin_user']['user_password'])
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function profileEdit()
	{
		$param = array("user_full_name"=>$_POST['user_full_name'], "user_mobile_number"=>$_POST['user_mobile_number'],"user_gender"=>$_POST['gender'], "user_modified_by"=>$_SESSION['app_admin_user']['user_id']);
		$condition = array("user_id"=>$_SESSION['app_admin_user']['user_id']);
		$resultUpdate = $GLOBALS["db"]->update('user', $param, $condition);
		if($resultUpdate=="true")
		{
			foreach($param as $k=>$v)
			{
				$_SESSION['app_admin_user'][$k]= $v;
			}
		}
		return $resultUpdate;
	}
	function changePassword()
	{
		$math = new math();
		$password = $math->hash("sha1", $_POST['new_password'], APP_SEC_KEY);
		$param = array("user_password"=>$password,"user_modified_by"=>$_SESSION['app_admin_user']['user_id']);
		$condition = array("user_id"=>$_SESSION['app_admin_user']['user_id']);
		$data = $GLOBALS["db"]->update("user", $param, $condition);
		if($data=="true")
		{
			$_SESSION['app_admin_user']['user_password'] = $password;
		}
		return $data;
	}
}
?>