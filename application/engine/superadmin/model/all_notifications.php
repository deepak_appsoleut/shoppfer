<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class all_notifications_model {

	function all_notifications()
	{
		$param = array("status"=>0);
		// fetch all the notifications from notification table
		$data = $GLOBALS["db"]->select("SELECT an_id, an_process FROM admin_notification WHERE an_status =:status ORDER BY an_id DESC", $param);
		$mydata = array();
		for($i=0; $i<count($data);$i++)
		{
			if(($data[$i]['an_process']=="profile_complete")||($data[$i]['an_process']=="profile_update")||($data[$i]['an_process']=="signup")||($data[$i]['an_process']=="gallery_update"))
			{
			$param = array();
			$mydata[] = $GLOBALS["db"]->select("SELECT m.mp_details_name as name, m.mp_details_email as email, a.an_text, a.an_process, a.an_id, a.an_created_time, a.an_status FROM admin_notification a, mp_details_temp m WHERE m.mp_details_id = a.an_mp_user_id AND a.an_id = ".$data[$i]['an_id']."", $param);
			}
		}
		if(!empty($mydata))
		{
			$category_array1 = array();
			foreach($mydata as $a1){
				foreach($a1 as $a){
					$category_array1[]= $a;
				}
			}
			return array_values($category_array1);	
		}
		else
		{
			return $mydata;
		}	
	}
	function view_profile($id)
	{
		$param = array("an_id"=>$id);
		// need to add table mp_details_sub_temp in place of mp_details_sub
		$data = $GLOBALS["db"]->select("SELECT m.*, an.an_id, c.category_name,  GROUP_CONCAT(DISTINCT(g.mp_gallery_id)) as mp_gallery_id, GROUP_CONCAT(DISTINCT(g.mp_gallery_image)) as gallery FROM category c, state st, admin_notification an, mp_details_temp m LEFT JOIN mp_gallery_temp g ON g.mp_gallery_mp_id = m.mp_details_id WHERE an.an_id =:an_id AND an.an_mp_user_id = m.mp_details_id AND m.mp_details_category = c.category_id GROUP BY m.mp_details_id", $param);
		return $data;
	}
	function view_update_profile($id)
	{
		$param = array("an_id"=>$id);
		$am = $GLOBALS["db"]->select("SELECT an_text FROM admin_notification WHERE an_id =:an_id", $param);
		//for profile update
		$from = "mp_details_temp m";
		$where = " an.an_id =:an_id AND an.an_mp_user_id = m.mp_details_id";
		$explode = explode(',', $am[0]['an_text']);
			for($i=0; $i<count($explode);$i++)
			{
				if($explode[$i]=="mp_details_category")
				{
					$explode[$i] = "c.category_name,m.mp_details_category";
					$from .=", category c";
					$where .=" AND m.mp_details_category = c.category_id";
				}
				else
				{
					$explode[$i] = "m.".$explode[$i];
				}
			}
			$implode = implode(',', $explode);	
			$data = $GLOBALS["db"]->select("SELECT m.mp_details_id, ".$implode." FROM  ".$from." ,admin_notification an WHERE ".$where."  GROUP BY m.mp_details_id", $param);
			$data[0]['an_id'] = $id;
			$data[0]['query'] = "SELECT m.mp_details_id, ".$implode." FROM  ".$from." ,admin_notification an WHERE ".$where."  GROUP BY m.mp_details_id";
			return $data;		
	}
	function view_gallery_profile($id)
	{
		$param = array("an_id"=>$id);
		$data = $GLOBALS["db"]->select("SELECT an.an_mp_user_id as mp_details_id, an_id, an.an_text FROM admin_notification an WHERE an_id =:an_id", $param);
		return $data;
	}
	function verifyCompleteStatus()
	{
		// if values are verified
		if($_POST["btn_value"]==="v")
		{
			$param = array("mp_details_id"=>$_POST['mp_details_id']);
			$fetchValueFirst = $GLOBALS["db"]->select("SELECT mp_details_name, mp_details_id, mp_details_person, mp_details_phone,mp_details_contact,mp_details_website,mp_details_country,mp_details_city,mp_details_address,mp_details_address1,mp_details_category,mp_details_state,mp_details_designation,mp_details_description,mp_details_recommend,mp_details_latitude,mp_details_longitude, mp_opentime, mp_closetime, mp_accept_card, mp_nonveg, mp_bar_available FROM mp_details_temp WHERE mp_details_id =:mp_details_id", $param);
			if(count($fetchValueFirst)>0)
			{
				$array = array();
				foreach($fetchValueFirst as $key=>$val)
				{
					foreach($val as $k=>$v)
					{
						$array[$k]=$v;
					}
				}
				$array['mp_details_modified_by'] = $_SESSION['app_admin_user']['user_id'];
				$array['mp_status'] = 1;
				$data = $GLOBALS["db"]->insert("mp_details", $array);
				if($data==1)
				{
					//mp temp process update
					$datatoUpdate = array("mp_process"=>0);
					$dataGet = $GLOBALS["db"]->update("mp_details_temp", $datatoUpdate, $param);
					
					//insertion in sub category of mp details table
					$mpSubCat = explode(',', $_POST['sub_category']);
					$mpSub = explode(',', $_POST['sub_cat_id']);
					
					for($i=0; $i<count($mpSubCat);$i++)
					{
						$mpArray = array("mp_details_mp_id"=>$_POST['mp_details_id'], "mp_details_sub_id"=>$mpSub[$i], 'mp_details_sub_cat_id'=>$mpSubCat[$i]);
						$insertMpArray = $GLOBALS["db"]->insert("mp_details_sub", $mpArray);
					}
		
					//entry in mp notification table
					$responseText = "profile_complete";
					$mpNot = array('mpn_response'=>1, 'mpn_user_id'=>$_POST['mp_details_id'], 'mpn_process'=>$responseText);
					$insertMpNot = $GLOBALS["db"]->insert("mp_notification", $mpNot);
					
					//admin notification update
					$adminNot = array("an_status"=>1);
					$condAdmin = array("an_id"=>$_POST["an_id"]);
					$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
					
					// entry in mp gallery
					$gallery = explode(',', $_POST['gallery']);
					$galleryArray = explode(',', $_POST['mp_gallery_id']);
					for($i=0; $i<count($gallery); $i++)
					{
						$galleryThumb = $gallery[$i];
						$galleryImage = explode('/', $galleryThumb);
						$actualImage = 'media/'.$galleryImage[1];
						$insertArray = array('mp_gallery_mp_id'=>$_POST['mp_details_id'], 'mp_gallery_id'=>$galleryArray[$i], 'mp_gallery_image'=>$actualImage, 'mp_gallery_thumb'=>$galleryThumb, 'mp_gallery_created_by'=>$_SESSION['app_admin_user']['user_id'], 'mp_gallery_modified_by'=>$_SESSION['app_admin_user']['user_id']);
						$insertAdNot = $GLOBALS["db"]->insert("mp_gallery", $insertArray);	
					}
					return $insertAdNot;
				}
			}			
		}
		// if values are not verified
		else
		{
			$param = array("mp_details_id"=>$_POST['mp_details_id']);
			$fetchValueFirst = $GLOBALS["db"]->select("SELECT mp_details_name FROM mp_details WHERE mp_details_id =:mp_details_id", $param);
			if(count($fetchValueFirst)>0)
			{
				$array = array();
				foreach($fetchValueFirst as $key=>$val)
				{
					foreach($val as $k=>$v)
					{
						$array[$k]=$v;
					}
				}
				$array['mp_process'] = 1;
				$array['mp_details_step'] = 1;
				$array['mp_details_step_complete'] = 0;
				$data = $GLOBALS["db"]->update("mp_details_temp", $array, $param);
				if($data=="true")
				{
					//entry in mp notification table
					$responseText = "profile_complete";
					$mpNot = array('mpn_response'=>0, 'mpn_user_id'=>$_POST['mp_details_id'], 'mpn_process'=>$responseText);
					$insertMpNot = $GLOBALS["db"]->insert("mp_notification", $mpNot);
					
					//admin notification update
					$adminNot = array("an_status"=>1);
					$condAdmin = array("an_id"=>$_POST["an_id"]);
					$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);	
					return $adminUpdate;
				}
			}
		}
	}
	function verifyUpdateStatus()
	{
		// if values are verified
		if($_POST["btn_value"]==="v")
		{
			$param = array("an_id"=>$_POST['an_id']);
			$fetchValueFirst = $GLOBALS["db"]->select($_POST['query'], $param);
			if(count($fetchValueFirst)>0)
			{
				$array = array();
				foreach($fetchValueFirst as $key=>$val)
				{
					foreach($val as $k=>$v)
					{
						if(($k!="state_name")&&($k!="category_name")&&($k!="mp_sub_category")&&($k!="sub_category_id"))
						{
							$array[$k]=$v;
						}
					}
				}
				$array['mp_details_modified_by'] = $_SESSION['app_admin_user']['user_id'];
				$array['mp_status'] =1;
				$param1 = array("mp_details_id"=>$_POST['mp_details_id']);
				//update mp details table
				$data = $GLOBALS["db"]->update("mp_details", $array, $param1);
				if($data=="true")
				{
					//check wheteher sub category id exist or not
					if(isset($fetchValueFirst[0]['sub_category_id']))
					{
						$exp = explode(',', $fetchValueFirst[0]['sub_category_id']);
						$param2 = array("mp_details_mp_id"=>$_POST['mp_details_id']);
						$result = $GLOBALS["db"]->deleteBulkQuery("mp_details_sub", $param2);
						for($j=0; $j<count($exp); $j++)
						{
							$paramater = array("mp_details_mp_id"=>$_POST['mp_details_id'], "mp_details_sub_cat_id"=>$exp[$j]);
							$result = $GLOBALS["db"]->insert("mp_details_sub", $paramater);
						}
					}
					//mp temp process update
					$datatoUpdate = array("mp_process"=>0);
					$dataGet = $GLOBALS["db"]->update("mp_details_temp", $datatoUpdate, $param1);
				
					
					//entry in mp notification table
					$responseText = "profile_update";
					$mpNot = array('mpn_response'=>1, 'mpn_user_id'=>$_POST['mp_details_id'], 'mpn_process'=>$responseText);
					$insertMpNot = $GLOBALS["db"]->insert("mp_notification", $mpNot);
					
					//admin notification update
					$adminNot = array("an_status"=>1);
					$condAdmin = array("an_id"=>$_POST["an_id"]);
					$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
					return $adminUpdate;
				}
			}			
		}
		// if values are not verified
		else
		{
			$param = array("an_id"=>$_POST['an_id']);
			$_POST['query'] = str_replace('_temp', '', $_POST['query']);
			$fetchValueFirst = $GLOBALS["db"]->select($_POST['query'], $param);
			if(count($fetchValueFirst)>0)
			{
				$array = array();
				foreach($fetchValueFirst as $key=>$val)
				{
					foreach($val as $k=>$v)
					{
						if(($k!="state_name")&&($k!="category_name")&&($k!="mp_sub_category")&&($k!="sub_category_id"))
						{
							$array[$k]=$v;
						}
					}
				}
				$array['mp_process'] =0;
				$param1 = array("mp_details_id"=>$_POST['mp_details_id']);
				//update mp details temp table
				$data = $GLOBALS["db"]->update("mp_details_temp", $array, $param1);
				if($data=="true")
				{
					//check wheteher sub category id exist or not
					if(isset($fetchValueFirst[0]['sub_category_id']))
					{
						$exp = explode(',', $fetchValueFirst[0]['sub_category_id']);
						$param2 = array("mp_details_mp_id"=>$_POST['mp_details_id']);
						$result = $GLOBALS["db"]->deleteBulkQuery("mp_details_sub_temp", $param2);
						for($j=0; $j<count($exp); $j++)
						{
							$paramater = array("mp_details_mp_id"=>$_POST['mp_details_id'], "mp_details_sub_cat_id"=>$exp[$j]);
							$result = $GLOBALS["db"]->insert("mp_details_sub_temp", $paramater);
						}
					}				
					
					//entry in mp notification table
					$responseText = "profile_update";
					$mpNot = array('mpn_response'=>0, 'mpn_user_id'=>$_POST['mp_details_id'], 'mpn_process'=>$responseText);
					$insertMpNot = $GLOBALS["db"]->insert("mp_notification", $mpNot);
					
					//admin notification update
					$adminNot = array("an_status"=>1);
					$condAdmin = array("an_id"=>$_POST["an_id"]);
					$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
					return $adminUpdate;
				}	
			}
		}
	}
	function verifyGalleryStatus()
	{
		// if values are verified
		if($_POST["btn_value"]==="v")
		{
			$param = array("mp_details_id"=>$_POST['mp_details_id'], "imageVal"=>'media/'.$_POST['imageVal']);
			$fetchValueFirst = $GLOBALS["db"]->select("SELECT mp_gallery_id FROM mp_gallery_temp WHERE mp_gallery_image =:imageVal AND mp_gallery_mp_id =:mp_details_id LIMIT 1", $param);
			if(count($fetchValueFirst)>0)
			{
				$param1 = array("mp_gallery_id"=>$fetchValueFirst[0]['mp_gallery_id']);
				$fetchValueSecond = $GLOBALS["db"]->select("SELECT mp_gallery_id FROM mp_gallery WHERE mp_gallery_id =:mp_gallery_id", $param1);
				$image = $_POST['imageVal'];
				if(count($fetchValueSecond)>0)
				{
					$cond = array("mp_gallery_id"=>$fetchValueSecond[0]['mp_gallery_id']);
					$updateParam = array("mp_gallery_id"=>$fetchValueFirst[0]['mp_gallery_id'], "mp_gallery_mp_id"=>$_POST['mp_details_id'], "mp_gallery_image"=>"media/".$image, "mp_gallery_thumb"=>"thumb/".$image, "mp_gallery_modified_by"=>$_SESSION['app_admin_user']['user_id']);
					$data = $GLOBALS["db"]->update("mp_gallery", $updateParam, $cond);
				}
				else
				{
					$insParam = array("mp_gallery_id"=>$fetchValueFirst[0]['mp_gallery_id'], "mp_gallery_mp_id"=>$_POST['mp_details_id'], "mp_gallery_image"=>"media/".$image, "mp_gallery_thumb"=>"thumb/".$image, "mp_gallery_created_by"=>$_SESSION['app_admin_user']['user_id'], "mp_gallery_modified_by"=>$_SESSION['app_admin_user']['user_id']);
					$data = $GLOBALS["db"]->insert("mp_gallery", $insParam);
				}
				
					//mp temp process update
					$datatoUpdate = array("mp_process"=>0);
					$dataGet = $GLOBALS["db"]->update("mp_details_temp", $datatoUpdate, $param1);
				
					
					//entry in mp notification table
					$responseText = "gallery_update";
					$mpNot = array('mpn_response'=>1, 'mpn_user_id'=>$_POST['mp_details_id'], 'mpn_process'=>$responseText);
					$insertMpNot = $GLOBALS["db"]->insert("mp_notification", $mpNot);
					
					//admin notification update
					$adminNot = array("an_status"=>1);
					$condAdmin = array("an_id"=>$_POST["an_id"]);
					$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
					return $adminUpdate;
			}			
		}
		// if values are not verified
		else
		{
			$param = array("mp_details_id"=>$_POST['mp_details_id'], "imageVal"=>$_POST['imageVal']);
			$fetchValueFirst = $GLOBALS["db"]->select("SELECT mp_gallery_id FROM mp_gallery_temp WHERE mp_gallery_image =:imageVal AND mp_gallery_mp_id =:mp_details_id LIMIT 1", $param);
			if(count($fetchValueFirst)>0)
			{
				$param1 = array("mp_gallery_id"=>$fetchValueFirst[0]['mp_gallery_id']);
				$fetchValueSecond = $GLOBALS["db"]->select("SELECT mp_gallery_id, mp_gallery_image FROM mp_gallery WHERE mp_gallery_id =:mp_gallery_id", $param1);
				$implode = explode('/', $fetchValueSecond[0]['mp_gallery_image']);
				$image = $implode[1];
				if(count($fetchValueSecond)>0)
				{
					$cond = array("mp_gallery_id"=>$fetchValueSecond[0]['mp_gallery_id']);
					$updateParam = array("mp_gallery_id"=>$fetchValueFirst[0]['mp_gallery_id'], "mp_gallery_mp_id"=>$_POST['mp_details_id'], "mp_gallery_image"=>"media/".$image, "mp_gallery_thumb"=>"thumb/".$image, "mp_gallery_modified_by"=>$_SESSION['app_admin_user']['user_id']);
					$data = $GLOBALS["db"]->update("mp_gallery_temp", $updateParam, $cond);
				}
				
					//mp temp process update
					$datatoUpdate = array("mp_process"=>0);
					$dataGet = $GLOBALS["db"]->update("mp_details_temp", $datatoUpdate, $param1);
				
					
					//entry in mp notification table
					$responseText = "gallery_update";
					$mpNot = array('mpn_response'=>0, 'mpn_user_id'=>$_POST['mp_details_id'], 'mpn_process'=>$responseText);
					$insertMpNot = $GLOBALS["db"]->insert("mp_notification", $mpNot);
					
					//admin notification update
					$adminNot = array("an_status"=>1);
					$condAdmin = array("an_id"=>$_POST["an_id"]);
					$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
					return $adminUpdate;
			}			
		}
	}
	function view_signup($id)
	{
		$param = array("an_id"=>$id);
		// need to add table mp_details_sub_temp in place of mp_details_sub
		$data = $GLOBALS["db"]->select("SELECT m.mp_details_name, m.mp_created_on, m.mp_details_email, a.an_id FROM mp_details_temp m, admin_notification a WHERE a.an_mp_user_id = m.mp_details_id AND a.an_id =:an_id AND a.an_process ='signup'", $param);
		return $data;
	}
	function verifySignupStatus()
	{
		$condition = array("an_id"=>$_POST['an_id']);
		$param = array("an_status"=>1);
		// need to add table mp_details_sub_temp in place of mp_details_sub
		$update = $GLOBALS["db"]->update("admin_notification", $param, $condition);
		return $update;
	}
	
}
?>