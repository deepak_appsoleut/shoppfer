<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class deal_notifications_model {

	function deal_notifications()
	{
		$param = array("status"=>0);
		// fetch all the notifications from notification table
		$data = $GLOBALS["db"]->select("SELECT an_id, an_process FROM admin_notification WHERE an_status=:status ORDER BY an_id DESC", $param);
		$mydata = array();
		for($i=0; $i<count($data);$i++)
		{
			$param = array();
			if(($data[$i]['an_process']=="create_deal")||($data[$i]['an_process']=="deal_update")||($data[$i]['an_process']=="offer_update")||($data[$i]['an_process']=="deal_status")||($data[$i]['an_process']=="deal_shoutout_update"))
			{
				if($data[$i]['an_process']!="offer_update")
				{
					$mydata[] = $GLOBALS["db"]->select("SELECT m.mp_details_name as name, m.mp_details_email as email, a.an_text, a.an_process, a.an_id, a.an_created_time,a.an_status, d.deal_image_status, d.deal_name FROM admin_notification a, mp_details_temp m, deal_temp d WHERE d.deal_id = a.an_mp_user_id AND d.deal_mp_id = m.mp_details_id AND a.an_id = ".$data[$i]['an_id']."", $param);
				}
				else
				{
					$mydata[] = $GLOBALS["db"]->select("SELECT m.mp_details_name as name, m.mp_details_email as email, a.an_text, a.an_process, a.an_id, a.an_created_time,a.an_status, d.deal_image_status, d.deal_name FROM admin_notification a, mp_details_temp m, deal_temp d, offers_temp o WHERE o.offer_deal_id = d.deal_id AND o.offer_id = a.an_mp_user_id AND d.deal_mp_id = m.mp_details_id AND a.an_id = ".$data[$i]['an_id']."", $param);
				}
			}			
		}
		if(!empty($mydata))
		{
			$category_array1 = array();
			foreach($mydata as $a1){
				foreach($a1 as $a){
					$category_array1[]= $a;
				}
			}
			return array_values($category_array1);	
		}
		else
		{
			return $mydata;
		}	
	}
	function deal_complete($id)
	{
		$param = array("an_id"=>$id);
		//$data = $GLOBALS["db"]->select("SELECT d.*, o.*, GROUP_CONCAT(DISTINCT(di.deal_image_media)) as deal_image_media, GROUP_CONCAT(DISTINCT(di.deal_image_details)) as deal_image_details, m.mp_details_name, m.mp_details_id,  a.an_id FROM admin_notification a, deal_temp d, mp_details_temp m, deal_gallery_temp di, offers_temp o WHERE a.an_id =:an_id AND m.mp_details_id = d.deal_mp_id AND a.an_mp_user_id = d.deal_id AND di.deal_id = d.deal_id AND o.offer_deal_id = d.deal_id GROUP BY o.offer_id", $param);
		$data = $GLOBALS["db"]->select("SELECT d.*, o.*, GROUP_CONCAT(DISTINCT(di.deal_image_media)) as deal_image_media, GROUP_CONCAT(DISTINCT(di.deal_image_details)) as deal_image_details, m.mp_details_name, m.mp_details_id,  a.an_id FROM admin_notification a, mp_details_temp m, deal_gallery_temp di, deal_temp d LEFT JOIN offers_temp o ON o.offer_deal_id = d.deal_id WHERE a.an_id =:an_id AND m.mp_details_id = d.deal_mp_id AND a.an_mp_user_id = d.deal_id AND di.deal_id = d.deal_id GROUP BY o.offer_id", $param);
		return $data;
	}
	function deal_update($id)
	{
		$param = array("an_id"=>$id[0]);
		if($id[1]=="offer_update")
		{
			$data = $GLOBALS["db"]->select("SELECT o.*, d.*, GROUP_CONCAT(dg.deal_image_details) as deal_image_details, m.mp_details_name, m.mp_details_id, a.an_id, a.an_text, a.is_drag, a.old_image, a.new_image, a.an_deal_type FROM admin_notification a, deal_gallery_temp dg, deal_temp d INNER JOIN offers_temp o ON d.deal_id = o.offer_deal_id INNER JOIN mp_details_temp m ON m.mp_details_id = o.offer_mp_id WHERE a.an_id =:an_id AND a.an_mp_user_id = o.offer_id AND dg.deal_id = d.deal_id", $param);
		}
		else
		{
			$data = $GLOBALS["db"]->select("SELECT o.*, d.*, GROUP_CONCAT(dg.deal_image_details) as deal_image_details, m.mp_details_name, m.mp_details_id, a.an_id, a.an_text, a.is_drag, a.old_image, a.new_image, a.an_deal_type FROM admin_notification a, deal_gallery_temp dg, deal_temp d LEFT JOIN offers_temp o ON d.deal_id = o.offer_deal_id LEFT JOIN mp_details_temp m ON m.mp_details_id = o.offer_mp_id WHERE a.an_id =:an_id AND a.an_mp_user_id = d.deal_id AND dg.deal_id = d.deal_id AND d.deal_mp_id = m.mp_details_id", $param);
		}
		return $data;
	}
	function dealStatus($id)
	{
		$param = array("an_id"=>$id);
		$data = $GLOBALS["db"]->select("SELECT d.deal_status, m.mp_details_name, m.mp_details_id, a.an_id, d.deal_id FROM admin_notification a, deal_temp d, mp_details_temp m WHERE a.an_id =:an_id AND m.mp_details_id = d.deal_mp_id AND a.an_mp_user_id = d.deal_id GROUP BY d.deal_id", $param);
		return $data;
	}
	function dealCompleteStatus()
	{		
		// if verify
		if($_POST["btn_value"]==="v")
		{
			$param = array("mp_details_id"=>$_POST['mp_details_id'], "deal_id"=>$_POST['deal_id']);
			$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_temp d WHERE d.deal_mp_id =:mp_details_id AND d.deal_id =:deal_id", $param);
			if(count($fetchValueFirst)>0)
			{
				//update offer data set to status 0 if any exist
				$param1 = array("mp_details_id"=>$_POST['mp_details_id'], "status"=>1);			
				$fetchdata = $GLOBALS["db"]->select("SELECT d.deal_id FROM deal d WHERE d.deal_mp_id =:mp_details_id AND d.deal_status =:status", $param1);
				if(count($fetchdata)>0)
				{
					for($i=0; $i<count($fetchdata); $i++)
					{
						$updateData = array("deal_status"=>0);
						$updatecond = array("deal_id"=>$fetchdata[$i]['deal_id']);
						$up = $GLOBALS["db"]->update("deal", $updateData, $updatecond);
					}
				}
				
				//insert data to deal
				$array = array();
				foreach($fetchValueFirst as $key=>$val)
				{
					foreach($val as $k=>$v)
					{
						$array[$k]=$v;
					}
				}
				$array['deal_modified_by'] = $_SESSION['app_admin_user']['user_id'];
				$array['deal_created_by'] = $_SESSION['app_admin_user']['user_id'];
				$array['deal_status'] = 1;
				$data = $GLOBALS["db"]->insert("deal", $array);
				if($data)
				{
					$paramNew = array("mp_details_id"=>$_POST['mp_details_id'], "deal_id"=>$_POST['deal_id']);
					$fetchValueOffers = $GLOBALS["db"]->select("SELECT o.* FROM offers_temp o WHERE o.offer_mp_id =:mp_details_id AND o.offer_deal_id =:deal_id", $paramNew);
					//insert data to offers
					
						$array1 = array();
						foreach($fetchValueOffers as $key1=>$val1)
						{
							foreach($val1 as $k1=>$v1)
							{
								$array1[$k1]=$v1;
							}
							$array1['offer_modified_by'] = $_SESSION['app_admin_user']['user_id'];
							$array1['offer_created_by'] = $_SESSION['app_admin_user']['user_id'];
							$array1['offer_status'] = 1;
							$data = $GLOBALS["db"]->insert("offers", $array1);
						}
					
					//admin notification update
					$adminNot = array("an_status"=>1);
					$condAdmin = array("an_id"=>$_POST["an_id"]);
					$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
					
					//OFFERS TEMP update
					/*
					$condAdmin =  array("mp_id"=>$_POST['mp_details_id']);
					$selectAll = $GLOBALS["db"]->select("SELECT o.offer_id FROM offers_temp o WHERE o.offer_status_time IS NOT NULL AND offer_mp_id =:mp_id", $condAdmin);
					if(count($selectAll)>0)
					{
						for($j=0; $j<count($selectAll); $j++)
						{
							$offerNot = array("offer_status_time"=>NULL);
							$offerAdmin =  array("offer_id"=>$selectAll[$j]['offer_id']);
							$offerUpdate = $GLOBALS["db"]->update("offers_temp", $offerNot, $offerAdmin);
						}
					}*/
											
					//insert into mp notifications table
					$insertMp = array("mpn_user_id"=>$_POST['mp_details_id'], "mpn_process"=>"create_deal", "mpn_response"=>"1");
					$insertData = $GLOBALS["db"]->insert("mp_notification", $insertMp);
					
					
					$param = array("deal_id"=>$_POST['deal_id']);
					$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_gallery_temp d WHERE d.deal_id =:deal_id", $param);
					
					if(count($fetchValueFirst)>0)
						{
							$array = array();
							foreach($fetchValueFirst as $key=>$val)
							{
								foreach($val as $k=>$v)
									{
										$array[$k]=$v;
									}
								$data = $GLOBALS["db"]->insert("deal_gallery", $array);
							}
						}
						return $data;
				}
			}
		}
		//if not verify
		else
		{
			//admin notification update
			//insert into mp notifications table
			$insertMp = array("mpn_user_id"=>$_POST['mp_details_id'], "mpn_process"=>"create_deal", "mpn_response"=>"0");
			$insertData = $GLOBALS["db"]->insert("mp_notification", $insertMp);
			return $insertData;
		}
	}
	function dealStatusUpdate()
	{	
		if($_POST["btn_value"]==="v")
		{
			$param = array("deal_status"=>$_POST['deal_status']);
			$condition = array("deal_id"=>$_POST['deal_id']);
			$update = $GLOBALS["db"]->update("deal", $param, $condition);
			
			$param = array("deal_status_time"=>NULL);
			$condition = array("deal_id"=>$_POST['deal_id']);
			$update1 = $GLOBALS["db"]->update("deal_temp", $param, $condition);
			
			if($update1=="true")
			{
				//admin notification update
				$adminNot = array("an_status"=>1);
				$condAdmin = array("an_id"=>$_POST["an_id"]);
				$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
					
				//insert into mp notifications table
				$insertMp = array("mpn_user_id"=>$_POST['mp_details_id'], "mpn_process"=>"deal_status", "mpn_response"=>"1");
				$insertData = $GLOBALS["db"]->insert("mp_notification", $insertMp);
			}
			return $update;
		}
	}
	
	function dealUpdateCompleteStatus()
	{		
		// if verify
		if($_POST["btn_value"]==="v")
		{
			if($_POST['offer_id']!="")
			{
				$param = array("mp_details_id"=>$_POST['mp_details_id'], "offer_id"=>$_POST['offer_id']);
				$fetchValueFirst = $GLOBALS["db"]->select("SELECT o.* FROM offers_temp o WHERE o.offer_mp_id =:mp_details_id AND o.offer_id =:offer_id", $param);
				if(count($fetchValueFirst)>0)
				{				
					//insert data to offers
					$array = array();
					foreach($fetchValueFirst as $key=>$val)
					{
						foreach($val as $k=>$v)
						{
							$array[$k]=$v;
						}
					}
					$array['offer_modified_by'] = $_SESSION['app_admin_user']['user_id'];
					$array['offer_created_by'] = $_SESSION['app_admin_user']['user_id'];
					$condition = array("offer_id"=>$_POST['offer_id']);
					$data = $GLOBALS["db"]->update("offers", $array, $condition);
					if($data=="true")
					{
						//admin notification update
						$adminNot = array("an_status"=>1);
						$condAdmin = array("an_id"=>$_POST["an_id"]);
						$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
						
						//insert into mp notifications table
						$insertMp = array("mpn_user_id"=>$_POST['mp_details_id'], "mpn_process"=>"deal_update", "mpn_response"=>"1");
						$insertData = $GLOBALS["db"]->insert("mp_notification", $insertMp);
						
						// to update deal gallery table
						$param = array("deal_id"=>$_POST['deal_id']);
						$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_gallery_temp d WHERE d.deal_id =:deal_id", $param);						
						if(count($fetchValueFirst)>0)
							{
								$array = array();
								$i = 0;
								foreach($fetchValueFirst as $key=>$val)
								{
									foreach($val as $k=>$v)
										{
											$array[$k]=$v;
										}
									if($i==0)
									{	
										$condition1 = array("deal_id"=>$val['deal_id']);
										$delete = $GLOBALS["db"]->deleteBulkQuery("deal_gallery", $condition1);
									}
									$i++;
									$data = $GLOBALS["db"]->insert("deal_gallery", $array);
								}
							}
							// to update deal table
							$param = array("deal_id"=>$_POST['deal_id']);
							$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_temp d WHERE d.deal_id =:deal_id", $param);						
							if(count($fetchValueFirst)>0)
								{
									//insert data to deal
									$array = array();
									foreach($fetchValueFirst as $key=>$val)
									{
										foreach($val as $k=>$v)
										{
											$array[$k]=$v;
										}
									}
									$array['deal_modified_by'] = $_SESSION['app_admin_user']['user_id'];
									$array['deal_created_by'] = $_SESSION['app_admin_user']['user_id'];
									$condition = array("deal_id"=>$_POST['deal_id']);
									$data = $GLOBALS["db"]->update("deal", $array, $condition);
								}
							return $data;
					}
				}
			}
			else
			{
				$param = array("deal_id"=>$_POST['deal_id']);
				$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_temp d WHERE d.deal_id =:deal_id", $param);		
				if(count($fetchValueFirst)>0)
				{				
					//insert data to deal
					$array = array();
					foreach($fetchValueFirst as $key=>$val)
					{
						foreach($val as $k=>$v)
						{
							$array[$k]=$v;
						}
					}
					$array['deal_modified_by'] = $_SESSION['app_admin_user']['user_id'];
					$array['deal_created_by'] = $_SESSION['app_admin_user']['user_id'];
					$condition = array("deal_id"=>$_POST['deal_id']);
					$data = $GLOBALS["db"]->update("deal", $array, $condition);
					if($data=="true")
					{
						//admin notification update
						$adminNot = array("an_status"=>1);
						$condAdmin = array("an_id"=>$_POST["an_id"]);
						$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
						
						//insert into mp notifications table
						$insertMp = array("mpn_user_id"=>$_POST['mp_details_id'], "mpn_process"=>"deal_update", "mpn_response"=>"1");
						$insertData = $GLOBALS["db"]->insert("mp_notification", $insertMp);
						
						// to update deal gallery table
						$param = array("deal_id"=>$_POST['deal_id']);
						$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_gallery_temp d WHERE d.deal_id =:deal_id", $param);						
						if(count($fetchValueFirst)>0)
							{
								$array = array();
								$i = 0;
								foreach($fetchValueFirst as $key=>$val)
								{
									foreach($val as $k=>$v)
										{
											$array[$k]=$v;
										}
									if($i==0)
									{	
										$condition1 = array("deal_id"=>$val['deal_id']);
										$delete = $GLOBALS["db"]->deleteBulkQuery("deal_gallery", $condition1);
									}
									$i++;
									$data = $GLOBALS["db"]->insert("deal_gallery", $array);
								}
							}
					}
					return $data;
				}
			}
		}
		//if not verify
		else
		{
				if($_POST['offer_id']!="")
				{
					//admin notification update
					//insert into mp notifications table
					$param = array("mp_details_id"=>$_POST['mp_details_id'], "offer_id"=>$_POST['offer_id']);
					$fetchValueFirst = $GLOBALS["db"]->select("SELECT o.* FROM offers o WHERE o.offer_mp_id =:mp_details_id AND o.offer_id =:offer_id", $param);
					if(count($fetchValueFirst)>0)
					{
						//insert data to offers
							
						$array = array();
						foreach($fetchValueFirst as $key=>$val)
						{
							foreach($val as $k=>$v)
							{
								$array[$k]=$v;
							}
						}
						$array['offer_modified_by'] = $_SESSION['app_admin_user']['user_id'];
						$array['offer_created_by'] = $_SESSION['app_admin_user']['user_id'];
						unset($array['id']);
						$condition = array("offer_id"=>$_POST['offer_id']);
						$data = $GLOBALS["db"]->update("offers_temp", $array, $condition);
						
						if($data=="true")
						{
							$param = array("deal_id"=>$_POST['deal_id']);
							$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal d WHERE d.deal_id =:deal_id", $param);		
							if(count($fetchValueFirst)>0)
							{				
								//insert data to deal
								$array = array();
								foreach($fetchValueFirst as $key=>$val)
								{
									foreach($val as $k=>$v)
									{
										$array[$k]=$v;
									}
								}
								$array['deal_modified_by'] = $_SESSION['app_admin_user']['user_id'];
								$array['deal_created_by'] = $_SESSION['app_admin_user']['user_id'];
								$condition = array("deal_id"=>$_POST['deal_id']);
								$data = $GLOBALS["db"]->update("deal_temp", $array, $condition);
							}
							
							//admin notification update
							$adminNot = array("an_status"=>1);
							$condAdmin = array("an_id"=>$_POST["an_id"]);
							$adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
							//insert into mp notifications table
							$insertMp = array("mpn_user_id"=>$_POST['mp_details_id'], "mpn_process"=>"deal_update", "mpn_response"=>"0");
							
							
							$insertData = $GLOBALS["db"]->insert("mp_notification", $insertMp);
							
							
							$param = array("deal_id"=>$_POST['deal_id']);
							$fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_gallery d WHERE d.deal_id =:deal_id", $param);
							
							if(count($fetchValueFirst)>0)
								{
									$array = array();
									$i = 0;
									foreach($fetchValueFirst as $key=>$val)
									{
										
										unset($val['id']);
										foreach($val as $k=>$v)
											{
												$array[$k]=$v;
											}
										if($i==0)
										{	
											$condition1 = array("deal_id"=>$val['deal_id']);
											$delete = $GLOBALS["db"]->deleteBulkQuery("deal_gallery_temp", $condition1);
										}
										$i++;
										$data = $GLOBALS["db"]->insert("deal_gallery_temp", $array);
									}
								}
								return $data;
						}
					}
				}
				else
				{
				  $param = array("deal_id"=>$_POST['deal_id']);
				  $fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal d WHERE d.deal_id =:deal_id", $param);		
				  if(count($fetchValueFirst)>0)
				  {				
					  //insert data to deal
					  $array = array();
					  foreach($fetchValueFirst as $key=>$val)
					  {
						  foreach($val as $k=>$v)
						  {
							  $array[$k]=$v;
						  }
					  }
					  $array['deal_modified_by'] = $_SESSION['app_admin_user']['user_id'];
					  $array['deal_created_by'] = $_SESSION['app_admin_user']['user_id'];
					  $condition = array("deal_id"=>$_POST['deal_id']);
					  $data = $GLOBALS["db"]->update("deal_temp", $array, $condition);
					  if($data=="true")
					  {
						  //admin notification update
						  $adminNot = array("an_status"=>1);
						  $condAdmin = array("an_id"=>$_POST["an_id"]);
						  $adminUpdate = $GLOBALS["db"]->update("admin_notification", $adminNot, $condAdmin);
						  
						  //insert into mp notifications table
						  $insertMp = array("mpn_user_id"=>$_POST['mp_details_id'], "mpn_process"=>"deal_update", "mpn_response"=>"0");
						  $insertData = $GLOBALS["db"]->insert("mp_notification", $insertMp);
						  
						  // to update deal gallery table
						  $param = array("deal_id"=>$_POST['deal_id']);
						  $fetchValueFirst = $GLOBALS["db"]->select("SELECT d.* FROM deal_gallery d WHERE d.deal_id =:deal_id", $param);						
						  if(count($fetchValueFirst)>0)
							  {
								  $array = array();
								  $i = 0;
								  foreach($fetchValueFirst as $key=>$val)
								  {
									  foreach($val as $k=>$v)
										  {
											  $array[$k]=$v;
										  }
									  if($i==0)
									  {	
										  $condition1 = array("deal_id"=>$val['deal_id']);
										  $delete = $GLOBALS["db"]->deleteBulkQuery("deal_gallery", $condition1);
									  }
									  $i++;
									  $data = $GLOBALS["db"]->insert("deal_gallery_temp", $array);
								  }
							  }
					  }
					  return $data;
				  }
				}
			//ende else
		}
	}
}
?>