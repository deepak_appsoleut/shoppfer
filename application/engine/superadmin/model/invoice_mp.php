<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class invoice_mp_model {

	function invoice_mp()
	{
		$param = array("status"=>1);
		$data = $GLOBALS["db"]->select("SELECT category_id, category_name FROM category WHERE category_status =:status", $param);
		return $data;
	}
	function getMP()
	{
		$param = array("category_id"=>$_POST['category']);
		$data = $GLOBALS["db"]->select("SELECT mp_details_id, mp_details_name FROM mp_details WHERE mp_details_category =:category_id", $param);
		return $data;
	}
	function invoice_preview()
	{
		$param = array("mp_id"=>$_POST['mp_id']);
		$data = $GLOBALS["db"]->select("SELECT m.mp_details_name, m.mp_details_address, m.mp_details_address1, m.mp_details_city, s.state_name FROM mp_details m, state s WHERE m.mp_details_id =:mp_id AND m.mp_details_state = s.state_id", $param);
		
		$implode = implode('-', $_POST['daterange']);
		$startDate = date('Y-m-d', strtotime($implode[0]));
		$endDate = date('Y-m-d', strtotime($implode[1]));	
		
		$amount = $GLOBALS["db"]->select("SELECT SUM(b.booking_amount) as amount FROM booking b WHERE b.booking_mp_id =:mp_id AND DATE_FORMAT(b.booking_created_on, '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."' GROUP BY b.booking_mp_id", $param);
		
		$body = '<body style="width:700px;">
<h2 style="color:#036; font-family:Arial, Helvetica, sans-serif; font-size:30px">Invoice</h2>
<table width="600" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
  <tr>
    <td>Invoice Date : Feb 01, 2016</td>
  </tr>
  <tr>
    <td>Period : Jan 26, 2016 - Feb 01, 2016</td>
  </tr>
  <tr>
    <td>Invoice Ref : 123654789</td>
  </tr>
</table>

<table width="600" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-top:20px">
  <tr>
    <td><strong>FROM:</strong></td>
  </tr>
  <tr>
    <td>HotelOne</td>
  </tr>
  <tr>
    <td>B-4, 523, Spaze i-Tech Park,</td>
  </tr>
  <tr>
    <td>Sec - 49, Sohna Road</td>
  </tr>
  <tr>
    <td>Haryana - Gurgaon, 122001</td>
  </tr>
  <tr>
    <td>India</td>
  </tr>
</table>

<table width="600" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-top:20px">
  <tr>
    <td><strong>To:</strong></td>
  </tr>
  <tr>
    <td>'.$data[0]['mp_details_name'].'</td>
  </tr>
  <tr>
    <td>'.$data[0]['mp_details_address'].','.$data[0]['mp_details_address1'].'</td>
  </tr>
  <tr>
    <td>'.$data[0]['mp_details_city'].'</td>
  </tr>
  <tr>
    <td>'.$data[0]['state_name'].'</td>
  </tr>
</table>
<table width="600" border="1" style="margin-top:20px; border:1px solid #ccc;font-family:Arial, Helvetica, sans-serif; font-size:14px; border-collapse: collapse;" >
  <tr>
    <th scope="col" style="padding:10px;">Description</th>
    <th scope="col" style="padding:10px;">Amount</th>
  </tr>
  <tr>
    <td style="padding:10px;">Food Services</td>
    <td style="padding:10px;">USD '.$amount[0]['amount'].'</td>
  </tr>
  <tr>
    <td style="padding:10px;">Service Tax(14%)</td>
    <td style="padding:10px;">USD 667.90</td>
  </tr>
  <tr>
    <td style="padding:10px;">Total</td>
    <td style="padding:10px;">USD 6,200</td>
  </tr>
</table>
</body>';
return $body;
	}
	
	function invoice_generate()
	{
		
	}
}
?>