<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class trending_model {

	function trending()
	{
		$param = array("status"=>1);
		$data = $GLOBALS["db"]->select("SELECT m.mp_details_name, m.mp_details_id, d.deal_name, d.deal_created_on, d.deal_id, d.is_trending, d.deal_trending_image FROM deal d, mp_details_temp m WHERE d.deal_mp_id = m.mp_details_id AND d.deal_status =:status AND m.mp_status =:status", $param);
		return $data;
	}
	function showTrend($id)
	{
		return $id;
	}
	function removeTrending()
	{
		$updateParams = array('is_trending'=>0, "deal_trending_image"=>NULL); 
		$condition = array('deal_id'=>$_POST['id']);
		$update = $GLOBALS["db"]->update('deal_temp', $updateParams, $condition);		
		$update1 = $GLOBALS["db"]->update('deal', $updateParams, $condition);	
		if($update=="true")
		{
			$delete = unlink(APP_CRM_UPLOADS.$_POST['image']);
		}
		return $update;
	}
	function addTrending()
	{
		$param = array('trending'=>1);
		$data = $GLOBALS["db"]->select("SELECT d.deal_id FROM deal d WHERE d.is_trending =:trending AND d.deal_status=:trending", $param);
		if(count($data)>3)
		{
			return 0;
		}
		else
		{
			//save into category
			$newNamePrefix = time() . '_';
			$manipulator = new imageManipulator($_FILES['trendImage']['tmp_name']);
			$imageName =  'trending/'.$newNamePrefix . $_FILES['trendImage']['name'];
			$save = $manipulator->save(APP_CRM_UPLOADS.$imageName);
			
			// update data
			$condition = array('deal_id'=>$_POST['id']);
			$updateParams = array('deal_trending_image'=>$imageName, 'is_trending'=>1); 
			$update = $GLOBALS["db"]->update('deal_temp', $updateParams, $condition);		
			$update1 = $GLOBALS["db"]->update('deal', $updateParams, $condition);	
		}
		return $update;
	}
}
?>