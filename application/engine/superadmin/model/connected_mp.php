<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class connected_mp_model {
	function connected_mp()
	{
		$param = array("status"=>1);
		$paramSelect = array();
		$data[0] = $GLOBALS["db"]->select("SELECT category_id, category_name FROM category WHERE category_status =:status", $param);
		$data[1] =  $GLOBALS["db"]->select("SELECT mp_details_name FROM mp_details_temp", $paramSelect);
		return $data;
	}
	function getLocationDataHotels()
	{
		if(isset($_POST['page_number']))
		{
			preg_match_all('!\d+!', $_POST['page_number'], $page_number);
			//$limit = $_POST['limit'];
			$limit = $page_number[0][0];
		}
		else
		{
			$limit = 1;
		}
		$ulimit = $limit * 10;
		$llimit = $ulimit -  10;
		
		if($_POST['mp']!="")
		{
			$_POST['mp'] = addslashes($_POST['mp']);
			$andQuery = " AND m.mp_details_name = '".$_POST['mp']."'";
		}
		else if(($_POST['category']=="")&&($_POST['status']=="")&&($_POST['mp']==""))
		{
			$andQuery = "";
		}
		else if(($_POST['category']=="")&&($_POST['status']!="")&&($_POST['mp']==""))
		{
			$andQuery = " AND m.mp_status = '".$_POST['status']."'";
		}
		else if(($_POST['category']!="")&&($_POST['status']!="")&&($_POST['mp']==""))
		{
			$_POST['category'] = implode(',', $_POST['category']);
			$andQuery = " AND m.mp_details_category IN (".$_POST['category'].") AND m.mp_status = '".$_POST['status']."'";
		}
		else if(($_POST['category']!="")&&($_POST['status']=="")&&($_POST['mp']==""))
		{
			$_POST['category'] = implode(',', $_POST['category']);
			$andQuery = " AND m.mp_details_category IN (".$_POST['category'].")";
		}
			/*	if(isset($_POST['category']) && $_POST['category']!="")
				{
					$_POST['category'] = implode(',', $_POST['category']);
					$andQuery .= " AND m.mp_details_category IN (".$_POST['category'].")";
				}
				else if(isset($_POST['mp']) && $_POST['mp']!="")
				{
					$andQuery .= " AND m.mp_details_name = '".$_POST['mp']."'";
				}
				else if(isset($_POST['status']))
				{
					if($_POST['status']!="")
					{
						$andQuery .= " AND m.mp_status = '".$_POST['status']."'";
					}
				}*/
			$param = array();				
			$data[0] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS m.mp_details_id, m.mp_details_name, m.mp_details_person, m.mp_details_contact, m.mp_details_description, m.mp_details_id,m.mp_details_address, m.mp_details_state, m.mp_details_city, m.mp_details_phone, m.mp_details_designation, m.mp_details_latitude, m.mp_details_longitude, m.mp_status, g.mp_gallery_image as image, c.category_name, d.deal_status, d.deal_id, d.deal_name, AVG(r.mp_review_score) as score FROM category c, mp_details_temp m LEFT JOIN deal_temp d ON m.mp_details_id = d.deal_mp_id AND d.deal_status =1 LEFT JOIN mp_reviews r ON r.mp_review_mp_details_id = m.mp_details_id LEFT JOIN mp_gallery_temp g ON g.mp_gallery_mp_id = m.mp_details_id WHERE c.category_id = m.mp_details_category".$andQuery." GROUP BY m.mp_details_id LIMIT ".$llimit.", 10", $param);
			$data[1] = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count;");
			$paramEmpty = array();
			return $data;
	}	
	function getSearchData()
	{
		$param =array();
		$data = $GLOBALS["db"]->select("SELECT mp_details_name FROM mp_details_temp", $param);
		return $data;
	}
	function merchant_view($id)
	{
		$param = array("mp_id"=>$id);
		$paramSelect = array();
		$data[0] = $GLOBALS["db"]->select("SELECT m.* FROM mp_details m WHERE m.mp_details_id =:mp_id", $param);
		$data[1] = $GLOBALS["db"]->select("SELECT category_id,category_name FROM category WHERE category_status =1", $param);
		$data[3] = $GLOBALS["db"]->select("SELECT * FROM mp_gallery WHERE mp_gallery_mp_id  =:mp_id AND mp_gallery_status =1", $param);
		return $data;
	}
	function deal_image_show_profile_update()
	{
		
		
		$image 			= $_POST['image'];
		$newNamePrefix 	= time() . '_';
		$manipulator 	= new imageManipulator($_FILES[$image]['tmp_name']);
		$imageName 		= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix .  str_replace(' ', '_', $_FILES[$image]['name']));
		$tm 			= $manipulator->resample(190, 140, true);
		$media 			= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$thumb 			= 'thumb/'.$newNamePrefix . str_replace(' ', '_', $_FILES[$image]['name']);
		$response['imageName'] = $media;
		$response['id'] = 1;
		
		
		
		if($_POST['mp_gallery_id_'.$image]	==	'')
		{
			
			$param = array('mp_gallery_mp_id'=>$_POST['mp_id'], 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_admin_user']['user_id'], 'mp_gallery_modified_by'=>$_SESSION['app_admin_user']['user_id']);
			
			$insert_temp 	= $GLOBALS["db"]->lastInsertNow('mp_gallery_temp', $param, 'mp_gallery_created_on');
				
			$param1 = array('mp_gallery_mp_id'=>$_POST['mp_id'], 'mp_gallery_id'=>$insert_temp, 'mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_created_by'=>$_SESSION['app_admin_user']['user_id'], 'mp_gallery_modified_by'=>$_SESSION['app_admin_user']['user_id']);
				
			$insert		=	$GLOBALS["db"]->lastInsertNow('mp_gallery', $param1, 'mp_gallery_created_on');
			$response['gallery_id']	= $insert_temp;
			
		}
		else
		{
			$param = array('mp_gallery_name'=>NULL, 'mp_gallery_image'=>$imageName, 'mp_gallery_thumb'=>$thumb, 'mp_gallery_modified_by'=>$_SESSION['app_admin_user']['user_id'],'mp_gallery_status'=>1);
			$condition = array("mp_gallery_id"=>$_POST['mp_gallery_id_'.$image]);
			$update = $GLOBALS["db"]->update('mp_gallery_temp', $param, $condition);
			$update = $GLOBALS["db"]->update('mp_gallery', $param, $condition);
		}
		return json_encode($response);
	}
	function deletePic()
	{
		$mp_gallery_id_image	=	$_POST['mp_gallery_id_image'];
		$condition = array("mp_gallery_id"=>$mp_gallery_id_image);
		$delete_temp 	= $GLOBALS["db"]->deleteQuery('mp_gallery_temp', $condition);
	}
	function profileEdit()
	{
		if(!empty($_POST['recommend']))
		{
			$_POST['recommend']= implode(',', $_POST['recommend']);
		}
		else
		{
			$_POST['recommend'] = NULL;
		}
		
		
		$latitude 	= $_POST["lat"];
		$longitude 	= $_POST["lng"];
		
		
		if(!isset($_POST['veg_nonveg']))
		{
			$_POST['veg_nonveg'] = NULL;
		}
		if(!isset($_POST['bar_available']))
		{
			$_POST['bar_available'] = NULL;
		}
		
		
		$param = array(	"mp_details_name"=>$_POST['business'], 
						"mp_details_city"=>$_POST['city'], 
						"mp_details_address"=>$_POST['address'], 
						"mp_details_person"=>$_POST['person'], 
						"mp_details_phone"=>$_POST['contact'],
						"mp_details_contact"=>$_POST['phone'],
						"mp_details_category"=>$_POST['category'], 
						"mp_details_designation"=>$_POST['designation'], 
						"mp_details_website"=>$_POST['web_url'], 
						"mp_details_description"=>$_POST['description'], 
						"mp_details_recommend"=>$_POST['recommend'], 
						"mp_opentime"=>$_POST['opentime'], 
						"mp_closetime"=>$_POST['closetime'], 
						"mp_accept_card"=>$_POST['accept_card'], 
						"mp_nonveg"=>$_POST['veg_nonveg'], 
						"mp_bar_available"=>$_POST['bar_available'],
						"mp_details_latitude"=>$latitude, 
						"mp_details_longitude"=>$longitude, 
						"mp_details_modified_by"=>$_SESSION['app_admin_user']['user_id']);
			
			
			$condition = array("mp_details_id"=>$_POST['mp_id']);
			$resultUpdate = $GLOBALS["db"]->update('mp_details_temp', $param, $condition);
			$resultUpdate1 = $GLOBALS["db"]->update('mp_details', $param, $condition);
			return $resultUpdate;
	}
	function deal_view($id)
	{
		$param = array("id"=>$id);
		$data[0] = $GLOBALS["db"]->select("SELECT dt.* FROM deal_temp dt LEFT JOIN deal d ON dt.deal_id = d.deal_id where dt.deal_mp_id =:id and (dt.deal_status =1 OR dt.deal_status =0) ORDER BY dt.deal_status DESC", $param);
		$data[1] = $id;
		return $data;	
	}
	
	function delete_offer()
	{
		$param = array("offer_id"=>$_POST['offer_id'],"offer_mp_id"=>$_POST['mp_id']);
		$data = $GLOBALS["db"]->deleteQuery("offers_temp", $param, $limit = 1);
		return $data;
	}
	function active_inactive()
	{
		$status			=	'active';
		
		$param 			= 	array("id"=>$_POST['mp_id']);
		
		$data 			= 	$GLOBALS["db"]->select("SELECT * FROM deal_temp WHERE deal_mp_id =:id AND deal_status = 1", $param);
		
		
		if(count($data) == 0)
		{
			$deal_id	=	$_POST['deal_id'];
			$condition	=	array("deal_id"=>$deal_id);
			$param		=	array("deal_status"=>1);
			$update = $GLOBALS["db"]->updateNow('deal_temp', $param, $condition, "deal_status_time");
			if($update)
			{
				echo "1";
				//-----START#Admin-Notification Table instertion-----//
				$notifiy_array	= array("an_mp_user_id"=>$deal_id,"an_process"=>"deal_status","an_text"=>'deal_status',"an_notify_from"=>"MP");
				$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
				//-----END#Admin-Notification Table instertion-----//
			}
			else
			{
				echo "0";
			}
		}
		else
		{
			$offer_name			=	$data[0]['deal_name'];
			if($data[0]['deal_type']	==	1)
			{
				//$desc	=	"Normal deal";
			}else{
				//$desc	=	"Shoutout deal";
			}
			//echo '<style>.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {}</style><div><h2 style="text-align: center;">You have already running Offer</h2><p style="    text-align: center;">If you want to Active this Offer, first disable old Offer</p><div class="table-responsive" style="margin-top:20px;"> <table class="table" style="margin-bottom: 0px;"> <thead> <tr> <th>#</th> <th>Deal Type</th> <th>Deal Descripition</th><th>Status</th></tr></thead> <tbody> <tr> <td>1</td><td>'.$offer_type_name.'</td><td>'.$desc.'</td><td><button id="active_inavtive_deal" type="button" class="btn btn-warning" value="'.$data[0]['offer_id'].'-'.$status.'">Inactive</button></td></tr></tbody> </table> </div></div>';
			echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' deal is currently running!</h2><p style="text-align: center;"></p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new deal,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running deal!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs popupcancel" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="active_inavtive_deal" value="'.$data[0]['deal_id'].'-'.$status.'">DISABLE</button></div></div>';
			
		}
	}
	function disable_deal()
	{
		$deal_id	=	$_POST['deal_id'];
		$condition	=	array("deal_id"=>$deal_id);
		$param		=	array("deal_status"=>0, "deal_status_time"=>NULL);
		$update 	= 	$GLOBALS["db"]->update('deal_temp', $param, $condition);
		$update1 	= 	$GLOBALS["db"]->update('deal', $param, $condition);
		if($update)
		{
			//-----START#Admin-Notification Table instertion-----//
			//$notifiy_array	= array("an_mp_user_id"=>$deal_id,"an_process"=>"deal_status","an_text"=>'deal_status',"an_notify_from"=>"MP");
			//$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
			//-----END#Admin-Notification Table instertion-----//
			return "1";
		}
		else
		{
			return "0";
		}
	}
	function enable_deal()
	{
		
		$offer_id	=	$_POST['offer_id'];
		$condition	=	array("offer_id"=>$offer_id);
		$param		=	array("offer_status"=>1, "offer_status_time"=>NULL);
		$update 	= 	$GLOBALS["db"]->update('offers_temp', $param, $condition);
		$update1 	= 	$GLOBALS["db"]->update('offers', $param, $condition);
		if($update)
		{
			echo "1";
			//-----START#Admin-Notification Table instertion-----//
			$notifiy_array	= array("an_mp_user_id"=>$offer_id,"an_process"=>"deal_status","an_text"=>'offer_status',"an_notify_from"=>"MP");
			$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
			//-----END#Admin-Notification Table instertion-----//
		}
		else
		{
			echo "0";
		}
	}
	function edit_deal($id)
	{
		//$param = array();
		//$paramAll = array("id"=>$id);
		//$data[1]= $GLOBALS["db"]->select("SELECT o.*, m.mp_details_name, m.mp_closetime, m.mp_opentime, m.mp_details_id, GROUP_CONCAT(ot.offer_image_details) as offer_image_details, GROUP_CONCAT(ot.offer_gallery_id) as offer_gallery_id FROM offers_temp o, mp_details m, offers_gallery_temp ot WHERE o.offer_id =:id AND ot.offer_id = o.offer_id AND o.offer_mp_id = m.mp_details_id", $paramAll);
		$param = array();
		$paramAll 	= array("id"=>$id);
		$data[1]	= $GLOBALS["db"]->select("SELECT d.*, m.mp_details_name, m.mp_opentime, mp_closetime FROM deal d, mp_details m WHERE deal_id =:id AND m.mp_details_id = d.deal_mp_id", $paramAll);
		$data[2]	= $GLOBALS["db"]->select("SELECT * FROM offers WHERE offer_deal_id =:id", $paramAll);
		$data[3]	= $GLOBALS["db"]->select("SELECT GROUP_CONCAT(dg.deal_image_details) as deal_image_details, GROUP_CONCAT(dg.deal_gallery_id) as deal_gallery_id, GROUP_CONCAT(dg.deal_image_media) as deal_image_media FROM deal_gallery dg WHERE deal_id =:id", $paramAll);
		return $data;
	}
	function Images_add_from_gallery()
	{
		$param = array();
		$data[1]= $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_status = 1 AND mp_gallery_mp_id = ".$_POST['mp_id']."", $param);
		
		$data[2]= $GLOBALS["db"]->select("SELECT d.deal_image_media FROM deal_gallery_temp d, mp_details_temp m, deal_temp dt WHERE d.deal_id = dt.deal_id AND m.mp_details_id = dt.deal_mp_id AND m.mp_details_id = ".$_POST['mp_id']."", $param);
		
		$div_open	=	'<div id="dragable" style="overflow: scroll;padding: 10px;width: 100%;float: left;overflow-x: hidden;height: 250px;"><ul id="images" class="ui-sortable ui-draggable" style="margin-left: 16px;position: relative;min-height: 200px;    ">';
		
		$div_mid	=	'';
		
		$image_array	=	array();
		
		for($i=0;$i<count($data[1]);$i++)
		{
			array_push($image_array,$data[1][$i]['mp_gallery_image']);
		}
		for($i=0;$i<count($data[2]);$i++)
		{
			array_push($image_array,$data[2][$i]['deal_image_media']);
		}
		
		for($i=0;$i<count(array_unique($image_array));$i++)
		{
			$j	=	$i + 1;
			$div_mid	=	$div_mid. '<li style="width:auto;padding-right: 5px;padding-left: 5px; width:125px; float:left;margin-bottom: 3px;" class="ui-sortable-handle"><img alt="'.$j.'" class="gallery_img" id="'.$image_array[$i].'" style="cursor:pointer;" height="90px" width="120px" src="'. APP_CRM_UPLOADS_PATH.$image_array[$i].'"><div style="position: absolute;top: 70px;right: 6px; display:none;" id="gallery_img_'.$j.'" class="gmi"><img src="'.APP_IMAGES.'right.png">
</div></li>';
		}
		
		$div_close		=	'</ul></div>';
		//$upload_button	=	'<div style="margin-top: 90px;margin-left: 30px;"><form id="gallery_image_form" name="gallery_image_form"><div><img src="http://localhost/st/public/assets/web/images/right.png"><input type="file" id="image1" name="imageMy"></div></form></div>';
		
		$upload_button	=	'<div style="margin-top: 90px;margin-left: 30px;"><form id="gallery_image_form" name="gallery_image_form"><div class="image-upload" style="bottom: -10px;position: absolute;"><div id="upload-file-container"><input type="file" id="image1" name="imageMy"></div><div style="font-weight: 500;">you may upload multiple files at once. Use JPG,GIF or PNG files.</div></div></form></div>';
		
		
		
		//echo $div_open . $div_mid . $div_close . $upload_button;
		
		
		echo '<div style="width: 100%;height: 280px;"><div id="uploading" style="text-align: center;float: left;width: 50%;height: 30px;background: #F1F1F1;"><span style="top: 12px;position: absolute;cursor: pointer">UPLOAD</span><div style="border-bottom: 2px solid #FF2F3C;width: 80px;height: 30px;margin-left: 100px;top: 20px;" id="up_line"></div></div><div id="existing" style="cursor: pointer;float: left;background: #F1F1F1;text-align: center;height: 250px;height: 30px;width: 50%;"><span style="top: 12px;position: absolute;margin-left: -25px;">EXISTING</span><div style="border-bottom: 2px solid #FF2F3C;width: 80px;height: 30px;margin-left: 80px;top: 20px;display:none;" id="ex_line"></div></div><div id="up" style="width: 100%;float: left;height: 250px;padding: 10px;"><div id="img_show" style="height: 185px;width: 250px;margin: 0 auto;"><img src="" id="uploading_image" style="    position: absolute;"><div id="img_per" style="text-align: center;margin-top: 80px;font-weight: bold;color: red;display:none;position: fixed;margin-left: 100px;">100%</div></div>'.$upload_button.'</div><div id="ex" style="padding: 10px;width: 100%;float: left;height: 250px;display:none;">'.$div_open . $div_mid . $div_close.'</div></div>';
		
	}
	function deal_image_show()
	{
		$image 			= $_POST['image'];
		
		$newNamePrefix 	= time() . '_';
		$manipulator 	= new imageManipulator($_FILES['imageMy']['tmp_name']);
		$imageName 		= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES['imageMy']['name']);
		$save 			= $manipulator->save(APP_CRM_UPLOADS.'media/' . $newNamePrefix . str_replace(' ', '_', $_FILES['imageMy']['name']));
		$tm 			= $manipulator->resample(190, 140, true);
		$media 			= 'media/'.$newNamePrefix . str_replace(' ', '_', $_FILES['imageMy']['name']);
		$response['imageName'] = $media;
		$response['id'] = 1;
		return json_encode($response);	
	}
	function editDeal()
	{
		
		//--------------------------------------------------Normal deal------------------------------------------//
		$form_value_array		=	explode("#",$_POST["id"]);
		$status					=	$form_value_array[0];
		$offer_type				=	$form_value_array[1];
		$offer_id				=	$form_value_array[2];
		if($offer_type	==	'name')
		{
			$condition 			=	array('deal_id'=>$_POST["deal_id"]);
			$deal_name_update 	=	array("deal_name"=>$_POST["deal_name"]);
			$insert_deal_name 	=	$GLOBALS["db"]->update('deal_temp', $deal_name_update, $condition);
			$insert_deal_name1 	=	$GLOBALS["db"]->update('deal', $deal_name_update, $condition);
			if($insert_deal_name	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		if($offer_type	==	'image')
		{
			$image_priority_url		=	$_POST["image_priority_url"];
			$deal_id				=	$_POST["deal_id"];
			$image_url				=	explode("#",$image_priority_url);
			$param_delete			=	array("deal_id"=>$deal_id);
			$delete 				= 	$GLOBALS["db"]->deleteBulkQuery('deal_gallery_temp', $param_delete);
			$delete 				= 	$GLOBALS["db"]->deleteBulkQuery('deal_gallery', $param_delete);
			$image_count			=	count($image_url);
			if($delete	==	"true")
			{	
				for($i=0;$i<$image_count;$i++)
				{
					$param_gallery_update 	=	array("deal_id"=>$deal_id,"deal_image_media"=>$image_url[$i]);
					$insert_up				= 	$GLOBALS["db"]->lastInsertNow('deal_gallery_temp', $param_gallery_update, 'deal_gallery_created_on');
					$param_gallery_update1 	=	array("deal_id"=>$deal_id,"deal_image_media"=>$image_url[$i], "deal_gallery_id"=>$insert_up);
					$insert_up				= 	$GLOBALS["db"]->insertNow('deal_gallery', $param_gallery_update1, 'deal_gallery_created_on');
					
				}
			
			}
			if($delete	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		if($offer_type	==	'flat')
		{
			$discount		=	$form_value_array[3];
			if($form_value_array[4] == 'NULL')
		   {
			$minbilling  = NULL;
		   }
		   else
		   {
			$minbilling  = $form_value_array[4];
		   }
			$offer_day_type	=	$form_value_array[5];
			$offer_day_value=	$form_value_array[6];
			$offer_timeings	=	$form_value_array[7];
			$coupon_status	=	$form_value_array[8];
			$max_capping	=	$form_value_array[9];
			
			$condition 		=	array('offer_id'=>$offer_id);
			$param 			=	array(	"offer_discount"=>$discount,
										"min_billing"=>$minbilling,
										"offers_day_type"=>$offer_day_type,
										"offers_day_value"=>$offer_day_value,
										"offers_timeings"=>$offer_timeings,
										"offer_coupon_status"=>$coupon_status,
										"max_capping"=>$max_capping);
										
			$data 		=	$GLOBALS["db"]->update('offers_temp', $param, $condition);
			$data1 		=	$GLOBALS["db"]->update('offers', $param, $condition);
			if($data	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		if($offer_type	==	'combo')
		{
			$offer_desc		=	$form_value_array[3];
			if($form_value_array[4] == 'NULL')
		   {
			$include_price = NULL;
		   }
		   else
		   {
			$include_price = $form_value_array[4];
		   }
		   if($form_value_array[5] == 'NULL')
		   {
			$minbilling  = NULL;
		   }
		   else
		   {
			$minbilling  = $form_value_array[5];
		   }
			$offer_day_type	=	$form_value_array[6];
			$offer_day_value=	$form_value_array[7];
			$offer_timeings	=	$form_value_array[8];
			$coupon_status	=	$form_value_array[9];
			$max_capping	=	$form_value_array[10];
			
			$condition 		=	array('offer_id'=>$offer_id);
			$param 			=	array(	"offer_description"=>$offer_desc,
										"offer_price"=>$include_price,
										"min_billing"=>$minbilling,
										"offers_day_type"=>$offer_day_type,
										"offers_day_value"=>$offer_day_value,
										"offers_timeings"=>$offer_timeings,
										"offer_coupon_status"=>$coupon_status,
										"max_capping"=>$max_capping);
										
			$data 		=	$GLOBALS["db"]->update('offers_temp', $param, $condition);
			$data1 		=	$GLOBALS["db"]->update('offers', $param, $condition);
			if($data	==	"true"){
				echo 1;
			}else{
				echo 0;
			}
		}
		//--------------------------------------------------Normal deal------------------------------------------//
		
	}
	function createDeal($id)
	{
		$param = array();
		$data[1]= $GLOBALS["db"]->select("SELECT * FROM mp_gallery_temp WHERE mp_gallery_status = 1 AND mp_gallery_mp_id = ".$id."", $param);
		$data[2]= $GLOBALS["db"]->select("SELECT * FROM mp_details_temp WHERE mp_details_id = ".$id."", $param);
		$data[3] = $id;
		return $data;		
	}
	function Create_deal()
	{
		$param 			= array("id"=>$_POST['myid']);
		$data 			= $GLOBALS["db"]->select("SELECT * FROM deal_temp WHERE deal_mp_id  =:id AND deal_status  = 1", $param);
		
		if($_POST["main_deal_type"]	==	1)
		{
			if(count($data) == 0)
			{
					//------------------Getting Global variable for page--------------------
					$deal_name					=	$_POST['deal_name'];
					$deal_type					=	$_POST["main_deal_type"];
					$img_selected_url			=	$_POST['image_priority_url'];
					$Global_offer_array_hidden	=	$_POST['Global_offer_array_hidden'];
					$mp_id						=	$_POST['myid'];
					//------------------Getting Global variable for page--------------------
					
					//------------------Deal + Deal gallery insert--------------------
					$param_temp				=	array("deal_name"=>$deal_name,"deal_type"=>$deal_type,"deal_mp_id"=>$mp_id);
					$deal_insert_id_temp 	=	$GLOBALS["db"]->lastInsertNow('deal_temp', $param_temp, 'deal_created_on');
					
					$offers_image	=	explode("#",$img_selected_url);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
							$gallery_param_temp = array("deal_image_media"=>$offers_image[$i],"deal_id"=>$deal_insert_id_temp,"deal_gallery_status"=>1);	
							$deal_gallery_id 	= $GLOBALS["db"]->lastInsertNow('deal_gallery_temp', $gallery_param_temp, 'deal_gallery_created_on');
						}
					}
					//------------------Deal + Deal gallery insert--------------------
					
					//-----START#Admin-Notification Table instertion-----//
					$notifiy_array	= array("an_mp_user_id"=>$deal_insert_id_temp,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP","an_deal_type"=>1);
					$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
					//-----END#Admin-Notification Table instertion-----//
					
					//------------------Offer insert--------------------
					if($Global_offer_array_hidden	!=	''){
						
						
						
						$Global_offer_array	=	explode(",",$Global_offer_array_hidden);
						for($i=0;$i<count($Global_offer_array);$i++)
						{
							//Flate offer
							$main_array_value		=	explode("#",$Global_offer_array[$i]);
							
							if($main_array_value[0]	==	'flat')
							{
								if(count($main_array_value)==8) // if max caping have value
								{
									$offer_mp_id		=	$mp_id;
									$offer_deal_id		=	$deal_insert_id_temp;
									$offer_type_id		=	1;
									if($main_array_value[6] == 'NULL')
									 {
									  $min_billing  = NULL;
									 }
									 else
									 {
									  $min_billing  = $main_array_value[6];
									 }
									$max_capping		=	$main_array_value[7];
									$offer_discount		=	$main_array_value[1];
									$offers_day_type	=	$main_array_value[2];
									$offers_day_value	=	$main_array_value[3];
									$offers_timeings	=	$main_array_value[4];
									$offer_coupon_status=	$main_array_value[5];
									$flat_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_discount"=>$offer_discount,"offer_deal_id"=>$offer_deal_id,"offer_type_id"=>$offer_type_id,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offers_day_value,"offers_timeings"=>$offers_timeings,"offer_coupon_status"=>$offer_coupon_status,"offer_status"=>1);
									
								
								}
								else// if max caping have not value
								{
									$offer_mp_id		=	$mp_id;
									$offer_deal_id		=	$deal_insert_id_temp;
									$offer_type_id		=	1;
									if($main_array_value[6] == 'NULL')
									 {
									  $min_billing  = NULL;
									 }
									 else
									 {
									  $min_billing  = $main_array_value[6];
									 }
									$offers_day_type	=	$main_array_value[2];
									$offer_discount		=	$main_array_value[1];
									$offers_day_value	=	$main_array_value[3];
									$offers_timeings	=	$main_array_value[4];
									$offer_coupon_status=	$main_array_value[5];
									$flat_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_discount"=>$offer_discount,"offer_deal_id"=>$offer_deal_id,"offer_type_id"=>$offer_type_id,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offers_day_value,"offers_timeings"=>$offers_timeings,"offer_coupon_status"=>$offer_coupon_status,"offer_status"=>1);
								
								}
								echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $flat_param, 'offer_created_on');
							
							}else{//combo offer
									if($main_array_value[7]	==	1) 
									{
										//deal type include
										$offer_mp_id		=	$mp_id;
										$offer_deal_id		=	$deal_insert_id_temp;
										if($main_array_value[1] == 'NULL')
										 {
										  $offer_price  = NULL;
										 }
										 else
										 {
										  $offer_price  = $main_array_value[1]; 
										 }
										$offer_description	=	$main_array_value[2];
										$offer_type_id		=	2;
										if($main_array_value[5] == 'NULL')
										 {
										  $min_billing  = NULL;
										 }
										 else
										 {
										  $min_billing  = $main_array_value[5];
										 }
										$offers_day_type	=	$main_array_value[6];
										$offers_day_value	=	$main_array_value[3];
										$offers_timeings	=	$main_array_value[4];
										$offer_coupon_status=	$main_array_value[7];
										$max_capping		=	$main_array_value[8];
										$combo_param		=	array("offer_mp_id"=>$offer_mp_id,
																	"offer_deal_id"=>$offer_deal_id,
																	"offer_price"=>$offer_price,
																	"offer_description"=>$offer_description,
																	"offer_type_id"=>$offer_type_id,
																	"max_capping"=>$max_capping,
																	"min_billing"=>$min_billing,
																	"offers_day_type"=>$offers_day_type,
																	"offers_day_value"=>$offers_day_value,
																	"offers_timeings"=>$offers_timeings,
																	"offer_coupon_status"=>$offer_coupon_status,
																	"offer_status"=>1);
									
									}else{
										//deal type not include
										$offer_mp_id		=	$mp_id;
										$offer_deal_id		=	$deal_insert_id_temp;
										if($main_array_value[1] == 'NULL')
										 {
										  $offer_price  = NULL;
										 }
										 else
										 {
										  $offer_price  = $main_array_value[1]; 
										 }
										$offer_description	=	$main_array_value[2];
										$offer_type_id		=	2;
										if($main_array_value[5] == 'NULL')
										 {
										  $min_billing  = NULL;
										 }
										 else
										 {
										  $min_billing  = $main_array_value[5];
										 }
										$offers_day_type	=	$main_array_value[6];
										$offers_day_value	=	$main_array_value[3];
										$offers_timeings	=	$main_array_value[4];
										$offer_coupon_status=	$main_array_value[7];
										$combo_param	=	array(	"offer_mp_id"=>$offer_mp_id,
																	"offer_deal_id"=>$offer_deal_id,
																	"offer_price"=>$offer_price,
																	"offer_description"=>$offer_description,
																	"offer_type_id"=>$offer_type_id,
																	"min_billing"=>$min_billing,
																	"offers_day_type"=>$offers_day_type,
																	"offers_day_value"=>$offers_day_value,
																	"offers_timeings"=>$offers_timeings,
																	"offer_coupon_status"=>$offer_coupon_status,"offer_status"=>1);
									}
								
								echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $combo_param, 'offer_created_on');
							}
						}
					}
					else
					{
						$offer_type		=	$_POST['offer_type'];
						if(isset($_POST['max_capping']))
						{
							$max_capping	=	$_POST['max_capping'];
						}
						else
						{
							$max_capping	=	NULL;
						}
						if(isset($_POST['min_billing']))
						{
							$min_billing	=	$_POST['min_billing'];
						}
						else
						{
							$min_billing	=	NULL;
						}
						$coupon_active_inactive	=	$_POST['coupon_active_inactive'];
						//Flat Offer
						if($offer_type	==	1)
						{
							$flat_discount			=	$_POST['flat_discount'];
							$offers_day_type		=	$_POST['offers_days'];
							$offer_day_hidden_type	=	$_POST['offer_day_hidden_type'];
							$offer_time_hidden_type	=	$_POST['offer_time_hidden_type'];
							//deal_insert_id_temp
							$param = array("offer_mp_id"=>$mp_id,"offer_deal_id"=>$deal_insert_id_temp, "offer_discount"=>$flat_discount, "offer_type_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
							
							$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
							echo $offer_id;
						}
						//Combo Offer
						if($offer_type	==	2)
						{
							$offers_day_type		=	$_POST['offers_days'];
							$offer_day_hidden_type	=	$_POST['offer_day_hidden_type'];
							$offer_time_hidden_type	=	$_POST['offer_time_hidden_type'];
							$offer_description		=	$_POST['short_desc'];
							$offer_price			=	$_POST['inc_price'];
							$param = array("offer_mp_id"=>$mp_id,"offer_deal_id"=>$deal_insert_id_temp,"offer_description"=>$offer_description,"offer_type_id"=>2,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive,"offer_price"=>$offer_price);
							$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
							echo $offer_id;
						}
					}
					//------------------Offer insert--------------------
				
				
					
			}
			else
			{
				$deal_type	=	$data[0]['deal_type'];
				$deal_name	=	$data[0]['deal_name'];
				if($deal_type	==	1)
				{
					$deal_type_name	=	'Normal Deal';
				}else{
					$deal_type_name	=	'Shoutout Deal';
				}
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$deal_name.' Deal is currently running!</h2><p style="text-align: center;"></p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new deal,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running deal!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['deal_id'].'">DISABLE</button></div></div>';
				
			}
		}
		else //Shout Deal code start here ------------------------------------------//
		{
			
			if(count($data) == 0)
			{
				
			//------------------Getting Global variable for page--------------------
			$deal_name						=	$_POST['deal_name'];
			$deal_type						=	$_POST["main_deal_type"];
			$img_selected_url				=	$_POST['image_priority_url'];
			$mp_id							=	$_POST['myid'];
			$deal_shoutout_description		=	$_POST['so_desc'];
			$deal_days						=	$_POST["shoutout_deal_days"];
			$deal_duration					=	$_POST["offers_timeings_so_hidden"];
			$Global_offer_array_hidden_so	=	$_POST["Global_offer_array_hidden_so"];
			$add_more_offer_so_flag			=	$_POST["add_more_offer_so_flag"];
			//------------------Getting Global variable for page--------------------
			
			//------------------Deal + Deal gallery insert--------------------
			$param_temp				=	array("deal_name"=>$deal_name,"deal_shoutout_description"=>$deal_shoutout_description,"deal_shoutout_day"=>$deal_days,"deal_shoutout_time"=>$deal_duration,"deal_type"=>$deal_type,"deal_mp_id"=>$mp_id);
			$deal_insert_id_temp 	=	$GLOBALS["db"]->lastInsertNow('deal_temp', $param_temp, 'deal_created_on');
			
			$offers_image	=	explode("#",$img_selected_url);
			for($i=0;$i<count($offers_image);$i++)
			{
				if($offers_image[$i]!="")
				{
					$gallery_param_temp = array("deal_image_media"=>$offers_image[$i],"deal_id"=>$deal_insert_id_temp,"deal_gallery_status"=>1);	
					$deal_gallery_id 	= $GLOBALS["db"]->lastInsertNow('deal_gallery_temp', $gallery_param_temp, 'deal_gallery_created_on');
				}
			}
			//------------------Deal + Deal gallery insert--------------------
			
			//-----START#Admin-Notification Table instertion-----//
			$notifiy_array	= array("an_mp_user_id"=>$deal_insert_id_temp,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP","an_deal_type"=>2);
			$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
			//-----END#Admin-Notification Table instertion-----//
			
			//------------------Multiple offer insert-------------------------
			if($Global_offer_array_hidden_so	!=	"")
			{
				$Global_offer_array_hidden_so_array	=	explode(",",$Global_offer_array_hidden_so);
				for($i=0;$i<count($Global_offer_array_hidden_so_array);$i++)
				{
					//Flate offer
					$main_array_value		=	explode("#",$Global_offer_array_hidden_so_array[$i]);
					if($main_array_value[0]	==	1)
					{
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	1;
						$flat_discount_so	=	$main_array_value[1];
						$flat_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_deal_id"=>$offer_deal_id,"offer_discount"=>$flat_discount_so,"offer_type_id"=>$offer_type_id,"offer_status"=>1);
						echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $flat_param, 'offer_created_on');
					
					}else{//combo offer
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	2;
						$offer_desc			=	$main_array_value[1];
						$combo_param	=	array("offer_mp_id"=>$offer_mp_id,"offer_deal_id"=>$offer_deal_id,"offer_type_id"=>$offer_type_id,"offer_status"=>1,"offer_description"=>$offer_desc);
						echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $combo_param, 'offer_created_on');
					}
				}
			}
			else
			{
				if($add_more_offer_so_flag	==	1)
				{
					$offer_type_so	=	$_POST["offer_type_so"];
					if($offer_type_so	==	1)//flate offer
					{
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	1;
						$flat_discount_so	=	$_POST["flat_discount_so"];
						$flat_param			=	array("offer_mp_id"=>$offer_mp_id,
											  	"offer_deal_id"=>$offer_deal_id,
											  	"offer_type_id"=>$offer_type_id,
											  	"offer_status"=>1,
											  	"offer_discount"=>$flat_discount_so);
						
					}else{	//combo offer
						$offer_mp_id		=	$mp_id;
						$offer_deal_id		=	$deal_insert_id_temp;
						$offer_type_id		=	2;
						$offer_desc			=	$_POST["so_offer_desc"];
						$flat_param			=	array("offer_mp_id"=>$offer_mp_id,
											  	"offer_deal_id"=>$offer_deal_id,
											  	"offer_type_id"=>$offer_type_id,
											  	"offer_status"=>1,
											  	"offer_description"=>$offer_desc);
												
						
					}
					echo $offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $flat_param, 'offer_created_on');
				}else{ echo '1'; }
			}
			//------------------Multiple offer insert-------------------------
		
			}else{
				$deal_type	=	$data[0]['deal_type'];
				$deal_name	=	$data[0]['deal_name'];
				if($deal_type	==	1)
				{
					$deal_type_name	=	'Normal Deal';
				}else{
					$deal_type_name	=	'Shoutout Deal';
				}
				
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$deal_name.' Deal is currently running!</h2><p style="text-align: center;"></p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new deal,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running deal!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['deal_id'].'">DISABLE</button></div></div>';
			
			}
		}
		die;
		//------------------Old Code from here start--------------------
		//------------------Getting Global variable for page--------------------
		$offer_type				=	$_POST['offer_type'];
		$deal_name				=	$_POST['deal_name'];
		$deal_desc				=	'NULL';
		$img_selected_url		=	$_POST['image_priority_url'];
		
		if(isset($_POST['max_capping']))
		{
			$max_capping	=	$_POST['max_capping'];
		}
		else
		{
			$max_capping	=	NULL;
		}
		$min_billing			=	$_POST['min_billing'];
		$image_priority_url		=	$_POST['image_priority_url'];
		$coupon_active_inactive	=	$_POST['coupon_active_inactive'];
		//------------------Getting Global variable for page--------------------
	
		//For Flat Offer
		if($offer_type	==	1)
		{
			//Find out how many Flat deal create it
			$param 	= array("id"=>$_POST['myid']);
			$offer_mp_id	=	$_POST['myid'];
			$data 	= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//one flat deal is create it
			if(count($data) == 0)							
			{
				//echo 'if';
				//$input_value	=	explode("$#$",$_POST['input_value']);
				$flat_discount	=	$_POST['flat_discount'];
				//$offer_timeing	=	$_POST['offer_timeing'];
				
				$offers_day_type	=	$_POST['offers_days'];
				$offer_day_hidden_type=	$_POST['offer_day_hidden_type'];
				$offer_time_hidden_type=	$_POST['offer_time_hidden_type'];
				
				
				
				$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_discount"=>$flat_discount, "offer_type_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
				
				$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
				$lastID;
				if(is_numeric($offer_id))
				{
					$offers_image	=	explode("#",$_POST['image_priority_url']);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
						$gallery_param_temp = array("offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						$offer_gallery_id 	= $GLOBALS["db"]->lastInsertNow('offers_gallery_temp', $gallery_param_temp, 'offer_gallery_created_on');
						
						//$gallery_param 		= array("offer_gallery_id"=>$offer_gallery_id,"offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						//$offer_gallery_idd 	= $GLOBALS["db"]->lastInsertNow('offers_gallery', $gallery_param, 'offer_gallery_created_on');
						$lastID	=	$offer_gallery_id;
						}
						
					}
				}
				if(is_numeric($lastID)){
					//-----START#Admin-Notification Table instertion-----//
					$notifiy_array	= array("an_mp_user_id"=>$offer_id,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP");
					$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
					//-----END#Admin-Notification Table instertion-----//
				}
				echo $lastID;
				
			}
			else//Show old data and tell him to disbale old flat deal first.
			{
				$offer_type_name	=	$data[0]['offer_type_id'];
				$offer_name			=	$data[0]['deal_name'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' Offer is currently running!</h2><p style="text-align: center;">'.$desc.'</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new offer,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running offer!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['offer_id'].'">DISABLE</button></div></div>';
			}
		}
		//For Combo Offer
		if($offer_type	==	2)
		{
			//Check how many offer which is LIVE
			$param 			= array("id"=>$_POST['myid']);
			$offer_mp_id	=	$_POST['myid'];
			$data 			= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//One Combo Offer is create it
			
			
			$offers_day_type	=	$_POST['offers_days'];
			$offer_day_hidden_type=	$_POST['offer_day_hidden_type'];
			$offer_time_hidden_type=	$_POST['offer_time_hidden_type'];
			
			
			if(count($data) == 0)							
			{
				//$input_value	=	explode("$#$",$_POST['input_value']);
				//$offer_timeing	=	$_POST['offer_timeing'];
				/*if($offer_timeing	==	1)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion, "offer_type_id"=>2, "offer_timeing_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);	
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>1,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
				}
				if($offer_timeing	==	2)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					$particular_date_1	=	$_POST['pariticular_date_input'];
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1" => $particular_date_1, "offer_type_id"=>2, "offer_timeing_id"=>2,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1" => $particular_date_1, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>2, "offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
				}
				if($offer_timeing	==	3)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					$particular_date_1	=	$_POST['pariticular_date_time_input'];
					//$time				=	$_POST['pariticular_date_time_input_hh'].":".$_POST['pariticular_date_time_input_mm'];
					$time_1					=	$_POST['pariticular_date_time_input_hh'];
					$ampm_1					=	$_POST['pariticular_date_time_input_hh_hh'];
					$time_2					=	$_POST['pariticular_date_time_input_mm'];
					$ampm_2					=	$_POST['pariticular_date_time_input_mm_mm'];
					$time1					=	$time_1." ".$ampm_1;
					$time2					=	$time_2." ".$ampm_2;
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_type_id"=>2, "offer_timeing_id"=>3,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>3, "offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
				}
				if($offer_timeing	==	4)
				{
					$offer_descriptiion	=	$_POST['short_desc'];
					$particular_date_1	=	$_POST['custom_date_time_input_1'];
					$particular_date_2	=	$_POST['custom_date_time_input_2'];
					//$time				=	$_POST['custom_date_time_input_hh'].":".$_POST['custom_date_time_input_mm'];
					$time_1						=	$_POST['custom_date_time_input_hh'];
					$ampm_1						=	$_POST['custom_date_time_input_hh_hh'];
					$time_2						=	$_POST['custom_date_time_input_mm'];
					$ampm_2						=	$_POST['custom_date_time_input_mm_mm'];
					$time1						=	$time_1." ".$ampm_1;
					$time2						=	$time_2." ".$ampm_2;
					//price is not include Here
					if($_POST['input_chk_value']	==	0)
					{
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_date_2"=>$particular_date_2,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_type_id"=>2, "offer_timeing_id"=>4,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
											
					}else{
					//Price is include Here
						$orignal_price		=	$_POST['price'];
						$offer_price		=	$_POST['offer_selling_price'];
						$discount			=	$_POST['range'];
						$saved_money		=	$_POST['saveAmount'];
						$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_descriptiion,"offer_pariticular_date_1"=>$particular_date_1,"offer_pariticular_date_2"=>$particular_date_2,"offer_pariticular_time_1"=>$time1,"offer_pariticular_time_2"=>$time2, "offer_price"=>$orignal_price, "offer_discount"=>$discount , "offer_save_price"=>$saved_money, "offer_selling_price"=>$offer_price, "offer_type_id"=>2, "offer_timeing_id"=>4, "offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url);
					}
			}*/
			
				$offer_description	=	$_POST['short_desc'];
			
				$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_description,"offer_type_id"=>2,"offer_status"=>1,"max_capping"=>$max_capping,"min_billing"=>$min_billing,"image_priority"=>$img_selected_url,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
			
				$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
				
				$lastID;
				if(is_numeric($offer_id))
				{
					$offers_image	=	explode("#",$_POST['image_priority_url']);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
						$gallery_param_temp = array("offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						$offer_gallery_id 	= $GLOBALS["db"]->lastInsertNow('offers_gallery_temp', $gallery_param_temp, 'offer_gallery_created_on');
						
						//$gallery_param 		= array("offer_gallery_id"=>$offer_gallery_id,"offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						//$offer_gallery_idd 	= $GLOBALS["db"]->lastInsertNow('offers_gallery', $gallery_param, 'offer_gallery_created_on');
						$lastID	=	$offer_gallery_id;
						}
						
					}
					if(is_numeric($lastID)){
						//-----START#Admin-Notification Table instertion-----//
						$notifiy_array	= array("an_mp_user_id"=>$offer_id,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP");
						$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
						//-----END#Admin-Notification Table instertion-----//
					}
					echo $lastID;
				}
				
			}
			else//Show old data and tell him to disbale old deal first.
			{
				$offer_type_name	=	$data[0]['offer_type_id'];
				$offer_name			=	$data[0]['deal_name'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' Offer is currently running!</h2><p style="text-align: center;">'.$desc.'</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new offer,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running offer!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['offer_id'].'">DISABLE</button></div></div>';
			}
		}
		//For shout out
		/*if($offer_type	==	3)
		{
			//Check how many offer which is LIVE
			$param 			= array("id"=>$_SESSION['app_user']['mp_details_id']);
			$offer_mp_id	=	$_SESSION['app_user']['mp_details_id'];
			$data 			= $GLOBALS["db"]->select("SELECT * FROM offers_temp WHERE offer_mp_id =:id AND offer_status = 1", $param);
			//One Combo Offer is create it
			
			
			$offers_day_type	=	$_POST['offers_days'];
			$offer_day_hidden_type=	$_POST['offer_day_hidden_type'];
			$offer_time_hidden_type=	$_POST['offer_time_hidden_type'];
			
			
			if(count($data) == 0)							
			{
				$offer_description	=	$_POST['short_desc'];
			
				$param = array("offer_mp_id"=>$offer_mp_id, "deal_name"=>$deal_name, "deal_description"=>$deal_desc, "offer_description"=>$offer_description,"offer_type_id"=>3,"offer_status"=>1,"image_priority"=>$img_selected_url,"offers_day_type"=>$offers_day_type,"offers_day_value"=>$offer_day_hidden_type,"offers_timeings"=>$offer_time_hidden_type,"offer_coupon_status"=>$coupon_active_inactive);
			
				$offer_id = $GLOBALS["db"]->lastInsertNow('offers_temp', $param, 'offer_created_on');
				
				$lastID;
				if(is_numeric($offer_id))
				{
					$offers_image	=	explode("#",$_POST['image_priority_url']);
					for($i=0;$i<count($offers_image);$i++)
					{
						if($offers_image[$i]!="")
						{
						$gallery_param_temp = array("offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						$offer_gallery_id 	= $GLOBALS["db"]->lastInsertNow('offers_gallery_temp', $gallery_param_temp, 'offer_gallery_created_on');
						
						//$gallery_param 		= array("offer_gallery_id"=>$offer_gallery_id,"offer_image_media"=>$offers_image[$i],"offer_id"=>$offer_id,"offer_gallery_status"=>1);
						
						//$offer_gallery_idd 	= $GLOBALS["db"]->lastInsertNow('offers_gallery', $gallery_param, 'offer_gallery_created_on');
						$lastID	=	$offer_gallery_id;
						}
						
					}
					if(is_numeric($lastID)){
						//-----START#Admin-Notification Table instertion-----//
						$notifiy_array	= array("an_mp_user_id"=>$offer_id,"an_process"=>"create_deal","an_text"=>'',"an_notify_from"=>"MP");
						$insert_notifiy = $GLOBALS["db"]->InsertNow("admin_notification", $notifiy_array, "an_created_time");
						//-----END#Admin-Notification Table instertion-----//
					}
					echo $lastID;
				}
				
			}
			else//Show old data and tell him to disbale old deal first.
			{
				$offer_type_name	=	$data[0]['offer_type_id'];
				$offer_name			=	$data[0]['deal_name'];
				if($data[0]['offer_type_id']	==	1)
				{
					$offer_type_name	=	'Flat Offer';
					$offer_discount		=	$data[0]['offer_discount'];
					$desc				=	'Flat Offer with '.$offer_discount.'% off';
					
				}else{
					$offer_type_name	=	'Combo Offer';
					$desc				=	$data[0]['offer_description'];	
				}
				
				echo '<div><h2 style="text-align: center;font-size: 21px;margin-top: 0px;color: #000;font-weight: 600;">'.$offer_name.' Offer is currently running!</h2><p style="text-align: center;">'.$desc.'</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;margin-bottom: 0px;">If you want to run a new offer,</p><p style="text-align: center;color: #E4142A;font-weight: 600;font-style: italic;">first disable the currently running offer!</p><div style="text-align: center;margin-top: 35px;"><button class="btn btn-success btn-xs" style="padding: 4px 30px;background-color: #CFCFCF;border-color: #CFCFCF;color: #FFFFFF;margin-right: 22px;" data-dismiss="modal">CANCEL</button><button class="btn btn-success btn-xs" style="padding: 4px 30px;" data-dismiss="modal" id="disable_deal" value="'.$data[0]['offer_id'].'">DISABLE</button></div></div>';
			}
		}*/
	}
	function revenue($id)
	{
		$param = array("id"=>$id);
		$data[0] 	= $GLOBALS["db"]->select("SELECT * FROM `booking` 
             WHERE booking_mp_id=:id", $param);
			 
  	   $data[1] = $GLOBALS["db"]->select("SELECT ROUND(AVG(mp_review_score), 1) as avg_cnt FROM mp_reviews 
             WHERE mp_review_mp_details_id=:id", $param);
	  $data[2] = $id; 			 
	   return $data;
	}
	function disable_mp()
	{
		$mp_id	=	$_POST['mp_id'];
		$condition	=	array("mp_details_id"=>$mp_id);
		$param		=	array("mp_status"=>0);
		$update 	= 	$GLOBALS["db"]->update('mp_details_temp', $param, $condition);
		$update1 	= 	$GLOBALS["db"]->update('mp_details', $param, $condition);
		if($update)
		{
			//-----START#Admin-Notification Table instertion-----//
			$notifiy_array	= array("mpn_user_id"=>$mp_id,"mpn_process"=>"mp_status","mpn_response"=>"0");
			$insert_notifiy = $GLOBALS["db"]->InsertNow("mp_notification", $notifiy_array, "mpn_created_time");
			//-----END#Admin-Notification Table instertion-----//
			return "1";
		}
		else
		{
			return "0";
		}
	}
	function active_inactive_mp()
	{
		$mp_id	=	$_POST['mp_id'];
		$condition	=	array("mp_details_id"=>$mp_id);
		$param		=	array("mp_status"=>1);
		$update 	= 	$GLOBALS["db"]->update('mp_details_temp', $param, $condition);
		$update1 	= 	$GLOBALS["db"]->update('mp_details', $param, $condition);
		if($update)
		{
			//-----START#Admin-Notification Table instertion-----//
			$notifiy_array	= array("mpn_user_id"=>$mp_id,"mpn_process"=>"mp_status","mpn_response"=>"1");
			$insert_notifiy = $GLOBALS["db"]->InsertNow("mp_notification", $notifiy_array, "mpn_created_time");
			//-----END#Admin-Notification Table instertion-----//
			return "1";
		}
		else
		{
			return "0";
		}
	}
	function offer_delete()
	{
		$param 	= 	array("offer_id"=>$_POST['id']);
		$data	 = $GLOBALS["db"]->deleteQuery("offers_temp", $param, $limit = 1);
		return $data;
	}
	function edit_deal_shoutout()
	{
		//--------------------------------------------------Deal Update------------------------------------------//
		
		$deal_values_array			=	explode("#",$_POST["deal_values"]);
		$deal_id					=	$_POST["deal_id"];
		$deal_name					=	$deal_values_array[0];
		$deal_shoutout_description	=	$deal_values_array[1];
		$deal_shoutout_day			=	$deal_values_array[2];
		$deal_shoutout_time			=	$deal_values_array[3];		
		
		$condition 			=	array('deal_id'=>$deal_id);
		$deal_name_update 	=	array("deal_name"=>$deal_name,"deal_shoutout_description"=>$deal_shoutout_description,"deal_shoutout_day"=>$deal_shoutout_day,"deal_shoutout_time"=>$deal_shoutout_time);
		$res	=	$insert_deal_name 	=	$GLOBALS["db"]->update('deal_temp', $deal_name_update, $condition);
		$res1	=	$insert_deal_name1 	=	$GLOBALS["db"]->update('deal', $deal_name_update, $condition);
		if($res	==	"true")
		{
			echo 1;
		}else{
			echo 0;
		}
		//--------------------------------------------------Deal Update------------------------------------------//
		
		//--------------------------------------------------Gallery Update------------------------------------------//
		$image_status			=	$_POST['image_status'];
		if($image_status	==	1)
		{
			$image_priority_url		=	$_POST["image_priority_url"];
			$image_url				=	explode("#",$image_priority_url);
			$param_delete			=	array("deal_id"=>$deal_id);
			$delete 				= 	$GLOBALS["db"]->deleteBulkQuery('deal_gallery_temp', $param_delete);
			$image_count			=	count($image_url);
			if($delete	==	"true")
			{	
				for($i=0;$i<$image_count;$i++)
				{
					$param_gallery_update 	=	array("deal_id"=>$deal_id,"deal_image_media"=>$image_url[$i]);
					$insert_up				= 	$GLOBALS["db"]->lastInsertNow('deal_gallery_temp', $param_gallery_update, 'deal_gallery_created_on');
					$param_gallery_update1 	=	array("deal_id"=>$deal_id,"deal_image_media"=>$image_url[$i], "deal_gallery_id"=>$insert_up);
					$insert_up				= 	$GLOBALS["db"]->insertNow('deal_gallery', $param_gallery_update1, 'deal_gallery_created_on');
				}
			}
		}
		//--------------------------------------------------Gallery Update------------------------------------------//
		
		//--------------------------------------------------offer Update------------------------------------------//
		$offer_shout_open	=	$_POST["offer_shout_open"];
		
		if($offer_shout_open	!=	'')
		{
			$offer_values_array	=	explode("#",$offer_shout_open);
			$offertype			=	$offer_values_array[1];
			$offerid			=	$offer_values_array[2];
			$condition 			=	array('offer_id'=>$offerid);
			
			if($offertype	==	'flat')
			{
				$offerdiscount	=	$offer_values_array[3];
				$shout_param 	=	array("offer_discount"=>$offerdiscount);
			}
			if($offertype	==	'combo')
			{
				$offerdesc		=	$offer_values_array[3];
				$shout_param 	=	array("offer_description"=>$offerdesc);
			}
			
			$insert_deal_name1 	=	$GLOBALS["db"]->update('offers_temp', $shout_param, $condition);
			$insert_deal_name2 	=	$GLOBALS["db"]->update('offers', $shout_param, $condition);
			
		}
		//--------------------------------------------------offer Update------------------------------------------//
	}
}
?>