<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class trending1_model {

	function trending1()
	{
		$param = array("status"=>1);
		$data[0] = $GLOBALS["db"]->select("SELECT m.mp_details_name, m.mp_details_id, o.deal_name, o.offer_created_on, offer_id, is_trending, offer_trending_image FROM offers o, mp_details_temp m WHERE o.offer_mp_id = m.mp_details_id AND o.offer_status =:status AND m.mp_status =:status AND o.is_trending=:status", $param);
		$data[1] =  $GLOBALS["db"]->select("SELECT m.mp_details_name, m.mp_details_id FROM mp_details m, offers o WHERE o.offer_mp_id = m.mp_details_id AND o.offer_status =:status AND m.mp_status =:status", $param);
		return $data;
	}
	function showTrend($id)
	{
		return $id;
	}
	function removeTrending()
	{
		$updateParams = array('is_trending'=>0, "offer_trending_image"=>NULL); 
		$condition = array('offer_id'=>$_POST['id']);
		$update = $GLOBALS["db"]->update('offers_temp', $updateParams, $condition);		
		$update1 = $GLOBALS["db"]->update('offers', $updateParams, $condition);	
		if($update=="true")
		{
			$delete = unlink(APP_CRM_UPLOADS.$_POST['image']);
		}
		return $update;
	}
	function addTrending()
	{
		$param = array('trending'=>1);
		$data = $GLOBALS["db"]->select("SELECT o.offer_id FROM offers o WHERE o.is_trending =:trending AND o.offer_status=:trending", $param);
		if(count($data)>3)
		{
			return 0;
		}
		else
		{
			//save into category
			$newNamePrefix = time() . '_';
			$manipulator = new imageManipulator($_FILES['trendImage']['tmp_name']);
			$imageName =  'trending/'.$newNamePrefix . $_FILES['trendImage']['name'];
			$save = $manipulator->save(APP_CRM_UPLOADS.$imageName);
			
			// update data
			$condition = array('offer_id'=>$_POST['id']);
			$updateParams = array('offer_trending_image'=>$imageName, 'is_trending'=>1); 
			$update = $GLOBALS["db"]->update('offers_temp', $updateParams, $condition);		
			$update1 = $GLOBALS["db"]->update('offers', $updateParams, $condition);	
		}
		return $update;
	}
}
?>