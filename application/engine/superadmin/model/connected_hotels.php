<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class connected_hotels_model {

	function connected_hotels()
	{
		$param = array();
		$data = $GLOBALS["db"]->select("SELECT d.deal_id, d.deal_name, d.deal_description, d.deal_image, s.sub_category_name,c.category_name, dm.deal_mp_details_id FROM deals d LEFT JOIN deals_mp dm ON d.deal_id = dm.deal_mp_deal_id LEFT JOIN mp_details mp ON mp.mp_details_id = dm.deal_mp_details_id, sub_category s, category c WHERE FIND_IN_SET(d.deal_sub_cat_id,'".$_SESSION['app_user']['sub_cat']."') AND s.sub_category_id = d.deal_sub_cat_id AND s.sub_category_cat_id = c.category_id", $param);
		return $data;		
	}
	function getLocation()
	{
		$param = array();
		$data = $GLOBALS["db"]->select("SELECT h.hotel_details_id, h.hotel_details_name,h.hotel_details_id,h.hotel_details_address, h.hotel_details_state, h.hotel_details_city, h.hotel_details_phone, h.hotel_details_designation, h.hotel_details_latitude, h.hotel_details_longitude, hr.hotel_mp_relation_id, GROUP_CONCAT(hr.hotel_mp_relation_mp_id) as gotData FROM hotel_details h LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_hotel_id = h.hotel_details_id GROUP BY h.hotel_details_id", $param);
		return json_encode($data);
	}
	function getLocationData()
	{
		$param = array();
		$data[0] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS h.hotel_details_id, h.hotel_details_contact, h.hotel_details_person, h.hotel_details_name, h.hotel_details_description, h.hotel_details_id,h.hotel_details_address, h.hotel_details_state, h.hotel_details_city, h.hotel_details_phone, h.hotel_details_designation, h.hotel_details_latitude, h.hotel_details_longitude, hr.hotel_mp_relation_id, GROUP_CONCAT(hr.hotel_mp_relation_mp_id) as gotData, COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(h.hotel_details_latitude)) * COS(RADIANS(77.2090212)) * COS(RADIANS(h.hotel_details_longitude - 28.6139391))+ SIN(RADIANS(h.hotel_details_latitude))* SIN(RADIANS(28.6139391)))), 0) AS mp_dist_km FROM hotel_details h LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_hotel_id = h.hotel_details_id GROUP BY h.hotel_details_id LIMIT 10", $param);
		
		$data[3] = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		
		$paramEmpty = array();
		$data[1] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as totalAmount FROM booking WHERE (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
		$data[2][] = array();
			if(!empty($data[0]))
			{
				for($i=0; $i<count($data[0]); $i++)
				{
					$data[2][] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as amount FROM booking WHERE booking_hotel_id = ".$data[0][$i]['hotel_details_id']." AND (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
				}
			}
		return $data;
	}
	function filter_result_hp()
	{
		if(isset($_POST['page_number']))
		{
			preg_match_all('!\d+!', $_POST['page_number'], $page_number);
			//$limit = $_POST['limit'];
			$limit = $page_number[0][0];
			
		}
		else
		{
			$limit = 1;
		}
		$ulimit = $limit * 10;
		$llimit = $ulimit -  10;
		
		$param = array();
		$data[0] = $GLOBALS["db"]->select("SELECT SQL_CALC_FOUND_ROWS h.hotel_details_id, h.hotel_details_contact, h.hotel_details_person, h.hotel_details_name, h.hotel_details_description, h.hotel_details_id,h.hotel_details_address, h.hotel_details_state, h.hotel_details_city, h.hotel_details_phone, h.hotel_details_designation, h.hotel_details_latitude, h.hotel_details_longitude, hr.hotel_mp_relation_id, GROUP_CONCAT(hr.hotel_mp_relation_mp_id) as gotData, COALESCE(111.1111 * DEGREES(ACOS(COS(RADIANS(h.hotel_details_latitude)) * COS(RADIANS(77.2090212)) * COS(RADIANS(h.hotel_details_longitude - 28.6139391))+ SIN(RADIANS(h.hotel_details_latitude))* SIN(RADIANS(28.6139391)))), 0) AS mp_dist_km FROM hotel_details h LEFT JOIN hotel_mp_relation hr ON hr.hotel_mp_relation_hotel_id = h.hotel_details_id GROUP BY h.hotel_details_id LIMIT ".$llimit.", 10", $param);
		
		$data[3] = $GLOBALS["db"]->select("SELECT FOUND_ROWS() as count; ");
		
		$paramEmpty = array();
		$data[1] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as totalAmount FROM booking WHERE (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
		$data[2][] = array();
			if(!empty($data[0]))
			{
				for($i=0; $i<count($data[0]); $i++)
				{
					$data[2][] = 	$GLOBALS["db"]->select("SELECT SUM(booking_amount) as amount FROM booking WHERE booking_hotel_id = ".$data[0][$i]['hotel_details_id']." AND (booking_status = 2 OR booking_status = 1 AND (CURDATE() > DATE_ADD(booking_created_on,INTERVAL 7 DAY)))", $paramEmpty);
				}
			}
		return $data;
	}
}
?>