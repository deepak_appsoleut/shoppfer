<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Superadmin Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
     <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
 <style>
#loading {
    position: absolute;
	display:none;
	top:5%;
    z-index: 999999;
}
</style>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $deal = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:40px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <!--<header class="panel-heading" style="border-bottom:1px solid #E2E2E4;">
                              Deal Notifications
                          </header>-->
                          <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong> Deal Notifications</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Date</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Notification</th>
                  <th>Status</th>
                  <th>Operation</th>
                </tr>
                </thead>
               <tbody>
               <?php 
				for($i=0; $i<count($data); $i++) { ?>
               <tr role="row">
               		<td><?php echo date('j F,Y h:i A', strtotime($data[$i]["an_created_time"])); ?></td>
                    <td><?php echo $data[$i]["name"]; ?></td>
                    <td><?php echo $data[$i]["deal_name"]; ?></td>
                    <td><?php if($data[$i]["an_process"]=='create_deal') { 
                       if($data[$i]["an_status"]==0)
						  {
							  $button = '<a class="btn btn-danger" data-target="#dealComplete" href="'.APP_URL.'superadmin/deal_notifications/deal_complete/'.$data[$i]["an_id"].'" data-toggle="modal">View</a>';
						  }
						  else
						  {
							  $button = '<button class="btn btn-danger" disabled>Already viewed</button>';
						  }
					  ?>
                      <p>Deal has been added!</p>
                      <?php } else if($data[$i]["an_process"]=='deal_update'||$data[$i]["an_process"]=='deal_shoutout_update' ||$data[$i]["an_process"]=='offer_update') { 
                       if($data[$i]["an_status"]==0)
						  {
							  $button = '<a class="btn btn-danger" data-target="#dealComplete" href="'.APP_URL.'superadmin/deal_notifications/deal_update/'.$data[$i]["an_id"].'/'.$data[$i]["an_process"].'" data-toggle="modal">View</a>';
						  }
						  else
						  {
							  $button = '<button class="btn btn-danger" disabled>Already viewed</button>';
						  }
					  ?>
                      <p>Deal has been updated!</p>
                      <?php }
						else if($data[$i]["an_process"]=='deal_status') { 
                       		if($data[$i]["an_status"]==0)
						  {
							  $button = '<a class="btn btn-danger" data-target="#galleryUpdate" href="'.APP_URL.'superadmin/deal_notifications/dealStatus/'.$data[$i]["an_id"].'" data-toggle="modal">View</a>';
						  }
						  else
						  {
							  $button = '<button class="btn btn-danger" disabled>Already viewed</button>';
						  }
					  ?>
                      <p>Deal status changed!</p>
                      <?php }					  
					   ?>
                      </td>
                    
                    <td><?php	
					if($data[$i]["an_process"]!='deal_status' && $data[$i]["an_process"]!='deal_update' && $data[$i]["an_process"]!='deal_shoutout_update')
					{
						if($data[$i]["deal_image_status"]==0)
						  {
							  $designer_status = '<button class="btn btn-danger" disabled>Pending</button>';
						  }
						  else
						  {
							  $designer_status = '<button class="btn btn-primary" disabled>Approval</button>';
						  }
					}
					else
					{
						$designer_status = '<button class="btn btn-primary" disabled>Approval</button>';
					}
					 echo $designer_status; ?></td>
                    <td><?php echo $button; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
              </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="profileComplete">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="profileUpdate">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="galleryUpdate">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="dealComplete">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>
</body>

<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script>
  $(function () {
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
	  aaSorting : [[0, 'desc']]
    });
	
	$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal', '.modal', function () {
$(this).removeData('bs.modal');
});

  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
