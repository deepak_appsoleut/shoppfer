<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />  
   <style>
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{ border-top:none;}
.table>thead>tr>th{border-bottom: 0px solid #ddd;}
#example_length,#example_filter{ display:none !important;}
table.table thead .sorting{ background:none !important;}
table.table thead .sorting_asc{ background:none !important;}
.has-switch span.switch-left{background-color: #13D4A8;color: #fff;}
.has-switch span.switch-right{background-color: #FF6C5E;color: #fff;}
.has-switch > div.switch-on label{background-color: #fff !important;}
.has-switch > div.switch-off label{background-color: #fff !important;}
.has-switch label{border: 7px solid #13D4A8;}
.has-switch{ width: 65px;height: 28px;}
.has-switch > div{ width:163%;}
.has-switch > div.switch-off label{border-color: #FF6C5E;}
.has-switch span{ font-weight:400;}
.site-min-height {min-height: 0px;}

</style>
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $connect = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:45px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">					
                      <section class="panel">
                      <header class="panel-heading" style="border-bottom:1px solid #E2E2E4; padding:0">
                          <div class="row">
                          	<div class="col-md-12">	
                            	<a href="<?php echo APP_URL; ?>superadmin/connected_mp/merchant_view/<?php echo $data[1]; ?>" class="btn btn-primary" style="border-radius:0; background-color:#e51429; border-color:#fff">Profile Details</a><a href="<?php echo APP_URL; ?>superadmin/connected_mp/deal_view/<?php echo $data[1]; ?>" class="btn btn-primary" style="border-radius:0; background-color:#980316; border-color:#fff">Deal Details</a><a href="<?php echo APP_URL; ?>superadmin/connected_mp/revenue/<?php echo $data[1]; ?>" class="btn btn-primary" style="border-radius:0; background-color:#e51429; border-color:#fff">Revenue Details</a>
                              </div>
</div>
                          </header>
                      
                      		<div id="message"></div><span class="pull-right" style="margin-top: 10px;">
                          			<a href="<?php echo APP_URL; ?>superadmin/connected_mp/createDeal/<?php echo $data[1]; ?>" class=" btn btn-success btn-xs" style="padding: 3px 14px;font-size: 12px;"> Create New Deals</a>
                     			</span>
                          	<header class="panel-heading">
                              <!--<strong>Manage Deals</strong>-->
                              <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Manage Deals</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                              <span class="pull-right">                              
                         		</header>
                          	<div class="panel-body">
                          		<div class="adv-table">
                                	<!--table-bordered-->
                                    <table id="example" class="table table-striped" style="border: 1px solid #F1F2F7;">
                                    <thead style="border-bottom: 1px solid #F1F2F7;">
                                    <tr>
                                        <th style="text-align: center;font-weight: normal;">ID</th>
                                        <th style="text-align: left;font-weight: normal;">Deal Name</th>
                                        <th style="text-align: center;font-weight: normal;">&nbsp;</th>
                                        <th style="text-align: center;font-weight: normal;">Deal Status</th>
                                        <th style="text-align: center;font-weight: normal;">Deal Delete</th>
                                        <th style="text-align: center;font-weight: normal;">View Deals</th>
                                        <th style="text-align: center;font-weight: normal;">Deal Edit</th>
                                    </tr>
                                    </thead>
                                   <tbody>
									<?php 
  									$disabled = "";
                                    $deal_mp_id = "";
									for($i=0; $i<count($data[0]); $i++) {
                                    $deal_mp_id = $data[0][$i]["deal_mp_id"];
                                    ?>
                                    <tr role="row">
                                        <td style="text-align: center;"><?php echo $i+1; ?></td>
                                        <td style="text-align: left;font-weight: bold;"><?php echo $data[0][$i]["deal_name"]; ?>
                                        <br>
                                         <?php 														
										if($data[0][$i]["deal_status"]==1)
										{ 
											 $active	=	'<div style="width: 83px;margin: 0 auto;" class="inactive" title="green" id="'.$data[0][$i]["deal_id"].'"><input type="checkbox" '.$disabled.' checked data-toggle="switch" /></div>';	 
										}
										else
										{
											$active	=	'<div style="width: 83px;margin: 0 auto;" class="active1" title="rad" id="'.$data[0][$i]["deal_id"].'"><input type="checkbox" '.$disabled.' data-toggle="switch"/></div>';
										}
										?>
                                        </td>
                                        <td style="text-align: left;">
                                       
                                        </td>
                                        <td style="text-align: center;">
										<?php echo $active; ?>
                                        </td>
                                        <td style="text-align: center;width: 120px;">
                                         <?php if($disabled=="") { ?>
                                        <a class="delete" id="<?php echo $data[0][$i]["deal_id"]; ?>" style="cursor:pointer;">
                                        	<i class="fa fa-trash-o" aria-hidden="true" style="font-size: 21px;cursor:pointer;"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a style="cursor:pointer;">
                                        	<i class="fa fa-trash-o" aria-hidden="true" style="font-size: 21px;cursor:pointer;"></i>
                                        </a>
                                        <?php } ?>
                                        </td>
                                        <td style="text-align: center;">
                                        <a class="view_deals_offers" id="<?php echo $data[0][$i]["deal_id"]; ?>" style="cursor:pointer;">
                                      	<!--<i class="fa fa-file-o" aria-hidden="true" style="font-size: 21px;cursor:pointer;"></i>-->
                                       <img src=" <?php echo APP_IMAGES.'VIEW.png' ?>" />
                                        </a>
                                        </td>
                                        <td style="text-align: center;">
                                        <?php if($disabled=="") { ?>
                                        <a style="padding: 2px 17px;" class="btn btn-round btn-info" href="<?php echo APP_URL; ?>superadmin/connected_mp/edit_deal/<?php echo $data[0][$i]['deal_id'];?>">
                                        	EDIT
                                            </a>
                                        <?php } else { ?>
                                         <a href="#" class="btn btn-round btn-info" style="padding: 2px 17px;">
                                         EDIT
                                        <a>
                                        <?php } ?>
                                        </td>
                                    </tr>
                    <?php 
					}
				?>
                </tbody>
              </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      
    <!-- Modal Dialog -->
        <div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Parmanently</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure about this ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger confirm_delete" id="">Delete</button>
              </div>
            </div>

          </div>
        </div>
        <div class="modal fade" id="disableoffer" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Disable Offer</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure about this ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger disable_offer" id="">Disable</button>
              </div>
            </div>
          </div>
        </div>
         <!-- Modal Dialog -->
</section>
<!-- Modal Dialog -->
        <div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content" style="top: 100px;width: 440px;float: right;margin-right: 5%;">
              <div class="modal-header" style="padding-top: 10px;padding-bottom: 10px;">
                <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <h4 class="modal-title">DELETED</h4>
              </div>
              <div class="modal-body">
              	<div style="width: 100px;float: left;">
                <img src="<?php echo APP_IMAGES.'del.png' ?>"/>
                </div>
                <p style="color: #000;font-size: 15px;margin-top: 10px;font-weight: 500;">Are you sure want to deleted this deal?</p>
              </div>
              <div class="modal-footer" style="border-top:none !important;">
               <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>-->
                <!--<button type="button" class="btn btn-danger confirm_delete" id="">Delete</button>-->
                <button class="btn btn-success btn-xs" style="padding: 2px 20px;background-color: #CFCFCF;border-color: #CFCFCF;" data-dismiss="modal">CANCEL</button>
                <button class="btn btn-success btn-xs confirm_delete" style="padding: 2px 30px;">OK</button>
              </div>
            </div>

          </div>
        </div>
        <div class="modal fade" id="disableoffer" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content" style="top: 100px;width: 440px;float: right;margin-right: 5%;">
              <div class="modal-header" style="padding-top: 10px;padding-bottom: 10px;">
                <h4 class="modal-title">DISABLE OFFER</h4>
              </div>
              <div class="modal-body">
              	<div style="width: 100px;float: left;">
                <img src="<?php echo APP_IMAGES.'del.png' ?>"/>
                </div>
                <p style="color: #000;font-size: 15px;margin-top: 10px;font-weight: 500;">Are you sure want to disable this deal?</p>
              </div>
              <div class="modal-footer" style="border-top:none !important;">
                <button class="btn btn-success btn-xs can" style="padding: 2px 20px;background-color: #CFCFCF;border-color: #CFCFCF;" data-dismiss="modal" type="button">CANCEL</button>
                <button class="btn btn-success btn-xs disable_offer" style="padding: 2px 30px;">OK</button>
              </div>
            </div>
          </div>
        </div>
         <!-- Modal Dialog -->
         
         <!--MODEL CODE HERE START HERE-->
<div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content" style="top: 110px;width: 500px;float: right;margin-right: 200px;">
                                        <div class="modal-header" style="text-align: center;font-size: 16px;padding-top: 10px;padding-bottom: 10px;
font-weight: 600;">
                                        NOTE
                                            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                                        </div>
                                        <div class="modal-body" id="getCode">
                                            
                                        </div>
                                        <input type="hidden" class="image_priority" name="image_priority" value="" />
                                       <!-- <div class="modal-footer" style="border-top:none;">-->
                                        <!--<button type="button" id="m_save" class="btn btn-default" style="display:none;">save</button>-->
                                        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                        
                                       
                                       <!-- </div>-->
                                    </div>
                                </div>
                            </div>
<div class="modal fade" id="getCodeModal_offerview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="top: 110px;width: 700px;float: right;margin-right: 100px;">
            <div class="modal-header" style="text-align: center;font-size: 16px;padding-top: 10px;padding-bottom: 10px;
font-weight: 600;background: #fff !important;color: #000;">
			<div class="modal-content1">
            </div>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="<?php echo APP_IMAGES.'cross.png' ?>" style="margin-top: -50px;"/>
            </button>
            </div>
            
            <div class="modal-body" id="getCode_offerview" style="padding-top: 10px;">
            </div>
        </div>
    </div>
</div>
<!--MODEL CODE HERE START HERE-->
</body>
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script src="http://thevectorlab.net/flatlab/js/bootstrap-switch.js"></script>
<script>
  $(function () {
	  
	$("[data-toggle='switch']").wrap('<div class="switch"/>').parent().bootstrapSwitch();
	
	$(document).on('click', '.delete', function(e){
		var id	=	$(this).attr("id");
		$(".confirm_delete").attr('id',id);
		jQuery("#confirmDelete").modal('show');
	});
	
	$(document).on('click', '.confirm_delete', function(e){
		var deal_id = $(this).attr('id');
		$.ajax({
				  url: '<?php echo APP_URL; ?>admin_mp/deals/delete_deals',
				  type: 'POST',
				  data: {deal_id : deal_id},
				  success: function (data) {
					  if(data	==	1)
					  {
						  	jQuery("#confirmDelete").modal('hide');
							$('#message').html('');
							var success = '<div class="alert alert-success alert-dismissable" style="margin-left: 15px;"> <i class="fa fa-ban"></i> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Offer Delete successfully!</span></div>';
							$('#message').html(success);
							window.setTimeout(function(){location.reload()},1000);
							$("html, body").animate({ scrollTop: 0 }, 600);
					  }else{
					  		jQuery("#confirmDelete").modal('hide');
							$('#message').html('');
							var success = '<div class="alert alert-success alert-dismissable" style="margin-left: 15px;"> <i class="fa fa-ban"></i> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>There is some Problme please contat to Admin!</span></div>';
							$('#message').html(success);
							window.setTimeout(function(){location.reload()},3000);
							$("html, body").animate({ scrollTop: 0 }, 600);
					  }
				  }
		 });
	});
	
	$(document).on('click', '.disable_offer', function(e){
		var deal_id	=	$(this).attr("id");
		$.ajax({
				url: '<?php echo APP_URL; ?>superadmin/connected_mp/disable_deal',
				 type: 'POST',
				 data: {'deal_id':deal_id},
				 success: function (data) {
					// alert(data);
					 location.reload();
				 }
		});
	});
	
	$(document).on('click', '.inactive', function(e){
		var id	=	$(this).attr("id");
		var dis = $('#'+id+' input').attr("disabled");
		if(dis=="disabled")
		{
			return false;
		}
		else
		{
			$(".disable_offer").attr('id',id);
			jQuery("#disableoffer").modal('show');
		}
	});
	
	$(document).on('click', '.can', function(e){
		$(".inactive").html('<div class="switch has-switch"><div class="switch-animate switch-on"><input type="checkbox" checked="" data-toggle="switch"><span class="switch-left">ON</span><label>&nbsp;</label><span class="switch-right">OFF</span></div></div>');
	});
	
	
	
	
	var global_offer_id	=	'';
	$(document).on('click', '.active1', function(e){
		var deal_id	=	$(this).attr("id");
		var dis = $('#'+deal_id+' input').attr("disabled");
		if(dis=="disabled")
		{
			return false;
		}
		else
		{
			global_offer_id	=	deal_id;
			$.ajax({
					 url: '<?php echo APP_URL; ?>superadmin/connected_mp/active_inactive',
					 type: 'POST',
					 data: {'deal_id':deal_id, "mp_id":<?php echo $deal_mp_id; ?>},
					 success: function (data) {
						if(data	==	1){
							 location.reload();
						}else{
						 $("#getCode").html(data);
						 jQuery("#getCodeModal").modal('show');
						}
					 }
			});
		}
	});
	
	$(document).on('click', '.popupcancel', function(e){
		var idd	=	global_offer_id;
		
		
		
		$("#"+idd).html('<div class="switch has-switch"><div class="switch-animate switch-off"><input type="checkbox" data-toggle="switch"><span class="switch-left">ON</span><label>&nbsp;</label><span class="switch-right">OFF</span></div></div>');
	});
	
	
	
	  
	//-------------Disable Deal-------------------------------//
	$(document).on('click', '#active_inavtive_deal', function(){
			var id_value	=	$(this).val();
			var id_val		=	id_value.split("-");
			var deal_id		=	id_val[0];
			var status		=	id_val[1];
			
			$.ajax({
				url: '<?php echo APP_URL; ?>admin_mp/createDeal/disable_deal',
				type: 'POST',
				data: {'deal_id':deal_id},
					success: function (data) {
					//alert(data);
					if(data	==	1)
					{
						if(data	==	1)
						{
							$.ajax({
								url: '<?php echo APP_URL; ?>admin_mp/createDeal/enable_deal',
								type: 'POST',
								data: {'deal_id':global_offer_id},
									success: function (data) {
									if(data	==	1)
									{
										//$("#getCode").html('<h3 style="text-align:center;">This Offer Is Succssfully Enabled</h3>');
										//jQuery("#getCodeModal").modal('show');
										location.reload();
									}
									else
									{
									}
								},
								error: function (data) {
								}
							});
						}else{
							alert('response Inactive');
						}
					}
					else
					{
						$("#getCode").html('<h3 style="text-align:center;">Something Going Wrong.</h3><p style="text-align: center;">Please Contact To Adminstrator</p>');
						jQuery("#getCodeModal").modal('show');
					}
				},
					error: function (data) {
				}
			});
			
	});
	//-------------Disable Deal-------------------------------//  
	
	$(document).on('click', '.view_deals_offers', function(){
		var id 	=	$(this).attr("id");
		$.ajax({
			url: '<?php echo APP_URL; ?>admin_mp/createDeal/view_deal_offers',
			type: 'POST',
			data: {'deal_id':id},
			success: function (data) {
				//alert(data);
				var d	=	data.split("#$");
				$(".modal-content1").html(d[0]);
				$("#getCode_offerview").html(d[1]);
				jQuery("#getCodeModal_offerview").modal('show');		
			},
			error: function (data) {
			}
		});
	});
	
	  
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
</html>
