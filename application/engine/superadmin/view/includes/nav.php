<aside>
<style>
.text_title{font-size: 14px;font-weight: bold;margin-left: 10px;font-family: 'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif;}
.top-nav {
    margin-top: 16px;
}
</style>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a class="<?php echo $dashboard ?>" href="<?php echo APP_URL; ?>superadmin/dashboard">
                          <!--<i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Dashboard</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $note ?>" href="<?php echo APP_URL; ?>superadmin/all_notifications">
                          <!--<i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Notifications</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $deal ?>" href="<?php echo APP_URL; ?>superadmin/deal_notifications">
                          <!--<i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Deal Notifications</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $connect ?>" href="<?php echo APP_URL; ?>superadmin/connected_mp">
                         <!-- <i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Marketing Partners</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $images ?>" href="<?php echo APP_URL; ?>superadmin/image_upload">
                          <!--<i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Image Upload</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $trend ?>" href="<?php echo APP_URL; ?>superadmin/trending">
                         <!-- <i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Trending</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $send ?>" href="<?php echo APP_URL; ?>superadmin/send_notification">
                          <!--<i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Send Notification</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php echo $analytics ?>" href="<?php echo APP_URL; ?>superadmin/analytics">
                          <!--<i class="fa fa-dashboard"></i>-->
                          <span class="text_title">Analytics</span>
                      </a>
                  </li>

                  <!--<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-laptop"></i>
                          <span>Layouts</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="boxed_page.html">Boxed Page</a></li>
                          <li><a  href="horizontal_menu.html">Horizontal Menu</a></li>
                          <li><a  href="language_switch_bar.html">Language Switch Bar</a></li>
                          <li><a  href="email_template.html" target="_blank">Email Template</a></li>
                      </ul>
                  </li>-->

              </ul>
              <!-- sidebar menu end-->
          </div>
</aside>