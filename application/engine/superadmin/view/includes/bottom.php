<!-- jQuery 2.1.4 -->
<script src="<?php echo APP_CRM_BS; ?>js/jquery.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap.min.js"></script>
<script>
function runNotification()
{	
$.ajax({
		url: '<?php echo APP_URL; ?>superadmin/notification',
		type: 'POST',
		success: function (data) {
			  	if(data.match('!doctype'))
				{
				 top.location = "<?php echo APP_URL; ?>superadmin/login";
				}
			$('#header_notification_bar').html(data);
		},
		error: function (data) {
		}
});	
}
runNotification();
setInterval(function(){runNotification();}, 120000);
$('#header_notification_bar').click(function()
{
	$.ajax({
		url: '<?php echo APP_URL; ?>superadmin/notification/viewNotification',
		type: 'POST',
		success: function (data) {
			$('#header_notification_bar .badge').html('');
		},
		error: function (data) {
		}
	});	
});
</script>