<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Trending</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
     <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
	.progress 
	{
		margin-top:5px;
	}
	.progress .percent
	{
		color:#fff;
		padding-left:10px;
	}
	.details
	{
		float:left;
		width:80%;
	}
	#spinThm, #spinCat
	{
		position:absolute;
		top:45%;
		left:35%;
		display:none;
		color:#000;
	}
	.fa-spinner
	{
		position:absolute;
		top:35%;
		left:45%;
		color:#000;
		display:none;
	}
</style>
 <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/select2.min.css"> 
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $trend = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:40px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Trending Merchants
                          </header>
                          <div class="panel-body">
                          <div class="row">
                          <?php for($i=0; $i<count($data[0]); $i++) {
						   if($data[0][$i]["offer_trending_image"]==NULL) { 
						  $data[0][$i]["offer_trending_image"] = "no-thumb.jpg";
						   } 
						  ?>           
                          
                          <div class="col-md-3">
                          	<select class="form-control select2" name="name" style="width: 100%;">
                              <option value="">--Select Merchant--</option>
                              <?php for($j=0; $j<count($data[1]); $j++) { 
							  if($data[0][$i]["mp_details_id"]==$data[1][$j]['mp_details_id'])
							  {
							  ?>
                              <option selected value="<?php echo $data[1][$j]['mp_details_id'] ?>"><?php echo $data[1][$j]['mp_details_name']; ?></option>
                              <?php }
							  else
							  {
								  ?>
                                   <option value="<?php echo $data[1][$j]['mp_details_id'] ?>"><?php echo $data[1][$j]['mp_details_name']; ?></option>
                                  <?php
							  }
							  }
							  ?>
                            </select>
                            
                         <img style="border:1px solid #ccc;margin-bottom:10px; margin-top:15px" width="230" height="150" id="img<?php echo $data[0][$i]["mp_details_id"]; ?>" src="<?php echo APP_CRM_UPLOADS_PATH.$data[0][$i]["offer_trending_image"]; ?>">
                          <div style="clear:both"></div>
                         <input type="file" class="details" name="det<?php echo $data[0][$i]["mp_details_id"]; ?>" id="det<?php echo $data[0][$i]["mp_details_id"]; ?>">
                         <i class="fa fa-spinner fa-pulse fa-3x" id="spin<?php echo $data[0][$i]["mp_details_id"]; ?>"></i>
                          <div style="clear:both"></div>
                         <div class="progress" id="progress<?php echo $data[0][$i]["mp_details_id"]; ?>" style="display:none">
                                <div class="percent" id="percent<?php echo $data[0][$i]["mp_details_id"]; ?>">0%</div >
                            </div>
                          
                          <button type="button" data-id="<?php echo $data[0][$i]["mp_details_id"]; ?>" class="btn btn-primary upload">Upload &amp; Save</button>
                            
                          </div>
                         <?php } 
						 for($j=$i; $j<4; $j++)
						 {
							 ?>
                             <div class="col-md-3">
                             
                          	<select class="form-control select2" name="name" style="width: 100%;">
                              <option value="">--Select Merchant--</option>
                              <?php for($j=0; $j<count($data[1]); $j++) { ?>
                              <option value="<?php echo $data[1][$j]['mp_details_id'] ?>"><?php echo $data[1][$j]['mp_details_name']; ?></option>
                              <?php } ?>
                            </select>
                            
                         <img style="border:1px solid #ccc;margin-bottom:10px; margin-top:15px" width="230" height="150" id="img" src="<?php echo APP_CRM_UPLOADS_PATH.'no-thumb.jpg'; ?>">
                          <div style="clear:both"></div>
                         <input type="file" class="details" name="" id="det">
                         <i class="fa fa-spinner fa-pulse fa-3x" id="spin"></i>
                          <div style="clear:both"></div>
                         <div class="progress" id="progress" style="display:none">
                                <div class="percent" id="percent">0%</div >
                            </div>
                            <button type="button" data-id="" class="btn btn-primary upload">Upload &amp; Save</button>
                          </div>
                             <?php
						 }
						 ?>
                          </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>

</body>

<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script src="<?php echo APP_CRM_BS; ?>js/select2.full.min.js"></script>
<script>
  $(function () {
	   $(".select2").select2();
		// for selecting image
	   $("input:file").change(function() 
		{
		var file = this.files[0];
		var imagefile = file.type;
		var id = $(this).attr('id');
		removedStr = id.substr(3);
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
			alert("Please select a proper format jpeg, png or jpg");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.readAsDataURL(this.files[0]);
			reader.onload = function (e) {
					var image  = new Image();
					image.src = e.target.result;
					image.onload = function () {
						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;
							if(width!=800&&height!=309)
							{
								alert("Please upload correct size");
								return false;	
							}
							else
							{
								$('#img'+removedStr).attr('src', e.target.result);
								$('#img'+removedStr).attr('height', '150px');
							}
						}
					}
			
		}
	});
		//to upload image
		
}); 
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
