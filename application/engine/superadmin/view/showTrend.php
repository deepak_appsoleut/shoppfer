<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">Add in Trending</h4>
    </div>
    <div class="modal-body">
      <div id="message"></div>
      <form id="form" method="post">
      		<div class="row">
            	<div class="col-md-12">
                	<img src="" id="img" style="margin-bottom:10px" />
                	<input type="hidden"  name="id" value="<?php echo $data; ?>" />
                	<input type="file" id="trendImage" name="trendImage" />
                </div>
                <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x" style="margin-right:10px;"></i>Loading Data....</div>	
                <div class="col-md-12" style="margin-top:20px">
                	<button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
      </form>
    </div>
<script>	
$(document).ready(function (e) {
$("#form").on('submit',(function(e) {
			$('#loading').show();
			var trendImage = $('#trendImage').val();
			if(trendImage!="")
			{
				e.preventDefault();
				$.ajax({
				url: "<?php echo APP_URL; ?>superadmin/trending/addTrending", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					console.log(data);
					$('#loading').hide();
					if(data=="true")
					{
						$('#message').html('');
						var success = '<div class="alert alert-success alert-dismissable" style="margin-left: 15px;"> <i class="fa fa-ban"></i> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Image Uploaded successfully!</span></div>';
						$('#message').html(success);
						window.setTimeout(function(){location.reload()},3000);
					}
					else if(data==0)
					{
						$('#message').html('');
						var success = '<div class="alert alert-danger alert-dismissable" style="margin-left: 15px;"> <i class="fa fa-ban"></i> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error : </b> <span>You can not add more than 4 trending images!!</span></div>';
						$('#message').html(success);
						return false;
					}
				}
				});
			}
			else
			{
				alert('Please upload pic!');
				return false;
			}
}));
});

$(function() 
{
	$("#trendImage").change(function() 
	{
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
			alert("Please select a proper format jpeg, png or jpg");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.readAsDataURL(this.files[0]);
			reader.onload = function (e) {
					var image  = new Image();
					image.src = e.target.result;
					image.onload = function () {
						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;
							if(width!=800&&height!=309)
							{
								alert("Please upload correct size");
								return false;	
							}
							else
							{
								$('#img').attr('src', e.target.result);
								$('#img').attr('height', '63px');
							}
						}
					}
			
		}
	});
});
</script>
    