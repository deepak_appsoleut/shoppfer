<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">View Signup Deatails</h4>
    </div>
    <div class="modal-body">
      <div class="error"></div>
      <form id="form" method="post">
      <div class="row">
      		<div class="col-md-6">
            	<label><strong>MP Name : </strong></label>
                <span><?php echo ucwords($data[0]['mp_details_name']); ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Contact : </strong></label>
                <span><?php echo $data[0]['mp_details_email']; ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Created on : </strong></label>
                <span><?php echo date('F j, Y h:i A', strtotime($data[0]['mp_created_on'])); ?></span>
                <div class="clearfix"></div>
            </div>
      </div>
     
       <div class="row">
       	 <div class="col-md-6">
        	<input type="hidden" name="an_id" value="<?php echo $data[0]['an_id']; ?>"  />
        </div>
       <div class="col-md-6 pull-right">  
       <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x pull-right" style="margin-right:10px;"></i></div>  
       <button class="btn-warning btn custom_warning_btn pull-right"  style="margin-left:20px" data-dismiss="modal" aria-label="Close" type="button">Cancel</button>
       <button class="btn-danger btn custom_warning_btn pull-right" name="v" value="v" type="submit">Ok</button>
      </div>
       </div>
      </form>
    </div>
    <script>
	
$('#form').bootstrapValidator({
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
		$('#loading').show();
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/all_notifications/verifySignupStatus',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				$('#loading').hide();
				if(data=="true")
				{
					$('.error').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Action is successfull!</span></div>');
					 location.reload();
				}
				else
				{
					$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span>There is some error!</span></div>');
				}
            },
            error: function (data) {
            }
   });
});
</script>