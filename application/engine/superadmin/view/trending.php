<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Superadmin Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
     <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
#loading {
   margin-left: 15px;
    margin-top: 15px;
    float: left;
	display:none;
}
</style>
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $trend = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:40px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <!--<header class="panel-heading" style="border-bottom:1px solid #E2E2E4;">
                              Trending
                          </header>-->
                          <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Trending</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>MP Name</th>
                   <th>Offer Name</th>
                  <th>Offer Created On</th>
                  <th>Image</th>
                  <th>Operation</th>
                </tr>
                </thead>
               <tbody>
               <?php 
				for($i=0; $i<count($data); $i++) { ?>
               <tr role="row">
                    <td><?php echo $data[$i]["deal_id"]; ?></td>
                     <td><?php echo $data[$i]["mp_details_name"]; ?></td>
                     <td><?php echo $data[$i]["deal_name"]; ?></td>
                    <td><?php echo date("d-m-Y", strtotime($data[$i]['deal_created_on'])); ?></td>
                    <td><a href=""><a target="_blank" href="<?php echo APP_CRM_UPLOADS_PATH.$data[$i]["deal_trending_image"]; ?>"><img width="100" src="<?php echo APP_CRM_UPLOADS_PATH.$data[$i]["deal_trending_image"]; ?>"></a></td>
                    <td>
                    <?php if($data[$i]["is_trending"]==0) { ?>
                    <a class="btn btn-primary" data-target="#showTrend" data-toggle="modal" href="<?php APP_URL; ?>trending/showTrend/<?php echo $data[$i]["deal_id"]; ?>">Add in Trending</a></td>
                    <?php } else { ?>
                    <button class="btn btn-danger remove" data-id="<?php echo $data[$i]["deal_trending_image"]; ?>" id="<?php echo $data[$i]["deal_id"]; ?>">Remove Trending</button></td>
                    <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
              </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="showTrend">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	
    </div>
  </div>
</div>

</body>

<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script>
  $(function () {
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
	  aaSorting : [[5, 'desc']]
    });
	
	$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal', '.modal', function () {
$(this).removeData('bs.modal');
});

$(document).on('click', '.remove', function(){
	var id = $(this).attr('id');
	var image = $(this).data('id');
	$.ajax({
				url: "<?php echo APP_URL; ?>superadmin/trending/removeTrending", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: {'id':id, 'image':image},
				success: function(data)   // A function to be called if request succeeds
				{
					console.log(data);
					location.reload();
				}
				});
});

  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
