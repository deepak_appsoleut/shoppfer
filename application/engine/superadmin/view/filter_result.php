<?php
$category_array1 = array();
foreach($data[2] as $a1){
	foreach($a1 as $a){
		$category_array1[] = $a;
	}
}

for($i=0; $i<count($data[0]); $i++)
{
	$value = 0;
	if($data[1][0]['totalAmount']!=0 && $category_array1[$i]['amount']!=NULL)
	{
		$value = ($category_array1[$i]['amount']/$data[1][0]['totalAmount'])*100;
	}
	if($value!=0)
	{
		$value = number_format($value, 2);
	}
 ?>
<div class="row">
<div class='col-md-5'>
<?php $imageExpolode = explode(',', $data[0][$i]['mp_gallery']); ?>
<img src='<?php echo APP_CRM_UPLOADS_PATH.$imageExpolode[0]; ?>' style="width:100%; padding:10px 0">
</div>
<div class='col-md-7'>
<h4 style='color: #123456;font-weight: bold; cursor:pointer' onclick='myClick("<?php echo $i; ?>");'><?php echo $data[0][$i]['mp_details_name'] ?></h4>
<h5>
<i class="fa fa-map-marker"></i>&nbsp; </span><?php echo $data[0][$i]['mp_details_address'] ?>,<?php echo $data[0][$i]['mp_details_city'] ?>,<?php echo $data[0][$i]['mp_details_state'] ?></h5>
<h5><i class="fa fa-road"></i>&nbsp; <strong>Distance</strong> : <?php echo number_format($data[0][$i]['mp_dist_km'], 2); ?> Km</h5>
<h5><strong>Category</strong> : <?php echo $data[0][$i]['category_name']; ?></h5>
<h5><strong>Speciality</strong> : <p><?php echo $data[0][$i]['sub_cat']; ?></p></h5>
<h5><strong>Rating</strong> : <?php echo $data[0][$i]['mp_details_rating']; ?></h5>
<h5><strong><i class="fa fa-user"></i> Manager : </strong><?php echo $data[0][$i]['mp_details_person'] ?>, <strong>Contact : </strong><?php echo $data[0][$i]['mp_details_contact']; ?></h5>
<p><strong>Description : </strong><?php echo $data[0][$i]['mp_details_description'] ?></p>
</div>
</div>
<hr style="border-top: 1px solid #9A9A9A; margin:0; margin-top:10px; margin-bottom:10px" />
<?php } ?>
<div class="row">
            <div class="col-sm-9">
                <ul class="pagination">  
                <?php
				$pageNumber = 1;
	        		if(isset($_POST['page_number']))
	        		{
						preg_match_all('!\d+!', $_POST['page_number'], $page);
						$pageNumber = $page[0][0];
	        		} 
                  $totalrecords = $data[3][0]['count']; 
                  $pg = new pagination();
                  $pg->pagenumber = $pageNumber;
                  $pg->pagesize = 10;
                  $pg->totalrecords = $totalrecords;
                  $pg->showfirst = true;
                  $pg->showlast = true;
                  $pg->paginationcss = "pagination-normal";
                  $pg->paginationstyle = 1; // 1: advance advance pagination, 0: normal pagination
                  $pg->defaultUrl = "";
                  $pg->paginationUrl = "";
                  echo $pg->process();
                ?>
                </ul> 
              </div>
              <div class="col-sm-3 viewall">
                  <a href="#" class="all-profile marBot30 marTop20 pull-right">
                    Results found <?php echo $totalrecords; ?> listings 
                    <img src="<?php echo APP_IMAGES; ?>viewAll-downArrow.jpg"  alt=""/>
                  </a>
              </div>
            </div>
<!--==========Edit lead view Modal========-->


