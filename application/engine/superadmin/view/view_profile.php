<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">View Profile</h4>
    </div>
    <div class="modal-body">
      <div class="error"></div>
      <form id="form" method="post">
      <div class="row">
      		<div class="col-md-6">
            	<label><strong>MP Name : </strong></label>
                <span><?php echo ucwords($data[0]['mp_details_name']); ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Contact : </strong></label>
                <span><?php echo $data[0]['mp_details_phone']; ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Person Name : </strong></label>
                <span><?php echo ucwords($data[0]['mp_details_person']); ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Person Contact : </strong></label>
                <span><?php echo $data[0]['mp_details_phone']; ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Website : </strong></label>
                <span><?php echo $data[0]['mp_details_website']; ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Address : </strong></label>
                <span><?php echo ucwords($data[0]['mp_details_address']).', '.ucwords($data[0]['mp_details_address1']).', '.ucwords($data[0]['mp_details_city']); ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Category : </strong></label>
                <span><?php echo $data[0]['category_name']; ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Designation : </strong></label>
                <span><?php echo $data[0]['mp_details_designation']; ?></span>
                <div class="clearfix"></div>
                <label><strong>MP Description : </strong></label>
                <span><?php echo $data[0]['mp_details_description']; ?></span>
                 <div class="clearfix"></div>
                 <label><strong>MP Opentime : </strong></label>
                <span><?php echo $data[0]['mp_opentime']; ?></span>
                 <div class="clearfix"></div>
                 <label><strong>MP Closetime : </strong></label>
                <span><?php echo $data[0]['mp_closetime']; ?></span>
                <div class="clearfix"></div>
                 <label><strong>MP Accept Card : </strong></label>
                <span><?php if($data[0]['mp_accept_card']==1) { echo "Accept Card"; } else {echo "Not Accept Card";} ?></span>
                 <div class="clearfix"></div>
                 
                 <?php if($data[0]['mp_details_category']==2) { ?>
                 <label><strong>MP Bar Available : </strong></label>
                <span><?php if($data[0]['mp_bar_available']==1) { echo "Bar Available"; } else {echo "Bar Not Available";} ?></span>
                 <div class="clearfix"></div>
                 <label><strong>MP Food Type : </strong></label>
                <span><?php if($data[0]['mp_nonveg']==1) { echo "Veg/Nonveg"; } else {echo "Veg";} ?></span>
                 <div class="clearfix"></div>
                 <?php } ?>
                 
            </div>
           <div class="col-md-6"> 
           		<div class="row">
                	<?php 
					if($data[0]['gallery']!=NULL)
					{
					$explode = explode(',', $data[0]['gallery']);
					for($i=0;$i<count($explode); $i++) { ?>
                    	<div class="col-md-6" style="margin-bottom:10px">
                        	<img width="200" height="100" src="<?php echo APP_CRM_UPLOADS_PATH.$explode[$i]; ?>" />
                        </div>
                    <?php } 
					} else {?>
                    <div class="col-md-6" style="margin-bottom:10px">
                        	<img width="200" height="100" src="<?php echo APP_CRM_UPLOADS_PATH; ?>no-thumb.jpg" />
                        </div>
                    <?php } ?>
                    
                </div>
           </div>
      </div>
       <div class="row">
       	 <div class="col-md-6">
        	<input type="hidden" name="an_id" value="<?php echo $data[0]['an_id']; ?>"  />
        </div>
       <div class="col-md-6 pull-right">  
       <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x pull-right" style="margin-right:10px;"></i></div>  
       <button class="btn-warning btn custom_warning_btn pull-right"  style="margin-left:20px" data-dismiss="modal" aria-label="Close" type="button">Cancel</button>
       <button class="btn-danger btn custom_warning_btn pull-right" name="v" value="v" type="submit">Ok</button>
      </div>
       </div>
      </form>
    </div>
    <script>
	
$('#form').bootstrapValidator({
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
		$('#loading').show();
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/all_notifications/verifySignupStatus',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				$('#loading').hide();
				if(data=="true")
				{
					$('.error').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Action is successfull!</span></div>');
					 location.reload();
				}
				else
				{
					$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span>There is some error!</span></div>');
				}
            },
            error: function (data) {
            }
   });
});
</script>