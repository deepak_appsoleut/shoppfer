<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/bootstrap-multiselect.css">
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>imagepicker/image-picker.css">
   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
#loading {
    position: absolute;
	display:none;
	top:5%;
    left: 50%;
    z-index: 999999;
}
.image_picker_image
{
	cursor:pointer;
}
.multiselect-container{max-height: 200px;overflow-y: auto;overflow-x: hidden;}
.btnCross {position: absolute;right: 15px;font-size: 12px;color: #fff;background: #454545;padding: 0px 5px;top: 0px;}
.form-group{ margin-bottom:45px !important;}
.site-min-height{ min-height:680px !important;}
.has-switch span.switch-left{background-color: #13D4A8;color: #fff;}
.has-switch span.switch-right{background-color: #FF6C5E;color: #fff;margin-top: 0px;}
.has-switch > div.switch-on label{background-color: #fff !important;}
.has-switch > div.switch-off label{background-color: #fff !important;}
.has-switch label{border: 7px solid #13D4A8;}
.has-switch{ width: 65px;height: 28px;}
.has-switch > div{ width:163%;}
.has-switch > div.switch-off label{border-color: #FF6C5E;}
.has-switch span{ font-weight:400;}
</style>
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $connect = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:40px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                      <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Connected MP</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                          <header class="panel-heading" style="border-bottom:1px solid #E2E2E4;">
                          <div class="row">
                          	<div class="col-md-2">	
                              <!--Connected MP-->
                              </div>
                              <div class="col-md-3">	
                              		<input type="text" id="tags" class="form-control" placeholder="Merchant Name" />
                              </div>
                              <div class="col-md-2">	
                              		<select id="status" class="form-control">
                                    	<option value="">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inctive</option>
                                    </select>
                              </div>
                              <div class="col-md-5 pull-right">            
    <select id="imageSelect" multiple class="image-picker show-html">
    <?php for($i=0; $i<count($data[0]); $i++) { ?>
      <option data-img-src="<?php echo APP_IMAGES.strtolower($data[0][$i]['category_name']); ?>.png" title="<?php echo $data[0][$i]['category_name']; ?>" value="<?php echo $data[0][$i]['category_id']; ?>"><?php echo $data[0][$i]['category_name']; ?></option>
	<?php } ?>
</select>  
</div>
</div>
                          </header>
                          <div class="panel-body">
                                 <section class="content">
           						 <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x" style="margin-right:10px;"></i>Loading Data....</div>							
              				<div id="myData1"></div>
    						</section>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>
</body>
<div class="modal fade" id="disableoffer" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Disable Merchant</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure about this ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger disable_offer" id="">Disable</button>
              </div>
            </div>
          </div>
        </div>
<?php include(APP_VIEW.'includes/bottom.php');?>
<script src="<?php echo APP_CRM_BS; ?>js/bootstrapValidator.min.js" type="text/javascript"></script>
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-multiselect.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>imagepicker/image-picker.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-switch.js"></script>
<script>
function getData(category, pagenumber, searchText, status)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>superadmin/connected_mp/getLocationDataHotels',
            type: 'POST',
			data: {"category" : category, "page_number" : pagenumber, "mp" : searchText, "status":status},
            success: function (data) {
					 $('#loading').hide();
					 $('#myData1').html("");
					$('#myData1').html(data);
            },
            error: function (data) {
            }
   	});
}
	$(function()
	{
		category = "";
		status = "";
		getData(category, 1, "", "");
		$('#category').multiselect();
		$('#imageSelect').change(function()
		{
			category = $(this).val();
			status = $('#status').val();
			getData(category, 1, "", status);
		});
		$("#imageSelect").imagepicker();
		
		$('#status').change(function()
		{
			status = $('#status').val();
			getData(category, 1, "", status);
		});
		
		$(document).on('click','.pagination-css',function(){
          var page_number = $(this).attr('id');
		  var status = $('#status').val();
		  getData(category, page_number, "", status);
    	});	
		
		
		<?php
		 $xy = array();
		 foreach($data[1] as $v)
		 {
			 foreach($v as $v1)
			 {
				 $xy[] = $v1;
			 }
		 }
		 $sd = array_values($xy);		 
		 ?>
		  var availableTags = <?php echo json_encode($sd); ?>;
			$( "#tags" ).autocomplete({
			  source: availableTags,
			  select: function( event, ui ) {
				  var mp = ui.item.value;
				  getData(category, 1, mp, status);
				  }
			});			
	});
	

	
	


//code for enable disable mp//////////////		
	$(document).on('click', '.inactive', function(e){
		var id	=	$(this).attr("id");
		var dis = $('#'+id+' input').attr("disabled");
		if(dis=="disabled")
		{
			return false;
		}
		else
		{
			$(".disable_offer").attr('id',id);
			jQuery("#disableoffer").modal('show');
		}
	});
	
	
	$(document).on('click', '.disable_offer', function(e){
		var mp_id	=	$(this).attr("id");
		$.ajax({
				url: '<?php echo APP_URL; ?>superadmin/connected_mp/disable_mp',
				 type: 'POST',
				 data: {'mp_id':mp_id},
				 success: function (data) {
					 location.reload();
				 }
		});
	});
	
	var global_mp_id	=	'';
	$(document).on('click', '.active1', function(e){
		var mp_id	=	$(this).attr("id");
		var dis = $('#'+mp_id+' input').attr("disabled");
		if(dis=="disabled")
		{
			return false;
		}
		else
		{
			global_mp_id	=	mp_id;
			$.ajax({
					 url: '<?php echo APP_URL; ?>superadmin/connected_mp/active_inactive_mp',
					 type: 'POST',
					 data: {'mp_id':mp_id},
					 success: function (data) {
						if(data	==	1){
							 location.reload();
						}
					 }
			});
		}
	});
	
	
	//On modal close Reload the Page
	$('#disableoffer').on('hidden.bs.modal', function () {
 		location.reload();
	});
	
// ends enable and disbale mp	
</script>
</html>
