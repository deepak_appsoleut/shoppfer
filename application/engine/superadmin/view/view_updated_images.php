<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Superadmin Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<style>
	.progress 
	{
		margin-top:5px;
	}
	.progress .percent
	{
		color:#fff;
		padding-left:10px;
	}
	.category, .thumb, .details
	{
		float:left;
		width:80%;
	}
	#spinThm, #spinCat
	{
		position:absolute;
		top:45%;
		left:35%;
		display:none;
		color:#000;
	}
	.fa-spinner
	{
		position:absolute;
		top:35%;
		left:45%;
		color:#000;
		display:none;
	}
</style>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $images = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
         <section class="wrapper site-min-height" style="margin-top:40px"> 
      <!-- page start-->
      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Upload Images</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
            <div class="panel-body">
              <form id="form">
                <div class="row">
                  <div class="col-md-12">
                    <h4>Media Images</h4>
                  </div>
                          <?php 
						  $imageName = '';
						  for($i=0; $i<count($data); $i++) { 
						  $explode = explode('/', $data[$i]["deal_image_media"]);
						  $imageName = $imageName.','.str_replace('.png', '.jpg', $explode[1]); 
						  $imageName = ltrim($imageName, ',');
						  ?> 
                          <div class="col-md-3" style="width:20%">
                          <a href="<?php echo APP_CRM_UPLOADS_PATH.$data[$i]["deal_image_media"]; ?>" target="_blank"><img width="200" height="150" style="margin-right:15px;border:1px solid #ccc" src="<?php echo APP_CRM_UPLOADS_PATH.$data[$i]["deal_image_media"]; ?>"></a>
                          </div>
                          <?php } 
						  ?>
                          </div>
                          <hr style="border-top: 1px solid #A28D8D;">
                          <div class="row">
                          <div class="col-md-3">
                          <h4>Thumb Image</h4>
                          <?php if($data[0]["deal_image_thumb"]==NULL) { 
						  $data[0]["image_thumb"] = "no-thumb.jpg";
						   } ?>
                          <img width="200" id="thumbImg" style="border:1px solid #ccc;margin-bottom:10px" height="150" src="<?php echo APP_CRM_UPLOADS_PATH.$data[0]["deal_image_thumb"]; ?>">
                          <div style="clear:both"></div>
                          <input class="thumb" type="file" class="file" name="thumb">
                          <input  type="hidden" id="offer" name="thumb" value="<?php echo $data[0]["offer_id"] ?>">
                          <i class="fa fa-spinner fa-pulse fa-3x" id="spinThm"></i>
                           <div style="clear:both"></div>
                          <div class="progress" id="progressThm" style="display:none">
                                <div class="percent" id="percentThm">0%</div >
                            </div>
                          </div>
                          <div class="col-md-3">
                          <h4>Category Image</h4>
                          <?php if($data[0]["deal_image_category"]==NULL) { 
						  $data[0]["deal_image_category"] = "no-thumb.jpg";
						   } ?>
                         <img width="200" id="catImg" style="border:1px solid #ccc; margin-bottom:10px" height="150" src="<?php echo APP_CRM_UPLOADS_PATH.$data[0]["deal_image_category"]; ?>">
                          <div style="clear:both"></div>
                          <input class="category" type="file" class="file" name="category">
                          <i class="fa fa-spinner fa-pulse fa-3x" id="spinCat"></i>
                           <div style="clear:both"></div>
                          	<div class="progress" id="progressCat" style="display:none">
                                <div class="percent" id="percentCat">0%</div >
                            </div>                          
                          </div>
                          </div>
                          <hr style="border-top: 1px solid #A28D8D;">
                          <div class="row">
                         <div class="col-md-12">
                          <h4>Details Images</h4>
                          </div>
                          <?php for($i=0; $i<count($data); $i++) { 
						   if($data[$i]["deal_image_details"]==NULL) { 
						  		$data[$i]["deal_image_details"] = "no-thumb.jpg";
						   } 
						   if(!file_exists(APP_CRM_UPLOADS.$data[$i]["deal_image_details"]))
						   {
							   $data[$i]["deal_image_details"] = "no-thumb.jpg";
						   }
						  ?> 
                          <div class="col-md-3" style="width:20%">
                         <img style="border:1px solid #ccc;margin-bottom:10px" width="200" height="150" id="img<?php echo $data[$i]["deal_gallery_id"]; ?>" src="<?php echo APP_CRM_UPLOADS_PATH.$data[$i]["deal_image_details"]; ?>">
                          <div style="clear:both"></div>
                         <input type="file" class="details" name="det<?php echo $data[$i]["deal_gallery_id"]; ?>" id="det<?php echo $data[$i]["deal_gallery_id"]; ?>">
                         <i class="fa fa-spinner fa-pulse fa-3x" id="spin<?php echo $data[$i]["deal_gallery_id"]; ?>"></i>
                          <div style="clear:both"></div>
                         <div class="progress" id="progress<?php echo $data[$i]["deal_gallery_id"]; ?>" style="display:none">
                                <div class="percent" id="percent<?php echo $data[$i]["deal_gallery_id"]; ?>">0%</div >
                            </div>
                          </div>
                         <?php } ?>
                          </div>
                          <hr style="border-top: 1px solid #A28D8D;">
                                <div class="row">
                                	<div class="col-md-12">
                                    	<button type="submit" class="btn btn-danger pull-right">Upload &amp; Submit</button>
                                    </div>
                                </div>
 							</form>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>
</body>

<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
<script>
$(function() 
{
	$("input:file").change(function() 
	{
		$('.progress').hide();
		var className = $(this).attr('class');
		var offer_id = $('#offer').val();
		var string = '<?php echo $imageName; ?>';
		var array = string.split(',');
		if(className =="category")
		{
			var file = this.files[0];
			var imagefile = file.type;
			var fileName = file.name;
			//var mydata = $.inArray(fileName, array);
			var match= ["image/jpeg","image/jpg"];
			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
			{
				alert("Please select a proper format - jpg");				
				$('.category').val("");
				return false;
			}
			
			/*else if(mydata==-1)
			{
				alert("Please rename file!");
				$('.category').val("");
				return false;
			}*/
			else
			{
			var reader = new FileReader();
			reader.readAsDataURL(this.files[0]);
			reader.onload = function (e) {
					var image  = new Image();
					image.src = e.target.result;
					image.onload = function () {
						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;
						if(width!=769&&height!=300)
						{
							alert("Please upload correct size");
							$('.category').val("");
							return false;	
						}
						else
						{
								$('#spinCat').show();
								var form1 = $('#form')[0];
								var formData = new FormData(form1);
								formData.append("offer_id", offer_id);
								formData.append("fileType", className);
								
								$('input[type="file"]').each(function() {
									$(this).attr('disabled', 'disabled');
								});
			
								$.ajax({
											xhr: function() {
											var xhr = new window.XMLHttpRequest();
											xhr.upload.addEventListener("progress", function(evt) {
											  if (evt.lengthComputable) {
												var percentComplete = evt.loaded / evt.total;
												percentComplete = parseInt(percentComplete * 100);
												$('#progressCat').show();
												$('#progressCat').css('background','#345678');
												$('#progressCat').css('width',percentComplete+'%');
												$('#percentCat').html(percentComplete+'%');
												console.log(percentComplete);
												if (percentComplete === 100) {
										
												}
										
											  }
											}, false);
											return xhr;
										  },
									
										url: '<?php echo APP_URL; ?>superadmin/image_upload/upload_process',
										type: 'POST',
										data: formData,
										processData: false,
										contentType: false,
										success: function (data) {
												console.log(data);
											if(data=="true")
											{
												$('#spinCat').hide();
												$('#catImg').attr('src', e.target.result);
												$('#catImg').attr('height', '150px');
												$('input[type="file"]').each(function() {
													$(this).removeAttr('disabled');
												});
											}
										},
										error: function (data) {
										}
							   });	
							
						}
					}
				}
			}
		}
		else if(className=="thumb")
		{
			var file = this.files[0];
			var imagefile = file.type;
			var fileName = file.name;
			//var mydata = $.inArray(fileName, array);
			var match= ["image/jpeg","image/jpg"];
			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
			{
				alert("Please select a proper format - jpg");
				$('.thumb').val("");
				return false;
			}
			
			/*else if(mydata==-1)
			{
				alert("Please rename file!");
				$('.thumb').val("");
				return false;
			}*/
			else
			{
			var reader = new FileReader();
			reader.readAsDataURL(this.files[0]);
			reader.onload = function (e) {
					var image  = new Image();
					image.src = e.target.result;
					image.onload = function () {
						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;
						if(width!=250&&height!=203)
						{
							alert("Please upload correct size");
							$('.thumb').val("");
							return false;	
						}
						else
						{
								$('#spinThm').show();
								var form1 = $('#form')[0];
								var formData = new FormData(form1);
								formData.append("offer_id", offer_id);
								formData.append("fileType", className);
								$('input[type="file"]').each(function() {
									$(this).attr('disabled', 'disabled');
								});
								$.ajax({
										xhr: function() {
											var xhr = new window.XMLHttpRequest();
											xhr.upload.addEventListener("progress", function(evt) {
											  if (evt.lengthComputable) {
												var percentComplete = evt.loaded / evt.total;
												percentComplete = parseInt(percentComplete * 100);
												$('#progressThm').show();
												$('#progressThm').css('background','#345678');
												$('#progressThm').css('width',percentComplete+'%');
												$('#percentThm').html(percentComplete+'%');
												console.log(percentComplete);
												if (percentComplete === 100) {
												}
										
											  }
											}, false);
										
											return xhr;
										  },
									
										url: '<?php echo APP_URL; ?>superadmin/image_upload/upload_process',
										type: 'POST',
										data: formData,
										processData: false,
										contentType: false,
										success: function (data) {
											console.log(data);
											if(data=="true")
											{
												$('#spinThm').hide();
												$('#thumbImg').attr('src', e.target.result);
												$('#thumbImg').attr('height', '150px');
												$('input[type="file"]').each(function() {
													$(this).removeAttr('disabled');
												});
											}
										},
										error: function (data) {
										}
							   });	
							
						}
					}
				}
			}
		}
		else if(className=="details")
		{
			var id = $(this).attr('id');
			removedStr = id.substr(3);
			var file = this.files[0];
			var imagefile = file.type;
			var fileName = file.name;
			//var mydata = $.inArray(fileName, array);
			var match= ["image/jpeg","image/jpg"];
			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
			{
				alert("Please select a proper format - jpg");
				$('#det'+removedStr).val("");
				return false;
			}
			
			/*else if(mydata==-1)
			{
				alert("Please rename file!");
				$('#det'+removedStr).val("");
				return false;
			}*/
			else
			{
			var reader = new FileReader();
			reader.readAsDataURL(this.files[0]);
			reader.onload = function (e) {
					var image  = new Image();
					image.src = e.target.result;
					image.onload = function () {
						//Determine the Height and Width.
						var height = this.height;
						var width = this.width;
						if(width!=800&&height!=533)
						{
							alert("Please upload correct size");
							$('#det'+removedStr).val("");
							return false;	
						}
						else
						{
								$('#spin'+removedStr).show();
								var form1 = $('#form')[0];
								var formData = new FormData(form1);
								formData.append("offer_gallery_id", removedStr);
								formData.append("fileType", className);
								$('input[type="file"]').each(function() {
									$(this).attr('disabled', 'disabled');
								});
								$.ajax({
										
										xhr: function() {
											var xhr = new window.XMLHttpRequest();
											xhr.upload.addEventListener("progress", function(evt) {
											  if (evt.lengthComputable) {
												var percentComplete = evt.loaded / evt.total;
												percentComplete = parseInt(percentComplete * 100);
												$('#progress'+removedStr).show();
												$('#progress'+removedStr).css('background','#345678');
												$('#progress'+removedStr).css('width',percentComplete+'%');
												$('#percent'+removedStr).html(percentComplete+'%');
												console.log(percentComplete);
												if (percentComplete === 100) {
										
												}
										
											  }
											}, false);
										
											return xhr;
										  },
									
										url: '<?php echo APP_URL; ?>superadmin/image_upload/upload_process',
										type: 'POST',
										data: formData,
										processData: false,
										contentType: false,
										success: function (data) {
											console.log(data);
											if(data=="true")
											{
												$('#spin'+removedStr).hide();
												$('#img'+removedStr).attr('src', e.target.result);
												$('#img'+removedStr).attr('height', '150px');
												$('input[type="file"]').each(function() {
													$(this).removeAttr('disabled');
												});
											}
										},
										error: function (data) {
										}
							   });	
							
						}
					}
				}
			}
		}
	});

$('#form').submit(function()
{
	var fileName = '<?php echo APP_CRM_UPLOADS_PATH.'no-thumb.jpg' ?>';
	var images = $('#form').find('img').map(function() { return this.src; }).get();
	var mydata = $.inArray(fileName, images);
	var offer_id = $('#offer').val();
	if(mydata!=-1)
	{
		alert("Please upload all files!");
		return false;
	}
	else
	{
	  $.ajax({
				url: '<?php echo APP_URL; ?>superadmin/image_upload/upload_status',
				type: 'POST',
				data: {'offer_id':offer_id},
				success: function (data) {
						console.log(data);
					if(data=="true")
					{
						alert('Image uploaded successfully!');
						location.reload();
					}
				},
				error: function (data) {
				}
		 });	
		 return false;
	}
});
	
});
</script>
</html>
