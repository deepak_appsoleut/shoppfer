<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
   <style>
  .state-overview .value{ float:left;}
  .p1{font-size: 15px;color: #6B6B6D;font-weight: 500;text-align: left;}
  .p2{font-size: 40px;color: #5AD0B6 !important;font-weight: bold;text-align: left;}
  .state-overview .terques {background: #F3F3F3;}
  .state-overview .value {padding-top: 0px; }
  </style>

</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $connect = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:45px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">					
                      <section class="panel">
                      <header class="panel-heading" style="border-bottom:1px solid #E2E2E4; padding:0">
                          <div class="row">
                          	<div class="col-md-12">	
                            	<a href="<?php echo APP_URL; ?>superadmin/connected_mp/merchant_view/<?php echo $data[2]; ?>" class="btn btn-primary" style="border-radius:0; background-color:#e51429; border-color:#fff">Profile Details</a><a href="<?php echo APP_URL; ?>superadmin/connected_mp/deal_view/<?php echo $data[2]; ?>" class="btn btn-primary" style="border-radius:0; background-color:#e51429; border-color:#fff">Deal Details</a><a href="<?php echo APP_URL; ?>superadmin/connected_mp/revenue/<?php echo $data[2]; ?>" class="btn btn-primary" style="border-radius:0; background-color:#980316; border-color:#fff">Revenue Details</a>
                              </div>
</div>
                          </header>
                      
                      		<div id="message"></div>
                          	<header class="panel-heading">
                              <!--<strong>Dashboard</strong>-->
                              <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Dashboard</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                              <span class="pull-right">
                              <!--<strong>Manage Deals</strong>-->
                              <?php $coupon_sold_array  = array();
$coupond_redeemed_array = array();
$coupond_redeemed = array();
for($i=0;$i<count($data[0]);$i++)
{
 if(($data[0][$i]["booking_status"] == "0") || ($data[0][$i]["booking_status"] == "1"))
 {
  array_push($coupon_sold_array,"1");
 }
 if($data[0][$i]["booking_status"] == "1")
 {
  array_push($coupond_redeemed,"1");
 }
}
$COUPONS_SOLD  = count($coupon_sold_array);
$COUPONS_REDEEMD = count($coupond_redeemed_array);
$HAPPY_CUSTOMER  = $data[1][0]["avg_cnt"];
?>
                         		</header>
                          	<div class="panel-body">
                          		<div class="row state-overview">
                          <div class="col-lg-3 col-sm-6">
                              <section class="panel" style="background-color:#fff">
                                  <div class="value">
                                      <p class="p1">COUPONS SOLD</p>
                                      <p class="p2"><?php echo $COUPONS_SOLD; ?></p>
                                  </div>
                                  <div class="symbol terques">
                                      <img src="<?php echo APP_IMAGES; ?>couponsold.png">
                                  </div>
                              </section>
                          </div>
                          <div class="col-lg-1"></div>
                          <div class="col-lg-3 col-sm-6">
                              <section class="panel" style="background-color:#fff">
                                  <div class="value">
                                      <p class="p1">COUPONS REDEEMD</p>
                                      <p class="p2"><?php echo $COUPONS_REDEEMD; ?></p>
                                  </div>
                                  <div class="symbol terques">
                                      <img src="<?php echo APP_IMAGES; ?>couponreedem.png">
                                  </div>
                              </section>
                          </div>
                          <div class="col-lg-1"></div>
                          <div class="col-lg-3 col-sm-6">
                              <section class="panel" style="background-color:#fff">
                                  <div class="value">
                                      <p class="p1">HAPPY CUSTOMERS</p>
                                      <p class="p2"><?php echo $HAPPY_CUSTOMER; ?></p>
                                  </div>
                                  <div class="symbol terques">
                                      <img src="<?php echo APP_IMAGES; ?>happyCustomer.png">
                                  </div>
                              </section>
                          </div>
                          <div class="col-lg-1"></div>
                      		</div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      
    <!-- Modal Dialog -->
        
        
         <!-- Modal Dialog -->
</section>
<!--MODEL CODE HERE START HERE-->
		
<!--MODEL CODE HERE START HERE-->
</body>
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->

</html>
