<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Shoppfer | Log in</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/bootstrap.min.css">

    <link href="<?php echo APP_CRM_BS; ?>css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <!-- Custom styles for this template -->
    <link href="<?php echo APP_CRM_BS; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo APP_CRM_BS; ?>css/style-responsive.css" rel="stylesheet" />
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="login-body">

    <div class="container">
      <form class="form-signin" id="form">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
        <div id="error"></div>
            <input type="text" name="email" class="form-control" id="email" placeholder="User ID" autofocus>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>
                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
        </div>

          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="email1" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" type="button">Submit</button>
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->

      </form>

    </div>

  </body>
<script src="<?php echo APP_CRM_BS; ?>js/jquery.js"></script> 
<!-- Bootstrap 3.3.5 --> 
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap.min.js"></script> 
<script>
$(function()
{
	$('#form').submit(function()
	{
		var email = $('#email').val();
		var password = $('#password').val();
		if(email=="")
		{
			$('#error').html('<div class="alert alert-danger alert-dismissable" style="padding: 10px 35px 10px 10px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span>Please enter email id!</span></div>');
			return false;
		}
		else if(email!='')
		{
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var result = regex.test(email);
			if(result==false)
			{
				$('#error').html('<div class="alert alert-danger alert-dismissable" style="padding: 10px 35px 10px 10px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span>Please enter correct email id!</span></div>');
				return false;
			}
			else if(password=="")
			{
				$('#error').html('<div class="alert alert-danger alert-dismissable" style="padding: 10px 35px 10px 10px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span>Please enter password!</span></div>');
				return false;
			}
			else
			{
				var formData = new FormData( this );
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/login/process',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) 
			    {
					console.log(data);
					if(data==0)
					{
						$('#error').html('<div class="alert alert-danger alert-dismissable" style="padding: 10px 35px 10px 10px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span>Please enter correct username or password!</span></div>');
						return false;
					}
					else
					{
						top.location = "<?php echo APP_URL; ?>superadmin/dashboard";
					}
				},
				error: function (data) {
				}
   				});
				return false;
			}
		}
	})
});
</script>
</html>
