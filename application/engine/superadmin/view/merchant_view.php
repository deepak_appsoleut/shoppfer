<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
   <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
 <link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/bootstrap-multiselect.css">
   <link rel="stylesheet" type="text/css" href="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <!--<link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/timepicker.css" />-->
  
   <style>
.multiselect-container{max-height: 200px;overflow-y: auto;overflow-x: hidden;}
.btnCross {position: absolute;right: 15px;font-size: 12px;color: #fff;background: #454545;padding: 0px 5px;top: 0px;}
.form-group{ margin-bottom:45px !important;}
.site-min-height{ min-height:680px !important;}
.has-switch span.switch-left{background-color: #13D4A8;color: #fff;}
.has-switch span.switch-right{background-color: #FF6C5E;color: #fff;margin-top: 0px;}
.has-switch > div.switch-on label{background-color: #fff !important;}
.has-switch > div.switch-off label{background-color: #fff !important;}
.has-switch label{border: 7px solid #13D4A8;}
.has-switch{ width: 65px;height: 28px;}
.has-switch > div{ width:163%;}
.has-switch > div.switch-off label{border-color: #FF6C5E;}
.has-switch span{ font-weight:400;}
</style>
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $connect = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:45px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading" style="border-bottom:1px solid #E2E2E4; padding:0">
                          <div class="row">
                          	<div class="col-md-12">	
                            	<a href="<?php echo APP_URL; ?>superadmin/connected_mp/merchant_view/<?php echo $data[0][0]['mp_details_id']; ?>" class="btn btn-primary" style="border-radius:0; background-color:#980316; border-color:#fff">Profile Details</a><a href="<?php echo APP_URL; ?>superadmin/connected_mp/deal_view/<?php echo $data[0][0]['mp_details_id']; ?>" class="btn btn-primary" style="border-radius:0; background-color:#e51429; border-color:#fff">Deal Details</a><a href="<?php echo APP_URL; ?>superadmin/connected_mp/revenue/<?php echo $data[0][0]['mp_details_id']; ?>" class="btn btn-primary" style="border-radius:0; background-color:#e51429; border-color:#fff">Revenue Details</a>
                              </div>
</div>
                          </header>
                          <div class="panel-body">
                                <section class="content">
                                	<div class="row" style="">
								<div class="col-xs-12">
							   <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Success : </b> <span></span></div>
									<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<b>Warning : </b> <span></span></div>
								<div class="box1 box-warning">
								<div class="box-header with-border">
								  <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>BUSINESS INFORMATION</strong></h3>
                                  <div style="height: 5px;width: 60px;margin-bottom: 30px;background: #5AD0B6;"></div>
								</div>
								<form id="form">
								<div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Business Name</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="business" id="business" value="<?php echo $data['0']['0']['mp_details_name']; ?>" placeholder="Business Name" class="form-control input-sm" type="text">
                                        <input type="hidden" value="<?php echo $data['0']['0']['mp_details_id']; ?>" name="mp_id">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Business Contact Number</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="phone" id="phone" value="<?php echo $data['0']['0']['mp_details_contact']; ?>" placeholder="Business Contact Number" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                 <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">City</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="city" id="city" value="<?php echo  $data['0']['0']['mp_details_city']; ?>" placeholder="City" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Address</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="address" id="address" value="<?php echo $data['0']['0']['mp_details_address']; ?>" placeholder="Address" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Web URL</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="web_url" id="web_url" value="<?php echo $data['0']['0']['mp_details_website']; ?>" placeholder="WEb URL eg : http:www.shoppfer.com" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Contact Person's Name</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="person" id="person" value="<?php echo $data['0']['0']['mp_details_person']; ?>" placeholder="person" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="marTop10"></div>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Contact Person Number</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="contact" id="contact" value="<?php echo $data['0']['0']['mp_details_phone']; ?>" placeholder="Contact Number" class="form-control input-sm" type="text" maxlength="13">
                                      </div>
                                    </div>
                                  </div>
								</div>
                                <div class="marTop10"></div>
                                <div class="row">
                                
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <div class="col-md-3">
                                        <label class="pull-left">Designation</label>
                                        </div>
                                        <div class="col-md-9">
                                        <input name="designation" value="<?php echo $data['0']['0']['mp_details_designation']; ?>" id="designation" placeholder="person" class="form-control input-sm" type="text">
                                      </div>
                                    </div>
                                  </div>
                                  
                                </div>
										<div class="marTop10"></div>
										<div class="box-header with-border" style="MARGIN-TOP: 25PX;">
									 <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>BUSINESS DETAILS</strong></h3>
                                  <div style="height: 5px;width: 60px;margin-bottom: 30px;background: #5AD0B6;"></div>
									</div>
										<div class="marTop10"></div>
										<div class="row">
										  <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
												<label class="pull-left">Category</label>
												</div>
												<div class="col-md-9">
												<select name="category" id="category" class="form-control input-sm">
												<option value="">--Select Category--</option>
												<?php for($i=0; $i<count($data[1]); $i++) { 
														if($data[1][$i]['category_id']==$data['0']['0']['mp_details_category'])
														{
															?>
															<option selected value="<?php echo $data[1][$i]['category_id']; ?>"><?php echo $data[1][$i]['category_name']; ?></option>
															<?php
														}
														else
														{
															?>
															<option value="<?php echo $data[1][$i]['category_id']; ?>"><?php echo $data[1][$i]['category_name']; ?></option>
															<?php	
														}
													?>
													
													<?php } ?>
												</select>
											  </div>
											</div>
										  </div>
										</div>
										<div class="marTop10"></div>
										 <div class="row">
                                         <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
												<label class="pull-left">Recommended</label>
												</div>
												<div class="col-md-9">
												 <?php 
												$explode[0] = $explode[1] = $explode[2] = "";
												$explode = explode(',', $data['0']['0']['mp_details_recommend']); 
												if(count($explode)==0)
												{
													$explode[0] = $explode[1] = $explode[2] = "";
												}
												else if(count($explode)==1)
												{
													$explode[1] = $explode[2] = "";
												}
												else if(count($explode)==2)
												{
													$explode[2] = "";
												}
												?>
												<input name="recommend[]" id="recommend1" value="<?php echo $explode[0]; ?>" placeholder="Recommended" class="form-control input-sm" type="text">
											  </div>
											</div>
										  </div>
										  
										  
										</div>
										<div class="marTop10"></div>
										 <div class="row">
										  <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
												<label class="pull-left">Business Description</label>
												</div>
												<div class="col-md-9">
												<textarea rows="3" name="description" id="description" placeholder="Description" class="form-control input-sm" type="text"><?php echo $data['0']['0']['mp_details_description']; ?></textarea>
											  </div>
											</div>
										  </div>
										</div>
                                        <input type="hidden" id="mp_details_id" value="<?php echo $data['0']['0']['mp_details_id']; ?>">
										<div class="marTop10"></div>
                                        <div class="marTop10" style="margin-bottom: 20px;"></div>
										 <div class="row">
										  <div class="col-md-12">
											<div class="form-group">
											  <div class="col-md-3">
												<label class="pull-left">Business Images</label>
												</div>
												<div class="col-md-9">
												<?php 
												$cnt=1;
												for($i=0; $i<4; $i++){
												$cnt = $i+2; 
												$j = $i+1;
												?>
													<div class="col-md-3" style="width:auto;padding-left: 0px;">
														<div class="form-group">
															<button type="button" id="image<?php echo $j; ?>btn" class="btn btn-box-tool btnCross" data-widget="remove" style=" <?php if(isset($data[3][$i]['mp_gallery_image'])){ }else{ echo "display:none"; } ?> ">

                                                            <i class="fa fa-times"></i>
															</button>
															<label for="image<?php echo $j; ?>">
																<img id="image<?php echo $j; ?>pic" style="cursor:pointer;" height="100px" width="130px" src="<?php if(isset($data[3][$i]['mp_gallery_image'])){echo APP_CRM_UPLOADS_PATH.$data[3][$i]['mp_gallery_image']; }else{ echo APP_CRM_UPLOADS_PATH.'add_image.jpg'; } ?>"/>
															</label>
															<input name="image<?php echo $j; ?>" id="image<?php echo $j; ?>" style="display:none" class="form-control input-sm" type="file">
															<!--<i class="fa fa-spinner fa-pulse" style="display:none;position: absolute;top: 60px; left: 100px;" id="image<?php echo $j; ?>Lod"></i>-->
                                                            <div style="position: absolute;top: 40px;left: 50px;color: red;display: none;font-weight: bold;" id="image<?php echo $j; ?>Lod"></div>
															<input type="hidden" class="imageUrl" name="image<?php echo $j; ?>Url" id="image<?php echo $j; ?>Url" value="<?php if(isset($data[3][$i]['mp_gallery_image'])){echo APP_CRM_UPLOADS_PATH.$data[3][$i]['mp_gallery_image']; }else{  } ?>" />
                                                            <input type="hidden" class="mp_gallery_id" name="mp_gallery_id_image<?php echo $j; ?>" id="mp_gallery_id_image<?php echo $j; ?>" value="<?php if(isset($data[3][$i]['mp_gallery_image'])){echo $data[3][$i]['mp_gallery_id']; }else{  } ?>" />
															<input type="hidden" name="image<?php echo $j; ?>Id" id="image<?php echo $j; ?>Id" value="" />
														</div>
													</div>
												<?php } ?>
											  </div>
											</div>
										  </div>
										</div>
                                        <div class="marTop10"></div>
                                        <div class="box-header with-border">
									 <h3 class="box-title" style="margin-top: 0px;margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px; font-size:16PX;"><strong>GENRAL INFORMATION</strong></h3>
                                  <div style="height: 5px;width: 60px;margin-bottom: 30px;background: #5AD0B6;"></div>
									</div>
                                     <div class="marTop10"></div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                               <div class="form-group" style="margin-top:10px;">
                                              <div class="col-md-3">
                                                <label class="pull-left">Timings</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="col-md-3" style="margin-left: -13px;">
                                                   <div class="input-group bootstrap-timepicker">
                                                            <!--<input id="opentime" type="text" class="time form-control" name="opentime" value="" style="width: 110px;padding: 5px;" placeholder="Opening Time"/>-->
                                                    <input type="text" class="form-control timepicker-default" id="opentime" name="opentime">
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                                    </span>     
                                                   </div>
                                                    </div>
                                               
                                                     <div style="float: left;margin-right: 45px;margin-top: 9px;font-size: 12px;">
                                                    <!--<img src="<?php echo APP_IMAGES.'Open_close_time.jpg' ?>" style="height: 34px;"/>-->
                                                    To
                                                    </div>
                                                     <div class="col-md-3" style=" padding-left:0px;margin-left: -36px;">
                                                    <div class="input-group bootstrap-timepicker">
                                                           <!-- <input id="closetime" type="text" class="time form-control" name="closetime" value="" style="width: 110px;padding: 5px;" placeholder="Closing Time"/>-->
                                                    <input type="text" class="form-control timepicker-default" id="closetime" name="closetime">
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>       
                                                      </div>
                                                     </div>
                                                  </div>
                                                <!--<div class="col-md-9">
                                                    <div class="col-md-3" style="margin-left: -13px;">
                                                   <div class="input-group bootstrap-timepicker">
                                                            <input id="opentime" type="text" class="time form-control" name="opentime" value="" style="width: 110px;padding: 5px;" placeholder="Opening Time"/>
                                                      </div>
                                                    </div>
                                               
                                                    <div style="float: left;margin-left: -70px;">
                                                    <img src="<?php //echo APP_IMAGES.'Open_close_time.jpg' ?>" style="height: 34px;"/>
                                                    </div>
                                                     <div class="col-md-3" style=" padding-left:0px;margin-left: -36px;">
                                                    <div class="input-group bootstrap-timepicker">
                                                            <input id="closetime" type="text" class="time form-control" name="closetime" value="" style="width: 110px;padding: 5px;" placeholder="Closing Time"/>
                                                      </div>
                                                     </div>
                                                  </div>-->
                                                </div>
                                            </div>
                                            
                                              <div class="col-md-12" style="margin-top:10px;" id="a_c">
                                               <div class="form-group">
                                                  <div class="col-md-3">
                                                    <label class="pull-left">Accept Card</label>
                                                    </div>
                                                    <div class="col-md-9" style="padding-left: 10px;">
                                                        <div class="" style="">
                                                            <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="ac">
                                                            <input type="checkbox" data-toggle="switch" <?php if($data['0']['0']['mp_accept_card']==1){echo 'checked';  }else{ } ?>/>
                                                            <input type="hidden" id="accept_card" name="accept_card" value="<?php echo $data['0']['0']['mp_accept_card']; ?>" />
                                                            </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if($data['0']['0']['mp_details_category']==2) { ?>
                                            <div class="col-md-12" style="margin-top:10px;" id="n_v">
                                               <div class="form-group">
                                                  <div class="col-md-3">
                                                    <label class="pull-left">Serves Non-veg</label>
                                                    </div>
                                                    <div class="col-md-9" style="padding-left: 10px;">
                                                        <div class="" style="">
                                                            <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="nv">
                                                            <input type="checkbox" data-toggle="switch" <?php if($data['0']['0']['mp_nonveg']==1){echo 'checked';  }else{ } ?> />
                                                            <input type="hidden" id="veg_nonveg" name="veg_nonveg" value="<?php echo $data['0']['0']['mp_nonveg'] ?>" />
                                                            </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                       						<div class="col-md-12" style="margin-top:10px;" id="b_a">
                                               <div class="form-group">
                                                    <div class="col-md-3">
                                                    <label class="pull-left">Bar Available</label>
                                                    </div>
                                                    <div class="col-md-9" style="padding-left: 10px;">
                                                        <div class="" style="">
                                                            <div style="width: 83px;margin: 0 auto;float: left;" class="inactive" id="bar">
                                                            <input type="checkbox" data-toggle="switch" <?php if($data['0']['0']['mp_bar_available']==1){echo 'checked';  }else{ } ?> />
                                                            <input type="hidden" id="bar_available" name="bar_available" value="<?php echo $data['0']['0']['mp_bar_available'] ?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
									
                                    
                                    <input type="hidden" id="mp_details_id" value="<?php echo $data['0']['0']['mp_details_id']; ?>">
									<div class="marTop10"></div>
									<div class="row"> 
										<div class="col-md-2" style="float: right;">
                                        	<div class="">
                                                <button class="btn btn-shadow btn-success pull-right" type="submit" style="margin-top: 25px;padding: 8px 40px; box-shadow:none !important;">Save</button>
                                            </div>
										</div>   
									</div>
									</form>
                                    
								</div>
								</div>
							  </div>
    						</section>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>
</body>
<?php include(APP_VIEW.'includes/bottom.php');?>
<script src="<?php echo APP_CRM_BS; ?>js/bootstrapValidator.min.js" type="text/javascript"></script>
<script src="<?php echo APP_CRM_BS; ?>dist/js/app.min.js" type="text/javascript"></script>

<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-multiselect.js"></script>
<!--<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-timepicker.js"></script>-->
<!--<script type="text/javascript" src="<?php //echo APP_CRM_BS; ?>js/jquery.timepicker.js"></script>-->
<script type="text/javascript" src="http://thevectorlab.net/flatlab/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo APP_CRM_BS; ?>js/bootstrap-switch.js"></script>
<script>
$(function()
{
	$("#category").change(function()
	{
		var id	=	$(this).val();
		if(id	==	2){
			$('#a_c').show();
			$('#n_v').show();
			$('#b_a').show();
		}else{
			$('#a_c').show();
			$('#n_v').hide();
			$('#b_a').hide();
		}
	});
	
	<!--$('.timepicker-default').timepicker();-->
	$('#opentime').timepicker();
	$('#closetime').timepicker();
	$("#opentime").val('<?php echo $data[0][0]['mp_opentime']; ?>');
	$("#closetime").val('<?php echo $data[0][0]['mp_closetime']; ?>');
	$("[data-toggle='switch']").wrap('<div class="switch"/>').parent().bootstrapSwitch();
	$("#ac").click(function(){
		var id	=	$("#accept_card").val();
		if(id	==	1)
		{
			$("#accept_card").val(0);
		}else{
			$("#accept_card").val(1);
		}
	});
	$("#nv").click(function(){
		var id	=	$("#veg_nonveg").val();
		if(id	==	1)
		{
			$("#veg_nonveg").val(0);
		}else{
			$("#veg_nonveg").val(1);
		}
	});
	$("#bar").click(function(){
		var id	=	$("#bar_available").val();
		if(id	==	1)
		{
			$("#bar_available").val(0);
		}else{
			$("#bar_available").val(1);
		}
	});
	
	$('.btnCross').click(function()
	{
		var id = $(this).attr('id');
		id = id.slice(0, -3);
		//$(this).hide();
		var mp_gallery_id_image	=	$("#mp_gallery_id_"+id).val();
		$.ajax({
				url: '<?php echo APP_URL; ?>superadmin/connected_mp/deletePic',
				type: 'POST',
				data: {'mp_gallery_id_image' : mp_gallery_id_image},
				success: function (data) {
					console.log(data);					
					$('#'+id+'pic').attr('src', "<?php echo APP_IMAGES; ?>add_image.jpg");
					$('#'+id).val('');
					$('#'+id+'Id').val('');
					$('#'+id+'Lod').hide();
					$('#'+id+'Url').val('');
					$('#'+id+'btn').hide();
					
				},
				error: function (data) {
				}
	   });	
	});
	
					
	
	
$('#form').bootstrapValidator({
        fields: {
			business: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Business Name'
                    }
                }
            },
			city: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter City'
                    }
                }
            },
			address: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Address'
                    }
                }
            },
			category: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Category'
                    }
                }
            },
			person: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Person Name'
                    }
                }
            },
			contact: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Contact Number'
                    }
                }
            },
			designation: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Designation'
                    }
                }
            },
			description: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Description'
                    }
                }
            },
			opentime: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Opening Time'
                    }
                }
            },
			opentime_ampm: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Opening Time'
                    }
                }
            },
			closetime: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Closing Time'
                    }
                }
            },
			closetime_ampm: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Closing Time'
                    }
                }
            },
			accpet_card: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Accept card'
                    }
                }
            },
			veg_nonveg: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Veg/nonVeg'
                    }
                }
            },
			bar_available: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Bar available'
                    }
                }
            }
			
        }
    })
	.on('success.form.bv', function (e) {
	
        e.preventDefault();
		var count 			= ($('.imageUrl[value!=""]').length);
		var address		=	$("#address").val();
		var city		=	$("#city").val();
		var add			=	address+','+city;
		var formData = new FormData( this );
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "http://maps.googleapis.com/maps/api/geocode/json",
			data: {'address': add,'sensor':false},
			success: function(data){
				if(data.results.length){
					var lat	=	data.results[0].geometry.location.lat;
					var lng	=	data.results[0].geometry.location.lng;
					
					if(count<-1)
					{
					}else{
						//var stateName = $('#state option:selected').data('state');
						
						formData.append('lat', lat);
						formData.append('lng', lng);
							$.ajax({
							url: '<?php echo APP_URL; ?>superadmin/connected_mp/profileEdit',
							type: 'POST',
							data: formData,
							processData: false,
							contentType: false,
							success: function (data) {
								console.log(data);
								if(data	==	'Loc_error')
								{
									$('.alert-success').hide();
									$('.alert-warning').show();
									$('.alert-warning span').html("There is some error while Updating your Address!");
									$("html, body").animate({ scrollTop: 0 }, 600);
								}
								else
								{
									if(data=="true")
									{
										$('.alert-warning').hide();
										$('.alert-success').show();
										$("html, body").animate({ scrollTop: 0 }, 600);
										$('.alert-success span').html("Profile updated successfully!");
										$('.alert-success span').html("Profile updated successfully!");
									}
									else
									{
										$('.alert-success').hide();
										$('.alert-warning').show();
										$('.alert-warning span').html("There is some error while Updating your profile!");
									}
								}
							},
							error: function (data) {
							}
						});
					}
				}else{
					alert('Please Enter vaild City or Address');
					$('#city').addClass("error");
					$('#city').focus();
					$('#address').addClass("error");
					$('#address').focus();
					return false;
			   }
			}
		});
		
		
		
		});
});
   //code for image uploading starts here
   
					function readURL(input, id, fileName, mp_gallery_id) {
						
						if (input.files && input.files[0]) {
							var reader = new FileReader();
							reader.readAsDataURL(input.files[0]);
    						fileSize = Math.round(input.files[0].size);
							var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
							if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png")
							{
								if(fileSize>10485760)
								{
									alert("Please select image less than 10 MB!");
									$('#'+id).val('');
									return false;
								}
								else
								{
									reader.onload = function (e) {
									var image  = new Image();
									image.src = e.target.result;
									image.onload = function () {
										var height = this.height;
										var width = this.width;
											/*if (height < 500 || width < 500) {
												alert("Height should not less than 500PX and Width should not less than 500PX.");
												$('#'+id).val('');
												return false;
											}
											else
											{*/
												
												
												$('#'+id+'Pic').css('opacity', '0.5');
												$('#'+id+'Lod').show();
												var form1 = $('#form')[0];
												var formData = new FormData(form1);
												
												formData.append("image", id);
												$.ajax({
														xhr: function() {
														var xhr = new window.XMLHttpRequest();
														xhr.upload.addEventListener("progress", function(evt) {
														  if (evt.lengthComputable) {
															var percentComplete = evt.loaded / evt.total;
															percentComplete = parseInt(percentComplete * 100);
															$('#'+id+'pic').attr('src', '');
															$('#'+id+'Lod').html(percentComplete+'%');
															console.log(percentComplete);
															
															if (percentComplete === 100) {
													
															}
													
														  }
														}, false);
														return xhr;
														
													  },
														
														url: '<?php echo APP_URL; ?>superadmin/connected_mp/deal_image_show_profile_update',
														type: 'POST',
														data: formData,
														processData: false,
														contentType: false,
														success: function (data) {
															console.log(data);
															var obj = JSON.parse(data);
															$('#'+id+'Lod').hide();
															$('#'+id+'pic').css('opacity', '1.0');
															$('#'+id+'pic').attr('src', '<?php echo APP_CRM_UPLOADS_PATH; ?>'+obj.imageName);
															$('#'+id+'Id').val(obj.id);
															$('#'+id+'btn').show();
															$('#'+id+'Url').val(obj.imageName);
															$('#mp_gallery_id_'+id).val(obj.gallery_id);
														},
														error: function (data) {
														}
											   });	
											
											/*}*/
										}
									}
								}
							}
							else
							{
								alert("Please upload jpg, png or gif format");
								$('#'+id).val('');
								return false;
							}
						}
					}
					$("input:file").change(function(){
						var id 				= 	$(this).attr('id');
						var fileName 		= 	$('#'+id).val();
						var mp_gallery_id	=	$("#mp_gallery_id_"+id).val();
						readURL(this, id, fileName, mp_gallery_id);
					});	
</script>
</html>
