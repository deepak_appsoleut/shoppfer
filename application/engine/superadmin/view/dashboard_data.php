  <?php
   if(count($data[1])>0) { 
  // print_r($data[1]);
   ?>
  <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box" style="background:#eaeaea">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-pricetag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Registered</span>
              <span class="info-box-number"><strong>Device : </strong> <?php echo $data[0][0]['TotalCount']; ?></span>
              <span class="info-box-number"><strong>Users : </strong><?php echo $data[4][0]['customerCnt']; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box" style="background:#eaeaea">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-pricetag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Gender</span>
              <span class="info-box-number"><strong>M : </strong><?php echo $data[4][0]['MaleCount']; ?></span>
               <span class="info-box-number"><strong>F : </strong><?php echo $data[4][0]['FemaleCount']; ?></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box" style="background:#eaeaea">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-pricetag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Registered Via</span>
              <span class="info-box-number"><strong>Google : </strong><?php echo $data[4][0]['googleCount']; ?></span>
               <span class="info-box-number"><strong>Facebook : </strong><?php echo $data[4][0]['facebookCount']; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box" style="background:#eaeaea">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-pricetag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Location Shared</span>
              <span class="info-box-number"><strong>Shared : </strong><?php echo ($data[0][0]['TotalCount'] - $data[0][0]['noLocation']); ?></span>
               <span class="info-box-number"><strong>Not Shared : </strong><?php echo $data[0][0]['noLocation']; ?></span>
                <span class="info-box-number"><strong>Delhi - NCR : </strong><?php echo $data[0][0]['delhi_ncr']; ?></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        
        <!-- /.col -->
      </div>
      <div class="row">
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          <!-- LINE CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Registration Chart</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="line-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- BAR CHART -->
          
          <!-- /.box -->
        </div>
        <!-- /.col (RIGHT) -->
        <div class="col-md-6">
        <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Gender Chart</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="gender-chart" style="height: 300px; position: relative;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Location Shared Chart</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="activity-chart" style="height: 300px;"></div>
            </div>
          </div>
        </div>
        
        <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Top Devices Chart</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
        
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Login Via Chart</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="verify-chart" style="height: 300px;"></div>
            </div>
           
          </div>
         
        </div>
        <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Delhi NCR Chart</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="ncr-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
        <!--
        <div class="col-md-6">
        <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Version Chart</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="version-chart" style="height: 300px; position: relative;"></div>
            </div>
          </div>
          </div>
      </div>-->
      <?php } else { ?>
      <p>No data exist between this range!</p>
      <?php } ?>
<script>
<?php 
$dataTot = "";
for($i=0; $i<count($data[1]); $i++) {
	$dataTot .= "{y: '".date("M j", strtotime($data[1][$i]['cdate']))."', registration :".$data[1][$i]['totalCount']."},";
}
$dataTot = rtrim($dataTot, ",");

$dataBar = "";
for($i=0; $i<count($data[2]); $i++) {
	$dataBar .= "{y: '".$data[2][$i]['device_name']."', a :".$data[2][$i]['cnt']."},";
}
$dataBar = rtrim($dataBar, ",");


?>
$(function()
{
	// LINE CHART
    var line = new Morris.Line({
      element: 'line-chart',
      resize: true,
      data: [
        <?php echo $dataTot; ?>
      ],
      xkey: ['y'],
      ykeys: ['registration'],
      labels: ['Registration'],
      lineColors: ['#3c8dbc'],
	  parseTime: false,
      hideHover: 'auto'
    });
	
	//DONUT CHART
    var donut = new Morris.Donut({
      element: 'gender-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954"],
      data: [
        {label: "Males", value: <?php echo $data[4][0]['MaleCount']; ?>},
        {label: "Females", value: <?php echo $data[4][0]['FemaleCount']; ?>}
      ],
	  formatter:function(value,data){return (value/<?php echo $data[4][0]['customerCnt']; ?> *100).toFixed(2) + '%'},
      hideHover: 'auto'
    });
	
	//bar chart
	 var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
       <?php echo $dataBar; ?>
      ],
      barColors: ['#00a65a'],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Count'],
      hideHover: 'auto'
    });
	
	//bar chart
	 var bar = new Morris.Bar({
      element: 'ncr-chart',
      resize: true,
      data: [
       {y: "Delhi", a: <?php echo $data[3][0]['delhi']; ?>},
        {y: "Gurgaon", a: <?php echo $data[3][0]['gurgaon']; ?>},
		{y: "Fardabad", a: <?php echo $data[3][0]['faridabad']; ?>},
		{y: "Noida", a: <?php echo $data[3][0]['noida']; ?>}
      ],
      barColors: ['#f56954'],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Count'],
      hideHover: 'auto'
    });
	
	//DONUT CHART
	/*
    var donut = new Morris.Donut({
      element: 'status-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954"],
      data: [
        {label: "Odd", value: <?php //echo $data[0][0]['OddCount']; ?>},
        {label: "Even", value: <?php //echo $data[0][0]['EvenCount']; ?>}
      ],
	  formatter:function(value,data){return (value/<?php //echo $data[0][0]['TotalCount']; ?> *100).toFixed(2) + '%'},
      hideHover: 'auto'
    });*/
	//DONUT CHART
    var donut = new Morris.Donut({
      element: 'activity-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954"],
      data: [
        {label: "Not Shared", value: <?php echo $data[0][0]['noLocation']; ?>},
        {label: "Shared", value: <?php echo ($data[0][0]['TotalCount'] - $data[0][0]['noLocation']); ?>}
      ],
	  formatter:function(value,data){return (value/<?php echo $data[0][0]['TotalCount']; ?> *100).toFixed(2) + '%'},
      hideHover: 'auto'
    });
	//DONUT CHART
	
    var donut = new Morris.Donut({
      element: 'verify-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954"],
      data: [
        {label: "Google", value: <?php echo $data[4][0]['googleCount']; ?>},
        {label: "Facebook", value: <?php echo $data[4][0]['facebookCount']; ?>}
      ],
	  formatter:function(value,data){return (value/<?php echo $data[4][0]['customerCnt']; ?> *100).toFixed(2) + '%'},
      hideHover: 'auto'
    });
	//DONUT CHART
	/*
    var donut = new Morris.Donut({
      element: 'version-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954", "#785412"],
      data: [
        {label: "1.2", value: <?php //echo $data[0][0]['v2']; ?>},
        {label: "1.3", value: <?php //echo $data[0][0]['v3']; ?>},
		{label: "1.4", value: <?php //echo $data[0][0]['v4']; ?>}
      ],
	  formatter:function(value,data){return (value/<?php //echo $data[0][0]['TotalCount']; ?> *100).toFixed(2) + '%'},
      hideHover: 'auto'
    });*/
});
</script>      
      