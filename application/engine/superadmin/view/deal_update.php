<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">Offer Details - <?php echo $data[0]['mp_details_name']; ?></h4>
    </div>
    <div class="modal-body">
      <div class="error"></div>
      <form id="form" method="post">
      <?php 
	  if($data[0]['is_drag']==1) {
		  	$noImage = 1;
			if($data[0]['deal_image_status']==0)
			{
				$var = 0;
			}
			else
			{
				$var = 1;
			}
		}
	 else
	 {
		 $noImage = 0;
		 $var = "2";
	 }
	  if($var==0) { ?>   
      <p style="margin:0 15px 15px; color:#fff; padding: 10px; border-radius: 5px;background-color: #C13434;">Ask designer to upload pics first before verify!</p>
      <?php } 
       
       
	  if($data[0]['an_text']!='')
	  {
	  $an_text = explode(',', $data[0]['an_text']);?>
      <div class="row">
       	<div class="col-md-12">
      	<h4 style="border-bottom:1px solid #ccc; padding-bottom:10px; margin-bottom:0">Offer Details</h4>
        </div>
        
           <?php 
		   if(in_array('deal_type', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?>    
                  
            <div class="col-md-4" style="margin-top:10px">
                    <strong style =" <?php echo $style; ?>">Deal Type :</strong> 
                    <span><?php echo $data[0]['offer_type_id']; if($data[0]['offer_type_id'] == 1) { echo 'Flat Offer'; } else { echo 'Combo Offer';}?></span>
            </div>
            
            <?php
             if(in_array('deal_name', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?>   
                   
      		<div class="col-md-4" style="margin-top:10px">
                    <strong style =" <?php echo $style; ?>">Deal Name :</strong> 
                    <span><?php echo $data[0]['deal_name'];?></span>
            </div>
            
            <?php
             if(in_array('offer_description', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?>  
                
            <?php if($data[0]['offer_description']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                    <strong  style =" <?php echo $style; ?>">Description :</strong> 
                    <span><?php echo $data[0]['offer_description'];?></span>
            </div>
            <?php } ?>  
  
  			 <?php
             if(in_array('deal_shoutout_description', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?>
                	
   			<?php if($data[0]['deal_shoutout_description']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                    <strong  style =" <?php echo $style; ?>">Description :</strong> 
                    <span><?php echo $data[0]['deal_shoutout_description'];?></span>
            </div>
            <?php } ?>  
  			 <?php
             if(in_array('offer_price', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?>  
      
       		 <?php if($data[0]['offer_price']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Offer Price :</strong> 
                        <span><?php echo $data[0]['offer_price'];?></span>
             </div>
             <?php } ?>
             
             
             <?php
             if(in_array('offer_discount', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?>  
                
              <?php if($data[0]['offer_discount']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Offer Discount :</strong> 
                        <span><?php echo $data[0]['offer_discount'];?></span>
             </div>
             <?php } ?>
             
               <?php
             if(in_array('offer_save_price', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                 
              <?php if($data[0]['offer_save_price']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Money Saved :</strong> 
                        <span><?php echo $data[0]['offer_save_price'];?></span>
             </div>
             <?php } ?>
      
       <?php
             if(in_array('offer_selling_price', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                
       		 <?php if($data[0]['offer_selling_price']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Selling Amount :</strong> 
                        <span><?php echo $data[0]['offer_selling_price'];?></span>
             </div>
             <?php } ?>
             
             <?php
             if(in_array('max_capping', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                
              <?php if($data[0]['max_capping']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Maximum Capping :</strong> 
                        <span><?php echo $data[0]['max_capping'];?></span>
             </div>
             <?php } ?>
             
             <?php
             if(in_array('min_billing', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                
              <?php if($data[0]['min_billing']!=NULL) { ?>
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Minimum Billing :</strong> 
                        <span><?php echo $data[0]['min_billing'];?></span>
             </div>
             <?php } ?>
             
             <?php
			 if($data[0]['an_deal_type']==1)
			 {
             	if(in_array('offers_day_type', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Offer Day :</strong> 
                        <span><?php echo $data[0]['offers_day_value'];?></span>
             </div>
             
             <?php
             if(in_array('offers_timeings', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Offer Timing :</strong> 
                        <span><?php echo $data[0]['offers_timeings'];?></span>
             </div>             
             <?php
				 if(in_array('offer_coupon_status', $an_text)) {
					$style = "color:#f00";
					} else { $style = ""; } ?> 
				<div class="col-md-4" style="margin-top:10px">
							<strong style =" <?php echo $style; ?>">Offer Coupon Available :</strong> 
							<span>
								<?php if($data[0]['offer_coupon_status']==1) { echo "Yes";} else if($data[0]['offer_coupon_status']==0) {echo "No";} ?>
							</span>
				 </div>
                 <?php 
			 }
			 else
			 {
             if(in_array('deal_shoutout_day', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Offer Day :</strong> 
                        <span><?php echo $data[0]['deal_shoutout_day'];?></span>
             </div>
             
             <?php
             if(in_array('deal_shoutout_time', $an_text)) {
			    $style = "color:#f00";
				} else { $style = ""; } ?> 
                
            <div class="col-md-4" style="margin-top:10px">
                        <strong style =" <?php echo $style; ?>">Offer Timing :</strong> 
                        <span><?php echo $data[0]['deal_shoutout_time'];?></span>
             </div>
              <?php
			 }?>
            
      
      </div>
      <?php } ?>
      
      <?php if($noImage==1) { ?>      
      <div class="row" style="margin-top:15px;">
      <div class="col-md-12">
      	<h4 style="border-bottom:1px solid #ccc; padding-bottom:10px; margin-bottom:0">Offer Image Details</h4>
        </div>
        </div>
      <div class="row" style="margin-top:15px;">
           <div class="col-md-12">
      		<h5><strong>Old Images</strong></h5>
        	</div>
           <?php 
		   if($data[0]['old_image']!="")
		   {
		  		 $oldImages = explode(',', $data[0]['old_image']); 
		   		for($i=0; $i<count($oldImages); $i++)
				{
		   ?>
           	<div class="col-md-4" style="margin-top:5px">
            	<label>Old Image <?php echo $i+1; ?>  : </label><p><img width="200" src="<?php echo APP_CRM_UPLOADS_PATH.$oldImages[$i];  ?>" /></p>
            </div>
            <?php } 
		   }else 
		   {
			   ?>
               <p style="margin-left:20px">No old image changed!</p>
               <?php
		   }
		   ?>
            
            </div>            
      <div class="row" style="margin-top:15px;">
           <div class="col-md-12">
      		<h5><strong>New Images</strong></h5>
        	</div>
           <?php 
		   		if($data[0]['new_image']!="")
				{
				$newImages = explode(',', $data[0]['new_image']); 
		   		for($i=0; $i<count($newImages); $i++)
				{
		   ?>
           	<div class="col-md-4" style="margin-top:5px">
            	<label>New Image <?php echo $i+1; ?>  : </label><p><img width="200" src="<?php echo APP_CRM_UPLOADS_PATH.$newImages[$i];  ?>" /></p>
            </div>
            <?php }
			}else { ?>
             <p style="margin-left:20px">No New image changed!</p>
            <?php } ?>
            
            </div>
     <?php } ?>
       <div class="row">
       	<div class="col-md-8">
        	<input type="hidden" name="an_id" value="<?php echo $data[0]['an_id']; ?>"  />
            <input type="hidden" name="mp_details_id" value="<?php echo $data[0]['mp_details_id']; ?>"  />
            <input type="hidden" name="btn_value" id="btn_value" />
            <input type="hidden" name="deal_id" id="deal_id" value="<?php echo $data[0]['deal_id']; ?>" />
            <input type="hidden" name="offer_id" id="offer_id" value="<?php echo $data[0]['offer_id']; ?>" />
        </div>
       <div class="col-md-4">   
       <?php if($var!=0) { ?>   
       
       <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x pull-right" style="margin-right:10px;"></i></div>
       <button class="btn-danger btn custom_warning_btn pull-right" name="nv" value="nv" style="margin-left:20px" type="submit">Don't Verify</button>
       <button class="btn-warning btn custom_warning_btn pull-right" name="v" value="v" type="submit">Verify</button>
       <?php } ?>
       </div>
       </div>
      </form>
    </div>
    <script>
	
	$("#form button").click(function()
	{
		$('#btn_value').val($(this).val());
	});
	
$('#form').bootstrapValidator({
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
		$('#loading').show();
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/deal_notifications/dealUpdateCompleteStatus',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				$('#loading').hide();
				console.log(data);
				if(data=="true"||data>0)
				{
					$('.error').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Deal Verified successfully!</span></div>');
					
				}
				else
				{
					$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span>There is some error!</span></div>');
				}
            },
            error: function (data) {
            }
   });
});
</script>