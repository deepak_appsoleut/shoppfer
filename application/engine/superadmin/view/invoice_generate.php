<?php $body = '<body style="width:700px;">
<h2 style="color:#036; font-family:Arial, Helvetica, sans-serif; font-size:30px">Invoice</h2>
<table width="600" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
  <tr>
    <td>Invoice Date : Feb 01, 2016</td>
  </tr>
  <tr>
    <td>Period : Jan 26, 2016 - Feb 01, 2016</td>
  </tr>
  <tr>
    <td>Invoice Ref : 123654789</td>
  </tr>
</table>

<table width="600" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-top:20px">
  <tr>
    <td><strong>FROM:</strong></td>
  </tr>
  <tr>
    <td>HotelOne</td>
  </tr>
  <tr>
    <td>B-4, 523, Spaze i-Tech Park,</td>
  </tr>
  <tr>
    <td>Sec - 49, Sohna Road</td>
  </tr>
  <tr>
    <td>Haryana - Gurgaon, 122001</td>
  </tr>
  <tr>
    <td>India</td>
  </tr>
</table>

<table width="600" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-top:20px">
  <tr>
    <td><strong>To:</strong></td>
  </tr>
  <tr>
    <td>Olive Greens</td>
  </tr>
  <tr>
    <td>Tk\'s Orient Grill</td>
  </tr>
  <tr>
    <td>Bhikaji Kama Place, Ring Road</td>
  </tr>
  <tr>
    <td>New Delhi - 110025</td>
  </tr>
  <tr>
    <td>India</td>
  </tr>
</table>
<table width="600" border="1" style="margin-top:20px; border:1px solid #ccc;font-family:Arial, Helvetica, sans-serif; font-size:14px; border-collapse: collapse;" >
  <tr>
    <th scope="col" style="padding:10px;">Description</th>
    <th scope="col" style="padding:10px;">Amount</th>
  </tr>
  <tr>
    <td style="padding:10px;">Food Services</td>
    <td style="padding:10px;">USD 5,589.45</td>
  </tr>
  <tr>
    <td style="padding:10px;">Service Tax(14%)</td>
    <td style="padding:10px;">USD 667.90</td>
  </tr>
  <tr>
    <td style="padding:10px;">Total</td>
    <td style="padding:10px;">USD 6,200</td>
  </tr>
</table>
</body>';
$apikey = '4443b167-7f36-4dbd-a744-49a5e2c735c0';
$result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode($apikey) . '&value=' .urlencode($body));
file_put_contents(APP_CRM_UPLOADS.'mypdf787.pdf',$result);
?>