<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Marketing Partner Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet">
  <!-- jvectormap -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $dashboard = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <aside class="profile-nav col-lg-3">
                      <section class="panel">
                          <div class="user-heading round">
                              <a href="#">
                                  <img src="<?php echo APP_CRM_BS; ?>img/profile-avatar.jpg" alt="">
                              </a>
                              <h1><?php echo $_SESSION['app_admin_user']['user_full_name']; ?></h1>
                              <p><?php echo $_SESSION['app_admin_user']['user_email_id']; ?></p>
                          </div>

                          <ul class="nav nav-pills nav-stacked">
                              <li class="active"><a href="#"> <i class="fa fa-user"></i> Profile</a></li>
                          </ul>

                      </section>
                  </aside>
                  <aside class="profile-info col-lg-9">
                      
                      <section class="panel">
                          <div class="panel-body bio-graph-info">
                              <h1>Edit Profile</h1>
                              <div id="error"></div>
                              <form role="form" id="update_profile" method="post">
                              <div class="row">
                                  <div class="bio-row">
                                  <div class="form-group">
                                      <p><span style="margin-bottom:5px">Name </span> <input type="text" class="form-control" id="f-name" placeholder="Name" name="user_full_name" value="<?php echo $_SESSION['app_admin_user']['user_full_name']; ?>"> </p>
                                      </div>
                                  </div>
                                  <div class="bio-row">
                                  <div class="form-group">
                                      <p><span style="margin-bottom:5px">Email Address </span> <input type="text" disabled class="form-control" id="f-name" placeholder="Name" value="<?php echo $_SESSION['app_admin_user']['user_email_id']; ?>"> </p>
                                      </div>
                                  </div>
                                  <div class="bio-row">
                                  <div class="form-group">
                                      <p><span style="margin-bottom:5px">Mobile Number </span> <input type="text" name="user_mobile_number" class="form-control" id="f-name" placeholder="Name" value="<?php echo $_SESSION['app_admin_user']['user_mobile_number']; ?>"> </p>
                                      </div>
                                  </div>
                                  <div class="bio-row">
                                  
                                  <div class="form-group">
                                      <p><span>Gender </span>
                                      <div class="radios" style="padding:0">
                                      <?php if($_SESSION['app_admin_user']['user_gender']=="m") { ?>
                                              <label class="label_radio r_on" for="radio-01" style="width:100px; float:left">
                                                  <input name="gender" id="radio-01" value="male" type="radio" checked=""> Male
                                              </label>
                                              <label class="label_radio r_off" for="radio-02" style="width:100px; float:left">
                                                  <input name="gender" id="radio-02" value="fdemale" type="radio">Female
                                              </label>
                                              <?php } else {?>
                                               <label class="label_radio r_on" for="radio-01" style="width:100px; float:left">
                                                  <input name="gender" id="radio-01" value="male" type="radio" > Male
                                              </label>
                                              <label class="label_radio r_off" for="radio-02" style="width:100px; float:left">
                                                  <input name="gender" id="radio-02" value="fdemale" checked="" type="radio">Female
                                              </label>
                                              <?php } ?>
                                          </div></p>
                                          </div>
                                  </div>
                                  <div class="form-group">
                                          <div class="col-lg-10">
                                              <button type="submit" class="btn btn-primary">Save</button>
                                          </div>
                                      </div>
                              </div>
                              </form>
                          </div>
                      </section>
                      <section>
                          <div class="panel panel-primary">
                              <div class="panel-heading"> Set New Password </div>
                              <div class="panel-body">
                              <div id="error1"></div>
                                  <form role="form" id="changepwd" class="form-horizontal" method="post">
                                      <div class="form-group">
                                          <label  class="col-lg-2 control-label">Current Password</label>
                                          <div class="col-lg-6">
                                              <input id="current_password" name="current_password" type="password" class="form-control">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label  class="col-lg-2 control-label">New Password</label>
                                          <div class="col-lg-6">
                                              <input id="new_password" name="new_password" type="password" class="form-control">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label  class="col-lg-2 control-label">Re-type New Password</label>
                                          <div class="col-lg-6">
                                              <input id="confirm_password" name="confirm_password" type="password" class="form-control">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button type="submit" class="btn btn-info">Save</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </section>
                  </aside>
              </div>

              <!-- page end-->
          </section>
      </section>
  <!-- /.content-wrapper -->
  
</section>
</body>
<?php include(APP_VIEW.'includes/bottom.php');?>
  <!--custom checkbox & radio-->
  <script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
  <script>
$('#update_profile').bootstrapValidator({
        fields: {
            user_full_name: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Full Name'
                    }
                }
            },
			user_mobile_number: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Mobile Number'
                    },
					stringLength: {
						max : 10,
                        message: 'Please Enter Valid Mobile Number'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
        $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/profile/profileEdit',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data.match("true"))
				{
					$('#error').html('<div class="alert alert-success alert-dismissable"><i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Profile Updated Successfully</b> </div>');	
				}
				else
				{
					$('#error').html('<div class="alert alert-warning alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>There is some problem updating profile, please try again or contact support</b> </div>');	
				}
            },
            error: function (data) {
            }
   });
});
$('#changepwd').bootstrapValidator({
        fields: {
			current_password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Old Password'
                    }
                }
            },
            new_password: {
                validators: {
					identical: {
						field: 'confirm_password',
						message: 'The password and its confirm are not the same'
					},
					notEmpty: {
                        message: 'Please Enter New Password'
                    },
					stringLength: {
						min : 6,
                        message: 'Please Enter Atleast six digits'
                    }
                }
            },
            confirm_password: {
                validators: {
					identical: {
						field: 'new_password',
						message: 'The password and its confirm are not the same'
					},
					notEmpty: {
                        message: 'Please Enter Confirm Password'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/profile/changePassword',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data.match("true"))
				{
					$('#error1').html('<div class="alert alert-success alert-dismissable"><i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Password Updated Successfully!</b> </div>');	
				}
				else
				{
					$('#error1').html('<div class="alert alert-warning alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>There is some problem updating password, please try again or contact support!</b> </div>');	
				}
            },
            error: function (data) {
            }
   });
});
$(function()
{
	$('#current_password').change(function()
	{
		var old = $(this).val();
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/profile/checkPassword',
            type: 'POST',
			data: {"old" : old},
            success: function (data) {
				if(data==0)
				{
					alert('Password does not match!');
					$('#current_password').val('');
				}
            },
            error: function (data) {
            }
   		});
	});
});
</script>
</html>
