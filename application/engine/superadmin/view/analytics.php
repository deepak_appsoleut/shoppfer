<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Superadmin Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
     <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
#loading {
    position: absolute;
	display:none;
	top:5%;
    z-index: 999999;
}
</style>
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $analytics = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:40px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <!--<header class="panel-heading" style="border-bottom:1px solid #E2E2E4;">
                              Notifications
                          </header>-->
                          <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Notifications</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>      
                <th>S.No</th>            
                  <th>Device Name</th>	
                  <th>Address</th>
                  <th>Location</th>
                  <th>Date</th>
                  <th>Version</th>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>Gender</th>
                </tr>
                </thead>
               <tbody>
               <?php 
				for($i=0; $i<count($data); $i++) { ?>
               <tr role="row">
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $data[$i]["device_name"]; ?></td>
                    <td><?php echo $data[$i]["device_address"]; ?></td>
                    <td><?php echo $data[$i]["device_location"]; ?></td>
                    <td><?php echo date('j F,Y', strtotime($data[$i]["device_created_on"])); ?></td>
                    <td><?php echo $data[$i]["device_version"]; ?></td>
                    <td><?php echo $data[$i]["customer_name"]; ?></td>
                    <td><?php echo $data[$i]["customer_mobile"]; ?></td>
                    <td><?php echo $data[$i]["customer_gender"]; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
              </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>
</body>

<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script>
  $(function () {
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
	  aaSorting : [[0, 'desc']]
    });
	
  });

<!-- modal hide and clear data in it -->
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
