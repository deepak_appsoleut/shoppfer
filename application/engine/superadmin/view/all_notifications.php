<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Superadmin Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
     <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
#loading {
    position: absolute;
	display:none;
	top:5%;
    z-index: 999999;
}
</style>
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $note = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:40px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <!--<header class="panel-heading" style="border-bottom:1px solid #E2E2E4;">
                              Notifications
                          </header>-->
                          <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Notifications</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>                  
                  <th>Date</th>	
                  <th>Name</th>
                  <th>Notification</th>
                  <th>Operation</th>
                </tr>
                </thead>
               <tbody>
               <?php 
				for($i=0; $i<count($data); $i++) { ?>
               <tr role="row">
                    <td><?php echo date('j F,Y h:i A', strtotime($data[$i]["an_created_time"])); ?></td>
                    <td><?php echo $data[$i]["name"]; ?></td>
                    <td><?php 
					if($data[$i]["an_process"]=='signup')
					  {
						  if($data[$i]["an_status"]==0)
						  {
							   $button = '<a class="btn btn-danger" data-target="#signup" href="'.APP_URL.'superadmin/all_notifications/view_signup/'.$data[$i]["an_id"].'" data-toggle="modal">View</a>';
						  }
						  else
						  {
							  $button = '';
						  }
					  ?>
                      <p>Signup process is done!</p>
                      <?php } else if($data[$i]["an_process"]=='profile_complete') { 
					  	if($data[$i]["an_status"]==0)
						  {
							  $button = '<a class="btn btn-danger" data-target="#profileComplete" href="'.APP_URL.'superadmin/all_notifications/view_profile/'.$data[$i]["an_id"].'" data-toggle="modal">View</a>';
						  }
						  else
						  {
							  $button = '';
						  }
					  ?>
                      <p>Profile completion is done!</p>
                      <?php } else if($data[$i]["an_process"]=='profile_update') { 
					  if($data[$i]["an_status"]==0)
						  {
							  $button = '<a class="btn btn-primary" data-target="#profileUpdate" href="'.APP_URL.'superadmin/all_notifications/view_update_profile/'.$data[$i]["an_id"].'" data-toggle="modal">Approval</a>';
						  }
						  else
						  {
							  $button = '';
						  }
					  ?>
                      <p>Profile details are updated!</p>
                      <?php } else if($data[$i]["an_process"]=='gallery_update') { 
					   if($data[$i]["an_status"]==0)
						  {
							  $button = '<a class="btn btn-primary" data-target="#galleryUpdate" href="'.APP_URL.'superadmin/all_notifications/view_gallery_profile/'.$data[$i]["an_id"].'" data-toggle="modal">Approval</a>';
						  }
						  else
						  {
							  $button = '';
						  }
					  ?>
                      <p>Gallery is updated!</p>
                      <?php } ?>
                      </td>
                    <td><?php echo $button; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
              </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="profileComplete">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="profileUpdate">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="galleryUpdate">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="signup">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	
    </div>
  </div>
</div>
</body>

<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script>
  $(function () {
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
	  aaSorting : [[0, 'desc']]
    });
	
	$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal', '.modal', function () {
$(this).removeData('bs.modal');
});

  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
