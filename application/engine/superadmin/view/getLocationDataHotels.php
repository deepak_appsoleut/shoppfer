<div class="row">
  <?php
$category_array1 = array();
for($i=0; $i<count($data[0]); $i++)
{
 ?>
  <div class="col-md-6" style="min-height:200px">
    <div class="row">
      <div class='col-md-5'>
        <?php if($data[0][$i]['image']==NULL) { $data[0][$i]['image'] = "no-thumb.jpg"; } ?>
        <img src='<?php echo APP_CRM_UPLOADS_PATH.$data[0][$i]['image']; ?>' style="width:200px; height:150px;border: 1px solid #ccc;"> </div>
      <div class='col-md-7'>
        <h4><a href="<?php APP_URL; ?>connected_mp/merchant_view/<?php echo $data[0][$i]['mp_details_id'];?>" style='color: #123456;font-weight: bold;'><?php echo ucwords($data[0][$i]['mp_details_name']); ?></a></h4>
        <h5> <i class="fa fa-map-marker"></i>&nbsp; </span><?php echo $data[0][$i]['mp_details_address'] ?>,<?php echo $data[0][$i]['mp_details_city'] ?></h5>
        <h5><strong>Category</strong> : <?php echo $data[0][$i]['category_name']; ?></h5>
        <p><strong>Deal Name : </strong>
          <?php if($data[0][$i]['deal_status']==NULL || $data[0][$i]['deal_status']==0) 
{ 
echo '<span class="label label-danger">No offer Running</span>';
} 
else
{
	echo '<span class="label label-primary">'.$data[0][$i]['deal_name'].'</span>';
}?>
        </p>
        <?php 
if($data[0][$i]["mp_status"]==1)
										{
											 $active	=	'<div style="width: 83px;float:right; margin-bottom:20px" class="inactive" title="green" id="'.$data[0][$i]["mp_details_id"].'"><input type="checkbox" checked data-toggle="switch" /></div>';	 
										}
										else
										{
											$active	=	'<div style="width: 83px; float:right;margin-bottom:20px" class="active1" title="rad" id="'.$data[0][$i]["mp_details_id"].'"><input type="checkbox" data-toggle="switch"/></div>';
										}
										echo $active;
?>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<div class="row">
  <div class="col-sm-9">
    <ul class="pagination">
      <?php
				$pageNumber = 1;
	        		if(isset($_POST['page_number']))
	        		{
						preg_match_all('!\d+!', $_POST['page_number'], $page);
						$pageNumber = $page[0][0];
	        		} 
                  $totalrecords = $data[1][0]['count']; 
                  $pg = new pagination();
                  $pg->pagenumber = $pageNumber;
                  $pg->pagesize = 10;
                  $pg->totalrecords = $totalrecords;
                  $pg->showfirst = true;
                  $pg->showlast = true;
                  $pg->paginationcss = "pagination-normal";
                  $pg->paginationstyle = 1; // 1: advance advance pagination, 0: normal pagination
                  $pg->defaultUrl = "";
                  $pg->paginationUrl = "";
                  echo $pg->process();
                ?>
    </ul>
  </div>
  <div class="col-sm-3 viewall"> <a href="#" class="all-profile marBot30 marTop20 pull-right"> Results found <?php echo $totalrecords; ?> listings </a> </div>
</div>
<!--==========Edit lead view Modal========--> 
<script>
$(function()
{
	$("[data-toggle='switch']").wrap('<div class="switch"/>').parent().bootstrapSwitch();
});
</script> 
