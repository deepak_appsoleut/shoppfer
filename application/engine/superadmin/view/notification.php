 <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                            <i class="fa fa-bell-o"></i>
                            <?php
							 if(count($data)>0) { ?>
                            <span class="badge bg-warning"><?php echo count($data); ?></span>
                            <?php } else { ?>
                            <span class="badge bg-warning"></span>
                            <?php } ?>
                        </a>
<ul class="dropdown-menu extended notification" style="height:500px; overflow-y:scroll; overflow-x: hidden; width:300px !important">
                            <div class="notify-arrow notify-arrow-yellow" style="display:none"></div>
                            <li>
                                <p class="yellow" style="background-color: #5AD0B6;">You have <?php echo count($data); ?> new notifications</p>
                            </li>
                             <?php for($i=0; $i<count($data); $i++) { 
							 if(($data[$i]["an_process"]=="create_deal")||($data[$i]["an_process"]=="deal_update")||($data[$i]["an_process"]=="deal_status"))			
							 {
								 $url = "deal_notifications";
							 }
							 else
							 {
								 $url = "all_notifications";
							 }
							 ?>
                            <li>
                               <a href="<?php echo APP_URL; ?>superadmin/<?php echo $url; ?>">
                      <h5><?php echo $data[$i]['name']; ?></small>
                      </h5>
                      <?php  
					  if($data[$i]["an_process"]=='signup')
					  {
					  ?>
                      <p>Signup process is done!</p>
                      <?php } else if($data[$i]["an_process"]=='profile_complete') { ?>
                      <p>Profile completion is done!</p>
                      <?php } else if($data[$i]["an_process"]=='profile_update') { ?>
                      <p>Profile details are updated!</p>
                      <?php } else if($data[$i]["an_process"]=='gallery_update') { ?>
                       <p>Gallery is updated!</p>
                      <?php } else if($data[$i]["an_process"]=='create_deal') { ?>
                      <p>Deal has been added!</p>
                      <?php } else if($data[$i]["an_process"]=='deal_update') { ?>
                       <p>Deal has been updated!</p>
                      <?php }
					   ?>
                      <div style="float:left; margin-top:10px">
                      <span style="float:left; width:130px"><?php if(isset($data[$i]["category_name"])) { echo $data[$i]["category_name"]; }?></span>
                      <span class="pull-right text-right" style="width:130px; font-size:12px"><i class="fa fa-calendar"></i> <?php echo date('j M, h:i A', strtotime($data[$i]["an_created_time"])) ?></span>
                      </div>
                    </a>
                            </li>
                            <?php } ?>
                            <li>
                                <!--<a href="<?php //APP_URL; ?>all_notifications" style="width:auto; top:530px; position:fixed">See all notifications</a>-->
                            </li>
                        </ul>