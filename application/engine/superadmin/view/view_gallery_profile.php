<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title" id="myModalLabel">View Gallery</h4>
    </div>
    <div class="modal-body">
      <div class="error"></div>
      <form id="form" method="post">
      <div class="row">
      		<div class="col-md-12"> 
            	<img width="200" src="<?php echo APP_CRM_UPLOADS_PATH.'media/'.$data[0]['an_text'];  ?>" />        	
            </div>
      </div>
       <div class="row">
       	<div class="col-md-6">
        	<input type="hidden" name="an_id" value="<?php echo $data[0]['an_id']; ?>"  />
            <input type="hidden" name="mp_details_id" value="<?php echo $data[0]['mp_details_id']; ?>"  />
            <input type="hidden" name="btn_value" id="btn_value" />
            <input type="hidden" name="imageVal" id="imageVal" value="<?php echo $data[0]['an_text']; ?>"  />
        </div>
       <div class="col-md-6">   
       <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x pull-right" style="margin-right:10px;"></i></div>   
       <button class="btn-danger btn custom_warning_btn pull-right" name="nv" value="nv" style="margin-left:20px" type="submit">Don't Verify</button>
       <button class="btn-warning btn custom_warning_btn pull-right" name="v" value="v" type="submit">Verify</button>
       </div>
       </div>
      </form>
    </div>
    <script>
	
	$("#form button").click(function()
	{
		$('#btn_value').val($(this).val());
	});
	
$('#form').bootstrapValidator({
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
		$('#loading').show();
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/all_notifications/verifyGalleryStatus',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				console.log(data);
				$('#loading').hide();
				if(data=="true")
				{
					$('.error').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Gallery Added successfully!</span></div>');
					 location.reload();
				}
				else
				{
					$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span></span></div>');
				}
            },
            error: function (data) {
            }
   });
});
</script>