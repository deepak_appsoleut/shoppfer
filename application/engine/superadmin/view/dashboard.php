<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Marketing Partner Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="http://appsoleut.technology/oddEvenCrm/public/assets/crm/dist/css/AdminLTE.css">

   <link rel="stylesheet" href="http://appsoleut.technology/oddEvenCrm/public/assets/crm/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="http://appsoleut.technology/oddEvenCrm/public/assets/crm/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="http://appsoleut.technology/oddEvenCrm/public/assets/crm/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- jvectormap -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $dashboard = "active";
  include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper">
              <!--state overview start-->
              <div class="row">	
    	<div class="col-md-3">
          <h3 style="margin:0; padding:0">Dashboard</h3>
        </div>
        <div class="col-md-6">
        	<div class="row" id="customDate">
            <div class="col-md-8">
        	 <div class="form-group">
             	<input name="datefrom" id="config-demo1" placeholder="Please pick date here" class="form-control input-sm" type="text">
             </div>
             </div>
             <div class="col-md-4">
        	 <div class="form-group">
             	<button type="submit" class="btn btn-primary btn-block" id="filter1">Filter</button>
             </div>
             </div>
             </div>
        </div>
        </div>
              <!--state overview end-->
              <section class="content">
                <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x" style="margin-right:10px;"></i>Loading Data....</div>
                  <!-- Info boxes -->
                    <div id="data"></div>
                </section>
          </section>
      </section>
  <!-- /.content-wrapper -->
  
</section>
</body>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>morris.js-0.4.3/morris.min.js"></script>
<!-- FastClick -->
<script src="http://appsoleut.technology/oddEvenCrm/public/assets/crm/plugins/fastclick/fastclick.js"></script>

<script src="http://appsoleut.technology/oddEvenCrm/public/assets/crm/plugins/daterangepicker/moment.js"></script>
<script src="http://appsoleut.technology/oddEvenCrm/public/assets/crm/plugins/daterangepicker/daterangepicker.js"></script>
<script>
function showData(startDate, endDate)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>superadmin/dashboard/dashboard_data',
            type: 'POST',
			data: {"startDate" : startDate, "endDate" : endDate},
            success: function (data) {
				$('#loading').hide();
				$('#data').html(data);
            },
            error: function (data) {
            }
   	});
}

	   $('#config-demo1').daterangepicker(
	   {
			ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			'Last 7 Days': [moment().subtract('days', 6), moment()],
			'Last 30 Days': [moment().subtract('days', 29), moment()],
			'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
			'Last Quarter': [moment().subtract('month', 3).startOf('month'), moment().subtract('month', 1).endOf('month')]
				},
				 format: 'YYYY/MM/DD',
	   }
	   );

  $(function () {
	 /* var d = new Date();
	  var month1 = d.getMonth();
	  var day1 = d.getDate();
	  var startDate = d.getFullYear() +'/' + ((''+month1).length<2 ? '0' : '') + month1 + '/' + ((''+day1).length<2 ? '0' : '') + day1;
	  var month = d.getMonth()+1;
	  var day = d.getDate();
      var endDate = d.getFullYear() +'/' + ((''+month).length<2 ? '0' : '') + month + '/' + ((''+day).length<2 ? '0' : '') + day;*/
	  showData(null, null);
	   $('#filter1').click(function()
	  {
		  var dateSelect = $('#config-demo1').val();
		  var res = dateSelect.split("-");
		  showData(res[0], res[1]);
	  });
  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</html>
