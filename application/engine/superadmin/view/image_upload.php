<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Superadmin Dashboard</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
 <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.css">
   <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
     <link href="<?php echo APP_CRM_BS; ?>css/bootstrapValidator.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<section id="container" >
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $images = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
          <section class="wrapper site-min-height" style="margin-top:40px">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <!--<header class="panel-heading" style="border-bottom:1px solid #E2E2E4;">
                              Image Upload
                          </header>-->
                          <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Image Upload</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Date</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Operation</th>
                </tr>
                </thead>
               <tbody>
               <?php 
				for($i=0; $i<count($data); $i++) { 
				?>
               <tr role="row">
                    <td><?php echo date("j F,Y h:i A", strtotime($data[$i]['an_created_time'])); ?></td>
                     <td><?php echo $data[$i]["mp_details_name"]; ?></td>
                     <td><?php echo $data[$i]["deal_name"]; ?></td>
                    <td>
                    <?php 
					if($data[$i]['an_process']=='deal_update') 
						{ 
							if($data[$i]["deal_image_status"]==0)
							{
							?>
                        		<a class="btn btn-danger" href="<?php APP_URL; ?>image_upload/view_updated_images/<?php echo $data[$i]["deal_id"]; ?>">View</a>
                        <?php 
							}
							else
							{
								?>
                                <a class="btn btn-primary" href="<?php APP_URL; ?>image_upload/view_updated_images/<?php echo $data[$i]["deal_id"]; ?>">Done</a>
                                <?php
							}
						} 
						else if($data[$i]['an_process']=='create_deal')
						{ 
							if($data[$i]["deal_image_status"]==0)
							{
						?>
                        
                    		<a class="btn btn-danger" href="<?php APP_URL; ?>image_upload/view_images/<?php echo $data[$i]["deal_id"]; ?>">View</a>
                     	<?php 
							}
							else
							{
								?>
                                <a class="btn btn-primary" href="<?php APP_URL; ?>image_upload/view_images/<?php echo $data[$i]["deal_id"]; ?>">Done</a>
                                <?php
							}
						} ?>
                    </td>
                    </tr>
                <?php } ?>
                </tbody>
              </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
</section>
</body>

<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo APP_CRM_PLUGIN; ?>advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>data-tables/DT_bootstrap.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script>
  $(function () {
    $('#example').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
	  aaSorting : [[0, 'desc']]
    });
	
	$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal', '.modal', function () {
$(this).removeData('bs.modal');
});

  });
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
