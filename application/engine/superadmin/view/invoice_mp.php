<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Profile Details</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>daterangepicker/daterangepicker-bs3.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  .daterangepicker.opensright .ranges, .daterangepicker.opensright .calendar, .daterangepicker.openscenter .ranges, .daterangepicker.openscenter .calendar
  {
	 float:left;
  }
  </style>
</head>
<body class="hold-transition skin-white sidebar-mini">
<div class="wrapper">
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include(APP_VIEW.'includes/nav.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice MP Details
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
    	<div class="row">
        <form id="form">
        	<div class="col-md-3">
            <div class="form-group">
                 <label>Select Category</label>
            	<select class="form-control" name="category" id="category">
                	<?php for($i=0; $i<count($data); $i++) { ?>
                    	<option value="<?php echo $data[$i]['category_id']; ?>"><?php echo $data[$i]['category_name']; ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                 <label>Select MP</label>
            	<select class="form-control" name="mp" id="mp">
                	
                </select>
                </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
                 <label>Select Daterange</label>
             	<input name="datefrom" id="config-demo" placeholder="Please pick date here" class="form-control input-sm" type="text">
                </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
            <label style="width:100%">&nbsp;</label>
             	<button class="btn btn-primary" type="button" id="preview">Preview</button>
                <button class="btn btn-primary" type="button" id="generate">Generate PDF</button>
                </div>
            </div>
            </form>
        </div>
        <div class="row">
        	<div class="col-md-12">
            	<div class="result" style="padding: 10px;background: #EAEAEA;border: 1px solid #ccc; display:none">
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include(APP_VIEW.'includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>daterangepicker/moment.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>daterangepicker/daterangepicker.js"></script>
<script>
// for datepicker 
 $('#config-demo').daterangepicker( {format: 'YYYY/MM/DD'} );
 
 //function for getting MP values
 function getMP(category)
 {
	     $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/invoice_mp/getMP',
            type: 'POST',
			data: {"category":category},
            success: function (data) {
				$('#mp').html(data);
            },
            error: function (data) {
            }
   });
 }
 
$(function()
{
	var category = $('#category').val();
    getMP(category);
	$('#category').change(function()
	{
		category = $(this).val();
		getMP(category);
	});
	
	// to generate preview
	$('#preview').click(function()
	{
		var category = $('#category').val();
		var mp = $('#mp').val();
		var daterange = $('#config-demo').val();
		 $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/invoice_mp/invoice_preview',
            type: 'POST',
			data: {"category":category, "mp_id":mp, "daterange" : daterange},
            success: function (data) {
				$('.result').html(data);
				$('.result').fadeIn();
            },
            error: function (data) {
            }
   		});
	});
	// to generate pdf
	$('#generate').click(function()
	{
		var category = $('#category').val();
		var mp = $('#mp').val();
		var datetange = $('#config-demo').val();
		 $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/invoice_mp/invoice_generate',
            type: 'POST',
			data: {"category":category},
            success: function (data) {
				$('.result').html(data);
				$('.result').fadeIn();
            },
            error: function (data) {
            }
   		});
	});
});

</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->

</body>
</html>
