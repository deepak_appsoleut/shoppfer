<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title" id="myModalLabel">Offer Details - <?php echo $data[0]['mp_details_name']; ?></h4>
</div>
<div class="modal-body">
  <div class="error"></div>
  <form id="form" method="post">
    <?php 
	  if($data[0]['deal_image_status']==0) { $var = 0; } else {$var = 1;}  ?>
    <?php if($var==0) { ?>
    <p style="margin:0 15px 15px; color:#fff; padding: 10px; border-radius: 5px;background-color: #C13434;">Ask designer to upload pics first before verify!</p>
    <?php } ?>
    <div class="row">
      <div class="col-md-12">
        <h4 style="border-bottom:1px solid #ccc; padding-bottom:10px; margin-bottom:0">Deal Details</h4>
      </div>
      <div class="col-md-4" style="margin-top:10px"> <strong>Deal Type :</strong> <span>
        <?php if($data[0]['deal_type'] == 1) { echo 'Normal Offer'; } else { echo 'Shotout Offer';}?>
        </span> </div>
      <div class="col-md-4" style="margin-top:10px"> <strong>Deal Name :</strong> <span><?php echo $data[0]['deal_name'];?></span> </div>
      <?php if($data[0]['deal_shoutout_description']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Description :</strong> <span><?php echo $data[0]['deal_shoutout_description'];?></span> </div>
      <?php } ?>
      <?php if($data[0]['deal_shoutout_day']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Deal Shoutout day :</strong> <span><?php echo $data[0]['deal_shoutout_day'];?></span> </div>
      <?php } ?>
      <?php if($data[0]['deal_shoutout_time']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Deal Shoutout Time :</strong> <span><?php echo $data[0]['deal_shoutout_time'];?></span> </div>
      <?php } ?>
      <?php if($data[0]['deal_created_on']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Deal Created on :</strong> <span><?php echo date('d-m-Y', strtotime($data[0]['deal_created_on']));?></span> </div>
      <?php } ?>
    </div>
    <?php if($data[0]['offer_id']!=NULL) { ?>
    <div class="row" style="margin-top:15px">
      <div class="col-md-12">
        <h4 style="border-bottom:1px solid #ccc; padding-bottom:10px; margin-bottom:0">Offer Details</h4>
      </div>
      <?php for($i=0; $i<count($data); $i++) { ?>
      <h4 style="float: left;width: 100%;margin-left: 15px;color: #d22537; font-size: 18px;">Offer <?php echo $i+1; ?></h4>
      <div class="col-md-4" style="margin-top:10px"> <strong>Offer Type :</strong> <span>
        <?php if($data[$i]['offer_type_id'] == 1) { echo 'Flat Offer'; } else { echo 'Combo Offer';}?>
        </span> </div>
      <div class="col-md-4" style="margin-top:10px"> <strong>Offer Name :</strong> <span><?php echo $data[$i]['deal_name'];?></span> </div>
      <?php if($data[$i]['offer_description']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Offer Description :</strong> <span><?php echo $data[$i]['offer_description'];?></span> </div>
      <?php } ?>
      <?php if($data[$i]['offer_price']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Offer Price :</strong> <span><?php echo $data[$i]['offer_price'];?></span> </div>
      <?php } ?>
      <?php if($data[$i]['offer_discount']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Offer Discount :</strong> <span><?php echo $data[$i]['offer_discount'];?>%</span> </div>
      <?php } ?>
      <?php if($data[$i]['offer_save_price']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Money Saved :</strong> <span><?php echo $data[$i]['offer_save_price'];?></span> </div>
      <?php } ?>
      <?php if($data[$i]['offer_selling_price']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Selling Amount :</strong> <span><?php echo $data[$i]['offer_selling_price'];?></span> </div>
      <?php } ?>
      <?php if($data[$i]['max_capping']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Maximum Capping :</strong> <span><?php echo $data[$i]['max_capping'];?></span> </div>
      <?php } ?>
      <?php if($data[$i]['min_billing']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong>Minimum Billing :</strong> <span><?php echo $data[$i]['min_billing'];?></span> </div>
      <?php } ?>
      <?php
             if($data[$i]['offers_day_value']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong style =" <?php echo $style; ?>">Offer Day :</strong> <span><?php echo $data[$i]['offers_day_value'];?></span> </div>
      <?php } ?>
      <?php
             if($data[$i]['offers_timeings']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong style =" <?php echo $style; ?>">Offer Timing :</strong> <span><?php echo $data[$i]['offers_timeings'];?></span> </div>
      <?php } ?>
      <?php
             if($data[$i]['offer_coupon_status']!=NULL) { ?>
      <div class="col-md-4" style="margin-top:10px"> <strong style =" <?php echo $style; ?>">Offer Coupon Available :</strong> <span>
        <?php if($data[$i]['offer_coupon_status']==1) { echo "Yes";} else {echo "No";} ?>
        </span> </div>
      <?php } 
		}?>
    </div>
    <?php } ?>
    <div class="row" style="margin-top:15px;">
      <div class="col-md-12">
        <h4 style="border-bottom:1px solid #ccc; padding-bottom:10px; margin-bottom:0">Offer Image Details</h4>
      </div>
    </div>
    <?php 
	   if($var==0) {
		   ?>
    <div class="row" style="margin-top:15px;">
      <div class="col-md-12">
        <h5><strong>Media Details</strong></h5>
      </div>
      <?php
	  $explode =explode(',',$data[0]['deal_image_media']); 
	  for($i=0; $i<count($explode); $i++) 
	  {
	  ?>
      <div class="col-md-4" style="margin-top:5px">
        <label>Deal Image <?php echo $i+1; ?> : </label>
        <p><img width="200" src="<?php echo APP_CRM_UPLOADS_PATH.$explode[$i];  ?>" /></p>
      </div>
      <?php }
			?>
    </div>
    <?php
		}
		else 
		{
			?>
    <div class="row">
      <div class="col-md-4" style="margin-top:5px">
        <label>Category Image  : </label>
        <p><img width="200" src="<?php echo APP_CRM_UPLOADS_PATH.$data[0]['deal_image_category'];  ?>" /></p>
      </div>
      <div class="col-md-4" style="margin-top:5px">
        <label>Thumb Image  : </label>
        <p><img width="200" src="<?php echo APP_CRM_UPLOADS_PATH.$data[0]['deal_image_thumb'];  ?>" /></p>
      </div>
    </div>
    <div class="row">
      <?php
				
				  $explode =explode(',',$data[0]['deal_image_details']); 
				  for($i=0; $i<count($explode); $i++) 
				  {
				  ?>
      <div class="col-md-4" style="margin-top:5px">
        <label>Details Image <?php echo $i+1; ?> : </label>
        <p><img width="200" src="<?php echo APP_CRM_UPLOADS_PATH.$explode[$i];  ?>" /></p>
      </div>
      <?php }
			?>
    </div>
    <?php
		}
		?>
    <div class="row">
      <div class="col-md-6">
        <input type="hidden" name="an_id" value="<?php echo $data[0]['an_id']; ?>"  />
        <input type="hidden" name="mp_details_id" value="<?php echo $data[0]['mp_details_id']; ?>"  />
        <input type="hidden" name="btn_value" id="btn_value" />
        <input type="hidden" name="deal_id" id="deal_id" value="<?php echo $data[0]['deal_id']; ?>" />
      </div>
      <div class="col-md-6">
        <?php if($var==1) { ?>
        <div id="loading"><i class="fa fa-spinner fa-pulse fa-3x pull-right" style="margin-right:10px;"></i></div>
        <button class="btn-danger btn custom_warning_btn pull-right" name="nv" value="nv" style="margin-left:20px" type="submit">Don't Verify</button>
        <button class="btn-warning btn custom_warning_btn pull-right" name="v" value="v" type="submit">Verify</button>
        <?php } ?>
      </div>
    </div>
  </form>
</div>
<script>
	
	$("#form button").click(function()
	{
		$('#btn_value').val($(this).val());
	});
	
$('#form').bootstrapValidator({
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
		$('#loading').show();
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/deal_notifications/dealCompleteStatus',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				$('#loading').hide();
				console.log(data);
				if(data=="1")
				{
					$('.error').html('<div class="alert alert-success alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Success : </b> <span>Deal Verified successfully!</span></div>');
				}
				else
				{
					$('.error').html('<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <b>Warning : </b> <span>There is some error!</span></div>');
				}
            },
            error: function (data) {
            }
   });
});
</script>