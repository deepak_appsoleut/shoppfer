<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Shoppfer | Roles</title>
<?php include(APP_VIEW.'includes/top.php');?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>datatables/dataTables.bootstrap.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-white sidebar-mini">
<div class="wrapper">
  <?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include(APP_VIEW.'includes/nav.php');?>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Role Managment </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo APP_URL ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Role</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content"> 
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"><a href="<?php echo APP_URL ?>admin/role" class="btn btn-warning btn-block">HotelONE</a></div>
        <div class="col-md-2"><a href="<?php echo APP_URL ?>admin/role_marketing" class="btn btn-default btn-block">Marketing Partners</a></div>
        <div class="col-md-2"><a href="<?php echo APP_URL ?>admin/role_hotels" class="btn btn-default btn-block">Hotels</a></div>
        <div class="col-md-6"><a class="pull-right btn btn-primary" href="<?php echo APP_URL; ?>admin/role/add_role" title="Add Role">Add Role</a></div>
      </div>
      <!-- datatable -->
      <div class="clearfix"></div>
      <div class="row marTop20">
        <div class="col-xs-12">
          <div class="box"> 
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-reponsive">
                <thead>
                <tr>
                  <th>Role Id</th>
                  <th>Role Name</th>
                  <th>Role Status</th>
                  <th>Operation</th>
                </tr>
                </thead>
                <tbody>
                <?php for($i=0; $i<count($data); $i++) { 
					if(($data[$i]['role_status'])==1)
                    {
                        $active = "<span class='badge bg-green'>Active</span>";
                    }
                    else
                    {
                        $active = "<span class='badge bg-red'>Inactive</span>";
                    }
				?>
                <tr role="row">
                    <td><?php echo $data[$i]['role_id']; ?></td>
                    <td><?php echo ucwords($data[$i]['role_name']); ?></td>
                    <td><?php echo $active; ?></td>
                    <td><a href="<?php echo APP_URL; ?>admin/role/edit_role/<?php echo $data[$i]['role_id'];?>"><i class="fa fa-pencil-square-o"></i>Edit</a>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
    </section>
    <!-- /.content --> 
  </div>
  <!-- /.content-wrapper -->
  <?php include(APP_VIEW.'includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App --> 
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script> 
<!-- DataTables -->
<script src="<?php echo APP_CRM_PLUGIN; ?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo APP_CRM_PLUGIN; ?>datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<!-- ChartJS 1.0.1 --> 
<!-- AdminLTE for demo purposes -->
</body>
</html>
