<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Shoppfer | Superadmin Send Notification</title>
<?php include(APP_VIEW.'includes/top.php');?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="<?php echo APP_CRM_BS; ?>css/select2.min.css"> 
<style>
#loading {
	margin-left: 15px;
	margin-top: 15px;
	float: left;
	display:none;
}
	.progress 
	{
		margin-top:5px;
	}
	.progress .percent
	{
		color:#fff;
		padding-left:10px;
	}
	#spin
	{
		position:absolute;
		top:20%;
		left:85%;
		display:none;
		color:#000;
		font-size:20px;
	}
	.fa-spinner
	{
		position:absolute;
		top:35%;
		left:45%;
		color:#000;
		display:none;
	}
</style>
<link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
</head>
<body>
<section id="container" >
  <?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $send = "active";
  include(APP_VIEW.'includes/nav.php');?>
  
  <!-- Content Wrapper. Contains page content -->
  <section id="main-content">
    <section class="wrapper" style="margin-top:40px"> 
      <!-- page start-->
      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <!--<header class="panel-heading" style="border-bottom:1px solid #E2E2E4;"> Send Notification </header>-->
            <h3 class="box-title" style="margin-bottom: 0px;border-bottom: 1px solid #5AD0B6;height: 22px;font-size:16PX;width: 98.5%;margin-top: 18px;">
    <strong>Send Notification</strong>
  </h3>
  <div style="height: 5px;width: 60px;background: #5AD0B6;"></div>
            <div class="panel-body">
              <form id="form">
                <aside class="profile-info col-lg-12">
                  <section class="panel">
                    <div class="panel-body bio-graph-info">
                      <div id="error"></div>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <p><span style="margin-bottom:5px">Merchant Name </span>
                              <select class="form-control select2" name="name" style="width: 100%;">
                              <option value="">--Select Merchant--</option>
                              <?php for($i=0; $i<count($data); $i++) { ?>
                              <option value="<?php echo $data[$i]['mp_details_id'] ?>"><?php echo $data[$i]['mp_details_name']; ?></option>
                              <?php } ?>
                            </select>
                            </p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p><span style="margin-bottom:5px">Title </span>
                              <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="" maxlength="30">
                            </p>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p><span style="margin-bottom:5px">Upload Image(800*578) </span>
                            </p>
                          	<p>
                            	<img width="200" id="imageShow" style="border:1px solid #ccc;margin-bottom:10px" height="150" src="<?php echo APP_CRM_UPLOADS_PATH.'no-thumb.jpg'; ?>">
                            </p>                            
                            <p>
                              <input type="file" name="file" id="imageFile" />
                              <input type="hidden" name="imageVal" id="imageVal" />
                            </p>
                              <i class="fa fa-spinner fa-pulse fa-3x" id="spin"></i>
                              <div style="clear:both"></div>
                             <div class="progress" id="progress" style="display:none">
                                    <div class="percent" id="percent">0%</div >
                                </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <p><span style="margin-bottom:5px">Description </span>
                              <input type="text" name="description" class="form-control" maxlength="50" id="description" placeholder="Description" value="">
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section>
                    <div class="panel panel-primary">
                      <div class="panel-heading"> Filter By </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <p><span style="margin-bottom:5px">Users </span>
                                <select class="form-control" id="user_type" name="user_type">
                                	<option value="all" selected>All </option>
                                    <option value="registered" >Registered </option>
                                    <option value="not-registered">Not Registered </option>
                                </select>
                              </p>
                            </div>
                          </div>
                          <div class="col-md-3">
                          <div class="form-group">
                            <p><span style="margin-bottom:5px">Location </span>
                              <input class="form-control" id="geocomplete1" type="text" name="location" placeholder="Type in an address" size="90" />
                               <div class="map_canvas" style="display:none"></div>
                            </p>
                          </div>
                        </div>
                        	<div class="col-md-3" style="margin-top:20px">
                          <div class="form-group">
                          	<button type="button" id="filter" class="btn btn-danger btn-block">Filter</button>                            
                          </div>
                        </div>
                        </div>
                        <div class="row" id="showResult">
                        </div>
                        <div class="row">
                        	<div class="col-md-6 col-sm-offset-3" style="margin-top:20px">
                            	<button type="submit" class="btn btn-primary col-md-12" id="send" disabled>Send Notification</button>
                            </div>
                            <div class="col-md-3" style="margin-top:20px">
                            	<i class="fa fa-spinner fa-pulse fa-3x" id="spin1"></i>
                                <p id="notifySuccess"></p>
                            </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </aside>
              </form>
            </div>
          </section>
        </div>
      </div>
      <!-- page end--> 
    </section>
  </section>
</section>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="showTrend">
  <div class="modal-dialog modal-sm">
    <div class="modal-content"> </div>
  </div>
</div>
</body>
<?php include(APP_VIEW.'includes/bottom.php');?>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBGVsk8yP5Ar8cCbQQp69OzGZJLqk3rwx8"></script>-->
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://ubilabs.github.io/geocomplete/jquery.geocomplete.js"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script src="<?php echo APP_CRM_BS; ?>js/select2.full.min.js"></script>
<script>
      $(function(){
		  $(".select2").select2();
		  $('.select2').change(function()
		  {
			  var value = $(this).val();
			  if(value!="")
			  {
				  $('#user_type').append("<option selected value='merchant'>Merchant Location</option>");						
			  }
			  else
			  {
				  $('#user_type').html('<option value="all" selected="">All </option><option value="registered">Registered </option><option value="not-registered">Not Registered </option>');
			  }
		  });		
 });
    </script>
<script type="text/javascript">
	$(function()
	{
		 var options = {
          location: ""
        };
  		$("#geocomplete").geocomplete(options);
  
		$('#filter').click(function()
		{
			var user_type = $('#user_type').val();
			var location = $('#geocomplete1').val();
			var merchant_id = $('.select2').val();
			$.ajax({
				url: "<?php echo APP_URL; ?>superadmin/send_notification/filterData", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: {"user_type":user_type, "location":location, "merchant_id":merchant_id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				success: function(data)   // A function to be called if request succeeds
				{
					$('#showResult').html(data);
					$('#send').removeAttr('disabled');
				}
				});
		});
		
		<?php
		/*
		 $xy = array();
		 foreach($data[1] as $v)
		 {
			 foreach($v as $v1)
			 {
				 $xy[] = $v1;
			 }
		 }
		 $sd = array_values($xy);	*/	 
		 ?>
		 
		  /*var availableTags = <?php //echo json_encode($sd); ?>;
		  console.log(availableTags);
			$( "#title" ).autocomplete({
			  source: availableTags,
			  select: function( event, ui ) {
				  var mp = ui.item.value;
				  }
			});*/
			
			
			$("input:file").change(function() 
			{
				$('.progress').hide();
				var file = this.files[0];
				var imagefile = file.type;
				var fileName = file.name;
				var match= ["image/jpeg","image/jpg"];
				if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
				{
					alert("Please select a proper format - jpg");				
					$('#imageFile').val("");
					return false;
				}			
				else
				{
						var reader = new FileReader();
						reader.readAsDataURL(this.files[0]);
						reader.onload = function (e) 
						{
						var image  = new Image();
						image.src = e.target.result;
						image.onload = function () 
							{
							//Determine the Height and Width.
							var height = this.height;
							var width = this.width;
							if(width!=800&&height!=578)
							{
								alert("Please upload correct size");
								$('#imageFile').val("");
								return false;	
							}
							else
							{
									$('#spin').show();
									var form1 = $('#form')[0];
									var formData = new FormData(form1);	
									$.ajax({
												xhr: function() {
												var xhr = new window.XMLHttpRequest();
												xhr.upload.addEventListener("progress", function(evt) {
												  if (evt.lengthComputable) {
													var percentComplete = evt.loaded / evt.total;
													percentComplete = parseInt(percentComplete * 100);
													$('#progress').show();
													$('#progress').css('background','#345678');
													$('#progress').css('width',percentComplete+'%');
													$('#percent').html(percentComplete+'%');
													console.log(percentComplete);
													if (percentComplete === 100) {
													}
												  }
												}, false);
												return xhr;
											  },
										
											url: '<?php echo APP_URL; ?>superadmin/send_notification/upload_process',
											type: 'POST',
											data: formData,
											processData: false,
											contentType: false,
											success: function (data) {
												console.log(data);
												$('#spin').hide();
												$('#imageShow').attr('src', e.target.result);
												$('#imageShow').attr('height', '150px');
												$('#imageVal').val(data);
											},
											error: function (data) {
											}
								   });	
								
							}
						}
					}
				}
	});
			
	});
</script>
<script type="text/javascript">
$('#form').bootstrapValidator({
        fields: {
			title: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Title'
                    }
                }
            },
			file: {
                validators: {
                    notEmpty: {
                        message: 'Please Select File'
                    }
                }
            },
            description: {
                validators: {
					notEmpty: {
                        message: 'Please Enter Description'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this );
		$('#spin1').show();
		    $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/send_notification/send_process',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				console.log(data);
				$('#spin1').hide();
				$('#notifySuccess').html('Notification sent successfully!');
            },
            error: function (data) {
            }
   });
});
</script>
<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
