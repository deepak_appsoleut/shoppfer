<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shoppfer | Deals</title>
  <?php include(APP_VIEW.'includes/top.php');?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo APP_CRM_PLUGIN; ?>jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo APP_CRM_DIST; ?>css/skins/_all-skins.min.css">
  <link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-white sidebar-mini">
<div class="wrapper">
<?php include(APP_VIEW.'includes/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $hotel = "active";
  include(APP_VIEW.'includes/nav.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Connected Hotels
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo APP_URL ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Connected Hotels</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes --> 
      <div class="row">
      	<div class="col-xs-12">
        <div class="box">
        	<div class="box-body">
            	<div class="row">
                    <div class="col-md-12">
                    <div id="map_canvas" style="height:350px;"></div>
                    </div>
                </div>
                <div id="myData">
                </div>
            </div>
        </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include(APP_VIEW.'includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include(APP_VIEW.'includes/bottom.php');?>
<!-- AdminLTE App -->
<script src="<?php echo APP_CRM_DIST; ?>js/app.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js" type="text/javascript"></script>
<script type="text/javascript">
function getData(id, category, sortingValue)
{
	$('#loading').show();
	$.ajax({
            url: '<?php echo APP_URL; ?>superadmin/connected_hotels/getLocationData',
            type: 'POST',
			data: {"tab" : id, "category" : category, "sortingValue" : sortingValue},
            success: function (data) {
				$('#loading').hide();
					$('#myData').html(data);
					initialize();
            },
            error: function (data) {
            }
   	});
}
    // Execute
	var markers = [];
    $(function() {
	
	<!----Modal Close Reload-------->   

/*	
	$.ajax({
            url: '<?php //echo APP_URL; ?>admin/connected_hotels/getLocationData',
            type: 'POST',
			data: {},
			processData: false,
			contentType: false,
            success: function (data) {
				$('#myData').html(data);
            },
            error: function (data) {
            }
   	});*/
		
		
        // Map options
     function initialize() {
		    var options = {
            zoom: 11,
            center: new google.maps.LatLng(28.6139391,77.2090212), // Centered
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            mapTypeControl: false
        };

        // Init map
        var map = new google.maps.Map(document.getElementById('map_canvas'), options);
        $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/connected_hotels/getLocation',
            success:function(data){
                var obj = JSON.parse(data);
                var totalLocations = obj.length;
				var image = '<?php echo APP_IMAGES; ?>hotel.png';
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(28.6139391,77.2090212),
                        map: map,
                        title: 'HotelOne',
						icon: image
                    });
				// Process multiple info windows
                    (function(marker, i) {
                        // add click event
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow = new google.maps.InfoWindow({
                                content: '<p>HotelOne</p><p>9811301718</p>'
                            });
                            infowindow.open(map, marker);
                        });
                    })(marker, i);
                for (var i = 0; i < totalLocations; i++) {
					var array = obj[i].gotData.split(',');
					var imageDisplay = {
					url: "<?php echo APP_IMAGES; ?>hotel.png", // url
					scaledSize: new google.maps.Size(50, 50)
				};
                    // Init markers
					var image = imageDisplay;
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(obj[i].hotel_details_latitude, obj[i].hotel_details_longitude),
                        map: map,
                        title: obj[i].hotel_details_name,
						icon: image
                    });
                    // Process multiple info windows
                    (function(marker, i) {
                        // add click event
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow = new google.maps.InfoWindow({
                                content: '<p><strong>'+obj[i].hotel_details_name+'</strong></p><p>'+obj[i].hotel_details_city+'</p>'
                            });
                            infowindow.open(map, marker);
                        });
                    })(marker, i);
					markers.push(marker);
                }
            }
        });
	}
	google.maps.event.addDomListener(window, 'load', initialize);
    });
	
	function myClick(id){
       google.maps.event.trigger(markers[id], 'click');
   }
   $(function()
	{
		getData('tab_1', '', '');
	});
	
$(document).on('click','.pagination-css',function(){
          var page_number = $(this).attr('id');
          $('#loading').show();
          $.ajax({
            url: '<?php echo APP_URL; ?>superadmin/connected_hotels/filter_result_hp',
            data: {'page_number' : page_number, "tab" : "tab_1", "category" : "", "sortingValue" : ""},
            type: 'POST',
            success: function (data) {
              $('#myData1').html('');
              $('#myData1').html(data);
              $('#loading').hide();
                  },
                  error: function (data) {
                  }
            });
          return false;
    });		
	
</script>

<!-- ChartJS 1.0.1 -->
<!-- AdminLTE for demo purposes -->
</body>
</html>
