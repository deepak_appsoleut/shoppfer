<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class invoice_mp extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function invoice_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__,$result);
	}
	function getMP()
	{
		$data = $this->model->process(__CLASS__, __FUNCTION__);
		for($i=0; $i<count($data); $i++)
		{
			?>
            <option value="<?php echo $data[$i]['mp_details_id']; ?>"><?php echo $data[$i]['mp_details_name']; ?></option>
            <?php
		}
	}
	function invoice_preview()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;		
	}
	
	function invoice_generate()
	{
		//$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__,NULL);		
	}
}
?>