<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class notification extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function notification()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function viewNotification()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}

}
?>