<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class connected_hotels extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function connected_hotels()
	{
		//$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, NULL);
	}
	function getLocation()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function getLocationData()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function filter_result_hp()
	{
		$result = $this->model->process('connected_hotels', __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
}
?>