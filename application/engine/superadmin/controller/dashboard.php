<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class dashboard extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function dashboard()
	{
		/*$result = $this->model->process(__CLASS__, __FUNCTION__);*/
		$this->view->render(__CLASS__, NULL);
	}
	function dashboard_data()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
}
?>