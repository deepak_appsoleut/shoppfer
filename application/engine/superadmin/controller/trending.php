<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class trending extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function trending()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__,$result);
	}
	function showTrend($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__,$result);
	}
	function removeTrending()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function addTrending()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
}
?>