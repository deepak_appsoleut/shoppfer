<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class deal_notifications extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function deal_notifications()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function deal_complete($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function deal_update($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, array($id));
		$this->view->render(__FUNCTION__, $result);
	}
	function dealStatus($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function dealCompleteStatus()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function dealUpdateCompleteStatus()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function dealStatusUpdate()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;		
	}
}
?>