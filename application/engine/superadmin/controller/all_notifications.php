<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class all_notifications extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function all_notifications()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function view_profile($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function view_update_profile($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function view_gallery_profile($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function deal_complete($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function verifyUpdateStatus()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function verifyCompleteStatus()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function verifyGalleryStatus()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function dealCompleteStatus()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function view_signup($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function verifySignupStatus()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
}
?>