<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class connected_mp extends controller {

	function __construct() {
		parent::__construct();
	}
	function connected_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, $result);
	}
	function getLocation()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function getLocationDataHotels()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__FUNCTION__, $result);
	}
	function getData()
	{
		$result = $this->model->process(__CLASS__, 'getSearchData');
		$this->view->render(__FUNCTION__, $result);
	}
	function merchant_view($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function deal_image_show_profile_update()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function deletePic()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}	
	function profileEdit()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function deal_view($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	}
	function disable_deal()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function enable_deal()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function delete_offer()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	} 
	function active_inactive()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function edit_deal($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	} 
	function deal_image_show()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function Create_a_offer()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function Images_add_from_gallery()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function editDeal()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function createDeal($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	} 
	function Create_deal()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	} 
	function revenue($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__, $result);
	} 
	function active_inactive_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function disable_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function offer_delete()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function edit_deal_shoutout()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
}
?>