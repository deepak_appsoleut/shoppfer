<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class login extends controller {

	function __construct() {
		parent::__construct();
		session::init();
	}
	
	function login()
	{
		$this->view->render(__CLASS__);
	}

	/* function for login process */
	function process()
	{
		$data = $this->model->process(__CLASS__, "checkLogin");
		if (!empty($data)) {
			$_SESSION['app_admin_user'] = $data[0];
			$return = json_encode($data);
		}
		else
		{
			$return = 0;
		}
		echo $return;
	}


	/* function for logout*/
	function deactivate()
	{
		unset($_SESSION['app_admin_user']);
		header("Location:".APP_URL."superadmin/login");
	}
}
?>