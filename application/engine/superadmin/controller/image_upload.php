<?php
/*
 * File: login.php
 * Created By: Deepak Bhardwaj
 */

class image_upload extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function image_upload()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__,$result);
	}
	function view_images($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__,$result);
	}
	function view_updated_images($id)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $id);
		$this->view->render(__FUNCTION__,$result);
	}
	function upload_process()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function upload_new_process()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function upload_status()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
}
?>