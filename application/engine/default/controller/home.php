<?php
/*
 * File: home.php
 * Created By: Deepak Bhardwaj
 */

class home extends controller {

	function __construct() {
		parent::__construct();
		session::init();
	}
	
	/*
	 * When dealing with default application -> there has to be `$default` parameter to be passed for managing landing website/app
	 */
	function home($default=0)
	{
		//$result = $this->model->process(__CLASS__, __FUNCTION__);
		$this->view->render(__CLASS__, null, $default);
	}
	/*
	function add_hotel()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	*/
	function add_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function verifyEmail($code)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $code);
		$this->view->render(__FUNCTION__, $result);
	}
}
?>