<?php
/*
 * File: home.php
 * Created By: Deepak Bhardwaj
 */

class signup extends controller {

	function __construct() {
		parent::__construct();
		session::init();
	}
	
	function signup()
	{
		$this->view->render(__CLASS__, __FUNCTION__);
	}
	
	function add_mp()
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__);
		echo $result;
	}
	function verifyEmail($code)
	{
		$result = $this->model->process(__CLASS__, __FUNCTION__, $code);
		$this->view->render(__FUNCTION__, $result);
	}
	
}
?>