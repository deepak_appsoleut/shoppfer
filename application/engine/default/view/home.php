<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->

<head>
<meta charset="UTF-8" />

<!-- Description -->
<title>Shoppfer</title>
<?php include('includes/top.php'); ?>
<style>
#logo {
	z-index:9 !important;
}
#logo span {
	margin-right: 3.0rem;
}
.swiper-slide article p {
	font-style:normal;
}
#timeline-bar {
	background:#ef1428 !important;
}
</style>
</head>

<body class="preload" data-current-section="0">
<div id="wrapper">
  <div id="borders">
    <div class="border hor top"></div>
    <div class="border hor bot"></div>
    <div class="border vert left"></div>
    <div class="border vert right"></div>
  </div>
  <div id="illustration-01" style="background-image:url(<?php echo APP_IMAGES; ?>bg-3.png); background-repeat:no-repeat; background-position:top center"> </div>
  <div id="stripe"></div>
  <div id="video-container">
    <video id="video" preload="auto" controls onplay="this.controls=false">
      <source src="<?php echo APP_IMAGES; ?>shoppfer-video.mp4" type="video/mp4">
      <source src="<?php echo APP_IMAGES; ?>shoppfer-video.webm" type="video/webm">
    </video>
  </div>
  <div id="timeline-ctn">
    <div id="timeline-track">
      <div id="timeline-bar"></div>
    </div>
    <!-- <div id="section-number"></div> --> 
  </div>
  <header id="header" class="nav-container noselect">
    <h1 id="logo" style="color:#ef1428; font-size:80px;"> <img src="<?php echo APP_IMAGES; ?>png-logo.png" /> </h1>
    <nav id="main-nav">
      <button class="nav-btn up" disabled>Go up<span></span></button>
      <ul>
        <li><a href="#search">Search</a></li>
        <li><a href="#find">Find</a></li>
        <li><a href="#explore">Explore</a></li>
        <li><a href="#suggest">Suggest</a></li>
        <li><a href="#newsletter">Newsletter</a></li>
      </ul>
      <button class="nav-btn down">Go down<span></span></button>
    </nav>
    <div id="sub-nav">
      <ul>
        <!--<li style="border:none;background-color:#5ad0b6; padding-left:20px; padding-right:20px"><a href="#" style="background-image:none; border:none; font-style:normal; color:#fff; font-size:20px; text-transform:uppercase; font-family:'calibri',Arial, Helvetica, sans-serif" target="_blank">Merchant Signup</a></li>-->
        <li style="border:none;"><a href="<?php echo APP_URL; ?>admin_mp/login"  id="merchant" style="background-image:none; border:none; font-style:normal; color:#000;      font-size: 10px;line-height: 5.5rem;text-transform: uppercase;letter-spacing: .3rem; font-family:Avenir-Next, 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif">Merchant Signup</a></li>
        <li style="border:none"><a href="" style="background-image:none; border:none" target="_blank"><img src="<?php echo APP_IMAGES; ?>googleplay.png" /></a></li>
      </ul>
    </div>
  </header>
  <!-- /#header -->
  
  <main id="content" role="main">
    <section id="home">
      <p class="tagline">Shop An <em>Offer</em></p>
    </section>
    <div id="sections-list-container">
      <div class="swiper-container">
        <ol id="sections-list" class="swiper-wrapper">
          <li class="swiper-slide">
            <section id="search">
              <figure> <img src="http://www.ginventory.co/img/mockup/ginventory-screenshot-find.jpg" tppabs="http://www.ginventory.co/img/mockup/ginventory-screenshot-find.jpg" alt=""> </figure>
              <article style="width:300px">
                <h2 style="color:#ef1428; text-align:right; width:250px">DISCOVER</h2>
                <p style="width:250px">Awesome deals and FREE Coupons<br>
                  from your Favourite Outlets</p>
              </article>
            </section>
          </li>
          <li class="swiper-slide">
            <section id="find">
              <figure> <img src="http://www.ginventory.co/img/mockup/ginventory-screenshot-search.jpg" tppabs="http://www.ginventory.co/img/mockup/ginventory-screenshot-search.jpg"  alt=""> </figure>
              <article style="width:300px">
                <h2 style="color:#ef1428; text-align:right; width:250px">EXPLORE</h2>
                <p style="width:300px; padding-right:58px">Exciting Offers around you<br>
                  Dine in the best, Relax in a Spa or GO Crazy Shopping</p>
              </article>
            </section>
          </li>
          <li class="swiper-slide">
            <section id="explore">
              <figure> <img src="http://www.ginventory.co/img/mockup/ginventory-screenshot-find.jpg" tppabs="http://www.ginventory.co/img/mockup/ginventory-screenshot-find.jpg" alt=""> </figure>
              <article style="width:300px">
                <h2 style="color:#ef1428; text-align:right; width:250px">CHOOSE</h2>
                <p style="width:250px">The deal you love,<br>
                  to avail it When you want!</p>
              </article>
            </section>
          </li>
          <li class="swiper-slide">
            <section id="suggest">
              <figure> <img src="http://www.ginventory.co/img/mockup/ginventory-screenshot-search.jpg" tppabs="http://www.ginventory.co/img/mockup/ginventory-screenshot-search.jpg" alt=""> </figure>
              <article style="width:300px">
                <h2 style="color:#ef1428; text-align:right; width:250px">AVAIL</h2>
                <p style="width:250px">The deal you want and make your<br>
                  chosen experience a pleasure!<br>
                  Less Pay, Enjoy More</p>
              </article>
            </section>
          </li>
        </ol>
        <!-- /.swiper-wrapper --> 
        
        <!--<div id="slider-images">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>--> 
        
      </div>
      <!-- /.swiper-container -->
      
      <ul id="slider-nav">
        <li><a href="#search">Search</a></li>
        <li><a href="#find">Find</a></li>
        <li><a href="#explore">Explore</a></li>
        <li><a href="#suggest">Suggest</a></li>
      </ul>
    </div>
    <!-- /#sections-list-container -->
    
    <section id="download">
      <article style="left: calc(55% - 164px);">
        <h2>Now Available for Download</h2>
        <p>Now Available for Download</p>
        <ul class="download-list">
          <li><a href="" class="download-btn" style="background-image:url(<?php echo APP_IMAGES; ?>googleplay.png); background-position: center 50%; margin-left:10px" target="_blank"></a></li>
        </ul>
      </article>
      <div id="illustration-02"></div>
    </section>
  </main>
  <!-- /#content -->
  
  <footer id="footer">
    <h3>SHOPPFER</h3>
    <p class="footer-note"><span style="color:#000; font-size:10px">ABOUT</span><span style="margin-left:20px; color:#000; font-size:10px"><a href="mailto:info@shoppfer.com">CONTACT</a></span></p>
  </footer>
</div>
<!-- /#wrapper --> 

<script src="<?php echo APP_JS; ?>jquery1.js"></script> 
<!--<script>
$(function()
{
var v = document.getElementById("foo");
v.addEventListener( "loadedmetadata", function (e) {
   alert(this.videoHeight +',' +this.videoWidth)
        //height = this.videoHeight;
}, false );
});
</script>-->

</body>
</html>