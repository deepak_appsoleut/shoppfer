<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Shoppfer Signup</title>
<?php include('includes/top-inner.php'); ?>
<link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
</head>
<body id="page-top" class="index">
<!-- Navigation -->
<?php include('includes/nav.php'); ?>
<!-- Header -->
<div class="container" style="margin-top:150px">
<div class="row text-center">
  <div class="col-md-12">
    <h2>Register</h2>
  </div>
</div>
<form id="form1">
  <div class="row text-center">
    <div class="col-sm-4 col-md-4 col-sm-offset-2">
      <div class="form-group">
        <label class="pull-left">Business Name</label>
        <input name="business" placeholder="Please Enter the Name of Your Business" class="form-control input-sm" type="text">
      </div>
    </div>
    <div class="col-sm-4 col-md-4">
      <div class="form-group">
        <label class="pull-left">Email Address</label>
        <input name="email" placeholder="Please Enter a valid Email Address" class="form-control input-sm" type="email">
      </div>
    </div>
  </div>
  <div class="row text-center" style="margin-top:10px">
    <div class="col-sm-4 col-md-4 col-sm-offset-2">
      <div class="form-group">
        <label class="pull-left">Password</label>
        <input name="password" placeholder="Create a password" class="form-control input-sm" type="password">
      </div>
    </div>
    <div class="col-sm-4 col-md-4">
      <div class="form-group">
        <label class="pull-left">Confirm Password*</label>
        <input name="cnf_password" placeholder="Confirm Password" class="form-control input-sm" type="password">
      </div>
    </div>
  </div>
  <div class="row text-center">
    <div class="col-sm-3 col-md-3 col-sm-offset-7">
    	<button class="btn redBtn btn-block"type="submit">Let's Go!</button>
    </div>
    <div class="col-sm-5 col-md-6 col-sm-offset-5">
        <span class="error"></span>
    </div>
  </div>
</form>
  </div>
<?php include('includes/footer.php'); ?>
<!-- jQuery --> 
<!-- Contact Form JavaScript --> 
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script type="text/javascript">
$('#form1').bootstrapValidator({
        fields: {
			business: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Business Name!'
                    }
                }
            },
			email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Email Address!'
                    }
                }
            },
			password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Password'
                    },
					stringLength: {
                        min: 6,
                        message: 'Password must be atleast 6 characters long!'
                    }
                }
            },
            cnf_password: {
                validators: {
					identical: {
						field: 'password',
						message: 'Passwords Do Not Match, Please Try Again!'
					},
					notEmpty: {
                        message: 'Please conform your password'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>default/signup/add_mp',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data!=0)
				{
					$('.error').html("Thanks for Registering with Shoppfer! Please check your registered email to verify it.");
					$('.error').fadeIn();
				}
				else
				{
					$('.error').html("*This email is already exist with same Marketing partner!");
					$('.error').fadeIn();
				}
            },
            error: function (data) {
            }
   });
});
</script> 
<script type="text/javascript">
$(function() {
<!----Modal Close Reload-------->   
$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal','.modal', function () {
$(this).removeData('bs.modal');
});
});
</script> 
<!-- Custom Theme JavaScript -->
</body>
</html>



