<!-- Viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<!-- Detect IE 10+ -->
		<script>
		(function detectIE() {
			var ua 		= window.navigator.userAgent;
			var version = -1;
			
			var msie = ua.indexOf('MSIE ');
			if (msie > 0) { version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10); }
			var trident = ua.indexOf('Trident/');
			if (trident > 0) { var rv = ua.indexOf('rv:'); version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10); }
			var edge = ua.indexOf('Edge/');
			if (edge > 0) { version = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10); }
			if (version != -1) { document.documentElement.className += ' ie' + version; }
		})();
		</script>

		<!-- CSS -->
        <link rel="stylesheet" href="<?php echo APP_CSS; ?>style.css">
		<!-- Icons -->
		<link rel="shortcut icon" href="favicon.ico" tppabs="http://www.ginventory.co/img/app-icons/favicon.ico">
		