<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Bookmydeal</title>
<?php include('includes/top.php'); ?>
<link href="<?php echo APP_CSS; ?>bootstrapValidator.min.css" rel="stylesheet">
</head>
<body id="page-top" class="index">
<!-- Navigation -->
<?php include('includes/nav.php'); ?>
<!-- Header -->
<header>
  <div class="container">
    <div class="intro-text">
      <div class="intro-lead-in">Welcome To Bookmydeal!</div>
      <div class="intro-heading">It's Nice To Meet You</div>
      <a href="#" class="page-scroll btn btn-xl">Tell Me More</a> </div>
  </div>
</header>
<!--<div class="portfolio-modal modal fade" id="forHotels" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"> </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body"> 
            
            <h4 style="border-bottom:1px solid #ccc; padding-bottom:10px;">Already a Partner? <a href="<?php //echo APP_URL; ?>admin_hp">Sign In</a></h4>
            <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success : </b> <span></span></div>
      <div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning : </b> <span></span></div>
            <form id="form" method="post">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Business Name*</label>
                        <input name="business" placeholder="Business Name" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Email Address</label>
                        <input name="email" placeholder="Email Address" class="form-control input-sm" type="email">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Password*</label>
                        <input name="password" placeholder="Password" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Confirm Password*</label>
                        <input name="cnf_password" placeholder="Confirm Password" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
              <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                  <button class="btn-warning btn" style="width:100%" type="submit">SIGN UP</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>-->
<div class="portfolio-modal modal fade" id="forMP" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"> </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body"> 
            <!-- Project Details Go Here -->
            <h4 style="border-bottom:1px solid #ccc; padding-bottom:10px;">
            Already a Marketing Partner? <a href="<?php echo APP_URL; ?>admin_mp">Sign In</a>
            </h4>
             <div class="alert alert-success alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success : </b> <span></span></div>
      		<div class="alert alert-warning alert-dismissable" style="display:none"> <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning : </b> <span></span></div>
            <form id="form1" method="post">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Business Name*</label>
                        <input name="business" placeholder="Business Name" class="form-control input-sm" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Email Address</label>
                        <input name="email" placeholder="Email Address" class="form-control input-sm" type="email">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Password*</label>
                        <input name="password" placeholder="Password" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="pull-left">Confirm Password*</label>
                        <input name="cnf_password" placeholder="Confirm Password" class="form-control input-sm" type="password">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="marTop10"></div>
              <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                <input name="" placeholder="" class="" type="hidden">
                <input name="" placeholder="" class="" type="hidden">
                  <button class="btn-warning btn" style="width:100%" type="submit">SIGN UP</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
<!-- jQuery --> 
<!-- Contact Form JavaScript --> 
<script src="<?php echo APP_JS; ?>bootstrapValidator.min.js"></script> 
<script type="text/javascript">
/*
$('#form').bootstrapValidator({
        fields: {
			business: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Business Name!'
                    }
                }
            },
			email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Email Address!'
                    }
                }
            },
			password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Password'
                    },
					stringLength: {
                        min: 6,
                        message: 'Password must be atleast 6 characters long!'
                    }
                }
            },
            cnf_password: {
                validators: {
					identical: {
						field: 'password',
						message: 'The password and its confirm password are not the same!'
					},
					notEmpty: {
                        message: 'Please Enter Confirm Password'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>default/home/add_hotel',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data)
				{
					$('.alert-warning').hide();
					$('.alert-success').show();
					var string = "Thanks for the regsitration! <a target='_blank' href='<?php echo APP_URL."default/home/verifyEmail/"; ?>"+data+"'>Click Here</a> for verify";
					$('.alert-success span').html(string);
				}
				else
				{
					$('.alert-success').hide();
					$('.alert-warning').show();
					$('.alert-warning span').html("This email is already exist with same Hotel partner!");
				}
            },
            error: function (data) {
            }
   });
});
*/
$('#form1').bootstrapValidator({
        fields: {
			business: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Business Name!'
                    }
                }
            },
			email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Email Address!'
                    }
                }
            },
			password: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Password'
                    },
					stringLength: {
                        min: 6,
                        message: 'Password must be atleast 6 characters long!'
                    }
                }
            },
            cnf_password: {
                validators: {
					identical: {
						field: 'password',
						message: 'The password and its confirm password are not the same!'
					},
					notEmpty: {
                        message: 'Please Enter Confirm Password'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function (e) {
        e.preventDefault();
        var formData = new FormData( this )
		    $.ajax({
            url: '<?php echo APP_URL; ?>default/home/add_mp',
            type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
            success: function (data) {
				if(data!=0)
				{
					$('.alert-warning').hide();
					$('.alert-success').show();
					var string = "Thanks for the regsitration! Please check your registered email for verification.";
					$('.alert-success span').html(string);
				}
				else
				{
					$('.alert-success').hide();
					$('.alert-warning').show();
					$('.alert-warning span').html("This email is already exist with same Marketing partner!");
				}
            },
            error: function (data) {
            }
   });
});
</script>
<script type="text/javascript">
$(function() {
<!----Modal Close Reload-------->   
$('.modal').on('hidden.bs.modal', function () {
  if($('.modal .alert-success').css('display') == 'block')
  {
    location.reload();
  }
}); 

<!-- modal hide and clear data in it -->
$('body').on('hidden.bs.modal','.modal', function () {
$(this).removeData('bs.modal');
});
});
</script>
<!-- Custom Theme JavaScript --> 
</body>
</html>
