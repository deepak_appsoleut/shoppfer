<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Shoppfer VerifyEmail</title>
<?php include('includes/top-inner.php'); ?>
</head>
<body id="page-top" class="index">
<!-- Navigation -->
<?php include('includes/nav.php'); ?>
<!-- Header -->
<?php 
if($data['success']==true)
{
?>
<div class="container" style="margin-top:200px">
<div class="row text-center">
    <div class="col-sm-8 col-md-8 col-sm-offset-2">
      <h3 style="font-size:25px"><?php echo $data['message']; ?></h3>
      <h5>Please click here to Login.</h5>
    </div>
    <div class="col-sm-3 col-md-3 col-sm-offset-4">
    	<a class="btn redBtn btn-block" style="background-color:#ef1428;margin-left:50px;" href="<?php echo APP_URL; ?>admin_mp/login" type="submit">Let's Start Now!</a>
    </div>
  </div>
</div>
<?php } else { ?>
<div class="container" style="margin-top:200px">
<div class="row text-center">
    <div class="col-sm-8 col-md-8 col-sm-offset-2">
      <h3 style="font-size:25px"><?php echo $data['message']; ?></h3>
    </div>
    <div class="col-sm-3 col-md-3 col-sm-offset-4">
    	<a class="btn redBtn btn-block" style="background-color:#ef1428;margin-left:50px;" href="<?php echo APP_URL; ?>admin_mp/contact" type="submit">Contact</a>
    </div>
  </div>
</div>
<?php } ?>
<?php include('includes/footer.php'); ?>
</body>
</html>
