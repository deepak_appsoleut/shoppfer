<?php
/*
 * File: home.php
 * Created By: Deepak Bhardwaj
 */

class home_model {
	
	function home()
	{
		/*$param = array("status"=>1);
		$data[0] = $GLOBALS["db"]->select("SELECT role_id, role_name FROM role WHERE role_type_id = 1 AND role_status =:status", $param);
		$data[1] = $GLOBALS["db"]->select("SELECT role_id, role_name FROM role WHERE role_type_id = 2 AND role_status =:status", $param);	
		return $data;*/
	}
	function add_mp()
	{
		$math 	  = new math();
		$password = $math->hash("sha1", $_POST['password'], APP_SEC_KEY);
		$link 	  = $math->hash("sha1", rand('00000', '99999').time(), APP_SEC_KEY);
		$mp_Array = array("mp_details_name"=>$_POST['business'],"mp_details_email"=>$_POST['email'],"mp_details_password"=>$password, "mp_details_role_id"=>1, "mp_user_code"=>$link, "mp_process"=>1);
		
		$insert_temp 	= $GLOBALS["db"]->lastInsertNow("mp_details_temp", $mp_Array, "mp_created_on");
		
		$mp_Ar = array("mp_details_id"=>$insert_temp,"mp_details_name"=>$_POST['business'],"mp_details_email"=>$_POST['email'],"mp_details_password"=>$password, "mp_details_role_id"=>1);
		
		$insert   = $GLOBALS["db"]->lastInsertNow("mp_details", $mp_Ar, "mp_created_on");
		
		//code for making email body 
			$body = $math->registerTemplate($business_name = $_POST['business'], $link);
			
			$GLOBALS["db"]->mailSend($_POST['email'], $subject = 'SHOPPFER - Activate your account', $body, $from = "");
		
		//print_r($insert_temp);
		//print_r($insert);
		
		//echo $insert;
		if($insert_temp>0)
		{
			//-----START#Admin-Notification Table instertion-----//
			/*
			*/
			$type		=	"signup";
			$fields		=	"";
			$json_str	= '';
			//an_process
			$notifiy_array	= array("an_mp_user_id"=>$insert_temp,"an_process"=>"signup","an_text"=>$json_str,"an_notify_from"=>"MP");
			$insert_notifiy = $GLOBALS["db"]->lastInsertNow("admin_notification", $notifiy_array,"an_created_time");
			//print_r($insert_notifiy);
			//-----END#Admin-Notification Table instertion-----//
			return $link;
		}
		else
		{
			return 0;	
		}		
	}
	//============================== Function to verify Email ===================================//
	function verifyEmail($code)
	{
		$param 		= array("code"=>$code);
		$response 	= array("success"=>true);
		$data = $GLOBALS["db"]->select("SELECT mp_details_id,mp_verify FROM mp_details_temp WHERE mp_user_code =:code LIMIT 1", $param);
		//print_r($data);
		if(count($data)>0)
		{
			if($data[0]['mp_verify']==0)
			{
				$paramUpdate 	= array("mp_verify"=>1, "mp_status"=>1);
				$condition 		= array("mp_details_id"=>$data[0]['mp_details_id']);
				//$data_old 	= $GLOBALS["db"]->updateRows("mp_details", $paramUpdate, $condition);
				
				$data 		= $GLOBALS["db"]->updateRows("mp_details_temp", $paramUpdate, $condition);
				if($data>0)
				{
					$response['message'] = "Email ID verified successfully";
				}
				else
				{
					$response['success'] = 0;
					$response['message'] = "There is some error while verification.";
				}
			}
			else
			{
				$response['success'] = 0;
				$response['message'] = "You have already verified your mail id";
			}
		}
		else
		{
			$response['success'] = 0;
			$response['message'] = "There is some error while verification. Your verification code is wrong. Please contact our support team!";
		}
		return $response;
	}
}
?>