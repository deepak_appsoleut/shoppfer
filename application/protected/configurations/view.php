<?php
/*
 * File: view.php
 * Created By: Deepak Bhardwaj
 */

class view {
	public function render($name = null, $data = null, $default = 0, $message = null)
	{
		if ($name != null) {
			if($default == 0)
			{
				require APP_VIEW . $name . '.php';
			}
			elseif($default == 1)
			{
				require APP_DEFAULT . "view/error.php";
			}
			elseif($default == 2)
			{
				require APP_DEFAULT."view/" . $name . '.php';
			}
		}
	}
}
?>