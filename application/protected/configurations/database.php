<?php
/*
 * File: database.php
 * Created By: Deepak Bhardwaj
 */

/**
 * GUIDELINES: While creating database and its tables keep few things in mind.
 * 1. Every table should have a "status", "owner", "created_on", "modified_on" field.
 *    And it has to be the last field of the table always, if required in table.
 * 2. Every fieldname need to be followed by its tablename and an underscore.
 *    Like TABLE:employee should have FIELD:employee_name
 */

class database extends PDO { // This will inherit all methods of PDO into "database" class.
	function __construct() {
		//parent::__construct('mysql:host=localhost;dbname=shoppfer', 'root', ''); //for localhost
		parent::__construct('mysql:host=192.168.1.101;dbname=shoppfer', 'root6', ''); //for dev
		//parent::__construct('mysql:host=localhost;dbname=shoppfer', 'root', ''); ////for live
		//parent::__construct('mysql:host=localhost;dbname=adsappso_shoppfer_new', 'adsappso_shoppfe', 'Shoppfer#123', array(
    	//PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone = '+5:30'"));
	}

	/**
	 * select
	 * @param string $sql An SQL string
	 * @param associative array $array Paramters to bind
	 * @param constant $fetchMode A PDO Fetch mode
	 * @return mixed
	 **/
	public function select($sql, $array = array(), $fetchMode = parent::FETCH_ASSOC)
	{
		$sth = $this->prepare($sql);
		foreach ($array as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
		$sth->execute();
        //print_r($sth->errorInfo());
		return $sth->fetchAll($fetchMode);
	}

	/**
	 * insert
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * @return "true" for success and "false" for failure
	 */
	public function insert($table, $data)
	{
		ksort($data);
		$fieldNames = implode('`, `', array_keys($data));
		$fieldValues = ':' . implode(', :', array_keys($data));
		$sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		$Status = $sth->execute();
		//return print_r($sth->errorInfo());
		if($Status == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * update
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * @param string $where the WHERE query part
	 * @param string $now the NOW() function
	 */
	public function insertNow($table, $data, $now)
	{
		ksort($data);
		$fieldNames = implode('`, `', array_keys($data));
		$fieldNames = $fieldNames.'`, `'.$now;
		$fieldValues = ':' . implode(', :', array_keys($data));
		$fieldValues = $fieldValues.', NOW()';
		$sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		$Status = $sth->execute();
		//return json_encode($sth->errorInfo());
		if($Status == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * insert
	 * @param string $table A name of table to insert into
	 * @param string $data An array
	 */
	public function bulkinsert($table, $fields, $data)
	{
		$totalData = count($data);
		$total = count(explode(',', $fields));
		$fieldValues = null;
		$TotalLoop = $totalData / $total;
		for($K = 0; $K<$TotalLoop;$K++)
		{
			$fieldValues .= '(';
			for($i=0;$i<$total;$i++)
			{
				if($i==($total-1))
				{
					$fieldValues .= '?';
				}
				else
				{
					$fieldValues .= '?,';
				}
			}
			if($K == ($TotalLoop-1))
			{
				$fieldValues .= ")";
			}
			else
			{
				$fieldValues .= "),";
			}
		}
		$sth = $this->prepare("INSERT INTO ".$table." (".$fields.") VALUES ".$fieldValues);
		
		for($M=0; $M<$totalData; $M++)
		{
			$sth->bindValue($M+1, $data[$M]);
		} 
		$Status = $sth->execute();
		if($Status == true)
		{
			return "true";
		}
		else
		{
			return $sth->errorInfo();
		}
	}

	/**
	 * update
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * @param string $where the WHERE query part
	 */
	public function update($table, $data, $where)
	{
		ksort($data);
		$fieldDetails = NULL;
		$Wheredetail = NULL;
		foreach($data as $key=> $value) {
			$fieldDetails .= "`$key`=:$key,";
		}
		$fieldDetails = rtrim($fieldDetails, ',');

		foreach($where as $key=> $value) {
			$Wheredetail .= "`$key`=:$key AND ";
		}
		
		$Wheredetail = rtrim($Wheredetail, 'AND ');
		$sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $Wheredetail");
		
		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
		foreach ($where as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		$Status = $sth->execute();
		
		//return print_r($sth->errorInfo());
		
		if($Status == true)
		{
			return "true";
		}
		else
		{
			return "false";
		}
	}
	
	/**
	 * update
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * @param string $where the WHERE query part
	 */
	public function updateRows($table, $data, $where)
	{
		ksort($data);
		$fieldDetails = NULL;
		$Wheredetail = NULL;
		foreach($data as $key=> $value) {
			$fieldDetails .= "`$key`=:$key,";
		}
		$fieldDetails = rtrim($fieldDetails, ',');

		foreach($where as $key=> $value) {
			$Wheredetail .= "`$key`=:$key AND ";
		}
		
		$Wheredetail = rtrim($Wheredetail, 'AND ');
		$sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $Wheredetail");
		
		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
		foreach ($where as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		$Status = $sth->execute();
		//return print_r($sth->errorInfo());
		$count = $sth->rowCount();
		
		return $count;
	}

	public function validate($email)
	{
		$AssociativeArray= array('email'=>$email);
		$validate1 = self::select("SELECT u_email FROM users WHERE u_email = :email", $AssociativeArray);
		$validate2 = self::select("SELECT email FROM configuration");
		$validemail = $validate2[0]['email'];
		$MyErrors = array('email'=>0);
		foreach($validate1 as $key=>$value)
		{
			if(($validate1[$key]['u_email'] == $email) && ($validate1[$key]['u_email']!= $validemail))
			{
				$MyErrors['email'] = 1;
			}
		}
		return $MyErrors;
	}
	public function editvalidate($uid, $email)
	{
		$AssociativeArray= array('uid'=>$uid);
		$AssociativeArray1= array('uid'=>$uid,'email'=>$email);
		$validate1 = self::select("SELECT u_id, u_email FROM users WHERE u_id = :uid", $AssociativeArray);
		$MyErrors = array('uid'=>0, 'email'=>0);
		foreach($validate1 as $key=>$value)
		{
			if($validate1[$key]['u_email'] == $email)
			{
				$MyErrors['email'] = 2;
			}
		}
		if(($MyErrors['email']==0))
		{
			$validate2 = self::select("SELECT u_email FROM users WHERE u_email = :email AND u_id!=:uid", $AssociativeArray1);
			$validate3 = self::select("SELECT email FROM configuration");
			$validemail = $validate3[0]['email'];
			foreach($validate2 as $key=>$value)
			{
				if($email!= $validemail)
				{
					if($validate2[$key]['u_email'] == $email)
					{
						$MyErrors['email'] = 1;
					}
				}
			}
			return $MyErrors;
		}
		else
		{
			return $MyErrors;
		}
	}
	/**
	 * delete
	 *
	 * @param string $table
	 * @param $where is associative array
	 * @param integer $limit
	 * @return integer Affected Rows
	 */
	public function deleteQuery($table, $where, $limit = 1)
	{
		$Wheredetail = null;
		/*foreach($where as $key=> $value) {
			$Wheredetail .= "`$key`=:$key,";
		}
		$Wheredetail = rtrim($Wheredetail, ',');*/
		
		foreach($where as $key=> $value) {
			$Wheredetail .= "`$key`=:$key AND ";
		}
		$Wheredetail = rtrim($Wheredetail, 'AND ');
		$stmt = $this->prepare("DELETE FROM $table WHERE $Wheredetail LIMIT $limit");

		foreach ($where as $key => $value) {
			$stmt->bindValue(":$key", $value);
		}

		$stmt->execute();
		
		//return print_r($stmt->errorInfo());die;
		
		$count = $stmt->rowCount();

		if($count == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//for bulk delete
	public function deleteBulkQuery($table, $where)
	{
		$Wheredetail = null;
		foreach($where as $key=> $value) {
			$Wheredetail .= "$key IN($value)";
		}
		$stmt = $this->prepare("DELETE FROM $table WHERE $Wheredetail");
		$stmt->execute();
		//return print_r($stmt->errorInfo());
		$count = $stmt->rowCount();
		if($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
     * insert & get id
     * @param string $table A name of table to insert into
     * @param string $data An associative array
     * @param string $column Primary key column
     * @return "true" for success and "false" for failure
     */
    public function insertReturnId($table, $data,$column)
    {
        ksort($data);
        $fieldNames = implode('`, `', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));
        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        $Status = $sth->execute();
        if($Status == true)
        {
            return array('status'=>true,'last_insert_id'=>$this->lastInsertId($column));
        }
        else
        {
            return array('status'=>false);
        }
    }

    // for getting last insert id
	public function lastInsertNow($table, $data, $now)
	{
		ksort($data);
		$fieldNames = implode('`, `', array_keys($data));
		$fieldNames = $fieldNames.'`, `'.$now;
		$fieldValues = ':' . implode(', :', array_keys($data));
		$fieldValues = $fieldValues.',NOW()';
		$sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
		
		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
		
		$Status = $sth->execute();
		//return print_r($sth->errorInfo());
		$InsertId = $this->lastInsertId();
		if($Status == true)
		{
			return $InsertId;
		}
		else
		{
			return false;
		}
	}
	//function to check username is exist or not
	public function checkUsername($username)
    {
		$AssociativeArray = array("username"=>$username);
		$validate = self::select("SELECT astrologer_id FROM astrologer WHERE astrologer_username = :username", $AssociativeArray);
		if(count($validate)>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//function to email send
	public function mailSend($to, $subject, $email_body, $from)
	{
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'From: SHOPPFER support@shoppfer.com' . "\r\n" ;
		$headers .= "Reply-To: <".$to.">\r\n";
		$headers .= "mailed-by: "."\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		@mail($to,  $subject, $email_body, $headers);
	}
	public function uploadImage($files,$name,$folder)
	{
		$path = APP_UPLOADS.$folder.'/';
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		$imagename = $files['name'];
		$size = $files['size'];
		$text = strrpos(strtolower($imagename),".");
			if (!$text)
			{
				$ext = '';
			}
			else
			{
				$len = strlen(strtolower($imagename)) - $text;
				$ext = substr($imagename,$text+1,$len);
			}
		if(in_array($ext,$valid_formats))
		{
				$actual_image_name = $name.time()."-".$imagename;
				$uploadedfile = $files['tmp_name'];
		}
		else
		{
			return "false";	
		}
		if(move_uploaded_file($uploadedfile, $path.$actual_image_name))
		{
			return $actual_image_name;
		}
		else
		{
			return "false";
		}
	}
	public function uploadImageCRM($files,$name,$folder)
	{
		$path = APP_CRM_UPLOADS.$folder.'/';
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		$imagename = $files['name'];
		$size = $files['size'];
		$text = strrpos(strtolower($imagename),".");
			if (!$text)
			{
				$ext = '';
			}
			else
			{
				$len = strlen(strtolower($imagename)) - $text;
				$ext = substr($imagename,$text+1,$len);
			}
		if(in_array($ext,$valid_formats))
		{
				$actual_image_name = $name.time()."-".$imagename;
				$uploadedfile = $files['tmp_name'];
		}
		else
		{
			return "false";	
		}
		if(move_uploaded_file($uploadedfile, $path.$actual_image_name))
		{
			return $actual_image_name;
		}
		else
		{
			return "false";
		}
	}
	public function uploadMultipleImageCRM($files,$name,$folder)
	{
		$path = APP_CRM_UPLOADS.$folder.'/';
		$array = array();
		foreach($files['name'] as $key=>$val)
		{
			$imagename = $files['name'][$key];
			$size = $files['size'][$key];
			$text = strrpos(strtolower($imagename),".");
				if (!$text)
				{
					$ext = '';
				}
				else
				{
					$len = strlen(strtolower($imagename)) - $text;
					$ext = substr($imagename,$text+1,$len);
				}
			$actual_image_name = $name.time()."-".$imagename;
			$uploadedfile = $files['tmp_name'][$key];
			if(move_uploaded_file($uploadedfile, $path.$actual_image_name))
			{
				array_push($array, $actual_image_name);
			}
		}
		return $array;
	}
	public function uploadMultipleImage($files,$name,$folder)
	{
		$path = APP_UPLOADS.$folder.'/';
		$array = array();
		foreach($_FILES['images']['name'] as $key=>$val)
		{
			$imagename = $files['name'][$key];
			$size = $files['size'][$key];
			$text = strrpos(strtolower($imagename),".");
				if (!$text)
				{
					$ext = '';
				}
				else
				{
					$len = strlen(strtolower($imagename)) - $text;
					$ext = substr($imagename,$text+1,$len);
				}
			$actual_image_name = $name.time()."-".$imagename;
			$uploadedfile = $files['tmp_name'][$key];
			if(move_uploaded_file($uploadedfile, $path.$actual_image_name))
			{
				array_push($array, $actual_image_name);
			}
		}
		return $array;
	}
	public function getUserIp()
	{
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
	
		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}
		return $ip;
	}
	
	//function to add in delete history
	public function deleteHistory($user_id, $item,  $field)
    {
		$AssociativeArray = array("username"=>$username);
		$param = array("dh_user_id"=>$user_id, "dh_field"=>$field, "dh_item"=>$item);
		$validate = self::insert("delete_history", $param);
		if(count($validate)>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//function to find distance 
	public function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
		  $theta = $lon1 - $lon2;
		  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		  $dist = acos($dist);
		  $dist = rad2deg($dist);
		  $miles = $dist * 60 * 1.1515;
		  $unit = strtoupper($unit);
		  return ($miles * 1.609344);
	}
	//function to convert image
	// still not used
	public function convertJpg($files,$name,$folder)
	{
		$path = APP_CRM_UPLOADS.$folder.'/';
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		$imagename = $files['name'];
		$size = $files['size'];
		$text = strrpos(strtolower($imagename),".");
			if (!$text)
			{
				$ext = '';
			}
			else
			{
				$len = strlen(strtolower($imagename)) - $text;
				$ext = substr($imagename,$text+1,$len);
			}
		if(in_array($ext,$valid_formats))
		{
				$actual_image_name = $name.time()."-".$imagename;
				$uploadedfile = $files['tmp_name'];
		}
		else
		{
			return "false";	
		}		
		if(move_uploaded_file($uploadedfile, $path.$actual_image_name))
		{
			
			$destinationPath = $path . basename($actual_image_name, $ext) . 'jpg';
			if(($ext=="PNG")||($ext=="png"))
			{
				$source = imagecreatefrompng($path . $actual_image_name);
				$img = imagejpeg($source, $destinationPath, 75);
				unlink($path . $actual_image_name);
			}
			else if(($ext=="GIF")||($ext=="gif"))
			{
				$source = imagecreatefromgif($path . $actual_image_name);
				$img = imagejpeg($source, $destinationPath, 75);
				unlink($path . $actual_image_name);
			}
			else
			{
				$source = imagecreatefromjpeg($path . $actual_image_name);
				$img = imagejpeg($source, $destinationPath, 75);
			}
			return $img;
		}
		else
		{
			return "false";
		}
	}
	/**
	 * update
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * @param string $where the WHERE query part
	 * @param string $now the NOW() function
	 */
	public function updateNow($table, $data, $where, $now)
	{
		ksort($data);
		$fieldDetails = NULL;
		$Wheredetail = NULL;
		foreach($data as $key=> $value) {
			$fieldDetails .= "`$key`=:$key,";
		}
		$now = $now.'=NOW()';
		$fieldDetails .= $now;
		//$fieldDetails = rtrim($fieldDetails, ',');

		foreach($where as $key=> $value) {
			$Wheredetail .= "`$key`=:$key,";
		}
		$Wheredetail = rtrim($Wheredetail, ',');
		$sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $Wheredetail");
		
		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
		foreach ($where as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		$Status = $sth->execute();
		// print_r($sth->errorInfo());
		 
		if($Status == true)
		{
			return "true";
		}
		else
		{
			return "false";
		}
	}
}
?>