<?php
/*
 * File: index.php
 * Created By: Deepak Bhardwaj
 */

class controller {
	function __construct() {
		$this->view = new view();
		$this->model = new model();
		session::init();
	}
}
 ?>