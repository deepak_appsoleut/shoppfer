<?php
/*
 * File: config.php
 * Created By: Deepak Bhardwaj
 */

class constants {

	/*
	 * Always use CAPS for constants and always prefix it with "APP_"
	 */ 

	function __construct($app = null) {

		//Use appropriate working environment | Production, Staging, Development
		$this->Development();

		//Set default-time zone for application globally
		date_default_timezone_set('Asia/Kolkata');

		//Application Specific Constants
		define('APP_NAME', 'Simplion - Appraisl System');
		define('APP_PUBLISHER', 'Simplion');
		define('APP_VERSION', '1.0');
		define('APP_SEC_KEY', 'H&q]`43309E,]}cNI_]u7g;62BG$:.#GD58iI3c*d_R2Lg413H|4f1K[6xqqd;4'); // 502 bit key
		
		define('APP_GOOGLE_KEY', 'AIzaSyBnJfKlOqtIePPu08e5Y-IXD8p9oYliMKg'); // 502 bit key
		/*
		 * Path specific constants
		 */

		//With reference to Application URL - FOR WEB
		define('APP_IMAGES', APP_URL.'public/assets/web/images/');
		define('APP_UPLOADS_PATH', APP_URL.'public/assets/web/uploads/');
		define('APP_CSS', APP_URL.'public/assets/web/css/');
		define('APP_JS', APP_URL.'public/assets/web/js/');
		define('APP_FONTS', APP_URL.'public/assets/web/fonts/');
		
		//With reference to Application URL - FOR WEB
		define('APP_CRM_UPLOADS_PATH', APP_URL.'public/assets/crm/uploads/');
		define('APP_CRM_BS', APP_URL.'public/assets/crm/');
		define('APP_CRM_PLUGIN', APP_URL.'public/assets/crm/plugins/');
		
		//With reference to Application Absolute Path - CRM
		define('APP_CRM_UPLOADS', APP_PATH.'public/assets/crm/uploads/');
		
		//With reference to Application Absolute Path
		define('APP_VIEW', APP_PATH.'application/engine/'.$app.'/view/');
		define('APP_MODEL', APP_PATH.'application/engine/'.$app.'/model/');
		define('APP_CONTROLLER', APP_PATH.'application/engine/'.$app.'/controller/');

		define('APP_UPLOADS', APP_PATH.'public/assets/web/uploads/');
		define('APP_MUSIC', APP_URL.'public/assets/web/music/');
		define('APP_LIBRARY', APP_PATH.'public/assets/web/library/');
		define('APP_ENGINE', APP_PATH.'application/engine/');
		define('APP_DEFAULT', APP_ENGINE.'default/');
	}

	function Development()
	{
		define('APP_URL', 'http://192.168.1.105:8080/shoppfer/'); //for dev
		//define('APP_URL', 'http://192.168.1.101:8080/shoppfer/'); //for live
		define('APP_PATH', 'd:/xampp/htdocs/shoppfer/');
		//define('APP_URL', 'http://www.shoppfer.com/'); //for live
		//define('APP_PATH', '/home/shoppfer/public_html/');
	}

	function Staging()
	{
		
	}

	function Production()
	{
		
	}
}
?>
