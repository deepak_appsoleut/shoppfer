<?php
/*
 * File: model.php
 * Created By: Deepak Bhardwaj
 */

class model{
	function __construct() {
		$this->db = new database();
	}

	/*
	 * Activity
	 * list, add, edit, delete	 
	 */

	function process($name = null, $activity = null, $param = null)
	{
		if ($name != null) {
			require_once APP_MODEL . $name . '.php';
			$class = $name."_model";
			$GLOBALS["db"] = $this->db;
			$model = new $class();
			if($activity != null)
			{
				if($param != null)
				{
					return $model->$activity($param[0]);
				}
				else
				{
					return $model->$activity();
				}				
			}			
		}
	}
}
?>