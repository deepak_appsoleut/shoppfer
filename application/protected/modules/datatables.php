<?php
/*
 * File: index.php
 * Created By: Deepak Bhardwaj
 */

class datatables {
	public function ReturnData($GetData = array(), $skipArray = array(), $table = null, $zid = null, $smid = null) {		
		//Range set
		$Range = array('zone'=> 0, 'zonal_manager'=> 5, 'region'=>12, 'regional_manager'=>17, 'branch'=>24, 'branch_manager'=>29, 'sales_manager'=>36, 'advisor'=>45, 'testimonial'=>53, 'users'=>57, 'end'=>67); 
		
		$ReturnData = null;
		if(empty($GetData))
		{
			$ReturnData = '<h2 style="padding-top:100px; color:#CCC; text-align:center;"><em>No data to display.</em></h2>';
			return $ReturnData;
		}
		if($table=='advisor')
		{
			$smid = $GetData[0]['sl_sm_id'];
		}
		if($table == 'sm_media')
		{
			$noedit = 'display:none;';
		}
		else
		{
			$noedit = '';
		}
		$textBox = null;
		$DateFilter = null;
		$BuildString = '{},{},'; $smPrefix = false;
		for ($m=3; $m<count($GetData[0]);$m++)
		{
			if(($table == 'advisor' && $m==3) || ($table == 'testimonial' && $m==3))
			{
				//Do nothing
			}
			else {
				$BuildString .= '{type: "checkbox", values: null},';
			}
		}
		$BuildString .= '{}';
		
		if(($table == 'advisor') || ($table == 'testimonial'))
		{
			$showStar = 'display:block';
		}
		else
		{
			$showStar = 'display:none';
		}
		if(($table == 'zone') || ($table == 'region') || ($table == 'branch') || ($table == 'testimonial') || ($table == 'sm_media'))
		{
			$noAjax = 'ajax';
			$noDisplay  = 'display:none';
		}
		else
		{
			$noAjax = '';
			$noDisplay = 'display:block';
		}
		if($table == 'sales_manager')
		{
			$visitWeb = 'display:block';
			$addTesti = 'display:block';
			$addDetails = 'display:block';
		}
		else
		{
			$visitWeb = 'display:none';
			$addTesti = 'display:none';
			$addDetails = 'display:none';
		}
		$ReturnData .= '
		<link href="'.APP_URL.'public/resources/css/demo_page.css" rel="stylesheet" type="text/css" />
		<link href="'.APP_URL.'public/resources/css/demo_table.css" rel="stylesheet" type="text/css" />
		<link href="'.APP_URL.'public/resources/css/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="'.APP_URL.'public/resources/css/data-table.css" type="text/css" rel="stylesheet" />
		<script src="'.APP_URL.'public/resources/js/data-table.js" type="text/javascript"></script>
		<script src="'.APP_URL.'public/resources/js/jquery-ui.js" type="text/javascript"></script>
		<script type="text/javascript" src="'.APP_URL.'public/resources/js/clipboard.js"></script>
		<script type="text/javascript" src="'.APP_URL.'public/resources/js/table-tools.js"></script>
		<script type="text/javascript" src="'.APP_URL.'public/resources/js/columnFilter.js"></script>
		<script type="text/javascript">
				$(document).ready( function () {
			 var oTable = $("#example").dataTable({"bJQueryUI": true,
								"sPaginationType": "full_numbers",
								"sDom": \'T<"clear">lfrtip\',
								"oTableTools": {
									"sRowSelect": "multi",
									"fnRowSelected": function (node) {
									jQuery(node).find("input[type=checkbox]").attr("checked", true);
									},
									"fnRowDeselected": function ( node ) {
									jQuery(node).find("input[type=checkbox]").attr("checked", false);
									},
									"aButtons": [
									{
										"sExtends":    "copy",
										"bSelectedOnly": "true"
									},
									{
										"sExtends":    "pdf",
										"bSelectedOnly": "true"
									},
									{
										"sExtends":    "csv",
										"bSelectedOnly": "true"
									},
									"print",
									"select_all", "select_none"
									]
								}
					}).columnFilter({sPlaceHolder: "head:after",aoColumns: [
					                                                  	  '.$BuildString.']});
				    new FixedHeader( oTable, { "top": true } );
;
			});
		</script>
		<table cellpadding="0" cellspacing="0" border="1px" bordercolor="#ccc" class="display" id="example" style="margin-top:30px; border-collapse:collapse; background-color:#fff; border-color:#ccc;">
		<thead>';
		//Displaying headers
		$round = 1; $StatusKey = null; $IsSOM = null;
		for($i = 1;$i<=2;$i++)
		{
			foreach($GetData as $key=>$value)
			{
				$ReturnData .= '<tr>';
				foreach($GetData[$key] as $keyIn => $valueIn)
				{
					foreach($skipArray as $val)
					{
						//Check if the key is a status
						$IsStatus = strstr($keyIn, "_status");
						if($IsStatus != null)
						{
							$StatusKey = $keyIn;
						}
						
						//Check if it is star of the month: Advisor only
						$IsSOM = strstr($keyIn, "_som");
						$IsFeat = strstr($keyIn, "_featured");
	
						//Check if $SkipArray value exists in $KeyIn
						$Exists = strstr($keyIn, $val);
						if($Exists != null)
						{
							$keyIn = ucfirst(str_replace($val, '', $keyIn)); //Replace with $skippArray and make first letter uppercase
							$keyIn = str_replace('_', ' ', $keyIn); //Replacing any '_' with ' '
							break;
						}
					}
					if(($round == 1) || ($round == (1+count($GetData[$key]))))
					{
						$ReturnData .= '<th class="noIconDisplay"></th>';
					}
					elseif(($IsSOM != null) || ($IsFeat != null)){
						//Dont do anything
					}
					elseif(($round == count($GetData[$key])) || ($round == (count($GetData[$key])*2)))
					{
						$ReturnData .= '<th class="noIconDisplay">Operation</th>';
					}
					else
					{
						$ReturnData .= '<th>'.$keyIn.'</th>';
					}
					$round++;
				}
				$ReturnData .= '</tr>';
				break;
			}
		}
		$ReturnData .= "</thead>
			<tbody>";

			$round = 1; $star = null;
			$perm = unserialize($_SESSION['app_luser']['access_privileges']);
			foreach($GetData as $key=>$value)
			{
				$ReturnData .= '<tr>';
				foreach($GetData[$key] as $keyIn => $valueIn)
				{
					if($keyIn == 'sm_prefix')
					{
						$smPrefix = true;
					}
					
					$PrintStatus = null; $Style = null;

					//Checking if the value is a datetime
					if(preg_match("/^\d{4}-\d{2}-\d{2} ([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", $valueIn))
					{
						$date = new DateTime($valueIn);
						$valueIn = $date->format('d M Y, h:i');
					}

					//Displaying Data
					if($round == 1 )
					{
						$ReturnData .= '<td><input type="checkbox" name="check" value="'.$GetData[$key][$keyIn].'" id="'.$GetData[$key][$keyIn].'"/></td>';
					}
					elseif(($keyIn == 'advisor_som')|| ($keyIn == 'testi_featured'))
					{
						//Do nothing
					}
					elseif($round == count($GetData[$key]))
					{
						$hlink = '';
						if($smPrefix == true)
						{
							$hlink = APP_URL.$GetData[$key]['sm_prefix'];
						}
						$ReturnData .= '<td>';//<a href="add/add.php?ID=1&type=AddZonalMgr" class="ajax addIcon" title="Add Zonal Manager"></a>
						
						//Setting accrding to Permissions
						
						$PriorityRound = 1;
						
						//Get Next value of element in an associative array
						$current = $table;
						$keys = array_keys($Range);
						$ordinal = (array_search($current,$keys)+1)%count($keys);
						$nextValue = $keys[$ordinal];
						for($q = $Range[$table]+2; $q < $Range[$nextValue]; $q++)
						{
							if($PriorityRound == 1) //This one is always for edit
							{
								if($perm[$q] == 1)
								{
									$ReturnData .= '<a href="'.APP_ADMIN.'edit/'.$table.'/'.$GetData[$key][$zid].'" style="'.$noedit.'" title="Edit" class="'.$noAjax.' editIcon"></a>';
								}
							}
							elseif($PriorityRound == 2) //This one is always for delete
							{
								if($perm[$q] == 1)
								{
									$ReturnData .= '<a href="'.APP_ADMIN.'delete/'.$table.'/'.$GetData[$key][$zid].'" title="Delete" class="ajax deleteIcon"></a>';
								}
							}
							elseif($PriorityRound == 3) //This one is always for view more
							{
								if($perm[$q] == 1)
								{
									$ReturnData .= '<a href="'.APP_ADMIN.'more/'.$table.'/'.$GetData[$key][$zid].'" title="View More" class="ajax moreIcon" style='.$noDisplay.'></a>';
								}
							}
							elseif($PriorityRound == 4) //This one is always for vist website
							{
								if($perm[$q] == 1)
								{
									$ReturnData .= '<a href="'.$hlink.'" title="Visit Website" target="_blank" class="webIcon" style='.$visitWeb.'></a>';
								}
							}
							elseif($PriorityRound == 5) //This one is always for Add details
							{
								if($perm[$q] == 1)
								{
									$ReturnData .= '<a href="'.APP_ADMIN.'detail/'.$table.'/'.$GetData[$key][$zid].'" title="Add Details" class="ajax detailsIcon" style='.$addDetails.'></a><a href="'.APP_ADMIN.'sm_media/'.$table.'/'.$GetData[$key][$zid].'" title="Add Media" class="mediaIcon" style='.$addDetails.'></a>';
								}
							}
							$PriorityRound++;
						}
					if(isset($perm[53]) && $perm[53] == 1) {
					$ReturnData .= '<a href="'.APP_ADMIN.'testimonial/'.$table.'/'.$GetData[$key][$zid].'" title="Add testimonial" class="testiIcon" style='.$addTesti.'></a>';
					}
					$ReturnData .= '</td>';
					}
					else
					{
						if(($showStar == "display:block") && ($table =='advisor'))
						{
							if($GetData[$key]['advisor_som'] == 1)
							{
								$star = "star_gold";
							}
							else {
								$star = "star_gray";
							}
						}
						if(($showStar == "display:block") && ($table =='testimonial'))
						{
							if($GetData[$key]['testi_featured'] == 1)
							{
								$star = "star_green";
							}
							else {
								$star = "star_gray";
							}
						}
						if($round == 2)
						{
							if(($table != 'testimonial') && ($table != 'sm_media'))
							{
								 $status = $GetData[$key][$StatusKey];
								 if($status == 1)
								 {
									$PrintStatus = 'Active';
									$Style = 'enable';
	
								 }
								 else
								 {
									$PrintStatus = 'Inactive';
									$Style = 'disable';
								 }
							}
							 $ReturnData .= '<td >';
							 if(((isset($perm[51]) && $perm[51] == 1) && (($table =='advisor'))) || ((isset($perm[65]) && $perm[65] == 1) && ($table =='testimonial'))) 
							  { $ReturnData .='<a href="'.APP_ADMIN.'testimonial/'.$table.'_process/'.$GetData[$key][$zid].'/'.$smid.'" style='.$showStar.' class="'.$star.' ajax"></a>'; }
							   $ReturnData .= $valueIn.'  <span class="'.$Style.'">'.$PrintStatus.'</span></td>';
						}
						else
						{
							$ReturnData .= '<td >'.$valueIn.'</td>';
						}
					}
					$round++;
				}
				$ReturnData .= '</tr>';
				$round = 1;
			}
			$ReturnData .= "
			</tbody>
		</table>";
			return $ReturnData;
	}
}
 ?>