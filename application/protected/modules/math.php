<?php
/*
 * File: math.php
 * Created By: Deepak Bhardwaj
 */

class math {

	/**
     * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
     * @param string $data The data to encode
     * @param string $salt The salt (This should be the same throughout the system probably)
     * @return string The hashed/salted data
     */
    public static function hash($algo, $data, $salt)
    {
        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context, $data);
        return hash_final($context);
    }

    public static function randomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 8; $i++) 
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
	public static function hash_function($publicKey,$signature,$privateKey)
	{
	   $hash = base64_encode(hash_hmac('sha256',$publicKey,$privateKey, true));
	   if($hash==$signature)
	   {
	   	   return true;
	   }
	   else
	   {
		   return false;
	   }
	}
	public static function registerTemplate($business_name, $link)
	{
		$link = APP_URL."default/home/verifyEmail/".$link;
		$logo = APP_IMAGES."png-logo.png";
		$var = <<<EOD
<body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style='font-family:Helvetica, Arial,serif;'>

  <tr>
    <td height='35'></td>
  </tr>
  <tr>
    <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background: #efefef;">
        <tr>
          <td width='40'></td>
          <td width='520'><table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
              
              <!-- =============================== Header ====================================== -->
              
              <tr>
                <td height='75'></td>
              </tr>
              <!-- =============================== Body ====================================== -->
              
              <tr>
                <td class='movableContentContainer' valign='top'><div lass='movableContent'>
                    <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td valign='top' align='center'><div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable">
                              <p style='text-align:center;margin:0; margin-bottom:25px;font-size:26px;color:#01635A;'>Welcome to</p>
                            </div>
                          </div></td>
                      </tr>
                    </table>
                  </div>
                  <div lass='movableContent'>
                    <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td valign='top' align='center'><div class="contentEditableContainer contentImageEditable">
                            <div class="contentEditable"> <img src="$logo" width='423' height='61' alt='' data-default="placeholder" data-max-width="560"> </div>
                          </div></td>
                      </tr>
                    </table>
                  </div>
                  <div class='movableContent'>
                    <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td height='55'></td>
                      </tr>
                      <tr>
                        <td align='left'><div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable" align='center'>
                              <h2 style="text-align:left;color:#01635A;font-size:19px;font-weight:normal;">Hi $business_name,</h2>
                            </div>

                          </div></td>
                      </tr>
                      <tr>
                        <td height='15'></td>
                      </tr>
                      <tr>
                        <td align='left'><div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable" align='center'>
                              <p  style='text-align:left;color:#454545;font-size:14px;font-weight:normal;line-height:19px;'> Here’s what you can say: Thanks again for signing up to the newsletter! You’re all set up, and will be getting the emails once per week. Meanwhile, you can check out our Getting Started section to get the most out of your new account. <br>
                                <br>
                                Have questions? Get in touch with us via Facebook or Twitter, or email our support team. <br>
                                <br>
                              </p>
                            </div>
                          </div></td>
                      </tr>
                      <tr>
                        <td height='55'></td>
                      </tr>
                      <tr>
                        <td align='center'><table>
                            <tr>
                              <td align='center' bgcolor='#289CDC' style='background:#ef1428; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'><div class="contentEditableContainer contentTextEditable">
                                  <div class="contentEditable" align='center'> <a target='_blank' href='$link' class='link2' style='color:#ffffff; font-size:16px;text-decoration:none;'>Activate your Account</a> </div>
                                </div></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td height='20'></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
              
              <!-- =============================== footer ====================================== -->
              
            </table></td>
          <td width='40'></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height='88'></td>
  </tr>
</table>
</body>
EOD;
return $var;
	}
	
	
	
	public static function resetTemplate($business_name, $link)
	{
		$link = APP_URL."admin_mp/login/reset_password/".$link;
		$logo = APP_IMAGES."png-logo.png";
		$var = <<<EOD
<body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style='font-family:Helvetica, Arial,serif;'>

  <tr>
    <td height='35'></td>
  </tr>
  <tr>
    <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background: #efefef;">
        <tr>
          <td width='40'></td>
          <td width='520'><table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
              
              <!-- =============================== Header ====================================== -->
              
              <tr>
                <td height='75'></td>
              </tr>
              <!-- =============================== Body ====================================== -->
              
              <tr>
                <td class='movableContentContainer' valign='top'><div lass='movableContent'>
                    <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td valign='top' align='center'><div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable">
                              <p style='text-align:center;margin:0; margin-bottom:25px;font-size:26px;color:#01635A;'>Welcome to</p>
                            </div>
                          </div></td>
                      </tr>
                    </table>
                  </div>
                  <div lass='movableContent'>
                    <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td valign='top' align='center'><div class="contentEditableContainer contentImageEditable">
                            <div class="contentEditable"> <img src="$logo" width='423' height='61' alt='' data-default="placeholder" data-max-width="560"> </div>
                          </div></td>
                      </tr>
                    </table>
                  </div>
                  <div class='movableContent'>
                    <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td height='55'></td>
                      </tr>
                      <tr>
                        <td align='left'><div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable" align='center'>
                              <h2 style="text-align:left;color:#01635A;font-size:19px;font-weight:normal;">Hi $business_name,</h2>
                            </div>

                          </div></td>
                      </tr>
                      <tr>
                        <td height='15'></td>
                      </tr>
                      <tr>
                        <td align='left'><div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable" align='center'>
                              <p  style='text-align:left;color:#454545;font-size:14px;font-weight:normal;line-height:19px;'> Here’s what you can say: Thanks again for signing up to the newsletter! You’re all set up, and will be getting the emails once per week. Meanwhile, you can check out our Getting Started section to get the most out of your new account. <br>
                                <br>
                                Have questions? Get in touch with us via Facebook or Twitter, or email our support team. <br>
                                <br>
                              </p>
                            </div>
                          </div></td>
                      </tr>
                      <tr>
                        <td height='55'></td>
                      </tr>
                      <tr>
                        <td align='center'><table>
                            <tr>
                              <td align='center' bgcolor='#289CDC' style='background:#ef1428; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'><div class="contentEditableContainer contentTextEditable">
                                  <div class="contentEditable" align='center'> <a target='_blank' href='$link' class='link2' style='color:#ffffff; font-size:16px;text-decoration:none;'>Reset Your Password</a> </div>
                                </div></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td height='20'></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
              
              <!-- =============================== footer ====================================== -->
              
            </table></td>
          <td width='40'></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height='88'></td>
  </tr>
</table>
</body>
EOD;
return $var;
	}
}
?>